# README #

![OrcNetStartPage.png](https://bitbucket.org/repo/rd8z96/images/1494488992-OrcNetStartPage.png)

*                                     WPF Start Page

![AvaloniaStartPage.png](https://bitbucket.org/repo/rd8z96/images/2100828229-AvaloniaStartPage.png)

*                                   Avalonia Start Page

![MinimalSample.png](https://bitbucket.org/repo/rd8z96/images/3350238856-MinimalSample.png)

*                                 Sample 1: Texture loading.


![Sample2.png](https://bitbucket.org/repo/rd8z96/images/3237218455-Sample2.png)

*                                 Sample 2: Simple rendering.


![Sample3.png](https://bitbucket.org/repo/rd8z96/images/2065042570-Sample3.png)

*                            Sample 3: Resource loading (Sample 2 from files)


![Sample4.png](https://bitbucket.org/repo/rd8z96/images/674126510-Sample4.png)

*                         Sample 4: SceneGraph creation (Sample 2 using scene graph)

![Sample5.png](https://bitbucket.org/repo/rd8z96/images/3404470032-Sample5.png)

*                   Sample 5: Resource SceneGraph loading with advanced shaders (postprocessing)


![Sample6.png](https://bitbucket.org/repo/rd8z96/images/3964682901-Sample6.png)

*                         Sample 6: Tesselation using control and evaluation stages.


### What is this repository for? ###

* OrcNet is based on the Eric Bruneton Ork engine but written in C#.
* Version 1.0
* See http://ork.gforge.inria.fr/ for more information.

### How do I get set up? ###

* Checkout the repository.
* The OpenTK package is linked to the Graphic module (precompiled Dlls instead of a git sub module that we would have to compile).
* Compile and this is it. (For now...)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact