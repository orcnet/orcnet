﻿namespace OrcNet.Constants
{
    /// <summary>
    /// Contains all OrcNet constants in a single 
    /// static class definition.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Stores the major version of OrcNet
        /// </summary>
        public const string MAJOR = "1";

        /// <summary>
        /// Stores the major version of OrcNet
        /// </summary>
        public const string MINOR = ".0";

        /// <summary>
        /// Stores the build version of OrcNet
        /// </summary>
        public const string BUILD = ".0";

        /// <summary>
        /// Stores the revision version of OrcNet
        /// </summary>
        public const string REVISION = ".0";

        /// <summary>
        /// Stores the copyright of OrcNet
        /// </summary>
        public const string COPYRIGHT = "OrcNet © 2016";

        /// <summary>
        /// Stores the company name of OrcNet creators.
        /// </summary>
        public const string COMPANY = "ORCLAND";
    }
}
