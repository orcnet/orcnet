﻿using OrcNet.Core.Logger;
using OrcNet.Windows.Samples.Pages;
using OrcNet.Windows.UI.View;
using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OrcNet.Windows.Samples
{
    /// <summary>
    /// Definition of the <see cref="SampleWindow"/> class.
    /// </summary>
    public partial class SampleWindow : Window
    {
        #region DependencyProperty

        /// <summary>
        /// Is displaying sample dependency property.
        /// </summary>
        public static readonly DependencyProperty IsDisplayingSampleProperty = DependencyProperty.Register( "IsDisplayingSample", typeof(bool), typeof(SampleWindow), new PropertyMetadata(false));

        #endregion DependencyProperty

        #region Fields

        /// <summary>
        /// Stores the close image bitmap.
        /// </summary>
        private readonly static BitmapImage sCloseImage;

        /// <summary>
        /// Stores the minimize image bitmap.
        /// </summary>
        private readonly static BitmapImage sMinimizeImage;

        /// <summary>
        /// Stores the maximize image bitmap.
        /// </summary>
        private readonly static BitmapImage sMaximizeImage;

        /// <summary>
        /// Stores the current sample.
        /// </summary>
        private ASample mCurrentSample;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the close image source.
        /// </summary>
        public ImageSource CloseImage
        {
            get
            {
                return sCloseImage;
            }
        }

        /// <summary>
        /// Gets the minimize image bitmap.
        /// </summary>
        public ImageSource MinimizeImage
        {
            get
            {
                return sMinimizeImage;
            }
        }

        /// <summary>
        /// Gets the maximize image bitmap.
        /// </summary>
        public ImageSource MaximizeImage
        {
            get
            {
                return sMaximizeImage;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the window is displaying a sample or not.
        /// </summary>
        public bool IsDisplayingSample
        {
            get
            {
                return (bool)GetValue( IsDisplayingSampleProperty );
            }
            private set
            {
                SetValue( IsDisplayingSampleProperty, value );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="SampleWindow"/> class.
        /// </summary>
        static SampleWindow()
        {
            string lCloseIconName    = Path.Combine( Environment.CurrentDirectory, @"..\Resources\Window\Icons\CloseWindow.ico");
            string lMinimizeIconName = Path.Combine( Environment.CurrentDirectory, @"..\Resources\Window\Icons\MinimizeWindow.ico");
            string lMaximizeIconName = Path.Combine( Environment.CurrentDirectory, @"..\Resources\Window\Icons\MaximizeWindow.ico");
            sCloseImage    = new BitmapImage( new Uri( lCloseIconName ) );
            sMinimizeImage = new BitmapImage( new Uri( lMinimizeIconName ) );
            sMaximizeImage = new BitmapImage( new Uri( lMaximizeIconName ) );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleWindow"/> class.
        /// </summary>
        public SampleWindow()
        {
            this.InitializeComponent();

            this.Title = "WPF OrcNet Samples";

            this.DataContext = this;

            string lIconName = Path.Combine( Environment.CurrentDirectory, @"..\Resources\Assets\Textures\OrcIcon.ico" );
            this.Icon = new BitmapImage( new Uri( lIconName ) );

            this.Loaded += this.OnSampleWindowLoaded;
        }
        
        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Adjusts the WindowSize to correct parameters when Maximize button is clicked
        /// </summary>
        private void AdjustWindowSize()
        {
            if ( this.WindowState == WindowState.Maximized )
            {
                this.WindowState = WindowState.Normal;
                //MaximizeButton.Content = "1";
            }
            else
            {
                this.WindowState = WindowState.Maximized;
                //MaximizeButton.Content = "2";
            }
        }

        #endregion Methods Internal

        #region Methods Events

        /// <summary>
        /// Delegate called on window loaded.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSampleWindowLoaded(object pSender, RoutedEventArgs pEventArgs)
        {
            Core.Application.Instance.Initialize( LoggerType.CONSOLE );
        }

        /// <summary>
        /// Delegate called on title bar mouse down.
        /// Drag if single-click, resize if double-click
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnTitleBarMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.ChangedButton == MouseButton.Left )
            {
                if ( pEventArgs.ClickCount == 2 )
                {
                    this.AdjustWindowSize();
                }
                else
                {
                    Application.Current.MainWindow.DragMove();
                }
            }
        }

        /// <summary>
        /// Delegate called on close button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnCloseButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Delegate called on thumb close button info click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnCloseThumbButtonInfoClick(object pSender, EventArgs pEventArgs)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Delegate called on maximize button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnMaximizeButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            this.AdjustWindowSize();
        }

        /// <summary>
        /// Delegate called on minimize button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnMinimizeButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            this.WindowState = WindowState.Minimized;
        }
        
        /// <summary>
        /// Delegate called in sample button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSampleButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            Button lSender = pSender as Button;
            if ( lSender != null )
            {
                Type lSampleType = null;
                if ( lSender.Name == "mSample1Button" )
                {
                    lSampleType = typeof(MinimalSample);
                }
                else if ( lSender.Name == "mSample2Button" )
                {
                    lSampleType = typeof(ResourceSample);
                }
                else if ( lSender.Name == "mSample3Button" )
                {
                    lSampleType = typeof(RenderSample);
                }
                else if ( lSender.Name == "mSample4Button" )
                {
                    lSampleType = typeof(SceneGraphSample);
                }
                else if ( lSender.Name == "mSample5Button" )
                {
                    lSampleType = typeof(SceneGraphResourceSample);
                }
                else if ( lSender.Name == "mSample6Button" )
                {
                    lSampleType = typeof(TessellationSample);
                }

                // Auto create.
                if ( lSampleType != null )
                {
                    ConstructorInfo lCtor = lSampleType.GetConstructor( new Type[] { typeof(OrcView) } );
                    if ( lCtor != null )
                    {
                        this.IsDisplayingSample = true;

                        // Hide the start page.
                        this.mStartPage.Visibility = Visibility.Hidden;
                        
                        // Plugs the new sample with the rendering view.
                        this.mCurrentSample = lCtor.Invoke( new object[] { this.mRenderingView } ) as ASample;
                        this.mCurrentSample.Initialize();

                        // Show the rendering panel.
                        this.mRendering.Visibility = Visibility.Visible;
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called on start page button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnStartPageButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            this.IsDisplayingSample = false;

            this.mStartPage.Visibility = Visibility.Visible;

            if ( this.mCurrentSample != null )
            {
                this.mCurrentSample.Dispose();
                this.mCurrentSample = null;
            }
            
            this.mRendering.Visibility = Visibility.Hidden;
        }

        #endregion Methods Events

        #endregion Methods
    }
}
