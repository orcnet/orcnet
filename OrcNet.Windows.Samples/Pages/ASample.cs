﻿using OrcNet.Core.Service;
using OrcNet.Core.UI;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Services;
using OrcNet.Windows.UI.View;
using System;

namespace OrcNet.Windows.Samples.Pages
{
    /// <summary>
    /// Definition of the <see cref="ASample"/> class.
    /// </summary>
    public abstract class ASample : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the sample has been disposed.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the orc view panel which delegates rootine must be set to.
        /// </summary>
        protected OrcView mView;

        /// <summary>
        /// Stores the rendering parameters for the view.
        /// </summary>
        protected ViewParameters mParameters;

        /// <summary>
        /// Stores the FBO.
        /// </summary>
        private FrameBuffer mFrameBuffer;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the FBO.
        /// </summary>
        protected FrameBuffer FrameBuffer
        {
            get
            {
                if ( this.mFrameBuffer == null )
                {
                    IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                    this.mFrameBuffer = lRenderService.DefaultFrameBuffer;
                }

                return this.mFrameBuffer;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ASample"/> class.
        /// </summary>
        /// <param name="pView">The view sample content will be rendered to.</param>
        /// <param name="pParameters">The rendering parameters for the view.</param>
        protected ASample(OrcView pView, ViewParameters pParameters)
        {
            this.mParameters = pParameters;
            this.mView = pView;
            this.mIsDisposed = false;
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the sample.
        /// </summary>
        internal virtual void Initialize()
        {
            // Init the orc view so that it be ready for rendering the sample.
            this.mView.InitializeView( this.mParameters );
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDisposed();

                this.mView = null;

                // Releases the handle to the frame buffer.
                this.mFrameBuffer = null;

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDisposed()
        {

        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
