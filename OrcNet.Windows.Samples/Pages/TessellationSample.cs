﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Math;
using OrcNet.Core.Service;
using OrcNet.Core.UI;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Services;
using OrcNet.Windows.UI.View;
using System.Text;

namespace OrcNet.Windows.Samples.Pages
{
    /// <summary>
    /// Definition of the <see cref="TessellationSample"/> class.
    /// </summary>
    public class TessellationSample : ASample
    {
        #region Fields

        /// <summary>
        /// Stores the model to render.
        /// </summary>
        private Mesh<Vertex3, uint> mModel;

        /// <summary>
        /// Stores the local to screen transform.
        /// </summary>
        private Matrix4FUniform mLocalToScreen;

        /// <summary>
        /// Stores the pipeline pass used to render.
        /// </summary>
        private PipelinePass mPass;

        /// <summary>
        /// Stores the inner tesselation factor.
        /// </summary>
        FloatUniform mInner;

        /// <summary>
        /// Stores the outer tesselation factor.
        /// </summary>
        FloatUniform mOuter;

        /// <summary>
        /// Stores the field of view in degrees.
        /// </summary>
        private float mFov;

        /// <summary>
        /// Stores the alpha camera rotation angle in degrees.
        /// </summary>
        private float mAlpha;

        /// <summary>
        /// Stores the theta camera rotation angle in degrees.
        /// </summary>
        private float mTheta;

        /// <summary>
        /// Stores the distance of the camera to the scene.
        /// </summary>
        private float mDistance;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TessellationSample"/> class.
        /// </summary>
        /// <param name="pView">The view sample content will be rendered to.</param>
        public TessellationSample(OrcView pView) :
        base( pView, new ViewParameters() { UseDepth = true  }.SetSize( 1024, 768 ).SetVersion( 4, 0 ) )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the sample.
        /// </summary>
        internal override void Initialize()
        {
            base.Initialize();

            this.mView.Rendering    += this.OnRender;
            this.mView.Resizing     += this.OnResize;
            this.mView.MouseMoved   += this.OnMouseMoved;
            this.mView.MouseWheeled += this.OnMouseWheeled;

            this.mFov      = 80.0f;
            this.mAlpha    = 135.0f;
            this.mTheta    = 45.0f;
            this.mDistance = 5.0f;

            this.mModel = new Mesh<Vertex3, uint>( PrimitiveType.Patches, MeshUsage.GPU_STATIC );
            this.mModel.VerticesPerPatch = 3;
            this.mModel.AddAttributeType( 0, 3, AttributeType.FLOAT_BASED, false );
            this.mModel.AddVertex( new Vertex3( 0.000f, 0.000f, 1.000f ) );
            this.mModel.AddVertex( new Vertex3( 0.894f, 0.000f, 0.447f ) );
            this.mModel.AddVertex( new Vertex3( 0.276f, 0.851f, 0.447f ) );
            this.mModel.AddVertex( new Vertex3( -0.724f, 0.526f, 0.447f ) );
            this.mModel.AddVertex( new Vertex3( -0.724f, -0.526f, 0.447f ) );
            this.mModel.AddVertex( new Vertex3( 0.276f, -0.851f, 0.447f ) );
            this.mModel.AddVertex( new Vertex3( 0.724f, 0.526f, -0.447f ) );
            this.mModel.AddVertex( new Vertex3( -0.276f, 0.851f, -0.447f ) );
            this.mModel.AddVertex( new Vertex3( -0.894f, 0.000f, -0.447f ) );
            this.mModel.AddVertex( new Vertex3( -0.276f, -0.851f, -0.447f ) );
            this.mModel.AddVertex( new Vertex3( 0.724f, -0.526f, -0.447f ) );
            this.mModel.AddVertex( new Vertex3( 0.000f, 0.000f, -1.000f ) );
            this.mModel.AddIndex( 2 );
            this.mModel.AddIndex( 1 );
            this.mModel.AddIndex( 0 );
            this.mModel.AddIndex( 3 );
            this.mModel.AddIndex( 2 );
            this.mModel.AddIndex( 0 );
            this.mModel.AddIndex( 4 );
            this.mModel.AddIndex( 3 );
            this.mModel.AddIndex( 0 );
            this.mModel.AddIndex( 5 );
            this.mModel.AddIndex( 4 );
            this.mModel.AddIndex( 0 );
            this.mModel.AddIndex( 1 );
            this.mModel.AddIndex( 5 );
            this.mModel.AddIndex( 0 );
            this.mModel.AddIndex( 11 );
            this.mModel.AddIndex( 6 );
            this.mModel.AddIndex( 7 );
            this.mModel.AddIndex( 11 );
            this.mModel.AddIndex( 7 );
            this.mModel.AddIndex( 8 );
            this.mModel.AddIndex( 11 );
            this.mModel.AddIndex( 8 );
            this.mModel.AddIndex( 9 );
            this.mModel.AddIndex( 11 );
            this.mModel.AddIndex( 9 );
            this.mModel.AddIndex( 10 );
            this.mModel.AddIndex( 11 );
            this.mModel.AddIndex( 10 );
            this.mModel.AddIndex( 6 );
            this.mModel.AddIndex( 1 );
            this.mModel.AddIndex( 2 );
            this.mModel.AddIndex( 6 );
            this.mModel.AddIndex( 2 );
            this.mModel.AddIndex( 3 );
            this.mModel.AddIndex( 7 );
            this.mModel.AddIndex( 3 );
            this.mModel.AddIndex( 4 );
            this.mModel.AddIndex( 8 );
            this.mModel.AddIndex( 4 );
            this.mModel.AddIndex( 5 );
            this.mModel.AddIndex( 9 );
            this.mModel.AddIndex( 5 );
            this.mModel.AddIndex( 1 );
            this.mModel.AddIndex( 10 );
            this.mModel.AddIndex( 2 );
            this.mModel.AddIndex( 7 );
            this.mModel.AddIndex( 6 );
            this.mModel.AddIndex( 3 );
            this.mModel.AddIndex( 8 );
            this.mModel.AddIndex( 7 );
            this.mModel.AddIndex( 4 );
            this.mModel.AddIndex( 9 );
            this.mModel.AddIndex( 8 );
            this.mModel.AddIndex( 5 );
            this.mModel.AddIndex( 10 );
            this.mModel.AddIndex( 9 );
            this.mModel.AddIndex( 1 );
            this.mModel.AddIndex( 6 );
            this.mModel.AddIndex( 10 );

            StringBuilder lPassSource = new StringBuilder();
            lPassSource.AppendLine( "#ifdef _VERTEX_" );
            lPassSource.AppendLine( "layout(location = 0) in vec3 pos;" );
            lPassSource.AppendLine( "out vec3 vPos;" );
            lPassSource.AppendLine( "void main() {" );
            lPassSource.AppendLine( "    vPos = pos;" );
            lPassSource.AppendLine( "}" );
            lPassSource.AppendLine( "#endif" );
            lPassSource.AppendLine( "#ifdef _TESS_CONTROL_" );
            lPassSource.AppendLine( "layout(vertices = 3) out;");
            lPassSource.AppendLine( "in vec3 vPos[];");
            lPassSource.AppendLine( "out vec3 tcPos[];" );
            lPassSource.AppendLine( "uniform float inner;" );
            lPassSource.AppendLine( "uniform float outer;" );
            lPassSource.AppendLine( "void main() {" );
            lPassSource.AppendLine( "    tcPos[gl_InvocationID] = vPos[gl_InvocationID];" );
            lPassSource.AppendLine( "    if (gl_InvocationID == 0)");
            lPassSource.AppendLine( "    {" );
            lPassSource.AppendLine( "        gl_TessLevelInner[0] = inner;" );
            lPassSource.AppendLine( "        gl_TessLevelOuter[0] = outer;" );
            lPassSource.AppendLine( "        gl_TessLevelOuter[1] = outer;" );
            lPassSource.AppendLine( "        gl_TessLevelOuter[2] = outer;" );
            lPassSource.AppendLine( "    }" );
            lPassSource.AppendLine( "}" );
            lPassSource.AppendLine( "#endif" );
            lPassSource.AppendLine( "#ifdef _TESS_EVAL_" );
            lPassSource.AppendLine( "layout(triangles, equal_spacing, cw) in;" );
	        lPassSource.AppendLine(  "in vec3 tcPos[];" );
            lPassSource.AppendLine( "uniform mat4 localToScreen;" );
            lPassSource.AppendLine( "void main() {" );
            lPassSource.AppendLine( "    vec3 p0 = gl_TessCoord.x * tcPos[0];" );
            lPassSource.AppendLine( "    vec3 p1 = gl_TessCoord.y * tcPos[1];" );
            lPassSource.AppendLine( "    vec3 p2 = gl_TessCoord.z * tcPos[2];" );
            lPassSource.AppendLine( "    vec3 p = normalize(p0 + p1 + p2);" );
            lPassSource.AppendLine( "    gl_Position = localToScreen * vec4(p, 1.0);" );
            lPassSource.AppendLine( "}" );
            lPassSource.AppendLine( "#endif" );
            lPassSource.AppendLine( "#ifdef _GEOMETRY_" );
            lPassSource.AppendLine( "layout(triangles) in;" );
            lPassSource.AppendLine( "layout(triangle_strip, max_vertices = 3) out;" );
            lPassSource.AppendLine( "out vec3 triDist;" );
            lPassSource.AppendLine( "void main() {" );
            lPassSource.AppendLine( "    gl_Position = gl_in[0].gl_Position; triDist = vec3(1.0, 0.0, 0.0); EmitVertex();" );
            lPassSource.AppendLine( "    gl_Position = gl_in[1].gl_Position; triDist = vec3(0.0, 1.0, 0.0); EmitVertex();" );
            lPassSource.AppendLine( "    gl_Position = gl_in[2].gl_Position; triDist = vec3(0.0, 0.0, 1.0); EmitVertex();" );
            lPassSource.AppendLine( "    EndPrimitive();" );
            lPassSource.AppendLine( "}" );
            lPassSource.AppendLine( "#endif" );
            lPassSource.AppendLine( "#ifdef _FRAGMENT_" );
            lPassSource.AppendLine( "in vec3 triDist;" );
            lPassSource.AppendLine( "layout(location = 0) out vec4 data;" );
            lPassSource.AppendLine( "void main() {" );
            lPassSource.AppendLine( "    float d = min(min(triDist.x, triDist.y), triDist.z);" );
            lPassSource.AppendLine( "    float w = fwidth(d) * 0.5;" );
            lPassSource.AppendLine( "    float t = smoothstep(0.02 - w, 0.02 + w, d);" );
            lPassSource.AppendLine( "    data = mix(vec4(1.0, 0.0, 0.0, 1.0), vec4(1.0), t);" );
            lPassSource.AppendLine( "}");
            lPassSource.AppendLine( "#endif" );

            this.mPass = new PipelinePass( new PipelineDescription( 400, lPassSource.ToString() ) );

            this.mLocalToScreen = this.mPass.GetUniform<Matrix4FUniform>( new UniformName( "localToScreen" ) );
            this.mInner = this.mPass.GetUniform<FloatUniform>( new UniformName( "inner" ) );
            this.mOuter = this.mPass.GetUniform<FloatUniform>( new UniformName( "outer" ) );
            this.mInner.Value = 5.0f;
            this.mOuter.Value = 5.0f;
        }
        
        /// <summary>
        /// Internal render method to override.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute time.</param>
        /// <param name="pDeltaTime">The elasped time since last render.</param>
        /// <returns>True if anything has been rendered, false otherwise.</returns>
        private bool OnRender(double pAbsoluteTime, double pDeltaTime)
        {
            this.FrameBuffer.Clear( true, false, true );

            AMatrix<float> lCameraToWorld = Matrix4F.RotateX( 90 );
            lCameraToWorld = lCameraToWorld * Matrix4F.RotateY(-this.mAlpha);
            lCameraToWorld = lCameraToWorld * Matrix4F.RotateX(-this.mTheta);
            lCameraToWorld = lCameraToWorld * Matrix4F.Translate( new Vector3F( 0.0f, 0.0f, this.mDistance ));

            bool lResult;
            AMatrix<float> lWorldToCamera = lCameraToWorld.Inverse( float.Epsilon, out lResult ) as AMatrix<float>;

            Viewport lViewport = this.FrameBuffer.Parameters[ new FrameBufferParameters.ViewportIndex( 0 ) ];
            float lWidth  = lViewport.Width;
            float lHeight = lViewport.Height;
            float lFov    = (float)MathUtility.ToDegrees( 2 * System.Math.Atan( lHeight / lWidth * System.Math.Tan( MathUtility.ToRadians( this.mFov / 2 ) ) ) );
            Matrix4F lCameraToScreen = Matrix4F.PerspectiveProjection( lFov, lWidth / lHeight, 0.1f, 1e5f );

            this.mLocalToScreen.Value = lCameraToScreen * lWorldToCamera * Matrix4F.RotateZ( 15.0f );
            this.FrameBuffer.Draw( this.mPass, this.mModel );

            return true;
        }

        /// <summary>
        /// Internal resize method to override.
        /// </summary>
        /// <param name="pWidth">The new width.</param>
        /// <param name="pHeight">The new height.</param>
        private void OnResize(int pWidth, int pHeight)
        {
            // Applies the new size to the viewport.
            this.FrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, pWidth, pHeight ) );
            
            // Reinit the depth test function.
            this.FrameBuffer.Parameters.DepthDescription.DepthTest( true, DepthFunction.Less );
        }

        /// <summary>
        /// Delegate called on mouse wheeled events.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnMouseWheeled(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.Delta > 0 ) // Wheel up?
            {
                this.mDistance *= 1.05f;
            }
            else
            {
                this.mDistance /= 1.05f;
            }
        }

        /// <summary>
        /// Delegate called on mouse moved events.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnMouseMoved(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.Button == MouseButtons.Left )
            {
                this.mAlpha = (float)(pEventArgs.X / this.mView.Width * 360.0);
                this.mTheta = (float)((pEventArgs.Y / this.mView.Height - 0.5) * 180.0);
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDisposed()
        {
            this.mView.Rendering    -= this.OnRender;
            this.mView.Resizing     -= this.OnResize;
            this.mView.MouseMoved   -= this.OnMouseMoved;
            this.mView.MouseWheeled -= this.OnMouseWheeled;
            
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.Dispose();
            }

            if ( this.mModel != null )
            {
                this.mModel.Dispose();
                this.mModel = null;
            }

            if ( this.mPass != null )
            {
                this.mPass.Dispose();
                this.mPass = null;
            }

            base.OnDisposed();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
