﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Math;
using OrcNet.Core.Render;
using OrcNet.Core.Resource;
using OrcNet.Core.Resource.Loaders;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Core.UI;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.SceneGraph;
using OrcNet.Graphics.Services;
using OrcNet.Windows.UI.View;
using System;
using System.IO;

namespace OrcNet.Windows.Samples.Pages
{
    /// <summary>
    /// Definition of the <see cref="SceneGraphSample"/> class.
    /// </summary>
    public class SceneGraphSample : ASample
    {
        #region Fields
        
        /// <summary>
        /// Stores the field of view in degrees.
        /// </summary>
        private float mFov;

        /// <summary>
        /// Stores the alpha camera rotation angle in degrees.
        /// </summary>
        private float mAlpha;

        /// <summary>
        /// Stores the theta camera rotation angle in degrees.
        /// </summary>
        private float mTheta;

        /// <summary>
        /// Stores the distance of the camera to the scene.
        /// </summary>
        private float mDistance;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneGraphSample"/> class.
        /// </summary>
        /// <param name="pView">The view sample content will be rendered to.</param>
        public SceneGraphSample(OrcView pView) :
        base( pView, new ViewParameters() { UseDepth = true }.SetSize( 1024, 768 ) )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the sample.
        /// </summary>
        internal override void Initialize()
        {
            base.Initialize();

            this.mView.Rendering    += this.OnRender;
            this.mView.Resizing     += this.OnResize;
            this.mView.MouseMoved   += this.OnMouseMoved;
            this.mView.MouseWheeled += this.OnMouseWheeled;

            this.mFov      = 80.0f;
            this.mAlpha    = 135.0f;
            this.mTheta    = 45.0f;
            this.mDistance = 15.0f;
            
            string lAssetDirectory    = Path.Combine( Environment.CurrentDirectory, @"..\Resources\Assets");
            string lTexturesDirectory = Path.Combine( lAssetDirectory, "Textures" );
            string lShadersDirectory  = Path.Combine( lAssetDirectory, "Shaders" );
            string lMeshesDirectory   = Path.Combine( lAssetDirectory, "Meshes" );
            string lMethodsDirectory  = Path.Combine( lAssetDirectory, "Methods" );
            string lScenesDirectory   = Path.Combine( lAssetDirectory, "Scenes" );

            XMLResourceLoader lLoader = new XMLResourceLoader();
            lLoader.AddPath( lTexturesDirectory );
            lLoader.AddPath( lShadersDirectory );
            lLoader.AddPath( lMeshesDirectory );
            lLoader.AddPath( lMethodsDirectory );
            lLoader.AddPath( lScenesDirectory );

            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            SceneManager lSceneService = ServiceManager.Instance.GetService<SceneManager>();
            lSceneService.Scheduler = new ParallelScheduler();

            // Set the frame buffer as scene frame buffer as it is that one that has been configurated for the scene.
            lSceneService.CurrentFrameBuffer = this.FrameBuffer;

            ISceneNode lRoot = new SceneNode();

            // Create the camera
            ISceneNode lCameraNode = new SceneNode();
            lCameraNode.AddFlag( "camera" );
            IResource lCameraResource = lResourceService.LoadResource( "camera" );
            if ( lCameraResource != null )
            {
                lCameraNode.AddPipelineDescription( "material", lCameraResource.OwnedObject as IPipelineDescription );
            }
            IResource lCameraMethodResource = lResourceService.LoadResource( "cameraMethod" );
            if ( lCameraMethodResource != null )
            {
                lCameraNode.AddMethod( "draw", new SceneNodeMethod( lCameraMethodResource.OwnedObject as ITaskFactory ) );
            }
            lRoot.AddChild( lCameraNode );

            // Create light.
            SceneNode lLightNode = new SceneNode();
            lLightNode.LocalToParentTransform = Matrix4F.Translate( new Vector3F( 3.0f, 3.0f, 3.0f ) ) * Matrix4F.RotateZ( 135.0f ) * Matrix4F.RotateX( 45.0f );
            lLightNode.AddFlag( "light" );
            IResource lLightResource = lResourceService.LoadResource( "spotlight" );
            if ( lLightResource != null )
            {
                lLightNode.AddPipelineDescription( "material", lLightResource.OwnedObject as IPipelineDescription );
            }
            IResource lLightMethodResource = lResourceService.LoadResource( "lightMethod" );
            if ( lLightMethodResource != null )
            {
                lLightNode.AddMethod( "draw", new SceneNodeMethod( lLightMethodResource.OwnedObject as ITaskFactory ) );
            }
            lRoot.AddChild( lLightNode );

            SceneNode lCubeNode = new SceneNode();
            lCubeNode.LocalToParentTransform = Matrix4F.RotateZ( 15.0f );
            lCubeNode.AddFlag( "object" );
            IResource lCubeResource = lResourceService.LoadResource( "cube.mesh" );
            if ( lCubeResource != null )
            {
                lCubeNode.AddMesh( "geometry", lCubeResource.OwnedObject as MeshBuffers );
            }
            IResource lCubeShaderResource = lResourceService.LoadResource( "plastic" );
            if ( lCubeShaderResource != null )
            {
                lCubeNode.AddPipelineDescription( "material", lCubeShaderResource.OwnedObject as IPipelineDescription );
            }
            IResource lCubeMethodResource = lResourceService.LoadResource( "objectMethod" );
            if ( lCubeMethodResource != null )
            {
                lCubeNode.AddMethod( "draw", new SceneNodeMethod( lCubeMethodResource.OwnedObject as ITaskFactory ) );
            }
            lRoot.AddChild( lCubeNode );

            SceneNode lPlaneNode = new SceneNode();
            lPlaneNode.LocalToParentTransform = Matrix4F.Translate( new Vector3F( 0, 0, -2 ) ) * Matrix4F.RotateZ( 180.0f );
            lPlaneNode.AddFlag( "object" );
            IResource lPlaneResource = lResourceService.LoadResource( "plane.mesh" );
            if ( lPlaneResource != null )
            {
                lPlaneNode.AddMesh( "geometry", lPlaneResource.OwnedObject as MeshBuffers );
            }
            IResource lPlaneShaderResource = lResourceService.LoadResource( "texturedPlastic" );
            if ( lPlaneShaderResource != null )
            {
                lPlaneNode.AddPipelineDescription( "material", lPlaneShaderResource.OwnedObject as IPipelineDescription );
            }
            IResource lPlaneMethodResource = lResourceService.LoadResource( "objectMethod" );
            if ( lPlaneMethodResource != null )
            {
                lPlaneNode.AddMethod( "draw", new SceneNodeMethod( lPlaneMethodResource.OwnedObject as ITaskFactory ) );
            }
            lRoot.AddChild( lPlaneNode );

            ISceneNode lLogNode = new SceneNode();
            lLogNode.AddFlag( "overlay" );
            IResource lLogMethodResource = lResourceService.LoadResource( "logMethod" );
            if ( lLogMethodResource != null )
            {
                lLogNode.AddMethod( "draw", new SceneNodeMethod( lLogMethodResource.OwnedObject as ITaskFactory ) );
            }
            lRoot.AddChild( lLogNode );

            ISceneNode lInfoNode = new SceneNode();
            lInfoNode.AddFlag( "overlay" );
            IResource lInfoMethodResource = lResourceService.LoadResource( "infoMethod" );
            if ( lInfoMethodResource != null )
            {
                lInfoNode.AddMethod( "draw", new SceneNodeMethod( lInfoMethodResource.OwnedObject as ITaskFactory ) );
            }
            lRoot.AddChild( lInfoNode );

            lSceneService.Root = lRoot;
            lSceneService.SetNodeAsCamera( "camera" );
            lSceneService.CameraMethod = "draw";

            // Applies the new size to the viewport.
            this.FrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, this.mParameters.Width, this.mParameters.Height ) );
            
            // Reinit the depth test function.
            this.FrameBuffer.Parameters.DepthDescription.DepthTest( true, DepthFunction.Less );
                
            float lFov   = (float)MathUtility.ToDegrees( 2 * System.Math.Atan( (float)this.mParameters.Height / this.mParameters.Width * System.Math.Tan( MathUtility.ToRadians( this.mFov / 2 ) ) ) );
            lSceneService.CameraToScreenTransform = Matrix4F.PerspectiveProjection( lFov, (float)this.mParameters.Width / this.mParameters.Height, 0.1f, 1e5f );
        }
        
        /// <summary>
        /// Internal render method to override.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute time.</param>
        /// <param name="pDeltaTime">The elasped time since last render.</param>
        /// <returns>True if anything has been rendered, false otherwise.</returns>
        private bool OnRender(double pAbsoluteTime, double pDeltaTime)
        {
            // Update resources if changed.
            //IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            //lResourceService.UpdateResources();
            
            ISceneService lSceneService = ServiceManager.Instance.GetService<ISceneService>();

            AMatrix<float> lCameraToWorld = Matrix4F.RotateX( 90 );
            lCameraToWorld = lCameraToWorld * Matrix4F.RotateY(-this.mAlpha);
            lCameraToWorld = lCameraToWorld * Matrix4F.RotateX(-this.mTheta);
            lCameraToWorld = lCameraToWorld * Matrix4F.Translate( new Vector3F( 0.0f, 0.0f, this.mDistance ));

            SceneNode lCameraNode = lSceneService.Camera as SceneNode;
            lCameraNode.LocalToParentTransform = lCameraToWorld;
            
            this.FrameBuffer.Clear( true, false, true );

            lSceneService.Update( pAbsoluteTime, pDeltaTime );
            lSceneService.Render();

            return true;
        }

        /// <summary>
        /// Internal resize method to override.
        /// </summary>
        /// <param name="pWidth">The new width.</param>
        /// <param name="pHeight">The new height.</param>
        private void OnResize(int pWidth, int pHeight)
        {
            // Applies the new size to the viewport.
            this.FrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, pWidth, pHeight ) );
            
            // Reinit the depth test function.
            this.FrameBuffer.Parameters.DepthDescription.DepthTest( true, DepthFunction.Less );
                
            float lFov   = (float)MathUtility.ToDegrees( 2 * System.Math.Atan( (float)pHeight / pWidth * System.Math.Tan( MathUtility.ToRadians( this.mFov / 2 ) ) ) );

            SceneManager lSceneService = ServiceManager.Instance.GetService<SceneManager>();
            lSceneService.CameraToScreenTransform = Matrix4F.PerspectiveProjection( lFov, (float)pWidth / pHeight, 0.1f, 1e5f );
        }

        /// <summary>
        /// Delegate called on mouse wheeled events.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnMouseWheeled(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.Delta > 0 ) // Wheel up?
            {
                this.mDistance *= 1.05f;
            }
            else
            {
                this.mDistance /= 1.05f;
            }
        }

        /// <summary>
        /// Delegate called on mouse moved events.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnMouseMoved(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.Button == MouseButtons.Left )
            {
                this.mAlpha = (float)(pEventArgs.X / this.mView.Width * 360.0);
                this.mTheta = (float)((pEventArgs.Y / this.mView.Height - 0.5) * 180.0);
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDisposed()
        {
            this.mView.Rendering    -= this.OnRender;
            this.mView.Resizing     -= this.OnResize;
            this.mView.MouseMoved   -= this.OnMouseMoved;
            this.mView.MouseWheeled -= this.OnMouseWheeled;
            
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.Dispose();
            }

            ISceneService lSceneService = ServiceManager.Instance.GetService<ISceneService>();
            lSceneService.Dispose();

            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.Dispose();

            base.OnDisposed();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
