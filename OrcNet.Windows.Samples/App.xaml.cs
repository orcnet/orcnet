﻿using System;
using System.Threading;
using System.Windows;

namespace OrcNet.Windows.Samples
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            //Thread.CurrentThread.SetApartmentState( ApartmentState.STA );
        }

        #endregion Constructor
    }
}
