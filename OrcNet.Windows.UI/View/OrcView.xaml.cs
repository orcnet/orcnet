﻿using OpenTK;
using OpenTK.Graphics;
using OrcNet.Core.Timer;
using OrcNet.Windows.UI.Utilities;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

namespace OrcNet.Windows.UI.View
{
    /// <summary>
    /// Idle delegate prototype definition.
    /// </summary>
    public delegate void RestingDelegate();

    /// <summary>
    /// Mouse actions delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender</param>
    /// <param name="pEventArgs">The event arguments</param>
    public delegate void MouseDelegate(object pSender, OrcNet.Core.UI.MouseEventArgs pEventArgs);

    /// <summary>
    /// Keyboard actions delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender</param>
    /// <param name="pEventArgs">The event arguments</param>
    public delegate void KeyboardDelegate(object pSender, OrcNet.Core.UI.KeyEventArgs pEventArgs);

    /// <summary>
    /// Render delegate prototype definition.
    /// </summary>
    /// <param name="pAbsoluteTime">The absolute time.</param>
    /// <param name="pDeltaTime">The elasped time since last render.</param>
    /// <returns></returns>
    public delegate bool RenderDelegate(double pAbsoluteTime, double pDeltaTime);

    /// <summary>
    /// Resize delegate prototype definition.
    /// </summary>
    /// <param name="pWidth">The new width.</param>
    /// <param name="pHeight">The new height.</param>
    public delegate void ResizeDelegate(int pWidth, int pHeight);

    /// <summary>
    /// Definition of the <see cref="OrcView"/>
    /// </summary>
    public partial class OrcView : System.Windows.Controls.UserControl, Core.UI.IView
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the rendering thread must be interrupted or not.
        /// </summary>
        private bool mInterrupt;

        /// <summary>
        /// Stores the rendering frequency.
        /// </summary>
        private double mRenderFrequency;

        /// <summary>
        /// Stores the time between 2 rendering (in ms)
        /// Its value (1 / mRenderFrequency) is kept for performance reasons
        /// </summary>
        private int mRenderingPeriod;

        /// <summary>
        /// The thread used to render the viewer at a given frequency
        /// </summary>
        private Thread mRenderThread;

        /// <summary>
        /// Enables/Disables statistics computation
        /// </summary>
        private bool mEnableStats = true;

        /// <summary>
        /// The rendering start time for FPS counting
        /// </summary>
        private int mStartTime = Environment.TickCount;

        /// <summary>
        /// The number of frame in seconds
        /// </summary>
        private int mNumberFrames = 0;

        /// <summary>
        /// Stores an estimation of the frames rendered per second. (Only meaningful if the mTimer is enabled)
        /// </summary>
        private double mFramesPerSecond;

        /// <summary>
        /// Stores the absolute time at the end of the last Render call.
        /// </summary>
        private double mAbsoluteTime;

        /// <summary>
        /// Stores the elapsed time between two Render calls.
        /// </summary>
        private double mDeltaTime;

        /// <summary>
        /// Stores the window timer used in Render.
        /// </summary>
        private BaseTimer mTimer;

        /// <summary>
        /// Stores the flag indicating whether the window has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the OpenTK control view.
        /// </summary>
        private GLControl mControlView;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event triggered on view resting to allow background extra process in idle mode.
        /// </summary>
        public event RestingDelegate Resting;

        /// <summary>
        /// Event triggered on mouse pressed to allow to modify the scene on user mouse action.
        /// </summary>

        public event MouseDelegate MousePressed;

        /// <summary>
        /// Event triggered on mouse released to allow to modify the scene on user mouse action.
        /// </summary>

        public event MouseDelegate MouseReleased;

        /// <summary>
        /// Event triggered on mouse moved to allow to modify the scene on user mouse action.
        /// </summary>

        public event MouseDelegate MouseMoved;

        /// <summary>
        /// Event triggered on mouse wheeled to allow to modify the scene on user mouse action.
        /// </summary>

        public event MouseDelegate MouseWheeled;

        /// <summary>
        /// Event triggered on key pressed to allow to modify the scene on user keyboard action.
        /// </summary>
        public event KeyboardDelegate KeyPressed;

        /// <summary>
        /// Event triggered on key released to allow to modify the scene on user keyboard action.
        /// </summary>
        public event KeyboardDelegate KeyReleased;

        /// <summary>
        /// Event triggered on view rendering to allow to pass what has to be rendered.
        /// </summary>
        public event RenderDelegate Rendering;

        /// <summary>
        /// Event triggered on view resizing to allow to make extra update for size dependent objects.
        /// </summary>
        public event ResizeDelegate Resizing;

        #endregion Events

        #region Properties

        #region Properties IView

        /// <summary>
        /// Gets the view width.
        /// </summary>
        int Core.UI.IResizable.Width
        {
            get
            {
                return this.mControlView.Width;
            }
        }

        /// <summary>
        /// Gets the view height.
        /// </summary>
        int Core.UI.IResizable.Height
        {
            get
            {
                return this.mControlView.Height;
            }
        }

        #endregion Properties IView

        /// <summary>
        /// Gets or sets the rendering frequency in Hz.
        /// </summary>
        public double RenderFrequency
        {
            get
            {
                return this.mRenderFrequency;
            }
            set
            {
                if ( value > 0 )
                {
                    this.mRenderFrequency = value;
                    this.mRenderingPeriod = (int)(1000.0 / value);
                }
            }
        }

        /// <summary>
        /// Gets an estimation of the frames rendered per second. (Only meaningful if the mTimer is enabled)
        /// </summary>
        public double FramesPerSecond
        {
            get
            {
                return this.mFramesPerSecond;
            }
        }

        /// <summary>
        /// Gets the graphical OpenTK context.
        /// </summary>
        public IGraphicsContext Context
        {
            get
            {
                return this.mControlView.Context;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="OrcView"/> class.
        /// </summary>
        public OrcView()
        {
            this.mIsDisposed = false;
            this.RenderFrequency = 20; // 20Hz by default.

            this.InitializeComponent();
            WindowsFormsHost.EnableWindowsFormsInterop();

            // Default.
            //this.InitializeWindow( new WindowParameters() );
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Start threaded rendering at RenderingFrequency
        /// </summary>
        private void StartThreadedRendering()
        {
            this.mInterrupt = false;

            if ( this.mRenderThread == null )
            {
                // Lauch render thread (faster than DispatchTimer)
                this.mRenderThread = new Thread( this.RenderThread );
                this.mRenderThread.Name = "RenderThread of " + this.Name;
                this.mRenderThread.Start();
            }
        }

        /// <summary>
        /// The threaded method rendering the scene at a given frequency
        /// </summary>
        private void RenderThread()
        {
            while ( this.mInterrupt == false )
            {
                // Dispatches a rendering request by invalidating the UserControl
                System.Windows.Application lApplication = System.Windows.Application.Current;
                if ( lApplication != null )
                {
                    lApplication.Dispatcher.Invoke( (Action)delegate
                    {
                        // Render scene
                        this.mControlView.Invalidate();
                    });
                }

                // Wait until it's time to render a new frame
                Thread.Sleep( this.mRenderingPeriod );
            }
        }

        /// <summary>
        /// Internal render method to override.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute time.</param>
        /// <param name="pDeltaTime">The elasped time since last render.</param>
        /// <returns>True if anything has been rendered, false otherwise.</returns>
        private void InternalRender(double pAbsoluteTime, double pDeltaTime)
        {
            if ( this.Rendering != null )
            {
                this.Rendering( pAbsoluteTime, pDeltaTime );
            }
        }

        /// <summary>
        /// Internal resize method to override.
        /// </summary>
        /// <param name="pWidth">The new width.</param>
        /// <param name="pHeight">The new height.</param>
        private void InternalResize(int pWidth, int pHeight)
        {
            if ( this.Resizing != null )
            {
                this.Resizing( pWidth, pHeight );
            }
        }

        /// <summary>
        /// Updates the window's frame rate.
        /// </summary>
        private void UpdateFrameRate()
        {
            this.mNumberFrames++;
            if ( Environment.TickCount - this.mStartTime > 1000 )
            {
                this.mFramesPerSecond = this.mNumberFrames / ((Environment.TickCount - this.mStartTime) / 1000f);
                this.mStartTime = Environment.TickCount;
                this.mNumberFrames = 0;
            }
        }

        /// <summary>
        /// Clean the orc view.
        /// </summary>
        private void Clean()
        {
            this.mInterrupt = true;
            this.mRenderThread = null;

            if ( this.mControlView != null )
            {
                this.mControlView.Resize     -= this.OnControlViewResize;
                this.mControlView.Paint      -= this.OnControlViewPaint;
                this.mControlView.Disposed   -= this.OnControlViewDisposed;
                this.mControlView.MouseDown  -= this.OnMouseDown;
                this.mControlView.MouseMove  -= this.OnMouseMove;
                this.mControlView.MouseUp    -= this.OnMouseUp;
                this.mControlView.MouseWheel -= this.OnMouseWheel;
                this.mControlView.KeyDown    -= this.OnKeyDown;
                this.mControlView.KeyUp      -= this.OnKeyUp;
                this.mControlView.Width  = 1;
                this.mControlView.Height = 1;
                this.mControlView = null;
            }
        }

        #endregion Methods Internal

        #region Methods IView

        /// <summary>
        /// Initializes the window.
        /// </summary>
        /// <param name="pParameters">The window parameters.</param>
        public void InitializeView(Core.UI.ViewParameters pParameters)
        {
            if ( this.mControlView != null )
            {
                this.Clean();
            }

            // Allow the previous rendering thread to stop.
            Thread.Sleep( this.mRenderingPeriod );

            // create and wrap winforms control
            if ( DesignerProperties.GetIsInDesignMode( this ) == false )
            {
                this.RenderFrequency = pParameters.Frequency;
                this.mAbsoluteTime   = 0.0;
                this.mDeltaTime      = 0.0;
                this.mTimer = new BaseTimer();
                this.mTimer.Start();

                GraphicsMode lGraphicMode = GraphicsMode.Default;
                GraphicsMode lOrcGraphicMode = new GraphicsMode( new ColorFormat( lGraphicMode.ColorFormat.Red, 
                                                                                  lGraphicMode.ColorFormat.Green, 
                                                                                  lGraphicMode.ColorFormat.Blue, 
                                                                                  pParameters.UseAlpha ? lGraphicMode.ColorFormat.Alpha : 0 ),
                                                                 pParameters.UseDepth ? lGraphicMode.Depth : 0,
                                                                 pParameters.UseStencil ? lGraphicMode.Stencil : 0,
                                                                 pParameters.UseMultiSampling ? 4 : 0, // anti-alias
                                                                 lGraphicMode.AccumulatorFormat,
                                                                 lGraphicMode.Buffers,
                                                                 lGraphicMode.Stereo );
                this.mControlView = new GLControl( lOrcGraphicMode );
                this.mControlView.VSync = false;
                this.mControlView.AllowDrop = true;

                this.mWFHOpenGLControl.Child = this.mControlView;

                // Listen the GLControl input events.
                this.mControlView.Resize     += this.OnControlViewResize;
                this.mControlView.Paint      += this.OnControlViewPaint;
                this.mControlView.Disposed   += this.OnControlViewDisposed;
                this.mControlView.MouseDown  += this.OnMouseDown;
                this.mControlView.MouseMove  += this.OnMouseMove;
                this.mControlView.MouseUp    += this.OnMouseUp;
                this.mControlView.MouseWheel += this.OnMouseWheel;
                this.mControlView.KeyDown    += this.OnKeyDown;
                this.mControlView.KeyUp      += this.OnKeyUp;
                
                // Resize the GLControl.
                //this.Resize( pParameters.Width, pParameters.Height );

                // Launch the rendering thread.
                this.StartThreadedRendering();

                // Resize the UserControl which will resize the GLControl.
                this.Width  = pParameters.Width;
                this.Height = pParameters.Height;
            }
        }

        /// <summary>
        /// Render the window.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute time.</param>
        /// <param name="pDeltaTime">The elasped time since last render.</param>
        public void Render(double pAbsoluteTime, double pDeltaTime)
        {
            if ( this.IsVisible )
            {
                // Use this context
                this.mControlView.MakeCurrent();

                // Render Scene in the window
                this.InternalRender( pAbsoluteTime, pDeltaTime );

                // Update statistics if requested.
                if ( this.mEnableStats )
                {
                    this.UpdateFrameRate();
                }

                // Swap buffers to display the computed image
                this.mControlView.SwapBuffers();

                // Update 
                double lNewTime = this.mTimer.End();
                this.mDeltaTime = lNewTime - this.mAbsoluteTime;
                this.mAbsoluteTime = lNewTime;
            }
        }

        /// <summary>
        /// Resize the window
        /// </summary>
        /// <param name="pWidth">The window width.</param>
        /// <param name="pHeight">The window height.</param>
        public void Resize(int pWidth, int pHeight)
        {
            if ( pWidth == 0 )
            {
                pWidth = 1;
            }

            if ( pHeight == 0 )
            {
                pHeight = 1;
            }

            this.mControlView.ClientSize = new System.Drawing.Size( pWidth, pHeight );

            this.mControlView.MakeCurrent();

            this.InternalResize( pWidth, pHeight );
        }

        #endregion Methods IView

        #region Methods Events

        #region Methods Events Internal

        /// <summary>
        /// Mouse down event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        private void OnMouseDown(object pSender, MouseEventArgs pEventArgs)
        {
            this.OnMouseDown( pSender, 
                              new OrcNet.Core.UI.MouseEventArgs( WinToOrcUtilities.ToOrc( pEventArgs.Button ), pEventArgs.Clicks, pEventArgs.X, pEventArgs.Y, pEventArgs.Delta ) );
        }

        /// <summary>
        /// Mouse up event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        private void OnMouseUp(object pSender, MouseEventArgs pEventArgs)
        {
            this.OnMouseUp( pSender, 
                            new OrcNet.Core.UI.MouseEventArgs( WinToOrcUtilities.ToOrc( pEventArgs.Button ), pEventArgs.Clicks, pEventArgs.X, pEventArgs.Y, pEventArgs.Delta ) );
        }

        /// <summary>
        /// Mouse move event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        private void OnMouseMove(object pSender, MouseEventArgs pEventArgs)
        {
            this.OnMouseMove( pSender, 
                              new OrcNet.Core.UI.MouseEventArgs( WinToOrcUtilities.ToOrc( pEventArgs.Button ), pEventArgs.Clicks, pEventArgs.X, pEventArgs.Y, pEventArgs.Delta ) );
        }

        /// <summary>
        /// Mouse wheel event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        private void OnMouseWheel(object pSender, MouseEventArgs pEventArgs)
        {
            this.OnMouseWheel( pSender, 
                               new OrcNet.Core.UI.MouseEventArgs( WinToOrcUtilities.ToOrc( pEventArgs.Button ), pEventArgs.Clicks, pEventArgs.X, pEventArgs.Y, pEventArgs.Delta ) );
        }

        /// <summary>
        /// Key down event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        private void OnKeyDown(object pSender, KeyEventArgs pEventArgs)
        {
            byte lData = 0;
            if ( pEventArgs.KeyValue > 0 &&
                 pEventArgs.KeyValue < 256 )
            {
                lData = (byte)pEventArgs.KeyValue;
            }

            this.OnKeyDown( pSender, 
                            new OrcNet.Core.UI.KeyEventArgs( lData, WinToOrcUtilities.ToOrc( pEventArgs.Modifiers ) ) );
        }

        /// <summary>
        /// Key up event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        private void OnKeyUp(object pSender, KeyEventArgs pEventArgs)
        {
            byte lData = 0;
            if ( pEventArgs.KeyValue > 0 &&
                 pEventArgs.KeyValue < 256 )
            {
                lData = (byte)pEventArgs.KeyValue;
            }

            this.OnKeyUp( pSender, 
                          new OrcNet.Core.UI.KeyEventArgs( lData, WinToOrcUtilities.ToOrc( pEventArgs.Modifiers ) ) );
        }

        #endregion Methods Events Internal

        #region Methods Events IInputHandler

        /// <summary>
        /// Delegate called on idle time.
        /// </summary>
        public void OnIdle()
        {
            if ( this.Resting != null )
            {
                this.Resting();
            }
        }

        /// <summary>
        /// Mouse down event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        public void OnMouseDown(object pSender, OrcNet.Core.UI.MouseEventArgs pEventArgs)
        {
            if ( this.MousePressed != null )
            {
                this.MousePressed( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Mouse up event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        public void OnMouseUp(object pSender, OrcNet.Core.UI.MouseEventArgs pEventArgs)
        {
            if ( this.MouseReleased != null )
            {
                this.MouseReleased( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Mouse move event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        public void OnMouseMove(object pSender, OrcNet.Core.UI.MouseEventArgs pEventArgs)
        {
            if ( this.MouseMoved != null )
            {
                this.MouseMoved( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Mouse wheel event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        public void OnMouseWheel(object pSender, OrcNet.Core.UI.MouseEventArgs pEventArgs)
        {
            if ( this.MouseWheeled != null )
            {
                this.MouseWheeled( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Key down event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        public void OnKeyDown(object pSender, OrcNet.Core.UI.KeyEventArgs pEventArgs)
        {
            if ( this.KeyPressed != null )
            {
                this.KeyPressed( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Key up event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        public void OnKeyUp(object pSender, OrcNet.Core.UI.KeyEventArgs pEventArgs)
        {
            if ( this.KeyReleased != null )
            {
                this.KeyReleased( pSender, pEventArgs );
            }
        }

        #endregion Methods Events IInputHandler

        #region Methods Events GLControl

        /// <summary>
        /// Reset the view on GLContol size changed.
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnControlViewResize(object pSender, EventArgs pEventArgs)
        {
            if ( this.mControlView != null )
            {
                int lWidth  = this.mControlView.ClientSize.Width;
                int lHeight = this.mControlView.ClientSize.Height;

                this.Resize( lWidth, lHeight );
                this.Render( this.mAbsoluteTime, this.mDeltaTime );
            }
        }

        /// <summary>
        /// Render the content on GLControl paint events.
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnControlViewPaint(object pSender, EventArgs pEventArgs)
        {
            this.Render( this.mAbsoluteTime, this.mDeltaTime );
        }

        /// <summary>
        /// Delegate called on GLControl view disposed.
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnControlViewDisposed(object pSender, EventArgs pEventArgs)
        {
            this.Dispose();
        }

        #endregion Methods Events GLControl

        #endregion Methods Events

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.Clean();

                this.OnDispose();

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {

        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
