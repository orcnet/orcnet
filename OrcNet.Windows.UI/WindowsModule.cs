﻿using OrcNet.Core;

namespace OrcNet.Windows.UI
{
    /// <summary>
    /// Definition of the <see cref="WindowsModule"/> class.
    /// </summary>
    internal sealed class WindowsModule : AModule
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsModule"/> class.
        /// </summary>
        public WindowsModule()
        {

        }

        #endregion Constructor
    }
}
