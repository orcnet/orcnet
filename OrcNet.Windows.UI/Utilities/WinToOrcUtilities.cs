﻿namespace OrcNet.Windows.UI.Utilities
{
    /// <summary>
    /// Definition of the <see cref="WinToOrcUtilities"/> class.
    /// </summary>
    public static class WinToOrcUtilities
    {
        #region Methods

        /// <summary>
        /// Turns a windows forms mouse button state into an Orc mouse state.
        /// </summary>
        /// <param name="pButtons">The windows mouse state.</param>
        /// <returns>The Orc mouse state.</returns>
        public static OrcNet.Core.UI.MouseButtons ToOrc(System.Windows.Forms.MouseButtons pButtons)
        {
            switch ( pButtons )
            {
                case System.Windows.Forms.MouseButtons.Left:
                    return Core.UI.MouseButtons.Left;
                case System.Windows.Forms.MouseButtons.None:
                    return Core.UI.MouseButtons.None;
                case System.Windows.Forms.MouseButtons.Right:
                    return Core.UI.MouseButtons.Right;
                case System.Windows.Forms.MouseButtons.Middle:
                    return Core.UI.MouseButtons.Middle;
                case System.Windows.Forms.MouseButtons.XButton1:
                    return Core.UI.MouseButtons.XButton1;
                case System.Windows.Forms.MouseButtons.XButton2:
                    return Core.UI.MouseButtons.XButton2;
                default:
                    return Core.UI.MouseButtons.None;
            }
        }

        /// <summary>
        /// Turns a windows forms key modifiers state into an Orc key modifier state.
        /// </summary>
        /// <param name="pModifiers">The windows key modifiers.</param>
        /// <returns>The Orc key modifiers.</returns>
        public static OrcNet.Core.UI.KeyModifiers ToOrc(System.Windows.Forms.Keys pModifiers)
        {
            OrcNet.Core.UI.KeyModifiers lModifiers = Core.UI.KeyModifiers.None;
            if ( (pModifiers & System.Windows.Forms.Keys.Alt) == System.Windows.Forms.Keys.Alt )
            {
                lModifiers |= Core.UI.KeyModifiers.Alt;
            }

            if ( (pModifiers & System.Windows.Forms.Keys.Shift) == System.Windows.Forms.Keys.Shift )
            {
                lModifiers |= Core.UI.KeyModifiers.Shift;
            }

            if ( (pModifiers & System.Windows.Forms.Keys.Control) == System.Windows.Forms.Keys.Control )
            {
                lModifiers |= Core.UI.KeyModifiers.Ctrl;
            }

            return lModifiers;
        }

        #endregion Methods
    }
}
