﻿namespace OrcNet.Diagnostics.Memory
{
    /// <summary>
    /// Memory statistics class definition.
    /// </summary>
    public class MemoryStatistics
    {
        #region Properties

        /// <summary>
        /// Gets or sets the sum of objects size of a type.
        /// </summary>
        public double SizeSum
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sum of the squares of objects size of a type.
        /// </summary>
        public double SizeSqrSum
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the amount of instanciated object of a type.
        /// </summary>
        public int Count
        {
            get;
            set;
        }

        #endregion Properties
    }
}
