﻿using OrcNet.Core;
using OrcNet.Core.Profiling;

namespace OrcNet.Diagnostics.Memory
{
    /// <summary>
    /// Memory profiler class definition.
    /// </summary>
    public sealed class MemoryProfiler : AProfiler, IMemoryProfiler
    {
        #region Fields
        
        /// <summary>
        /// Stores the sync root to lock on the profiler rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        #endregion Fields

        #region Properties
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="MemoryProfiler"/> class.
        /// </summary>
        static MemoryProfiler()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryProfiler"/> class.
        /// </summary>
        public MemoryProfiler()
        {
            this.mName = "MemoryProfiler";
        }

        #endregion Constructor

        #region Methods

        #region Methods IDisposable

        /// <summary>
        /// Delegate called on object disposing.
        /// </summary>
        protected override void OnDispose()
        {
            // Add task profiler spe stuff.

            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
