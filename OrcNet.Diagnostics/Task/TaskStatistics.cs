﻿namespace OrcNet.Diagnostics.Task
{
    /// <summary>
    /// Task statistics class definition.
    /// </summary>
    public class TaskStatistics
    {
        #region Properties

        /// <summary>
        /// Gets or sets the amount of task executed.
        /// </summary>
        public int    TaskCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sum of task execution times as a whole duration.
        /// </summary>
        public double DurationSum
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sum of the squares of the task execution times as a whole duration.
        /// </summary>
        public double DurationSqrSum
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the minimum task execution time.
        /// </summary>
        public double MinDuration
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the maximum task execution time.
        /// </summary>
        public double MaxDuration
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskStatistics"/> class.
        /// </summary>
        public TaskStatistics()
        {
            this.DurationSum    = 0.0;
            this.DurationSqrSum = 0.0;
            this.MinDuration = double.PositiveInfinity;
            this.MaxDuration = 0.0;
            this.TaskCount   = 0;
        }

        #endregion Constructor
    }
}
