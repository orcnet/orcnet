﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using OrcNet.Core.Profiling;
using OrcNet.Core.Task;
using System;
using System.Collections.Generic;
using System.IO;

namespace OrcNet.Diagnostics.Task
{
    /// <summary>
    /// Task profiler class definition.
    /// </summary>
    public sealed class TaskProfiler : AProfiler, ITaskProfiler
    {
        #region Fields
        
        /// <summary>
        /// Stores the sync root to lock on the profiler rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the set of execution time statistics by task type.
        /// </summary>
        private static Dictionary<Type, TaskStatistics> sStatistics;

        /// <summary>
        /// Stores the set of task classes whose execution time must be monitored.
        /// </summary>
        private List<Type>                            mMonitoredTasks;

        /// <summary>
        /// Stores the statistics per frame for monitored task classes and map for a specific
        /// task type its amount and the overall execution time.
        /// </summary>
        private Dictionary<Type, Tuple<int, double>>  mFrameStatistics;

        /// <summary>
        /// Stores the save file info where statistics will be written to.
        /// </summary>
        private FileInfo                              mSaveFile;

        /// <summary>
        /// Stores the buffered array of frame statistics to avoid writing to disk each frame.
        /// </summary>
        private double[]                              mBufferedStatistics;

        /// <summary>
        /// Stores the number of buffered frame statistics.
        /// see mBufferedStatistics.
        /// </summary>
        private int                                   mBufferedStatisticCount;

        #endregion Fields

        #region Properties
        
        /// <summary>
        /// Gets the flag indicating whether the profiler is actually
        /// profiling some task type(s)
        /// </summary>
        public bool IsProfiling
        {
            get
            {
                return this.mMonitoredTasks.Count > 0;
            }
        }

        /// <summary>
        /// Gets the set of task whose execution time must be monitored.
        /// </summary>
        public IEnumerable<Type> MonitoredTasks
        {
            get
            {
                return this.mMonitoredTasks;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="TaskProfiler"/> class.
        /// </summary>
        static TaskProfiler()
        {
            sStatistics = new Dictionary<Type, TaskStatistics>();
            IEnumerable<Type> lTaskTypes = typeof(ITask).GetInheritedTypes();
            foreach
                ( Type lTaskType in lTaskTypes )
            {
                // Create one statistic object for each task type.
                sStatistics.Add( lTaskType, new TaskStatistics() );
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskProfiler"/> class.
        /// </summary>
        public TaskProfiler()
        {
            this.mName = "TaskProfiler";
            this.mBufferedStatisticCount = 0;
            this.mBufferedStatistics = null;
            this.mMonitoredTasks  = new List<Type>();
            this.mFrameStatistics = new Dictionary<Type, Tuple<int, double>>();
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Adds a new task type to profile.
        /// </summary>
        /// <param name="pToProfile">The task type to profile</param>
        public void ProfileTask(Type pToProfile)
        {
            lock
                ( sSyncRoot )
            { 
                this.mMonitoredTasks.Add( pToProfile );
            }
        }

        /// <summary>
        /// Resets the profiler statistics
        /// </summary>
        public void ResetStatistics()
        {
            lock
                ( sSyncRoot )
            { 
                if
                    ( this.IsProfiling )
                {
                    int lMonitoredCount = this.mMonitoredTasks.Count;
                    this.mFrameStatistics.Clear();
                    foreach
                        ( Type lMonitoredType in this.mMonitoredTasks )
                    {
                        this.mFrameStatistics.Add( lMonitoredType, new Tuple<int, double>( 0, 0.0 ) );
                    }

                    if
                        ( this.mBufferedStatistics == null )
                    {
                        this.mBufferedStatistics = new double[ (2 * lMonitoredCount + 2) * 1000 ];
                    }
                }
            }
        }

        /// <summary>
        /// Updates the profiler global statistics by task type.
        /// </summary>
        /// <param name="pTask">The new task completed</param>
        /// <param name="pNewDuration">The new task duration to take into account.</param>
        public void UpdateGlobalStatistics(ITask pTask, double pNewDuration)
        {
            Type lTaskType = pTask.GetType();
            lock
                ( sSyncRoot )
            {
                TaskStatistics lStatistics = null;
                if
                    ( sStatistics.TryGetValue( lTaskType, out lStatistics ) == false )
                {
                    lStatistics = new TaskStatistics();
                }

                double lDuration = pNewDuration / pTask.Complexity;
                lStatistics.DurationSum += lDuration;
                lStatistics.DurationSqrSum += lDuration * lDuration;
                lStatistics.MinDuration = Math.Min( lDuration, lStatistics.MinDuration );
                lStatistics.MaxDuration = Math.Max( lDuration, lStatistics.MaxDuration );
                lStatistics.TaskCount++;
            }
        }

        /// <summary>
        /// Updates the profiler per frame statistics by task type.
        /// </summary>
        /// <param name="pTask">The new task completed</param>
        /// <param name="pNewDuration">The new task duration to take into account.</param>
        public void UpdateFrameStatistics(ITask pTask, double pNewDuration)
        {
            Type lTaskType = pTask.GetType();
            lock
                ( sSyncRoot )
            {
                Tuple<int, double> lTaskFrameStatistics = null;
                if
                    ( this.mFrameStatistics.TryGetValue( lTaskType, out lTaskFrameStatistics ) )
                {
                    Tuple<int, double> lNewValues = new Tuple<int, double>( lTaskFrameStatistics.Item1 + 1, lTaskFrameStatistics.Item2 + pNewDuration );
                    this.mFrameStatistics[ lTaskType ] = lNewValues; // Refresh.
                }
            }
        }

        /// <summary>
        /// Prefetches some statistic info. If prefetched enough
        /// previous will be written to file and prefetch new info.
        /// </summary>
        /// <param name="pScheduleDuration">The duration task schedulation took.</param>
        /// <param name="pTotalDuration">The overall thread work duration</param>
        public void PrefetchStatistics(double pScheduleDuration, double pTotalDuration)
        {
            lock
                ( sSyncRoot )
            {
                // If too much prefetched statistics info, write them into 
                // the file.
                if
                    ( this.mBufferedStatisticCount == 1000.0 )
                {
                    this.ClearBufferedFrames();
                }

                int lIndex = this.mBufferedStatisticCount * (2 * this.mMonitoredTasks.Count + 2);
                this.mBufferedStatistics[ lIndex++ ] = pScheduleDuration;
                this.mBufferedStatistics[ lIndex++ ] = pTotalDuration;
                foreach
                    ( Type lMonitoredType in this.mMonitoredTasks )
                {
                    Tuple<int, double> lTaskFrameStatistics = null;
                    if
                        ( this.mFrameStatistics.TryGetValue( lMonitoredType, out lTaskFrameStatistics ) )
                    {
                        this.mBufferedStatistics[ lIndex++ ] = (double)lTaskFrameStatistics.Item1;
                        this.mBufferedStatistics[ lIndex++ ] = lTaskFrameStatistics.Item2;
                    }
                }
                this.mBufferedStatisticCount++;
            }
        }

        /// <summary>
        /// Displays debug information about profiled task(s)
        /// </summary>
        public void DisplaysDebug()
        {
            lock
                ( sSyncRoot )
            {
                foreach
                    ( KeyValuePair<Type, TaskStatistics> lPair in sStatistics )
                {
                    TaskStatistics lStatistics = lPair.Value;
                    double lMean      = lStatistics.DurationSum / lStatistics.TaskCount;
                    double lSqMean    = lStatistics.DurationSqrSum / lStatistics.TaskCount;
                    double lVariance  = lSqMean - lMean * lMean;
                    double lDeviation = Math.Sqrt( lVariance );

                    LogManager.Instance.Log( string.Format( "{0}: {1:0.000} +/- {2:0.000}\nMin/Max: {3:0.000}/{4:0.000}", lPair.Key.Name, lMean / 1000.0, lDeviation / 1000.0, lStatistics.MinDuration / 1000.0, lStatistics.MaxDuration / 1000.0 ) );
                }
            }
        }

        /// <summary>
        /// Clears the frame statistics buffer by writing them in the 
        /// expected file. NOTE that the mutex must be locked before.
        /// see mSaveFile.
        /// </summary>
        private void ClearBufferedFrames()
        {
            if
                ( this.mSaveFile == null ||
                  this.mSaveFile.Exists == false )
            {
                this.mSaveFile = new FileInfo( @"TaskProfiler.stat" );
            }

            using 
                ( StreamWriter lWriter = new StreamWriter( this.mSaveFile.FullName ) )
            {
                lWriter.Write( "Frame scheduling total:" );
                foreach
                    ( Type lMonitored in this.mMonitoredTasks )
                {
                    string lStr = lMonitored.Name;
                    lWriter.Write( string.Format( " {0} {1}", lStr, lStr ) );
                }
                lWriter.WriteLine(); // Jump a line.

                int lCounter = 0;
                for
                    ( int lCurrentBuffered = 0; lCurrentBuffered < this.mBufferedStatisticCount; lCurrentBuffered++ )
                {
                    double lN = this.mBufferedStatistics[ lCounter ] * 1e-3;
                    double lNPlusOne = this.mBufferedStatistics[ lCounter + 1 ] * 1e-3;
                    lWriter.Write( string.Format( "{0} {1} {2}", lCurrentBuffered.ToString(), lN.ToString(), lNPlusOne.ToString() ) );
                    lCounter += 2;
                    foreach
                        ( Type lMonitored in this.mMonitoredTasks )
                    {
                        lWriter.Write(string.Format(" {0} {1}", (int)this.mBufferedStatistics[ lCounter ], this.mBufferedStatistics[ lCounter + 1 ] * 1e-3 ) );
                        lCounter += 2;
                    }
                    lWriter.WriteLine(); // Jump a line.
                }
                this.mBufferedStatisticCount = 0;
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Delegate called on object disposing.
        /// </summary>
        protected override void OnDispose()
        {
            // Add task profiler spe stuff.
            this.mMonitoredTasks.Clear();
            if
                ( this.mBufferedStatisticCount > 0 )
            {
                this.ClearBufferedFrames();
            }
            
            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
