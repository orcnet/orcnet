﻿using OrcNet.Core;

namespace OrcNet.Diagnostics
{
    /// <summary>
    /// Definition of the <see cref="DiagnosticModule"/> class.
    /// </summary>
    internal sealed class DiagnosticModule : AModule
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DiagnosticModule"/> class.
        /// </summary>
        public DiagnosticModule()
        {

        }

        #endregion Constructor
    }
}
