﻿using OrcNet.Core;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Core.Resource;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using System;
using System.Drawing;

namespace OrcNet.Graphics.Text
{
    /// <summary>
    /// Font class to use to draw a text in a given font.
    /// Such a class allows to easily write a line of text directly in a
    /// FrameBuffer at a given position using a texture containing the ASCII chars
    /// and knows which can be displayed.
    /// Any characters outside its range will be displayed as a blank character
    /// defined in the texture (e.g. a square or a question symbol).
    /// </summary>
    public class Font : AMemoryProfilable, IResourceCreatable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the font has been released or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the Font creator resource.
        /// </summary>
        private IResource mCreator;

        /// <summary>
        /// Stores the texture which contains the images of the font.
        /// </summary>
        protected Texture2D mFontText;

        /// <summary>
        /// Stores the amount of characters columns in the texture.
        /// </summary>
        protected int mColumnCount;

        /// <summary>
        /// Stores the amount of characters rows in the texture.
        /// </summary>
        protected int mRowCount;

        /// <summary>
        /// Stores the first ASCII char code to take into account. Lower chars will be replaced by the InvalidChar.
        /// </summary>
        protected int mMinValidChar;

        /// <summary>
        /// Stores the last ASCII char code to take into account. Greater chars will be replaced by the InvalidChar.
        /// </summary>
        protected int mMaxValidChar;

        /// <summary>
        /// Stores the ASCII code to use for invalid characters going beyond the valid range above.
        /// </summary>
        protected int mInvalidChar;

        /// <summary>
        /// Stores the flag indicating whether the font has a fixed width (faster draw call and disallow overlapping characters).
        /// </summary>
        protected bool mHasFixedSize;

        /// <summary>
        /// Stores the set of (pMaxValidChar - pMinValidChar + 1) character's widths in texel. Simply gives spaces between characters.
        /// </summary>
        protected int[] mCharWidths;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = sizeof(int) * 5;
                lSize += sizeof(bool);
                if
                    ( this.mFontText != null )
                {
                    lSize += this.mFontText.Size;
                }
                if
                    ( this.mCharWidths != null )
                {
                    lSize += sizeof(int) * (uint)this.mCharWidths.Length;
                }

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        public IResource Creator
        {
            get
            {
                return this.mCreator;
            }
            set
            {
                this.mCreator = value;
            }
        }

        #endregion Properties IResourceCreatable

        /// <summary>
        /// Gets the texture containing the image of this font.
        /// </summary>
        public Texture2D Image
        {
            get
            {
                return this.mFontText;
            }
        }

        /// <summary>
        /// Gets the width of a character tile.
        /// </summary>
        public float TileWidth
        {
            get
            {
                return (float)this.mFontText.Width / this.mColumnCount;
            }
        }

        /// <summary>
        /// Gets the height of a character tile.
        /// </summary>
        public float TileHeight
        {
            get
            {
                return (float)this.mFontText.Height / this.mRowCount;
            }
        }

        /// <summary>
        /// Gets the aspect ratio of a character tile.
        /// </summary>
        public float TileAspectRatio
        {
            get
            {
                return this.TileWidth / this.TileHeight;
            }
        }

        /// <summary>
        /// Gets the space between the given char and the next one.
        /// Approximates the width of the drawned char.
        /// </summary>
        /// <param name="pChar">The char to get the space for.</param>
        /// <returns>The space between the given char and the next one.</returns>
        public float this[char pChar]
        {
            get
            {
                return this.mCharWidths[ this.GetCharCount( pChar ) ];
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Font"/> class.
        /// </summary>
        /// <param name="pFontText">The texture which contains the images of the font.</param>
        /// <param name="pColumnCount">The amount of characters columns in the texture.</param>
        /// <param name="pRowCount">The amount of characters rows in the texture.</param>
        /// <param name="pMinValidChar">The first ASCII char code to take into account. Lower chars will be replaced by the InvalidChar.</param>
        /// <param name="pMaxValidChar">The last ASCII char code to take into account. Greater chars will be replaced by the InvalidChar.</param>
        /// <param name="pInvalidChar">The ASCII code to use for invalid characters going beyond the valid range above.</param>
        /// <param name="pHasFixedWidth">The flag indicating whether the font has a fixed width (faster draw call and disallow overlapping characters).</param>
        /// <param name="pCharWidths">The set of (pMaxValidChar - pMinValidChar + 1) character's widths in texel. Simply gives spaces between characters.</param>
        public Font(Texture2D pFontText, int pColumnCount, int pRowCount, int pMinValidChar, int pMaxValidChar, int pInvalidChar, bool pHasFixedWidth, int[] pCharWidths)
        {
            this.mFontText = pFontText;
            this.mColumnCount = pColumnCount;
            this.mRowCount = pRowCount;
            this.mMinValidChar = pMinValidChar;
            this.mMaxValidChar = pMaxValidChar;
            this.mInvalidChar = pInvalidChar;
            this.mCharWidths = pCharWidths;
            this.mHasFixedSize = pHasFixedWidth;

            if
                ( this.mCharWidths != null &&
                  this.mCharWidths.Length != (1 + pMaxValidChar - pMinValidChar) )
            {
                LogManager.Instance.Log( "Invalid font characters widths!!!", LogType.ERROR );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a line of text in a given mesh and returns the final position of the line.
        /// </summary>
        /// <param name="pViewport">The framebuffer viewport in pixels.</param>
        /// <param name="pScreenX">The X component on the screen of the first char to display.</param>
        /// <param name="pScreenY">The Y component on the screen of the first char to display.</param>
        /// <param name="pNewLine">The new line of text to display.</param>
        /// <param name="pCharHeight">The height of one char in pixels.</param>
        /// <param name="pLineColor">The color of the line to display.</param>
        /// <param name="pTextMesh">The mesh to write the new line into.</param>
        /// <returns>The final position of the line.</returns>
        public Vector2F AddLine(Viewport pViewport, float pScreenX, float pScreenY, string pNewLine, float pCharHeight, Color pLineColor, Mesh<FontVertex, uint> pTextMesh)
        {
            float lScreenX = pScreenX;
            float lScreenY = pScreenY;
            for ( int lCurr = 0; lCurr < pNewLine.Length; lCurr++ )
            {
                int lIndex = this.GetCharCount( pNewLine[ lCurr ] );
                int lWidth = this.mCharWidths[ lIndex ];

                // If using a fixed width, draw smaller non-overlapping quads
                // Else, draw overlapping quads to allow overlapping characters.
                float lCharRatio = this.mHasFixedSize ? (lWidth / this.TileWidth) : 1.0f;

                int lX = lIndex % this.mColumnCount;
                int lY = lIndex / this.mColumnCount;
                lY = (this.mRowCount - 1) - lY;

                float lU0 = lX / (float)this.mColumnCount;
                float lU1 = (lX + lCharRatio) / this.mColumnCount;
                float lV0 = lY / (float)this.mRowCount;
                float lV1 = (lY + 1.0f) / this.mRowCount;

                float lTileAspectRatio = this.TileAspectRatio;

                float lScreenX0 = lScreenX / pViewport.Width;
                float lScreenX1 = (lScreenX + lCharRatio * lTileAspectRatio * pCharHeight) / pViewport.Width;
                float lScreenY0 = lScreenY / pViewport.Height;
                float lScreenY1 = (lScreenY + pCharHeight) / pViewport.Height;

                //Vector4H lPosUV0 = new Vector4H( new Half( lScreenX0 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY1 * 2.0f ), new Half( lU0 ), new Half( lV0 ) );
                //Vector4H lPosUV1 = new Vector4H( new Half( lScreenX1 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY1 * 2.0f ), new Half( lU1 ), new Half( lV0 ) );
                //Vector4H lPosUV2 = new Vector4H( new Half( lScreenX1 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY0 * 2.0f ), new Half( lU1 ), new Half( lV1 ) );
                //Vector4H lPosUV3 = new Vector4H( new Half( lScreenX0 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY0 * 2.0f ), new Half( lU0 ), new Half( lV1 ) );

                pTextMesh.AddVertex( new FontVertex( new Half( lScreenX0 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY1 * 2.0f ), new Half( lU0 ), new Half( lV0 ), pLineColor.R, pLineColor.G, pLineColor.B, pLineColor.A ) );
                pTextMesh.AddVertex( new FontVertex( new Half( lScreenX1 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY1 * 2.0f ), new Half( lU1 ), new Half( lV0 ), pLineColor.R, pLineColor.G, pLineColor.B, pLineColor.A ) );
                pTextMesh.AddVertex( new FontVertex( new Half( lScreenX1 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY0 * 2.0f ), new Half( lU1 ), new Half( lV1 ), pLineColor.R, pLineColor.G, pLineColor.B, pLineColor.A ) );
                pTextMesh.AddVertex( new FontVertex( new Half( lScreenX1 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY0 * 2.0f ), new Half( lU1 ), new Half( lV1 ), pLineColor.R, pLineColor.G, pLineColor.B, pLineColor.A ) );
                pTextMesh.AddVertex( new FontVertex( new Half( lScreenX0 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY0 * 2.0f ), new Half( lU0 ), new Half( lV1 ), pLineColor.R, pLineColor.G, pLineColor.B, pLineColor.A ) );
                pTextMesh.AddVertex( new FontVertex( new Half( lScreenX0 * 2.0f - 1.0f ), new Half( 1.0f - lScreenY1 * 2.0f ), new Half( lU0 ), new Half( lV0 ), pLineColor.R, pLineColor.G, pLineColor.B, pLineColor.A ) );

                lScreenX += (pCharHeight * lWidth) / this.TileWidth;
            }

            return new Vector2F( lScreenX, lScreenY );
        }

        /// <summary>
        /// Adds a line of text in a given mesh centered at a given position and 
        /// returns the length of the line.
        /// </summary>
        /// <param name="pViewport">The framebuffer viewport in pixels.</param>
        /// <param name="pScreenX">The X component on the screen of the first char to display.</param>
        /// <param name="pScreenY">The Y component on the screen of the first char to display.</param>
        /// <param name="pNewLine">The new line of text to display.</param>
        /// <param name="pCharHeight">The height of one char in pixels.</param>
        /// <param name="pLineColor">The color of the line to display.</param>
        /// <param name="pTextMesh">The mesh to write the new line into.</param>
        /// <returns>The length of the line</returns>
        public Vector2F AddLineCentered(Viewport pViewport, float pScreenX, float pScreenY, string pNewLine, float pCharHeight, Color pLineColor, Mesh<FontVertex, uint> pTextMesh)
        {
            float lScreenX = pScreenX;
            float lScreenY = pScreenY;
            Vector2F lSize = this.GetSize( pNewLine, pCharHeight );
            lScreenX -= lSize.X * 0.5f;

            this.AddLine( pViewport, lScreenX, lScreenY, pNewLine, pCharHeight, pLineColor, pTextMesh );

            return lSize;
        }

        /// <summary>
        /// Gets the size of a given line of text.
        /// </summary>
        /// <param name="pLine">The line of text.</param>
        /// <param name="pHeight">The height of the output text in pixels.</param>
        /// <returns>The size of the given line of text.</returns>
        public Vector2F GetSize(string pLine, float pHeight)
        {
            float lResult = 0.0f;
            int lLineSize = pLine.Length;

            for ( int lCurr = 0; lCurr < lLineSize; lCurr++ )
            {
                int lIndex = this.GetCharCount( pLine[ lCurr ] );
                int lWidth = this.mCharWidths[ lIndex ];
                lResult += (pHeight * lWidth) / this.TileWidth;
            }

            return new Vector2F( lResult, pHeight );
        }

        #region Methods Internal

        /// <summary>
        /// Swaps this font with another.
        /// </summary>
        /// <param name="pOther">The other font to swap with.</param>
        internal void Swap(Font pOther)
        {
            OrcNet.Core.Helpers.Utilities.Swap( ref this.mFontText, ref pOther.mFontText );
            OrcNet.Core.Helpers.Utilities.Swap( ref this.mColumnCount, ref pOther.mColumnCount );
            OrcNet.Core.Helpers.Utilities.Swap( ref this.mRowCount, ref pOther.mRowCount );
            OrcNet.Core.Helpers.Utilities.Swap( ref this.mMinValidChar, ref pOther.mMinValidChar );
            OrcNet.Core.Helpers.Utilities.Swap( ref this.mMaxValidChar, ref pOther.mMaxValidChar );
            OrcNet.Core.Helpers.Utilities.Swap( ref this.mInvalidChar, ref pOther.mInvalidChar );
            OrcNet.Core.Helpers.Utilities.Swap( ref this.mHasFixedSize, ref pOther.mHasFixedSize );
            OrcNet.Core.Helpers.Utilities.Swap( ref this.mCharWidths, ref pOther.mCharWidths );
        }

        /// <summary>
        /// Gets the tile index for the given character and will force
        /// the last character if the supplied char is not supported by this font.
        /// </summary>
        /// <param name="pChar">The character to get the tile index for.</param>
        /// <returns>The Tile index.</returns>
        private int GetCharCount(char pChar)
        {
            int lChar = (int)pChar;
            if
                ( lChar < this.mMinValidChar )
            {
                return this.mInvalidChar - this.mMinValidChar;
            }

            if
                ( lChar > this.mMaxValidChar )
            {
                return this.mInvalidChar - this.mMinValidChar;
            }

            return lChar - this.mMinValidChar;
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if
                ( this.mIsDisposed == false )
            {
                if
                    ( this.mFontText != null )
                {
                    this.mFontText.Dispose();
                    this.mFontText = null;
                }

                this.mCreator = null;

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
