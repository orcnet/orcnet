﻿using OrcNet.Core.Math;

namespace OrcNet.Graphics.Text
{
    /// <summary>
    /// Font vertex structure definition.
    /// </summary>
    public struct FontVertex
    {
        #region Properties

        /// <summary>
        /// Gets or sets the UV1 vertex position.
        /// </summary>
        public Half UV1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the UV2 vertex position.
        /// </summary>
        public Half UV2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the UV3 vertex position.
        /// </summary>
        public Half UV3
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the UV4 vertex position.
        /// </summary>
        public Half UV4
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets or sets the Red color component.
        /// </summary>
        public byte R
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Green color component.
        /// </summary>
        public byte G
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Blue color component.
        /// </summary>
        public byte B
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Alpha color component.
        /// </summary>
        public byte A
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FontVertex"/> class.
        /// </summary>
        /// <param name="pUV1">The UV1 vertex position.</param>
        /// <param name="pUV2">The UV2 vertex position.</param>
        /// <param name="pUV3">The UV3 vertex position.</param>
        /// <param name="pUV4">The UV4 vertex position.</param>
        /// <param name="pR">The Red color component.</param>
        /// <param name="pG">The Green color component.</param>
        /// <param name="pB">The Blue color component.</param>
        /// <param name="pA">The Alpha color component.</param>
        public FontVertex(Half pUV1, Half pUV2, Half pUV3, Half pUV4, byte pR, byte pG, byte pB, byte pA)
        {
            this.UV1 = pUV1;
            this.UV2 = pUV2;
            this.UV3 = pUV3;
            this.UV4 = pUV4;
            this.R = pR;
            this.G = pG;
            this.B = pB;
            this.A = pA;
        }

        #endregion Constructor
    }
}
