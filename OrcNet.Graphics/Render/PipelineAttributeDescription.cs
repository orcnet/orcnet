﻿using OrcNet.Core;
using OrcNet.Core.Render;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Vertex attribute buffer class definition which contains the values
    /// of a single vertex attribute for a list of vertices, that is, a vector
    /// of one or more components of the same type.
    /// E.g. 
    ///     - Position AttributeBuffer contains all positions of a list of vertices.
    ///     - Normal AttributeBuffer contains all normals of a list of vertices.
    ///     - Color AttributeBuffer contains all colors of a list of vertices.
    ///     - so on...
    /// It then describes how these values, stored in buffer object that could be shared
    /// over differents AttributeBuffers, are organized based on an offset and a stride
    /// parameters.
    /// </summary>
    [DebuggerDisplay("Index = {Index}, Type = {Type}, AttributeSize = {AttributeSize},\n ComponentCount = {ComponentCount}, Stride = {Stride}, Normalize = {MustBeNormalized}")]
    public class PipelineAttributeDescription : AMemoryProfilable, IPipelineAttributeDescription
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the attribute is declared with integers or not in the shader.
        /// </summary>
        private bool mUsesInteger;

        /// <summary>
        /// Stores the flag indicating whether the attribute is declared with doubles or not in the shader.
        /// </summary>
        private bool mUsesDouble;

        /// <summary>
        /// Stores the vertex attribute index.
        /// </summary>
        private int mIndex;

        /// <summary>
        /// Stores the vertex attribute components count.
        /// </summary>
        private int mComponentCount;

        /// <summary>
        /// Stores the vertex attribute type.
        /// </summary>
        private AttributeType mType;

        /// <summary>
        /// Stores the flag indicating whether the components must be normalized or not.
        /// </summary>
        private bool mMustBeNormalized;

        /// <summary>
        /// Stores the buffer that contains actual vertex data.
        /// </summary>
        private IBuffer mData;

        /// <summary>
        /// Stores the offset between two consecutive attributes in the data buffer.
        /// </summary>
        private int mStride;

        /// <summary>
        /// Stores the offset of the first attribute value in the data buffer.
        /// </summary>
        private int mOffset;

        /// <summary>
        /// Stores the amount of times each attribute must be instanciated.
        /// Zero to disable attribute instanciating.
        /// </summary>
        private int mDivisor;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(int) * 5;
                lSize += sizeof(AttributeType);
                lSize += sizeof(bool);
                if
                    ( this.mData != null )
                {
                    IntPtr lData = this.mData.GetData();
                    if
                        ( lData != IntPtr.Zero )
                    {
                        lSize += (uint)Marshal.SizeOf( lData );
                    }
                }

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Stores the flag indicating whether the components must be normalized or not.
        /// </summary>
        public bool MustBeNormalized
        {
            get
            {
                return this.mMustBeNormalized;
            }
        }

        /// <summary>
        /// Gets the vertex attribute index.
        /// </summary>
        public int Index
        {
            get
            {
                return this.mIndex;
            }
        }

        /// <summary>
        /// Gets a vertex attribute's value size
        /// </summary>
        public int AttributeSize
        {
            get
            {
                int lSize = 0;
                switch
                    ( this.mType )
                {
                    case AttributeType.SBYTE_BASED:
                    case AttributeType.BYTE_BASED:
                        lSize = 1;
                        break;
                    case AttributeType.INT16_BASED:
                    case AttributeType.UINT16_BASED:
                    case AttributeType.HALF_BASED:
                        lSize = 2;
                        break;
                    case AttributeType.INT_BASED:
                    case AttributeType.UINT_BASED:
                    case AttributeType.FLOAT_BASED:
                    case AttributeType.INT_PACKED_BASED:
                    case AttributeType.UINT_PACKED_BASED:
                        lSize = 4;
                        break;
                    case AttributeType.DOUBLE_BASED:
                        lSize = 8;
                        break;
                }

                return this.mComponentCount * lSize;
            }
        }

        /// <summary>
        /// Gets the vertex attribute components count.
        /// </summary>
        public int ComponentCount
        {
            get
            {
                return this.mComponentCount;
            }
        }

        /// <summary>
        /// Gets the vertex attribute type.
        /// </summary>
        public AttributeType Type
        {
            get
            {
                return this.mType;
            }
        }

        /// <summary>
        /// Gets the buffer that contains actual vertex data.
        /// </summary>
        public IBuffer Data
        {
            get
            {
                return this.mData;
            }
            set
            {
                this.mData = value;
            }
        }

        /// <summary>
        /// Gets the untyped buffer that contains actual vertex data.
        /// </summary>
        object IPipelineAttributeDescription.UntypedData
        {
            get
            {
                return this.Data;
            }
            set
            {
                this.Data = value as IBuffer;
            }
        }

        /// <summary>
        /// Gets the offset between two consecutive attributes in the data buffer.
        /// </summary>
        public int Stride
        {
            get
            {
                return this.mStride;
            }
        }

        /// <summary>
        /// Gets the offset of the first attribute value in the data buffer.
        /// </summary>
        public int Offset
        {
            get
            {
                return this.mOffset;
            }
        }

        /// <summary>
        /// Gets the amount of times each attribute must be instanciated.
        /// Zero to disable attribute instanciating.
        /// </summary>
        public int Divisor
        {
            get
            {
                return this.mDivisor;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the attribute is declared with integers or not in the shader.
        /// </summary>
        public bool UsesInteger
        {
            get
            {
                return this.mUsesInteger;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the attribute is declared with doubles or not in the shader.
        /// </summary>
        public bool UsesDouble
        {
            get
            {
                return this.mUsesDouble;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineAttributeDescription"/> class for
        /// floating point attributes. If the values is defined as integer values, it will need
        /// to be normalized.
        /// </summary>
        /// <param name="pIndex">The vertex attribute index.</param>
        /// <param name="pComponentCount">The vertex attribute components count.</param>
        /// <param name="pType">The vertex attribute components type.</param>
        /// <param name="pMustNormalize">The flag indicating whether passed values (which could be passed as integers) must be normalized between 0-1 or not.</param>
        /// <param name="pData">The buffer containing attribute buffer's data.</param>
        /// <param name="pStride">The offset between two consecutive attribute's values in the data buffer.</param>
        /// <param name="pOffset">The offset of the first attribute value in the data buffer.</param>
        /// <param name="pDivisor">The amount of times each attribute must be instanciated or Zero to disable attributes instanciating.</param>
        public PipelineAttributeDescription(int pIndex, int pComponentCount, AttributeType pType, bool pMustNormalize, IBuffer pData, int pStride = 0, int pOffset = 0, int pDivisor = 0)
        {
            this.mIndex   = pIndex;
            this.mComponentCount   = pComponentCount;
            this.mType    = pType;
            this.mMustBeNormalized = pMustNormalize;
            this.mData    = pData;
            this.mStride  = pStride;
            this.mOffset  = pOffset;
            this.mDivisor = pDivisor;
            this.mUsesDouble  = false;
            this.mUsesInteger = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineAttributeDescription"/> class for
        /// signed or unsigned integer attributes. So values must be defined as so.
        /// </summary>
        /// <param name="pIndex">The vertex attribute index.</param>
        /// <param name="pComponentCount">The vertex attribute components count.</param>
        /// <param name="pType">The vertex attribute components type.</param>
        /// <param name="pData">The buffer containing attribute buffer's data.</param>
        /// <param name="pStride">The offset between two consecutive attribute's values in the data buffer.</param>
        /// <param name="pOffset">The offset of the first attribute value in the data buffer.</param>
        /// <param name="pDivisor">The amount of times each attribute must be instanciated or Zero to disable attributes instanciating.</param>
        public PipelineAttributeDescription(int pIndex, int pComponentCount, AttributeType pType, IBuffer pData, int pStride = 0, int pOffset = 0, int pDivisor = 0) :
        this( pIndex, pComponentCount, pType, false, pData, pStride, pOffset, pDivisor )
        {
            this.mUsesDouble  = false;
            this.mUsesInteger = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineAttributeDescription"/> class for
        /// double precision attributes. So values must be defined as so.
        /// </summary>
        /// <param name="pIndex">The vertex attribute index.</param>
        /// <param name="pComponentCount">The vertex attribute components count.</param>
        /// <param name="pData">The buffer containing attribute buffer's data.</param>
        /// <param name="pStride">The offset between two consecutive attribute's values in the data buffer.</param>
        /// <param name="pOffset">The offset of the first attribute value in the data buffer.</param>
        /// <param name="pDivisor">The amount of times each attribute must be instanciated or Zero to disable attributes instanciating.</param>
        public PipelineAttributeDescription(int pIndex, int pComponentCount, IBuffer pData, int pStride = 0, int pOffset = 0, int pDivisor = 0) :
        this( pIndex, pComponentCount, AttributeType.DOUBLE_BASED, pData, pStride, pOffset, pDivisor )
        {
            this.mUsesDouble  = true;
            this.mUsesInteger = false;
        }

        #endregion Constructor

        #region Methods

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if
                ( this.mData != null )
            {
                this.mData.Dispose();
                this.mData = null;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
