﻿using OrcNet.Core;
using System;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Point description structure definition.
    /// </summary>
    public class PointDescription : AObservable, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the size of a drawn point.
        /// </summary>
        private float mPointSize;

        /// <summary>
        /// Stores the fade threshold point size, that is, 
        /// when the point has to be clamped.
        /// </summary>
        private float mPointSizeFadeThreshold;

        /// <summary>
        /// Stores the flag indicating whether the point origin 
        /// is the lower left corner (true), or the upper left one (false)
        /// </summary>
        private bool  mHasPointLowerLeftOrigin;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the size of a drawn point.
        /// </summary>
        public float PointSize
        {
            get
            {
                return this.mPointSize;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mPointSize, value, "PointSize" );
            }
        }

        /// <summary>
        /// Gets or sets the fade threshold point size, that is, 
        /// when the point has to be clamped.
        /// </summary>
        public float PointSizeFadeThreshold
        {
            get
            {
                return this.mPointSizeFadeThreshold;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mPointSizeFadeThreshold, value, "PointSizeFadeThreshold" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the point origin 
        /// is the lower left corner (true), or the upper left one (false)
        /// </summary>
        public bool HasPointLowerLeftOrigin
        {
            get
            {
                return this.mHasPointLowerLeftOrigin;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mHasPointLowerLeftOrigin, value, "HasPointLowerLeftOrigin" );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PointDescription"/> class.
        /// </summary>
        /// <param name="pPointSize">the size of a drawn point.</param>
        /// <param name="pPointSizeFadeThreshold">the fade threshold point size, that is, when the point has to be clamped.</param>
        /// <param name="pHasPointLowerLeftOrigin">the flag indicating whether the point origin is the lower left corner (true), or the upper left one (false)</param>
        public PointDescription(float pPointSize = 1.0f, float pPointSizeFadeThreshold = 1.0f, bool pHasPointLowerLeftOrigin = false)
        {
            this.mPointSize = pPointSize;
            this.mPointSizeFadeThreshold  = pPointSizeFadeThreshold;
            this.mHasPointLowerLeftOrigin = pHasPointLowerLeftOrigin;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Clone the description.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            PointDescription lClone = new PointDescription( this.mPointSize, this.mPointSizeFadeThreshold, this.mHasPointLowerLeftOrigin );
            return lClone;
        }

        #endregion Methods
    }
}
