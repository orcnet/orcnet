﻿namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Enumeration of the available GLSL uniform
    /// types.
    /// </summary>
    public enum UniformType
    {
        /// <summary>
        /// Single float uniform.
        /// </summary>
        VEC1F,

        /// <summary>
        /// 2D float based vector uniform.
        /// </summary>
        VEC2F,

        /// <summary>
        /// 3D float based vector uniform.
        /// </summary>
        VEC3F,

        /// <summary>
        /// Homogeneous coord float based vector uniform.
        /// </summary>
        VEC4F,

        /// <summary>
        /// Single double uniform.
        /// </summary>
        VEC1D,

        /// <summary>
        /// 2D double based vector uniform.
        /// </summary>
        VEC2D,

        /// <summary>
        /// 3D double based vector uniform.
        /// </summary>
        VEC3D,

        /// <summary>
        /// Homogeneous coord double based vector uniform.
        /// </summary>
        VEC4D,

        /// <summary>
        /// Single integer uniform.
        /// </summary>
        VEC1I,

        /// <summary>
        /// 2D integer based vector uniform.
        /// </summary>
        VEC2I,

        /// <summary>
        /// 3D integer based vector uniform.
        /// </summary>
        VEC3I,

        /// <summary>
        /// Homogeneous coord integer based vector uniform.
        /// </summary>
        VEC4I,

        /// <summary>
        /// Single unsigned integer uniform.
        /// </summary>
        VEC1UI,

        /// <summary>
        /// 2D unsigned integer based vector uniform.
        /// </summary>
        VEC2UI,

        /// <summary>
        /// 3D unsigned integer based vector uniform.
        /// </summary>
        VEC3UI,

        /// <summary>
        /// Homogeneous coord unsigned integer based vector uniform.
        /// </summary>
        VEC4UI,

        /// <summary>
        /// Single boolean uniform.
        /// </summary>
        VEC1B,

        /// <summary>
        /// 2D boolean based vector uniform.
        /// </summary>
        VEC2B,

        /// <summary>
        /// 3D boolean based vector uniform.
        /// </summary>
        VEC3B,

        /// <summary>
        /// Homogeneous coord boolean based vector uniform.
        /// </summary>
        VEC4B,

        /// <summary>
        /// 2x2 float based matrix uniform
        /// </summary>
        MAT2F,

        /// <summary>
        /// 3x3 float based matrix uniform
        /// </summary>
        MAT3F,

        /// <summary>
        /// 4x4 float based matrix uniform
        /// </summary>
        MAT4F,

        /// <summary>
        /// 2x3 float based matrix uniform
        /// </summary>
        MAT2x3F,

        /// <summary>
        /// 2x4 float based matrix uniform
        /// </summary>
        MAT2x4F,

        /// <summary>
        /// 3x2 float based matrix uniform
        /// </summary>
        MAT3x2F,

        /// <summary>
        /// 3x4 float based matrix uniform
        /// </summary>
        MAT3x4F,

        /// <summary>
        /// 4x2 float based matrix uniform
        /// </summary>
        MAT4x2F,

        /// <summary>
        /// 4x3 float based matrix uniform
        /// </summary>
        MAT4x3F,

        /// <summary>
        /// 2x2 double based matrix uniform
        /// </summary>
        MAT2D,

        /// <summary>
        /// 3x3 double based matrix uniform
        /// </summary>
        MAT3D,

        /// <summary>
        /// 4x4 double based matrix uniform
        /// </summary>
        MAT4D,

        /// <summary>
        /// 2x3 double based matrix uniform
        /// </summary>
        MAT2x3D,

        /// <summary>
        /// 2x4 double based matrix uniform
        /// </summary>
        MAT2x4D,

        /// <summary>
        /// 3x2 double based matrix uniform
        /// </summary>
        MAT3x2D,

        /// <summary>
        /// 3x4 double based matrix uniform
        /// </summary>
        MAT3x4D,

        /// <summary>
        /// 4x2 double based matrix uniform
        /// </summary>
        MAT4x2D,

        /// <summary>
        /// 4x3 double based matrix uniform
        /// </summary>
        MAT4x3D,

        /// <summary>
        /// 2x2 float based matrix uniform
        /// </summary>
        MAT2I,

        /// <summary>
        /// 3x3 float based matrix uniform
        /// </summary>
        MAT3I,

        /// <summary>
        /// 4x4 float based matrix uniform
        /// </summary>
        MAT4I,

        /// <summary>
        /// 2x3 float based matrix uniform
        /// </summary>
        MAT2x3I,

        /// <summary>
        /// 2x4 float based matrix uniform
        /// </summary>
        MAT2x4I,

        /// <summary>
        /// 3x2 float based matrix uniform
        /// </summary>
        MAT3x2I,

        /// <summary>
        /// 3x4 float based matrix uniform
        /// </summary>
        MAT3x4I,

        /// <summary>
        /// 4x2 float based matrix uniform
        /// </summary>
        MAT4x2I,

        /// <summary>
        /// 4x3 float based matrix uniform
        /// </summary>
        MAT4x3I,

        /// <summary>
        /// 1D texture sampler.
        /// </summary>
        SAMPLER_1D,

        /// <summary>
        /// 2D texture sampler.
        /// </summary>
        SAMPLER_2D,

        /// <summary>
        /// 3D texture sampler.
        /// </summary>
        SAMPLER_3D,

        /// <summary>
        /// Cube texture sampler.
        /// </summary>
        SAMPLER_CUBE,

        /// <summary>
        /// 1D texture array sampler.
        /// </summary>
        SAMPLER_1D_ARRAY,

        /// <summary>
        /// 2D texture array sampler.
        /// </summary>
        SAMPLER_2D_ARRAY,

        /// <summary>
        /// Cube texture array sampler.
        /// </summary>
        SAMPLER_CUBE_MAP_ARRAY,

        /// <summary>
        /// 2D texture accurate sampler.
        /// </summary>
        SAMPLER_2D_MULTISAMPLE,

        /// <summary>
        /// 2D texture array accurate sampler.
        /// </summary>
        SAMPLER_2D_MULTISAMPLE_ARRAY,

        /// <summary>
        /// Sampler buffer.
        /// </summary>
        SAMPLER_BUFFER,

        /// <summary>
        /// None power of 2 texture sampler.
        /// </summary>
        SAMPLER_2D_RECT,

        /// <summary>
        /// 1D Texture sampler integer based.
        /// </summary>
        INT_SAMPLER_1D,

        /// <summary>
        /// 2D Texture sampler integer based.
        /// </summary>
        INT_SAMPLER_2D,

        /// <summary>
        /// 3D Texture sampler integer based.
        /// </summary>
        INT_SAMPLER_3D,

        /// <summary>
        /// Cube Texture sampler integer based.
        /// </summary>
        INT_SAMPLER_CUBE,

        /// <summary>
        /// 1D Texture array sampler integer based.
        /// </summary>
        INT_SAMPLER_1D_ARRAY,

        /// <summary>
        /// 2D Texture array sampler integer based.
        /// </summary>
        INT_SAMPLER_2D_ARRAY,

        /// <summary>
        /// Cube Texture array sampler integer based.
        /// </summary>
        INT_SAMPLER_CUBE_MAP_ARRAY,

        /// <summary>
        /// 2D Texture accurate sampler integer based.
        /// </summary>
        INT_SAMPLER_2D_MULTISAMPLE,

        /// <summary>
        /// 2D Texture array accurate sampler integer based.
        /// </summary>
        INT_SAMPLER_2D_MULTISAMPLE_ARRAY,

        /// <summary>
        /// Sampler buffer integer based.
        /// </summary>
        INT_SAMPLER_BUFFER,

        /// <summary>
        /// 2D none power of 2 texture sampler integer based.
        /// </summary>
        INT_SAMPLER_2D_RECT,

        /// <summary>
        /// 1D Texture sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_1D,

        /// <summary>
        /// 2D Texture sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_2D,

        /// <summary>
        /// 3D Texture sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_3D,

        /// <summary>
        /// Cube Texture sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_CUBE,

        /// <summary>
        /// 1D Texture array sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_1D_ARRAY,

        /// <summary>
        /// 2D Texture array sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_2D_ARRAY,

        /// <summary>
        /// Cube Texture array sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY,

        /// <summary>
        /// 2D Texture accurate sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE,

        /// <summary>
        /// 2D Texture array accurate sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY,

        /// <summary>
        /// Sampler buffer unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_BUFFER,

        /// <summary>
        /// None power of 2 2D texture sampler unsigned integer based.
        /// </summary>
        UNSIGNED_INT_SAMPLER_2D_RECT,

        /// <summary>
        /// Subroutine.
        /// </summary>
        SUBROUTINE
    };
}
