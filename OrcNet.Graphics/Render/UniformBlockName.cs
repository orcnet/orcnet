﻿using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Uniform block name structure definition.
    /// </summary>
    [DebuggerDisplay("Name = {mName}")]
    public struct UniformBlockName
    {
        #region Fields

        /// <summary>
        /// Stores the block name.
        /// </summary>
        private string mName;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformBlockName"/> class.
        /// </summary>
        /// <param name="pName">The block name</param>
        public UniformBlockName(string pName)
        {
            this.mName = pName;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from UniformBlockName to string.
        /// </summary>
        /// <param name="pName">The block name.</param>
        public static implicit operator string(UniformBlockName pName)
        {
            return pName.mName;
        }

        #endregion Methods
    }
}
