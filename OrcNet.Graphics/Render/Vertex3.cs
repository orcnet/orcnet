﻿namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Definition of the <see cref="Vertex3"/> structure.
    /// Container whose size is determinable by the Marshal.SizeOf() with no padding
    /// important to send to GPU.
    /// </summary>
    public struct Vertex3
    {
        #region Properties

        /// <summary>
        /// Gets or sets the X position component
        /// </summary>
        public float X
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Y position component
        /// </summary>
        public float Y
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Z position component
        /// </summary>
        public float Z
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vertex3"/> structure.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        /// <param name="pZ">The Z component.</param>
        public Vertex3(float pX, float pY, float pZ)
        {
            this.X = pX;
            this.Y = pY;
            this.Z = pZ;
        }

        #endregion Constructor
    }
}
