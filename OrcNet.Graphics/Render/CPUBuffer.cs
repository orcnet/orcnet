﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Helpers;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// CPU buffer class definition.
    /// </summary>
    [DebuggerDisplay("CPUBuffer : Data = {mData}, IsInvalid = {IsInvalid}")]
    public class CPUBuffer : IBuffer
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the flag indicating whether the buffer own the unmanaged memory or not.
        /// </summary>
        private bool mOwnUnmanagedMemory = false;

        /// <summary>
        /// Stores the buffer data pointer
        /// </summary>
        private IntPtr mData;

        #endregion Fields

        #region Properties

        #region Properties IBuffer

        /// <summary>
        /// Gets the flag indicating whether the buffer is up to date or not.
        /// </summary>
        public bool IsInvalid
        {
            get
            {
                return false;
            }
        }

        #endregion Properties IBuffer

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CPUBuffer"/> class.
        /// </summary>
        /// <param name="pData">The unmanaged data.</param>
        public CPUBuffer(IntPtr pData)
        {
            this.mData = pData;
            this.mOwnUnmanagedMemory = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CPUBuffer"/> class.
        /// </summary>
        /// <param name="pData">The managed data.</param>
        /// <param name="pOffset">The offset to apply when copying the data into IntPtr</param>
        public CPUBuffer(byte[] pData, int pOffset = 0)
        {
            if ( pData != null )
            {
                int lLength = pData.Length - pOffset;
                IntPtr lData = Marshal.AllocHGlobal( lLength );
                Marshal.Copy( pData, pOffset, lData, lLength );
                this.mData = lData;
                this.mOwnUnmanagedMemory = true; // The buffer itself allocated the unmanaged memory.
            }
            else
            {
                this.mData = IntPtr.Zero;
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Binds this buffer to the supplied target.
        /// </summary>
        /// <param name="pTarget">The GL buffer target. (e.g: ARRAY_BUFFER, so on)</param>
        public void Bind(int pTarget)
        {
            // Unbind...
            GL.BindBuffer( (BufferTarget)pTarget, 0 );
            GLHelpers.GetError();
        }

        /// <summary>
        /// Gets the pointer to the given offset in this data buffer.
        /// </summary>
        /// <param name="pOffset">The offset from the start of this buffer in byte(s)</param>
        /// <returns>The pointer offseted</returns>
        public IntPtr GetData(int pOffset)
        {
            return this.mData + pOffset;
        }

        /// <summary>
        /// Unbinds this buffer from the supplied target.
        /// </summary>
        /// <param name="pTarget">The GL buffer target. (e.g: ARRAY_BUFFER, so on)</param>
        public void Unbind(int pTarget)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                // The unmanaged memory has been allocated
                // especially for that buffer, so release it.
                if ( this.mOwnUnmanagedMemory &&
                     this.mData != IntPtr.Zero )
                {
                    Marshal.FreeHGlobal( this.mData );
                    this.mData = IntPtr.Zero;
                    this.mOwnUnmanagedMemory = false;
                }
            
                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods
    }
}
