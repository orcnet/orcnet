﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Buffer layout in client memory for transferring pixels to or from GPU.
    /// </summary>
    [DebuggerDisplay("BufferLayout : Alignment = {Alignment}, CompressedSize = {CompressedSize}, IsLSBInFront = {IsLSBInFront}, UsesLittleIndian = {UsesLittleIndian}")]
    public class BufferLayoutParameters
    {
        #region Fields

        /// <summary>
        /// Stores the 2D subpart of the buffer that must be used to transfer pixels,
        /// that is, the X and Y origin with the total width of the image.
        /// </summary>
        private Tuple<int, int, int> mSubImage2D;

        /// <summary>
        /// Stores the 3D part of the buffer that must be used to transfer pixels,
        /// that is, the Z origin with the total height of the image.
        /// </summary>
        private Tuple<int, int> mSubImage3D;

        /// <summary>
        /// Stores the data memory alignment in bytes.
        /// </summary>
        private int  mAlignment;

        /// <summary>
        /// Stores the compressed size of the pixels in bytes.
        /// </summary>
        private int  mCompressedSize;

        /// <summary>
        /// Stores the flag indicating whether it uses little indian mode or not.
        /// </summary>
        private bool mUsesLittleIndian;

        /// <summary>
        /// Stores the flag indicating whether the LSB is first or not.
        /// </summary>
        private bool mIsLSBInFront;

        /// <summary>
        /// Stores the flag indicating whether those parameters have changed or not.
        /// </summary>
        private bool mHasChanged;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether it uses little indian mode or not.
        /// </summary>
        public bool UsesLittleIndian
        {
            get
            {
                return this.mUsesLittleIndian;
            }
            set
            {
                if 
                    ( this.mUsesLittleIndian != value )
                {
                    this.mUsesLittleIndian = value;
                    this.mHasChanged = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the LSB is first or not.
        /// </summary>
        public bool IsLSBInFront
        {
            get
            {
                return this.mIsLSBInFront;
            }
            set
            {
                if 
                    ( this.mIsLSBInFront != value )
                {
                    this.mIsLSBInFront = value;
                    this.mHasChanged = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the data memory alignment in bytes.
        /// </summary>
        public int Alignment
        {
            get
            {
                return this.mAlignment;
            }
            set
            {
                if 
                    ( this.mAlignment != value )
                {
                    this.mAlignment = value;
                    this.mHasChanged = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the compressed size of the pixels in bytes.
        /// </summary>
        public int CompressedSize
        {
            get
            {
                return this.mCompressedSize;
            }
            set
            {
                if 
                    ( this.mCompressedSize != value )
                {
                    this.mCompressedSize = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the 2D subpart of the buffer that must be used to transfer pixels,
        /// that is, the X and Y origin with the total width of the image.
        /// </summary>
        public Tuple<int, int, int> SubImage2D
        {
            get
            {
                return this.mSubImage2D;
            }
            set
            {
                this.mSubImage2D = value;
                this.mHasChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets the 3D part of the buffer that must be used to transfer pixels,
        /// that is, the Z origin with the total height of the image.
        /// </summary>
        public Tuple<int, int> SubImage3D
        {
            get
            {
                return this.mSubImage3D;
            }
            set
            {
                this.mSubImage3D = value;
                this.mHasChanged = true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BufferLayoutParameters"/> class.
        /// </summary>
        public BufferLayoutParameters()
        {
            this.mUsesLittleIndian = false;
            this.mIsLSBInFront = false;
            this.mAlignment = 4;
            this.mCompressedSize = 0;
            this.mHasChanged = false;
            this.mSubImage2D = new Tuple<int, int, int>( 0, 0, 0 );
            this.mSubImage3D = new Tuple<int, int>( 0, 0 );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Applies the defined GL state.
        /// </summary>
        public void Apply()
        {
            // Only do it if any changes.
            if
                ( this.mHasChanged )
            {
                GL.PixelStore( PixelStoreParameter.UnpackSwapBytes,   this.mUsesLittleIndian ? 1 : 0 );
                GL.PixelStore( PixelStoreParameter.UnpackLsbFirst,    this.mIsLSBInFront ? 1 : 0 );
                GL.PixelStore( PixelStoreParameter.UnpackAlignment,   this.mAlignment );
                GL.PixelStore( PixelStoreParameter.UnpackSkipPixels,  this.mSubImage2D.Item1 );
                GL.PixelStore( PixelStoreParameter.UnpackSkipRows,    this.mSubImage2D.Item2 );
                GL.PixelStore( PixelStoreParameter.UnpackRowLength,   this.mSubImage2D.Item3 );
                GL.PixelStore( PixelStoreParameter.UnpackSkipImages,  this.mSubImage3D.Item1 );
                GL.PixelStore( PixelStoreParameter.UnpackImageHeight, this.mSubImage3D.Item2 );
            }
        }

        /// <summary>
        /// Reset the default GL state.
        /// </summary>
        public void Clear()
        {
            // Only do it if any changes.
            if
                ( this.mHasChanged )
            {
                GL.PixelStore( PixelStoreParameter.UnpackSwapBytes,   0 );
                GL.PixelStore( PixelStoreParameter.UnpackLsbFirst,    0 );
                GL.PixelStore( PixelStoreParameter.UnpackAlignment,   4 );
                GL.PixelStore( PixelStoreParameter.UnpackSkipPixels,  0 );
                GL.PixelStore( PixelStoreParameter.UnpackSkipRows,    0 );
                GL.PixelStore( PixelStoreParameter.UnpackRowLength,   0 );
                GL.PixelStore( PixelStoreParameter.UnpackSkipImages,  0 );
                GL.PixelStore( PixelStoreParameter.UnpackImageHeight, 0 );
            }
        }

        #endregion Methods
    }
}
