﻿namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Enumerates the pipeline varying feedback modes.
    /// </summary>
    public enum PipelineVaryingFeedback : int
    {
        /// <summary>
        /// Any mode.
        /// </summary>
        ANY = 0,

        /// <summary>
        /// Interleaved attribute(s).
        /// </summary>
        INTERLEAVED_ATTRIBUTES,

        /// <summary>
        /// Separate attribute(s).
        /// </summary>
        SEPARATE_ATTRIBUTES
    }
}
