﻿// GL.MapBuffer and GL.UnmapBuffer both seem inefficient.
// This option avoids theses calls by using a copy of
// the buffer data on CPU.
#define CUSTOM_MAP_BUFFER

using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Graphics.Helpers;
using OrcNet.Graphics.Render.Uniforms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// GPU buffer class definition.
    /// </summary>
    [DebuggerDisplay("GPUBuffer : BufferId = {BufferId}, BufferSize = {BufferSize}, IsInvalid = {mIsInvalid}")]
    public class GPUBuffer : AMemoryProfilable, IBuffer
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the static buffer manager owning available uniform units.
        /// </summary>
        private static UniformBufferManager sBufferManager = new UniformBufferManager();

        /// <summary>
        /// Stores the buffer id.
        /// </summary>
        private uint mBufferId;

        /// <summary>
        /// Stores the buffer size.
        /// </summary>
        private int  mBufferSize;

        /// <summary>
        /// Stores the current uniform block binding unit this is bounded to.
        /// NOTE: -1 if not bounded.
        /// </summary>
        private int  mCurrentUniformUnit;

        /// <summary>
        /// Stores the flag indicating whether the CPU data are invalid or not due to GPU data changes
        /// e.g: ReadPixels, TransformFeedback, so on).
        /// </summary>
        private bool mIsInvalid;

        /// <summary>
        /// Stores the managed to unmanaged ptr handle.
        /// </summary>
        private GCHandle mDataPtr;

        /// <summary>
        /// Stores the currently mapped data usable from multiple threads 
        /// at the same time whence volatile.
        /// NOTE: Null if not mapped.
        /// </summary>
        private volatile IntPtr mMappedData;

        /// <summary>
        /// Stores the optional copy of the unmanaged volatile mapped buffer data on CPU.
        /// </summary>
        private byte[] mCpuData;

        /// <summary>
        /// Stores the set of pass using that buffer as uniform block.
        /// </summary>
        private List<uint> mUsers;

        /// <summary>
        /// Stores the mutex.
        /// </summary>
        private readonly static object sSyncRoot = new object();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the buffer identifier.
        /// </summary>
        public uint BufferId
        {
            get
            {
                return this.mBufferId;
            }
        }

        /// <summary>
        /// Gets the buffer's size.
        /// </summary>
        public int  BufferSize
        {
            get
            {
                return this.mBufferSize;
            }
        }

        /// <summary>
        /// Gets or sets the current uniform unit that buffer is bound to.
        /// </summary>
        public int CurrentUniformUnit
        {
            get
            {
                return this.mCurrentUniformUnit;
            }
            set
            {
                this.mCurrentUniformUnit = value;
            }
        }

        /// <summary>
        /// Gets the mapped data of that buffer or Null if unmapped.
        /// </summary>
        public IntPtr MappedData
        {
            get
            {
                return this.mMappedData;
            }
        }
        
        #region Properties IBuffer

        /// <summary>
        /// Gets the flag indicating whether the buffer is up to date or not.
        /// </summary>
        public bool IsInvalid
        {
            get
            {
                return this.mIsInvalid;
            }
        }

        #endregion Properties IBuffer

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool);
                lSize += sizeof(int) * 2;
                lSize += sizeof(uint);
                lSize += (uint)IntPtr.Size;
                if
                    ( this.mCpuData != null )
                {
                    lSize += (uint)this.mCpuData.Length * sizeof(char);
                }

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GPUBuffer"/> class.
        /// </summary>
        public GPUBuffer()
        {
            this.mBufferSize = 0;
            this.mMappedData = IntPtr.Zero;
            this.mCpuData    = null;
            this.mIsInvalid  = false;
            this.mUsers = new List<uint>();
            this.mCurrentUniformUnit = -1;

            GL.GenBuffers( 1, out this.mBufferId );
            GLHelpers.GetError();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Maps this buffer into CPU memory and gives a pointer to it.
        /// If access level is different from read only, then any changes
        /// to the mapped buffer in CPU memory will be pushed on GPU once unmapped.
        /// </summary>
        /// <param name="pAccess">The Access level/permission.</param>
        /// <returns></returns>
        public IntPtr Map(BufferAccess pAccess)
        {
            if ( this.mMappedData != IntPtr.Zero )
            {
                return this.mMappedData;
            }

            if ( this.mCpuData != null )
            {
                if ( this.mIsInvalid )
                {
                    GL.BindBuffer( BufferTarget.CopyReadBuffer, this.mBufferId );
                    GL.GetBufferSubData( BufferTarget.CopyReadBuffer, IntPtr.Zero, new IntPtr( this.mBufferSize ), this.mCpuData );
                    GL.BindBuffer( BufferTarget.CopyReadBuffer, 0 );
                    this.mIsInvalid = false;
                }

                this.mDataPtr = GCHandle.Alloc( this.mCpuData, GCHandleType.Pinned );
                this.mMappedData = this.mDataPtr.AddrOfPinnedObject();
            }
            else
            {
                GL.BindBuffer( BufferTarget.CopyReadBuffer, this.mBufferId );
                if ( this.mDataPtr.IsAllocated )
                {
                    // Release previous handle if not null.
                    this.mDataPtr.Free();
                }
                this.mMappedData = GL.MapBuffer( BufferTarget.CopyReadBuffer, pAccess );
                GL.BindBuffer( BufferTarget.CopyReadBuffer, 0 );
                GLHelpers.GetError();
            }
            
            return this.mMappedData;
        }

        /// <summary>
        /// Sets the new data to the GPU and refresh the CPU cache.
        /// </summary>
        /// <param name="pSize">The data size in bytes</param>
        /// <param name="pData">The unmanaged data</param>
        /// <param name="pUsage">The buffer usage.</param>
        public void SetData(int pSize, IntPtr pData, BufferUsageHint pUsage)
        {
            if ( this.mMappedData == IntPtr.Zero )
            {
                this.mBufferSize = pSize;
                GL.BindBuffer( BufferTarget.CopyWriteBuffer, this.mBufferId );
                GL.BufferData( BufferTarget.CopyWriteBuffer, new IntPtr( pSize ), pData, pUsage );
                GL.BindBuffer( BufferTarget.CopyWriteBuffer, 0 );
                GLHelpers.GetError();

                if ( this.mCpuData != null )
                {
                    this.mCpuData = null;
                }

#if (CUSTOM_MAP_BUFFER)
                if ( pSize < 1024 )
                {
                    this.mCpuData = new byte[ pSize ];
                    if ( pData != IntPtr.Zero )
                    {
                        Marshal.Copy( pData, this.mCpuData, 0, pSize );
                    }
                    this.mIsInvalid = false;
                }
#endif
            }
        }

        /// <summary>
        /// Sets the new data to the GPU and refresh the CPU cache.
        /// </summary>
        /// <param name="pData">The byte array</param>
        /// <param name="pUsage">The buffer usage.</param>
        public void SetData(byte[] pData, BufferUsageHint pUsage)
        {
            if ( this.mMappedData == IntPtr.Zero )
            {
                this.mBufferSize = pData.Length;
                GL.BindBuffer( BufferTarget.CopyWriteBuffer, this.mBufferId );
                GL.BufferData( BufferTarget.CopyWriteBuffer, new IntPtr( this.mBufferSize ), pData, pUsage );
                GL.BindBuffer( BufferTarget.CopyWriteBuffer, 0 );
                GLHelpers.GetError();

                if ( this.mCpuData != null )
                {
                    this.mCpuData = null;
                }

#if (CUSTOM_MAP_BUFFER)
                if ( this.mBufferSize < 1024 )
                {
                    this.mCpuData = pData; // Test if it would be better to copy the array or if OK.
                    this.mIsInvalid = false;
                }
#endif
            }
        }

        /// <summary>
        /// Sets a new set of sub data into GPU and refresh the CPU cache as well.
        /// </summary>
        /// <param name="pOffset">The offset index new data must start to be filled at.</param>
        /// <param name="pSize">The overall data size in bytes</param>
        /// <param name="pData">The new sub data to pass to the GPU.</param>
        public void SetSubData(int pOffset, int pSize, IntPtr pData)
        {
            if ( this.mMappedData == null )
            {
                GL.BindBuffer( BufferTarget.CopyWriteBuffer, this.mBufferId );
                GL.BufferSubData( BufferTarget.CopyWriteBuffer, new IntPtr( pOffset ), new IntPtr( pSize ), pData );
                GL.BindBuffer( BufferTarget.CopyWriteBuffer, 0 );
                GLHelpers.GetError();

                if ( this.mCpuData != null )
                {
                    Marshal.Copy( pData, this.mCpuData, pOffset, pSize );
                }
            }
        }

        /// <summary>
        /// Gets a set of sub data from GPU.
        /// </summary>
        /// <param name="pOffset">The offset the copy must start at.</param>
        /// <param name="pSize">The overall data size in bytes.</param>
        /// <param name="pData">The resulting data from GPU.</param>
        public void GetSubData(int pOffset, int pSize, ref IntPtr pData)
        {
            if ( this.mMappedData == IntPtr.Zero )
            {
                GL.BindBuffer( BufferTarget.CopyReadBuffer, this.mBufferId );
                GL.GetBufferSubData( BufferTarget.CopyReadBuffer, new IntPtr( pOffset ), new IntPtr( pSize ), ref pData );
                GL.BindBuffer( BufferTarget.CopyReadBuffer, 0 );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Gets a set of sub data from GPU.
        /// </summary>
        /// <typeparam name="TReturn">The resulting data type.</typeparam>
        /// <param name="pOffset">The offset the copy must start at.</param>
        /// <param name="pSize">The overall data size in bytes.</param>
        /// <param name="pData">The resulting data from GPU.</param>
        public void GetSubData<TReturn>(int pOffset, int pSize, TReturn[] pData) where TReturn : struct
        {
            if ( this.mMappedData == IntPtr.Zero )
            {
                GL.BindBuffer( BufferTarget.CopyReadBuffer, this.mBufferId );
                GL.GetBufferSubData( BufferTarget.CopyReadBuffer, new IntPtr( pOffset ), new IntPtr( pSize ), pData );
                GL.BindBuffer( BufferTarget.CopyReadBuffer, 0 );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Unmaps this buffer from CPU memory.
        /// </summary>
        public void Unmap()
        {
            if ( this.mMappedData == IntPtr.Zero )
            {
                return;
            }

            if ( this.mCpuData != null )
            {
                GL.BindBuffer( BufferTarget.CopyWriteBuffer, this.mBufferId );
                GL.BufferSubData( BufferTarget.CopyWriteBuffer, IntPtr.Zero, new IntPtr( this.mBufferSize ), this.mCpuData );
                GL.BindBuffer( BufferTarget.CopyWriteBuffer, 0 );
                GLHelpers.GetError();
            }
            else
            {
                GL.BindBuffer( BufferTarget.CopyReadBuffer, this.mBufferId );
                GL.UnmapBuffer( BufferTarget.CopyReadBuffer );
                GL.BindBuffer( BufferTarget.CopyReadBuffer, 0 );
                GLHelpers.GetError();
            }

            if ( this.mDataPtr.IsAllocated )
            {
                // Release previous handle if not null.
                this.mDataPtr.Free();
            }
            this.mMappedData = IntPtr.Zero;
        }

        #region Methods Internal

        /// <summary>
        /// Invalidate the buffer data.
        /// </summary>
        internal void Invalidate()
        {
            this.mIsInvalid = true;
        }

        /// <summary>
        /// Binds that buffer to a uniform buffer unit.
        /// </summary>
        /// <param name="pPassIds">The pass(es) ids that must use that buffer as uniform block.</param>
        /// <returns>The uniform unit identifier if binding is successful, -1 otherwise if too many units used already.</returns>
        internal int BindToUniformUnit(IEnumerable<uint> pPassIds)
        {
            int lResult = this.mCurrentUniformUnit;
            if ( lResult == -1 )
            {
                lResult = sBufferManager.FindAvailableUnit( pPassIds );
            }

            // If anything...
            if ( lResult >= 0 )
            {
                sBufferManager.Bind( (uint)lResult, this );
            }

            return lResult;
        }

        /// <summary>
        /// Checks whether the supplied pass(es) ids are using the buffer as an uniform block.
        /// </summary>
        /// <param name="pPassIds">The pass ids that use that buffer</param>
        /// <returns>True if the pass(es) are using that buffer as uniform block, false otherwise.</returns>
        internal bool HasUsers(IEnumerable<uint> pPassIds)
        {
            if ( pPassIds == null )
            {
                return false;
            }

            return this.mUsers.Intersect( pPassIds ).Any();
        }

        /// <summary>
        /// Adds a new pass that will use that buffer as uniform block.
        /// </summary>
        /// <param name="pPassId">The pass id that will use that buffer as uniform block.</param>
        internal void AddUser(uint pPassId)
        {
            if ( this.mUsers.Contains( pPassId ) == false )
            {
                this.mUsers.Add( pPassId );
            }
        }

        /// <summary>
        /// Removes a pass using that buffer as uniform block.
        /// </summary>
        /// <param name="pPassId">The pass id that was using that buffer as uniform block.</param>
        /// <returns>True if the pass was a user and thus has been removed, false otherwise.</returns>
        internal bool RemoveUser(uint pPassId)
        {
            if ( this.mUsers.Contains( pPassId ) )
            {
                return this.mUsers.Remove( pPassId );
            }

            return false;
        }

        #endregion Methods Internal

        #region Methods IBuffer

        /// <summary>
        /// Binds this buffer to the supplied target.
        /// </summary>
        /// <param name="pTarget">The GL buffer target. (e.g: ARRAY_BUFFER, so on)</param>
        public void Bind(int pTarget)
        {
            GL.BindBuffer( (BufferTarget)pTarget, this.mBufferId );
            GLHelpers.GetError();
        }

        /// <summary>
        /// Gets the pointer to the given offset in this data buffer.
        /// </summary>
        /// <param name="pOffset">The offset from the start of this buffer in byte(s)</param>
        /// <returns>The pointer offseted</returns>
        public IntPtr GetData(int pOffset)
        {
            return new IntPtr( (long)pOffset );
        }

        /// <summary>
        /// Unbinds this buffer from the supplied target.
        /// </summary>
        /// <param name="pTarget">The GL buffer target. (e.g: ARRAY_BUFFER, so on)</param>
        public void Unbind(int pTarget)
        {
            GL.BindBuffer( (BufferTarget)pTarget, 0 );
            GLHelpers.GetError();
        }

        #endregion Methods IBuffer

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            sBufferManager.Unbind( this );

            this.mCpuData = null;

            GL.DeleteBuffers( 1, ref this.mBufferId );
            GLHelpers.GetError();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
