﻿using OrcNet.Core;
using System;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Line description structure definition.
    /// </summary>
    public class LineDescription : AObservable, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the size of a drawn line.
        /// </summary>
        private float mLineSize;
        
        /// <summary>
        /// Stores the flag indicating whether the antialiasing must be used or not.
        /// </summary>
        private bool  mSmoothesLines;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the size of a drawn line.
        /// </summary>
        public float LineSize
        {
            get
            {
                return this.mLineSize;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mLineSize, value, "LineSize" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the antialiasing must be used or not.
        /// </summary>
        public bool  SmoothesLines
        {
            get
            {
                return this.mSmoothesLines;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mSmoothesLines, value, "SmoothesLines" );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LineDescription"/> class.
        /// </summary>
        /// <param name="pLineSize">the size of a drawn line.</param>
        /// <param name="pSmoothesLines">the flag indicating whether the antialiasing must be used or not.</param>
        public LineDescription(float pLineSize = 1.0f, bool pSmoothesLines = false)
        {
            this.mLineSize = pLineSize;
            this.mSmoothesLines = pSmoothesLines;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Clone the description.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            LineDescription lClone = new LineDescription( this.mLineSize, this.mSmoothesLines );
            return lClone;
        }

        #endregion Methods
    }
}
