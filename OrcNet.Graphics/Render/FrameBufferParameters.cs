﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Logger;
using OrcNet.Graphics.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// State parameters of a frame buffer class definition.
    /// </summary>
    [DebuggerDisplay("FrameBufferParams : ViewportCount = {mViewports.Length}, ClearColor = {ClearColor}, ClearDepth = {ClearDepth}, ClearStencil = {ClearStencil}, ClipDistances = {ClipDistances}")]
    public class FrameBufferParameters : AObservable, IObserver
    {
        #region Delegates

        /// <summary>
        /// Delegate prototype definition for an observable property changed notification.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pSender">The object owning the property that has changed.</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        public delegate void ObservableCallback(IObserver pObserver, IObservable pSender, PropertyChangedEventArgs pEventArgs);

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the global OpenGL based changes counter.
        /// </summary>
        private static int sGlobalOpenGLChangesCounter;

        /// <summary>
        /// Stores the observable callbacks by observed object types to allow a specific process regarding
        /// to the observed object and not a global single method with a huge branching as the user observes
        /// multiple observables.
        /// </summary>
        private static Dictionary<Type, ObservableCallback> sObservableByTypeCallBacks;

        /// <summary>
        /// Stores the maximum viewport count allowed.
        /// </summary>
        private const int    cMaxViewports = 16;

        /// <summary>
        /// Stores the maximum draw buffer count allowed.
        /// </summary>
        private const int    cMawDrawBuffers = 4;

        /// <summary>
        /// Stores the flag indicating whether multiple viewports are specified or not.
        /// </summary>
        private bool         mHasMultipleViewports;

        /// <summary>
        /// Stores the viewport(s). Can be only one.
        /// </summary>
        private Viewport[]   mViewports;

        /// <summary>
        /// Stores the depth range(s). Depends on viewport count.
        /// </summary>
        private DepthRange[] mDepthRanges;

        /// <summary>
        /// Stores the clipping flags defining which planes must be used
        /// for clipping.
        /// </summary>
        private int mClipDistances;

        /// <summary>
        /// Stores the unique transform counter incremented each time viewport count or one of them dependencies
        /// change such as depth, clipdistances and so on.
        /// </summary>
        private int mTransformCounter;

        /// <summary>
        /// Stores the color to use when clearing the frame buffer.
        /// </summary>
        private Color mClearColor;

        /// <summary>
        /// Stores the depth value to use when clearing the depth buffer.
        /// </summary>
        private float mClearDepth;

        /// <summary>
        /// Stores the stencil value to use when clearing the stencil buffer.
        /// </summary>
        private int mClearStencil;

        /// <summary>
        /// Stores the unique clear counter incremented each time the color, depth or stencil buffers
        /// are cleared.
        /// </summary>
        private int mClearCounter;

        /// <summary>
        /// Stores the drawn points description.
        /// </summary>
        private PointDescription mPointDescription;

        /// <summary>
        /// Stores the unique point counter incremented each time 
        /// the point size, fade threshold or origin parameters change.
        /// </summary>
        private int   mPointChangesCounter;

        /// <summary>
        /// Stores the drawn lines description.
        /// </summary>
        private LineDescription mLineDescription;

        /// <summary>
        /// Stores the unique line counter incremented each time 
        /// the parameters change.
        /// </summary>
        private int mLineChangesCounter;

        /// <summary>
        /// Stores the drawn polygons description.
        /// </summary>
        private PolygonDescription mPolygonDescription;

        /// <summary>
        /// Stores the unique polygon counter incremented each time
        /// the polygon parameters change.
        /// </summary>
        private int   mPolygonChangesCounter;

        /// <summary>
        /// Stores the sampling description.
        /// </summary>
        private SamplingDescription mSamplingDescription;

        /// <summary>
        /// Stores the unique sampling counter incremented each time
        /// the sampling parameters change.
        /// </summary>
        private int mSamplingChangesCounter;

        /// <summary>
        /// Stores the occlusion description.
        /// </summary>
        private OcclusionDescription mOcclusionDescription;

        /// <summary>
        /// Stores the scissor description.
        /// </summary>
        private ScissorDescription mScissorDescription;

        /// <summary>
        /// Stores the unique scissor counter incremented each time
        /// the scissor parameters change.
        /// </summary>
        private int mScissorChangesCounter;

        /// <summary>
        /// Stores the stencil description.
        /// </summary>
        private StencilDescription mStencilDescription;

        /// <summary>
        /// Stores the unique stencil counter incremented each time
        /// the stencil parameters change.
        /// </summary>
        private int mStencilChangesCounter;

        /// <summary>
        /// Stores the depth description.
        /// </summary>
        private DepthDescription mDepthDescription;

        /// <summary>
        /// Stores the unique depth counter incremented each time
        /// the depth parameters change.
        /// </summary>
        private int mDepthChangesCounter;

        /// <summary>
        /// Stores the blending description.
        /// </summary>
        private BlendingDescription mBlendingDescription;

        /// <summary>
        /// Stores the unique blending counter incremented each time
        /// the blending parameters change.
        /// </summary>
        private int mBlendingChangesCounter;

        /// <summary>
        /// Stores the flag indicating whether the dithering is used or not.
        /// Dithering selects, for each color component, either the largest representable color
        /// value(for that particular color component) that is less than or equal to the incoming
        /// color component value, c, or the smallest representable color value that is greater
        /// than or equal to c.
        /// </summary>
        private bool mUseDither;

        /// <summary>
        /// Stores the flag indicating whether logical operations must be applied between the incoming
        /// fragment's color value and the framebuffer's color value at the corresponding location.
        /// </summary>
        private bool mApplyLogicalOperations;

        /// <summary>
        /// Stores the logical operation to combine fragment colors with the framebuffer colors if enabled.
        /// </summary>
        private LogicOp mLogicalOperation;

        /// <summary>
        /// Stores the flag indicating whether each draw buffers can have their own color mask or not.
        /// </summary>
        private bool mAllowColorMaskPerBuffer;

        /// <summary>
        /// Stores the color masks per draw buffer. Ordered in the form Red, Green, Blue, Alpha.
        /// </summary>
        private Tuple<bool, bool, bool, bool>[] mColorMasks;

        /// <summary>
        /// Stores the flag indicating whether the depth buffer can be written to or not.
        /// </summary>
        private bool mAllowDepthBufferWrite;

        /// <summary>
        /// Stores the the stencil mask to use for front faces.
        /// </summary>
        private uint mFrontStencilMask;

        /// <summary>
        /// Stores the the stencil mask to use for back faces.
        /// </summary>
        private uint mBackStencilMask;

        /// <summary>
        /// Stores the unique mask counter incremented each time
        /// the mask parameters change.
        /// </summary>
        private int  mMaskChangesCounter;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether multiple viewports are specified or not.
        /// </summary>
        public bool HasMultipleViewports
        {
            get
            {
                return this.mHasMultipleViewports;
            }
        }

        /// <summary>
        /// Gets the viewport(s). Can be only one.
        /// </summary>
        public IEnumerable<Viewport> Viewports
        {
            get
            {
                return this.mViewports;
            }
        }

        /// <summary>
        /// Gets the viewport at the given index.
        /// </summary>
        /// <param name="pIndex">The viewport index.</param>
        /// <returns>The viewport.</returns>
        public Viewport this[ViewportIndex pIndex]
        {
            get
            {
                return this.mViewports[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the depth range(s). Depends on viewport count.
        /// </summary>
        public IEnumerable<DepthRange> DepthRanges
        {
            get
            {
                return this.mDepthRanges;
            }
        }

        /// <summary>
        /// Gets the depth range at the given index.
        /// </summary>
        /// <param name="pIndex">The depth range index.</param>
        /// <returns>The depth range.</returns>
        public DepthRange this[DepthRangeIndex pIndex]
        {
            get
            {
                return this.mDepthRanges[ pIndex ];
            }
        }

        /// <summary>
        /// Gets or sets the clipping flags defining which planes must be used
        /// for clipping.
        /// </summary>
        public int ClipDistances
        {
            get
            {
                return this.mClipDistances;
            }
            set
            {
                if
                    ( this.mClipDistances != value )
                {
                    this.mTransformCounter = ++sGlobalOpenGLChangesCounter;
                }

                this.NotifyPropertyChanged( ref this.mClipDistances, value, "ClipDistances" );
            }
        }
        
        /// <summary>
        /// Gets or sets the color to use when clearing the frame buffer.
        /// </summary>
        public Color ClearColor
        {
            get
            {
                return this.mClearColor;
            }
            set
            {
                if
                    ( this.mClearColor != value )
                {
                    this.mClearCounter = ++sGlobalOpenGLChangesCounter;
                }

                this.NotifyPropertyChanged( ref this.mClearColor, value, "ClearColor" );
            }
        }

        /// <summary>
        /// Gets or sets the depth value to use when clearing the depth buffer.
        /// </summary>
        public float ClearDepth
        {
            get
            {
                return this.mClearDepth;
            }
            set
            {
                if
                    ( this.mClearDepth != value )
                {
                    this.mClearCounter = ++sGlobalOpenGLChangesCounter;
                }

                this.NotifyPropertyChanged( ref this.mClearDepth, value, "ClearDepth" );
            }
        }

        /// <summary>
        /// Gets or sets the stencil value to use when clearing the stencil buffer.
        /// </summary>
        public int ClearStencil
        {
            get
            {
                return this.mClearStencil;
            }
            set
            {
                if
                    ( this.mClearStencil != value )
                {
                    this.mClearCounter = ++sGlobalOpenGLChangesCounter;
                }

                this.NotifyPropertyChanged( ref this.mClearStencil, value, "ClearStencil" );
            }
        }
        
        /// <summary>
        /// Gets the drawn points description.
        /// </summary>
        public PointDescription PointDescription
        {
            get
            {
                return this.mPointDescription;
            }
        }
        
        /// <summary>
        /// Gets the drawn lines description.
        /// </summary>
        public LineDescription LineDescription
        {
            get
            {
                return this.mLineDescription;
            }
        }
        
        /// <summary>
        /// Gets the drawn polygons description.
        /// </summary>
        public PolygonDescription PolygonDescription
        {
            get
            {
                return this.mPolygonDescription;
            }
        }
        
        /// <summary>
        /// Gets the sampling description.
        /// </summary>
        public SamplingDescription SamplingDescription
        {
            get
            {
                return this.mSamplingDescription;
            }
        }
        
        /// <summary>
        /// Gets the occlusion description.
        /// </summary>
        public OcclusionDescription OcclusionDescription
        {
            get
            {
                return this.mOcclusionDescription;
            }
        }

        /// <summary>
        /// Gets the scissor description.
        /// </summary>
        public ScissorDescription ScissorDescription
        {
            get
            {
                return this.mScissorDescription;
            }
        }
        
        /// <summary>
        /// Gets the stencil description.
        /// </summary>
        public StencilDescription StencilDescription
        {
            get
            {
                return this.mStencilDescription;
            }
        }
        
        /// <summary>
        /// Gets the depth description.
        /// </summary>
        public DepthDescription DepthDescription
        {
            get
            {
                return this.mDepthDescription;
            }
        }

        /// <summary>
        /// Gets the blending description.
        /// </summary>
        public BlendingDescription BlendingDescription
        {
            get
            {
                return this.mBlendingDescription;
            }
        }
        
        /// <summary>
        /// Gets or sets the flag indicating whether the dithering is used or not.
        /// Dithering selects, for each color component, either the largest representable color
        /// value(for that particular color component) that is less than or equal to the incoming
        /// color component value, c, or the smallest representable color value that is greater
        /// than or equal to c.
        /// </summary>
        public bool UseDither
        {
            get
            {
                return this.mUseDither;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mUseDither, value, "UseDither" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether logical operations must be applied between the incoming
        /// fragment's color value and the framebuffer's color value at the corresponding location.
        /// </summary>
        public bool UseLogicalOperations
        {
            get
            {
                return this.mApplyLogicalOperations;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mApplyLogicalOperations, value, "UseLogicalOperations" );
            }
        }

        /// <summary>
        /// Gets the logical operation to combine fragment colors with the framebuffer colors if enabled.
        /// </summary>
        public LogicOp LogicalOperation
        {
            get
            {
                return this.mLogicalOperation;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether each draw buffers can have their own color mask or not.
        /// </summary>
        public bool IsColorMaskPerBuffer
        {
            get
            {
                return this.mAllowColorMaskPerBuffer;
            }
        }

        /// <summary>
        /// Gets the color mask for the given draw buffer index.
        /// </summary>
        /// <param name="pIndex">The index of the draw buffer the color mask is for.</param>
        /// <returns>The color mask</returns>
        public Tuple<bool, bool, bool, bool> this[ColorMaskIndex pIndex]
        {
            get
            {
                return this.mColorMasks[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the color mask per buffer. Ordered in the form Red, Green, Blue, Alpha per mask.
        /// </summary>
        public IEnumerable<Tuple<bool, bool, bool, bool>> ColorMasks
        {
            get
            {
                return this.mColorMasks;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the depth buffer can be written to or not.
        /// </summary>
        public bool AllowDepthBufferWrite
        {
            get
            {
                return this.mAllowDepthBufferWrite;
            }
            set
            {
                if
                    ( this.mAllowDepthBufferWrite != value )
                {
                    this.mMaskChangesCounter = ++sGlobalOpenGLChangesCounter;
                }

                this.NotifyPropertyChanged( ref this.mAllowDepthBufferWrite, value, "AllowDepthBufferWrite" );
            }
        }

        /// <summary>
        /// Gets or sets the the stencil mask to use for front faces.
        /// </summary>
        public uint FrontStencilMask
        {
            get
            {
                return this.mFrontStencilMask;
            }
            set
            {
                if
                    ( this.mFrontStencilMask != value )
                {
                    this.mMaskChangesCounter = ++sGlobalOpenGLChangesCounter;
                }

                this.NotifyPropertyChanged( ref this.mFrontStencilMask, value, "FrontStencilMask" );
            }
        }

        /// <summary>
        /// Gets or sets the the stencil mask to use for back faces.
        /// </summary>
        public uint BackStencilMask
        {
            get
            {
                return this.mBackStencilMask;
            }
            set
            {
                if
                    ( this.mBackStencilMask != value )
                {
                    this.mMaskChangesCounter = ++sGlobalOpenGLChangesCounter;
                }

                this.NotifyPropertyChanged( ref this.mBackStencilMask, value, "BackStencilMask" );
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="FrameBufferParameters"/> class.
        /// </summary>
        static FrameBufferParameters()
        {
            sGlobalOpenGLChangesCounter = 0;
            sObservableByTypeCallBacks = new Dictionary<Type, ObservableCallback>();
            sObservableByTypeCallBacks.Add( typeof(PointDescription), OnPointDescriptionChanged );
            sObservableByTypeCallBacks.Add( typeof(LineDescription), OnLineDescriptionChanged );
            sObservableByTypeCallBacks.Add( typeof(PolygonDescription), OnPolygonDescriptionChanged );
            sObservableByTypeCallBacks.Add( typeof(SamplingDescription), OnSamplingDescriptionChanged );
            sObservableByTypeCallBacks.Add( typeof(OcclusionDescription), OnOcclusionDescriptionChanged );
            sObservableByTypeCallBacks.Add( typeof(ScissorDescription), OnScissorDescriptionChanged );
            sObservableByTypeCallBacks.Add( typeof(StencilDescription), OnStencilDescriptionChanged );
            sObservableByTypeCallBacks.Add( typeof(DepthDescription), OnDepthDescriptionChanged );
            sObservableByTypeCallBacks.Add( typeof(BlendingDescription), OnBlendingDescriptionChanged );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameBufferParameters"/> class.
        /// </summary>
        public FrameBufferParameters()
        {
            this.mHasMultipleViewports = false;
            this.mViewports = new Viewport[ cMaxViewports ];
            this.mViewports[ 0 ] = new Viewport( 0.0f, 0.0f, 0.0f, 0.0f );
            this.mDepthRanges = new DepthRange[ cMaxViewports ];
            this.mDepthRanges[ 0 ] = new DepthRange( 0.0, 1.0 );
            this.mClipDistances = 0;
            this.mTransformCounter = 0;
            this.mClearColor = Color.FromArgb( 0, 0, 0, 0 );
            this.mClearDepth = 1.0f;
            this.mClearStencil = 0;
            this.mClearCounter = 0;
            this.mPointDescription = new PointDescription();
            this.mPointDescription.AddPropertyObserver( this );
            this.mPointChangesCounter = 0;
            this.mLineDescription = new LineDescription();
            this.mLineDescription.AddPropertyObserver( this );
            this.mPolygonDescription = new PolygonDescription();
            this.mPolygonDescription.AddPropertyObserver( this );
            this.mPolygonChangesCounter = 0;
            this.mSamplingDescription = new SamplingDescription();
            this.mSamplingDescription.AddPropertyObserver( this );
            this.mSamplingChangesCounter = 0;
            this.mOcclusionDescription = new OcclusionDescription();
            this.mOcclusionDescription.AddPropertyObserver( this );
            Scissor[] lScissors = new Scissor[ cMaxViewports ];
            lScissors[ 0 ] = new Scissor( 0, 0, 0, 0 );
            bool[] lEnableScissors = new bool[ cMaxViewports ];
            lEnableScissors[ 0 ] = false;
            this.mScissorDescription = new ScissorDescription( false, lScissors, lEnableScissors );
            this.mScissorDescription.AddPropertyObserver( this );
            this.mScissorChangesCounter = 0;
            this.mStencilDescription = new StencilDescription();
            this.mStencilDescription.AddPropertyObserver( this );
            this.mStencilChangesCounter = 0;
            this.mDepthDescription = new DepthDescription();
            this.mDepthDescription.AddPropertyObserver( this );
            this.mDepthChangesCounter = 0;
            this.mBlendingDescription = new BlendingDescription( false, false, cMawDrawBuffers );
            this.mBlendingDescription.AddPropertyObserver( this );
            this.mBlendingChangesCounter = 0;
            this.mUseDither = false;
            this.mApplyLogicalOperations = false;
            this.mLogicalOperation = LogicOp.Copy;
            this.mAllowColorMaskPerBuffer = false;
            this.mAllowDepthBufferWrite   = true;
            this.mFrontStencilMask = 0xFFFFFFFF;
            this.mBackStencilMask  = 0xFFFFFFFF;
            this.mMaskChangesCounter = 0;
            
            this.mColorMasks = new Tuple<bool, bool, bool, bool>[ cMawDrawBuffers ];
            for
                ( uint lCurr = 0; lCurr < cMawDrawBuffers; lCurr++ )
            {
                this.mColorMasks[ lCurr ] = new Tuple<bool, bool, bool, bool>( true, true, true, true );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Provides the single viewport the frame buffer must use.
        /// </summary>
        /// <param name="pViewport">The viewport.</param>
        public void SingleViewport(Viewport pViewport)
        {
            this.mHasMultipleViewports = false;
            this.mViewports[ 0 ] = pViewport;
            this.mTransformCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.
        }

        /// <summary>
        /// Provides one of the viewports the frame buffer must use for
        /// multiple viewports use.
        /// </summary>
        /// <param name="pViewportIndex">The viewport index.</param>
        /// <param name="pViewport">The viewport.</param>
        public void InsertViewport(ViewportIndex pViewportIndex, Viewport pViewport)
        {
            if ( this.mHasMultipleViewports == false )
            {
                for ( int lCurr = 0; lCurr < cMaxViewports; lCurr++ )
                {
                    Viewport lFirstViewport = this.mViewports[ 0 ];
                    DepthRange lFirstDepthRange = this.mDepthRanges[ 0 ];
                    this.mViewports[ lCurr ] = lFirstViewport.Clone() as Viewport;
                    this.mDepthRanges[ lCurr ] = lFirstDepthRange.Clone() as DepthRange;
                }
                this.mHasMultipleViewports = true;
            }

            this.mViewports[ pViewportIndex ] = pViewport;
            this.mTransformCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.
        }

        /// <summary>
        /// Provides the single viewport depth range.
        /// </summary>
        /// <param name="pRange">The depth range.</param>
        public void ViewportDepthRange(DepthRange pRange)
        {
            this.mHasMultipleViewports = false;
            this.mDepthRanges[ 0 ] = pRange;
            this.mTransformCounter = ++sGlobalOpenGLChangesCounter;
        }

        /// <summary>
        /// Provides one of the viewports depth range.
        /// </summary>
        /// <param name="pIndex">The depth range index corresponding to the viewport as well.</param>
        /// <param name="pRange">The depth range.</param>
        public void InsertViewportDepthRange(DepthRangeIndex pIndex, DepthRange pRange)
        {
            if ( this.mHasMultipleViewports == false )
            {
                for ( int lCurr = 0; lCurr < cMaxViewports; lCurr++ )
                {
                    Viewport lFirstViewport = this.mViewports[ 0 ];
                    DepthRange lFirstDepthRange = this.mDepthRanges[ 0 ];
                    this.mViewports[ lCurr ] = lFirstViewport.Clone() as Viewport;
                    this.mDepthRanges[ lCurr ] = lFirstDepthRange.Clone() as DepthRange;
                }
                this.mHasMultipleViewports = true;
            }

            this.mDepthRanges[ pIndex ] = pRange;
            this.mTransformCounter = ++sGlobalOpenGLChangesCounter;
        }

        /// <summary>
        /// Pass the logical operation to apply if enabled
        /// </summary>
        /// <param name="pEnable">The flag indicating whether the logical operations must be applied or not.</param>
        /// <param name="pOperation">The operation to apply.</param>
        public void ApplyLogicalOperations(bool pEnable, LogicOp pOperation)
        {
            this.mApplyLogicalOperations = pEnable;
            this.mLogicalOperation = pOperation;

            this.InformPropertyChangedListener( pEnable, pEnable, "UseLogicalOperations" );
        }

        /// <summary>
        /// Sets the color buffer writing mask 
        /// </summary>
        /// <param name="pRed">Can write red channel</param>
        /// <param name="pGreen">Can write green channel</param>
        /// <param name="pBlue">Can write blue channel</param>
        /// <param name="pAlpha">Can write alpha channel</param>
        public void ColorMask(bool pRed, bool pGreen, bool pBlue, bool pAlpha)
        {
            this.mAllowColorMaskPerBuffer = false;
            this.mColorMasks[ 0 ] = new Tuple<bool, bool, bool, bool>( pRed, pGreen, pBlue, pAlpha );
            this.mMaskChangesCounter = ++sGlobalOpenGLChangesCounter;

            this.InformPropertyChangedListener( this.mColorMasks, this.mColorMasks, "ColorMasks" );
        }

        /// <summary>
        /// Sets the color buffer writing mask for a given draw buffer.
        /// </summary>
        /// <param name="pBufferId">The buffer id</param>
        /// <param name="pRed">Can write red channel</param>
        /// <param name="pGreen">Can write green channel</param>
        /// <param name="pBlue">Can write blue channel</param>
        /// <param name="pAlpha">Can write alpha channel</param>
        public void ColorMask(BufferId pBufferId, bool pRed, bool pGreen, bool pBlue, bool pAlpha)
        {
            this.mAllowColorMaskPerBuffer = true;
            this.mColorMasks[ GLHelpers.GetBufferIndex( pBufferId ) ] = new Tuple<bool, bool, bool, bool>( pRed, pGreen, pBlue, pAlpha );
            this.mMaskChangesCounter = ++sGlobalOpenGLChangesCounter;

            this.InformPropertyChangedListener( this.mColorMasks, this.mColorMasks, "ColorMasks" );
        }
        
        /// <summary>
        /// Updates the frame buffer state by settings a new set of parameters
        /// Only different parameters will be sent.
        /// </summary>
        /// <param name="pNewState">The new frame buffer state.</param>
        public void Update(FrameBufferParameters pNewState)
        {
            LogManager.Instance.Log( "Updating frame buffer state!!!" );

            int lVersion;
            GL.GetInteger( GetPName.MajorVersion, out lVersion );

            // Check for transform changes?
            if ( this.mTransformCounter != pNewState.mTransformCounter )
            {
                this.mTransformCounter = pNewState.mTransformCounter;
                this.mHasMultipleViewports = pNewState.mHasMultipleViewports;

                if ( pNewState.mHasMultipleViewports )
                {
                    for ( int lCurrViewport = 0; lCurrViewport < cMaxViewports; lCurrViewport++ )
                    {
                        Viewport lViewport = pNewState.mViewports[ lCurrViewport ];
                        DepthRange lDepthRange = pNewState.mDepthRanges[ lCurrViewport ];
                        GL.ViewportIndexed( lCurrViewport, lViewport.Left, lViewport.Bottom, lViewport.Width, lViewport.Height );
                        GL.DepthRangeIndexed( lCurrViewport, lDepthRange.Near, lDepthRange.Far );

                        this.mViewports[ lCurrViewport ] = lViewport.Clone() as Viewport;
                        this.mDepthRanges[ lCurrViewport ] = lDepthRange.Clone() as DepthRange;
                    }
                }
                else
                {
                    Viewport lViewport = pNewState.mViewports[ 0 ];
                    DepthRange lDepthRange = pNewState.mDepthRanges[ 0 ];
                    GL.Viewport( (int)lViewport.Left, (int)lViewport.Bottom, (int)lViewport.Width, (int)lViewport.Height );
                    GL.DepthRange( lDepthRange.Near, lDepthRange.Far );

                    this.mViewports[ 0 ] = lViewport.Clone() as Viewport;
                    this.mDepthRanges[ 0 ] = lDepthRange.Clone() as DepthRange;
                }

                // Enable clip distances.
                for ( int lCurrClip = 0; lCurrClip < 6; lCurrClip++ )
                {
                    GLHelpers.Enable( (EnableCap)((int)EnableCap.ClipPlane0 + lCurrClip), (pNewState.mClipDistances & (1 << lCurrClip)) != 0 );
                }
                this.mClipDistances = pNewState.mClipDistances;
            }

            // Checks for clear changes?
            if ( this.mClearCounter != pNewState.mClearCounter )
            {
                this.mClearCounter = pNewState.mClearCounter;

                GL.ClearColor( pNewState.mClearColor );
                GL.ClearDepth( pNewState.mClearDepth );
                GL.ClearStencil( pNewState.mClearStencil );

                this.mClearColor   = pNewState.mClearColor;
                this.mClearDepth   = pNewState.mClearDepth;
                this.mClearStencil = pNewState.mClearStencil;

            }

            // Checks for points description changes?
            if ( this.mPointChangesCounter != pNewState.mPointChangesCounter )
            {
                this.mPointChangesCounter = pNewState.mPointChangesCounter;

                PointDescription lNewDescription = pNewState.mPointDescription;
                GLHelpers.Enable( EnableCap.ProgramPointSize, lNewDescription.PointSize <= 0.0f );
                GL.PointSize( lNewDescription.PointSize );
                GL.PointParameter( PointParameterName.PointFadeThresholdSize, lNewDescription.PointSizeFadeThreshold );
                GL.PointParameter( lNewDescription.HasPointLowerLeftOrigin ? PointSpriteCoordOriginParameter.LowerLeft : PointSpriteCoordOriginParameter.UpperLeft );

                this.mPointDescription = pNewState.mPointDescription.Clone() as PointDescription;
            }

            // Checks for lines description changes?
            if ( this.mLineChangesCounter != pNewState.mLineChangesCounter )
            {
                this.mLineChangesCounter = pNewState.mLineChangesCounter;

                LineDescription lNewDescription = pNewState.mLineDescription;
                GLHelpers.Enable( EnableCap.LineSmooth, lNewDescription.SmoothesLines );
                GL.LineWidth( lNewDescription.LineSize );

                this.mLineDescription = pNewState.mLineDescription.Clone() as LineDescription;
            }

            // Checks for polygons description changes?
            if ( this.mPolygonChangesCounter != pNewState.mPolygonChangesCounter )
            {
                this.mPolygonChangesCounter = pNewState.mPolygonChangesCounter;

                PolygonDescription lNewDescription = pNewState.mPolygonDescription;
                GL.FrontFace( lNewDescription.AreFrontFacesCW ? FrontFaceDirection.Cw : FrontFaceDirection.Ccw );

                if ( lNewDescription.CullFront || 
                     lNewDescription.CullBack )
                {
                    GL.Enable( EnableCap.CullFace );
                    if ( lNewDescription.CullFront && 
                         lNewDescription.CullBack )
                    {
                        GL.CullFace( CullFaceMode.FrontAndBack );
                    }
                    else if ( lNewDescription.CullFront )
                    {
                        GL.CullFace( CullFaceMode.Front );
                    }
                    else
                    {
                        GL.CullFace( CullFaceMode.Back );
                    }
                }
                else
                {
                    GL.Disable( EnableCap.CullFace );
                }

                if ( lNewDescription.CullFront )
                {
                    if ( lNewDescription.CullBack == false )
                    {
                        GL.PolygonMode( MaterialFace.FrontAndBack, lNewDescription.BackMode );
                    }
                }
                else
                {
                    GL.PolygonMode( MaterialFace.FrontAndBack, lNewDescription.FrontMode );
                }

                GLHelpers.GetError();

                GLHelpers.Enable( EnableCap.PolygonSmooth, lNewDescription.SmoothesPolygons );
                GL.PolygonOffset( lNewDescription.PolygonOffset.X, lNewDescription.PolygonOffset.Y );
                GLHelpers.Enable( EnableCap.PolygonOffsetPoint, lNewDescription.PolygonOffsets.Item1 );
                GLHelpers.Enable( EnableCap.PolygonOffsetLine,  lNewDescription.PolygonOffsets.Item2 );
                GLHelpers.Enable( EnableCap.PolygonOffsetFill,  lNewDescription.PolygonOffsets.Item3 );

                this.mPolygonDescription = pNewState.mPolygonDescription.Clone() as PolygonDescription;
            }

            // Checks for sampling description changes?
            if ( this.mSamplingChangesCounter != pNewState.mSamplingChangesCounter )
            {
                this.mSamplingChangesCounter = pNewState.mSamplingChangesCounter;

                SamplingDescription lNewDescription = pNewState.mSamplingDescription;
                GLHelpers.Enable( EnableCap.Multisample, lNewDescription.UseMultiSampling );
                GLHelpers.Enable( EnableCap.SampleAlphaToCoverage, lNewDescription.SampleAlphaToCoverage );
                GLHelpers.Enable( EnableCap.SampleAlphaToOne, lNewDescription.ForceOpacity );
                GLHelpers.Enable( EnableCap.SampleCoverage, lNewDescription.FragmentCoverage < 1.0f );
                GL.SampleCoverage( System.Math.Abs( lNewDescription.FragmentCoverage ), lNewDescription.FragmentCoverage < 0.0f );
                GLHelpers.Enable( EnableCap.SampleMask, lNewDescription.SampleMask != 0xFFFFFFFF );
                GL.SampleMask( 0, lNewDescription.SampleMask );
                if ( lVersion >= 4 )
                {
                    GLHelpers.Enable( EnableCap.SampleShading, lNewDescription.SampleShading );
                    GL.MinSampleShading( lNewDescription.MinSampleCount );
                }

                this.mSamplingDescription = pNewState.mSamplingDescription.Clone() as SamplingDescription;
            }

            // Cheks for scissor description changes?
            if ( this.mScissorChangesCounter != pNewState.mScissorChangesCounter )
            {
                this.mScissorChangesCounter = pNewState.mScissorChangesCounter;

                ScissorDescription lNewDescription = pNewState.mScissorDescription;
                if ( lNewDescription.UseMultiScissor )
                {
                    for
                        ( int lCurrScissor = 0; lCurrScissor < cMaxViewports; lCurrScissor++ )
                    {
                        if
                            ( lNewDescription[ new ScissorDescription.ScissorStateIndex( lCurrScissor ) ] )
                        {
                            GL.Enable( IndexedEnableCap.ScissorTest, lCurrScissor );
                        }
                        else
                        {
                            GL.Disable( IndexedEnableCap.ScissorTest, lCurrScissor );
                        }

                        Scissor lScissor = lNewDescription[ new ScissorDescription.ScissorIndex( lCurrScissor ) ];
                        GL.ScissorIndexed( lCurrScissor, (int)lScissor.Left, (int)lScissor.Bottom, (int)lScissor.Width, (int)lScissor.Height );
                    }
                }
                else
                {
                    GLHelpers.Enable( EnableCap.ScissorTest, lNewDescription[ new ScissorDescription.ScissorStateIndex( 0 ) ] );

                    Scissor lFirst = lNewDescription[ new ScissorDescription.ScissorIndex( 0 ) ];
                    GL.Scissor( (int)lFirst.Left, (int)lFirst.Bottom, (int)lFirst.Width, (int)lFirst.Height );
                }

                this.mScissorDescription = pNewState.mScissorDescription.Clone() as ScissorDescription;
            }

            // Checks for stencil description changes?
            if ( this.mStencilChangesCounter != pNewState.mStencilChangesCounter )
            {
                this.mStencilChangesCounter = pNewState.mStencilChangesCounter;

                StencilDescription lNewDescription = pNewState.mStencilDescription;
                GLHelpers.Enable( EnableCap.StencilTest, lNewDescription.UseStencil );
                GL.StencilFuncSeparate( StencilFace.Front, lNewDescription.FrontFunction, lNewDescription.FrontReference, lNewDescription.FrontMask );
                GL.StencilFuncSeparate( StencilFace.Back, lNewDescription.BackFunction, lNewDescription.BackReference, lNewDescription.BackMask );
                GL.StencilOpSeparate( StencilFace.Front, lNewDescription.FrontStencilFailAction, lNewDescription.FrontDepthFailAction, lNewDescription.FrontDepthPassAction );
                GL.StencilOpSeparate( StencilFace.Back, lNewDescription.BackStencilFailAction, lNewDescription.BackDepthFailAction, lNewDescription.BackDepthPassAction );

                this.mStencilDescription = pNewState.mStencilDescription.Clone() as StencilDescription;
            }

            // Checks for depth description changes?
            if ( this.mDepthChangesCounter != pNewState.mDepthChangesCounter )
            {
                this.mDepthChangesCounter = pNewState.mDepthChangesCounter;

                DepthDescription lNewDescription = pNewState.DepthDescription;
                GLHelpers.Enable( EnableCap.DepthTest, lNewDescription.UseDepth );
                GL.DepthFunc( lNewDescription.DepthFunction );

                this.mDepthDescription = pNewState.mDepthDescription.Clone() as DepthDescription;
            }

            // Checks for blending description changes?
            if ( this.mBlendingChangesCounter != pNewState.mBlendingChangesCounter )
            {
                this.mBlendingChangesCounter = pNewState.mBlendingChangesCounter;

                BlendingDescription lNewDescription = pNewState.mBlendingDescription;
                if ( lNewDescription.UseMultiBlending )
                {
                    for ( int lCurr = 0; lCurr < cMawDrawBuffers; lCurr++ )
                    {
                        if
                            ( lNewDescription[ new BlendingDescription.BlendingStateIndex( lCurr ) ] )
                        {
                            GL.Enable( IndexedEnableCap.Blend, lCurr );
                        }
                        else
                        {
                            GL.Disable( IndexedEnableCap.Blend, lCurr );
                        }
                    }
                }
                else
                {
                    GLHelpers.Enable( EnableCap.Blend, lNewDescription[ new BlendingDescription.BlendingStateIndex( 0 ) ] );
                }

                if ( lNewDescription.UseMultipleEquations &&
                     lVersion >= 4 )
                {
                    for ( int lCurr = 0; lCurr < cMawDrawBuffers; lCurr++ )
                    {
                        GL.BlendEquationSeparate( lCurr, lNewDescription[ new BlendingDescription.BlendingRGBEquationIndex( lCurr ) ], lNewDescription[ new BlendingDescription.BlendingAlphaEquationIndex( lCurr ) ] );
                        GL.BlendFuncSeparate( lCurr, lNewDescription[ new BlendingDescription.BlendingRGBSourceFactorIndex( lCurr ) ], lNewDescription[ new BlendingDescription.BlendingRGBDestinationFactorIndex( lCurr ) ], lNewDescription[ new BlendingDescription.BlendingAlphaSourceFactorIndex( lCurr ) ], lNewDescription[ new BlendingDescription.BlendingAlphaDestinationFactorIndex( lCurr ) ] );
                    }
                }
                else
                {
                    GL.BlendEquationSeparate( lNewDescription[ new BlendingDescription.BlendingRGBEquationIndex( 0 ) ], lNewDescription[ new BlendingDescription.BlendingAlphaEquationIndex( 0 ) ] );
                    GL.BlendFuncSeparate( lNewDescription[ new BlendingDescription.BlendingRGBSourceFactorIndex( 0 ) ], lNewDescription[ new BlendingDescription.BlendingRGBDestinationFactorIndex( 0 ) ], lNewDescription[ new BlendingDescription.BlendingAlphaSourceFactorIndex( 0 ) ], lNewDescription[ new BlendingDescription.BlendingAlphaDestinationFactorIndex( 0 ) ] );
                }

                GL.BlendColor( lNewDescription.ConstantColor );

                this.mBlendingDescription = pNewState.mBlendingDescription.Clone() as BlendingDescription;
            }

            // Dithering?
            if ( this.mUseDither != pNewState.mUseDither )
            {
                GLHelpers.Enable( EnableCap.Dither, pNewState.UseDither );

                this.mUseDither = pNewState.mUseDither;
            }

            // Logical operations??
            if ( this.mApplyLogicalOperations != pNewState.mApplyLogicalOperations ||
                 this.mLogicalOperation != pNewState.mLogicalOperation )
            {
                GLHelpers.Enable( EnableCap.ColorLogicOp, this.mApplyLogicalOperations );
                GL.LogicOp( this.mLogicalOperation );

                this.mApplyLogicalOperations = pNewState.mApplyLogicalOperations;
                this.mLogicalOperation = pNewState.mLogicalOperation;
            }

            // Write masks changes?
            if ( this.mMaskChangesCounter != pNewState.mMaskChangesCounter )
            {
                this.mMaskChangesCounter = pNewState.mMaskChangesCounter;
                this.mAllowColorMaskPerBuffer = pNewState.mAllowColorMaskPerBuffer;

                if ( pNewState.mAllowColorMaskPerBuffer )
                {
                    for ( int lCurrMask = 0; lCurrMask < cMawDrawBuffers; lCurrMask++ )
                    {
                        Tuple<bool, bool, bool, bool> lColorMask = pNewState.mColorMasks[ lCurrMask ];
                        GL.ColorMask( lCurrMask, lColorMask.Item1, lColorMask.Item2, lColorMask.Item3, lColorMask.Item4 );

                        this.mColorMasks[ lCurrMask ] = new Tuple<bool, bool, bool, bool>( lColorMask.Item1, lColorMask.Item2, lColorMask.Item3, lColorMask.Item4 );
                    }
                }
                else
                {
                    Tuple<bool, bool, bool, bool> lColorMask = pNewState.mColorMasks[ 0 ];
                    GL.ColorMask( lColorMask.Item1, lColorMask.Item2, lColorMask.Item3, lColorMask.Item4 );

                    this.mColorMasks[ 0 ] = new Tuple<bool, bool, bool, bool>( lColorMask.Item1, lColorMask.Item2, lColorMask.Item3, lColorMask.Item4 );
                }

                GL.DepthMask( pNewState.mAllowDepthBufferWrite );
                GL.StencilMaskSeparate( StencilFace.Front, pNewState.mFrontStencilMask );
                GL.StencilMaskSeparate( StencilFace.Back, pNewState.mBackStencilMask );

                this.mAllowDepthBufferWrite = pNewState.mAllowDepthBufferWrite;
                this.mFrontStencilMask = pNewState.mFrontStencilMask;
                this.mBackStencilMask  = pNewState.mBackStencilMask;
            }

            GLHelpers.GetError();
        }

        #region Methods IObserver

        /// <summary>
        /// Delegate called on observed object property changes.
        /// </summary>
        /// <param name="pSender">The property changed sender</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        public void OnObservablePropertyChanged(IObservable pSender, PropertyChangedEventArgs pEventArgs)
        {
            if ( pSender == null )
            {
                return;
            }

            ObservableCallback lCallback;
            if ( sObservableByTypeCallBacks.TryGetValue( pSender.GetType(), out lCallback ) )
            {
                lCallback( this, pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on point description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnPointDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;
            lParameters.mPointChangesCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.

            lParameters.InformPropertyChangedListener( lParameters.mPointDescription, lParameters.mPointDescription, "PointDescription" );
        }

        /// <summary>
        /// Delegate called on line description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnLineDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;
            lParameters.mLineChangesCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.

            lParameters.InformPropertyChangedListener( lParameters.mLineDescription, lParameters.mLineDescription, "LineDescription" );
        }

        /// <summary>
        /// Delegate called on polygon description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnPolygonDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;
            lParameters.mPolygonChangesCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.

            lParameters.InformPropertyChangedListener( lParameters.mPolygonDescription, lParameters.mPolygonDescription, "PolygonDescription" );
        }

        /// <summary>
        /// Delegate called on sampling description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnSamplingDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;
            lParameters.mSamplingChangesCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.

            lParameters.InformPropertyChangedListener( lParameters.mSamplingDescription, lParameters.mSamplingDescription, "SamplingDescription" );
        }

        /// <summary>
        /// Delegate called on occlusion description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnOcclusionDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;

            lParameters.InformPropertyChangedListener( lParameters.mOcclusionDescription, lParameters.mOcclusionDescription, "OcclusionDescription" );
        }

        /// <summary>
        /// Delegate called on scissor description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnScissorDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;
            lParameters.mScissorChangesCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.

            lParameters.InformPropertyChangedListener( lParameters.mScissorDescription, lParameters.mScissorDescription, "ScissorDescription" );
        }

        /// <summary>
        /// Delegate called on stencil description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnStencilDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;
            lParameters.mStencilChangesCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.

            lParameters.InformPropertyChangedListener( lParameters.mStencilDescription, lParameters.mStencilDescription, "StencilDescription" );
        }

        /// <summary>
        /// Delegate called on depth description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnDepthDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;
            lParameters.mDepthChangesCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.

            lParameters.InformPropertyChangedListener( lParameters.mDepthDescription, lParameters.mDepthDescription, "DepthDescription" );
        }

        /// <summary>
        /// Delegate called on blending description property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        /// <param name="pDescription">The description</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        private static void OnBlendingDescriptionChanged(IObserver pObserver, IObservable pDescription, PropertyChangedEventArgs pEventArgs)
        {
            FrameBufferParameters lParameters = pObserver as FrameBufferParameters;
            lParameters.mBlendingChangesCounter = ++sGlobalOpenGLChangesCounter; // Incremente before setting.

            lParameters.InformPropertyChangedListener( lParameters.mBlendingDescription, lParameters.mBlendingDescription, "BlendingDescription" );
        }

        #endregion Methods IObserver

        #region Methods IDisposable

        /// <summary>
        /// Delegate called on dispose.
        /// </summary>
        protected override void OnDispose()
        {
            if ( this.mPointDescription != null )
            {
                this.mPointDescription.Dispose();
                this.mPointDescription = null;
            }

            if ( this.mLineDescription != null )
            {
                this.mLineDescription.Dispose();
                this.mLineDescription = null;
            }

            if ( this.mPolygonDescription != null )
            {
                this.mPolygonDescription.Dispose();
                this.mPolygonDescription = null;
            }

            if ( this.mSamplingDescription != null )
            {
                this.mSamplingDescription.Dispose();
                this.mSamplingDescription = null;
            }

            if ( this.mOcclusionDescription != null )
            {
                this.mOcclusionDescription.Dispose();
                this.mOcclusionDescription = null;
            }

            if ( this.mScissorDescription != null )
            {
                this.mScissorDescription.Dispose();
                this.mScissorDescription = null;
            }

            if ( this.mStencilDescription != null )
            {
                this.mStencilDescription.Dispose();
                this.mStencilDescription = null;
            }

            if ( this.mDepthDescription != null )
            {
                this.mDepthDescription.Dispose();
                this.mDepthDescription = null;
            }

            if ( this.mBlendingDescription != null )
            {
                this.mBlendingDescription.Dispose();
                this.mBlendingDescription = null;
            }

            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Helper structure to access by index one of the owned viewports.
        /// </summary>
        public struct ViewportIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="ViewportIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The viewport index.</param>
            public ViewportIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from ViewportIndex to int
            /// </summary>
            /// <param name="pIndex">The viewport index</param>
            public static implicit operator int(ViewportIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned depth ranges.
        /// </summary>
        public struct DepthRangeIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="DepthRangeIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The depth range index.</param>
            public DepthRangeIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from DepthRangeIndex to int
            /// </summary>
            /// <param name="pIndex">The depth range index</param>
            public static implicit operator int(DepthRangeIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned color masks.
        /// </summary>
        public struct ColorMaskIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="ColorMaskIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The color mask index.</param>
            public ColorMaskIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from ColorMaskIndex to int
            /// </summary>
            /// <param name="pIndex">The color mask index</param>
            public static implicit operator int(ColorMaskIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        #endregion Inner classes
    }
}
