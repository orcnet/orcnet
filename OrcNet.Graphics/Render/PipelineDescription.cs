﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Render;
using OrcNet.Core.Resource;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Render.Uniforms.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// NOTE: Module :-D
    /// Pipeline description class owning all the pipeline stages.
    /// All stages are optional. These stages must be defined either
    /// in its own GLSL compilation unit for each, or all grouped in
    /// a single compilation unit but seperated using the following
    /// preprocessor directives. It can also specify some initial values
    /// for stages uniforms and record varying variables.
    /// </summary>
    [DebuggerDisplay("Version = {Version}, IsValid = {IsValid}, VaryingCount = {mFeedbackVaryings.Count}, UserCount = {mUsers.Count}")]
    public class PipelineDescription : AMemoryProfilable, IPipelineDescription
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the stage header identifier prefix 
        /// </summary>
        private const string cHeaderPrefix = "#define";

        /// <summary>
        /// Stores the constant stage identifier prefix.
        /// </summary>
        private const string cStagePrefix = "#ifdef";

        /// <summary>
        /// Stores the vertex stage identifier.
        /// </summary>
        private const string cVertexIdentifier = "_VERTEX_";

        /// <summary>
        /// Stores the tesselation control stage identifier.
        /// </summary>
        private const string cTessControlIdentifier = "_TESS_CONTROL_";

        /// <summary>
        /// Stores the tesselation evaluation stage identifier.
        /// </summary>
        private const string cTessEvaluationIdentifier = "_TESS_EVAL_";

        /// <summary>
        /// Stores the geometry stage identifier.
        /// </summary>
        private const string cGeometryIdentifier = "_GEOMETRY_";

        /// <summary>
        /// Stores the fragment stage identifier.
        /// </summary>
        private const string cFragmentIdentifier = "_FRAGMENT_";

        /// <summary>
        /// Stores the flag indicating whether teh description is valid or not.
        /// That is every stages are Valid as well.
        /// </summary>
        private bool mIsValid;

        /// <summary>
        /// Stores the pipeline description creator resource.
        /// </summary>
        private IResource mCreator;

        /// <summary>
        /// Stores the transform feedback mode to use.
        /// </summary>
        private PipelineVaryingFeedback mFeedbackMode;

        /// <summary>
        /// Stores the pipeline GLSL version
        /// </summary>
        private int mVersion;

        /// <summary>
        /// Stores the pipeline definition stages.
        /// </summary>
        private Dictionary<StageType, PipelineStage> mStages;

        /// <summary>
        /// Stores the pipeline description's pass(es) user(s)
        /// </summary>
        private List<PipelinePass>  mUsers;

        /// <summary>
        /// Stores the pipeline initial value(s).
        /// </summary>
        private Dictionary<string, IPipelineValue> mInitialValues;

        /// <summary>
        /// Stores the output varying variables of the pipeline that must be recorded
        /// in transform feedback mode.
        /// </summary>
        private List<string> mFeedbackVaryings;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(PipelineVaryingFeedback);
                lSize += sizeof(bool);
                lSize += sizeof(char) * (uint)cStagePrefix.Length;
                lSize += sizeof(char) * (uint)cVertexIdentifier.Length;
                lSize += sizeof(char) * (uint)cTessControlIdentifier.Length;
                lSize += sizeof(char) * (uint)cTessEvaluationIdentifier.Length;
                lSize += sizeof(char) * (uint)cGeometryIdentifier.Length;
                lSize += sizeof(char) * (uint)cFragmentIdentifier.Length;
                lSize += sizeof(int);
                foreach
                    ( string lVarying in this.mFeedbackVaryings )
                {
                    lSize += sizeof(char) * (uint)lVarying.Length;
                }
                foreach
                    ( KeyValuePair<StageType, PipelineStage> lPair in this.mStages )
                {
                    lSize += lPair.Value.Size;
                }
                foreach
                    ( PipelinePass lUser in this.mUsers )
                {
                    lSize += lUser.Size;
                }
                foreach
                    ( KeyValuePair<string, IPipelineValue> lPair in this.mInitialValues )
                {
                    lSize += sizeof(char) * (uint)lPair.Key.Length;
                    lSize += lPair.Value.Size;
                }
                lSize += sizeof(StageType) * (uint)this.mStages.Count;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        public IResource Creator
        {
            get
            {
                return this.mCreator;
            }
            set
            {
                this.mCreator = value;
            }
        }

        #endregion Properties IResourceCreatable

        /// <summary>
        /// Gets the flag indicating whether teh description is valid or not.
        /// That is every stages are Valid as well.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.mIsValid;
            }
        }

        /// <summary>
        /// Gets or sets the transform feedback mode to use.
        /// </summary>
        public PipelineVaryingFeedback FeedbackMode
        {
            get
            {
                return this.mFeedbackMode;
            }
            set
            {
                this.mFeedbackMode = value;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the pipeline description is still used or not.
        /// </summary>
        public bool HasUsers
        {
            get
            {
                return this.mUsers.Count > 0;
            }
        }

        /// <summary>
        /// Gets the pipeline description's pass(es) user(s).
        /// </summary>
        public IEnumerable<PipelinePass> Users
        {
            get
            {
                return this.mUsers;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the pipeline description has feedback varying value(s) or not.
        /// </summary>
        public bool HasVaryings
        {
            get
            {
                return this.mFeedbackVaryings.Count > 0;
            }
        }

        /// <summary>
        /// Gets the output varying variables of the pipeline that must be recorded
        /// in transform feedback mode.
        /// </summary>
        public IEnumerable<string> Varyings
        {
            get
            {
                return this.mFeedbackVaryings;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the pipeline description has any stages or not.
        /// </summary>
        public bool HasStages
        {
            get
            {
                return this.mStages.Count > 0;
            }
        }
        
        /// <summary>
        /// Gets the pipeline description's stage(s).
        /// </summary>
        public IEnumerable<PipelineStage> Stages
        {
            get
            {
                return this.mStages.Values;
            }
        }

        /// <summary>
        /// Gets the requested stage corresponding to the supplied type.
        /// </summary>
        /// <param name="pType">The stage type</param>
        /// <returns>The pipeline stage if found, null otherwise.</returns>
        public PipelineStage this[StageType pType]
        {
            get
            {
                PipelineStage lStage;
                if ( this.mStages.TryGetValue( pType, out lStage ) )
                {
                    return lStage;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the pipeline description's initial values.
        /// </summary>
        public IEnumerable<KeyValuePair<string, IPipelineValue>> InitialValues
        {
            get
            {
                 return this.mInitialValues;
            }
        }

        /// <summary>
        /// Gets the pipeline GLSL version
        /// </summary>
        public int Version
        {
            get
            {
                return this.mVersion;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineDescription"/> class.
        /// </summary>
        /// <param name="pStages">The stages constituting the pipeline description.</param>
        public PipelineDescription(params PipelineStage[] pStages)
        {
            this.Initialize( pStages );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineDescription"/> class.
        /// </summary>
        /// <param name="pVersion">The required version.</param>
        /// <param name="pSource">The stage(s) source code.</param>
        public PipelineDescription(int pVersion, string pSource)
        {
            this.mVersion = pVersion;
            if ( this.mVersion == -1 ) // If invalid version??
            {
                GL.GetInteger( GetPName.MajorVersion, out this.mVersion );
            }
            
            // Create stage(s) from source
            List<PipelineStage> lStages = new List<PipelineStage>();

            // Gets each stage if exists
            PipelineStage lVertexStage;
            if ( CreateVertexStage( this.mVersion, pSource, out lVertexStage ) )
            {
                lStages.Add( lVertexStage );
            }

            PipelineStage lTessControlStage;
            if ( CreateTesselationControlStage( this.mVersion, pSource, out lTessControlStage ) )
            {
                lStages.Add( lTessControlStage );
            }

            PipelineStage lTessEvaluationStage;
            if ( CreateTesselationEvaluationStage( this.mVersion, pSource, out lTessEvaluationStage ) )
            {
                lStages.Add( lTessEvaluationStage );
            }

            PipelineStage lGeometryStage;
            if ( CreateGeometryStage( this.mVersion, pSource, out lGeometryStage ) )
            {
                lStages.Add( lGeometryStage );
            }

            PipelineStage lFragmentStage;
            if ( CreateFragmentStage( this.mVersion, pSource, out lFragmentStage ) )
            {
                lStages.Add( lFragmentStage );
            }
            
            this.Initialize( lStages.ToArray() );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the pipeline description has the given stage or not.
        /// </summary>
        /// <param name="pType">The stage to look for.</param>
        /// <returns>True if such a stage is described by that pipeline description, false otherwise.</returns>
        public bool HasStage(StageType pType)
        {
            return this.mStages.ContainsKey( pType );
        }

        /// <summary>
        /// Adds a new pipeline user of that description.
        /// </summary>
        /// <param name="pUser">The new pass user.</param>
        public void AddUser(PipelinePass pUser)
        {
            if ( this.mUsers.Contains( pUser ) == false )
            {
                this.mUsers.Add( pUser );
            }
        }

        /// <summary>
        /// Removes a pipeline description's pass user.
        /// </summary>
        /// <param name="pUser">The pass that was using that pipeline description.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveUser(PipelinePass pUser)
        {
            if ( this.mUsers.Contains( pUser ) )
            {
                return this.mUsers.Remove( pUser );
            }

            return false;
        }

        /// <summary>
        /// Adds an output varying variable that must be recorded in transform feedback mode.
        /// The order they are registered is important as they are recorded in the very same order.
        /// </summary>
        /// <param name="pVaryingName">The varying variable name to record</param>
        public void AddFeedbackVarying(string pVaryingName)
        {
            if ( this.mFeedbackVaryings.Contains( pVaryingName ) == false )
            {
                this.mFeedbackVaryings.Add( pVaryingName );
            }
        }

        /// <summary>
        /// Adds an initial value for the given variable.
        /// </summary>
        /// <param name="pValue">The initial value.</param>
        public void AddInitialValue(IPipelineValue pValue)
        {
            SubroutineValue lSubroutine = pValue as SubroutineValue;
            if ( lSubroutine != null )
            {
                switch ( lSubroutine.Stage )
                {
                    case StageType.VERTEX:
                        {
                            StringBuilder lBuilder = new StringBuilder( "VERTEX" );
                            lBuilder.Append( " " );
                            lBuilder.Append( lSubroutine.Name );
                            this.mInitialValues.Add( lBuilder.ToString(), lSubroutine );
                        }
                        break;
                    case StageType.TESSELATION_CONTROL:
                        {
                            StringBuilder lBuilder = new StringBuilder( "TESS_CONTROL" );
                            lBuilder.Append( " " );
                            lBuilder.Append( lSubroutine.Name );
                            this.mInitialValues.Add( lBuilder.ToString(), lSubroutine );
                        }
                        break;
                    case StageType.TESSELATION_EVALUATION:
                        {
                            StringBuilder lBuilder = new StringBuilder( "TESS_EVAL" );
                            lBuilder.Append( " " );
                            lBuilder.Append( lSubroutine.Name );
                            this.mInitialValues.Add( lBuilder.ToString(), lSubroutine );
                        }
                        break;
                    case StageType.GEOMETRY:
                        {
                            StringBuilder lBuilder = new StringBuilder( "GEOMETRY" );
                            lBuilder.Append( " " );
                            lBuilder.Append( lSubroutine.Name );
                            this.mInitialValues.Add( lBuilder.ToString(), lSubroutine );
                        }
                        break;
                    case StageType.FRAGMENT:
                        {
                            StringBuilder lBuilder = new StringBuilder( "FRAGMENT" );
                            lBuilder.Append( " " );
                            lBuilder.Append( lSubroutine.Name );
                            this.mInitialValues.Add( lBuilder.ToString(), lSubroutine );
                        }
                        break;
                }
            }
            else
            {
                this.mInitialValues.Add( pValue.Name, pValue );
            }
        }

        /// <summary>
        /// Swaps the pipeline description with another one.
        /// </summary>
        /// <param name="pOther">The other pipeline description to swap with.</param>
        public void Swap(PipelineDescription pOther)
        {
            for ( StageType lStage = StageType.VERTEX; 
                            lStage < StageType.COUNT; 
                            lStage++ )
            {
                PipelineStage lThisStage;
                PipelineStage lOtherStage;
                if ( this.mStages.TryGetValue( lStage, out lThisStage ) &&
                     pOther.mStages.TryGetValue( lStage, out lOtherStage ) )
                {
                    lThisStage.Swap( lOtherStage );
                }
            }

            Utilities.Swap( ref this.mInitialValues, ref pOther.mInitialValues );
        }

        #region Methods StageCreator

        /// <summary>
        /// Creates a vertex pipeline stage from the given source if the corresponding code is detected.
        /// </summary>
        /// <param name="pVersion">The shader version to use.</param>
        /// <param name="pSource">The overall source code.</param>
        /// <param name="pStage">The resulting vertex pipeline stage, Null if nothing found in the source.</param>
        private static bool CreateVertexStage(int pVersion, string pSource, out PipelineStage pStage)
        {
            // Null by default.
            pStage = null;

            // If anything about a vertex stage in the source?
            if ( pSource.Contains( cVertexIdentifier ) )
            {
                StringBuilder lHeaderBuilder = new StringBuilder( cHeaderPrefix );
                lHeaderBuilder.Append( " " );
                lHeaderBuilder.Append( cVertexIdentifier );
                lHeaderBuilder.AppendLine();
                
                pStage = new PipelineStage( pVersion, 
                                            pSource, // Despite the source having more than the vertex code only, the above #define make sure it will be the only one used for that stage, so GPU compilation should skip the rest cause unused.
                                            StageType.VERTEX, 
                                            lHeaderBuilder.ToString() );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Creates a tesselation control pipeline stage from the given source if the corresponding code is detected.
        /// </summary>
        /// <param name="pVersion">The shader version to use.</param>
        /// <param name="pSource">The overall source code.</param>
        /// <param name="pStage">The resulting tesselation control pipeline stage, Null if nothing found in the source.</param>
        private static bool CreateTesselationControlStage(int pVersion, string pSource, out PipelineStage pStage)
        {
            // Null by default.
            pStage = null;

            // If anything about a tesselation control stage in the source?
            if ( pSource.Contains( cTessControlIdentifier ) )
            {
                StringBuilder lHeaderBuilder = new StringBuilder( cHeaderPrefix );
                lHeaderBuilder.Append( " " );
                lHeaderBuilder.Append( cTessControlIdentifier );
                lHeaderBuilder.AppendLine();
                
                pStage = new PipelineStage( pVersion, 
                                            pSource, // Despite the source having more than the tesselation control code only, the above #define make sure it will be the only one used for that stage, so GPU compilation should skip the rest cause unused.
                                            StageType.TESSELATION_CONTROL, 
                                            lHeaderBuilder.ToString() );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Creates a tesselation evaluation pipeline stage from the given source if the corresponding code is detected.
        /// </summary>
        /// <param name="pVersion">The shader version to use.</param>
        /// <param name="pSource">The overall source code.</param>
        /// <param name="pStage">The resulting evaluation control pipeline stage, Null if nothing found in the source.</param>
        private static bool CreateTesselationEvaluationStage(int pVersion, string pSource, out PipelineStage pStage)
        {
            // Null by default.
            pStage = null;

            // If anything about a tesselation evaluation stage in the source?
            if ( pSource.Contains( cTessEvaluationIdentifier ) )
            {
                StringBuilder lHeaderBuilder = new StringBuilder( cHeaderPrefix );
                lHeaderBuilder.Append( " " );
                lHeaderBuilder.Append( cTessEvaluationIdentifier );
                lHeaderBuilder.AppendLine();
                
                pStage = new PipelineStage( pVersion, 
                                            pSource, // Despite the source having more than the tesselation evaluation code only, the above #define make sure it will be the only one used for that stage, so GPU compilation should skip the rest cause unused.
                                            StageType.TESSELATION_EVALUATION, 
                                            lHeaderBuilder.ToString() );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Creates a geometry pipeline stage from the given source if the corresponding code is detected.
        /// </summary>
        /// <param name="pVersion">The shader version to use.</param>
        /// <param name="pSource">The overall source code.</param>
        /// <param name="pStage">The resulting geometry pipeline stage, Null if nothing found in the source.</param>
        private static bool CreateGeometryStage(int pVersion, string pSource, out PipelineStage pStage)
        {
            // Null by default.
            pStage = null;

            // If anything about a geometry stage in the source?
            if ( pSource.Contains( cGeometryIdentifier ) )
            {
                StringBuilder lHeaderBuilder = new StringBuilder( cHeaderPrefix );
                lHeaderBuilder.Append( " " );
                lHeaderBuilder.Append( cGeometryIdentifier );
                lHeaderBuilder.AppendLine();
                
                pStage = new PipelineStage( pVersion, 
                                            pSource, // Despite the source having more than the geometry code only, the above #define make sure it will be the only one used for that stage, so GPU compilation should skip the rest cause unused.
                                            StageType.GEOMETRY, 
                                            lHeaderBuilder.ToString() );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Creates a fragment pipeline stage from the given source if the corresponding code is detected.
        /// </summary>
        /// <param name="pVersion">The shader version to use.</param>
        /// <param name="pSource">The overall source code.</param>
        /// <param name="pStage">The resulting fragment pipeline stage, Null if nothing found in the source.</param>
        private static bool CreateFragmentStage(int pVersion, string pSource, out PipelineStage pStage)
        {
            // Null by default.
            pStage = null;

            // If anything about a fragment stage in the source?
            if ( pSource.Contains( cFragmentIdentifier ) )
            {
                StringBuilder lHeaderBuilder = new StringBuilder( cHeaderPrefix );
                lHeaderBuilder.Append( " " );
                lHeaderBuilder.Append( cFragmentIdentifier );
                lHeaderBuilder.AppendLine();
                
                pStage = new PipelineStage( pVersion, 
                                            pSource, // Despite the source having more than the fragment code only, the above #define make sure it will be the only one used for that stage, so GPU compilation should skip the rest cause unused.
                                            StageType.FRAGMENT, 
                                            lHeaderBuilder.ToString() );

                return true;
            }

            return false;
        }

        #endregion Methods StageCreator

        #region Methods Internal

        /// <summary>
        /// Initializes the pipeline description with the given stage(s).
        /// </summary>
        /// <param name="pStages">The pipeline stage(s).</param>
        private void Initialize(PipelineStage[] pStages)
        {
            this.mIsValid = true; // Valid until opposite proof.
            this.mFeedbackMode = PipelineVaryingFeedback.ANY;
            this.mVersion = int.MinValue;
            this.mUsers   = new List<PipelinePass>();
            this.mStages  = new Dictionary<StageType, PipelineStage>();
            List<StageType> lDefinedStages = new List<StageType>();
            foreach ( PipelineStage lStage in pStages )
            {
                if ( lDefinedStages.Contains( lStage.Type ) == false )
                {
                    // Keep the highest required GLSL version.
                    this.mVersion = System.Math.Max( lStage.Version, this.mVersion );
                    this.mStages.Add( lStage.Type, lStage );
                    lStage.AddUser( this );
                    lDefinedStages.Add( lStage.Type );

                    if ( lStage.IsValid == false )
                    {
                        this.mIsValid = false;
                    }
                }
                else
                {
                    LogManager.Instance.Log( string.Format( "Attempting to define a pipeline with more than one {0} stage...", lStage.Type ), LogType.WARNING );
                }
            }

            this.mFeedbackVaryings = new List<string>();
            this.mInitialValues = new Dictionary<string, IPipelineValue>();
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            foreach ( PipelineStage lStage in this.mStages.Values )
            {
                lStage.RemoveUser( this );
                if ( lStage.HasUsers )
                {
                    lStage.Dispose();
                }
            }
            this.mStages.Clear();
            this.mInitialValues.Clear();
            this.mFeedbackVaryings.Clear();
            this.mUsers.Clear();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
