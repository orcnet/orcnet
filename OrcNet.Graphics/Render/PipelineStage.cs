﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Graphics.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// GPU pipeline stage class definition which can define a Vertex, a Tesselation Control,
    /// a Tesselation Evaluation, a Geometry or a Fragment stage.
    /// It is called a shader as well.
    /// </summary>
    [DebuggerDisplay("Id = {StageId}, Type = {Type}, Version = {Version}, IsValid = {IsValid}, UserCount = {mUsers.Count}")]
    public class PipelineStage : AMemoryProfilable, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether that stage is valid or not.
        /// </summary>
        private bool      mIsValid;

        /// <summary>
        /// Stores the GLSL version.
        /// </summary>
        private int       mVersion;

        /// <summary>
        /// Stores the stage type.
        /// </summary>
        private StageType mType;

        /// <summary>
        /// Stores the stage Id.
        /// </summary>
        private int       mStageId;

        /// <summary>
        /// Stores the pipeline definition using that stage.
        /// </summary>
        private List<PipelineDescription> mUsers;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = sizeof(StageType);
                foreach
                    ( PipelineDescription lUser in this.mUsers )
                {
                    lSize += lUser.Size;
                }
                lSize += 2 * sizeof(int);
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the flag indicating whether the stage is still used or not.
        /// </summary>
        public bool HasUsers
        {
            get
            {
                return this.mUsers.Count > 0;
            }
        }

        /// <summary>
        /// Gets the stage's pipeline description user(s).
        /// </summary>
        public IEnumerable<PipelineDescription> Users
        {
            get
            {
                return this.mUsers;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether that stage is valid or not.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.mIsValid;
            }
        }

        /// <summary>
        /// Gets the stage GLSL version.
        /// </summary>
        public int Version
        {
            get
            {
                return this.mVersion;
            }
        }

        /// <summary>
        /// Gets the stage identifier.
        /// </summary>
        public int StageId
        {
            get
            {
                return this.mStageId;
            }
        }

        /// <summary>
        /// Gets the stage type.
        /// </summary>
        public StageType Type
        {
            get
            {
                return this.mType;
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineStage"/> class.
        /// </summary>
        /// <param name="pVersion">The OpenGL GLSL version </param>
        /// <param name="pSourceCode">The stage source code to compile.</param>
        /// <param name="pType">The stage type.</param>
        /// <param name="pHeader">The GLSL shader header if any.</param>
        public PipelineStage(int pVersion, string pSourceCode, StageType pType, string pHeader = null)
        {
            int lMaxVersionSupported;
            GL.GetInteger( GetPName.MajorVersion, out lMaxVersionSupported );

            this.mIsValid = true;
            this.mVersion = pVersion;
            this.mType  = pType;
            this.mUsers = new List<PipelineDescription>();

            if ( pSourceCode != null )
            {
                int lElementCount = 2;
                if ( pHeader != null )
                {
                    lElementCount = 3;
                }

                int[] lLengths = new int[ lElementCount ];
                string[] lSourceArray = new string[ lElementCount ];
                lSourceArray[ 0 ] = string.Format( "#version {0}\n", this.mVersion );
                lLengths[ 0 ] = lSourceArray[ 0 ].Length;
                if ( lElementCount == 3 )
                {
                    lSourceArray[ 1 ] = pHeader;
                    lLengths[ 1 ] = pHeader.Length;
                    lSourceArray[ 2 ] = pSourceCode;
                    lLengths[ 2 ] = pSourceCode.Length;
                }
                else
                {
                    lSourceArray[ 1 ] = pSourceCode;
                    lLengths[ 1 ] = pSourceCode.Length;
                }

                this.mStageId = -1;
                switch ( this.mType )
                {
                    case StageType.VERTEX:
                        {
                            this.mStageId = GL.CreateShader( ShaderType.VertexShader );
                        }
                        break;
                    case StageType.TESSELATION_CONTROL:
                        {
                            if ( lMaxVersionSupported >= 4 )
                            {
                                this.mStageId = GL.CreateShader( ShaderType.TessControlShader );
                            }
                        }
                        break;
                    case StageType.TESSELATION_EVALUATION:
                        {
                            if ( lMaxVersionSupported >= 4 )
                            {
                                this.mStageId = GL.CreateShader( ShaderType.TessEvaluationShader );
                            }
                        }
                        break;
                    case StageType.GEOMETRY:
                        {
                            this.mStageId = GL.CreateShader( ShaderType.GeometryShader );
                        }
                        break;
                    case StageType.FRAGMENT:
                        {
                            this.mStageId = GL.CreateShader( ShaderType.FragmentShader );
                        }
                        break;
                }

                GLHelpers.GetError();

                if ( this.mStageId != -1 )
                {
                    GL.ShaderSource( this.mStageId, lElementCount, lSourceArray, lLengths );
                    GL.CompileShader( this.mStageId );

                    if ( GLHelpers.IsStageCompilationSucceeded( this.mStageId ) )
                    {
                        GLHelpers.LogStageCompilationFeedbacks( this.mStageId, false );

                        LogManager.Instance.Log( string.Format( "Compilation succeeded of the {0} stage!!!", this.mType ), LogType.INFO );
                    }
                    else
                    {
                        GLHelpers.LogStageCompilationFeedbacks( this.mStageId, true );

                        // Releases compilated resources if failure.
                        GL.DeleteShader( this.mStageId );
                        this.mStageId = -1;
                        this.mIsValid = false;
                    }
                }
                else
                {
                    this.mIsValid = false;
                }

                GLHelpers.GetError();
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a pipeline stage to compose a pipeline definition
        /// </summary>
        /// <param name="pDefinition">The definition that will use the stage.</param>
        public void AddUser(PipelineDescription pDefinition)
        {
            if
                ( pDefinition == null )
            {
                return;
            }

            if
                ( this.mUsers.Contains( pDefinition ) == false )
            {
                this.mUsers.Add( pDefinition );
            }
        }

        /// <summary>
        /// Removes a pipeline stage which composed a pipelinbe definition.
        /// </summary>
        /// <param name="pDefinition">The definition that was using the stage.</param>
        /// <returns>True if removed, false if not found.</returns>
        public bool RemoveUser(PipelineDescription pDefinition)
        {
            if
                ( pDefinition == null )
            {
                return false;
            }

            if
                ( this.mUsers.Contains( pDefinition ) )
            {
                return this.mUsers.Remove( pDefinition );
            }

            return false;
        }

        /// <summary>
        /// Swaps the pipeline stage 
        /// </summary>
        /// <param name="pOther">The other pipeline stage to swap with.</param>
        public void Swap(PipelineStage pOther)
        {
           Utilities.Swap( ref this.mStageId, ref pOther.mStageId );
        }

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            if
                ( this.mStageId != -1 )
            {
                GL.DeleteShader( this.mStageId );
                this.mStageId = -1;
                this.mIsValid = false;
            }

            this.mUsers.Clear();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
