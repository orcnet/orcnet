﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Render;
using OrcNet.Core.Resource;
using OrcNet.Core.Service;
using OrcNet.Graphics.Helpers;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Render.Uniforms.Values;
using OrcNet.Graphics.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// NOTE: Program :-D
    /// GPU pipeline pass class definition which can define a Vertex, Tesselation Control,
    /// Tesselation Evaluation, Geometry and Fragment stage. It could be made of one or more
    /// Pipeline definitions, each made of one or multiple stages.
    /// </summary>
    [DebuggerDisplay("PassId = {Id}, IsCurrent = {IsCurrent}, DescriptionCount = {DescriptionCount}, UniformCount = {mUniforms.Count}")]
    public class PipelinePass : AMemoryProfilable, IPipelinePass
    {
        #region Delegates

        /// <summary>
        /// Uniform creator delegate prototype definition.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private delegate void UniformCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform);

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the buffer has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the pipeline pass creator resource.
        /// </summary>
        private IResource mCreator;

        /// <summary>
        /// Stores the set of uniform creator delegates by GL types.
        /// </summary>
        private static Dictionary<int, UniformCreator> sUniformCreators;

        /// <summary>
        /// Stores the uniform blocks by name.
        /// </summary>
        private Dictionary<string, UniformBlock> mUniformBlocks;

        /// <summary>
        /// Stores the pipeline pass's uniforms whatever it is a uniform in a block or not
        /// including sampler and subroutine uniforms.
        /// </summary>
        private Dictionary<string, IPipelineUniform> mUniforms;

        /// <summary>
        /// Stores the old pipeline pass's uniforms that were present
        /// </summary>
        private Dictionary<string, IPipelineUniform> mOldUniforms;

        /// <summary>
        /// Stores especially the texture uniforms of this pass.
        /// </summary>
        private List<TextureUniform>      mTextureUniforms;

        /// <summary>
        /// Stores the set of pipeline descriptions for that pass.
        /// </summary>
        private List<PipelineDescription> mDescriptions;

        /// <summary>
        /// Stores the set of pipeline pass(es) providing a stage for that pass.
        /// </summary>
        private List<PipelinePass> mSharedPasses;

        /// <summary>
        /// Stores the set of pass identifier providing shader stages for that pass.
        /// NOTE: This pass identifier is in the set as well.
        /// </summary>
        private List<uint> mPassIds;

        /// <summary>
        /// Stores the pipeline stages identifier provided by the external pipeline passes.
        /// </summary>
        private List<uint>  mSharedStageIds;

        /// <summary>
        /// Stores the pipeline pass identifier.
        /// </summary>
        private uint mPassId;

        /// <summary>
        /// Stores the pipeline identifier (if applicable)
        /// </summary>
        private uint mPipelineId;
        
        /// <summary>
        /// Stores the uniform subroutines OpenGL Ids of that pass.
        /// </summary>
        private Dictionary<StageType, uint[]> mSubroutineUniformIds;

        /// <summary>
        /// Stores the bitfield indicating which stage has any subroutine uniform that has changed.
        /// </summary>
        private uint  mDirtySubroutineStageFlag;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IPipelinePass

        /// <summary>
        /// Gets or sets the flag indicating whether the pass is the current being used or not.
        /// </summary>
        public bool IsCurrent
        {
            get
            {
                IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                if
                    ( lRenderService != null )
                {
                    if
                        ( lRenderService.CurrentPass == null )
                    {
                        return false;
                    }

                    // If that pass itself is the current one?
                    if
                        ( this == lRenderService.CurrentPass )
                    {
                        return true;
                    }

                    // Or is one of the shared passes constituting the current one?
                    foreach
                        ( PipelinePass lSharedPass in lRenderService.CurrentPass.mSharedPasses )
                    {
                        if
                            ( this == lSharedPass )
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the GL Id whatever it is using a single program using a pass id or multiple programs
        /// using a pipeline Id.
        /// See OpenGL.
        /// </summary>
        public uint Id
        {
            get
            {
                return this.mPassId > 0 ? this.mPassId : this.mPipelineId;
            }
        }

        /// <summary>
        /// Gets the pipeline description at the given index.
        /// </summary>
        /// <param name="pIndex">The description index.</param>
        /// <returns></returns>
        IPipelineDescription IPipelinePass.this[PipelineDescriptionIndex pIndex]
        {
            get
            {
                return this[ pIndex ];
            }
        }

        #endregion Properties IPipelinePass

        #region Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        public IResource Creator
        {
            get
            {
                return this.mCreator;
            }
            set
            {
                this.mCreator = value;
            }
        }

        #endregion Properties IResourceCreatable

        /// <summary>
        /// Gets the pipeline identifier (if applicable)
        /// </summary>
        internal uint PipelineId
        {
            get
            {
                return this.mPipelineId;
            }
        }

        /// <summary>
        /// Gets the pipeline pass identifier.
        /// </summary>
        internal uint PassId
        {
            get
            {
                return this.mPassId;
            }
        }

        /// <summary>
        /// Gets the set of pass identifier providing shader stages for that pass.
        /// NOTE: This pass identifier is in the set as well.
        /// </summary>
        public IEnumerable<uint> PassIds
        {
            get
            {
                return this.mPassIds;
            }
        }

        /// <summary>
        /// Gets the pipeline description count.
        /// </summary>
        public int DescriptionCount
        {
            get
            {
                return this.mDescriptions.Count;
            }
        }

        /// <summary>
        /// Gets the pipeline description at the given index.
        /// </summary>
        /// <param name="pIndex">The description index.</param>
        /// <returns></returns>
        public PipelineDescription this[PipelineDescriptionIndex pIndex]
        {
            get
            {
                if
                    ( pIndex < 0 || 
                      pIndex >= this.mDescriptions.Count )
                {
                    return null;
                }

                return this.mDescriptions[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the uniform block given its name.
        /// </summary>
        /// <param name="pName">The uniform block's name.</param>
        /// <returns>The uniform block if any matching the given name, false otherwise.</returns>
        public UniformBlock this[UniformBlockName pName]
        {
            get
            {
                UniformBlock lBlock;
                if
                    ( this.mUniformBlocks.TryGetValue( pName, out lBlock ) )
                {
                    return lBlock;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets all the pass's uniforms.
        /// </summary>
        public IEnumerable<IPipelineUniform> Uniforms
        {
            get
            {
                // TO DO: Check if must be thread safe and/or return a copy of values...
                return this.mUniforms.Values;
            }
        }

        /// <summary>
        /// Gets the uniform corresponding to the given name.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        /// <returns>The uniform corresponding to the supplied name, null otherwise.</returns>
        public IPipelineUniform this[UniformName pName]
        {
            get
            {
                IPipelineUniform lUniform;
                if
                    ( this.mUniforms.TryGetValue( pName, out lUniform ) )
                {
                    return lUniform;
                }

                return null;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes the static member(s) of the <see cref="PipelinePass"/> class.
        /// </summary>
        static PipelinePass()
        {
            sUniformCreators = new Dictionary<int, UniformCreator>();
            sUniformCreators.Add( (int)All.Float,       Uniform1fCreator );
            sUniformCreators.Add( (int)All.FloatVec2,   Uniform2fCreator );
            sUniformCreators.Add( (int)All.FloatVec3,   Uniform3fCreator );
            sUniformCreators.Add( (int)All.FloatVec4,   Uniform4fCreator );
            sUniformCreators.Add( (int)All.Double,      Uniform1dCreator );
            sUniformCreators.Add( (int)All.DoubleVec2,  Uniform2dCreator );
            sUniformCreators.Add( (int)All.DoubleVec3,  Uniform3dCreator );
            sUniformCreators.Add( (int)All.DoubleVec4,  Uniform4dCreator );
            sUniformCreators.Add( (int)All.Int,         Uniform1iCreator );
            sUniformCreators.Add( (int)All.IntVec2,     Uniform2iCreator );
            sUniformCreators.Add( (int)All.IntVec3,     Uniform3iCreator );
            sUniformCreators.Add( (int)All.IntVec4,     Uniform4iCreator );
            sUniformCreators.Add( (int)All.Bool,        Uniform1bCreator );
            sUniformCreators.Add( (int)All.BoolVec2,    Uniform2bCreator );
            sUniformCreators.Add( (int)All.BoolVec3,    Uniform3bCreator );
            sUniformCreators.Add( (int)All.BoolVec4,    Uniform4bCreator );
            sUniformCreators.Add( (int)All.UnsignedInt, Uniform1uiCreator );
            sUniformCreators.Add( (int)All.UnsignedIntVec2, Uniform1uiCreator );
            sUniformCreators.Add( (int)All.UnsignedIntVec3, Uniform1uiCreator );
            sUniformCreators.Add( (int)All.UnsignedIntVec4, Uniform1uiCreator );
            sUniformCreators.Add( (int)All.FloatMat2,   UniformMatrix2fCreator );
            sUniformCreators.Add( (int)All.FloatMat2x3, UniformMatrix2x3fCreator );
            sUniformCreators.Add( (int)All.FloatMat2x4, UniformMatrix2x4fCreator );
            sUniformCreators.Add( (int)All.FloatMat3,   UniformMatrix3fCreator );
            sUniformCreators.Add( (int)All.FloatMat3x2, UniformMatrix3x2fCreator );
            sUniformCreators.Add( (int)All.FloatMat3x4, UniformMatrix3x4fCreator );
            sUniformCreators.Add( (int)All.FloatMat4,   UniformMatrix4fCreator );
            sUniformCreators.Add( (int)All.FloatMat4x2, UniformMatrix4x2fCreator );
            sUniformCreators.Add( (int)All.FloatMat4x3, UniformMatrix4x3fCreator );
            sUniformCreators.Add( (int)All.DoubleMat2,   UniformMatrix2dCreator );
            sUniformCreators.Add( (int)All.DoubleMat2x3, UniformMatrix2x3dCreator );
            sUniformCreators.Add( (int)All.DoubleMat2x4, UniformMatrix2x4dCreator );
            sUniformCreators.Add( (int)All.DoubleMat3,   UniformMatrix3dCreator );
            sUniformCreators.Add( (int)All.DoubleMat3x2, UniformMatrix3x2dCreator );
            sUniformCreators.Add( (int)All.DoubleMat3x4, UniformMatrix3x4dCreator );
            sUniformCreators.Add( (int)All.DoubleMat4,   UniformMatrix4dCreator );
            sUniformCreators.Add( (int)All.DoubleMat4x2, UniformMatrix4x2dCreator );
            sUniformCreators.Add( (int)All.DoubleMat4x3, UniformMatrix4x3dCreator );
            sUniformCreators.Add( (int)All.Sampler1D,         UniformSampler1DCreator );
            sUniformCreators.Add( (int)All.Sampler1DShadow,   UniformSampler1DCreator );
            sUniformCreators.Add( (int)All.Sampler2D,         UniformSampler2DCreator );
            sUniformCreators.Add( (int)All.Sampler2DShadow,   UniformSampler2DCreator );
            sUniformCreators.Add( (int)All.Sampler3D,         UniformSampler3DCreator );
            sUniformCreators.Add( (int)All.SamplerCube,       UniformSamplerCubeCreator );
            sUniformCreators.Add( (int)All.SamplerCubeShadow, UniformSamplerCubeCreator );
            sUniformCreators.Add( (int)All.Sampler1DArray,            UniformSampler1DArrayCreator );
            sUniformCreators.Add( (int)All.Sampler1DArrayShadow,      UniformSampler1DArrayCreator );
            sUniformCreators.Add( (int)All.Sampler2DArray,            UniformSampler2DArrayCreator );
            sUniformCreators.Add( (int)All.Sampler2DArrayShadow,      UniformSampler2DArrayCreator );
            sUniformCreators.Add( (int)All.SamplerCubeMapArray,       UniformSamplerCubeArrayCreator );
            sUniformCreators.Add( (int)All.SamplerCubeMapArrayShadow, UniformSamplerCubeArrayCreator );
            sUniformCreators.Add( (int)All.Sampler2DMultisample,      UniformSampler2DMultiSampleCreator );
            sUniformCreators.Add( (int)All.Sampler2DMultisampleArray, UniformSampler2DMultiSampleArrayCreator );
            sUniformCreators.Add( (int)All.SamplerBuffer,             UniformSamplerBufferCreator );
            sUniformCreators.Add( (int)All.Sampler2DRect,             UniformSampler2DRectCreator );
            sUniformCreators.Add( (int)All.Sampler2DRectShadow,       UniformSampler2DRectCreator );
            sUniformCreators.Add( (int)All.IntSampler1D,     UniformSamplerINT1DCreator );
            sUniformCreators.Add( (int)All.IntSampler2D,     UniformSamplerINT2DCreator );
            sUniformCreators.Add( (int)All.IntSampler3D,     UniformSamplerINT3DCreator );
            sUniformCreators.Add( (int)All.IntSamplerCube,   UniformSamplerINTCubeCreator );
            sUniformCreators.Add( (int)All.IntSampler1DArray,      UniformSamplerINT1DArrayCreator );
            sUniformCreators.Add( (int)All.IntSampler2DArray,      UniformSamplerINT2DArrayCreator );
            sUniformCreators.Add( (int)All.IntSamplerCubeMapArray, UniformSamplerINTCubeArrayCreator );
            sUniformCreators.Add( (int)All.IntSampler2DMultisample,      UniformSamplerINT2DMultiSampleCreator );
            sUniformCreators.Add( (int)All.IntSampler2DMultisampleArray, UniformSamplerINT2DMultiSampleArrayCreator );
            sUniformCreators.Add( (int)All.IntSamplerBuffer,             UniformSamplerINTBufferCreator );
            sUniformCreators.Add( (int)All.IntSampler2DRect,             UniformSamplerINT2DRectCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSampler1D,   UniformSamplerUINT1DCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSampler2D,   UniformSamplerUINT2DCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSampler3D,   UniformSamplerUINT3DCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSamplerCube, UniformSamplerUINTCubeCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSampler1DArray,      UniformSamplerUINT1DArrayCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSampler2DArray,      UniformSamplerUINT2DArrayCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSamplerCubeMapArray, UniformSamplerUINTCubeArrayCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSampler2DMultisample,      UniformSamplerUINT2DMultiSampleCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSampler2DMultisampleArray, UniformSamplerUINT2DMultiSampleArrayCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSamplerBuffer,             UniformSamplerUINTBufferCreator );
            sUniformCreators.Add( (int)All.UnsignedIntSampler2DRect,             UniformSamplerUINT2DRectCreator );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelinePass"/> class.
        /// </summary>
        protected PipelinePass()
        {
            this.mPassIds = new List<uint>();
            this.mDescriptions = new List<PipelineDescription>();
            this.mSubroutineUniformIds = new Dictionary<StageType, uint[]>();
            this.mUniformBlocks = new Dictionary<string, UniformBlock>();
            this.mUniforms = new Dictionary<string, IPipelineUniform>();
            this.mOldUniforms = new Dictionary<string, IPipelineUniform>();
            this.mTextureUniforms = new List<TextureUniform>();
            this.mSharedPasses = new List<PipelinePass>();
            this.mSharedStageIds = new List<uint>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelinePass"/> class.
        /// </summary>
        /// <param name="pDescription">The pipeline pass description</param>
        /// <param name="pUseSeparately">The flag indicating whether the description must separately use pipeline stages shaders or not.</param>
        public PipelinePass(PipelineDescription pDescription, bool pUseSeparately = false) :
        this()
        {
            List<PipelineDescription> lDescriptions = new List<PipelineDescription>() { pDescription };
            this.Initialize( lDescriptions, pUseSeparately );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelinePass"/> class.
        /// </summary>
        /// <param name="pDescriptions">The pipeline pass descriptions</param>
        /// <param name="pUseSeparately">The flag indicating whether the description must separately use pipeline stages shaders or not.</param>
        public PipelinePass(IEnumerable<PipelineDescription> pDescriptions, bool pUseSeparately = false) :
        this()
        {
            this.Initialize( pDescriptions, pUseSeparately );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelinePass"/> class.
        /// </summary>
        /// <param name="pData">The compiled pass data</param>
        /// <param name="pGLFormat">The pass data GL format</param>
        /// <param name="pUseSeparately">The flag indicating whether the description must separately use pipeline stages shaders or not.</param>
        public PipelinePass(byte[] pData, uint pGLFormat, bool pUseSeparately = false) :
        this()
        {
            this.Initialize( pData, pGLFormat, pUseSeparately );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelinePass"/> class.
        /// </summary>
        /// <param name="pVertexStage">The pipeline pass owning the vertex stage to use.</param>
        /// <param name="pTesselationControlStage">The pipeline pass owning the tesselation control stage to use.</param>
        /// <param name="pTesselationEvaluationStage">The pipeline pass owning the tesselation evaluation stage to use.</param>
        /// <param name="pGeometryStage">The pipeline pass owning the geometry stage to use.</param>
        /// <param name="pFragmentStage">The pipeline pass owning the fragment stage to use.</param>
        public PipelinePass(PipelinePass pVertexStage, PipelinePass pTesselationControlStage, PipelinePass pTesselationEvaluationStage, PipelinePass pGeometryStage, PipelinePass pFragmentStage) :
        this()
        {
            this.mPassId = 0;
            GL.GenProgramPipelines( 1, out this.mPipelineId );

            if ( this.mPipelineId > 0 )
            {
                if ( pVertexStage != null )
                {
                    this.Initialize( ShaderType.VertexShader, pVertexStage );
                    GL.UseProgramStages( this.mPipelineId, ProgramStageMask.VertexShaderBit, pVertexStage.mPassId );
                }
                if ( pTesselationControlStage != null )
                {
                    this.Initialize( ShaderType.TessControlShader, pTesselationControlStage );
                    GL.UseProgramStages( this.mPipelineId, ProgramStageMask.TessControlShaderBit, pTesselationControlStage.mPassId );
                }
                if ( pTesselationEvaluationStage != null )
                {
                    this.Initialize( ShaderType.TessEvaluationShader, pTesselationEvaluationStage );
                    GL.UseProgramStages( this.mPipelineId, ProgramStageMask.TessEvaluationShaderBit, pTesselationEvaluationStage.mPassId );
                }
                if ( pGeometryStage != null )
                {
                    this.Initialize( ShaderType.GeometryShader, pGeometryStage );
                    GL.UseProgramStages( this.mPipelineId, ProgramStageMask.GeometryShaderBit, pGeometryStage.mPassId );
                }
                if ( pFragmentStage != null )
                {
                    this.Initialize( ShaderType.FragmentShader, pFragmentStage );
                    GL.UseProgramStages( this.mPipelineId, ProgramStageMask.FragmentShaderBit, pFragmentStage.mPassId );
                }
            }
            else
            {
                LogManager.Instance.Log( "Unable to create a program pipeline for a pass having shared stage(s)!!!", LogType.ERROR );
            }
            
            this.mDirtySubroutineStageFlag = 0;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Refreshes the pass's subroutine of a given pass's stage.
        /// </summary>
        /// <param name="pStage">The modified stage</param>
        /// <param name="pLocation">The subroutine location</param>
        /// <param name="pSubroutineId">The subroutine OpenGL id.</param>
        public void RefreshSubroutine(StageType pStage, int pLocation, uint pSubroutineId)
        {
            uint lToShift = 1;
            this.mSubroutineUniformIds[ pStage ][ pLocation ] = pSubroutineId;
            this.mDirtySubroutineStageFlag |= (lToShift << (int)pStage);
        }

        /// <summary>
        /// Gets the compiled overall pass program.
        /// </summary>
        /// <returns>The compiled pass program data in bytes.</returns>
        public byte[] GetCompiledData()
        {
            if ( this.mPassId == 0 )
            {
                return null;
            }

            int lProgramLength = 3000; // For now 3000 bytes max, that is, 24 K bits.
            BinaryFormat lFormat;
            // Missing OpenTK enum value, so just attempt to get the program with a constant buffer size.
            //GL.GetProgram( this.mPassId, (GetProgramParameterName)0x8741 , out lProgramLength );
            byte[] lTemp = new byte[ lProgramLength ];
            GL.GetProgramBinary( this.mPassId, lProgramLength, out lProgramLength, out lFormat, lTemp );
            if ( lProgramLength < lTemp.Count() )
            {
                byte[] lData = new byte[ lProgramLength ]; // Correct size returned
                Array.Copy( lTemp, lData, lProgramLength );
                return lData;
            }
            else
            {
                return lTemp;
            }
        }

        /// <summary>
        /// Gets the uniform corresponding to the given name.
        /// </summary>
        /// <typeparam name="T">The uniform type.</typeparam>
        /// <param name="pName">The uniform name.</param>
        /// <returns>The uniform corresponding to the supplied name, null otherwise.</returns>
        public T GetUniform<T>(UniformName pName) where T : class, IPipelineUniform
        {
            return this[ pName ] as T;
        }

        /// <summary>
        /// Gets the uniform subroutine corresponding to the given name in the given stage.
        /// </summary>
        /// <param name="pStage">The stage the subroutine is in.</param>
        /// <param name="pName">The uniform name.</param>
        /// <returns>The subroutine, null otherwise.</returns>
        public SubroutineUniform GetSubroutineUniform(StageType pStage, UniformName pName)
        {
            string lName;
            switch ( pStage )
            {
                case StageType.VERTEX:
                    {
                        StringBuilder lBuilder = new StringBuilder( "VERTEX" );
                        lBuilder.Append( " " );
                        lBuilder.Append( pName );
                        lName = lBuilder.ToString();
                    }
                    break;
                case StageType.TESSELATION_CONTROL:
                    {
                        StringBuilder lBuilder = new StringBuilder( "TESS_CONTROL" );
                        lBuilder.Append( " " );
                        lBuilder.Append( pName );
                        lName = lBuilder.ToString();
                    }
                    break;
                case StageType.TESSELATION_EVALUATION:
                    {
                        StringBuilder lBuilder = new StringBuilder( "TESS_EVAL" );
                        lBuilder.Append( " " );
                        lBuilder.Append( pName );
                        lName = lBuilder.ToString();
                    }
                    break;
                case StageType.GEOMETRY:
                    {
                        StringBuilder lBuilder = new StringBuilder( "GEOMETRY" );
                        lBuilder.Append( " " );
                        lBuilder.Append( pName );
                        lName = lBuilder.ToString();
                    }
                    break;
                case StageType.FRAGMENT:
                    {
                        StringBuilder lBuilder = new StringBuilder( "FRAGMENT" );
                        lBuilder.Append( " " );
                        lBuilder.Append( pName );
                        lName = lBuilder.ToString();
                    }
                    break;
                default:
                    return null;
            }

            return this.GetUniform<SubroutineUniform>( new UniformName( lName ) );
        }

        #region Methods Internal

        /// <summary>
        /// Prepares the pipeline pass for being used as rendering pass.
        /// </summary>
        internal void Prepare()
        {
            if ( this.mPipelineId == 0 )
            {
                this.BindTexturesAndBlocks();
            }
            else
            {
                foreach ( PipelinePass lSharedPass in this.mSharedPasses )
                {
                    lSharedPass.BindTexturesAndBlocks();
                }
            }
        }

        /// <summary>
        /// Updates the pipeline pass's uniforms.
        /// </summary>
        internal void UpdateUniforms()
        {
            if ( this.mPipelineId == 0 )
            {
                this.UpdateDirtyUniforms( 0xFFFFFFFF );
            }
            else
            {
                int lCounter = 0;
                foreach ( PipelinePass lSharedPass in this.mSharedPasses )
                {
                    lSharedPass.UpdateDirtyUniforms( this.mSharedStageIds[ lCounter ] );
                    lCounter++;
                }
            }
        }

        /// <summary>
        /// Swaps that pipeline pass with another.
        /// </summary>
        /// <param name="pOtherPass">The other pass that will overwrite that pass.</param>
        internal virtual void Swap(PipelinePass pOtherPass)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null &&
                 lRenderService.CurrentPass == this )
            {
                lRenderService.UsePass( null );
            }

            this.UpdateTexturesUsers( false );
            pOtherPass.UpdateTexturesUsers( false );

            this.UpdateBlocks( false );
            pOtherPass.UpdateBlocks( false );

            Utilities.Swap( ref this.mDescriptions, ref pOtherPass.mDescriptions );
            Utilities.Swap( ref this.mPassId, ref pOtherPass.mPassId );
            Utilities.Swap( ref this.mPipelineId, ref pOtherPass.mPipelineId );
            Utilities.Swap( ref this.mPassIds, ref pOtherPass.mPassIds );
            Utilities.Swap( ref this.mSharedPasses, ref pOtherPass.mSharedPasses );
            Utilities.Swap( ref this.mSharedStageIds, ref pOtherPass.mSharedStageIds );
            Utilities.Swap( ref this.mUniforms, ref pOtherPass.mUniforms );
            Utilities.Swap( ref this.mUniformBlocks, ref pOtherPass.mUniformBlocks );
            Utilities.Swap( ref this.mSubroutineUniformIds, ref pOtherPass.mSubroutineUniformIds );
            
            foreach ( KeyValuePair<string, IPipelineUniform> lPair in pOtherPass.mUniforms.ToList() )
            {
                IPipelineUniform lThisUniform;
                if ( this.mUniforms.TryGetValue( lPair.Key, out lThisUniform ) )
                {
                    if ( lPair.Value.GetType() == lThisUniform.GetType() )
                    {
                        this.mUniforms.Swap( pOtherPass.mUniforms, lPair.Key, lPair.Key );
                        //Utilities.Swap( ref lPair.Value, ref lThisUniform );
                        int lTemp = lPair.Value.Location; // Make sure the stage location remains the same for both.
                        lPair.Value.Location = lThisUniform.Location;
                        lThisUniform.Location = lTemp;
                        lPair.Value.Owner = this;
#if (ORK_NO_GLPROGRAMUNIFORM)
                        lPair.Value.IsDirty = true;
#else
                        lPair.Value.SendUniform();
#endif
                    }
                }
                else
                {
                    // No longer an uniform of that pass, but keep it in the case it becomes
                    // one of its uniforms back later on.
                    this.mOldUniforms[ lPair.Key ] = lPair.Value;
                }
            }

            foreach ( KeyValuePair<string, IPipelineUniform> lPair in this.mOldUniforms.ToList() )
            {
                IPipelineUniform lThisUniform;
                if ( this.mUniforms.TryGetValue( lPair.Key, out lThisUniform ) )
                {
                    if ( lPair.Value != lThisUniform &&
                         lPair.Value.GetType() == lThisUniform.GetType() )
                    {
                        this.mUniforms.Swap( this.mOldUniforms, lPair.Key, lPair.Key );
                        //Utilities.Swap( ref lPair.Value, ref lThisUniform );
                        int lTemp = lPair.Value.Location; // Make sure the stage location remains the same for both.
                        lPair.Value.Location  = lThisUniform.Location;
                        lThisUniform.Location = lTemp;
                        lPair.Value.Owner = this;
#if (ORK_NO_GLPROGRAMUNIFORM)
                        lPair.Value.IsDirty = true;
#else
                        lPair.Value.SendUniform();
#endif
                    }
                    this.mOldUniforms.Remove( lPair.Key );
                }
            }

            foreach ( KeyValuePair<string, UniformBlock> lPair in this.mUniformBlocks.ToList() )
            {
                UniformBlock lOtherBlock;
                if ( pOtherPass.mUniformBlocks.TryGetValue( lPair.Key, out lOtherBlock ) )
                {
                    this.mUniformBlocks.Swap( pOtherPass.mUniformBlocks, lPair.Key, lPair.Key );
                    //Utilities.Swap( ref lPair.Key, ref lOtherBlock );
                }
            }

            if ( this.mSubroutineUniformIds.Any() &&
                 pOtherPass.mSubroutineUniformIds.Any() )
            {
                for ( StageType lStage = StageType.VERTEX; lStage < StageType.COUNT; lStage++ )
                {
                    uint[] lThisSubroutineIds;
                    uint[] lOtherSubroutineIds;
                    if ( this.mSubroutineUniformIds.TryGetValue( lStage, out lThisSubroutineIds ) &&
                         pOtherPass.mSubroutineUniformIds.TryGetValue( lStage, out lOtherSubroutineIds ) )
                    {
                        if
                            ( lThisSubroutineIds.Length == lOtherSubroutineIds.Length )
                        {
                            this.mSubroutineUniformIds.Swap( pOtherPass.mSubroutineUniformIds, lStage, lStage );
                            //lThisSubroutineIds.Swap( lOtherSubroutineIds );
                        }
                    }
                }
            }

            this.ModifyUniformsOwner( this );
            pOtherPass.ModifyUniformsOwner( pOtherPass );

            this.mDirtySubroutineStageFlag = 0;
            pOtherPass.mDirtySubroutineStageFlag = 0;
            for ( StageType lStage = StageType.VERTEX; lStage < StageType.COUNT; lStage++ )
            {
                uint lToShift = 1;
                uint[] lThisSubroutineIds;
                if ( this.mSubroutineUniformIds.TryGetValue( lStage, out lThisSubroutineIds ) )
                {
                    this.mDirtySubroutineStageFlag |= (lToShift << (int)lStage);
                }

                uint[] lOtherSubroutineIds;
                if ( pOtherPass.mSubroutineUniformIds.TryGetValue( lStage, out lOtherSubroutineIds ) )
                {
                    pOtherPass.mDirtySubroutineStageFlag |= (lToShift << (int)lStage);
                }
            }

            this.UpdateTexturesUsers( true );
            pOtherPass.UpdateTexturesUsers( true );
        }

        /// <summary>
        /// Initializes the pipeline pass with the given pass stage.
        /// </summary>
        /// <param name="pStageName">The stage to use from the supplied pass.</param>
        /// <param name="pPass">The pass offering the stage to use.</param>
        private void Initialize(ShaderType pStageName, PipelinePass pPass)
        {
            if ( this.mPassId > 0 )
            {
                uint lToShift = 1;
                int lStageId  = (int)pStageName;
                int lCounter  = 0;
                foreach ( PipelinePass lPass in this.mSharedPasses )
                {
                    if ( lPass == pPass )
                    {
                        this.mSharedStageIds[ lCounter ] |= (lToShift << lStageId);
                        return;
                    }
                    lCounter++;
                }

                this.mPassIds.Add( pPass.mPassId );
                this.mSharedPasses.Add( pPass );
                this.mSharedStageIds.Add( lToShift << lStageId );
            }
            else
            {
                LogManager.Instance.Log( "Unable to create a new GL program!!!", LogType.ERROR );
                return;
            }
        }

        /// <summary>
        /// Initializes the pipeline pass with the given pipeline description(s)
        /// </summary>
        /// <param name="pDescriptions">The pipeline description(s)</param>
        /// <param name="pUseSeparately">The flag indicating whether the description must separately use pipeline stages shaders or not.</param>
        private void Initialize(IEnumerable<PipelineDescription> pDescriptions, bool pUseSeparately)
        {
            this.mPipelineId   = 0;
            this.mDescriptions = new List<PipelineDescription>( pDescriptions );

            // Creates the program.
            int lId = GL.CreateProgram();
            if ( lId > 0 )
            {
                this.mPassId = (uint)lId;
            }
            else
            {
                LogManager.Instance.Log( "Unable to create a new GL program!!!", LogType.ERROR );
                return;
            }

            this.mPassIds = new List<uint>();
            this.mPassIds.Add( this.mPassId );

            bool lIsInterleaved = false;
            List<string> lFlattenedVaryings = new List<string>();
            // Attach all the shader object(s) to the pass.
            foreach ( PipelineDescription lDescription in this.mDescriptions )
            {
                lDescription.AddUser( this );
                foreach ( PipelineStage lStage in lDescription.Stages )
                {
                    if ( lStage.IsValid )
                    {
                        GL.AttachShader( this.mPassId, (uint)lStage.StageId );
                    }
                }
                lFlattenedVaryings.AddRange( lDescription.Varyings );
                if ( lDescription.FeedbackMode == PipelineVaryingFeedback.INTERLEAVED_ATTRIBUTES )
                {
                    lIsInterleaved = true;
                }
            }

            // Initializes the transform feedback varying value(s).
            if ( lFlattenedVaryings.Any() )
            {
                GL.TransformFeedbackVaryings( this.mPassId, lFlattenedVaryings.Count, lFlattenedVaryings.ToArray(), lIsInterleaved ? TransformFeedbackMode.InterleavedAttribs : TransformFeedbackMode.SeparateAttribs );
            }
            lFlattenedVaryings.Clear();

            // Links everything together??
            if ( pUseSeparately )
            {
                GL.ProgramParameter( this.mPassId, ProgramParameterName.ProgramSeparable, 1 );
            }
            GL.LinkProgram( this.mPassId );

            this.InitializeUniforms();
        }

        /// <summary>
        /// Initializes the pipeline pass with the given compiled representation.
        /// </summary>
        /// <param name="pData">The compiled pass data</param>
        /// <param name="pGLFormat">The pass data GL format</param>
        /// <param name="pUseSeparately">The flag indicating whether the description must separately use pipeline stages shaders or not.</param>
        private void Initialize(byte[] pData, uint pGLFormat, bool pUseSeparately)
        {
            this.mPipelineId   = 0;

            // Creates the program.
            int lId = GL.CreateProgram();
            if ( lId > 0 )
            {
                this.mPassId = (uint)lId;
            }
            else
            {
                LogManager.Instance.Log( "Unable to create a new GL program!!!", LogType.ERROR );
                return;
            }
            
            this.mPassIds.Add( this.mPassId );

            if ( pUseSeparately )
            {
                GL.ProgramParameter( this.mPassId, ProgramParameterName.ProgramSeparable, 1 );
            }

            GL.ProgramBinary( this.mPassId, (BinaryFormat)pGLFormat, pData, pData.Length );

            this.InitializeUniforms();
        }

        /// <summary>
        /// Initializes the pass uniforms.
        /// </summary>
        private void InitializeUniforms()
        {
            int lLinkStatus;
            GL.GetProgram( this.mPassId, GetProgramParameterName.LinkStatus, out lLinkStatus );
            if ( lLinkStatus == 0 )
            {
                // Any link error??
                int lLogLength;
                GL.GetProgram( this.mPassId, GetProgramParameterName.InfoLogLength, out lLogLength );
                if ( lLogLength > 0 )
                {
                    string lInfo;
                    GL.GetProgramInfoLog( (int)this.mPassId, out lInfo );
                    LogManager.Instance.Log( "Program linking info: " + lInfo, LogType.ERROR );
                }
                GL.DeleteProgram( this.mPassId );
                this.mPassId = 0;

                LogManager.Instance.Log( "Program linking error!!!", LogType.ERROR );
            }

            int lMaxNameLength;
            int lMaxLength;
            GL.GetProgram( this.mPassId, GetProgramParameterName.ActiveUniformMaxLength, out lMaxNameLength );
            GL.GetProgram( this.mPassId, GetProgramParameterName.ActiveUniformBlockMaxNameLength, out lMaxLength );
            lMaxNameLength = System.Math.Max( lMaxNameLength, lMaxLength );
            if ( GLHelpers.MajorVersion() >= 4 )
            {
                for ( StageType lStage = StageType.VERTEX; lStage < StageType.COUNT; lStage++ )
                {
                    ShaderType lType = lStage.ToShaderType();
                    GL.GetProgramStage( this.mPassId, lType, ProgramStageParameter.ActiveSubroutineUniformMaxLength, out lMaxLength );
                    lMaxNameLength = System.Math.Max( lMaxNameLength, lMaxLength );
                    GL.GetProgramStage( this.mPassId, lType, ProgramStageParameter.ActiveSubroutineMaxLength, out lMaxLength );
                    lMaxNameLength = System.Math.Max( lMaxNameLength, lMaxLength );
                }
            }

            int lUniformCount;
            GL.GetProgram( this.mPassId, GetProgramParameterName.ActiveUniforms, out lUniformCount );
            
            for ( uint lCurr = 0; lCurr < lUniformCount; lCurr++ )
            {
                int lLength;
                int lOffset;
                int lUniformType;
                int lUniformSize;
                int lBlockIndex;
                StringBuilder lBuffer = new StringBuilder();
                GL.GetActiveUniformName( this.mPassId, lCurr, lMaxNameLength, out lLength, lBuffer );
                GL.GetActiveUniforms( this.mPassId, 1, ref lCurr, ActiveUniformParameter.UniformType, out lUniformType );
                GL.GetActiveUniforms( this.mPassId, 1, ref lCurr, ActiveUniformParameter.UniformSize, out lUniformSize );
                GL.GetActiveUniforms( this.mPassId, 1, ref lCurr, ActiveUniformParameter.UniformBlockIndex, out lBlockIndex );
                if ( lBlockIndex == -1 )
                {
                    lOffset = GL.GetUniformLocation( this.mPassId, lBuffer.ToString() );
                }
                else
                {
                    GL.GetActiveUniforms( this.mPassId, 1, ref lCurr, ActiveUniformParameter.UniformOffset, out lOffset );
                }

                int lArrayStride;
                int lMatrixStride;
                int lIsRowMajor;
                GL.GetActiveUniforms( this.mPassId, 1, ref lCurr, ActiveUniformParameter.UniformArrayStride, out lArrayStride );
                GL.GetActiveUniforms( this.mPassId, 1, ref lCurr, ActiveUniformParameter.UniformMatrixStride, out lMatrixStride );
                GL.GetActiveUniforms( this.mPassId, 1, ref lCurr, ActiveUniformParameter.UniformIsRowMajor, out lIsRowMajor );

                string lName = lBuffer.ToString();
                if ( lUniformSize > 1 )
                {
                    int lIndex = lName.IndexOf( '[' );
                    if ( lIndex > 0 )
                    {
                        lName = lName.Substring( 0, lIndex );
                    }
                }

                UniformBlock lBlock = null;
                if ( lBlockIndex != -1 )
                {
                    uint lUnsignedIndex = (uint)lBlockIndex;
                    GL.GetActiveUniformBlockName( this.mPassId, lUnsignedIndex, lMaxNameLength, out lLength, lBuffer );
                    string lBlockName = lBuffer.ToString();
                    if ( this.mUniformBlocks.TryGetValue( lBlockName, out lBlock ) == false )
                    {
                        int lBlockSize;
                        GL.GetActiveUniformBlock( this.mPassId, lUnsignedIndex, ActiveUniformBlockParameter.UniformBlockDataSize, out lBlockSize );
                        lBlock = new UniformBlock( this, lBlockName, lUnsignedIndex, (uint)lBlockSize );
                        this.mUniformBlocks.Add( lBlockName, lBlock );
                    }
                }

                for ( int lCurrUniform = 0; lCurrUniform < lUniformSize; lCurrUniform++ )
                {
                    string lUniformName;
                    IPipelineUniform lUniform = null;
                    if ( lUniformSize == 1 )
                    {
                        lUniformName = lName;
                    }
                    else
                    {
                        StringBuilder lNameBuilder = new StringBuilder();
                        lNameBuilder.Append( lName + "[" + lCurrUniform.ToString() + "]" );
                        lUniformName = lNameBuilder.ToString();
                    }

                    int lUniformOffset;
                    if ( lBlock == null )
                    {
                        lUniformOffset = GL.GetUniformLocation( this.mPassId, lUniformName );
                    }
                    else
                    {
                        lUniformOffset = lOffset + lCurrUniform * lArrayStride;
                    }

                    UniformCreator lCreator;
                    if ( sUniformCreators.TryGetValue( lUniformType, out lCreator ) )
                    {
                        lCreator( this, lBlock, lUniformName, lUniformOffset, lMatrixStride, lIsRowMajor == 0 ? false : true, out lUniform );
                        this.mUniforms.Add( lUniformName, lUniform );
                        if ( lBlock != null )
                        {
                            lBlock.AddUniform( lUniform );
                        }

                        if ( lUniform is TextureUniform )
                        {
                            this.mTextureUniforms.Add( lUniform as TextureUniform );
                        }
                    }
                    else
                    {
                        LogManager.Instance.Log( string.Format( "Uniform type {0} not supported!!!", lUniformType ), LogType.WARNING );
                    }
                }
            }

            this.mSubroutineUniformIds = new Dictionary<StageType, uint[]>();
            this.mDirtySubroutineStageFlag = 0;

            if ( GLHelpers.MajorVersion() >= 4 )
            {
                for ( StageType lStage = StageType.VERTEX; lStage < StageType.COUNT; lStage++ )
                {
                    ShaderType lType = lStage.ToShaderType();
                    int lActiveSubroutineCount;
                    GL.GetProgramStage( this.mPassId, lType, ProgramStageParameter.ActiveSubroutineUniforms, out lActiveSubroutineCount );
                    for ( uint lCurr = 0; lCurr < lActiveSubroutineCount; lCurr++ )
                    {
                        int lSize;
                        int lLength;
                        StringBuilder lBuilder = new StringBuilder();
                        GL.GetActiveSubroutineUniform( this.mPassId, lType, lCurr, ActiveSubroutineUniformParameter.UniformSize, out lSize );
                        GL.GetActiveSubroutineUniformName( this.mPassId, lType, lCurr, lMaxLength, out lLength, lBuilder );
                        string lUniformName = lBuilder.ToString();
                        if ( lSize > 1 )
                        {
                            int lIndex = lUniformName.IndexOf( '[' );
                            if ( lIndex > 0 )
                            {
                                lUniformName = lUniformName.Substring( 0, lIndex );
                            }
                        }

                        for ( int lCurrUniform = 0; lCurrUniform < lSize; lCurrUniform++ )
                        {
                            string lSubName = lUniformName;
                            if ( lSize > 1 )
                            {
                                StringBuilder lSubNameBuilder = new StringBuilder();
                                lSubNameBuilder.Append( lUniformName );
                                lSubNameBuilder.Append( "[" );
                                lSubNameBuilder.Append( lCurrUniform.ToString() );
                                lSubNameBuilder.Append( "]" );
                                lSubName = lSubNameBuilder.ToString();
                            }

                            int lSubroutineUniformCount;
                            int lSubroutineLocation = GL.GetSubroutineUniformLocation( this.mPassId, lType, lSubName );
                            GL.GetActiveSubroutineUniform( this.mPassId, lType, lCurr, ActiveSubroutineUniformParameter.NumCompatibleSubroutines, out lSubroutineUniformCount );
                            int[] lIndices = new int[ lSubroutineUniformCount ];
                            GL.GetActiveSubroutineUniform( this.mPassId, lType, lCurr, ActiveSubroutineUniformParameter.CompatibleSubroutines, lIndices );
                            List<string> lSubroutineNames = new List<string>();
                            List<int> lSubroutineIndices  = new List<int>();
                            for ( int lCurrSubroutine = 0; lCurrSubroutine < lSubroutineUniformCount; lCurrSubroutine++ )
                            {
                                StringBuilder lSubroutineNameBuilder = new StringBuilder();
                                GL.GetActiveSubroutineName( this.mPassId, lType, (uint)lIndices[ lCurrSubroutine ], lMaxLength, out lLength, lSubroutineNameBuilder );
                                lSubroutineNames.Add( lSubroutineNameBuilder.ToString() );
                                lSubroutineIndices.Add( lIndices[ lCurrSubroutine ] );
                            }

                            SubroutineUniform lUniform = new SubroutineUniform( this, lSubName, lSubroutineLocation, lStage, lSubroutineNames, lSubroutineIndices );
                            switch ( lStage )
                            {
                                case StageType.VERTEX:
                                    {
                                        StringBuilder lSubBuilder = new StringBuilder( "VERTEX" );
                                        lSubBuilder.Append( " " );
                                        lSubBuilder.Append( lSubName );
                                        this.mUniforms.Add( lSubBuilder.ToString(), lUniform );
                                    }
                                    break;
                                case StageType.TESSELATION_CONTROL:
                                    {
                                        StringBuilder lSubBuilder = new StringBuilder( "TESS_CONTROL" );
                                        lSubBuilder.Append( " " );
                                        lSubBuilder.Append( lSubName );
                                        this.mUniforms.Add( lSubBuilder.ToString(), lUniform );
                                    }
                                    break;
                                case StageType.TESSELATION_EVALUATION:
                                    {
                                        StringBuilder lSubBuilder = new StringBuilder( "TESS_EVAL" );
                                        lSubBuilder.Append( " " );
                                        lSubBuilder.Append( lSubName );
                                        this.mUniforms.Add( lSubBuilder.ToString(), lUniform );
                                    }
                                    break;
                                case StageType.GEOMETRY:
                                    {
                                        StringBuilder lSubBuilder = new StringBuilder( "GEOMETRY" );
                                        lSubBuilder.Append( " " );
                                        lSubBuilder.Append( lSubName );
                                        this.mUniforms.Add( lSubBuilder.ToString(), lUniform );
                                    }
                                    break;
                                case StageType.FRAGMENT:
                                    {
                                        StringBuilder lSubBuilder = new StringBuilder( "FRAGMENT" );
                                        lSubBuilder.Append( " " );
                                        lSubBuilder.Append( lSubName );
                                        this.mUniforms.Add( lSubBuilder.ToString(), lUniform );
                                    }
                                    break;
                            }
                        }
                    }

                    int lLocationCount;
                    GL.GetProgramStage( this.mPassId, lType, ProgramStageParameter.ActiveSubroutineUniformLocations, out lLocationCount );
                    if ( lLocationCount > 0 )
                    {
                        this.mSubroutineUniformIds[ lStage ] = new uint[ lLocationCount ];
                    }
                }
            }

            // Finds GPU buffer suitable for the blocks used in this program.
            List<string> lNewBlocks = new List<string>();
            foreach ( UniformBlock lBlock in this.mUniformBlocks.Values )
            {
                StringBuilder lBlockBufferNameBuilder = new StringBuilder();
                lBlockBufferNameBuilder.Append( lBlock.Name );
                lBlockBufferNameBuilder.Append( "-" );
                lBlockBufferNameBuilder.Append( lBlock.TotalSize );
                lBlockBufferNameBuilder.Append( "-" );
                lBlockBufferNameBuilder.Append( lBlock.UniformCount.ToString() ); // E.g: Deformation-8-32. looks the opposite but we never know...
                GPUBuffer lBlockBuffer = UniformBlock.GetOrCreate( lBlockBufferNameBuilder.ToString() );
                if ( lBlockBuffer.BufferSize == 0 )
                {
                    lBlockBuffer.SetData( (int)lBlock.TotalSize, IntPtr.Zero, BufferUsageHint.DynamicDraw );
                    lNewBlocks.Add( lBlock.Name );
                }
                lBlock.Buffer = lBlockBuffer;
            }

            // Sets the initial values of the uniforms.
            foreach ( PipelineDescription lDescription in this.mDescriptions )
            {
                foreach ( KeyValuePair<string, IPipelineValue> lInitialValue in lDescription.InitialValues )
                {
                    IPipelineUniform lUniform = null;
                    this.mUniforms.TryGetValue( lInitialValue.Key, out lUniform );

                    if ( lUniform != null &&
                         lUniform.Block != null &&
                         lNewBlocks.Contains( lUniform.Block.Name ) == false )
                    {
                        // Do not set initial values for uniforms in already existing
                        // uniform blocks, to avoid overriding the values of their uniforms
                        lUniform = null;
                    }

                    // Set the uniform initial value.
                    if ( lUniform != null )
                    {
                        // Both name must matches
                        if ( lInitialValue.Value.Name == lUniform.Name )
                        {
                            TextureValue lTextureValue = lInitialValue.Value as TextureValue;
                            TextureUniform lTextureUniform = lUniform as TextureUniform;
                            if ( lUniform.Type == lInitialValue.Value.Type ||
                                 ( lTextureValue != null && lTextureUniform != null ) )
                            {
                                lUniform.UntypedValue = lInitialValue.Value;
                            }
                        }
                        else
                        {
                            LogManager.Instance.Log( string.Format( "Attempting to initialize an uniform {0} with a value {1}. Both names must be equal!!!", lInitialValue.Value.Name, lUniform.Name ), LogType.ERROR );
                        }
                    }
                }
            }

            GLHelpers.GetError();
        }

        /// <summary>
        /// Creates a uniform1 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform1fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new FloatUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform1 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform1dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new DoubleUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform1 int based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform1iCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new IntUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform1 bool based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform1bCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new BoolUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform1 unsigned int based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform1uiCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new UIntUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform2 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform2fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Vector2FUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform2 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform2dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Vector2DUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform2 int based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform2iCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores ints as float due to missing OpenTK structure.
            pUniform = new Vector2IUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform2 bool based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform2bCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores the bools as float due to missing OpenTK structure.
            pUniform = new Vector2BUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform2 unsigned int based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform2uiCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores the uints as float due to missing OpenTK structure.
            pUniform = new Vector2UIUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform3 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform3fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Vector3FUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform3 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform3dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Vector3DUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform3 int based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform3iCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores ints as float due to missing OpenTK structure.
            pUniform = new Vector3IUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform3 bool based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform3bCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores the bools as float due to missing OpenTK structure.
            pUniform = new Vector3BUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform3 unsigned int based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform3uiCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores the uints as float due to missing OpenTK structure.
            pUniform = new Vector3UIUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform4 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform4fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Vector4FUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform4 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform4dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Vector4DUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform4 int based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform4iCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores ints as float due to missing OpenTK structure.
            pUniform = new Vector4IUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform4 bool based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform4bCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores the bools as float due to missing OpenTK structure.
            pUniform = new Vector4BUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniform4 unsigned int based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void Uniform4uiCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            // Stores the uints as float due to missing OpenTK structure.
            pUniform = new Vector4UIUniform( pOwner, pName, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a uniformMatrix2 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix2fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix2FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix2 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix2dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix2DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix2x3 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix2x3fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix2x3FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix2x3 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix2x3dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix2x3DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix2x4 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix2x4fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix2x4FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix2x4 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix2x4dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix2x4DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix3 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix3fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix3FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix3 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix3dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix3DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix3x2 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix3x2fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix3x2FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix3x2 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix3x2dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix3x2DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix3x4 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix3x4fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix3x4FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix3x4 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix3x4dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix3x4DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix4 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix4fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix4FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix4 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix4dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix4DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix4x2 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix4x2fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix4x2FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix4x2 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix4x2dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix4x2DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix4x3 float based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix4x3fCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix4x3FUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a uniformMatrix4x3 double based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformMatrix4x3dCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new Matrix4x3DUniform( pOwner, pName, pBlock, pLocation, pStride, pIsRowMajor );
        }

        /// <summary>
        /// Creates a texture uniform 1D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSampler1DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_1D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform 1D array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSampler1DArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_1D_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform 2D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSampler2DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_2D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform 2D array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSampler2DArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_2D_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform 3D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSampler3DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_3D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Cube based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerCubeCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_CUBE, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Cube map array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerCubeArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_CUBE_MAP_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform 2D multi sample based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSampler2DMultiSampleCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_2D_MULTISAMPLE, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform 2D multi sample array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSampler2DMultiSampleArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_2D_MULTISAMPLE_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform buffer based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerBufferCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_BUFFER, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform 2D rectangle based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSampler2DRectCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.SAMPLER_2D_RECT, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int 1D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINT1DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_1D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int 1D Array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINT1DArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_1D_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt 1D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINT1DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_1D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt 1D Array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINT1DArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_1D_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int 2D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINT2DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_2D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int 2D Array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINT2DArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_2D_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt 2D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINT2DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_2D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt 2D Array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINT2DArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_2D_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int 3D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINT3DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_3D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt 3D based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINT3DCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_3D, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int Cube based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINTCubeCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_CUBE, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int Cube Array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINTCubeArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_CUBE_MAP_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt Cube based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINTCubeCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_CUBE, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt Cube Array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINTCubeArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int 2D multi sample based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINT2DMultiSampleCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_2D_MULTISAMPLE, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int 2D multi sample array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINT2DMultiSampleArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_2D_MULTISAMPLE_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt 2D multi sample based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINT2DMultiSampleCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt 2D multi sample array based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINT2DMultiSampleArrayCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int buffer based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINTBufferCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_BUFFER, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform Int 2D rectangle based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerINT2DRectCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.INT_SAMPLER_2D_RECT, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt buffer based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINTBufferCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_BUFFER, pBlock, pLocation );
        }

        /// <summary>
        /// Creates a texture uniform UInt 2D rectangle based for that pass.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pBlock">The block in which the uniform will be if not null.</param>
        /// <param name="pName">THe uniform's name</param>
        /// <param name="pLocation">The uniform location or offset if in a block.</param>
        /// <param name="pStride">The stride to apply if needed.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the elements are ordered in row major for matrices.</param>
        /// <param name="pUniform">The resulting uniform.</param>
        private static void UniformSamplerUINT2DRectCreator(PipelinePass pOwner, UniformBlock pBlock, string pName, int pLocation, int pStride, bool pIsRowMajor, out IPipelineUniform pUniform)
        {
            pUniform = new TextureUniform( pOwner, pName, UniformType.UNSIGNED_INT_SAMPLER_2D_RECT, pBlock, pLocation );
        }

        /// <summary>
        /// Checks for each texture uniform that it is bound to a texture.
        /// </summary>
        private bool CheckTextures()
        {
            foreach ( TextureUniform lUniform in this.mTextureUniforms )
            {
                if ( lUniform.Location != -1 &&
                     lUniform.Value == null )
                {
                    LogManager.Instance.Log( string.Format( "Texture {0} not bound!!!", lUniform.Name ), LogType.ERROR );
                    return false;
                }
            }

            foreach ( PipelinePass lSharedPass in this.mSharedPasses )
            {
                if ( lSharedPass.CheckTextures() == false )
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Updates the dirty uniforms of dirty stages of that pass, that is, 
        /// the ones that have changed since the last time that pass has been used.
        /// </summary>
        /// <param name="pDirtyStagesFlag">The flag indicating which stage(s) is/are dirty or not.</param>
        private void UpdateDirtyUniforms(uint pDirtyStagesFlag)
        {
            // First unmaps block(s)
            foreach ( UniformBlock lBlock in this.mUniformBlocks.Values )
            {
                if ( lBlock.IsMapped )
                {
                    lBlock.UnmapBuffer();
                }
            }

#if (ORK_NO_GLPROGRAMUNIFORM)
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            foreach ( IPipelineUniform lUniform in this.mUniforms.Values )
            {
                if ( lUniform.IsDirty )
                {
                    if ( lRenderService != null &&
                         lRenderService.CurrentPass != null &&
                         lRenderService.CurrentPass.PipelineId > 0 )
                    {
                        GL.ActiveShaderProgram( lRenderService.CurrentPass.PipelineId, this.mPassId );
                    }
                    lUniform.SendUniform();
                    lUniform.IsDirty = false;
                }
            }
#endif
            if ( (this.mDirtySubroutineStageFlag & pDirtyStagesFlag) != 0 )
            {
                for ( StageType lStage = StageType.VERTEX; 
                                lStage <= StageType.COUNT; 
                                lStage = lStage + 1 )
                {
                    if ( ((this.mDirtySubroutineStageFlag & pDirtyStagesFlag) & (1 << (int)lStage)) != 0 )
                    {
                        uint[] lIndices = this.mSubroutineUniformIds[ lStage ];
                        switch ( lStage )
                        {
                            case StageType.VERTEX:
                                GL.UniformSubroutines( ShaderType.VertexShader, lIndices.Length, lIndices );
                                break;
                            case StageType.TESSELATION_CONTROL:
                                GL.UniformSubroutines( ShaderType.TessControlShader, lIndices.Length, lIndices );
                                break;
                            case StageType.TESSELATION_EVALUATION:
                                GL.UniformSubroutines( ShaderType.TessEvaluationShader, lIndices.Length, lIndices );
                                break;
                            case StageType.GEOMETRY:
                                GL.UniformSubroutines( ShaderType.GeometryShader, lIndices.Length, lIndices );
                                break;
                            case StageType.FRAGMENT:
                                GL.UniformSubroutines( ShaderType.FragmentShader, lIndices.Length, lIndices );
                                break;
                        }
                    }
                }
                this.mDirtySubroutineStageFlag &= ~pDirtyStagesFlag;
            }
        }

        /// <summary>
        /// Adds or removes from textures uniforms that pass as user.
        /// </summary>
        /// <param name="pAdd">Whether</param>
        private void UpdateTexturesUsers(bool pAdd)
        {
            foreach ( TextureUniform lTextureUniform in this.mTextureUniforms )
            {
                if ( lTextureUniform.Value != null )
                {
                    if ( lTextureUniform.Owner != null )
                    {
                        if ( pAdd )
                        {
                            lTextureUniform.Value.AddUser( lTextureUniform.Owner.Id );
                        }
                        else
                        {
                            lTextureUniform.Value.RemoveUser( lTextureUniform.Owner.Id );
                            lTextureUniform.UnitId = -1;
                        }
                    }
                    else
                    {
                        LogManager.Instance.Log( string.Format( "Texture uniform {0} has no owner !!!", lTextureUniform.Name ), LogType.WARNING );
                    }
                }
            }
        }

        /// <summary>
        /// Binds textures and uniform blocks.
        /// </summary>
        private void BindTexturesAndBlocks()
        {
            foreach ( TextureUniform lUniform in this.mTextureUniforms )
            {
                lUniform.SendUniform();
            }

            IEnumerable<uint> lPassIds = null;
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null &&
                 lRenderService.CurrentPass != null )
            {
                lPassIds = lRenderService.CurrentPass.PassIds;
            }
            else
            {
                lPassIds = new List<uint>();
            }

            foreach ( UniformBlock lBlock in this.mUniformBlocks.Values )
            {
                int lUnitId = lBlock.Buffer.BindToUniformUnit( lPassIds );
                if ( lUnitId >= 0 )
                {
                    GL.UniformBlockBinding( this.mPassId, lBlock.Index, (uint)lUnitId );
                }
                else
                {
                    LogManager.Instance.Log( string.Format( "Unable to bind the block {0} to an unit!!!", lBlock.Name ), LogType.ERROR );
                }
            }

            GLHelpers.GetError();
        }

        /// <summary>
        /// Modifies the uniforms owner of that pass by another.
        /// </summary>
        /// <param name="pNewOwner">The new uniforms owner.</param>
        private void ModifyUniformsOwner(PipelinePass pNewOwner)
        {
            this.mTextureUniforms.Clear();

            foreach ( IPipelineUniform lUniform in this.mUniforms.Values )
            {
                TextureUniform lTextureUniform = lUniform as TextureUniform;
                if ( lTextureUniform != null )
                {
                    this.mTextureUniforms.Add( lTextureUniform );
                }
                lUniform.Owner = pNewOwner;
            }

            foreach ( UniformBlock lBlock in this.mUniformBlocks.Values )
            {
                if ( lBlock.Buffer != null &&
                     lBlock.IsMapped )
                {
                    lBlock.UnmapBuffer();
                }

                if ( pNewOwner == null )
                {
                    // Invalidate the block if no new owner.
                    lBlock.Buffer = null;
                }

                lBlock.Owner = pNewOwner;

                foreach ( IPipelineUniform lUniform in lBlock.Uniforms )
                {
                    lUniform.Owner = pNewOwner;
                    lUniform.Block = lBlock; // Assures they be part of the proper block instance.
                }
            }

            if
                ( pNewOwner != null )
            {
                this.UpdateBlocks( true );
            }
        }

        /// <summary>
        /// Adds or removes from that pass's uniforms those ones being inside
        /// a block.
        /// </summary>
        /// <param name="pAdd">Whether the blocks uniforms must be flattened into that pass's uniforms set or not.</param>
        private void UpdateBlocks(bool pAdd)
        {
            if ( pAdd )
            {
                // Adds all blocks uniforms to that pass's uniforms set (to flatten them)
                foreach ( UniformBlock lBlock in this.mUniformBlocks.Values )
                {
                    foreach ( IPipelineUniform lUniform in lBlock.Uniforms )
                    {
                        this.mUniforms.Add( lUniform.Name, lUniform );
                    }
                }
            }
            else
            {
                foreach ( IPipelineUniform lUniform in this.mUniforms.Values.ToArray() )
                {
                    // If in a block, remove it from that pass's uniforms set.
                    if ( lUniform.Block != null )
                    {
                        this.mUniforms.Remove( lUniform.Name );
                    }
                }
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                if ( lRenderService.CurrentPass == this )
                {
                    lRenderService.UsePass( null );
                }
            }

            if ( this.mPassId != 0 )
            {
                this.UpdateTexturesUsers( false );
                this.ModifyUniformsOwner( null );
            }

            foreach ( PipelineDescription lDescription in this.mDescriptions )
            {
                lDescription.RemoveUser( this );
            }
            this.mDescriptions.Clear();

            this.mSubroutineUniformIds.Clear();

            if ( this.mPassId > 0 )
            {
                GL.DeleteProgram( this.mPassId );
            }

            if ( this.mPipelineId > 0 )
            {
                GL.DeleteProgramPipelines( 1, ref this.mPipelineId );
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
