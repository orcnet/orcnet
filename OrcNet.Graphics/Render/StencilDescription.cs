﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using System;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Stencil description class definition.
    /// If enabled, fragments are discarded or not based on the outcome
    /// of a comparison between the stencil buffer value at the fragment's 
    /// position and a reference value.
    /// Multiple test functions can be used and front faces are threated separatly
    /// from back faces.
    /// </summary>
    public class StencilDescription : AObservable, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the fragments are discarded or not based on the outcome
        /// of a comparison between the stencil buffer value at the fragment's position and a reference value.
        /// </summary>
        private bool mUseStencil;

        /// <summary>
        /// Stores the stencil reference used in comparison for front faces.
        /// </summary>
        private int  mFrontReference;

        /// <summary>
        /// Stores the stencil mask value to inform which part has to be taken into account in the comparison for front faces.
        /// </summary>
        private uint mFrontMask;

        /// <summary>
        /// Stores the stencil test function used for front faces.
        /// </summary>
        private StencilFunction mFrontFunction;

        /// <summary>
        /// Stores the action to perform if the stencil test fails for front faces.
        /// </summary>
        private StencilOp mFrontStencilFailAction;

        /// <summary>
        /// Stores the action to perform if the depth test fails for front faces.
        /// </summary>
        private StencilOp mFrontDepthFailAction;

        /// <summary>
        /// Stores the action to perform if the depth test passes for front faces.
        /// </summary>
        private StencilOp mFrontDepthPassAction;
        
        /// <summary>
        /// Stores the stencil reference used in comparison for back faces.
        /// </summary>
        private int  mBackReference;

        /// <summary>
        /// Stores the stencil mask value to inform which part has to be taken into account in the comparison for back faces.
        /// </summary>
        private uint mBackMask;

        /// <summary>
        /// Stores the stencil test function used for back faces.
        /// </summary>
        private StencilFunction mBackFunction;

        /// <summary>
        /// Stores the action to perform if the stencil test fails for back faces.
        /// </summary>
        private StencilOp mBackStencilFailAction;

        /// <summary>
        /// Stores the action to perform if the depth test fails for back faces.
        /// </summary>
        private StencilOp mBackDepthFailAction;

        /// <summary>
        /// Stores the action to perform if the depth test passes for back faces.
        /// </summary>
        private StencilOp mBackDepthPassAction;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the fragments are discarded or not based on the outcome
        /// of a comparison between the stencil buffer value at the fragment's position and a reference value.
        /// </summary>
        public bool UseStencil
        {
            get
            {
                return this.mUseStencil;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mUseStencil, value, "UseStencil" );
            }
        }

        /// <summary>
        /// Gets or sets the stencil reference used in comparison for front faces.
        /// </summary>
        public int FrontReference
        {
            get
            {
                return this.mFrontReference;
            }
        }

        /// <summary>
        /// Gets or sets the stencil mask value to inform which part has to be taken into account in the comparison for front faces.
        /// </summary>
        public uint FrontMask
        {
            get
            {
                return this.mFrontMask;
            }
        }

        /// <summary>
        /// Gets or sets the stencil test function used for front faces.
        /// </summary>
        public StencilFunction FrontFunction
        {
            get
            {
                return this.mFrontFunction;
            }
        }

        /// <summary>
        /// Gets or sets the action to perform if the stencil test fails for front faces.
        /// </summary>
        public StencilOp FrontStencilFailAction
        {
            get
            {
                return this.mFrontStencilFailAction;
            }
        }

        /// <summary>
        /// Gets or sets the action to perform if the depth test fails for front faces.
        /// </summary>
        public StencilOp FrontDepthFailAction
        {
            get
            {
                return this.mFrontDepthFailAction;
            }
        }

        /// <summary>
        /// Gets or sets the action to perform if the depth test passes for front faces.
        /// </summary>
        public StencilOp FrontDepthPassAction
        {
            get
            {
                return this.mFrontDepthPassAction;
            }
        }

        /// <summary>
        /// Gets or sets the stencil reference used in comparison for back faces.
        /// </summary>
        public int BackReference
        {
            get
            {
                return this.mBackReference;
            }
        }

        /// <summary>
        /// Gets or sets the stencil mask value to inform which part has to be taken into account in the comparison for back faces.
        /// </summary>
        public uint BackMask
        {
            get
            {
                return this.mBackMask;
            }
        }

        /// <summary>
        /// Gets or sets the stencil test function used for back faces.
        /// </summary>
        public StencilFunction BackFunction
        {
            get
            {
                return this.mBackFunction;
            }
        }

        /// <summary>
        /// Gets or sets the action to perform if the stencil test fails for back faces.
        /// </summary>
        public StencilOp BackStencilFailAction
        {
            get
            {
                return this.mBackStencilFailAction;
            }
        }

        /// <summary>
        /// Gets or sets the action to perform if the depth test fails for back faces.
        /// </summary>
        public StencilOp BackDepthFailAction
        {
            get
            {
                return this.mBackDepthFailAction;
            }
        }

        /// <summary>
        /// Gets or sets the action to perform if the depth test passes for back faces.
        /// </summary>
        public StencilOp BackDepthPassAction
        {
            get
            {
                return this.mBackDepthPassAction;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StencilDescription"/> class.
        /// </summary>
        /// <param name="pUseStencil">the flag indicating whether the fragments are discarded or not based on the outcome of a comparison between the stencil buffer value at the fragment's position and a reference value.</param>
        /// <param name="pFrontReference">the stencil reference used in comparison for front faces.</param>
        /// <param name="pFrontMask">the stencil mask value to inform which part has to be taken into account in the comparison for front faces.</param>
        /// <param name="pFrontFunction">the stencil test function used for front faces.</param>
        /// <param name="pFrontStencilFailAction">the action to perform if the stencil test fails for front faces.</param>
        /// <param name="pFrontDepthFailAction">the action to perform if the depth test fails for front faces.</param>
        /// <param name="pFrontDepthPassAction">the action to perform if the depth test passes for front faces.</param>
        /// <param name="pBackReference">the stencil reference used in comparison for back faces.</param>
        /// <param name="pBackMask">the stencil mask value to inform which part has to be taken into account in the comparison for back faces.</param>
        /// <param name="pBackFunction">the stencil test function used for back faces.</param>
        /// <param name="pBackStencilFailAction">the action to perform if the stencil test fails for back faces.</param>
        /// <param name="pBackDepthFailAction">the action to perform if the depth test fails for back faces.</param>
        /// <param name="pBackDepthPassAction">the action to perform if the depth test passes for back faces.</param>
        public StencilDescription(bool pUseStencil = false, int pFrontReference = 0, uint pFrontMask = 0xFFFFFFFF, StencilFunction pFrontFunction = StencilFunction.Always, StencilOp pFrontStencilFailAction = StencilOp.Keep, StencilOp pFrontDepthFailAction = StencilOp.Keep, StencilOp pFrontDepthPassAction = StencilOp.Keep,
                                                            int pBackReference = 0, uint pBackMask = 0xFFFFFFFF, StencilFunction pBackFunction = StencilFunction.Always, StencilOp pBackStencilFailAction = StencilOp.Keep, StencilOp pBackDepthFailAction = StencilOp.Keep, StencilOp pBackDepthPassAction = StencilOp.Keep)
        {
            this.mUseStencil = pUseStencil;
            this.mFrontReference = pFrontReference;
            this.mFrontMask = pFrontMask;
            this.mFrontFunction = pFrontFunction;
            this.mFrontStencilFailAction = pFrontStencilFailAction;
            this.mFrontDepthFailAction = pFrontDepthFailAction;
            this.mFrontDepthPassAction = pFrontDepthPassAction;
            this.mBackReference = pBackReference;
            this.mBackMask = pBackMask;
            this.mBackFunction = pBackFunction;
            this.mBackStencilFailAction = pBackStencilFailAction;
            this.mBackDepthFailAction = pBackDepthFailAction;
            this.mBackDepthPassAction = pBackDepthPassAction;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Enables or disables stencil tests.
        /// </summary>
        /// <param name="pEnable">the flag indicating whether the fragments are discarded or not based on the outcome of a comparison between the stencil buffer value at the fragment's position and a reference value.</param>
        /// <param name="pFunction">The front and back faces function</param>
        /// <param name="pReference">The front and back faces reference value</param>
        /// <param name="pMask">The front and back faces mask</param>
        /// <param name="pStencilFailure">The front and back faces operation to use on stencil test failure.</param>
        /// <param name="pDepthFailure">The front and back faces operation to use on depth test failure despite stencil success</param>
        /// <param name="pDepthPass">The front and back faces operation to use on stencil and depth tests success.</param>
        public void StencilTest(bool pEnable, StencilFunction pFunction, int pReference, uint pMask, StencilOp pStencilFailure, StencilOp pDepthFailure, StencilOp pDepthPass)
        {
            this.mUseStencil = pEnable;
            this.mFrontFunction = pFunction;
            this.mBackFunction  = pFunction;
            this.mFrontReference = pReference;
            this.mBackReference  = pReference;
            this.mFrontMask = pMask;
            this.mBackMask  = pMask;
            this.mFrontStencilFailAction = pStencilFailure;
            this.mBackStencilFailAction  = pStencilFailure;
            this.mFrontDepthFailAction = pDepthFailure;
            this.mBackDepthFailAction  = pDepthFailure;
            this.mFrontDepthPassAction = pDepthPass;
            this.mBackDepthPassAction  = pDepthPass;

            this.InformPropertyChangedListener( this.mUseStencil, this.mUseStencil, "UseStencil" );
        }

        /// <summary>
        /// Enables or disables stencil tests.
        /// </summary>
        /// <param name="pEnable">the flag indicating whether the fragments are discarded or not based on the outcome of a comparison between the stencil buffer value at the fragment's position and a reference value.</param>
        /// <param name="pFrontFunction">The front faces function</param>
        /// <param name="pFrontReference">The front faces reference value</param>
        /// <param name="pFrontMask">The front faces mask</param>
        /// <param name="pFrontStencilFailure">The front faces operation to use on stencil test failure.</param>
        /// <param name="pFrontDepthFailure">The front faces operation to use on depth test failure despite stencil success</param>
        /// <param name="pFrontDepthPass">The front faces operation to use on stencil and depth tests success.</param>
        /// <param name="pBackFunction">The back faces function</param>
        /// <param name="pBackReference">The back faces reference value</param>
        /// <param name="pBackMask">The back faces mask</param>
        /// <param name="pBackStencilFailure">The back faces operation to use on stencil test failure.</param>
        /// <param name="pBackDepthFailure">The back faces operation to use on depth test failure despite stencil success</param>
        /// <param name="pBackDepthPass">The back faces operation to use on stencil and depth tests success.</param>
        public void StencilTest(bool pEnable, StencilFunction pFrontFunction, int pFrontReference, uint pFrontMask, StencilOp pFrontStencilFailure, StencilOp pFrontDepthFailure, StencilOp pFrontDepthPass,
                                              StencilFunction pBackFunction, int pBackReference, uint pBackMask, StencilOp pBackStencilFailure, StencilOp pBackDepthFailure, StencilOp pBackDepthPass)
        {
            this.mUseStencil = pEnable;
            this.mFrontFunction = pFrontFunction;
            this.mBackFunction  = pBackFunction;
            this.mFrontReference = pFrontReference;
            this.mBackReference  = pBackReference;
            this.mFrontMask = pFrontMask;
            this.mBackMask  = pBackMask;
            this.mFrontStencilFailAction = pFrontStencilFailure;
            this.mBackStencilFailAction  = pBackStencilFailure;
            this.mFrontDepthFailAction = pFrontDepthFailure;
            this.mBackDepthFailAction  = pBackDepthFailure;
            this.mFrontDepthPassAction = pFrontDepthPass;
            this.mBackDepthPassAction  = pBackDepthPass;

            this.InformPropertyChangedListener( this.mUseStencil, this.mUseStencil, "UseStencil" );
        }

        #region Methods IDisposable

        /// <summary>
        /// Clone the description.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            StencilDescription lClone = new StencilDescription( this.mUseStencil, this.mFrontReference, this.mFrontMask, this.mFrontFunction, this.mFrontStencilFailAction, this.mFrontDepthFailAction, this.mFrontDepthPassAction,
                                                                this.mBackReference, this.mBackMask, this.mBackFunction, this.mBackStencilFailAction, this.mBackDepthFailAction, this.mBackDepthPassAction );
            return lClone;
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
