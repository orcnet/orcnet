﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Math;
using System;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Polygon description structure definition.
    /// </summary>
    public class PolygonDescription : AObservable, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the flags indicating whether fragment's depth will be offset after interpolation for,
        /// in order of appearence, the Points, Lines, and Polygons.
        /// </summary>
        private Tuple<bool, bool, bool> mPolygonOffsets;

        /// <summary>
        /// Stores the polygon offset info, that is, the scale factor on X for variable depth creation
        /// and the Y value is used to create a constant depth offset. See GL.PolygonOffset.
        /// </summary>
        private Vector2F    mPolygonOffset;

        /// <summary>
        /// Stores how front facing polygons will be rasterized.
        /// </summary>
        private PolygonMode mFrontMode;

        /// <summary>
        /// Stores how back facing polygons will be rasterized.
        /// </summary>
        private PolygonMode mBackMode;

        /// <summary>
        /// Stores the flag indicating whether the front faces orientation are clockwise or counter clockwise.
        /// </summary>
        private bool mAreFrontFacesCW;

        /// <summary>
        /// Stores the flag indicating whether the front polygons must be culled or not.
        /// </summary>
        private bool mCullFront;

        /// <summary>
        /// Stores the flag indicating whether the back polygons must be culled or not.
        /// </summary>
        private bool mCullBack;

        /// <summary>
        /// Stores the flag indicating whether the antialiasing must be used or not.
        /// </summary>
        private bool mSmoothesPolygons;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flags indicating whether fragment's depth will be offset after interpolation for,
        /// in order of appearence, the Points, Lines, and Polygons.
        /// </summary>
        public Tuple<bool, bool, bool> PolygonOffsets
        {
            get
            {
                return this.mPolygonOffsets;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mPolygonOffsets, value, "PolygonOffsets" );
            }
        }

        /// <summary>
        /// Gets or sets the polygon offset info, that is, the scale factor on X for variable depth creation
        /// and the Y value is used to create a constant depth offset. See GL.PolygonOffset.
        /// </summary>
        public Vector2F PolygonOffset
        {
            get
            {
                return this.mPolygonOffset;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mPolygonOffset, value, "PolygonOffset" );
            }
        }

        /// <summary>
        /// Gets or sets how front facing polygons will be rasterized.
        /// </summary>
        public PolygonMode FrontMode
        {
            get
            {
                return this.mFrontMode;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mFrontMode, value, "FrontMode" );
            }
        }

        /// <summary>
        /// Gets or sets how back facing polygons will be rasterized.
        /// </summary>
        public PolygonMode BackMode
        {
            get
            {
                return this.mBackMode;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mBackMode, value, "BackMode" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the front faces orientation are clockwise or counter clockwise.
        /// </summary>
        public bool AreFrontFacesCW
        {
            get
            {
                return this.mAreFrontFacesCW;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mAreFrontFacesCW, value, "AreFrontFacesCW" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the front polygons must be culled or not.
        /// </summary>
        public bool CullFront
        {
            get
            {
                return this.mCullFront;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCullFront, value, "CullFront" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the back polygons must be culled or not.
        /// </summary>
        public bool CullBack
        {
            get
            {
                return this.mCullBack;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCullBack, value, "CullBack" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the antialiasing must be used or not.
        /// </summary>
        public bool SmoothesPolygons
        {
            get
            {
                return this.mSmoothesPolygons;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mSmoothesPolygons, value, "SmoothesPolygons" );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PolygonDescription"/> class.
        /// </summary>
        /// <param name="pAreFrontFacesCW">the flag indicating whether the front faces orientation are clockwise or counter clockwise.</param>
        /// <param name="pFrontMode">how front facing polygons will be rasterized.</param>
        /// <param name="pBackMode">how back facing polygons will be rasterized.</param>
        /// <param name="pCullFront">the flag indicating whether the front polygons must be culled or not.</param>
        /// <param name="pCullBack">the flag indicating whether the back polygons must be culled or not.</param>
        /// <param name="pSmoothesPolygons">the flag indicating whether the antialiasing must be used or not.</param>
        /// <param name="pPolygonOffsetFactor">the polygon offset scale factor</param>
        /// <param name="pPolygonOffsetUnits">the polygon offset units</param>
        /// <param name="pOffsetPolygonPoint">the flag indicating whether to offset polygon points</param>
        /// <param name="pOffsetPolygonLine">the flag indicating whether to offset polygon lines</param>
        /// <param name="pOffsetPolygonFill">the flag indicating whether to offset polygon polygons</param>
        public PolygonDescription(bool pAreFrontFacesCW = false, PolygonMode pFrontMode = PolygonMode.Fill, PolygonMode pBackMode = PolygonMode.Fill, bool pCullFront = false, bool pCullBack = false, bool pSmoothesPolygons = false, float pPolygonOffsetFactor = 0.0f, float pPolygonOffsetUnits = 0.0f, bool pOffsetPolygonPoint = false, bool pOffsetPolygonLine = false, bool pOffsetPolygonFill = false)
        {
            this.mAreFrontFacesCW = pAreFrontFacesCW;
            this.mFrontMode = pFrontMode;
            this.mBackMode  = pBackMode;
            this.mCullFront = pCullFront;
            this.mCullBack  = pCullBack;
            this.mSmoothesPolygons = pSmoothesPolygons;
            this.mPolygonOffset  = new Vector2F( pPolygonOffsetFactor, pPolygonOffsetUnits );
            this.mPolygonOffsets = new Tuple<bool, bool, bool>( pOffsetPolygonPoint, pOffsetPolygonLine, pOffsetPolygonFill );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Clone the description.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            PolygonDescription lClone = new PolygonDescription( this.mAreFrontFacesCW, this.mFrontMode, this.mBackMode, this.mCullFront, this.mCullBack, 
                                                                this.mSmoothesPolygons, this.mPolygonOffset.X, this.mPolygonOffset.Y, 
                                                                this.mPolygonOffsets.Item1, this.mPolygonOffsets.Item2, this.mPolygonOffsets.Item3 );
            return lClone;
        }

        #endregion Methods
    }
}
