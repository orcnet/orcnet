﻿using OrcNet.Core;
using OrcNet.Core.Logger;
using OrcNet.Graphics.Render.Uniforms.Values;
using System;
using System.Collections.Generic;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// Subroutine uniform class definition holding a subroutine value.
    /// </summary>
    public class SubroutineUniform : AMemoryProfilable, IPipelineUniform, IDisposable
    {
        #region Fields

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Stores the flag indicating whether this uniform 
        /// is not up to date or not.
        /// </summary>
        private bool mIsDirty;
#endif

        /// <summary>
        /// Stores the uniform name.
        /// </summary>
        private string       mName;
        
        /// <summary>
        /// Stores the uniform's owner
        /// </summary>
        private PipelinePass mOwner;

        /// <summary>
        /// Stores the GPU uniform location (or offset if in a block)
        /// </summary>
        private int          mLocation;

        /// <summary>
        /// Stores the block this uniform belongs to if not Null.
        /// </summary>
        private UniformBlock mBlock;

        /// <summary>
        /// Stores the stage type this subroutine belongs to.
        /// </summary>
        private StageType    mStage;

        /// <summary>
        /// Stores the current subroutine index corresponding to the subroutine name 
        /// in the set of compatible subroutines.
        /// </summary>
        private int          mValue;

        /// <summary>
        /// Stores the set of compatible subroutine names for that subroutine uniform.
        /// </summary>
        private List<string> mCompatibleSubroutineNames;

        /// <summary>
        /// Stores the set of OpenGL identifier of the compatible subroutines for that subroutine uniform.
        /// </summary>
        private List<int>    mCompatibleSubroutineIndices;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(int) * 2;
                lSize += sizeof(StageType);
                lSize += string.IsNullOrEmpty( this.mName ) ? 0 : sizeof(char) * (uint)this.mName.Length;
                if
                    ( this.mOwner != null )
                {
                    lSize += this.mOwner.Size;
                }
                if
                    ( this.mBlock != null )
                {
                    lSize += this.mBlock.Size;
                }
                lSize += sizeof(int) * (uint)this.mCompatibleSubroutineIndices.Count;
                foreach
                    ( string lName in this.mCompatibleSubroutineNames )
                {
                    lSize += string.IsNullOrEmpty( lName ) ? 0 : sizeof(char) * (uint)lName.Length;
                }
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IPipelineUniform

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Gets or sets the flag indicating whether this uniform 
        /// is not up to date or not.
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return this.mIsDirty;
            }
            set
            {
                this.mIsDirty = value;
            }
        }
#endif

        /// <summary>
        /// Gets or sets the uniform's owner
        /// </summary>
        public PipelinePass Owner
        {
            get
            {
                return this.mOwner;
            }
            set
            {
                this.mOwner = value;
            }
        }

        /// <summary>
        /// Gets or sets the block this uniform belongs to if not Null.
        /// </summary>
        public UniformBlock Block
        {
            get
            {
                return this.mBlock;
            }
            set
            {
                this.mBlock = value;
            }
        }

        /// <summary>
        /// Gets the uniform name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public UniformType Type
        {
            get
            {
                return UniformType.SUBROUTINE;
            }
        }

        /// <summary>
        /// Sets the uniform untyped value.
        /// </summary>
        public IPipelineValue UntypedValue
        {
            set
            {
                SubroutineValue lValue = value as SubroutineValue;
                if
                    ( lValue != null )
                {
                    this.SubroutineName = lValue.Value;
                }
            }
        }

        /// <summary>
        /// Gets the uniform stage location (or offset if in a block)
        /// </summary>
        public int Location
        {
            get
            {
                return this.mLocation;
            }
            set
            {
                this.mLocation = value;
            }
        }

        #endregion Properties IPipelineUniform

        /// <summary>
        /// Gets or sets the uniform's subroutine index as value.
        /// </summary>
        public int Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                this.mValue = value;
                if
                    ( this.mOwner != null )
                {
                    // Add the subroutine to the pass.
                    this.mOwner.RefreshSubroutine( this.mStage, this.mLocation, (uint)this.mCompatibleSubroutineIndices[ value ] );
                }
            }
        }

        /// <summary>
        /// Gets the stage type this subroutine belongs to.
        /// </summary>
        public StageType Stage
        {
            get
            {
                return this.mStage;
            }
        }

        /// <summary>
        /// Gets the subroutine name corresponding to that uniform's value.
        /// </summary>
        public string SubroutineName
        {
            get
            {
                return this.mCompatibleSubroutineNames[ this.mValue ];
            }
            set
            {
                for
                    ( int lCurrName = 0; lCurrName < this.mCompatibleSubroutineNames.Count; lCurrName++ )
                {
                    if
                        ( value == this.mCompatibleSubroutineNames[ lCurrName ] )
                    {
                        this.Value = lCurrName;
                        return;
                    }
                }

                LogManager.Instance.Log( string.Format( "Subroutine name {0} not found!!!", value ) );
            }
        }

        /// <summary>
        /// Gets the set of compatible subroutine names for that subroutine uniform.
        /// </summary>
        public List<string> CompatibleSubroutineNames
        {
            get
            {
                return this.mCompatibleSubroutineNames;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SubroutineUniform"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        /// <param name="pStage">The stage to which this uniform subroutine belongs to.</param>
        /// <param name="pCompatibleSubroutineNames">The subroutine names compatible with that uniform subroutine.</param>
        /// <param name="pCompatibleSubroutineIds">The compatible subroutines OpenGL identifiers</param>
        public SubroutineUniform(PipelinePass pOwner, string pName, int pLocation, StageType pStage, List<string> pCompatibleSubroutineNames, List<int> pCompatibleSubroutineIds)
        {
            this.mValue = 0;
            this.mStage = pStage;
            this.mOwner = pOwner;
            this.mName  = pName;
            this.mLocation = pLocation;
            this.mCompatibleSubroutineNames   = pCompatibleSubroutineNames;
            this.mCompatibleSubroutineIndices = pCompatibleSubroutineIds;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public void SendUniform()
        {
            // Nothing to do for subroutines.
        }

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call but ONLY if its pass is being used so current.
        /// </summary>
        public void SendUniformIfCurrent()
        {
            if
                ( this.mBlock != null )
            {
                return;
            }

            if
                ( this.mOwner.IsCurrent )
            {
                IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                if( lRenderService != null &&
                    lRenderService.CurrentPass.PipelineId > 0 )
                {
                    GL.ActiveShaderProgram( lRenderService.CurrentPass.PipelineId, this.mOwner.PassId );
                }
                this.SendUniform();
                this.mIsDirty = false;
            }
            else
            {
                this.mIsDirty = true;
            }
        }
#endif

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            this.mOwner = null;
            this.mBlock = null;
            this.mCompatibleSubroutineNames.Clear();
            this.mCompatibleSubroutineIndices.Clear();

            GC.SuppressFinalize( this );
        }

        #endregion Methods
    }
}
