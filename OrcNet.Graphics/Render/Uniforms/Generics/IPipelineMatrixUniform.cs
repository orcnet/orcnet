﻿namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// Definition of the <see cref="IPipelineMatrixUniform{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">The matrix primitive type.</typeparam>
    public interface IPipelineMatrixUniform<T> : IPipelineUniform where T : struct
    {
        #region Properties

        /// <summary>
        /// Sets the uniform matrix value.
        /// </summary>
        IPipelineMatrixValue<T> Value
        {
            set;
        }

        #endregion Properties
    }
}
