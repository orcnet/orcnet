﻿namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// Generic pipeline matrix value base interface definition allowing
    /// to type the owned value.
    /// </summary>
    /// <typeparam name="T">The owned value type.</typeparam>
    public interface IPipelineMatrixValue<T> : IPipelineValue where T : struct
    {
        #region Properties

        /// <summary>
        /// Gets the uniform matrix set of values.
        /// </summary>
        T[] Value
        {
            get;
        }

        #endregion Properties
    }
}
