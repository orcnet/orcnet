﻿using OrcNet.Core;
using System.Diagnostics;

namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// GLSL uniform value class definition.
    /// </summary>
    /// <typeparam name="T">The uniform value type by value whence struct</typeparam>
    [DebuggerDisplay("Name = {Name}, Type = {Type}, Value = {Value}")]
    public abstract class APipelineValue<T> : AMemoryProfilable, IPipelineValue<T>
    {
        #region Fields

        /// <summary>
        /// Stores the uniform name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the uniform value.
        /// </summary>
        private T      mValue;

        #endregion Fields

        #region Properties

        #region Properties IUniformValue

        /// <summary>
        /// Gets the pipeline untyped value.
        /// </summary>
        object IPipelineValue.UntypedValue
        {
            get
            {
                return this.Value;
            }
        }

        #endregion Properties IUniformValue

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += string.IsNullOrEmpty( this.mName ) ? 0 : sizeof(char) * (uint)this.mName.Length;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public abstract UniformType Type
        {
            get;
        }

        /// <summary>
        /// Gets the pipeline value name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the pipeline value.
        /// </summary>
        public T Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                this.mValue = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APipelineValue{T}"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        protected APipelineValue(string pName)
        {
            this.mName = pName;
        }

        #endregion Constructor
    }
}
