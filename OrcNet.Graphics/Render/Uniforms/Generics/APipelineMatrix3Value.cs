﻿using OrcNet.Core.Math;
using System.Diagnostics;

namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// GLSL uniform matrix3 value class definition.
    /// </summary>
    /// <typeparam name="T">The uniform matrix value type by value whence struct</typeparam>
    [DebuggerDisplay("Name = {Name}, Type = {Type}")]
    public abstract class APipelineMatrix3Value<T> : APipelineMatrixValue<T> where T : struct
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Matrix3 values.
        /// </summary>
        public abstract Matrix3<T> Matrix
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APipelineMatrix3Value{T}"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        protected APipelineMatrix3Value(string pName) :
        base( pName, 3, 3 )
        {

        }

        #endregion Constructor
    }
}
