﻿namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// Generic pipeline value base interface definition allowing
    /// to type the owned value.
    /// </summary>
    /// <typeparam name="T">The owned value type.</typeparam>
    public interface IPipelineValue<T> : IPipelineValue
    {
        #region Properties

        /// <summary>
        /// Gets the uniform value.
        /// </summary>
        T Value
        {
            get;
        }

        #endregion Properties
    }
}
