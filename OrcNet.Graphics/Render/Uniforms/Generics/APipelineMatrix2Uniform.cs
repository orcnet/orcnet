﻿using OrcNet.Core.Math;
using System.Diagnostics;

namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// Base abstract pipeline matrix2 based uniform class definition
    /// in charge of passing the value to the pipeline pass.
    /// </summary>
    /// <typeparam name="T">The uniform matrix value type</typeparam>
    [DebuggerDisplay("Name = {Name}, Type = {Type}")]
    public abstract class APipelineMatrix2Uniform<T> : APipelineMatrixUniform<T> where T : struct
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Matrix2 values.
        /// </summary>
        public abstract Matrix2<T> Matrix
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APipelineMatrix2Uniform{T}"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        /// <param name="pStride">The matrix stride between two consecutive rows or columns when stored in an uniform block.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the matrix is row majr or column major.</param>
        protected APipelineMatrix2Uniform(PipelinePass pOwner, string pName, UniformBlock pBlock = null, int pLocation = -1, int pStride = 0, bool pIsRowMajor = true) :
        base( pOwner, pName, 2, 2, pBlock, pLocation, pStride, pIsRowMajor )
        {
            
        }

        #endregion Constructor
    }
}
