﻿namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// Base pipeline uniform interface definition.
    /// </summary>
    /// <typeparam name="T">The uniform value type</typeparam>
    public interface IPipelineUniform<T> : IPipelineUniform
    {
        #region Properties

        /// <summary>
        /// Sets the uniform value.
        /// </summary>
        IPipelineValue<T> Value
        {
            set;
        }

        #endregion Properties
    }
}
