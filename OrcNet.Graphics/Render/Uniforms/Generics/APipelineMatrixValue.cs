﻿using OrcNet.Core;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// GLSL uniform matrix value class definition.
    /// </summary>
    /// <typeparam name="T">The uniform matrix value type by value whence struct</typeparam>
    [DebuggerDisplay("Name = {Name}, Type = {Type}")]
    public abstract class APipelineMatrixValue<T> : AMemoryProfilable, IPipelineMatrixValue<T> where T : struct
    {
        #region Fields

        /// <summary>
        /// Stores the uniform name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the row count
        /// </summary>
        private int    mRowCount;

        /// <summary>
        /// Stores the column count
        /// </summary>
        private int    mColumnCount;

        /// <summary>
        /// Stores the uniform matrix set of values.
        /// </summary>
        private T[]    mValue;

        #endregion Fields

        #region Properties

        #region Properties IUniformValue

        /// <summary>
        /// Gets the pipeline untyped value.
        /// </summary>
        object IPipelineValue.UntypedValue
        {
            get
            {
                return this.Value;
            }
        }

        #endregion Properties IUniformValue

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += string.IsNullOrEmpty( this.mName ) ? 0 : sizeof(char) * (uint)this.mName.Length;
                lSize += (uint)(Marshal.SizeOf( typeof(T) ) * this.mValue.Length);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public abstract UniformType Type
        {
            get;
        }

        /// <summary>
        /// Gets the pipeline value name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }
        
        /// <summary>
        /// Gets or sets the pipeline matrix set of values.
        /// </summary>
        public T[] Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                if ( value == null )
                {
                    return;
                }

                for ( int lCurr = 0; lCurr < this.mRowCount * this.mColumnCount; lCurr++ )
                {
                    this.mValue[ lCurr ] = value[ lCurr ];
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APipelineMatrixValue{T}"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        /// <param name="pRowCount">The matrix row count.</param>
        /// <param name="pColumnCount">The matrix column count.</param>
        protected APipelineMatrixValue(string pName, int pRowCount, int pColumnCount)
        {
            this.mName = pName;
            this.mRowCount = pRowCount;
            this.mColumnCount = pColumnCount;
            this.mValue = new T[ this.mRowCount * this.mColumnCount ];
        }

        #endregion Constructor
    }
}
