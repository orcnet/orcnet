﻿using OrcNet.Core;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.Render.Uniforms.Generics
{
    /// <summary>
    /// Base abstract pipeline matrix based uniform class definition
    /// in charge of passing the value to the pipeline pass.
    /// </summary>
    /// <typeparam name="T">The uniform matrix value type</typeparam>
    [DebuggerDisplay("Name = {Name}, Type = {Type}")]
    public abstract class APipelineMatrixUniform<T> : AMemoryProfilable, IPipelineMatrixUniform<T>, IDisposable where T : struct
    {
        #region Fields
        
#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Stores the flag indicating whether this uniform 
        /// is not up to date or not.
        /// </summary>
        private bool           mIsDirty;
#endif

        /// <summary>
        /// Stores the GPU uniform name.
        /// </summary>
        private string         mName;

        /// <summary>
        /// Stores the block this uniform belongs to if not Null.
        /// </summary>
        protected UniformBlock mBlock;

        /// <summary>
        /// Stores the uniform's value.
        /// </summary>
        protected T[]          mValue;

        /// <summary>
        /// Stores the uniform's owner
        /// </summary>
        protected PipelinePass mOwner;

        /// <summary>
        /// Stores the GPU uniform location (or offset if in a block)
        /// </summary>
        protected int          mLocation;

        /// <summary>
        /// Stores the flag indicating whether the matrix is row majr or column major.
        /// </summary>
        protected readonly bool mIsRowMajor;

        /// <summary>
        /// Stores the matrix stride between two consecutive rows or columns when stored
        /// in an uniform block.
        /// </summary>
        protected readonly int mMatrixStride;

        /// <summary>
        /// Stores the row count.
        /// </summary>
        protected readonly int mRowCount;

        /// <summary>
        /// Stores the column count.
        /// </summary>
        protected readonly int mColumnCount;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += string.IsNullOrEmpty( this.mName ) ? 0 : sizeof(char) * (uint)this.mName.Length;

                if ( this.mOwner != null )
                {
                    lSize += this.mOwner.Size;
                }

                if ( this.mBlock != null )
                {
                    lSize += this.mBlock.Size;
                }

                lSize += sizeof(int) * 4;
                lSize += sizeof(bool);
                
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Gets or sets the flag indicating whether this uniform 
        /// is not up to date or not.
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return this.mIsDirty;
            }
            set
            {
                this.mIsDirty = value;
            }
        }
#endif

        /// <summary>
        /// Gets or sets the uniform's owner
        /// </summary>
        public PipelinePass Owner
        {
            get
            {
                return this.mOwner;
            }
            set
            {
                this.mOwner = value;
            }
        }

        /// <summary>
        /// Gets or sets the block this uniform belongs to if not Null.
        /// </summary>
        public UniformBlock Block
        {
            get
            {
                return this.mBlock;
            }
            set
            {
                this.mBlock = value;
            }
        }

        /// <summary>
        /// Gets or sets the uniform stage location (or offset if in a block)
        /// </summary>
        public int Location
        {
            get
            {
                return this.mLocation;
            }
            set
            {
                this.mLocation = value;
            }
        }

        /// <summary>
        /// Gets the GPU uniform name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public abstract UniformType Type
        {
            get;
        }

        /// <summary>
        /// Gets or sets the uniform matrix set of values.
        /// </summary>
        public T[] Value
        {
            get
            {
                if ( this.mBlock != null &&
                     this.mOwner != null )
                {
                    this.MapsUniform( this.mLocation, ref this.mValue );
                }
                
                return this.mValue;
            }
            set
            {
                if ( this.mBlock == null ||
                     this.mOwner == null )
                {
                    for ( int lCurr = 0; lCurr < this.mRowCount * this.mColumnCount; lCurr++ )
                    {
                        this.mValue[ lCurr ] = value[ lCurr ];
                    }
                    
                    if ( this.mOwner != null )
                    {
                        // Send the value to the GPU.
                        this.SendUniform();
                    }
                }
                else
                {
                    this.MapsUniform( this.mLocation, value );
                }
            }
        }

        /// <summary>
        /// Sets the uniform matrix value.
        /// </summary>
        IPipelineMatrixValue<T> IPipelineMatrixUniform<T>.Value
        {
            set
            {
                this.Value = value.Value;
            }
        }

        /// <summary>
        /// Sets the uniform untyped value.
        /// </summary>
        IPipelineValue IPipelineUniform.UntypedValue
        {
            set
            {
                this.Value = value.UntypedValue as T[];
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the matrix is row majr or column major.
        /// </summary>
        public bool IsRowMajor
        {
            get
            {
                return this.mIsRowMajor;
            }
        }

        /// <summary>
        /// Gets the matrix stride between two consecutive rows or columns when stored
        /// in an uniform block.
        /// </summary>
        public int  MatrixStride
        {
            get
            {
                return this.mMatrixStride;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APipelineMatrixUniform{T}"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pRowCount">The matrix row count.</param>
        /// <param name="pColumnCount">The matrix column count.</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        /// <param name="pStride">The matrix stride between two consecutive rows or columns when stored in an uniform block.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the matrix is row majr or column major.</param>
        protected APipelineMatrixUniform(PipelinePass pOwner, string pName, int pRowCount, int pColumnCount, UniformBlock pBlock = null, int pLocation = -1, int pStride = 0, bool pIsRowMajor = true)
        {
            this.mOwner        = pOwner;
            this.mName         = pName;
            this.mBlock        = pBlock;
            this.mLocation     = pLocation;
            this.mRowCount     = pRowCount;
            this.mColumnCount  = pColumnCount;
            this.mIsRowMajor   = pIsRowMajor;
            this.mMatrixStride = pStride;
            this.mValue        = new T[ this.mRowCount * this.mColumnCount ];
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pToUpdate">The buffer to update.</param>
        protected abstract void MapsUniform(int pOffset, ref T[] pToUpdate);

        /// <summary>
        /// Sets the uniform's value into the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pNewValue">The new set of values to pass into the uniform block buffer memory</param>
        protected abstract void MapsUniform(int pOffset, T[] pNewValue);

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public abstract void SendUniform();

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call but ONLY if its pass is being used so current.
        /// </summary>
        public void SendUniformIfCurrent()
        {
            if ( this.mBlock != null )
            {
                return;
            }

            if ( this.mOwner.IsCurrent )
            {
                IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                if ( lRenderService != null &&
                     lRenderService.CurrentPass.PipelineId > 0 )
                {
                    GL.ActiveShaderProgram( lRenderService.CurrentPass.PipelineId, this.mOwner.PassId );
                }
                this.SendUniform();
                this.mIsDirty = false;
            }
            else
            {
                this.mIsDirty = true;
            }
        }
#endif

        #region Methods IDisposable

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Releases resources (overridable)
        /// </summary>
        protected virtual void OnDispose()
        {
            this.mOwner = null;
            this.mBlock = null;
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
