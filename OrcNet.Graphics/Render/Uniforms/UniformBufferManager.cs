﻿using OrcNet.Graphics.Helpers;
using System;
using System.Collections.Generic;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// Internal helper uniform buffer manager class definition.
    /// </summary>
    internal class UniformBufferManager : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the times a unit has been bound.
        /// </summary>
        private uint mTime;

        /// <summary>
        /// Stores the set of allowed uniform buffer units.
        /// </summary>
        private UniformBufferUnit[] mUnits;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformBufferManager"/> class.
        /// </summary>
        public UniformBufferManager()
        {
            this.mTime = 0;

            uint lMaxUnitsAllowed = GLHelpers.GetUniformMaxBufferUnits();
            this.mUnits = new UniformBufferUnit[ lMaxUnitsAllowed ];
            for
                ( uint lCurrentUnit = 0; lCurrentUnit < lMaxUnitsAllowed; lCurrentUnit++ )
            {
                this.mUnits[ lCurrentUnit ] = new UniformBufferUnit( lCurrentUnit );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Finds an available unit to bind a buffer to.
        /// </summary>
        /// <param name="pPassIds">The used pass(es) ids</param>
        /// <returns>The unit identifier if any freed, -1 otherwise.</returns>
        public int FindAvailableUnit(IEnumerable<uint> pPassIds)
        {
            // If any free, return it straight.
            int lCurrentId = 0;
            foreach
                ( UniformBufferUnit lUnit in this.mUnits )
            {
                if
                    ( lUnit.IsFree )
                {
                    return lCurrentId;
                }
                lCurrentId++;
            }

            lCurrentId = 0;
            int lFreedUnit = -1;
            uint lOldest = this.mTime;
            foreach
                ( UniformBufferUnit lUnit in this.mUnits )
            {
                GPUBuffer lBuffer = lUnit.CurrentBuffer;
                if
                    ( lBuffer.HasUsers( pPassIds ) == false ) // If any unit not used by a pass?
                {
                    uint lLastBindingTime = lUnit.LastBindingTime;
                    if
                        ( lFreedUnit == -1 || // If first time
                          lLastBindingTime < lOldest ) // If older
                    {
                        lFreedUnit = lCurrentId;
                        lOldest    = lLastBindingTime;
                    }
                }
                lCurrentId++;
            }

            return lFreedUnit;
        }

        /// <summary>
        /// Binds a supplied buffer to a uniform unit.
        /// </summary>
        /// <param name="pUnit">The unit to bind the buffer to.</param>
        /// <param name="pBuffer">The buffer to bind to the unit</param>
        public void Bind(uint pUnit, GPUBuffer pBuffer)
        {
            uint lMaxUnitsAllowed = GLHelpers.GetUniformMaxBufferUnits();
            if
                ( pUnit >= lMaxUnitsAllowed ||
                  pBuffer == null )
            {
                return;
            }

            this.mUnits[ pUnit ].Bind( pBuffer, this.mTime++ );
        }

        /// <summary>
        /// Unbinds the supplied buffer from that uniform unit.
        /// </summary>
        /// <param name="pBuffer">The buffer to unbind from that unit.</param>
        public void Unbind(GPUBuffer pBuffer)
        {
            if
                ( pBuffer == null )
            {
                return;
            }

            foreach
                ( UniformBufferUnit lUnit in this.mUnits )
            {
                if
                    ( lUnit.CurrentBuffer == pBuffer )
                {
                    lUnit.Bind( null, this.mTime++ );
                }
            }
        }

        /// <summary>
        /// Unbinds all units from their buffer.
        /// </summary>
        public void UnbindAll()
        {
            foreach
                ( UniformBufferUnit lUnit in this.mUnits )
            {
                lUnit.Bind( null, 0 );
            }

            this.mTime = 0;
        }

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            foreach
                ( UniformBufferUnit lUnit in this.mUnits )
            {
                lUnit.Dispose();
            }
            this.mUnits = null;

            GC.SuppressFinalize( this );
        }

        #endregion Methods
    }
}
