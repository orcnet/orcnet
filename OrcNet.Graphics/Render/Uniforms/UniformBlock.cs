﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// Uniform block containing uniforms as a whole. It is stored in a GPU buffer.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Pos = {Index}, UniformCount = {UniformCount}, IsMapped = {IsMapped}")]
    public class UniformBlock : AMemoryProfilable
    {
        #region Fields

        /// <summary>
        /// Stores the buffers associated with each uniform block, but a buffer can be shared among different block
        /// regarding to the block's declaration defined by the container's key.
        /// </summary>
        private static Dictionary<string, GPUBuffer> sBlockBuffers;

        /// <summary>
        /// Stores the buffers ref counters
        /// </summary>
        private static Dictionary<string, int> sBufferRefCounters;

        /// <summary>
        /// Stores the pipeline pass owner of this block.
        /// </summary>
        private PipelinePass mOwner;

        /// <summary>
        /// Stores the uniform block's GPU name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the uniform block's index in the pass.
        /// </summary>
        private uint   mIndex;

        /// <summary>
        /// Stores the total buffer size to store uniforms of that block.
        /// </summary>
        private uint   mSize;

        /// <summary>
        /// Stores the GPU buffer that stores values of the uniforms of this block.
        /// </summary>
        private GPUBuffer mBuffer;

        /// <summary>
        /// Stores the block's set of uniforms.
        /// </summary>
        private Dictionary<string, IPipelineUniform> mUniforms;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(uint) * 2;
                if
                    ( string.IsNullOrEmpty( this.mName ) == false )
                {
                    lSize += sizeof(char) * (uint)this.mName.Length;
                }
                
                if
                    ( this.mOwner != null )
                {
                    lSize += this.mOwner.Size;
                }

                if
                    ( this.mBuffer != null )
                {
                    lSize += this.mBuffer.Size;
                }

                foreach
                    ( KeyValuePair<string, IPipelineUniform> lUniform in this.mUniforms )
                {
                    lSize += sizeof(char) * (uint)lUniform.Key.Length;
                    lSize += lUniform.Value.Size;
                }

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets or sets the pipeline pass owner of this block.
        /// </summary>
        public PipelinePass Owner
        {
            get
            {
                return this.mOwner;
            }
            set
            {
                this.mOwner = value;
            }
        }

        /// <summary>
        /// Gets the uniform block's GPU name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the uniform block's index in its pass.
        /// </summary>
        public uint Index
        {
            get
            {
                return this.mIndex;
            }
        }

        /// <summary>
        /// Gets the total buffer size to store uniforms of that block.
        /// </summary>
        public uint TotalSize
        {
            get
            {
                return this.mSize;
            }
        }

        /// <summary>
        /// Gets the current uniform count of that block.
        /// </summary>
        public int UniformCount
        {
            get
            {
                return this.mUniforms.Count;
            }
        }

        /// <summary>
        /// Gets the block's uniform(s)
        /// </summary>
        public IEnumerable<IPipelineUniform> Uniforms
        {
            get
            {
                // TO DO: Checks whether a copy would be more suitable...
                return this.mUniforms.Values;
            }
        }

        /// <summary>
        /// Gets the corresponding pipeline uniform.
        /// </summary>
        /// <param name="pName">The uniform's GPU name to look for.</param>
        /// <returns></returns>
        public IPipelineUniform this[string pName]
        {
            get
            {
                IPipelineUniform lUniform = null;
                if
                    ( this.mUniforms.TryGetValue( pName, out lUniform ) )
                {
                    return lUniform;
                }

                string lExtendedName = this.mName + "." + pName;
                if
                    ( this.mUniforms.TryGetValue( lExtendedName, out lUniform ) )
                {
                    return lUniform;
                }

                return null;
            }
        }
        
        /// <summary>
        /// Gets or sets the GPU buffer that stores values of the uniforms of this block.
        /// </summary>
        public GPUBuffer Buffer
        {
            get
            {
                return this.mBuffer;
            }
            set
            {
                if ( this.mOwner != null )
                {
                    if ( this.mBuffer != null )
                    {
                        this.mBuffer.RemoveUser( this.mOwner.Id );
                    }

                    if ( value != null )
                    {
                        value.AddUser( this.mOwner.Id );
                    }
                }

                if ( this.mBuffer != null &&
                     this.IsMapped )
                {
                    this.UnmapBuffer();
                    UniformBlockBuffer lOldCast = this.mBuffer as UniformBlockBuffer;
                    if ( lOldCast != null )
                    {
                        int lRefCounter;
                        if ( sBufferRefCounters.TryGetValue( lOldCast.Name, out lRefCounter ) )
                        {
                            lRefCounter--;
                            if ( lRefCounter <= 0 )
                            {
                                sBufferRefCounters.Remove( lOldCast.Name );
                                sBlockBuffers.Remove( lOldCast.Name );
                            }
                            else
                            {
                                sBufferRefCounters[ lOldCast.Name ] = lRefCounter;
                            }
                        }
                    }
                }

                this.mBuffer = value;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the GPU buffer associated with this block 
        /// is currently mapped in client memory or not.
        /// </summary>
        internal bool IsMapped
        {
            get
            {
                if
                    ( this.mBuffer != null )
                {
                    return this.mBuffer.MappedData != IntPtr.Zero;
                }

                return false;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="UniformBlock"/> class.
        /// </summary>
        static UniformBlock()
        {
            sBlockBuffers = new Dictionary<string, GPUBuffer>();
            sBufferRefCounters = new Dictionary<string, int>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformBlock"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform block's owner</param>
        /// <param name="pName">The uniform block's GPU name</param>
        /// <param name="pIndex">The uniform block's index in the pass.</param>
        /// <param name="pSize">The minimum buffer size to store uniforms of that block.</param>
        public UniformBlock(PipelinePass pOwner, string pName, uint pIndex, uint pSize)
        {
            this.mOwner = pOwner;
            this.mName  = pName;
            this.mIndex = pIndex;
            this.mSize  = pSize;

            this.mUniforms = new Dictionary<string, IPipelineUniform>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Block's method to get or create if does not exist yet the block's buffer
        /// given its name as key.
        /// </summary>
        /// <param name="pName"></param>
        /// <returns></returns>
        internal static GPUBuffer GetOrCreate(string pName)
        {
            GPUBuffer lBuffer;
            if ( sBlockBuffers.TryGetValue( pName, out lBuffer ) == false )
            {
                lBuffer = new UniformBlockBuffer( pName );
                sBlockBuffers.Add( pName, lBuffer );
                sBufferRefCounters.Add( pName, 1 );
            }
            else
            {
                sBufferRefCounters[ pName ]++;
            }
            
            return lBuffer;
        }

        /// <summary>
        /// Retrieves the uniform in that block using the given uniform name.
        /// </summary>
        /// <typeparam name="T">The return type.</typeparam>
        /// <param name="pName">The uniform name to look for.</param>
        /// <returns>The uniform, null otherwise.</returns>
        public T GetUniform<T>(string pName) where T : class, IPipelineUniform
        {
            return this[ pName ] as T;
        }

        /// <summary>
        /// Adds a pipeline uniform to that uniforms block.
        /// </summary>
        /// <param name="pUniform">The uniform to add to that block.</param>
        internal void AddUniform(IPipelineUniform pUniform)
        {
            this.mUniforms[ pUniform.Name ] = pUniform;
        }

        /// <summary>
        /// Removes a pipeline uniform to that uniforms block.
        /// </summary>
        /// <param name="pUniform">The uniform to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        internal bool RemoveUniform(IPipelineUniform pUniform)
        {
            if ( this.mUniforms.ContainsKey( pUniform.Name ) )
            {
                return this.mUniforms.Remove( pUniform.Name );
            }

            return false;
        }

        /// <summary>
        /// Maps the GPU buffer associated with this block in client memory
        /// </summary>
        /// <param name="pOffset">The offset of the uniform that must be returned</param>
        /// <returns>The GPU uniform at the given offset in the block.</returns>
        internal IntPtr MapBuffer(int pOffset)
        {
            if ( this.mBuffer != null )
            {
                IntPtr lResult = this.mBuffer.MappedData;
                if ( lResult == IntPtr.Zero )
                {
                    lResult = this.mBuffer.Map( BufferAccess.ReadWrite );
                }

                return lResult + pOffset;
            }

            return IntPtr.Zero;
        }

        /// <summary>
        /// Unmaps the GPU buffer associated with this block in client memory.
        /// </summary>
        internal void UnmapBuffer()
        {
            if
                ( this.mBuffer != null )
            {
                this.mBuffer.Unmap();
            }
        }

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Block's specific buffer.
        /// </summary>
        private class UniformBlockBuffer : GPUBuffer
        {
            #region Fields

            /// <summary>
            /// Stores the buffer's name.
            /// </summary>
            private string mName;

            #endregion Fields

            #region Properties

            /// <summary>
            /// Gets the buffer's name.
            /// </summary>
            public string Name
            {
                get
                {
                    return this.mName;
                }
            }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="UniformBlockBuffer"/> class.
            /// </summary>
            /// <param name="pName">The buffer's name.</param>
            internal UniformBlockBuffer(string pName)
            {
                this.mName = pName;
            }

            #endregion Constructor
        }

        #endregion Inner classes
    }
}
