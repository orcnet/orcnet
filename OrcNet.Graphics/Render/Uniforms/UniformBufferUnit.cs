﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Helpers;
using System;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// Uniform buffer unit class definition used to
    /// bind buffers used as uniform blocks in Pass(es)
    /// </summary>
    internal class UniformBufferUnit : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the unit identifier.
        /// </summary>
        private uint  mUnit;

        /// <summary>
        /// Stores the last time that unit has been bound.
        /// </summary>
        private uint  mLastBindingTime;

        /// <summary>
        /// Stores the currently bound buffer.
        /// </summary>
        private GPUBuffer mCurrentBuffer;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the unit is freed or not.
        /// </summary>
        public bool IsFree
        {
            get
            {
                return this.mCurrentBuffer == null;
            }
        }

        /// <summary>
        /// Gets the last time that unit has been bound with a buffer.
        /// </summary>
        public uint LastBindingTime
        {
            get
            {
                return this.mLastBindingTime;
            }
        }

        /// <summary>
        /// Gets the current buffer in that unit.
        /// </summary>
        public GPUBuffer CurrentBuffer
        {
            get
            {
                return this.mCurrentBuffer;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformBufferUnit"/> class.
        /// </summary>
        /// <param name="pUnit">The unit identifier.</param>
        public UniformBufferUnit(uint pUnit)
        {
            this.mUnit = pUnit;
            this.mLastBindingTime = 0;
            this.mCurrentBuffer = null;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Binds the supplied buffer to that unit.
        /// </summary>
        /// <param name="pBuffer">The new buffer to bind to that unit</param>
        /// <param name="pTime">The last binding time.</param>
        public void Bind(GPUBuffer pBuffer, uint pTime)
        {
            this.mLastBindingTime = pTime;

            if
                ( this.mCurrentBuffer != null )
            {
                this.mCurrentBuffer.CurrentUniformUnit = -1;
            }

            this.mCurrentBuffer = pBuffer;

            if
                ( this.mCurrentBuffer != null )
            {
                this.mCurrentBuffer.CurrentUniformUnit = (int)this.mUnit;
                GL.BindBufferBase( BufferRangeTarget.UniformBuffer, this.mUnit, this.mCurrentBuffer.BufferId );
            }
            else
            {
                GL.BindBufferBase( BufferRangeTarget.UniformBuffer, this.mUnit, 0 );
            }
            
            GLHelpers.GetError();
        }

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            this.mCurrentBuffer = null;

            GC.SuppressFinalize( this );
        }

        #endregion Methods
    }
}
