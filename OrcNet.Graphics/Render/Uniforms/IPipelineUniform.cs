﻿using OrcNet.Core;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// Base pipeline uniform interface definition.
    /// </summary>
    public interface IPipelineUniform : IMemoryProfilable
    {
        #region Properties

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Gets or sets the flag indicating whether this uniform 
        /// is not up to date or not.
        /// </summary>
        bool IsDirty
        {
            get;
            set;
        }
#endif

        /// <summary>
        /// Gets or sets the uniform's owner
        /// </summary>
        PipelinePass Owner
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the block this uniform belongs to if not Null.
        /// </summary>
        UniformBlock Block
        {
            get;
            set;
        }

        /// <summary>
        /// Sets the uniform untyped value.
        /// </summary>
        IPipelineValue UntypedValue
        {
            set;
        }

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        UniformType Type
        {
            get;
        }

        /// <summary>
        /// Gets the uniform name.
        /// </summary>
        string Name
        {
            get;
        }

        /// <summary>
        /// Gets or sets the uniform stage location (or offset if in a block)
        /// </summary>
        int Location
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods
        
        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        void SendUniform();

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call but ONLY if its pass is being used so current.
        /// </summary>
        void SendUniformIfCurrent();
#endif

        #endregion Methods
    }
}
