﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// 3x2 Matrix double based uniform class definition.
    /// </summary>
    public class Matrix3x2DUniform : APipelineMatrixUniform<double>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT3x2D;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3x2DUniform"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        /// <param name="pStride">The matrix stride between two consecutive rows or columns when stored in an uniform block.</param>
        /// <param name="pIsRowMajor">The flag indicating whether the matrix is row majr or column major.</param>
        public Matrix3x2DUniform(PipelinePass pOwner, string pName, UniformBlock pBlock = null, int pLocation = -1, int pStride = 0, bool pIsRowMajor = true) : 
        base( pOwner, pName, 3, 2, pBlock, pLocation, pStride, pIsRowMajor )
        {
            
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pToUpdate">The buffer to update.</param>
        protected override void MapsUniform(int pOffset, ref double[] pToUpdate)
        {
            unsafe
            {
                byte* lBuffer = (byte*)this.mBlock.MapBuffer( pOffset );
                if ( this.IsRowMajor )
                {
                    for ( int lRow = 0; lRow < this.mRowCount; lRow++ )
                    {
                        for ( int lColumn = 0; lColumn < this.mColumnCount; lColumn++ )
                        {
                            pToUpdate[ lRow * this.mColumnCount + lColumn ] = ((double*)(lBuffer + lRow * this.mMatrixStride))[ lColumn ];
                        }
                    }
                }
                else
                {
                    for ( int lRow = 0; lRow < this.mRowCount; lRow++ )
                    {
                        for ( int lColumn = 0; lColumn < this.mColumnCount; lColumn++ )
                        {
                            pToUpdate[ lRow * this.mColumnCount + lColumn ] = ((double*)(lBuffer + lColumn * this.mMatrixStride))[ lRow ];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the uniform's value into the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pValue">The value to pass into the uniform block buffer memory</param>
        protected override void MapsUniform(int pOffset, double[] pValue)
        {
            unsafe
            {
                byte* lBuffer = (byte*)this.mBlock.MapBuffer( pOffset );
                if ( this.IsRowMajor )
                {
                    for ( int lRow = 0; lRow < this.mRowCount; lRow++ )
                    {
                        for ( int lColumn = 0; lColumn < this.mColumnCount; lColumn++ )
                        {
                            ((double*)(lBuffer + lRow * this.mMatrixStride))[ lColumn ] = pValue[ lRow * this.mColumnCount + lColumn ];
                        }
                    }
                }
                else
                {
                    for ( int lRow = 0; lRow < this.mRowCount; lRow++ )
                    {
                        for ( int lColumn = 0; lColumn < this.mColumnCount; lColumn++ )
                        {
                            ((double*)(lBuffer + lColumn * this.mMatrixStride))[ lRow ] = pValue[ lRow * this.mColumnCount + lColumn ];
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public override void SendUniform()
        {
#if (ORK_NO_GLPROGRAMUNIFORM)
            GL.UniformMatrix3x2( this.mLocation, 1, true, this.mValue );
#else
            GL.ProgramUniformMatrix3x2( this.mOwner.Id, this.mLocation, 1, true, this.mValue );
#endif
        }

        #endregion Methods
    }
}
