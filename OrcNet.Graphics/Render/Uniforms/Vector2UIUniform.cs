﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;
using System;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// 2D Vector unsigned integer based uniform class definition.
    /// </summary>
    public class Vector2UIUniform : APipelineUniform<Vector2UI>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC2UI;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2UIUniform"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        public Vector2UIUniform(PipelinePass pOwner, string pName, UniformBlock pBlock = null, int pLocation = -1) : 
        base( pOwner, pName, pBlock, pLocation )
        {
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <returns>The uniform value</returns>
        protected override Vector2UI MapsUniform(int pOffset)
        {
            if ( this.mBlock != null )
            {
                int[] lResult = new int[ 2 ];
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( lDataPtr, lResult, 0, 2 );
                return new Vector2UI( (uint)lResult[ 0 ], (uint)lResult[ 1 ] );
            }

            return Vector2UI.ZERO;
        }

        /// <summary>
        /// Sets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pValue">The value to pass into the uniform block buffer memory</param>
        protected override void MapsUniform(int pOffset, Vector2UI pValue)
        {
            if ( this.mBlock != null )
            {
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( new int[ 2 ] { (int)pValue.X, (int)pValue.Y }, 0, lDataPtr, 2 );
            }
        }

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public override void SendUniform()
        {
#if (ORK_NO_GLPROGRAMUNIFORM)
            GL.Uniform2( this.mLocation, this.mValue.X, this.mValue.Y );
#else
            GL.ProgramUniform2( this.mOwner.Id, this.mLocation, this.mValue.X, this.mValue.Y );
#endif
        }

        #endregion Methods
    }
}
