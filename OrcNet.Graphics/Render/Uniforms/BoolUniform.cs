﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Render.Uniforms.Generics;
using System;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// Boolean based single primitive uniform class definition.
    /// </summary>
    public class BoolUniform : APipelineUniform<bool>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC1B;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BoolUniform"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        public BoolUniform(PipelinePass pOwner, string pName, UniformBlock pBlock = null, int pLocation = -1) : 
        base( pOwner, pName, pBlock, pLocation )
        {
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <returns>The uniform value</returns>
        protected override bool MapsUniform(int pOffset)
        {
            if ( this.mBlock != null )
            {
                int[] lResult = new int[ 1 ];
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( lDataPtr, lResult, 0, 1 );
                return Convert.ToBoolean( lResult[ 0 ] );
            }

            return false;
        }

        /// <summary>
        /// Sets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pValue">The value to pass into the uniform block buffer memory</param>
        protected override void MapsUniform(int pOffset, bool pValue)
        {
            if ( this.mBlock != null )
            {
                int lValue = pValue ? 1 : 0;
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( new int[ 1 ] { lValue }, 0, lDataPtr, 1 );
            }
        }

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public override void SendUniform()
        {
#if (ORK_NO_GLPROGRAMUNIFORM)
            GL.Uniform1( this.mLocation, this.mValue ? 1 : 0 );
#else
            GL.ProgramUniform1( this.mOwner.Id, this.mLocation, this.mValue ? 1 : 0 );
#endif
        }

        #endregion Methods
    }
}
