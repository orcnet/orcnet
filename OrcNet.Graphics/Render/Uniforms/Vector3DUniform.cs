﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;
using System;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// 3D Vector double based uniform class definition.
    /// </summary>
    public class Vector3DUniform : APipelineUniform<Vector3D>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC3D;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3DUniform"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        public Vector3DUniform(PipelinePass pOwner, string pName, UniformBlock pBlock = null, int pLocation = -1) : 
        base( pOwner, pName, pBlock, pLocation )
        {
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <returns>The uniform value</returns>
        protected override Vector3D MapsUniform(int pOffset)
        {
            if ( this.mBlock != null )
            {
                double[] lResult = new double[ 3 ];
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( lDataPtr, lResult, 0, 3 );
                return new Vector3D( lResult[ 0 ], lResult[ 1 ], lResult[ 2 ] );
            }

            return Vector3D.ZERO;
        }

        /// <summary>
        /// Sets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pValue">The value to pass into the uniform block buffer memory</param>
        protected override void MapsUniform(int pOffset, Vector3D pValue)
        {
            if ( this.mBlock != null )
            {
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( new double[ 3 ] { pValue.X, pValue.Y, pValue.Z }, 0, lDataPtr, 3 );
            }
        }

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public override void SendUniform()
        {
#if (ORK_NO_GLPROGRAMUNIFORM)
            GL.Uniform3( this.mLocation, this.mValue.X, this.mValue.Y, this.mValue.Z );
#else
            GL.ProgramUniform3( this.mOwner.Id, this.mLocation, this.mValue.X, this.mValue.Y, this.mValue.Z );
#endif
        }

        #endregion Methods
    }
}
