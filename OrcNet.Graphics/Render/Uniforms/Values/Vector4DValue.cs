﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 4D Vector double based uniform value.
    /// </summary>
    public class Vector4DValue : APipelineValue<Vector4D>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC4D;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4DValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector4DValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
