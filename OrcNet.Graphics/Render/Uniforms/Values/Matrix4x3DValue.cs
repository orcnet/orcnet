﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Definition of the <see cref="Matrix4x3DValue"/> class.
    /// </summary>
    public class Matrix4x3DValue : APipelineMatrixValue<double>
    {
        #region Properties

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT4x3D;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4x3DValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        public Matrix4x3DValue(string pName) :
        base( pName, 4, 3 )
        {

        }

        #endregion Constructor
    }
}
