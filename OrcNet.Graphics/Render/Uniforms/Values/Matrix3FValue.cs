﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Definition of the <see cref="Matrix3FValue"/> class.
    /// </summary>
    public class Matrix3FValue : APipelineMatrix3Value<float>
    {
        #region Properties

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT3F;
            }
        }

        /// <summary>
        /// Gets or sets the Matrix3 values.
        /// </summary>
        public override Matrix3<float> Matrix
        {
            get
            {
                return new Matrix3F( this.Value[ 0 ], this.Value[ 1 ], this.Value[ 2 ], 
                                     this.Value[ 3 ], this.Value[ 4 ], this.Value[ 5 ], 
                                     this.Value[ 6 ], this.Value[ 7 ], this.Value[ 8 ] );
            }
            set
            {
                if ( value == null )
                {
                    return;
                }

                this.Value = value.Data;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3FValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        public Matrix3FValue(string pName) :
        base( pName )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3FValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        /// <param name="pMatrix">The matrix2 values.</param>
        public Matrix3FValue(string pName, Matrix3F pMatrix) :
        base( pName )
        {
            this.Matrix = pMatrix;
        }

        #endregion Constructor
    }
}
