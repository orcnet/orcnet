﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 2D Vector integer based uniform value.
    /// </summary>
    public class Vector2IValue : APipelineValue<Vector2I>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC2I;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2IValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector2IValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
