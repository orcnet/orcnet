﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Definition of the <see cref="Matrix4DValue"/> class.
    /// </summary>
    public class Matrix4DValue : APipelineMatrix4Value<double>
    {
        #region Properties

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT4D;
            }
        }

        /// <summary>
        /// Gets or sets the Matrix4 values.
        /// </summary>
        public override Matrix4<double> Matrix
        {
            get
            {
                return new Matrix4D( this.Value[ 0 ],  this.Value[ 1 ],  this.Value[ 2 ],  this.Value[ 3 ], 
                                     this.Value[ 4 ],  this.Value[ 5 ],  this.Value[ 6 ],  this.Value[ 7 ], 
                                     this.Value[ 8 ],  this.Value[ 9 ],  this.Value[ 10 ], this.Value[ 11 ], 
                                     this.Value[ 12 ], this.Value[ 13 ], this.Value[ 14 ], this.Value[ 15 ] );
            }
            set
            {
                if ( value == null )
                {
                    return;
                }

                this.Value = value.Data;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4DValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        public Matrix4DValue(string pName) :
        base( pName )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4DValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        /// <param name="pMatrix">The matrix2 values.</param>
        public Matrix4DValue(string pName, Matrix4D pMatrix) :
        base( pName )
        {
            this.Matrix = pMatrix;
        }

        #endregion Constructor
    }
}
