﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Unsigned integer based single primitive uniform value.
    /// </summary>
    public class UIntValue : APipelineValue<uint>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC1UI;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UIntValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public UIntValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
