﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 4D Vector unsigned integer based uniform value.
    /// </summary>
    public class Vector4UIValue : APipelineValue<Vector4UI>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC4UI;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4UIValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector4UIValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
