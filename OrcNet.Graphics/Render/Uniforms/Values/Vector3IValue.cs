﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 3D Vector integer based uniform value.
    /// </summary>
    public class Vector3IValue : APipelineValue<Vector3I>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC3I;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3IValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector3IValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
