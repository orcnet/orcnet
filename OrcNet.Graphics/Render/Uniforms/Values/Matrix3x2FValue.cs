﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Definition of the <see cref="Matrix3x2FValue"/> class.
    /// </summary>
    public class Matrix3x2FValue : APipelineMatrixValue<float>
    {
        #region Properties

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT3x2F;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3x2FValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        public Matrix3x2FValue(string pName) :
        base( pName, 3, 2 )
        {

        }

        #endregion Constructor
    }
}
