﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 4D Vector float based uniform value.
    /// </summary>
    public class Vector4FValue : APipelineValue<Vector4F>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC4F;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4FValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector4FValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
