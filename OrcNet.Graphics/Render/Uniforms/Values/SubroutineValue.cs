﻿using OrcNet.Core;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Subroutine uniform value class definition.
    /// </summary>
    public class SubroutineValue : AMemoryProfilable, IPipelineValue
    {
        #region Fields

        /// <summary>
        /// Stores the subroutine as value.
        /// </summary>
        private string mValue;

        /// <summary>
        /// Stores the uniform value name.
        /// </summary>
        private string mName;
        
        /// <summary>
        /// Stores the subroutine stage type (e.g: Vertex, Geometry, so on)
        /// </summary>
        private StageType mStage;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IUniformValue

        /// <summary>
        /// Gets the uniform value name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the uniform value type.
        /// </summary>
        public UniformType Type
        {
            get
            {
                return UniformType.SUBROUTINE;
            }
        }

        /// <summary>
        /// Gets the uniform untyped value.
        /// </summary>
        public object UntypedValue
        {
            get
            {
                return this.Value;
            }
        }

        #endregion Properties IUniformValue

        /// <summary>
        /// Gets or sets the value being the subroutine.
        /// </summary>
        public string Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                this.mValue = value;
            }
        }

        /// <summary>
        /// Gets the subroutine stage type (e.g: Vertex, Geometry, so on)
        /// </summary>
        public StageType Stage
        {
            get
            {
                return this.mStage;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SubroutineValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        /// <param name="pStage">The pipeline stage type for this subroutine.</param>
        public SubroutineValue(string pName, StageType pStage) :
        this( pName, pStage, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubroutineValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        /// <param name="pStage">The pipeline stage type for this subroutine.</param>
        /// <param name="pValue">The uniform value being the subroutine.</param>
        public SubroutineValue(string pName, StageType pStage, string pValue)
        {
            this.mName  = pName;
            this.mValue = pValue;
            this.mStage = pStage;
        }

        #endregion Constructor
    }
}
