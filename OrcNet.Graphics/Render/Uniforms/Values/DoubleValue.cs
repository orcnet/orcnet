﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Double based single primitive uniform value.
    /// </summary>
    public class DoubleValue : APipelineValue<double>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC1D;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DoubleValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public DoubleValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
