﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 3D Vector double based uniform value.
    /// </summary>
    public class Vector3DValue : APipelineValue<Vector3D>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC3D;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3DValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector3DValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
