﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Definition of the <see cref="Matrix2FValue"/> class.
    /// </summary>
    public class Matrix2FValue : APipelineMatrix2Value<float>
    {
        #region Properties

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT2F;
            }
        }

        /// <summary>
        /// Gets or sets the Matrix2 values.
        /// </summary>
        public override Matrix2<float> Matrix
        {
            get
            {
                return new Matrix2F( this.Value[ 0 ], this.Value[ 1 ], 
                                     this.Value[ 2 ], this.Value[ 3 ] );
            }
            set
            {
                if ( value == null )
                {
                    return;
                }

                this.Value = value.Data;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2FValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        public Matrix2FValue(string pName) :
        base( pName )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2FValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        /// <param name="pMatrix">The matrix2 values.</param>
        public Matrix2FValue(string pName, Matrix2F pMatrix) :
        base( pName )
        {
            this.Matrix = pMatrix;
        }

        #endregion Constructor
    }
}
