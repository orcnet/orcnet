﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 3D Vector unsigned integer based uniform value.
    /// </summary>
    public class Vector3UIValue : APipelineValue<Vector3UI>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC3UI;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3UIValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector3UIValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
