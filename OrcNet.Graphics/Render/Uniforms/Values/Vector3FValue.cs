﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 3D Vector float based uniform value.
    /// </summary>
    public class Vector3FValue : APipelineValue<Vector3F>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC3F;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3FValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector3FValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
