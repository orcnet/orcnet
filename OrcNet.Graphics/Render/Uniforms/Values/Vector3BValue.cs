﻿using OrcNet.Graphics.Render.Uniforms.Generics;
using System;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 3D Vector boolean based uniform value.
    /// </summary>
    public class Vector3BValue : APipelineValue<Tuple<bool, bool, bool>>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC3B;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3BValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector3BValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
