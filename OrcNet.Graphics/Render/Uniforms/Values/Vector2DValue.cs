﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 2D Vector double based uniform value.
    /// </summary>
    public class Vector2DValue : APipelineValue<Vector2D>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC2D;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2DValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector2DValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
