﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Definition of the <see cref="Matrix2x4FValue"/> class.
    /// </summary>
    public class Matrix2x4FValue : APipelineMatrixValue<float>
    {
        #region Properties

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT2x4F;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2x4FValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        public Matrix2x4FValue(string pName) :
        base( pName, 2, 4 )
        {
            
        }
        
        #endregion Constructor
    }
}
