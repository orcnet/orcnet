﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 2D Vector float based uniform value.
    /// </summary>
    public class Vector2FValue : APipelineValue<Vector2F>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC2F;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2FValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector2FValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
