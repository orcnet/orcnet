﻿using OrcNet.Graphics.Render.Uniforms.Generics;
using System;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 2D Vector boolean based uniform value.
    /// </summary>
    public class Vector2BValue : APipelineValue<Tuple<bool, bool>>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC2B;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2BValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector2BValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
