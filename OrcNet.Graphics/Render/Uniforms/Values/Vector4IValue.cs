﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 4D Vector integer based uniform value.
    /// </summary>
    public class Vector4IValue : APipelineValue<Vector4I>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC4I;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4IValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector4IValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
