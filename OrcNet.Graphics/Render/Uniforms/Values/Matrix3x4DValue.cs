﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Definition of the <see cref="Matrix3x4DValue"/> class.
    /// </summary>
    public class Matrix3x4DValue : APipelineMatrixValue<double>
    {
        #region Properties

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT3x4D;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3x4DValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        public Matrix3x4DValue(string pName) :
        base( pName, 3, 4 )
        {

        }

        #endregion Constructor
    }
}
