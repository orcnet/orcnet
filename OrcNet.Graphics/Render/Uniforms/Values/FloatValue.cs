﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Float based single primitive uniform value.
    /// </summary>
    public class FloatValue : APipelineValue<float>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC1F;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public FloatValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
