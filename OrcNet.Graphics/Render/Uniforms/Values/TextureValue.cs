﻿using OrcNet.Core;
using OrcNet.Graphics.Render.Textures;
using System;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Texture uniform value class definition.
    /// </summary>
    public class TextureValue : AMemoryProfilable, IPipelineValue, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the texture as value.
        /// </summary>
        private ITexture mValue;

        /// <summary>
        /// Stores the uniform value name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the uniform value type.
        /// </summary>
        private UniformType mType;

        #endregion Fields

        #region Properties

        #region Properties IUniformValue

        /// <summary>
        /// Gets the uniform value name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the uniform value type.
        /// </summary>
        public UniformType Type
        {
            get
            {
                return this.mType;
            }
        }

        /// <summary>
        /// Gets the uniform untyped value.
        /// </summary>
        public object UntypedValue
        {
            get
            {
                return this.Value;
            }
        }

        #endregion Properties IUniformValue

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += string.IsNullOrEmpty(this.mName) ? 0 : sizeof(char) * (uint)this.mName.Length;
                lSize += sizeof(UniformType);
                if
                    ( this.mValue != null )
                {
                    lSize += this.mValue.Size;
                }
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets or sets the value being the texture.
        /// </summary>
        public ITexture Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                this.mValue = value;
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        /// <param name="pType">The uniform type.</param>
        /// <param name="pValue">The uniform value.</param>
        public TextureValue(string pName, UniformType pType, ITexture pValue)
        {
            this.mName  = pName;
            this.mType  = pType;
            this.mValue = pValue;
        }

        #endregion Constructor

        #region Methods

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Releases resources (overridable)
        /// </summary>
        protected virtual void OnDispose()
        {
            this.mValue = null;
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
