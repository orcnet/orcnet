﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Boolean based single primitive uniform value.
    /// </summary>
    public class BoolValue : APipelineValue<bool>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC1B;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BoolValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public BoolValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
