﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Definition of the <see cref="Matrix2x3IValue"/> class.
    /// </summary>
    public class Matrix2x3IValue : APipelineMatrixValue<int>
    {
        #region Properties

        /// <summary>
        /// Gets the pipeline value type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.MAT2x3I;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2x3IValue"/> class.
        /// </summary>
        /// <param name="pName">The pipeline value name.</param>
        public Matrix2x3IValue(string pName) :
        base( pName, 2, 3 )
        {
            
        }
        
        #endregion Constructor
    }
}
