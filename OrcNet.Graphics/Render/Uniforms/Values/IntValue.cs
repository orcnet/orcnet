﻿using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// Integer based single primitive uniform value.
    /// </summary>
    public class IntValue : APipelineValue<int>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC1I;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="IntValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public IntValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
