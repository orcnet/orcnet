﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;

namespace OrcNet.Graphics.Render.Uniforms.Values
{
    /// <summary>
    /// 2D Vector unsigned integer based uniform value.
    /// </summary>
    public class Vector2UIValue : APipelineValue<Vector2UI>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC2UI;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2UIValue"/> class.
        /// </summary>
        /// <param name="pName">The uniform name.</param>
        public Vector2UIValue(string pName) :
        base( pName )
        {

        }

        #endregion Constructor
    }
}
