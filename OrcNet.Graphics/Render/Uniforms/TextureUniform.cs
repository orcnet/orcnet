﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Service;
using OrcNet.Graphics.Helpers;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms.Values;
using OrcNet.Graphics.Services;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// Texture uniform holding a texture value and could also
    /// have a sampler to modify the texture sampling parameters.
    /// </summary>

    [DebuggerDisplay("UnitId = {UnitId}, Sampler = {Sampler}, Value = {Value}")]
    public class TextureUniform : AMemoryProfilable, IPipelineUniform, IDisposable
    {
        #region Fields

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Stores the flag indicating whether this uniform 
        /// is not up to date or not.
        /// </summary>
        private bool mIsDirty;
#endif

        /// <summary>
        /// Stores the uniform's current sampler used for sampling the texture.
        /// </summary>
        private ISampler mSampler;

        /// <summary>
        /// Stores the uniform's current texture as value.
        /// </summary>
        private ITexture mValue;

        /// <summary>
        /// Stores the uniform name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the uniform's current texture unit identifier
        /// </summary>
        private int    mUnitId;

        /// <summary>
        /// Stores the uniform type.
        /// </summary>
        private UniformType mType;

        /// <summary>
        /// Stores the uniform's owner
        /// </summary>
        protected PipelinePass mOwner;

        /// <summary>
        /// Stores the GPU uniform location (or offset if in a block)
        /// </summary>
        protected int          mLocation;

        /// <summary>
        /// Stores the block this uniform belongs to if not Null.
        /// </summary>
        protected UniformBlock mBlock;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(int) * 2;
                lSize += string.IsNullOrEmpty( this.mName ) ? 0 : sizeof(char) * (uint)this.mName.Length;
                lSize += sizeof(UniformType);
                if
                    ( this.mSampler != null )
                {
                    lSize += this.mSampler.Size;
                }
                if
                    ( this.mValue != null )
                {
                    lSize += this.mValue.Size;
                }
                if
                    ( this.mOwner != null )
                {
                    lSize += this.mOwner.Size;
                }
                if
                    ( this.mBlock != null )
                {
                    lSize += this.mBlock.Size;
                }
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IPipelineUniform

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Gets or sets the flag indicating whether this uniform 
        /// is not up to date or not.
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return this.mIsDirty;
            }
            set
            {
                this.mIsDirty = value;
            }
        }
#endif

        /// <summary>
        /// Gets or sets the uniform's owner
        /// </summary>
        public PipelinePass Owner
        {
            get
            {
                return this.mOwner;
            }
            set
            {
                this.mOwner = value;
            }
        }

        /// <summary>
        /// Gets or sets the block this uniform belongs to if not Null.
        /// </summary>
        public UniformBlock Block
        {
            get
            {
                return this.mBlock;
            }
            set
            {
                this.mBlock = value;
            }
        }

        /// <summary>
        /// Gets the uniform name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public UniformType Type
        {
            get
            {
                return this.mType;
            }
        }

        /// <summary>
        /// Sets the uniform untyped value.
        /// </summary>
        public IPipelineValue UntypedValue
        {
            set
            {
                TextureValue lValue = value as TextureValue;
                if
                    ( lValue != null )
                {
                    this.Value = lValue.Value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the uniform stage location (or offset if in a block)
        /// </summary>
        public int Location
        {
            get
            {
                return this.mLocation;
            }
            set
            {
                this.mLocation = value;
            }
        }

        #endregion Properties IPipelineUniform

        /// <summary>
        /// Gets or sets the uniform's current texture unit identifier
        /// </summary>
        public int UnitId
        {
            get
            {
                return this.mUnitId;
            }
            set
            {
                this.mUnitId = value;
            }
        }

        /// <summary>
        /// Gets the uniform's current sampler used for sampling the texture.
        /// </summary>
        public ISampler Sampler
        {
            get
            {
                return this.mSampler;
            }
            set
            {
                this.mSampler = value;
                if
                    ( this.mOwner != null &&
                      this.mOwner.IsCurrent )
                {
                    this.SendUniform();
                }
            }
        }

        /// <summary>
        /// Gets or sets the uniform's current texture as value.
        /// </summary>
        public ITexture Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                if
                    ( this.mOwner != null )
                {
                    if
                        ( this.mValue != null )
                    {
                        this.mValue.RemoveUser( this.mOwner.Id );
                    }
                }

                this.mValue = value;

                if
                    ( this.mOwner != null )
                {
                    if
                        ( this.mValue != null )
                    {
                        this.mValue.AddUser( this.mOwner.Id );
                    }

                    if
                        ( this.mOwner.IsCurrent )
                    {
                        this.SendUniform();
                    }
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureUniform"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pType">The uniform's type.</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        public TextureUniform(PipelinePass pOwner, string pName, UniformType pType, UniformBlock pBlock = null, int pLocation = -1)
        {
            this.mUnitId = -1;
            this.mOwner  = pOwner;
            this.mName   = pName;
            this.mType   = pType;
            this.mBlock  = pBlock;
            this.mLocation = pLocation;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public void SendUniform()
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( this.mValue != null &&
                 this.mLocation != -1 &&
                 lRenderService != null &&
                 lRenderService.CurrentPass != null )
            {
                int lAvailableUnitId = TextureUnitManager.Instance.BindToAvailableUnit( this.mValue, this.mSampler, lRenderService.CurrentPass.PassIds );
                if ( lAvailableUnitId >= 0 &&
                     lAvailableUnitId != this.mUnitId )
                {
#if (ORK_NO_GLPROGRAMUNIFORM)
                    if ( lRenderService.CurrentPass.PipelineId > 0 )
                    {
                        GL.ActiveShaderProgram( lRenderService.CurrentPass.PipelineId, lRenderService.CurrentPass.PassId );
                    }
                    GL.Uniform1( this.mLocation, lAvailableUnitId );
#else
                    GL.ProgramUniform1( this.mOwner.Id, this.mLocation, lAvailableUnitId );
#endif
                    GLHelpers.GetError();
                    this.mUnitId = lAvailableUnitId;
                }
            }
            else
            {
                this.mUnitId = -1;
            }
        }

#if (ORK_NO_GLPROGRAMUNIFORM)
        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call but ONLY if its pass is being used so current.
        /// </summary>
        public void SendUniformIfCurrent()
        {
            if ( this.mBlock != null )
            {
                return;
            }

            if ( this.mOwner.IsCurrent )
            {
                IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                if ( lRenderService != null &&
                     lRenderService.CurrentPass.PipelineId > 0 )
                {
                    GL.ActiveShaderProgram( lRenderService.CurrentPass.PipelineId, this.mOwner.PassId );
                }
                this.SendUniform();
                this.mIsDirty = false;
            }
            else
            {
                this.mIsDirty = true;
            }
        }
#endif

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.mOwner = null;
            this.mBlock = null;
            this.mValue = null;
            this.mSampler = null;

            GC.SuppressFinalize( this );
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
