﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Math;
using OrcNet.Graphics.Render.Uniforms.Generics;
using System;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// 2D Vector integer based uniform class definition.
    /// </summary>
    public class Vector2IUniform : APipelineUniform<Vector2I>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC2I;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2IUniform"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        public Vector2IUniform(PipelinePass pOwner, string pName, UniformBlock pBlock = null, int pLocation = -1) : 
        base( pOwner, pName, pBlock, pLocation )
        {
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <returns>The uniform value</returns>
        protected override Vector2I MapsUniform(int pOffset)
        {
            if ( this.mBlock != null )
            {
                int[] lResult = new int[ 2 ];
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( lDataPtr, lResult, 0, 2 );
                return new Vector2I( lResult[ 0 ], lResult[ 1 ] );
            }

            return Vector2I.ZERO;
        }

        /// <summary>
        /// Sets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pValue">The value to pass into the uniform block buffer memory</param>
        protected override void MapsUniform(int pOffset, Vector2I pValue)
        {
            if ( this.mBlock != null )
            {
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( new int[ 2 ] { pValue.X, pValue.Y }, 0, lDataPtr, 2 );
            }
        }

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public override void SendUniform()
        {
#if (ORK_NO_GLPROGRAMUNIFORM)
            GL.Uniform2( this.mLocation, this.mValue.X, this.mValue.Y );
#else
            GL.ProgramUniform2( this.mOwner.Id, this.mLocation, this.mValue.X, this.mValue.Y );
#endif
        }

        #endregion Methods
    }
}
