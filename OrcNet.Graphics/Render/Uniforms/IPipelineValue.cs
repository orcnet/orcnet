﻿using OrcNet.Core;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// Base pipeline value interface definition.
    /// </summary>
    public interface IPipelineValue : IMemoryProfilable
    {
        #region Properties

        /// <summary>
        /// Gets the uniform untyped value.
        /// </summary>
        object UntypedValue
        {
            get;
        }

        /// <summary>
        /// Gets the uniform value type.
        /// </summary>
        UniformType Type
        {
            get;
        }

        /// <summary>
        /// Gets the uniform value name.
        /// </summary>
        string Name
        {
            get;
        }

        #endregion Properties
    }
}
