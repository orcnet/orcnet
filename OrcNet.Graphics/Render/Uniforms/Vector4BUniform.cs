﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Render.Uniforms.Generics;
using System;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Render.Uniforms
{
    /// <summary>
    /// 4D Vector boolean based uniform class definition.
    /// </summary>
    public class Vector4BUniform : APipelineUniform<Tuple<bool, bool, bool, bool>>
    {
        #region Properties

        /// <summary>
        /// Gets the uniform type.
        /// </summary>
        public override UniformType Type
        {
            get
            {
                return UniformType.VEC4B;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4BUniform"/> class.
        /// </summary>
        /// <param name="pOwner">The uniform's owner.</param>
        /// <param name="pName">The uniform's GPU name</param>
        /// <param name="pBlock">The block to which the uniform belongs to if not Null.</param>
        /// <param name="pLocation">The uniform GPU location which is an offset if in an uniform block.</param>
        public Vector4BUniform(PipelinePass pOwner, string pName, UniformBlock pBlock = null, int pLocation = -1) : 
        base( pOwner, pName, pBlock, pLocation )
        {
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <returns>The uniform value</returns>
        protected override Tuple<bool, bool, bool, bool> MapsUniform(int pOffset)
        {
            if ( this.mBlock != null )
            {
                int[] lResult = new int[ 4 ];
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( lDataPtr, lResult, 0, 4 );
                return new Tuple<bool, bool, bool, bool>( lResult[ 0 ] == 1 ? true : false, 
                                                          lResult[ 1 ] == 1 ? true : false, 
                                                          lResult[ 2 ] == 1 ? true : false, 
                                                          lResult[ 3 ] == 1 ? true : false );
            }

            return new Tuple<bool, bool, bool, bool>( false, false, false, false );
        }

        /// <summary>
        /// Sets the uniform's value from the uniform block's GPU buffer.
        /// </summary>
        /// <param name="pOffset">The location offset of the uniform to get in the block.</param>
        /// <param name="pValue">The value to pass into the uniform block buffer memory</param>
        protected override void MapsUniform(int pOffset, Tuple<bool, bool, bool, bool> pValue)
        {
            if ( this.mBlock != null )
            {
                IntPtr lDataPtr = this.mBlock.MapBuffer( pOffset );
                Marshal.Copy( new int[ 4 ] { pValue.Item1 ? 1 : 0, pValue.Item2 ? 1 : 0, pValue.Item3 ? 1 : 0, pValue.Item4 ? 1 : 0 }, 0, lDataPtr, 4 );
            }
        }

        /// <summary>
        /// Sends the uniform to the GPU using the appropriate GL call.
        /// </summary>
        public override void SendUniform()
        {
#if (ORK_NO_GLPROGRAMUNIFORM)
            GL.Uniform4( this.mLocation, this.mValue.Item1 ? 1 : 0, this.mValue.Item2 ? 1 : 0, this.mValue.Item3 ? 1 : 0, this.mValue.Item4 ? 1 : 0 );
#else
            GL.ProgramUniform4( this.mOwner.Id, this.mLocation, this.mValue.Item1 ? 1 : 0, this.mValue.Item2 ? 1 : 0, this.mValue.Item3 ? 1 : 0, this.mValue.Item4 ? 1 : 0 );
#endif
        }

        #endregion Methods
    }
}
