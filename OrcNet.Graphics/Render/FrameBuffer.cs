﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Logger;
using OrcNet.Core.Render;
using OrcNet.Core.Service;
using OrcNet.Graphics.Helpers;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Frame buffer class definition which will be either the default or a frame buffer of a set.
    /// Each frame buffer has its own state owning attachments (such as render buffers) and fixed functions
    /// parameters.
    /// </summary>
    [DebuggerDisplay("Id = {Id}, State = {State}, ReadBuffer = {ReadBuffer}, DrawBufferCount = {DrawBufferCount}")]
    public class FrameBuffer : IFrameBuffer
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores a quad mesh.
        /// </summary>
        private static Mesh<Vertex4, uint> sQuad;

        /// <summary>
        /// Stores the current frame buffer state.
        /// </summary>
        private FrameBufferState mState;

        /// <summary>
        /// Stores the frame buffer Id.
        /// </summary>
        private uint mFrameBufferId;

        /// <summary>
        /// Stores the attachments of this frame buffer.
        /// </summary>
        private object[] mAttachments;

        /// <summary>
        /// Stores the levels specified for each attachments of this frame buffer.
        /// </summary>
        private int[] mLevels;

        /// <summary>
        /// Stores the layers specified for each attachments of this frame buffer.
        /// Only used for Texture Array/Cube/3D.
        /// </summary>
        private int[] mLayers;
        
        /// <summary>
        /// Stores the read buffer.
        /// </summary>
        private BufferId mReadBuffer;

        /// <summary>
        /// Stores the number of draw buffers.
        /// </summary>
        private int mDrawBufferCount;

        /// <summary>
        /// Stores the draw buffers.
        /// </summary>
        private BufferId[] mDrawBuffers;
        
        /// <summary>
        /// Stores the frame buffer parameters.
        /// </summary>
        private FrameBufferParameters mParameters;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the frame buffer Id.
        /// </summary>
        public uint Id
        {
            get
            {
                return this.mFrameBufferId;
            }
        }

        /// <summary>
        /// Gets the read buffer.
        /// </summary>
        public BufferId ReadBuffer
        {
            get
            {
                return this.mReadBuffer;
            }
        }

        /// <summary>
        /// Gets the draw buffers.
        /// </summary>
        public IEnumerable<BufferId> DrawBuffers
        {
            get
            {
                return this.mDrawBuffers;
            }
        }

        /// <summary>
        /// Gets the draw buffer at the given Index.
        /// </summary>
        /// <param name="pIndex">The buffer id</param>
        /// <returns>The buffer attachment id.</returns>
        public BufferId this[uint pIndex]
        {
            get
            {
                if
                    ( pIndex >= this.mDrawBufferCount )
                {
                    return BufferId.NONE;
                }

                int lIndex = (int)pIndex;
                return this.mDrawBuffers[ lIndex ];
            }
        }

        /// <summary>
        /// Gets the number of draw buffers.
        /// </summary>
        public int DrawBufferCount
        {
            get
            {
                return this.mDrawBufferCount;
            }
        }

        /// <summary>
        /// Gets the current frame buffer state.
        /// </summary>
        public FrameBufferState State
        {
            get
            {
                return this.mState;
            }
        }

        /// <summary>
        /// Gets the frame buffer parameters.
        /// </summary>
        public FrameBufferParameters Parameters
        {
            get
            {
                return this.mParameters;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameBuffer"/> class.
        /// </summary>
        /// <param name="pIsMain">The flag indicating whether the frame buffer is the main one or not.</param>
        internal FrameBuffer(bool pIsMain)
        {
            this.mState  = FrameBufferState.UpToDate;
            this.mAttachments = new object[ 10 ];
            this.mLevels = new int[ 10 ];
            this.mLayers = new int[ 10 ];
            this.mDrawBuffers = new BufferId[ 8 ];
            this.mParameters = new FrameBufferParameters();
            this.mParameters.AddPropertyObserver( this );

            if ( pIsMain )
            {
                this.mFrameBufferId = 0;
            }
            else
            {
                GL.GenFramebuffers( 1, out this.mFrameBufferId );
                GLHelpers.GetError();
            }

            this.mReadBuffer = BufferId.COLOR0;
            this.mDrawBufferCount = 1;
            this.mDrawBuffers[ 0 ] = BufferId.COLOR0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameBuffer"/> class.
        /// </summary>
        public FrameBuffer()
        {
            this.mState  = FrameBufferState.UpToDate;
            this.mAttachments = new object[ 10 ];
            this.mLevels = new int[ 10 ];
            this.mLayers = new int[ 10 ];
            this.mDrawBuffers = new BufferId[ 8 ];
            this.mParameters = new FrameBufferParameters();
            this.mParameters.AddPropertyObserver( this );

            GL.GenFramebuffers( 1, out this.mFrameBufferId );
            GLHelpers.GetError();

            for ( int lCurr = 0; lCurr < 10; lCurr++ )
            {
                this.mAttachments[ lCurr ] = null;
                this.mLevels[ lCurr ] = 0;
                this.mLayers[ lCurr ] = 0;
            }

            this.mReadBuffer = BufferId.COLOR0;
            this.mDrawBufferCount = 1;
            this.mDrawBuffers[ 0 ] = BufferId.COLOR0;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the requested attachment to the given attachment point.
        /// </summary>
        /// <typeparam name="T">The type to retrieve</typeparam>
        /// <param name="pAttachmentPoint">The attachment point.</param>
        /// <returns>The attachment (Texture or RenderBuffer for instance), Null otherwise.</returns>
        public T GetAttachment<T>(BufferId pAttachmentPoint) where T : class
        {
            return this.mAttachments[ pAttachmentPoint.ToBufferIndex() ] as T;
        }
        
        /// <summary>
        /// Attaches an attachment to the given attachment point.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pAttachmentPoint">The attachment point.</param>
        /// <param name="pAttachment">The attachment to store.</param>
        public void Attach<T>(BufferId pAttachmentPoint, T pAttachment) where T : class
        {
            if ( this.mFrameBufferId != 0 )
            {
                int lIndex = pAttachmentPoint.ToBufferIndex();
                this.mAttachments[ lIndex ] = pAttachment;
                this.mState |= FrameBufferState.AttachmentsChanged;
            }
        }

        /// <summary>
        /// Attaches an attachment to the given attachment point.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pAttachmentPoint">The attachment point.</param>
        /// <param name="pAttachment">The attachment to store.</param>
        /// <param name="pLevel">The LOD level</param>
        public void Attach<T>(BufferId pAttachmentPoint, T pAttachment, int pLevel) where T : class
        {
            if ( this.mFrameBufferId != 0 )
            {
                int lIndex = pAttachmentPoint.ToBufferIndex();
                this.mAttachments[ lIndex ] = pAttachment;
                this.mLevels[ lIndex ] = pLevel;
                this.mState |= FrameBufferState.AttachmentsChanged;
            }
        }

        /// <summary>
        /// Attaches an attachment to the given attachment point.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pAttachmentPoint">The attachment point.</param>
        /// <param name="pAttachment">The attachment to store.</param>
        /// <param name="pLevel">The LOD level to be attached.</param>
        /// <param name="pLayer">The layer to be attached (-1 to attach all layers)</param>
        public void Attach<T>(BufferId pAttachmentPoint, T pAttachment, int pLevel, int pLayer) where T : class
        {
            if ( this.mFrameBufferId != 0 )
            {
                int lIndex = pAttachmentPoint.ToBufferIndex();
                this.mAttachments[ lIndex ] = pAttachment;
                this.mLevels[ lIndex ] = pLevel;
                this.mLayers[ lIndex ] = pLayer;
                this.mState |= FrameBufferState.AttachmentsChanged;
            }
        }
        
        /// <summary>
        /// Uses the attachment corresponding to the given attachment point as a Read buffer for
        /// ReadPixels and CopyPixels calls.
        /// </summary>
        /// <param name="pAttachmentPoint">The attachment point given an attachment to use.</param>
        public void UseAsReadBuffer(BufferId pAttachmentPoint)
        {
            this.mReadBuffer = pAttachmentPoint;
            this.mState |= FrameBufferState.ReadDrawBuffersChanged;
        }

        /// <summary>
        /// Uses a single draw buffer given the attachment point of the Draw buffer for
        /// Clear and Draw calls.
        /// </summary>
        /// <param name="pAttachmentPoint">The attachment point given an attachment to use. (e.g: One of the color buffer)</param>
        public void UseSingleDrawBuffer(BufferId pAttachmentPoint)
        {
            this.mDrawBufferCount = 1;
            this.mDrawBuffers[ 0 ] = pAttachmentPoint;
            this.mState |= FrameBufferState.ReadDrawBuffersChanged;
        }

        /// <summary>
        /// Uses multiple draw buffers given the set of attachment point(s) of the Draw buffers for
        /// Clear and Draw calls.
        /// </summary>
        /// <param name="pORedAttachmentPoints">The attachment point(s) ORed given (an) attachment(s) to use. (e.g: A set of the color buffer(s))</param>
        public void UseMultipleDrawBuffers(BufferId pORedAttachmentPoints)
        {
            this.mDrawBufferCount = 0;

            if ((pORedAttachmentPoints & BufferId.COLOR0) != 0)
            {
                this.mDrawBuffers[ this.mDrawBufferCount++ ] = BufferId.COLOR0;
            }
            if ((pORedAttachmentPoints & BufferId.COLOR1) != 0)
            {
                this.mDrawBuffers[ this.mDrawBufferCount++] = BufferId.COLOR1;
            }
            if ((pORedAttachmentPoints & BufferId.COLOR2) != 0)
            {
                this.mDrawBuffers[ this.mDrawBufferCount++] = BufferId.COLOR2;
            }
            if ((pORedAttachmentPoints & BufferId.COLOR3) != 0)
            {
                this.mDrawBuffers[ this.mDrawBufferCount++] = BufferId.COLOR3;
            }
            if ((pORedAttachmentPoints & BufferId.COLOR4) != 0)
            {
                this.mDrawBuffers[ this.mDrawBufferCount++] = BufferId.COLOR4;
            }
            if ((pORedAttachmentPoints & BufferId.COLOR5) != 0)
            {
                this.mDrawBuffers[ this.mDrawBufferCount++] = BufferId.COLOR5;
            }
            if ((pORedAttachmentPoints & BufferId.COLOR6) != 0)
            {
                this.mDrawBuffers[ this.mDrawBufferCount++] = BufferId.COLOR6;
            }
            if ((pORedAttachmentPoints & BufferId.COLOR7) != 0)
            {
                this.mDrawBuffers[ this.mDrawBufferCount++] = BufferId.COLOR7;
            }

            this.mState |= FrameBufferState.ReadDrawBuffersChanged;
        }

        /// <summary>
        /// Draws the given mesh.
        /// </summary>
        /// <typeparam name="V">The vertex type.</typeparam>
        /// <typeparam name="I">The index type.</typeparam>
        /// <param name="pPass">The pipeline pass to use for drawing.</param>
        /// <param name="pMesh">The mesh to draw.</param>
        /// <param name="pPrimCount">The number of times the mesh must be drawn (with geometry instancing).</param>
        public void Draw<V, I>(PipelinePass pPass, Mesh<V, I> pMesh, int pPrimCount = 1) where V : struct 
                                                                                         where I : struct
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null &&
                 lRenderService.CurrentTransformFeedbackPass == null )
            {
                lRenderService.UseFrameBuffer( this );
                lRenderService.UsePass( pPass );

                this.EnableConditionalRender();

                lRenderService.Draw( pMesh.Buffers, pMesh.Mode, 0, pMesh.IndexCount == 0 ? pMesh.VertexCount : pMesh.IndexCount, pPrimCount, 0 );

                this.DisableConditionalRender();
            }
        }

        /// <summary>
        /// Draws a part of a mesh one or multiple times.
        /// </summary>
        /// <param name="pPass">The pipeline pass to use for drawing.</param>
        /// <param name="pMesh">The mesh info to use for drawing.</param>
        /// <param name="pMode">The mesh topology describing how to interpret vertices.</param>
        /// <param name="pFirstToDraw">The first vertex or index to use for drawing.</param>
        /// <param name="pCountToDraw">The amount of vertices or indices to use for drawing.</param>
        /// <param name="pPrimCount">The number of times the mesh must be drawn (with geometry instancing).</param>
        /// <param name="pBase">The base vertex to use if using Indices ONLY.</param>
        public void Draw(PipelinePass pPass, MeshBuffers pMesh, PrimitiveType pMode, int pFirstToDraw, int pCountToDraw, int pPrimCount = 1, int pBase = 0)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null &&
                 lRenderService.CurrentTransformFeedbackPass == null )
            {
                lRenderService.UseFrameBuffer( this );
                lRenderService.UsePass( pPass );

                LogManager.Instance.Log( string.Format( "Draw mesh ({0} vertices)...", pCountToDraw ) );

                this.EnableConditionalRender();

                lRenderService.Draw( pMesh, pMode, pFirstToDraw, pCountToDraw, pPrimCount, pBase );

                this.DisableConditionalRender();
            }
        }

        /// <summary>
        /// Draws several parts of a mesh each part being specified with a first starting element index and a count.
        /// </summary>
        /// <param name="pPass">The pipeline pass to use for drawing.</param>
        /// <param name="pMesh">The mesh info to use for drawing.</param>
        /// <param name="pMode">The mesh topology describing how to interpret vertices.</param>
        /// <param name="pFirstsToDraw">An array of pPrimCount starting vertex or index to use for drawing.</param>
        /// <param name="pCountsToDraw">An array of pPrimCount amount of vertices or indices to use for drawing.</param>
        /// <param name="pPrimCount">The number of times the mesh must be drawn (with geometry instancing).</param>
        /// <param name="pBases">The base vertices to use if using Indices ONLY.</param>
        public void MultiDraw(PipelinePass pPass, MeshBuffers pMesh, PrimitiveType pMode, int[] pFirstsToDraw, int[] pCountsToDraw, int pPrimCount, int[] pBases = null)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null &&
                 lRenderService.CurrentTransformFeedbackPass == null )
            {
                lRenderService.UseFrameBuffer( this );
                lRenderService.UsePass( pPass );

                LogManager.Instance.Log( string.Format( "Multidraw mesh ({0} instances)...", pPrimCount ) );

                this.EnableConditionalRender();

                lRenderService.MultiDraw( pMesh, pMode, pFirstsToDraw, pCountsToDraw, pPrimCount, pBases );

                this.DisableConditionalRender();
            }
        }

        /// <summary>
        /// Draws a part of a mesh one or more times.
        /// Only available with OpenGL >= 4.0.
        /// </summary>
        /// <param name="pPass">The pipeline pass to use for drawing.</param>
        /// <param name="pMesh">The mesh info to use for drawing.</param>
        /// <param name="pMode">The mesh topology describing how to interpret vertices.</param>
        /// <param name="pBuffer">The CPU or GPU buffer containing the count, primCount, starting and base info in that order followed by 0 as 32 bits Int.</param>
        public void DrawIndirect(PipelinePass pPass, MeshBuffers pMesh, PrimitiveType pMode, IBuffer pBuffer)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null &&
                 lRenderService.CurrentTransformFeedbackPass == null )
            {
                lRenderService.UseFrameBuffer( this );
                lRenderService.UsePass( pPass );

                LogManager.Instance.Log( "Draw indirect..." );

                this.EnableConditionalRender();

                lRenderService.DrawIndirect( pMesh, pMode, pBuffer );

                this.DisableConditionalRender();
            }
        }

        /// <summary>
        /// Draws the mesh with a vertex count resulting from a transform feedback session.
        /// Only available with OpenGL >= 4.0.
        /// </summary>
        /// <param name="pPass">The pipeline pass to use for drawing.</param>
        /// <param name="pMesh">The mesh info to use for drawing.</param>
        /// <param name="pMode">The mesh topology describing how to interpret vertices.</param>
        /// <param name="pFeedback">The buffers in which storing feedback session results.</param>
        /// <param name="pStream">The stream to draw.</param>
        public void DrawFeedback(PipelinePass pPass, MeshBuffers pMesh, PrimitiveType pMode, TransformFeedback pFeedback, uint pStream = 0)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null &&
                 lRenderService.CurrentTransformFeedbackPass == null &&
                 pFeedback.Id != 0 )
            {
                lRenderService.UseFrameBuffer( this );
                lRenderService.UsePass( pPass );

                LogManager.Instance.Log( "Draw feedback..." );

                this.EnableConditionalRender();

                lRenderService.DrawFeedback( pMesh, pMode, pFeedback.Id, pStream );

                this.DisableConditionalRender();
            }
        }

        /// <summary>
        /// Draws a quad mesh having a position attribute made of 4 floats whose 
        /// xy coordinates vary from -1 to 1 and zw coordinates from 0 to 1.
        /// </summary>
        /// <param name="pPass">The pipeline pass to use for drawing.</param>
        public void DrawQuad(PipelinePass pPass)
        {
            if ( sQuad == null )
            {
                sQuad = new Mesh<Vertex4, uint>( PrimitiveType.TriangleStrip, MeshUsage.GPU_STATIC );
                sQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false ); // Position as Float4.
                sQuad.AddVertex( new Vertex4( -1.0f, -1.0f, 0.0f, 1.0f ) );
                sQuad.AddVertex( new Vertex4(  1.0f, -1.0f, 0.0f, 1.0f ) );
                sQuad.AddVertex( new Vertex4( -1.0f,  1.0f, 0.0f, 1.0f ) );
                sQuad.AddVertex( new Vertex4(  1.0f,  1.0f, 0.0f, 1.0f ) );
            }

            this.Draw( pPass, sQuad );
        }

        /// <summary>
        /// Reads pixels from the attached color buffers into the given buffer.
        /// </summary>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area to read.</param>
        /// <param name="pHeight">The height of the area to read.</param>
        /// <param name="pFormat">The pixels format in the destination buffer.</param>
        /// <param name="pType">The pixels components type in the destination buffer.</param>
        /// <param name="pParameters">The optional pixel storage parameters for the destination buffer.</param>
        /// <param name="pBuffer">The read pixels destination buffer</param>
        /// <param name="pClamp">The flag indicating whether to clamp read colors to the range 0-1 or not.</param>
        public void ReadPixels(int pX, int pY, int pWidth, int pHeight, PixelFormat pFormat, PixelType pType, BufferLayoutParameters pParameters, IBuffer pBuffer, bool pClamp = false)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                LogManager.Instance.Log( string.Format( "Read {0} pixels...", pWidth * pHeight ) );

                lRenderService.UseFrameBuffer( this );

                int lBufferFlag = (int)All.PixelPackBuffer;
                pBuffer.Bind( lBufferFlag );
                pParameters.Apply();

                GL.ClampColor( ClampColorTarget.ClampReadColor, pClamp ? ClampColorMode.True : ClampColorMode.False );
                GL.ReadPixels( pX, pY, pWidth, pHeight, pFormat, pType, pBuffer.GetData() );
                
                pParameters.Clear();
                pBuffer.Unbind( lBufferFlag );

                GPUBuffer lCast = pBuffer as GPUBuffer;
                if ( lCast != null )
                {
                    lCast.Invalidate();
                }

                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Copies pixels from the attached color buffers into the given Texture1D.
        /// </summary>
        /// <param name="pXOffset">The X offset in the destination texture.</param>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area where pixels must be read.</param>
        /// <param name="pDestination">The destination texture for read pixels.</param>
        /// <param name="pLevel">The destination texture LOD level for read pixels.</param>
        public void CopyPixels(int pXOffset, int pX, int pY, int pWidth, Texture1D pDestination, int pLevel)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if
                ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                pDestination.BindToTextureUnit();
                GL.CopyTexSubImage1D( TextureTarget.Texture1D, pLevel, pXOffset, pX, pY, pWidth );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Copies pixels from the attached color buffers into the given Texture1DArray.
        /// </summary>
        /// <param name="pXOffset">The X offset in the destination texture.</param>
        /// <param name="pLayer">The layer in the destination texture.</param>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area where pixels must be read.</param>
        /// <param name="pLayerCount">The number of layer to read.</param>
        /// <param name="pDestination">The destination texture for read pixels.</param>
        /// <param name="pLevel">The destination texture LOD level for read pixels.</param>
        public void CopyPixels(int pXOffset, int pLayer, int pX, int pY, int pWidth, int pLayerCount, Texture1DArray pDestination, int pLevel)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if
                ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                pDestination.BindToTextureUnit();
                GL.CopyTexSubImage2D( TextureTarget.Texture1D, pLevel, pXOffset, pLayer, pX, pY, pWidth, pLayerCount );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Copies pixels from the attached color buffers into the given Texture2D.
        /// </summary>
        /// <param name="pXOffset">The X offset in the destination texture.</param>
        /// <param name="pYOffset">The Y offset in the destination texture.</param>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area where pixels must be read.</param>
        /// <param name="pHeight">The height of the area where pixels must be read.</param>
        /// <param name="pDestination">The destination texture for read pixels.</param>
        /// <param name="pLevel">The destination texture LOD level for read pixels.</param>
        public void CopyPixels(int pXOffset, int pYOffset, int pX, int pY, int pWidth, int pHeight, Texture2D pDestination, int pLevel)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                pDestination.BindToTextureUnit();
                GL.CopyTexSubImage2D( TextureTarget.Texture2D, pLevel, pXOffset, pYOffset, pX, pY, pWidth, pHeight );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Copies pixels from the attached color buffers into the given Texture2DArray.
        /// </summary>
        /// <param name="pXOffset">The X offset in the destination texture.</param>
        /// <param name="pYOffset">The Y offset in the destination texture.</param>
        /// <param name="pLayer">The layer in the destination texture.</param>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area where pixels must be read.</param>
        /// <param name="pHeight">The height of the area where pixels must be read.</param>
        /// <param name="pDestination">The destination texture for read pixels.</param>
        /// <param name="pLevel">The destination texture LOD level for read pixels.</param>
        public void CopyPixels(int pXOffset, int pYOffset, int pLayer, int pX, int pY, int pWidth, int pHeight, Texture2DArray pDestination, int pLevel)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                pDestination.BindToTextureUnit();
                GL.CopyTexSubImage3D( TextureTarget.Texture2DArray, pLevel, pXOffset, pYOffset, pLayer, pX, pY, pWidth, pHeight );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Copies pixels from the attached color buffers into the given Texture3D.
        /// </summary>
        /// <param name="pXOffset">The X offset in the destination texture.</param>
        /// <param name="pYOffset">The Y offset in the destination texture.</param>
        /// <param name="pZOffset">The Z offset in the destination texture.</param>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area where pixels must be read.</param>
        /// <param name="pHeight">The height of the area where pixels must be read.</param>
        /// <param name="pDestination">The destination texture for read pixels.</param>
        /// <param name="pLevel">The destination texture LOD level for read pixels.</param>
        public void CopyPixels(int pXOffset, int pYOffset, int pZOffset, int pX, int pY, int pWidth, int pHeight, Texture3D pDestination, int pLevel)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                pDestination.BindToTextureUnit();
                GL.CopyTexSubImage3D( TextureTarget.Texture3D, pLevel, pXOffset, pYOffset, pZOffset, pX, pY, pWidth, pHeight );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Copies pixels from the attached color buffers into the given TextureCube.
        /// </summary>
        /// <param name="pXOffset">The X offset in the destination texture.</param>
        /// <param name="pYOffset">The Y offset in the destination texture.</param>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area where pixels must be read.</param>
        /// <param name="pHeight">The height of the area where pixels must be read.</param>
        /// <param name="pDestination">The destination texture for read pixels.</param>
        /// <param name="pLevel">The destination texture LOD level for read pixels.</param>
        /// <param name="pFace">The destination face for read pixels.</param>
        public void CopyPixels(int pXOffset, int pYOffset, int pX, int pY, int pWidth, int pHeight, TextureCube pDestination, int pLevel, CubeFace pFace)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                pDestination.BindToTextureUnit();
                GL.CopyTexSubImage2D( (TextureTarget)pFace, pLevel, pXOffset, pYOffset, pX, pY, pWidth, pHeight );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Copies pixels from the attached color buffers into the given TextureCubeArray.
        /// </summary>
        /// <param name="pXOffset">The X offset in the destination texture.</param>
        /// <param name="pYOffset">The Y offset in the destination texture.</param>
        /// <param name="pLayer">The layer in the desination texture.</param>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area where pixels must be read.</param>
        /// <param name="pHeight">The height of the area where pixels must be read.</param>
        /// <param name="pDestination">The destination texture for read pixels.</param>
        /// <param name="pLevel">The destination texture LOD level for read pixels.</param>
        /// <param name="pFace">The destination face for read pixels.</param>
        public void CopyPixels(int pXOffset, int pYOffset, int pLayer, int pX, int pY, int pWidth, int pHeight, TextureCubeArray pDestination, int pLevel, CubeFace pFace)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                pDestination.BindToTextureUnit();
                GL.CopyTexSubImage3D( (TextureTarget)pFace, pLevel, pXOffset, pYOffset, pLayer, pX, pY, pWidth, pHeight );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Copies pixels from the attached color buffers into the given TextureRectangle.
        /// </summary>
        /// <param name="pXOffset">The X offset in the destination texture.</param>
        /// <param name="pYOffset">The Y offset in the destination texture.</param>
        /// <param name="pX">The X component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pY">The Y component of the lower left corner of the area where the pixels must be read.</param>
        /// <param name="pWidth">The width of the area where pixels must be read.</param>
        /// <param name="pHeight">The height of the area where pixels must be read.</param>
        /// <param name="pDestination">The destination texture for read pixels.</param>
        /// <param name="pLevel">The destination texture LOD level for read pixels.</param>
        public void CopyPixels(int pXOffset, int pYOffset, int pX, int pY, int pWidth, int pHeight, TextureRectangle pDestination, int pLevel)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                pDestination.BindToTextureUnit();
                GL.CopyTexSubImage2D( TextureTarget.TextureRectangle, pLevel, pXOffset, pYOffset, pX, pY, pWidth, pHeight );
                GLHelpers.GetError();
            }
        }

        /// <summary>
        /// Clears the buffers attached to that frame buffer.
        /// </summary>
        /// <param name="pMustClearColorBuffers">The flag indicating whether to clear color buffers or not.</param>
        /// <param name="pMustClearStencilBuffer">The flag indicating whether to clear stencil buffer or not.</param>
        /// <param name="pMustClearDepthBuffer">The flag indicating whether to clear depth buffer or not.</param>
        public void Clear(bool pMustClearColorBuffers, bool pMustClearStencilBuffer, bool pMustClearDepthBuffer)
        {
            LogManager.Instance.Log( "Clearing frame buffer!!!" );

            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.UseFrameBuffer( this );

                int lBuffers = 0;
                if ( pMustClearColorBuffers )
                {
                    lBuffers |= (int)ClearBufferMask.ColorBufferBit;
                }

                if ( pMustClearStencilBuffer )
                {
                    lBuffers |= (int)ClearBufferMask.StencilBufferBit;
                }

                if ( pMustClearDepthBuffer )
                {
                    lBuffers |= (int)ClearBufferMask.DepthBufferBit;
                }

                this.EnableConditionalRender();
                GL.Clear( (ClearBufferMask)lBuffers );
                this.DisableConditionalRender();

                GLHelpers.GetError();
            }
        }

        #region Methods IObserver

        /// <summary>
        /// Delegate called on Frame buffer parameters changes.
        /// </summary>
        /// <param name="pSender">The parameters</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        public void OnObservablePropertyChanged(IObservable pSender, PropertyChangedEventArgs pEventArgs)
        {
            // Inform parameters have changed.
            this.mState |= FrameBufferState.ParametersChanged;
        }

        #endregion Methods IObserver

        #region Methods Internal

        /// <summary>
        /// Reset the frame buffer parameters state.
        /// </summary>
        internal void ParametersUpdated()
        {
            this.mState &= ~FrameBufferState.ParametersChanged;
        }

        /// <summary>
        /// Reset the frame buffer attachments state.
        /// </summary>
        internal void AttachmentsUpdated()
        {
            this.mState &= ~FrameBufferState.AttachmentsChanged;
        }

        /// <summary>
        /// Reset the frame buffer inner buffers state.
        /// </summary>
        internal void BuffersUpdated()
        {
            this.mState &= ~FrameBufferState.ReadDrawBuffersChanged;
        }

        /// <summary>
        /// Send the attachment(s) of that frame buffer.
        /// </summary>
        internal void SetAttachment()
        {
            LogManager.Instance.Log( "Setting the Framebuffer attachments..." );

            FramebufferAttachment[] cATTACHMENTS = 
            {
                FramebufferAttachment.ColorAttachment0,
                FramebufferAttachment.ColorAttachment1,
                FramebufferAttachment.ColorAttachment2,
                FramebufferAttachment.ColorAttachment3,
                FramebufferAttachment.ColorAttachment4,
                FramebufferAttachment.ColorAttachment5,
                FramebufferAttachment.ColorAttachment6,
                FramebufferAttachment.ColorAttachment7,
                FramebufferAttachment.StencilAttachment,
                FramebufferAttachment.DepthAttachment
            };

            for ( int lCurrAttachment = 0; lCurrAttachment < 10; lCurrAttachment++ )
            {
                if ( this.mFrameBufferId == 0 && 
                     lCurrAttachment >= 4 && 
                     lCurrAttachment < 8 )
                {
                    continue;
                }

                object lAttachment = this.mAttachments[ lCurrAttachment ];
                if ( lAttachment == null )
                {
                    GL.FramebufferRenderbuffer( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], RenderbufferTarget.Renderbuffer, 0 );
                    continue;
                }

                if ( lAttachment is RenderBuffer )
                {
                    RenderBuffer lRenderBuffer = lAttachment as RenderBuffer;
                    uint lBufferId = lRenderBuffer.BufferId;
                    GL.FramebufferRenderbuffer( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], RenderbufferTarget.Renderbuffer, lBufferId );
                    continue;
                }

                if ( lAttachment is ITexture )
                {
                    ITexture lTexture = lAttachment as ITexture;
                    uint lId = lTexture.TextureId;
                    if ( lTexture is Texture1DArray ||
                         lTexture is Texture2DArray ||
                         lTexture is Texture2DMultisampleArray ||
                         lTexture is Texture3D )
                    {
                        if ( this.mLayers[ lCurrAttachment ] == -1 )
                        {
                            GL.FramebufferTexture( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], lId, this.mLevels[ lCurrAttachment ] );
                        }
                        else
                        {
                            GL.FramebufferTextureLayer( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], lId, this.mLevels[ lCurrAttachment ], this.mLayers[ lCurrAttachment ] );
                        }
                    }
                    else if ( lTexture is Texture1D )
                    {
                        GL.FramebufferTexture1D( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], TextureTarget.Texture1D, lId, this.mLevels[ lCurrAttachment ] );
                    }
                    else if ( lTexture is Texture2D )
                    {
                        GL.FramebufferTexture2D( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], TextureTarget.Texture2D, lId, this.mLevels[ lCurrAttachment ] );
                    }
                    else if ( lTexture is TextureRectangle )
                    {
                        GL.FramebufferTexture2D( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], TextureTarget.TextureRectangle, lId, this.mLevels[ lCurrAttachment ] );
                    }
                    else if ( lTexture is Texture2DMultisample )
                    {
                        GL.FramebufferTexture2D( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], TextureTarget.Texture2DMultisample, lId, this.mLevels[ lCurrAttachment ] );
                    }
                    else if ( lTexture is TextureCube )
                    {
                        if ( this.mLayers[ lCurrAttachment ] == -1 )
                        {
                            GL.FramebufferTexture( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], lId, this.mLevels[ lCurrAttachment ] );
                        }
                        else
                        {
                            GL.FramebufferTexture2D( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], TextureTarget.TextureCubeMapPositiveX + this.mLayers[ lCurrAttachment ], lId, this.mLevels[ lCurrAttachment ] );
                        }
                    }
                    else if ( lTexture is TextureCubeArray )
                    {
                        GL.FramebufferTextureLayer( FramebufferTarget.Framebuffer, cATTACHMENTS[ lCurrAttachment ], lId, this.mLevels[ lCurrAttachment ], this.mLayers[ lCurrAttachment ] );
                    }

                    GLHelpers.GetError();
                }
            }
        }

        /// <summary>
        /// Checks the attachment(s) of that frame buffer.
        /// </summary>
        internal void CheckAttachment()
        {
            FramebufferErrorCode lStatus = GL.CheckFramebufferStatus( FramebufferTarget.Framebuffer );
            switch ( lStatus )
            {
                case FramebufferErrorCode.FramebufferComplete:
                    return;
                case FramebufferErrorCode.FramebufferUndefined:
                    LogManager.Instance.Log( "Framebuffer object: undefined", LogType.ERROR );
                    break;
                case FramebufferErrorCode.FramebufferIncompleteAttachment:
                    LogManager.Instance.Log( "Framebuffer object: incomplete attachement", LogType.ERROR );
                    break;
                case FramebufferErrorCode.FramebufferIncompleteMissingAttachment:
                    LogManager.Instance.Log( "Framebuffer object: incomplete missing attachement", LogType.ERROR );
                    break;
                case FramebufferErrorCode.FramebufferIncompleteDrawBuffer:
                    LogManager.Instance.Log( "Framebuffer object: incomplete draw buffer", LogType.ERROR );
                    break;
                case FramebufferErrorCode.FramebufferIncompleteReadBuffer:
                    LogManager.Instance.Log( "Framebuffer object: incomplete read buffer", LogType.ERROR );
                    break;
                case FramebufferErrorCode.FramebufferUnsupported:
                    LogManager.Instance.Log( "Framebuffer object: unsupported", LogType.ERROR );
                    break;
                case FramebufferErrorCode.FramebufferIncompleteMultisample:
                    LogManager.Instance.Log( "Framebuffer object: incomplete multisample", LogType.ERROR );
                    break;
                case FramebufferErrorCode.FramebufferIncompleteLayerTargets:
                    LogManager.Instance.Log( "Framebuffer object: incomplete layer targets", LogType.ERROR );
                    break;
                default:
                    LogManager.Instance.Log( "Framebuffer object: Default error...", LogType.ERROR );
                    break;
            }
        }

        /// <summary>
        /// Enables the conditional rendering mode.
        /// </summary>
        internal void EnableConditionalRender()
        {
            OcclusionDescription lOcclusionDesc = this.mParameters.OcclusionDescription;
            GPUQuery lQuery = lOcclusionDesc.OcclusionQuery;
            if ( lQuery != null )
            {
                GL.BeginConditionalRender( lQuery.Id, (ConditionalRenderType)lOcclusionDesc.OcclusionMode.ToGLQuery() );
            }
        }

        /// <summary>
        /// Disables the conditional rendering mode.
        /// </summary>
        internal void DisableConditionalRender()
        {
            OcclusionDescription lOcclusionDesc = this.mParameters.OcclusionDescription;
            GPUQuery lQuery = lOcclusionDesc.OcclusionQuery;
            if ( lQuery != null )
            {
                GL.EndConditionalRender();
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                if ( lRenderService.CurrentFrameBuffer == this )
                {
                    lRenderService.UseFrameBuffer( null );
                }
            }

            if ( this.mParameters != null )
            {
                this.mParameters.Dispose();
                this.mParameters = null;
            }

            for ( int lCurr = 0; lCurr < 10; lCurr ++)
            {
                IDisposable lAttachment = this.mAttachments[ lCurr ] as IDisposable;
                if ( lAttachment != null )
                {
                    lAttachment.Dispose();
                }
            }

            if ( this.mFrameBufferId != 0 )
            {
                GL.DeleteFramebuffers( 1, ref this.mFrameBufferId );
                GLHelpers.GetError();
            }
        }
        
        #endregion Methods IDisposable

        #endregion Methods
    }
}
