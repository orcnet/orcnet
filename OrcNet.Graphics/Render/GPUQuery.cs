﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Asynchronous GPU query that offer its result once available.
    /// </summary>
    [DebuggerDisplay("GPUQuery : Id = {Id}, Reason = {Reason}, IsResultAvailable = {IsResultAvailable}")]
    public class GPUQuery : AMemoryProfilable, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the query's identifier.
        /// </summary>
        private uint mId;

        /// <summary>
        /// Stores the query's reason.
        /// </summary>
        private QueryTarget mReason;

        /// <summary>
        /// Stores the flag indicating whether the result is available or not.
        /// </summary>
        private bool mIsResultAvailable;

        /// <summary>
        /// Stores the flag indicating whether the result has been read.
        /// </summary>
        private bool mIsResultRead;

        /// <summary>
        /// Stores the query's result.
        /// </summary>
        private UInt64 mResult;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(QueryTarget);
                lSize += sizeof(UInt64);
                lSize += sizeof(bool) * 2;
                lSize += sizeof(uint);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the query's identifier.
        /// </summary>
        public uint Id
        {
            get
            {
                return this.mId;
            }
        }

        /// <summary>
        /// Gets the query's reason.
        /// </summary>
        public QueryTarget Reason
        {
            get
            {
                return this.mReason;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the result is available or not.
        /// </summary>
        public bool IsResultAvailable
        {
            get
            {
                if
                    ( this.mIsResultAvailable == false )
                {
                    int lIsAvailable;
                    GL.GetQueryObject( this.mId, GetQueryObjectParam.QueryResultAvailable, out lIsAvailable );
                    this.mIsResultAvailable = lIsAvailable != 0;
                }

                return this.mIsResultAvailable;
            }
        }

        /// <summary>
        /// Gets the query's result.
        /// NOTE: This may block the caller until it be available. Check IsResultAvailable first.
        /// </summary>
        public ulong Result
        {
            get
            {
                if
                    ( this.mIsResultRead == false )
                {
                    GL.GetQueryObject( this.mId, GetQueryObjectParam.QueryResult, out this.mResult );
                    this.mIsResultRead = true;
                }

                return this.mResult;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GPUQuery"/> class.
        /// </summary>
        /// <param name="pReason">The query's reason.</param>
        public GPUQuery(QueryTarget pReason)
        {
            GL.GenQueries( 1, out this.mId );
            this.mReason = pReason;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Starts the query
        /// </summary>
        public void Begin()
        {
            this.mIsResultRead = false;
            GL.BeginQuery( this.mReason, this.mId );

            if
                ( this.mReason == QueryTarget.TimeElapsed )
            {
                GL.QueryCounter( this.mId, QueryCounterTarget.Timestamp );
            }
        }

        /// <summary>
        /// Ends the query.
        /// </summary>
        public void End()
        {
            GL.EndQuery( this.mReason );
        }

        #region Methods IDisposable

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            GL.DeleteQueries( 1, ref this.mId );

            GC.SuppressFinalize( this );
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
