﻿using OpenTK.Graphics.OpenGL;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Enumerates the frame buffer attachment point
    /// </summary>
    public enum BufferId : uint
    {
        /// <summary>
        /// No Attachment.
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Color attachment 0
        /// </summary>
        COLOR0 = 1,

        /// <summary>
        /// Color attachment 1
        /// </summary>
        COLOR1 = 2,

        /// <summary>
        /// Color attachment 2
        /// </summary>
        COLOR2 = 4,

        /// <summary>
        /// Color attachment 3
        /// </summary>
        COLOR3 = 8,

        /// <summary>
        /// Color attachment 4
        /// </summary>
        COLOR4 = 16,

        /// <summary>
        /// Color attachment 5
        /// </summary>
        COLOR5 = 32,

        /// <summary>
        /// Color attachment 6
        /// </summary>
        COLOR6 = 64,

        /// <summary>
        /// Color attachment 7
        /// </summary>
        COLOR7 = 128,

        /// <summary>
        /// Stencil buffer attachment
        /// </summary>
        STENCIL = 256,

        /// <summary>
        /// Depth buffer attachment
        /// </summary>
        DEPTH = 512
    }

    /// <summary>
    /// Buffer Id enumeration extensions.
    /// </summary>
    public static class BufferIdExtensions
    {
        #region Methods

        /// <summary>
        /// Gets the buffer index by buffer Identifier.
        /// </summary>
        /// <param name="pId">The buffer identifier.</param>
        /// <returns>The corresponding buffer index.</returns>
        public static int ToBufferIndex(this BufferId pId)
        {
            switch 
                ( pId )
            {
                case BufferId.COLOR0:
                    return 0;
                case BufferId.COLOR1:
                    return 1;
                case BufferId.COLOR2:
                    return 2;
                case BufferId.COLOR3:
                    return 3;
                case BufferId.COLOR4:
                    return 4;
                case BufferId.COLOR5:
                    return 5;
                case BufferId.COLOR6:
                    return 6;
                case BufferId.COLOR7:
                    return 7;
                case BufferId.STENCIL:
                    return 8;
                case BufferId.DEPTH:
                    return 9;
            }

            return -1;
        }

        /// <summary>
        /// Gets the GL Enum corresponding value from a buffer identifier.
        /// </summary>
        /// <param name="pId">The buffer identifier.</param>
        /// <param name="pFrameBufferId">The frame buffer GL identifier.</param>
        /// <returns>The corresponding GL Enum value, -1 otherwise.</returns>
        public static All ToAttachment(this BufferId pId, uint pFrameBufferId)
        {
            switch
                ( pId & (BufferId.COLOR0 | BufferId.COLOR1 | BufferId.COLOR2 | BufferId.COLOR3 | BufferId.COLOR4 | BufferId.COLOR5 | BufferId.COLOR6 | BufferId.COLOR7) )
            {
                case 0:
                    return All.None;
                case BufferId.COLOR0:
                    return pFrameBufferId == 0 ? All.FrontLeft : All.ColorAttachment0;
                case BufferId.COLOR1:
                    return pFrameBufferId == 0 ? All.FrontRight : All.ColorAttachment1;
                case BufferId.COLOR2:
                    return pFrameBufferId == 0 ? All.BackLeft : All.ColorAttachment2;
                case BufferId.COLOR3:
                    return pFrameBufferId == 0 ? All.BackRight : All.ColorAttachment3;
                case BufferId.COLOR4:
                    return pFrameBufferId == 0 ? All.None : All.ColorAttachment4;
                case BufferId.COLOR5:
                    return pFrameBufferId == 0 ? All.None : All.ColorAttachment5;
                case BufferId.COLOR6:
                    return pFrameBufferId == 0 ? All.None : All.ColorAttachment6;
                case BufferId.COLOR7:
                    return pFrameBufferId == 0 ? All.None : All.ColorAttachment7;
            }

            return All.None;
        }

        #endregion Methods
    }
}
