﻿using System;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Base buffer interface definition.
    /// </summary>
    public interface IBuffer : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the buffer is up to date or not.
        /// </summary>
        bool IsInvalid
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Binds this buffer to the supplied target.
        /// </summary>
        /// <param name="pTarget">The GL buffer target. (e.g: ARRAY_BUFFER, so on)</param>
        void Bind(int pTarget);

        /// <summary>
        /// Gets the pointer to the given offset in this data buffer.
        /// </summary>
        /// <param name="pOffset">The offset from the start of this buffer in byte(s)</param>
        /// <returns>The pointer offseted</returns>
        IntPtr GetData(int pOffset = 0);

        /// <summary>
        /// Unbinds this buffer from the supplied target.
        /// </summary>
        /// <param name="pTarget">The GL buffer target. (e.g: ARRAY_BUFFER, so on)</param>
        void Unbind(int pTarget);

        #endregion Methods
    }
}
