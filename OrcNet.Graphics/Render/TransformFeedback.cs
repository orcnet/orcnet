﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Graphics.Helpers;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Transform feedback class definition being a set of buffer objects to collect results of
    /// a transform feedback.
    /// With GL 3.0, Only a single TransformFeedback instance can be used.
    /// With GL 4.0, Multiple TransformFeedback instances can be used.
    /// Whatever the version, only one query can be done at a time.
    /// </summary>
    [DebuggerDisplay("TransformFeedback : Id = {Id}")]
    public class TransformFeedback : AMemoryProfilable, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether that object has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the transform feedback id.
        /// </summary>
        private uint mTransformFeedbackId;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool);
                lSize += sizeof(uint);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the transform feedback id.
        /// </summary>
        public uint Id
        {
            get
            {
                return this.mTransformFeedbackId;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TransformFeedback"/> class.
        /// </summary>
        public TransformFeedback(bool pIsMain = false)
        {
            this.mIsDisposed = false;
            if
                ( pIsMain )
            {
                this.mTransformFeedbackId = 0;
            }
            else
            {
                GL.GenTransformFeedbacks( 1, out this.mTransformFeedbackId );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Attachs the given buffer to collect the transformed output varying
        /// of the given index.
        /// </summary>
        /// <param name="pIndex">The index of the recorded output varying variable.</param>
        /// <param name="pRecord">The buffer used to store recorded values of the varying.</param>
        public void RecordVertexVarying(uint pIndex, GPUBuffer pRecord)
        {
            this.Bind();

            GL.BindBufferBase( BufferRangeTarget.TransformFeedbackBuffer, pIndex, pRecord.BufferId );

            pRecord.Invalidate();
        }

        /// <summary>
        /// Attachs the given buffer to collect the transformed output varying
        /// of the given index.
        /// </summary>
        /// <param name="pIndex">The index of the recorded output varying variable.</param>
        /// <param name="pRecord">The buffer used to store recorded values of the varying.</param>
        /// <param name="pOffset">The offset at which the first recorded value must be stored.</param>
        /// <param name="pMaxSize">The maximum size of the recorded values.</param>
        public void RecordVertexVarying(uint pIndex, GPUBuffer pRecord, uint pOffset, int pMaxSize)
        {
            this.Bind();

            GL.BindBufferRange( BufferRangeTarget.TransformFeedbackBuffer, pIndex, pRecord.BufferId, new IntPtr( pOffset ), new IntPtr( pMaxSize ) );

            pRecord.Invalidate();
        }

        /// <summary>
        /// Binds the transform feedback.
        /// </summary>
        public void Bind()
        {
            int lVersion;
            GL.GetInteger( GetPName.MajorVersion, out lVersion );
            if
                ( lVersion >= 4 )
            {
                GL.BindTransformFeedback( TransformFeedbackTarget.TransformFeedback, this.mTransformFeedbackId );
            }
        }

        /// <summary>
        /// Removes all the transform feedback's buffers.
        /// </summary>
        public void Reset()
        {
            int lCount;
            GL.GetInteger( GetPName.MaxTransformFeedbackSeparateAttribs, out lCount );

            this.Bind();

            for
                ( uint lCurr = 0; lCurr < lCount; lCurr++ )
            {
                GL.BindBufferBase( BufferRangeTarget.TransformFeedbackBuffer, lCurr, 0 );
            }

            GLHelpers.GetError();
        }
        
        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if
                ( this.mIsDisposed == false )
            {
                this.Reset();
                if
                    ( this.mTransformFeedbackId != 0 )
                {
                    GL.DeleteTransformFeedbacks( 1, ref this.mTransformFeedbackId );
                }

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
