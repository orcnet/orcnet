﻿using OpenTK.Graphics.OpenGL;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Enumerate the different pipeline stage types.
    /// </summary>
    public enum StageType : int
    {
        /// <summary>
        /// Vertex stage of the GPU pipeline.
        /// </summary>
        VERTEX = 0,

        /// <summary>
        /// Tesselation control stage of the GPU pipeline.
        /// </summary>
        TESSELATION_CONTROL,

        /// <summary>
        /// Tesselation evaluation stage of the GPU pipeline.
        /// </summary>
        TESSELATION_EVALUATION,

        /// <summary>
        /// Geometry stage of the GPU pipeline.
        /// </summary>
        GEOMETRY,

        /// <summary>
        /// Fragment stage of the GPU pipeline.
        /// </summary>
        FRAGMENT,

        /// <summary>
        /// The amount of pipeline stages available.
        /// </summary>
        COUNT
    }

    /// <summary>
    /// Stage type extension(s).
    /// </summary>
    public static class StageTypeExtensions
    {
        /// <summary>
        /// Transform the OrcNet Stage type enum into a OpenTK Shader type enum.
        /// </summary>
        /// <param name="pStage"></param>
        /// <returns>The OpenGL shader type.</returns>
        public static ShaderType ToShaderType(this StageType pStage)
        {
            switch
                ( pStage )
            {
                case StageType.TESSELATION_CONTROL:
                    return ShaderType.VertexShader;
                case StageType.TESSELATION_EVALUATION:
                    return ShaderType.VertexShader;
                case StageType.GEOMETRY:
                    return ShaderType.VertexShader;
                case StageType.FRAGMENT:
                    return ShaderType.FragmentShader;
            }

            return ShaderType.VertexShader;
        }
    }
}
