﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Occlusion description class definition.
    /// </summary>
    public class OcclusionDescription : AObservable
    {
        #region Fields

        /// <summary>
        /// Stores the occlusion query in charge of tracking the number of fragments or
        /// samples that pass the depth test.
        /// </summary>
        private GPUQuery  mOcclusionQuery;

        /// <summary>
        /// Stores the occlusion comparison mode used for depth tests.
        /// </summary>
        private QueryMode mOcclusionMode;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the occlusion query in charge of tracking the number of fragments or
        /// samples that pass the depth test.
        /// </summary>
        public GPUQuery OcclusionQuery
        {
            get
            {
                return this.mOcclusionQuery;
            }
        }

        /// <summary>
        /// Gets or sets the occlusion comparison mode used for depth tests.
        /// </summary>
        public QueryMode OcclusionMode
        {
            get
            {
                return this.mOcclusionMode;
            }
        }

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="OcclusionDescription"/> class.
        /// </summary>
        /// <param name="pQuery">the occlusion query in charge of tracking the number of fragments or samples that pass the depth test.</param>
        /// <param name="pMode">the occlusion comparison mode used for depth tests.</param>
        public OcclusionDescription(GPUQuery pQuery = null, QueryMode pMode = QueryMode.WAIT)
        {
            this.mOcclusionQuery = pQuery;
            this.mOcclusionMode  = pMode;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Asks for an occlusion query.
        /// </summary>
        /// <param name="pQuery">the occlusion query in charge of tracking the number of fragments or samples that pass the depth test.</param>
        /// <param name="pMode">the occlusion comparison mode used for depth tests.</param>
        public void OcclusionTest(GPUQuery pQuery, QueryMode pMode)
        {
            if
                ( pQuery == null ||
                  pQuery.Reason == QueryTarget.SamplesPassed ||
                  pQuery.Reason == QueryTarget.AnySamplesPassed )
            {
                this.mOcclusionQuery = pQuery;
                this.mOcclusionMode  = pMode;

                this.InformPropertyChangedListener( this.mOcclusionQuery, this.mOcclusionQuery, "OcclusionQuery" );
            }
        }

        #region Methods AObservable

        /// <summary>
        /// Delegate called on dispose.
        /// </summary>
        protected override void OnDispose()
        {
            if
                ( this.mOcclusionQuery != null )
            {
                this.mOcclusionQuery.Dispose();
                this.mOcclusionQuery = null;
            }

            base.OnDispose();
        }

        #endregion Methods AObservable

        #endregion Methods
    }
}
