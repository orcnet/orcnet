﻿using OpenTK.Graphics.OpenGL;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Enumerates the query mode.
    /// </summary>
    public enum QueryMode
    {
        /// <summary>
        /// Wait query
        /// </summary>
        WAIT = 0,

        /// <summary>
        /// Do not wait query
        /// </summary>
        NO_WAIT,

        /// <summary>
        /// Region wait query
        /// </summary>
        REGION_WAIT,

        /// <summary>
        /// Region no wait query
        /// </summary>
        REGION_NO_WAIT,

        /// <summary>
        /// The amount of query modes.
        /// </summary>
        COUNT
    }

    /// <summary>
    /// Query mode extensions.
    /// </summary>
    public static class QueryModeExtensions
    {
        #region Methods

        /// <summary>
        /// Turns Query mode enumeration into the GL corresponding query mode.
        /// </summary>
        /// <param name="pMode"></param>
        /// <returns></returns>
        public static All ToGLQuery(this QueryMode pMode)
        {
            switch
                ( pMode )
            {
                case QueryMode.NO_WAIT:
                    return All.QueryNoWait;
                case QueryMode.REGION_WAIT:
                    return All.QueryByRegionWait;
                case QueryMode.REGION_NO_WAIT:
                    return All.QueryByRegionNoWait;
            }

            return All.QueryWait;
        }

        #endregion Methods
    }
}
