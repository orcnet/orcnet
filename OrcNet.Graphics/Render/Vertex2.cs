﻿namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Definition of the <see cref="Vertex2"/> structure.
    /// Container whose size is determinable by the Marshal.SizeOf() with no padding
    /// important to send to GPU.
    /// </summary>
    public struct Vertex2
    {
        #region Properties

        /// <summary>
        /// Gets or sets the X position component
        /// </summary>
        public float X
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Y position component
        /// </summary>
        public float Y
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vertex2"/> structure.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        public Vertex2(float pX, float pY)
        {
            this.X = pX;
            this.Y = pY;
        }

        #endregion Constructor
    }
}
