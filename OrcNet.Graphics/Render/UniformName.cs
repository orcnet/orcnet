﻿using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Uniform name structure definition.
    /// </summary>
    [DebuggerDisplay("Name = {mName}")]
    public struct UniformName
    {
        #region Fields

        /// <summary>
        /// Stores the uniform name.
        /// </summary>
        private string mName;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformName"/> class.
        /// </summary>
        /// <param name="pName">The uniform name</param>
        public UniformName(string pName)
        {
            this.mName = pName;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from UniformName to string.
        /// </summary>
        /// <param name="pName">The uniform name</param>
        public static implicit operator string(UniformName pName)
        {
            return pName.mName;
        }

        #endregion Methods
    }
}
