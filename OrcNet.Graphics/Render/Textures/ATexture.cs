﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Core.Resource;
using OrcNet.Core.Service;
using OrcNet.Graphics.Helpers;
using OrcNet.Graphics.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Base abstract texture class definition.
    /// </summary>
    [DebuggerDisplay("Id = {TextureId}, Type = {TextureType}, Format = {Format}, IsCompressed = {IsCompressed}")]
    public abstract class ATexture : AMemoryProfilable, IDisposable, ITexture
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the texture creator resource.
        /// </summary>
        private IResource mCreator;

        /// <summary>
        /// Stores the program identifiers that use that texture.
        /// </summary>
        private List<uint> mUsers;

        /// <summary>
        /// Stores the texture parameters.
        /// </summary>
        protected TextureParameters mParameters;

        /// <summary>
        /// Stores the texture internal format.
        /// </summary>
        protected PixelInternalFormat mInternalFormat;

        /// <summary>
        /// Stores the texture type (texture 1D, 2D, so on)
        /// </summary>
        private TextureTarget mTextureType;

        /// <summary>
        /// Stores the texture identifier.
        /// </summary>
        protected uint mTextureId;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(uint) * (uint)this.mUsers.Count;
                lSize += sizeof(PixelInternalFormat);
                lSize += sizeof(TextureTarget);
                lSize += sizeof(uint);

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        public IResource Creator
        {
            get
            {
                return this.mCreator;
            }
            set
            {
                this.mCreator = value;
            }
        }

        #endregion Properties IResourceCreatable

        /// <summary>
        /// Gets the texture component count.
        /// </summary>
        public uint ComponentCount
        {
            get
            {
                switch
                    ( this.Format )
                {
                    case PixelFormat.StencilIndex:
                    case PixelFormat.DepthComponent:
                    case PixelFormat.Red:
                    case PixelFormat.Green:
                    case PixelFormat.Blue:
                    case PixelFormat.RedInteger:
                    case PixelFormat.BlueInteger:
                    case PixelFormat.GreenInteger:
                        return 1;
                    case PixelFormat.DepthStencil:
                    case PixelFormat.Rg:
                    case PixelFormat.RgInteger:
                        return 2;
                    case PixelFormat.Rgb:
                    case PixelFormat.Bgr:
                    case PixelFormat.RgbInteger:
                    case PixelFormat.BgrInteger:
                        return 3;
                    case PixelFormat.Rgba:
                    case PixelFormat.Bgra:
                    case PixelFormat.RgbaInteger:
                    case PixelFormat.BgraInteger:
                        return 4;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets the texture type being the GL target.
        /// </summary>
        public TextureTarget TextureType
        {
            get
            {
                return this.mTextureType;
            }
        }

        /// <summary>
        /// Gets the texture format
        /// </summary>
        public PixelFormat Format
        {
            get
            {
                switch
                    ( this.mInternalFormat )
                {
                    case PixelInternalFormat.R8:
                    case PixelInternalFormat.R8Snorm:
                    case PixelInternalFormat.R16:
                    case PixelInternalFormat.R16Snorm:
                    case PixelInternalFormat.CompressedRed:
                    case PixelInternalFormat.R16f:
                    case PixelInternalFormat.R32f:
                    case PixelInternalFormat.CompressedRedRgtc1:
                    case PixelInternalFormat.CompressedSignedRedRgtc1:
                        return PixelFormat.Red;
                    case PixelInternalFormat.R8i:
                    case PixelInternalFormat.R8ui:
                    case PixelInternalFormat.R16i:
                    case PixelInternalFormat.R16ui:
                    case PixelInternalFormat.R32i:
                    case PixelInternalFormat.R32ui:
                        return PixelFormat.RedInteger;
                    case PixelInternalFormat.Rg8:
                    case PixelInternalFormat.Rg8Snorm:
                    case PixelInternalFormat.Rg16:
                    case PixelInternalFormat.Rg16Snorm:
                    case PixelInternalFormat.Rg16f:
                    case PixelInternalFormat.Rg32f:
                    case PixelInternalFormat.CompressedRg:
                    case PixelInternalFormat.CompressedRgRgtc2:
                    case PixelInternalFormat.CompressedSignedRgRgtc2:
                        return PixelFormat.Rg;
                    case PixelInternalFormat.Rg8i:
                    case PixelInternalFormat.Rg8ui:
                    case PixelInternalFormat.Rg16i:
                    case PixelInternalFormat.Rg16ui:
                    case PixelInternalFormat.Rg32i:
                    case PixelInternalFormat.Rg32ui:
                        return PixelFormat.RgInteger;
                    case PixelInternalFormat.R3G3B2:
                    case PixelInternalFormat.Rgb4:
                    case PixelInternalFormat.Rgb5:
                    case PixelInternalFormat.Rgb8:
                    case PixelInternalFormat.Rgb8Snorm:
                    case PixelInternalFormat.Rgb10:
                    case PixelInternalFormat.Rgb12:
                    case PixelInternalFormat.Rgb16:
                    case PixelInternalFormat.Rgb16Snorm:
                    case PixelInternalFormat.Srgb8:
                    case PixelInternalFormat.Rgb16f:
                    case PixelInternalFormat.Rgb32f:
                    case PixelInternalFormat.R11fG11fB10f:
                    case PixelInternalFormat.Rgb9E5:
                    case PixelInternalFormat.CompressedRgb:
                    case PixelInternalFormat.CompressedRgbBptcSignedFloat:
                    case PixelInternalFormat.CompressedRgbBptcUnsignedFloat:
                    case PixelInternalFormat.CompressedRgbS3tcDxt1Ext:
                        return PixelFormat.Rgb;
                    case PixelInternalFormat.Rgb8i:
                    case PixelInternalFormat.Rgb8ui:
                    case PixelInternalFormat.Rgb16i:
                    case PixelInternalFormat.Rgb16ui:
                    case PixelInternalFormat.Rgb32i:
                    case PixelInternalFormat.Rgb32ui:
                        return PixelFormat.RgbInteger;
                    case PixelInternalFormat.Rgb5A1:
                    case PixelInternalFormat.Rgba2:
                    case PixelInternalFormat.Rgba4:
                    case PixelInternalFormat.Rgba8:
                    case PixelInternalFormat.Rgba8Snorm:
                    case PixelInternalFormat.Rgb10A2:
                    case PixelInternalFormat.Rgb10A2ui:
                    case PixelInternalFormat.Rgba12:
                    case PixelInternalFormat.Rgba16:
                    case PixelInternalFormat.Rgba16Snorm:
                    case PixelInternalFormat.Srgb8Alpha8:
                    case PixelInternalFormat.Rgba16f:
                    case PixelInternalFormat.Rgba32f:
                        return PixelFormat.Rgba;
                    case PixelInternalFormat.DepthComponent16:
                    case PixelInternalFormat.DepthComponent24:
                    case PixelInternalFormat.DepthComponent32f:
                        return PixelFormat.DepthComponent;
                    case PixelInternalFormat.Depth24Stencil8:
                    case PixelInternalFormat.Depth32fStencil8:
                        return PixelFormat.DepthStencil;
                    case PixelInternalFormat.CompressedRgba:
                    case PixelInternalFormat.CompressedSrgb:
                    case PixelInternalFormat.CompressedRgbaBptcUnorm:
                    //case PixelInternalFormat.CompressedSrgbAlphaBptcUnorm:
                    case PixelInternalFormat.CompressedRgbaS3tcDxt1Ext:
                    case PixelInternalFormat.CompressedRgbaS3tcDxt3Ext:
                    case PixelInternalFormat.CompressedRgbaS3tcDxt5Ext:
                        return PixelFormat.Rgba;
                    case PixelInternalFormat.Rgba8i:
                    case PixelInternalFormat.Rgba8ui:
                    case PixelInternalFormat.Rgba16i:
                    case PixelInternalFormat.Rgba16ui:
                    case PixelInternalFormat.Rgba32i:
                    case PixelInternalFormat.Rgba32ui:
                        return PixelFormat.RgbaInteger;
                }

                // Not supported.
                return PixelFormat.ColorIndex;
            }
        }

        /// <summary>
        /// Gets the texture internal format.
        /// </summary>
        public PixelInternalFormat InternalFormat
        {
            get
            {
                return this.mInternalFormat;
            }
        }

        /// <summary>
        /// Gets the texture internal format name.
        /// </summary>
        public string InternalFormatName
        {
            get
            {
                return this.InternalFormat.ToString();
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the texture has mipmaps or not.
        /// </summary>
        public bool HasMipmaps
        {
            get
            {
                return this.mParameters.MinFilter != TextureMinFilter.Nearest && 
                       this.mParameters.MinFilter != TextureMinFilter.Linear;
            }
        }
        
        /// <summary>
        /// Gets the flag indicating whether the texture is compressed or not on the GPU.
        /// </summary>
        public bool IsCompressed
        {
            get
            {
                switch
                    ( this.mInternalFormat )
                {
                    case PixelInternalFormat.CompressedRed:
                    case PixelInternalFormat.CompressedRg:
                    case PixelInternalFormat.CompressedRgb:
                    case PixelInternalFormat.CompressedRgba:
                    case PixelInternalFormat.CompressedSrgb:
                    case PixelInternalFormat.CompressedRedRgtc1:
                    case PixelInternalFormat.CompressedSignedRedRgtc1:
                    case PixelInternalFormat.CompressedRgRgtc2:
                    case PixelInternalFormat.CompressedSignedRgRgtc2:
                    case PixelInternalFormat.CompressedRgbaBptcUnorm:
                    //case PixelInternalFormat.CompressedSrgbAlphaBptcUnorm:
                    case PixelInternalFormat.CompressedRgbBptcSignedFloat:
                    case PixelInternalFormat.CompressedRgbBptcUnsignedFloat:
                    case PixelInternalFormat.CompressedRgbS3tcDxt1Ext:
                    case PixelInternalFormat.CompressedRgbaS3tcDxt1Ext:
                    case PixelInternalFormat.CompressedRgbaS3tcDxt3Ext:
                    case PixelInternalFormat.CompressedRgbaS3tcDxt5Ext:
                        return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the texture identifier.
        /// </summary>
        public uint TextureId
        {
            get
            {
                return this.mTextureId;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ATexture"/> class.
        /// </summary>
        /// <param name="pType">The texture type (Texture 1D, 2D ans so on)</param>
        protected ATexture(TextureTarget pType)
        {
            this.mTextureType = pType;
            this.mUsers = new List<uint>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the texture is used by the given programs or not.
        /// </summary>
        /// <param name="pProgramIds">The programs identifiers.</param>
        /// <returns>True if used at least by one program, false otherwise.</returns>
        public bool IsUsedBy(IEnumerable<uint> pProgramIds)
        {
            return this.mUsers.Intersect( pProgramIds ).Any();
        }

        /// <summary>
        /// Adds a new program as user for that texture.
        /// </summary>
        /// <param name="pProgramId">The program identifier.</param>
        public void AddUser(uint pProgramId)
        {
            if ( this.mUsers.Contains( pProgramId ) == false )
            {
                this.mUsers.Add( pProgramId );
            }
        }

        /// <summary>
        /// Removes the supplied program from users of that texture.
        /// </summary>
        /// <param name="pProgramId">The program identifier.</param>
        /// <returns>True if was an user, false otherwise.</returns>
        public bool RemoveUser(uint pProgramId)
        {
            if ( this.mUsers.Contains( pProgramId ) )
            {
                return this.mUsers.Remove( pProgramId );
            }

            return false;
        }

        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        public virtual void Swap(ITexture pTexture)
        {
            ATexture lOther = pTexture as ATexture;

            TextureUnitManager.Instance.Unbind( this );
            TextureUnitManager.Instance.Unbind( lOther );

            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                if ( lRenderService.CurrentPass != null &&
                     this.IsUsedBy( lRenderService.CurrentPass.PassIds ) )
                {
                    lRenderService.UsePass( null );
                }
            }

            if ( this.mTextureType == lOther.mTextureType )
            {
                Utilities.Swap( ref this.mTextureId, ref lOther.mTextureId );
                Utilities.Swap( ref this.mInternalFormat, ref lOther.mInternalFormat );
                Utilities.Swap( ref this.mParameters, ref lOther.mParameters );
            }
            else
            {
                LogManager.Instance.Log( string.Format( "Unable to swap texture {0} of type {1} with texture {2} of type {3}!!!", this.mTextureId, this.mTextureType, lOther.mTextureId, lOther.mTextureType ), LogType.ERROR );
            }
        }

        /// <summary>
        /// Gets the mipmap compressed size at the given level.
        /// </summary>
        /// <param name="pLevel">The mipmap level.</param>
        /// <returns>The mipmap compressed size.</returns>
        public int GetMipmapCompressedSize(int pLevel)
        {
            int lSize;
            GL.GetTexLevelParameter( this.mTextureType, pLevel, GetTextureParameter.TextureCompressedImageSize, out lSize );
            GLHelpers.GetError();
            return lSize;
        }

        /// <summary>
        /// Generates the mipmaps.
        /// </summary>
        public void GenerateMipmaps()
        {
            if ( this.HasMipmaps )
            {
                this.BindToTextureUnit();
                GL.GenerateMipmap( (GenerateMipmapTarget)this.mTextureType );

                GLHelpers.GetError();
            }
            else
            {
                LogManager.Instance.Log( string.Format( "Attempting to generate mipmaps but it is not allowed!!!" ) );
            }
        }

        /// <summary>
        /// Gets the mipmap data at the given level in the supplied target format and type.
        /// </summary>
        /// <param name="pLevel">The mipmap level</param>
        /// <param name="pTargetFormat">The mipmap target format</param>
        /// <param name="pTargetType">The mipmap target type.</param>
        /// <param name="pData">The mipmap data.</param>
        public void GetMipmap(int pLevel, PixelFormat pTargetFormat, PixelType pTargetType, IntPtr pData)
        {
            this.BindToTextureUnit();
            
            GL.GetTexImage( this.mTextureType, pLevel, pTargetFormat, pTargetType, pData );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Gets the compressed mipmap data at the given level. Only if compressed.
        /// see IsCompressed
        /// </summary>
        /// <param name="pLevel">The mipmap level</param>
        /// <param name="pData">The mipmap compressed data.</param>
        public void GetCompressedMipmap(int pLevel, IntPtr pData)
        {
            this.BindToTextureUnit();
            
            GL.GetCompressedTexImage( this.mTextureType, pLevel, pData );

            GLHelpers.GetError();
        }

        #region Methods Internal

        /// <summary>
        /// Initializes the texture
        /// </summary>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pParameters">The texture parameters.</param>
        protected void Initialize(PixelInternalFormat pGPUFormat, TextureParameters pParameters)
        {
            GL.GenTextures( 1, out this.mTextureId );
            if ( this.mTextureId <= 0 )
            {
                LogManager.Instance.Log( string.Format( "Cannot create a new texture of GPU format {0}!!!", pGPUFormat ), LogType.ERROR );
                return;
            }

            this.mInternalFormat = pGPUFormat;
            this.mParameters = pParameters;

            this.BindToTextureUnit();

            if ( this.mTextureType == TextureTarget.TextureBuffer )
            {
                GLHelpers.GetError();
                return;
            }

            GL.TexParameter( this.mTextureType, TextureParameterName.TextureWrapS, (int)this.mParameters.WrapS );
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureWrapT, (int)this.mParameters.WrapT );
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureWrapR, (int)this.mParameters.WrapR );
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureMinFilter, (int)this.mParameters.MinFilter );
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureMagFilter, (int)this.mParameters.MagFilter );
            if ( pParameters.PassBorderAsFloat )
            {
                Vector4F lColor = pParameters.BorderColorAsFloat;
                GL.TexParameter( this.mTextureType, TextureParameterName.TextureBorderColor, new float[] { lColor.X, lColor.Y, lColor.Z, lColor.W } );
            }
            else
            {
                Color lBorder = pParameters.BorderColor;
                GL.TexParameterI( this.mTextureType, TextureParameterName.TextureBorderColor, new int[] { lBorder.R, lBorder.G, lBorder.B, lBorder.A } );
            }
                
            if ( this.mTextureType != TextureTarget.TextureRectangle )
            {
                GL.TexParameter( this.mTextureType, TextureParameterName.TextureMinLod, this.mParameters.LODMin );
                GL.TexParameter( this.mTextureType, TextureParameterName.TextureMaxLod, this.mParameters.LODMax );
            }
            
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureLodBias, this.mParameters.LODBias );
            if ( this.mParameters.CompareFunction != TextureComparison.Always )
            {
                GL.TexParameter( this.mTextureType, TextureParameterName.TextureCompareMode, (int)All.CompareRefToTexture );
                GL.TexParameter( this.mTextureType, TextureParameterName.TextureCompareFunc, (int)this.mParameters.CompareFunction );
            }
            
            GL.TexParameter( this.mTextureType, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, this.mParameters.MaxAnisotropy );
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureSwizzleR, this.GetTextureSwizzle( this.mParameters.Swizzle[ 0 ] ) );
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureSwizzleG, this.GetTextureSwizzle( this.mParameters.Swizzle[ 1 ] ) );
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureSwizzleB, this.GetTextureSwizzle( this.mParameters.Swizzle[ 2 ] ) );
            GL.TexParameter( this.mTextureType, TextureParameterName.TextureSwizzleA, this.GetTextureSwizzle( this.mParameters.Swizzle[ 3 ] ) );
            
            if ( this.mTextureType != TextureTarget.TextureRectangle )
            {
                GL.TexParameter( this.mTextureType, TextureParameterName.TextureBaseLevel, this.mParameters.MinLODIndex );
                GL.TexParameter( this.mTextureType, TextureParameterName.TextureMaxLevel, this.mParameters.MaxLODIndex );
            }

            GLHelpers.GetError();
        }

        /// <summary>
        /// Binds that texture to the first free unit. 
        /// NOTE: if already binded with an unit, it will activate it.
        /// </summary>
        /// <returns>The texture unit id bound to that texture.</returns>
        internal int BindToTextureUnit()
        {
            int lUnitId = TextureUnitManager.Instance.FirstUsedUnit( this );
            if ( lUnitId == -1 )
            {
                IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                if ( lRenderService != null )
                {
                    if ( lRenderService.CurrentPass == null )
                    {
                        lUnitId = TextureUnitManager.Instance.FindAvailableTextureUnit( new List<uint>() );
                    }
                    else
                    {
                        lUnitId = TextureUnitManager.Instance.FindAvailableTextureUnit( lRenderService.CurrentPass.PassIds );
                    }

                    if ( lUnitId != -1 )
                    {
                        TextureUnitManager.Instance.Bind( (uint)lUnitId, this, null );
                    }
                }
            }
            else
            {
                GL.ActiveTexture( OpenTK.Graphics.OpenGL.TextureUnit.Texture0 + lUnitId );
            }

            return lUnitId;
        }

        /// <summary>
        /// Binds that texture and the given sampler to a texture unit for the provided
        /// pass(es). If any available unit, it will be bound to it, otherwise the least
        /// recently used unit that is not used by supplied pass(es) will be requisitioned.
        /// </summary>
        /// <param name="pSampler">The texture's sampler</param>
        /// <param name="pPassIds">The pass(es) that texture must be bound for.</param>
        /// <returns>The texture unit id, -1 if no binding.</returns>
        private int BindToTextureUnit(ISampler pSampler, IEnumerable<uint> pPassIds)
        {
            int lUnitId = TextureUnitManager.Instance.UsedUnit( this, pSampler );
            if ( lUnitId == -1 )
            {
                lUnitId = TextureUnitManager.Instance.FindAvailableTextureUnit( pPassIds );
            }

            TextureUnitManager.Instance.Bind( (uint)lUnitId, this, pSampler );

            return lUnitId;
        }

        /// <summary>
        /// Gets the proper GL swizzle enum matching the swizzle char code.
        /// </summary>
        /// <param name="pSwizzle">The swizzle char code.</param>
        /// <returns>The texture swizzle value corresponding to the swizzle char code.</returns>
        private int GetTextureSwizzle(char pSwizzle)
        {
            switch ( pSwizzle )
            {
                case 'r':
                    {
                        return (int)All.Red;
                    }
                case 'g':
                    {
                        return (int)All.Green;
                    }
                case 'b':
                    {
                        return (int)All.Blue;
                    }
                case 'a':
                    {
                        return (int)All.Alpha;
                    }
                case '0':
                    {
                        return (int)All.Zero;
                    }
                case '1':
                    {
                        return (int)All.One;
                    }
            }

            return -1;
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            // Unbind texture first.
            TextureUnitManager.Instance.Unbind( this );

            GL.DeleteTextures( 1, ref this.mTextureId );

            GLHelpers.GetError();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
