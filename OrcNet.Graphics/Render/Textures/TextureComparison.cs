﻿namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Used in GL.TexParameter, GL.TexParameterI
    /// </summary>
    public enum TextureComparison
    {
        /// <summary>
        /// Original was GL_NEVER = 0x0200
        /// </summary>
        Never = 512,

        /// <summary>
        /// Original was GL_LESS = 0x0201
        /// </summary>
        Less = 513,

        /// <summary>
        /// Original was GL_EQUAL = 0x0202
        /// </summary>
        Equal = 514,

        /// <summary>
        /// Original was GL_LEQUAL = 0x0203
        /// </summary>
        Lequal = 515,

        /// <summary>
        /// Original was GL_GREATER = 0x0204
        /// </summary>
        Greater = 516,

        /// <summary>
        /// Original was GL_NOTEQUAL = 0x0205
        /// </summary>
        Notequal = 517,

        /// <summary>
        /// Original was GL_GEQUAL = 0x0206
        /// </summary>
        Gequal = 518,

        /// <summary>
        /// Original was GL_ALWAYS = 0x0207
        /// </summary>
        Always = 519
    }
}
