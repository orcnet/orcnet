﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Graphics.Helpers;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture unit class definition.
    /// </summary>
    public sealed class TextureUnit
    {
        #region Fields
        
        /// <summary>
        /// Stores the currently bound texture to that unit.
        /// </summary>
        private ITexture mCurrentBindedTexture;

        /// <summary>
        /// Stores the currently bound texture sampler to that unit.
        /// </summary>
        private ISampler mCurrentBindedSampler;

        /// <summary>
        /// Stores the unit texture identifier.
        /// </summary>
        private uint mUnitId;

        /// <summary>
        /// Stores the last time at which currenty bound texture components have been
        /// bound to that unit. This allow to manage unit by freeing the oldest one
        /// when one is needed.
        /// </summary>
        private uint mLastBindingTime;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the unit is available for binding
        /// texture and/or sampler or not.
        /// </summary>
        public bool IsAvailable
        {
            get
            {
                return this.mCurrentBindedTexture == null;
            }
        }

        /// <summary>
        /// Gets the currently bound texture to that unit.
        /// </summary>
        public ITexture CurrentBindedTexture
        {
            get
            {
                return this.mCurrentBindedTexture;
            }
        }

        /// <summary>
        /// Gets the currently bound texture sampler to that unit.
        /// </summary>
        public ISampler CurrentBindedSampler
        {
            get
            {
                return this.mCurrentBindedSampler;
            }
        }

        /// <summary>
        /// Gets the last time at which currenty bound texture components have been
        /// bound to that unit. This allow to manage unit by freeing the oldest one
        /// when one is needed.
        /// </summary>
        public uint LastBindingTime
        {
            get
            {
                return this.mLastBindingTime;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureUnit"/> class.
        /// </summary>
        /// <param name="pUnitId">The unit identifier.</param>
        public TextureUnit(uint pUnitId)
        {
            this.mUnitId = pUnitId;
            this.mLastBindingTime = 0;
            this.mCurrentBindedTexture = null;
            this.mCurrentBindedSampler = null;
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Binds a texture and a sampler to that unit.
        /// </summary>
        /// <param name="pTexture">The texture to bind</param>
        /// <param name="pSampler">The sampler to use with that texture.</param>
        /// <param name="pTime">The time of the binding.</param>
        public void Bind(ISampler pSampler, ITexture pTexture, uint pTime)
        {
            this.mLastBindingTime = pTime;

            uint lCurrentSamplerId = 0;
            if ( this.mCurrentBindedSampler != null )
            {
                lCurrentSamplerId = this.mCurrentBindedSampler.SamplerId;
            }

            uint lNewSamplerId = 0;
            if ( pSampler != null )
            {
                lNewSamplerId = pSampler.SamplerId;
            }

            GL.ActiveTexture( (OpenTK.Graphics.OpenGL.TextureUnit)((uint)OpenTK.Graphics.OpenGL.TextureUnit.Texture0 + this.mUnitId) );

            if ( pSampler != this.mCurrentBindedSampler )
            {
                GL.BindSampler( this.mUnitId, lNewSamplerId );
                this.mCurrentBindedSampler = pSampler;

                LogManager.Instance.Log( string.Format( "Binded sampler \"{0}\" to the unit \"{1}\"", pSampler.SamplerId, this.mUnitId ) );
            }

            if ( pTexture != this.mCurrentBindedTexture )
            {
                // Unbind the previous one
                if ( this.mCurrentBindedTexture != null )
                {
                    if ( TextureUnitManager.Instance.FreeTextureFromUnit( this.mCurrentBindedTexture.TextureId, lCurrentSamplerId ) )
                    {
                        if ( pTexture == null ||
                              this.mCurrentBindedTexture.TextureType != pTexture.TextureType )
                        {
                            GL.BindTexture( this.mCurrentBindedTexture.TextureType, 0 );
                        }
                    }
                }

                // Bind the new one.
                if ( pTexture != null )
                {
                    TextureUnitManager.Instance.MapTextureWithUnit( pTexture.TextureId, lNewSamplerId, this.mUnitId );
                    GL.BindTexture( pTexture.TextureType, pTexture.TextureId );

                    LogManager.Instance.Log( string.Format( "Binded texture \"{0}\" to the unit \"{1}\"", pTexture.TextureId, this.mUnitId ) );
                }

                this.mCurrentBindedTexture = pTexture;
            }

            GLHelpers.GetError();
        }

        #endregion Methods
    }
}
