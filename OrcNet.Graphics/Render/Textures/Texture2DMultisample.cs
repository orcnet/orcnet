﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Graphics.Helpers;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture 2D Multisample class definition.
    /// </summary>
    public class Texture2DMultisample : ATexture
    {
        #region Fields

        /// <summary>
        /// Stores the texture width
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the texture height.
        /// </summary>
        private int mHeight;

        /// <summary>
        /// Stores the texture sample per pixel count.
        /// </summary>
        private int mSampleCount;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int) * 3;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the texture 1D width
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
        }

        /// <summary>
        /// Gets the texture height.
        /// </summary>
        public int Height
        {
            get
            {
                return this.mHeight;
            }
        }

        /// <summary>
        /// Gets the texture sample per pixel count.
        /// </summary>
        public int SampleCount
        {
            get
            {
                return this.mSampleCount;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2DMultisample"/> class.
        /// </summary>
        /// <param name="pWidth">The texture's width</param>
        /// <param name="pHeight">The texture's height.</param>
        /// <param name="pSampleCount">The texture's sample per pixel count.</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pUseFixedLocations">The texture components count.</param>
        public Texture2DMultisample(int pWidth, int pHeight, int pSampleCount, PixelInternalFormat pGPUFormat, bool pUseFixedLocations) : 
        base( TextureTarget.Texture2DMultisample )
        {
            this.Initialize( pWidth, pHeight, pSampleCount, pGPUFormat, pUseFixedLocations );
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        public override void Swap(ITexture pTexture)
        {
            base.Swap( pTexture );

            Texture2DMultisample lOther = pTexture as Texture2DMultisample;
            Utilities.Swap( ref this.mWidth, ref lOther.mWidth );
            Utilities.Swap( ref this.mHeight, ref lOther.mHeight );
            Utilities.Swap( ref this.mSampleCount, ref lOther.mSampleCount );
        }

        /// <summary>
        /// Initializes the texture
        /// </summary>
        /// <param name="pWidth">The texture's width</param>
        /// <param name="pHeight">The texture's height.</param>
        /// <param name="pSampleCount">The texture's sample per pixel count.</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pUseFixedLocations">The texture components count.</param>
        private void Initialize(int pWidth, int pHeight, int pSampleCount, PixelInternalFormat pGPUFormat, bool pUseFixedLocations)
        {
            GL.GenTextures( 1, out this.mTextureId );
            if
                ( this.mTextureId <= 0 )
            {
                LogManager.Instance.Log( string.Format( "Cannot create a new texture of GPU format {0}!!!", pGPUFormat ), LogType.ERROR );
                return;
            }

            this.mInternalFormat = pGPUFormat;
            this.mWidth = pWidth;
            this.mHeight = pHeight;
            this.mSampleCount = pSampleCount;

            this.BindToTextureUnit();

            GL.TexImage2DMultisample( (TextureTargetMultisample)this.TextureType, this.mSampleCount, this.mInternalFormat, this.mWidth, this.mHeight, pUseFixedLocations );

            GLHelpers.GetError();
        }
        
        #endregion Methods
    }
}
