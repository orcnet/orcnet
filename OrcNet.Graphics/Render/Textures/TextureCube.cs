﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Graphics.Helpers;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture Cube class definition.
    /// </summary>
    public class TextureCube : ATexture
    {
        #region Fields

        /// <summary>
        /// Stores the texture width
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the texture height.
        /// </summary>
        private int mHeight;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the texture 1D width
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
        }

        /// <summary>
        /// Gets the texture height.
        /// </summary>
        public int Height
        {
            get
            {
                return this.mHeight;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureCube"/> class.
        /// </summary>
        /// <param name="pWidth">The texture's width</param>
        /// <param name="pHeight">The texture's height.</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBuffersParameters">Optional pixel faces buffers storage parameters.</param>
        /// <param name="pFacesBuffers">The pixels to be written into that texture.</param>
        public TextureCube(int pWidth, int pHeight, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters[] pBuffersParameters, IBuffer[] pFacesBuffers) : 
        base( TextureTarget.TextureCubeMap )
        {
            this.Initialize( pWidth, pHeight, pGPUFormat, pFormat, pType, pParameters, pBuffersParameters, pFacesBuffers );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Replaces the corresponding mipmap content of that texture.
        /// </summary>
        /// <param name="pFace">The cube face to modify.</param>
        /// <param name="pLevel">The mipmap level that must be changed.</param>
        /// <param name="pXOffset">The X offset of the part to be replaced.</param>
        /// <param name="pYOffset">The Y offset of the part to be replaced.</param>
        /// <param name="pWidth">The width of the part to be replaced.</param>
        /// <param name="pHeight">The height of the part to be replaced.</param>
        /// <param name="pFormat">The texture's components count</param>
        /// <param name="pType">The texture's components type.</param>
        /// <param name="pBufferParameter">The optional pixel storage parameters.</param>
        /// <param name="pPixels">The faces pixels to be written into the given mipmap.</param>
        public void SetMipmap(CubeFace pFace, int pLevel, int pXOffset, int pYOffset, int pWidth, int pHeight, PixelFormat pFormat, PixelType pType, BufferLayoutParameters pBufferParameter, IBuffer pPixels)
        {
            this.BindToTextureUnit();

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );
            pBufferParameter.Apply();

            GL.TexSubImage2D( (TextureTarget)pFace, pLevel, pXOffset, pYOffset, pWidth, pHeight, pFormat, pType, pPixels.GetData() );

            pBufferParameter.Clear();
            pPixels.Unbind( lUnpackBufferFlag );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Replaces the corresponding compressed mipmap content of that texture.
        /// </summary>
        /// <param name="pFace">The cube face to modify.</param>
        /// <param name="pLevel">The mipmap level that must be changed.</param>
        /// <param name="pXOffset">The X offset of the part to be replaced.</param>
        /// <param name="pYOffset">The Y offset of the part to be replaced.</param>
        /// <param name="pWidth">The width of the part to be replaced.</param>
        /// <param name="pHeight">The height of the part to be replaced.</param>
        /// <param name="pSizeInBytes">The size of the new content in bytes.</param>
        /// <param name="pPixels">The pixels to be written into the given mipmap.</param>
        public void SetCompressedMipmap(CubeFace pFace, int pLevel, int pXOffset, int pYOffset, int pWidth, int pHeight, int pSizeInBytes, IBuffer pPixels)
        {
            this.BindToTextureUnit();

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );

            // Commented the weird implem of bruneton... GL.TexSubImage3D( this.TextureType, pLevel, pXOffset, pYOffset, pZOffset, pWidth, pHeight, pDepth, (PixelFormat)this.mInternalFormat, (PixelType)pSizeInBytes, pPixels.GetData() );
            GL.CompressedTexSubImage2D( (TextureTarget)pFace, pLevel, pXOffset, pYOffset, pWidth, pHeight, (PixelFormat)this.InternalFormat, pSizeInBytes, pPixels.GetData() );
            
            pPixels.Unbind( lUnpackBufferFlag );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        public override void Swap(ITexture pTexture)
        {
            base.Swap( pTexture );

            TextureCube lOther = pTexture as TextureCube;
            Utilities.Swap( ref this.mWidth, ref lOther.mWidth );
            Utilities.Swap( ref this.mHeight, ref lOther.mHeight );
        }

        /// <summary>
        /// Initializes the texture
        /// </summary>
        /// <param name="pWidth">The texture width</param>
        /// <param name="pHeight">The texture's height</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBuffersParameters">Optional pixel faces buffers storage parameters.</param>
        /// <param name="pFacesBuffers">The faces pixels to be written into that texture.</param>
        protected void Initialize(int pWidth, int pHeight, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters[] pBuffersParameters, IBuffer[] pFacesBuffers)
        {
            this.Initialize( pGPUFormat, pParameters );
            this.mWidth  = pWidth;
            this.mHeight = pHeight;

            CubeFace[] cFaces = 
            {
                CubeFace.POSITIVE_X,
                CubeFace.NEGATIVE_X,
                CubeFace.POSITIVE_Y,
                CubeFace.NEGATIVE_Y,
                CubeFace.POSITIVE_Z,
                CubeFace.NEGATIVE_Z
            };

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            if ( this.IsCompressed )
            {
                for ( int lCurrFace = 0; lCurrFace < 6; lCurrFace++ )
                {
                    TextureTarget lFace = (TextureTarget)cFaces[ lCurrFace ];
                    IBuffer lCurrBuffer = pFacesBuffers[ lCurrFace ];
                    lCurrBuffer.Bind( lUnpackBufferFlag );
                    BufferLayoutParameters lCurrBufferParams = pBuffersParameters[ lCurrFace ];
                    if ( lCurrBufferParams.CompressedSize > 0 )
                    {
                        GL.CompressedTexImage2D( lFace, 0, this.mInternalFormat, this.mWidth, this.mHeight, 0, lCurrBufferParams.CompressedSize, lCurrBuffer.GetData() );
                    }
                    else
                    {
                        lCurrBufferParams.Apply();
                        GL.TexImage2D( lFace, 0, this.mInternalFormat, this.mWidth, this.mHeight, 0, pFormat, pType, lCurrBuffer.GetData() );
                        lCurrBufferParams.Clear();
                    }
                    lCurrBuffer.Unbind( lUnpackBufferFlag );
                }
            }
            else
            {
                for ( int lCurrFace = 0; lCurrFace < 6; lCurrFace++ )
                {
                    TextureTarget lFace = (TextureTarget)cFaces[ lCurrFace ];
                    IBuffer lCurrBuffer = pFacesBuffers[ lCurrFace ];
                    BufferLayoutParameters lCurrBufferParams = pBuffersParameters[ lCurrFace ];
                    lCurrBuffer.Bind( lUnpackBufferFlag );
                    lCurrBufferParams.Apply();
                    GL.TexImage2D( lFace, 0, this.mInternalFormat, this.mWidth, this.mHeight, 0, pFormat, pType, lCurrBuffer.GetData() );
                    lCurrBufferParams.Clear();
                    lCurrBuffer.Unbind( lUnpackBufferFlag );
                }
            }

            this.GenerateMipmaps();

            GLHelpers.GetError();
        }
        
        #endregion Methods
    }
}
