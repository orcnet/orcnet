﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Resource;
using System;
using System.Collections.Generic;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Base texture interface definition.
    /// </summary>
    public interface ITexture : IResourceCreatable, IMemoryProfilable, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the texture identifier.
        /// </summary>
        uint TextureId
        {
            get;
        }

        /// <summary>
        /// Gets the texture type being the GL target.
        /// </summary>
        TextureTarget TextureType
        {
            get;
        }

        /// <summary>
        /// Gets the texture component count.
        /// </summary>
        uint ComponentCount
        {
            get;
        }

        /// <summary>
        /// Gets the texture internal format.
        /// </summary>
        PixelInternalFormat InternalFormat
        {
            get;
        }

        /// <summary>
        /// Gets the texture format
        /// </summary>
        PixelFormat Format
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the texture has mipmaps or not.
        /// </summary>
        bool HasMipmaps
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the texture is compressed or not on the GPU.
        /// </summary>
        bool IsCompressed
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Checks whether the texture is used by the given programs or not.
        /// </summary>
        /// <param name="pProgramIds">The programs identifiers.</param>
        /// <returns>True if used at least by one program, false otherwise.</returns>
        bool IsUsedBy(IEnumerable<uint> pProgramIds);

        /// <summary>
        /// Adds a new program as user for that texture.
        /// </summary>
        /// <param name="pProgramId">The program identifier.</param>
        void AddUser(uint pProgramId);

        /// <summary>
        /// Removes the supplied program from users of that texture.
        /// </summary>
        /// <param name="pProgramId">The program identifier.</param>
        /// <returns>True if was an user, false otherwise.</returns>
        bool RemoveUser(uint pProgramId);

        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        void Swap(ITexture pTexture);

        /// <summary>
        /// Gets the mipmap compressed size at the given level.
        /// </summary>
        /// <param name="pLevel">The mipmap level.</param>
        /// <returns>The mipmap compressed size.</returns>
        int GetMipmapCompressedSize(int pLevel);

        /// <summary>
        /// Generates the mipmaps.
        /// </summary>
        void GenerateMipmaps();

        /// <summary>
        /// Gets the mipmap data at the given level in the supplied target format and type.
        /// </summary>
        /// <param name="pLevel">The mipmap level</param>
        /// <param name="pTargetFormat">The mipmap target format</param>
        /// <param name="pTargetType">The mipmap target type.</param>
        /// <param name="pData">The mipmap data.</param>
        void GetMipmap(int pLevel, PixelFormat pTargetFormat, PixelType pTargetType, IntPtr pData);

        /// <summary>
        /// Gets the compressed mipmap data at the given level. Only if compressed.
        /// see IsCompressed
        /// </summary>
        /// <param name="pLevel">The mipmap level</param>
        /// <param name="pData">The mipmap compressed data.</param>
        void GetCompressedMipmap(int pLevel, IntPtr pData);

        #endregion Methods
    }
}
