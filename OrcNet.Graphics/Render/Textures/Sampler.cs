﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Graphics.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture Sampler class definition.
    /// </summary>
    [DebuggerDisplay("Id = {SamplerId}")]
    public class Sampler : AMemoryProfilable, ISampler
    {
        #region Fields

        /// <summary>
        /// Stores the shared texture sampler instances. 
        /// All the samplers with the same options are presented with the same OpenGL instance.
        /// This map associates the shared sampler id and the corresponding sampler count to each
        /// possible value for the sampler parameters.
        /// </summary>
        private static Dictionary<SamplerParameters, KeyValuePair<uint, uint>> sInstances;

        /// <summary>
        /// Stores the constant sampler's parameters.
        /// </summary>
        private readonly SamplerParameters mParameters;

        /// <summary>
        /// Stores the sampler identifier.
        /// </summary>
        private uint mSamplerId;
        
        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(uint);
                lSize += this.mParameters.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the sampler's identifier.
        /// </summary>
        public uint SamplerId
        {
            get
            {
                return this.mSamplerId;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes the static member(s) of the <see cref="Sampler"/> class.
        /// </summary>
        static Sampler()
        {
            sInstances = new Dictionary<SamplerParameters, KeyValuePair<uint, uint>>( new SamplerParametersComparer() );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sampler"/> class.
        /// </summary>
        /// <param name="pParameters">The sampler parameters.</param>
        public Sampler(SamplerParameters pParameters)
        {
            this.mParameters = pParameters;

            KeyValuePair<uint, uint> lSharedInfo;
            if ( sInstances.TryGetValue( pParameters, out lSharedInfo ) )
            {
                this.mSamplerId = lSharedInfo.Key;
                sInstances[ pParameters ] = new KeyValuePair<uint, uint>( this.mSamplerId, lSharedInfo.Value + 1 );
            }
            else
            {
                // Unknown sampler parameters
                GL.GenSamplers( 1, out this.mSamplerId );
                
                if ( this.mSamplerId <= 0 )
                {
                    LogManager.Instance.Log( "Sampler generation failed!!!" );
                }
                
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureWrapS, (int)pParameters.WrapS );
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureWrapT, (int)pParameters.WrapT );
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureWrapR, (int)pParameters.WrapR );
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureMinFilter, (int)pParameters.MinFilter );
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureMagFilter, (int)pParameters.MagFilter );
                if ( pParameters.PassBorderAsFloat )
                {
                    Vector4F lColor = pParameters.BorderColorAsFloat;
                    GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureBorderColor, new float[] { lColor.X, lColor.Y, lColor.Z, lColor.W } );
                }
                else
                {
                    Color lBorder = pParameters.BorderColor;
                    GL.SamplerParameterI( this.mSamplerId, SamplerParameterName.TextureBorderColor, new int[] { lBorder.R, lBorder.G, lBorder.B, lBorder.A } );
                }
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureMinLod, pParameters.LODMin );
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureMaxLod, pParameters.LODMax );
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureLodBias, pParameters.LODBias );
                if ( pParameters.CompareFunction != TextureComparison.Always )
                {
                    GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRefToTexture );
                    GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureCompareFunc, (int)pParameters.CompareFunction );
                }
                GL.SamplerParameter( this.mSamplerId, SamplerParameterName.TextureMaxAnisotropyExt, pParameters.MaxAnisotropy );

                GLHelpers.GetError();

                sInstances[ pParameters ] = new KeyValuePair<uint, uint>( this.mSamplerId, 1 );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Overridable dispose method.
        /// </summary>
        protected virtual void OnDispose()
        {
            // Unbind sampler first.
            TextureUnitManager.Instance.Unbind( this );
            
            KeyValuePair<uint, uint> lSharedInfo;
            if
                ( sInstances.TryGetValue( this.mParameters, out lSharedInfo ) )
            {
                if
                    ( lSharedInfo.Value == 1 )
                {
                    GL.DeleteSamplers( 1, ref this.mSamplerId );
                    sInstances.Remove( this.mParameters );
                }
                else
                {
                    sInstances[ this.mParameters ] = new KeyValuePair<uint, uint>( this.mSamplerId, lSharedInfo.Value - 1 );
                }
            }
        }

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Sampler parameters comparer class definition.
        /// </summary>
        private class SamplerParametersComparer : IEqualityComparer<SamplerParameters>
        {
            #region Methods

            /// <summary>
            /// Checks whether the two parameters set are equal or not
            /// </summary>
            /// <param name="pSamplerParameters1">The first set</param>
            /// <param name="pSamplerParameters2">The second set</param>
            /// <returns>True if equal, false otherwise.</returns>
            public bool Equals(SamplerParameters pSamplerParameters1, SamplerParameters pSamplerParameters2)
            {
                return pSamplerParameters1.CompareTo( pSamplerParameters2 ) == 0;
            }

            /// <summary>
            /// Compute the hash code.
            /// </summary>
            /// <param name="pSamplerParameters"></param>
            /// <returns>The hash code.</returns>
            public int GetHashCode(SamplerParameters pSamplerParameters)
            {
                return pSamplerParameters.GetHashCode();
            }

            #endregion Methods
        }

        #endregion Inner classes
    }
}
