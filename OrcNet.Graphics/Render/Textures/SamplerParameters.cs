﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Math;
using System;
using System.Drawing;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture sampler parameters.
    /// </summary>
    public class SamplerParameters : AMemoryProfilable, IComparable<SamplerParameters>
    {
        #region Fields

        /// <summary>
        /// Stores the border color.
        /// </summary>
        private Color             mBorderColor;
        
        /// <summary>
        /// Stores the texture comparison function.
        /// </summary>
        private TextureComparison mCompareFunction;

        /// <summary>
        /// Stores the S wrap mode.
        /// </summary>
        private TextureWrapMode  mWrapS;

        /// <summary>
        /// Stores the T wrap mode.
        /// </summary>
        private TextureWrapMode  mWrapT;

        /// <summary>
        /// Stores the R wrap mode.
        /// </summary>
        private TextureWrapMode  mWrapR;

        /// <summary>
        /// Stores the filtering mode to use if scale the image up.
        /// </summary>
        private TextureMagFilter mMagFilter;

        /// <summary>
        /// Stores the filtering mode to use if scale the image down.
        /// </summary>
        private TextureMinFilter mMinFiler;

        /// <summary>
        /// Stores the minimum LOD level.
        /// </summary>
        private float mLODMin;

        /// <summary>
        /// Stores the maximum LOD level.
        /// </summary>
        private float mLODMax;

        /// <summary>
        /// Stores the distance threshold the LOD must switch to a lower resolution.
        /// </summary>
        private float mLODBias;

        /// <summary>
        /// Stores the maximum anisotropy factor
        /// </summary>
        private float mMaxAnisotropy;

        /// <summary>
        /// Stores the flag indicating whether the border color must be passed as floats or integers.
        /// </summary>
        private bool mPassBorderAsFloat;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(byte) * 4; // Color.
                lSize += sizeof(float) * 4;
                lSize += sizeof(TextureComparison);
                lSize += sizeof(TextureWrapMode) * 3;
                lSize += sizeof(TextureMagFilter);
                lSize += sizeof(TextureMinFilter);
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets or sets the border color.
        /// </summary>
        public Color BorderColor
        {
            get
            {
                return this.mBorderColor;
            }
            set
            {
                if
                    ( this.mBorderColor != value )
                {
                    this.mBorderColor = value;
                    this.mPassBorderAsFloat = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the border color as floats vector 
        /// where X is R, Y is G, Z is B and W is A.
        /// </summary>
        public Vector4F BorderColorAsFloat
        {
            get
            {
                Vector4F lColor = new Vector4F( this.mBorderColor.R / 255.0f, this.mBorderColor.G / 255.0f, this.mBorderColor.B / 255.0f, this.mBorderColor.A / 255.0f );
                return lColor;
            }
            set
            {
                this.mBorderColor = Color.FromArgb( (int)(value.W * 255), (int)(value.X * 255), (int)(value.Y * 255), (int)(value.Z * 255) );
                this.mPassBorderAsFloat = true;
            }
        }
        
        /// <summary>
        /// Gets or sets the texture comparison function.
        /// </summary>
        public TextureComparison CompareFunction
        {
            get
            {
                return this.mCompareFunction;
            }
            set
            {
                if
                    ( this.mCompareFunction != value )
                {
                    this.mCompareFunction = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the S wrap mode.
        /// </summary>
        public TextureWrapMode WrapS
        {
            get
            {
                return this.mWrapS;
            }
            set
            {
                if
                    ( this.mWrapS != value )
                {
                    this.mWrapS = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the T wrap mode.
        /// </summary>
        public TextureWrapMode WrapT
        {
            get
            {
                return this.mWrapT;
            }
            set
            {
                if
                    ( this.mWrapT != value )
                {
                    this.mWrapT = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the R wrap mode.
        /// </summary>
        public TextureWrapMode WrapR
        {
            get
            {
                return this.mWrapR;
            }
            set
            {
                if
                    ( this.mWrapR != value )
                {
                    this.mWrapR = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the filtering mode to use if scale the image up.
        /// </summary>
        public TextureMagFilter MagFilter
        {
            get
            {
                return this.mMagFilter;
            }
            set
            {
                if
                    ( this.mMagFilter != value )
                {
                    this.mMagFilter = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the filtering mode to use if scale the image down.
        /// </summary>
        public TextureMinFilter MinFilter
        {
            get
            {
                return this.mMinFiler;
            }
            set
            {
                if
                    ( this.mMinFiler != value )
                {
                    this.mMinFiler = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the minimum LOD level.
        /// </summary>
        public float LODMin
        {
            get
            {
                return this.mLODMin;
            }
            set
            {
                if
                    ( this.mLODMin != value )
                {
                    this.mLODMin = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the maximum LOD level.
        /// </summary>
        public float LODMax
        {
            get
            {
                return this.mLODMax;
            }
            set
            {
                if
                    ( this.mLODMax != value )
                {
                    this.mLODMax = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the distance threshold the LOD must switch to a lower resolution.
        /// </summary>
        public float LODBias
        {
            get
            {
                return this.mLODBias;
            }
            set
            {
                if
                    ( this.mLODBias != value )
                {
                    this.mLODBias = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the maximum anisotropy factor
        /// </summary>
        public float MaxAnisotropy
        {
            get
            {
                return this.mMaxAnisotropy;
            }
            set
            {
                if
                    ( this.mMaxAnisotropy != value )
                {
                    this.mMaxAnisotropy = value;
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the border color must be passed 
        /// as floats or integers.
        /// </summary>
        public bool PassBorderAsFloat
        {
            get
            {
                return this.mPassBorderAsFloat;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SamplerParameters"/> class.
        /// </summary>
        public SamplerParameters()
        {
            this.mWrapS = TextureWrapMode.ClampToEdge;
            this.mWrapT = TextureWrapMode.ClampToEdge;
            this.mWrapR = TextureWrapMode.ClampToEdge;
            this.mMinFiler = TextureMinFilter.Nearest;
            this.mMagFilter = TextureMagFilter.Linear;
            this.mBorderColor = Color.FromArgb( 0, 0, 0, 0 );
            this.mLODMin  = -1000.0f;
            this.mLODMax  = 1000.0f;
            this.mLODBias = 0.0f;
            this.mMaxAnisotropy = 1.0f;
            this.mCompareFunction = TextureComparison.Always;
            this.mPassBorderAsFloat = false;
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Compare two Sampler parameters objects.
        /// </summary>
        /// <param name="pOther">Another parameters set.</param>
        /// <returns>0 if equal, -1 if smaller, 1 if greater.</returns>
        public int CompareTo(SamplerParameters pOther)
        {
            if 
                ( this.mWrapS < pOther.mWrapS )
            {
                return 1;
            }

            if 
                ( this.mWrapS > pOther.mWrapS )
            {
                return -1;
            }

            if 
                ( this.mWrapT < pOther.mWrapT )
            {
                return 1;
            }
            if 
                ( this.mWrapT > pOther.mWrapT )
            {
                return -1;
            }

            if 
                ( this.mWrapR < pOther.mWrapR )
            {
                return 1;
            }

            if 
                ( this.mWrapR > pOther.mWrapR )
            {
                return -1;
            }

            if 
                ( this.mMinFiler < pOther.mMinFiler )
            {
                return 1;
            }

            if 
                ( this.mMagFilter > pOther.mMagFilter )
            {
                return -1;
            }
            
            if 
                ( this.BorderColor.R < pOther.BorderColor.R &&
                  this.BorderColor.G < pOther.BorderColor.G &&
                  this.BorderColor.B < pOther.BorderColor.B &&
                  this.BorderColor.A < pOther.BorderColor.A )
            {
                return 1;
            }

            if 
                ( this.BorderColor.R > pOther.BorderColor.R &&
                  this.BorderColor.G > pOther.BorderColor.G &&
                  this.BorderColor.B > pOther.BorderColor.B &&
                  this.BorderColor.A > pOther.BorderColor.A )
            {
                return -1;
            }

            if 
                ( this.mLODMin < pOther.mLODMin )
            {
                return 1;
            }

            if 
                ( this.mLODMin > pOther.mLODMin )
            {
                return -1;
            }

            if
                ( this.mLODMax < pOther.mLODMax )
            {
                return 1;
            }

            if 
                ( this.mLODMax > pOther.mLODMax )
            {
                return -1;
            }

            if 
                ( this.mLODBias < pOther.mLODBias )
            {
                return 1;
            }

            if 
                ( this.mLODBias > pOther.mLODBias )
            {
                return -1;
            }

            if 
                ( this.mMaxAnisotropy < pOther.mMaxAnisotropy )
            {
                return 1;
            }

            if 
                ( this.mMaxAnisotropy > pOther.mMaxAnisotropy )
            {
                return -1;
            }

            if
                ( this.mCompareFunction < pOther.mCompareFunction )
            {
                return 1;
            }

            return -1;
        }

        /// <summary>
        /// Computes the sampler parameters hashcode.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            int lHashCode = this.mBorderColor.GetHashCode();
            lHashCode += this.mMinFiler.GetHashCode() + this.mMagFilter.GetHashCode() + this.mLODMin.GetHashCode() + this.mLODMax.GetHashCode() + this.mLODBias.GetHashCode() + this.mMaxAnisotropy.GetHashCode();
            lHashCode += this.mWrapR.GetHashCode() + this.mWrapS.GetHashCode() + this.mWrapT.GetHashCode() + this.mCompareFunction.GetHashCode();
            return base.GetHashCode();
        }

        #endregion Methods
    }
}
