﻿namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture parameters class definition.
    /// </summary>
    public class TextureParameters : SamplerParameters
    {
        #region Fields

        /// <summary>
        /// Stores the swizzling order of this texture to allow to reorder components
        /// before to send it to the GPU.
        /// </summary>
        private char[] mSwizzle;

        /// <summary>
        /// Stores the minimum LOD level index.
        /// </summary>
        private int mMinLODIndex;

        /// <summary>
        /// Stores the maximum LOD level index.
        /// </summary>
        private int mMaxLODIndex;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the swizzling order of this texture to allow to reorder components
        /// before to send it to the GPU.
        /// </summary>
        public char[] Swizzle
        {
            get
            {
                return this.mSwizzle;
            }
            set
            {
                if
                    ( this.mSwizzle != value )
                {
                    this.mSwizzle = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the minimum LOD level index.
        /// </summary>
        public int MinLODIndex
        {
            get
            {
                return this.mMinLODIndex;
            }
            set
            {
                if
                    ( this.mMinLODIndex != value )
                {
                    this.mMinLODIndex = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the maximum LOD level index.
        /// </summary>
        public int MaxLODIndex
        {
            get
            {
                return this.mMaxLODIndex;
            }
            set
            {
                if
                    ( this.mMaxLODIndex != value )
                {
                    this.mMaxLODIndex = value;
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureParameters"/> class.
        /// </summary>
        public TextureParameters()
        {
            this.mSwizzle = new char[ 4 ] { 'r', 'g', 'b', 'a' };
            this.mMinLODIndex = 0;
            this.mMaxLODIndex = 1000;
        }

        #endregion Constructor
    }
}
