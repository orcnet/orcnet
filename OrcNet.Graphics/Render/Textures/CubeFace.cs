﻿namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Enumeration of the different cube map faces.
    /// </summary>
    public enum CubeFace
    {
        /// <summary>
        /// The cube map positive X face.
        /// </summary>
        POSITIVE_X = 34069,

        /// <summary>
        /// The cube map negative X face.
        /// </summary>
        NEGATIVE_X = 34070,

        /// <summary>
        /// The cube map positive Y face.
        /// </summary>
        POSITIVE_Y = 34071,

        /// <summary>
        /// The cube map negative Y face.
        /// </summary>
        NEGATIVE_Y = 34072,

        /// <summary>
        /// The cube map positive Z face.
        /// </summary>
        POSITIVE_Z = 34073,

        /// <summary>
        /// The cube map negative Z face.
        /// </summary>
        NEGATIVE_Z = 34074
    }
}
