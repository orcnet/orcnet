﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Graphics.Helpers;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture 2D Array class definition.
    /// </summary>
    public class Texture2DArray : ATexture
    {
        #region Fields

        /// <summary>
        /// Stores the texture width
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the texture height.
        /// </summary>
        private int mHeight;

        /// <summary>
        /// Stores the texture layer count.
        /// </summary>
        private int mLayerCount;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int) * 3;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable
        
        /// <summary>
        /// Gets the texture 1D width
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
        }

        /// <summary>
        /// Gets the texture height.
        /// </summary>
        public int Height
        {
            get
            {
                return this.mHeight;
            }
        }

        /// <summary>
        /// Gets the texture layer count.
        /// </summary>
        public int LayerCount
        {
            get
            {
                return this.mLayerCount;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2DArray"/> class.
        /// </summary>
        /// <param name="pWidth">The texture's width</param>
        /// <param name="pHeight">The texture's height.</param>
        /// <param name="pLayerCount">The texture's layer count.</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBufferParameter">Optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into that texture.</param>
        public Texture2DArray(int pWidth, int pHeight, int pLayerCount, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters pBufferParameter, IBuffer pPixels) : 
        base( TextureTarget.Texture2DArray )
        {
            this.Initialize( pWidth, pHeight, pLayerCount, pGPUFormat, pFormat, pType, pParameters, pBufferParameter, pPixels );
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Replaces the corresponding mipmap content of that texture.
        /// </summary>
        /// <param name="pLevel">The mipmap level that must be changed.</param>
        /// <param name="pXOffset">The X offset of the part to be replaced.</param>
        /// <param name="pYOffset">The Y offset of the part to be replaced.</param>
        /// <param name="pFirstLayer">The first layer of that array to be replaced.</param>
        /// <param name="pLayerCount">The layer count to be replaced after the first one.</param>
        /// <param name="pWidth">The width of the part to be replaced.</param>
        /// <param name="pHeight">The height of the part to be replaced.</param>
        /// <param name="pFormat">The texture's components count</param>
        /// <param name="pType">The texture's components type.</param>
        /// <param name="pBufferParameter">The optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into the given mipmap.</param>
        public void SetMipmap(int pLevel, int pXOffset, int pYOffset, int pFirstLayer, int pLayerCount, int pWidth, int pHeight, PixelFormat pFormat, PixelType pType, BufferLayoutParameters pBufferParameter, IBuffer pPixels)
        {
            this.BindToTextureUnit();

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );
            pBufferParameter.Apply();

            GL.TexSubImage3D( this.TextureType, pLevel, pXOffset, pYOffset, pFirstLayer, pWidth, pHeight, pLayerCount, pFormat, pType, pPixels.GetData() );

            pBufferParameter.Clear();
            pPixels.Unbind( lUnpackBufferFlag );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Replaces the corresponding compressed mipmap content of that texture.
        /// </summary>
        /// <param name="pLevel">The mipmap level that must be changed.</param>
        /// <param name="pXOffset">The X offset of the part to be replaced.</param>
        /// <param name="pYOffset">The Y offset of the part to be replaced.</param>
        /// <param name="pFirstLayer">The first layer of that array to be replaced.</param>
        /// <param name="pLayerCount">The layer count to be replaced after the first one.</param>
        /// <param name="pWidth">The width of the part to be replaced.</param>
        /// <param name="pHeight">The height of the part to be replaced.</param>
        /// <param name="pSizeInBytes">The size of the new content in bytes.</param>
        /// <param name="pPixels">The pixels to be written into the given mipmap.</param>
        public void SetCompressedMipmap(int pLevel, int pXOffset, int pYOffset, int pFirstLayer, int pLayerCount, int pWidth, int pHeight, int pSizeInBytes, IBuffer pPixels)
        {
            this.BindToTextureUnit();

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );

            GL.CompressedTexSubImage3D( this.TextureType, pLevel, pXOffset, pYOffset, pFirstLayer, pWidth, pHeight, pLayerCount, (PixelFormat)this.InternalFormat, pSizeInBytes, pPixels.GetData() );
            
            pPixels.Unbind( lUnpackBufferFlag );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        public override void Swap(ITexture pTexture)
        {
            base.Swap( pTexture );

            Texture2DArray lOther = pTexture as Texture2DArray;
            Utilities.Swap( ref this.mWidth, ref lOther.mWidth );
            Utilities.Swap( ref this.mHeight, ref lOther.mHeight );
            Utilities.Swap( ref this.mLayerCount, ref lOther.mLayerCount );
        }

        /// <summary>
        /// Initializes the texture
        /// </summary>
        /// <param name="pWidth">The texture width</param>
        /// <param name="pHeight">The texture's height</param>
        /// <param name="pLayerCount">The texture's layer count.</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBufferParameter">Optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into that texture.</param>
        protected void Initialize(int pWidth, int pHeight, int pLayerCount, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters pBufferParameter, IBuffer pPixels)
        {
            this.Initialize( pGPUFormat, pParameters );
            this.mWidth  = pWidth;
            this.mHeight = pHeight;
            this.mLayerCount = pLayerCount;

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );
            if
                ( this.IsCompressed &&
                  pBufferParameter.CompressedSize > 0 )
            {
                GL.CompressedTexImage3D( this.TextureType, 0, this.InternalFormat, pWidth, pHeight, pLayerCount, 0, pBufferParameter.CompressedSize, pPixels.GetData() );
            }
            else
            {
                pBufferParameter.Apply();
                GL.TexImage3D( this.TextureType, 0, this.InternalFormat, pWidth, pHeight, pLayerCount, 0, pFormat, pType, pPixels.GetData() );
                pBufferParameter.Clear();
            }
            pPixels.Unbind( lUnpackBufferFlag );

            this.GenerateMipmaps();

            GLHelpers.GetError();
        }
        
        #endregion Methods
    }
}
