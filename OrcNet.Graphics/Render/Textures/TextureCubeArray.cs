﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Graphics.Helpers;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture Cube Array class definition.
    /// </summary>
    public class TextureCubeArray : ATexture
    {
        #region Fields

        /// <summary>
        /// Stores the texture width
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the texture height.
        /// </summary>
        private int mHeight;

        /// <summary>
        /// Stores the texture layer count.
        /// </summary>
        private int mLayerCount;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int) * 3;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the texture 1D width
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
        }

        /// <summary>
        /// Gets the texture height.
        /// </summary>
        public int Height
        {
            get
            {
                return this.mHeight;
            }
        }

        /// <summary>
        /// Gets the texture layer count.
        /// </summary>
        public int LayerCount
        {
            get
            {
                return this.mLayerCount;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureCubeArray"/> class.
        /// </summary>
        /// <param name="pWidth">The texture's width</param>
        /// <param name="pHeight">The texture's height.</param>
        /// <param name="pLayerCount">The texture's layer count.</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBuffersParameter">Optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into that texture. Those pixels must be specified as in 3D texture with one face per layer in the following order: PosX NegX PosY NegY PosZ NegZ.</param>
        public TextureCubeArray(int pWidth, int pHeight, int pLayerCount, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters pBuffersParameter, IBuffer pPixels) : 
        base( TextureTarget.TextureCubeMapArray )
        {
            this.Initialize( pWidth, pHeight, pLayerCount, pGPUFormat, pFormat, pType, pParameters, pBuffersParameter, pPixels );
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        public override void Swap(ITexture pTexture)
        {
            base.Swap( pTexture );

            TextureCubeArray lOther = pTexture as TextureCubeArray;
            Utilities.Swap( ref this.mWidth, ref lOther.mWidth );
            Utilities.Swap( ref this.mHeight, ref lOther.mHeight );
            Utilities.Swap( ref this.mLayerCount, ref lOther.mLayerCount );
        }

        /// <summary>
        /// Initializes the texture
        /// </summary>
        /// <param name="pWidth">The texture width</param>
        /// <param name="pHeight">The texture's height</param>
        /// <param name="pLayerCount">The texture's layer count.</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBuffersParameter">Optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into that texture. Those pixels must be specified as in 3D texture with one face per layer in the following order: PosX NegX PosY NegY PosZ NegZ.</param>
        private void Initialize(int pWidth, int pHeight, int pLayerCount, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters pBuffersParameter, IBuffer pPixels)
        {
            this.Initialize( pGPUFormat, pParameters );
            this.mWidth  = pWidth;
            this.mHeight = pHeight;
            this.mLayerCount = pLayerCount;
            
            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );
            if
                ( this.IsCompressed )
            {
                GL.CompressedTexImage3D( this.TextureType, 0, this.mInternalFormat, this.mWidth, this.mHeight, 6 * this.mLayerCount, 0, pBuffersParameter.CompressedSize, pPixels.GetData() );
            }
            else
            {
                pBuffersParameter.Apply();
                GL.TexImage3D( this.TextureType, 0, this.mInternalFormat, this.mWidth, this.mHeight, 6 * this.mLayerCount, 0, pFormat, pType, pPixels.GetData() );
                pBuffersParameter.Clear();
            }
            pPixels.Unbind( lUnpackBufferFlag );

            this.GenerateMipmaps();

            GLHelpers.GetError();
        }
        
        #endregion Methods
    }
}
