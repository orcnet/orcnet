﻿using OrcNet.Core;
using System;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Base sampler interface definition
    /// </summary>
    public interface ISampler : IMemoryProfilable, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the sampler's identifier.
        /// </summary>
        uint SamplerId
        {
            get;
        }

        #endregion Properties
    }
}
