﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Graphics.Helpers;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture 1D class definition.
    /// </summary>
    public class Texture1D : ATexture
    {
        #region Fields

        /// <summary>
        /// Stores the texture 1D width
        /// </summary>
        private int mWidth;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the texture 1D width
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture1D"/> class.
        /// </summary>
        /// <param name="pWidth">The texture width</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBufferParameter">Optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into that texture.</param>
        public Texture1D(int pWidth, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters pBufferParameter, IBuffer pPixels) : 
        base( TextureTarget.Texture1D )
        {
            this.Initialize( pWidth, pGPUFormat, pFormat, pType, pParameters, pBufferParameter, pPixels );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Replaces the corresponding mipmap content of that texture.
        /// </summary>
        /// <param name="pLevel">The mipmap level that must be changed.</param>
        /// <param name="pLeftBorder">The left border of the part to be replaced.</param>
        /// <param name="pWidth">The size of the part to be replaced.</param>
        /// <param name="pFormat">The texture's components count</param>
        /// <param name="pType">The texture's components type.</param>
        /// <param name="pBufferParameter">The optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into the given mipmap.</param>
        public void SetMipmap(int pLevel, int pLeftBorder, int pWidth, PixelFormat pFormat, PixelType pType, BufferLayoutParameters pBufferParameter, IBuffer pPixels)
        {
            this.BindToTextureUnit();

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );
            pBufferParameter.Apply();

            GL.TexSubImage1D( TextureTarget.Texture1D, pLevel, pLeftBorder, pWidth, pFormat, pType, pPixels.GetData() );

            pBufferParameter.Clear();
            pPixels.Unbind( lUnpackBufferFlag );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Replaces the corresponding compressed mipmap content of that texture.
        /// </summary>
        /// <param name="pLevel">The mipmap level that must be changed.</param>
        /// <param name="pLeftBorder">The left border of the part to be replaced.</param>
        /// <param name="pWidth">The size of the part to be replaced.</param>
        /// <param name="pSizeInBytes">The size of the new content in bytes.</param>
        /// <param name="pPixels">The pixels to be written into the given mipmap.</param>
        public void SetCompressedMipmap(int pLevel, int pLeftBorder, int pWidth, int pSizeInBytes, IBuffer pPixels)
        {
            this.BindToTextureUnit();

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );

            GL.CompressedTexSubImage1D( TextureTarget.Texture1D, pLevel, pLeftBorder, pWidth, (PixelFormat)this.InternalFormat, pSizeInBytes, pPixels.GetData() );
            
            pPixels.Unbind( lUnpackBufferFlag );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        public override void Swap(ITexture pTexture)
        {
            base.Swap( pTexture );

            Texture1D lOther = pTexture as Texture1D;
            Utilities.Swap( ref this.mWidth, ref lOther.mWidth );
        }

        /// <summary>
        /// Initializes the texture
        /// </summary>
        /// <param name="pWidth">The texture width</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBufferParameter">Optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into that texture.</param>
        protected void Initialize(int pWidth, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters pBufferParameter, IBuffer pPixels)
        {
            this.Initialize( pGPUFormat, pParameters );
            this.mWidth = pWidth;

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );
            if
                ( this.IsCompressed &&
                  pBufferParameter.CompressedSize > 0 )
            {
                GL.CompressedTexImage1D( this.TextureType, 0, this.InternalFormat, pWidth, 0, pBufferParameter.CompressedSize, pPixels.GetData() );
            }
            else
            {
                pBufferParameter.Apply();
                GL.TexImage1D( this.TextureType, 0, this.InternalFormat, pWidth, 0, pFormat, pType, pPixels.GetData() );
                pBufferParameter.Clear();
            }
            pPixels.Unbind( lUnpackBufferFlag );

            this.GenerateMipmaps();

            GLHelpers.GetError();
        }
        
        #endregion Methods
    }
}
