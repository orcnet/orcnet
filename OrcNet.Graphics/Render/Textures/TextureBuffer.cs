﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Graphics.Helpers;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture Buffer class definition.
    /// </summary>
    public class TextureBuffer : ATexture
    {
        #region Fields

        /// <summary>
        /// Stores the texture width
        /// </summary>
        private int     mWidth;

        /// <summary>
        /// Stores the buffer holding the texel array.
        /// </summary>
        private IBuffer mTexels;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the texture 1D width
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
        }

        /// <summary>
        /// Gets the buffer holding the texel array.
        /// </summary>
        public IBuffer Texels
        {
            get
            {
                return this.mTexels;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureBuffer"/> class.
        /// </summary>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pPixels">The pixels to be written into that texture.</param>
        public TextureBuffer(PixelInternalFormat pGPUFormat, GPUBuffer pPixels) : 
        base( TextureTarget.TextureBuffer )
        {
            this.Initialize( pGPUFormat, pPixels );
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        public override void Swap(ITexture pTexture)
        {
            base.Swap( pTexture );

            TextureBuffer lOther = pTexture as TextureBuffer;
            Utilities.Swap( ref this.mWidth, ref lOther.mWidth );
            Utilities.Swap( ref this.mTexels, ref lOther.mTexels );
        }

        /// <summary>
        /// Initializes the texture
        /// </summary>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pPixels">The pixels to be written into that texture.</param>
        protected void Initialize(PixelInternalFormat pGPUFormat, GPUBuffer pPixels)
        {
            int lFormatSize;
            switch 
                ( pGPUFormat )
            {
                case PixelInternalFormat.R8:
                case PixelInternalFormat.R8i:
                case PixelInternalFormat.R8ui:
                    lFormatSize = 1;
                    break;
                case PixelInternalFormat.R16:
                case PixelInternalFormat.R16i:
                case PixelInternalFormat.R16ui:
                case PixelInternalFormat.R16f:
                case PixelInternalFormat.Rg8:
                case PixelInternalFormat.Rg8i:
                case PixelInternalFormat.Rg8ui:
                    lFormatSize = 2;
                    break;
                case PixelInternalFormat.R32i:
                case PixelInternalFormat.R32ui:
                case PixelInternalFormat.R32f:
                case PixelInternalFormat.Rg16:
                case PixelInternalFormat.Rg16i:
                case PixelInternalFormat.Rg16ui:
                case PixelInternalFormat.Rg16f:
                case PixelInternalFormat.Rgba8:
                case PixelInternalFormat.Rgba8i:
                case PixelInternalFormat.Rgba8ui:
                    lFormatSize = 4;
                    break;
                case PixelInternalFormat.Rg32i:
                case PixelInternalFormat.Rg32ui:
                case PixelInternalFormat.Rg32f:
                case PixelInternalFormat.Rgba16:
                case PixelInternalFormat.Rgba16i:
                case PixelInternalFormat.Rgba16ui:
                case PixelInternalFormat.Rgba16f:
                    lFormatSize = 8;
                    break;
                case PixelInternalFormat.Rgba32i:
                case PixelInternalFormat.Rgba32ui:
                case PixelInternalFormat.Rgba32f:
                    lFormatSize = 16;
                    break;
                default:
                    {
                        LogManager.Instance.Log( "GPU texture format not allowed for a texture buffer!!!", LogType.ERROR );
                        return;
                    }
            }

            TextureParameters lParameters = new TextureParameters();
            lParameters.WrapS = TextureWrapMode.ClampToEdge;
            lParameters.WrapT = TextureWrapMode.ClampToEdge;
            lParameters.WrapR = TextureWrapMode.ClampToEdge;
            lParameters.MinFilter = TextureMinFilter.Nearest;
            lParameters.MagFilter = TextureMagFilter.Nearest;
            lParameters.MaxLODIndex = 0;

            this.Initialize( pGPUFormat, lParameters );
            this.mWidth  = pPixels.BufferSize / lFormatSize;
            this.mTexels = pPixels;

            GL.TexBuffer( (TextureBufferTarget)this.TextureType, (SizedInternalFormat)this.mInternalFormat, pPixels.BufferId );

            GLHelpers.GetError();
        }
        
        #endregion Methods
    }
}
