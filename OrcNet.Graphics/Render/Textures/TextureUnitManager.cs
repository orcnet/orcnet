﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using OrcNet.Graphics.Helpers;
using OrcNet.Graphics.Services;
using System.Collections.Generic;
using System.Linq;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture unit manager class definition.
    /// </summary>
    public sealed class TextureUnitManager
    {
        #region Fields

        /// <summary>
        /// Stores the set of texture units.
        /// </summary>
        private TextureUnit[] mUnits;

        /// <summary>
        /// Stores the counter used to measure texture binding times which is incremented
        /// each time a texture is bound.
        /// </summary>
        private uint mBindingCounter;

        /// <summary>
        /// Stores the texture to sampler map.
        /// There is one possible binding per sampler object (a texture can be
        /// bound to several units with different sampler objects).
        /// </summary>
        private static Dictionary<uint, Dictionary<uint, uint>> sTextureUnitMap;

        /// <summary>
        /// Stores the texture unit manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile TextureUnitManager sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the texture unit manager handle.
        /// </summary>
        public static TextureUnitManager Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if
                    (sInstance == null)
                {
                    // Lock on
                    lock
                        (sSyncRoot)
                    {
                        // Delay instantiation until the object is first accessed
                        if
                            (sInstance == null)
                        {
                            sInstance = new TextureUnitManager();
                        }
                    }
                }

                return sInstance;
            }
        }

        /// <summary>
        /// Gets the flags indicating whether any unit is being used.
        /// </summary>
        public bool AnyUnitUsed
        {
            get
            {
                return sTextureUnitMap.Count > 0;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes the static member(s) of the <see cref="TextureUnitManager"/> class.
        /// </summary>
        static TextureUnitManager()
        {
            sTextureUnitMap = new Dictionary<uint, Dictionary<uint, uint>>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureUnitManager"/> class.
        /// </summary>
        private TextureUnitManager()
        {
            uint lMaxUnits = GLHelpers.GetMaxTextureUnits();
            this.mUnits = new TextureUnit[ lMaxUnits ];
            for
                ( int lCurrUnit = 0; lCurrUnit < lMaxUnits; lCurrUnit++ )
            {
                this.mUnits[ lCurrUnit ] = new TextureUnit( (uint)lCurrUnit );
            }

            this.mBindingCounter = 0;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Free the associated texture unit the given texture and sampler are mapped with.
        /// </summary>
        /// <param name="pTextureId">The texture ID</param>
        /// <param name="pSamplerId">The sampler ID</param>
        /// <returns>True if anything freed, false otherwise.</returns>
        internal bool FreeTextureFromUnit(uint pTextureId, uint pSamplerId)
        {
            Dictionary<uint, uint> lSamplerToUnitMap;
            if
                ( sTextureUnitMap.TryGetValue( pTextureId, out lSamplerToUnitMap ) )
            {
                return lSamplerToUnitMap.Remove(pSamplerId);
            }

            return false;
        }

        /// <summary>
        /// Maps the given texture and sampler with the supplied unit using its Id
        /// </summary>
        /// <param name="pTextureId">The texture ID</param>
        /// <param name="pSamplerId">The sampler ID</param>
        /// <param name="pUnitId">The unit ID</param>
        internal void MapTextureWithUnit(uint pTextureId, uint pSamplerId, uint pUnitId)
        {
            Dictionary<uint, uint> lSamplerToUnitMap;
            if
                (sTextureUnitMap.TryGetValue(pTextureId, out lSamplerToUnitMap))
            {
                if
                    (lSamplerToUnitMap.ContainsKey(pSamplerId) == false)
                {
                    lSamplerToUnitMap.Add(pSamplerId, pUnitId);
                }
            }
            else
            {
                lSamplerToUnitMap = new Dictionary<uint, uint>();
                lSamplerToUnitMap.Add(pSamplerId, pUnitId);
                sTextureUnitMap.Add(pTextureId, lSamplerToUnitMap);
            }
        }

        /// <summary>
        /// Gets the first used unit by the given texture.
        /// </summary>
        /// <param name="pTexture">The texture whose unit is used with.</param>
        /// <returns>The unit id, -1 otherwise.</returns>
        internal int FirstUsedUnit(ITexture pTexture)
        {
            int lUnitId = -1;
            // Returns the first one if multiples.
            Dictionary<uint, uint> lSamplerToUnitMap;
            if
                ( sTextureUnitMap.TryGetValue( pTexture.TextureId, out lSamplerToUnitMap ) )
            {
                if
                    ( lSamplerToUnitMap.Count > 0 ) // If any
                {
                    lUnitId = (int)lSamplerToUnitMap.First().Value;
                }
            }

            return lUnitId;
        }

        /// <summary>
        /// Gets the unit used by the given texture AND sampler.
        /// </summary>
        /// <param name="pTexture">The texture</param>
        /// <param name="pSampler">The texture's sampler.</param>
        /// <returns>The unit id, -1 otherwise.</returns>
        internal int UsedUnit(ITexture pTexture, ISampler pSampler)
        {
            int lUnitId = -1;
            Dictionary<uint, uint> lSamplerToUnitMap;
            if
                ( sTextureUnitMap.TryGetValue( pTexture.TextureId, out lSamplerToUnitMap ) )
            {
                uint lId;
                uint lSamplerId = pSampler == null ? 0 : pSampler.SamplerId;
                if
                    ( lSamplerToUnitMap.TryGetValue( lSamplerId, out lId ) )
                {
                    lUnitId = (int)lId;
                }
            }

            return lUnitId;
        }

        /// <summary>
        /// Finds a free texture unit and if no unit is available, gets 
        /// the oldest unused texture unit by supplied programs.
        /// </summary>
        /// <param name="pPassIds">The pass(es) ids</param>
        /// <returns>The available unit identifier, -1 otherwise.</returns>
        public int FindAvailableTextureUnit(IEnumerable<uint> pPassIds)
        {
            // First, attempts to find an available unit.
            for
                ( int lCurrUnit = 0; lCurrUnit < GLHelpers.cMAX_TEXTURE_UNITS; lCurrUnit++ )
            {
                if
                    ( this.mUnits[ lCurrUnit ].IsAvailable )
                {
                    return lCurrUnit;
                }
            }

            // If all units are not available, look for the oldest unused
            // unit by supplied programs.
            int lOldestUnit = -1;
            uint lOldestBindingTime = this.mBindingCounter;
            for
                ( int lCurrUnit = 0; lCurrUnit < GLHelpers.cMAX_TEXTURE_UNITS; lCurrUnit++ )
            {
                ITexture lTexture = this.mUnits[ lCurrUnit ].CurrentBindedTexture;
                if
                    ( lTexture.IsUsedBy( pPassIds ) == false )
                {
                    uint lBindingTime = this.mUnits[ lCurrUnit ].LastBindingTime;
                    if
                        ( lOldestUnit == -1 ||
                          lBindingTime < lOldestBindingTime )
                    {
                        lOldestUnit = lCurrUnit;
                        lOldestBindingTime = lBindingTime;
                    }
                }
            }

            return lOldestUnit;
        }

        /// <summary>
        /// Binds the given texture to the first available unit.
        /// </summary>
        /// <param name="pTexture">The texture to bind.</param>
        /// <returns>The texture unit the texture has been bound to. -1 if no available units.</returns>
        public int BindToAvailableUnit(ITexture pTexture)
        {
            if
                ( pTexture == null )
            {
                return -1;
            }

            bool lHasNoSamplerToUnit = false;
            Dictionary<uint, uint> lSamplerToUnitMap;
            if
                ( sTextureUnitMap.TryGetValue( pTexture.TextureId, out lSamplerToUnitMap ) )
            {
                if
                    ( lSamplerToUnitMap.Count == 0 )
                {
                    lHasNoSamplerToUnit = true;
                }
                else
                {
                    uint lUnitIdentifier = lSamplerToUnitMap.First().Value;
                    GL.ActiveTexture( (OpenTK.Graphics.OpenGL.TextureUnit)((uint)OpenTK.Graphics.OpenGL.TextureUnit.Texture0 + lUnitIdentifier) );
                    return (int)lUnitIdentifier;
                }
            }
            else
            {
                lHasNoSamplerToUnit = true;
                lSamplerToUnitMap  = new Dictionary<uint, uint>();
                sTextureUnitMap.Add( pTexture.TextureId, lSamplerToUnitMap );
            }

            if
                ( lHasNoSamplerToUnit )
            {
                int lUnitIdentifier = -1;
                IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                if
                    ( lRenderService != null &&
                      lRenderService.CurrentPass != null )
                {
                    lUnitIdentifier = this.FindAvailableTextureUnit( lRenderService.CurrentPass.PassIds );
                }
                else
                {
                    lUnitIdentifier = this.FindAvailableTextureUnit( new List<uint>() );
                }

                if
                    ( lUnitIdentifier != -1 )
                {
                    this.Bind( (uint)lUnitIdentifier, pTexture, null );
                }
                return lUnitIdentifier;
            }

            return -1;
        }

        /// <summary>
        /// Binds the given texture and the sampler to use with to the first available unit.
        /// </summary>
        /// <param name="pTexture">The texture to bind to the uint.</param>
        /// <param name="pSampler">The sampler to bind to the unit. May be null</param>
        /// <param name="pPassIds">The pass identifier(s) for which the texture must be bound.</param>
        /// <returns>The texture unit the texture has been bound to. -1 if no available units.</returns>
        public int BindToAvailableUnit(ITexture pTexture, ISampler pSampler, IEnumerable<uint> pPassIds)
        {
            if
                ( pTexture == null )
            {
                return -1;
            }

            uint lSamplerId = 0;
            if
                ( pSampler != null )
            {
                lSamplerId = pSampler.SamplerId;
            }

            int lUnitId = -1;
            bool lHasNoSamplerToUnit = false;
            Dictionary<uint, uint> lSamplerToUnitMap;
            if
                ( sTextureUnitMap.TryGetValue( pTexture.TextureId, out lSamplerToUnitMap ) )
            {
                uint lTemp;
                if
                    ( lSamplerToUnitMap.TryGetValue( lSamplerId, out lTemp ) )
                {
                    lUnitId = (int)lTemp;
                }
                else
                {
                    lHasNoSamplerToUnit = true;
                }
            }
            else
            {
                lHasNoSamplerToUnit = true;
                lSamplerToUnitMap   = new Dictionary<uint, uint>();
                sTextureUnitMap.Add( pTexture.TextureId, lSamplerToUnitMap );
            }

            if
                ( lHasNoSamplerToUnit )
            {
                lUnitId = this.FindAvailableTextureUnit( pPassIds );
            }

            this.Bind( (uint)lUnitId, pTexture, pSampler );

            return lUnitId;
        }

        /// <summary>
        /// Binds a sampler and a texture to a given unit.
        /// </summary>
        /// <param name="pUnitId">The unit identifier</param>
        /// <param name="pTexture">The texture to bind to the uint.</param>
        /// <param name="pSampler">The sampler to bind to the unit.</param>
        public void Bind(uint pUnitId, ITexture pTexture, ISampler pSampler)
        {
            uint lMaxUnits = GLHelpers.GetMaxTextureUnits();
            if
                ( pUnitId >= lMaxUnits )
            {
                LogManager.Instance.Log( string.Format( "Attempting to bind sampler and texture to an unavailable Unit {0}!!!", pUnitId ), LogType.ERROR );
                return;
            }

            this.mUnits[ (int)pUnitId ].Bind( pSampler, pTexture, this.mBindingCounter++ );
        }

        /// <summary>
        /// Unbinds a texture from that unit.
        /// </summary>
        /// <param name="pTexture">The texture to unbind.</param>
        public void Unbind(ITexture pTexture)
        {
            uint lMaxUnits = GLHelpers.GetMaxTextureUnits();
            for
                ( int lCurrUnit = 0; lCurrUnit < lMaxUnits; lCurrUnit++ )
            {
                if
                    ( this.mUnits[ lCurrUnit ].CurrentBindedTexture == pTexture )
                {
                    this.mUnits[ lCurrUnit ].Bind( null, null, this.mBindingCounter++ );
                }
            }
        }

        /// <summary>
        /// Unbinds a sampler from that unit.
        /// </summary>
        /// <param name="pSampler">The sampler to unbind.</param>
        public void Unbind(ISampler pSampler)
        {
            uint lMaxUnits = GLHelpers.GetMaxTextureUnits();
            for
                ( int lCurrUnit = 0; lCurrUnit < lMaxUnits; lCurrUnit++ )
            {
                if
                    ( this.mUnits[ lCurrUnit ].CurrentBindedSampler == pSampler )
                {
                    this.mUnits[ lCurrUnit ].Bind( null, null, this.mBindingCounter++ );
                }
            }
        }

        /// <summary>
        /// Unbinds all textures and samplers bound to unit(s)
        /// </summary>
        public void UnbindAll()
        {
            uint lMaxUnits = GLHelpers.GetMaxTextureUnits();
            for
                ( int lCurrUnit = 0; lCurrUnit < lMaxUnits; lCurrUnit++ )
            {
                this.mUnits[ lCurrUnit ].Bind( null, null, 0 );
            }

            this.mBindingCounter = 0;
        }

        #endregion Methods
    }
}
