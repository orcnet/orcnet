﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Graphics.Helpers;

namespace OrcNet.Graphics.Render.Textures
{
    /// <summary>
    /// Texture Rectangle class definition.
    /// </summary>
    public class TextureRectangle : ATexture
    {
        #region Fields

        /// <summary>
        /// Stores the texture width
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the texture height.
        /// </summary>
        private int mHeight;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the texture 1D width
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
        }

        /// <summary>
        /// Gets the texture height.
        /// </summary>
        public int Height
        {
            get
            {
                return this.mHeight;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextureRectangle"/> class.
        /// </summary>
        /// <param name="pWidth">The texture's width</param>
        /// <param name="pHeight">The texture's height.</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBufferParameter">Optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into that texture.</param>
        public TextureRectangle(int pWidth, int pHeight, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters pBufferParameter, IBuffer pPixels) : 
        base( TextureTarget.TextureRectangle )
        {
            this.Initialize( pWidth, pHeight, pGPUFormat, pFormat, pType, pParameters, pBufferParameter, pPixels );
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Replaces the corresponding mipmap content of that texture.
        /// </summary>
        /// <param name="pLevel">The mipmap level that must be changed.</param>
        /// <param name="pXOffset">The X offset of the part to be replaced.</param>
        /// <param name="pYOffset">The Y offset of the part to be replaced.</param>
        /// <param name="pWidth">The width of the part to be replaced.</param>
        /// <param name="pHeight">The height of the part to be replaced.</param>
        /// <param name="pFormat">The texture's components count</param>
        /// <param name="pType">The texture's components type.</param>
        /// <param name="pBufferParameter">The optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into the given mipmap.</param>
        public void SetMipmap(int pLevel, int pXOffset, int pYOffset, int pWidth, int pHeight, PixelFormat pFormat, PixelType pType, BufferLayoutParameters pBufferParameter, IBuffer pPixels)
        {
            this.BindToTextureUnit();

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );
            pBufferParameter.Apply();

            GL.TexSubImage2D( this.TextureType, pLevel, pXOffset, pYOffset, pWidth, pHeight, pFormat, pType, pPixels.GetData() );

            pBufferParameter.Clear();
            pPixels.Unbind( lUnpackBufferFlag );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Replaces the corresponding compressed mipmap content of that texture.
        /// </summary>
        /// <param name="pLevel">The mipmap level that must be changed.</param>
        /// <param name="pXOffset">The X offset of the part to be replaced.</param>
        /// <param name="pYOffset">The Y offset of the part to be replaced.</param>
        /// <param name="pWidth">The width of the part to be replaced.</param>
        /// <param name="pHeight">The height of the part to be replaced.</param>
        /// <param name="pSizeInBytes">The size of the new content in bytes.</param>
        /// <param name="pPixels">The pixels to be written into the given mipmap.</param>
        public void SetCompressedMipmap(int pLevel, int pXOffset, int pYOffset, int pWidth, int pHeight, int pSizeInBytes, IBuffer pPixels)
        {
            this.BindToTextureUnit();

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            pPixels.Bind( lUnpackBufferFlag );

            GL.CompressedTexSubImage2D( this.TextureType, pLevel, pXOffset, pYOffset, pWidth, pHeight, (PixelFormat)this.InternalFormat, pSizeInBytes, pPixels.GetData() );
            
            pPixels.Unbind( lUnpackBufferFlag );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Swaps that texture with the given one.
        /// </summary>
        /// <param name="pTexture">The texture to swap with.</param>
        public override void Swap(ITexture pTexture)
        {
            base.Swap( pTexture );

            TextureRectangle lOther = pTexture as TextureRectangle;
            Utilities.Swap( ref this.mWidth, ref lOther.mWidth );
            Utilities.Swap( ref this.mHeight, ref lOther.mHeight );
        }

        /// <summary>
        /// Initializes the texture
        /// </summary>
        /// <param name="pWidth">The texture width</param>
        /// <param name="pHeight">The texture's height</param>
        /// <param name="pGPUFormat">The texture data format on GPU.</param>
        /// <param name="pFormat">The texture components count.</param>
        /// <param name="pType">The components type</param>
        /// <param name="pParameters">The texture parameters.</param>
        /// <param name="pBufferParameter">Optional pixel storage parameters.</param>
        /// <param name="pPixels">The pixels to be written into that texture.</param>
        private void Initialize(int pWidth, int pHeight, PixelInternalFormat pGPUFormat, PixelFormat pFormat, PixelType pType, TextureParameters pParameters, BufferLayoutParameters pBufferParameter, IBuffer pPixels)
        {
            this.Initialize( pGPUFormat, pParameters );
            this.mWidth  = pWidth;
            this.mHeight = pHeight;

            int lUnpackBufferFlag = (int)All.PixelUnpackBuffer;
            bool lMustGenerateMipmaps = true;
            pPixels.Bind( lUnpackBufferFlag );
            if
                ( this.IsCompressed &&
                  pBufferParameter.CompressedSize > 0 )
            {
                GL.CompressedTexImage2D( this.TextureType, 0, this.InternalFormat, pWidth, pHeight, 0, pBufferParameter.CompressedSize, pPixels.GetData() );
            }
            else
            {
                pBufferParameter.Apply();
                GL.TexImage2D( this.TextureType, 0, this.InternalFormat, pWidth, pHeight, 0, pFormat, pType, pPixels.GetData() );
                pBufferParameter.Clear();

                int lSize = pBufferParameter.CompressedSize;
                int lPixelSize = (int)GLHelpers.GetFormatSize( pFormat, pType );
                if
                    ( lSize > this.mWidth * this.mHeight * lPixelSize )
                {
                    // Get the other levels from the same buffer.
                    int lOffset = this.mWidth * this.mHeight * lPixelSize;
                    int lLevel  = 0;
                    int lWidth = this.mWidth;
                    int lHeight = this.mHeight;
                    while 
                        ( lWidth % 2 == 0 && 
                          lHeight % 2 == 0 && 
                          lSize - lOffset >= (lWidth * lHeight / 4) * lPixelSize )
                    {
                        lLevel += 1;
                        lWidth  = lWidth / 2;
                        lHeight = lHeight / 2;
                        GL.TexImage2D( this.TextureType, lLevel, this.mInternalFormat, lWidth, lHeight, 0, pFormat, pType, pPixels.GetData( lOffset ) );
                        lOffset += lWidth * lHeight * lPixelSize;
                        lMustGenerateMipmaps = false;
                    }
                    this.mParameters.LODMax = OpenTK.MathHelper.Clamp( this.mParameters.LODMax, 0.0f, (float)lLevel );
                }
            }
            pPixels.Unbind( lUnpackBufferFlag );

            if
                ( lMustGenerateMipmaps )
            {
                this.GenerateMipmaps();
            }
            
            GLHelpers.GetError();
        }

        #endregion Methods
    }
}
