﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Graphics.Helpers;
using System;
using System.Drawing;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Blending description structure definition allowing to enable multi blending and
    /// activating blending or not for different draw buffers.
    /// </summary>
    public class BlendingDescription : AObservable, ICloneable
    {
        #region Fields
        
        /// <summary>
        /// Stores the flag indicating whether blending is allowed for multiple draw buffers or not.
        /// </summary>
        private bool mUseMultiBlending;

        /// <summary>
        /// Stores the flag indicating whether different blending equations must be used or not if multiple draw buffers.
        /// </summary>
        private bool mUseMultipleEquations;

        /// <summary>
        /// Stores the color used in the blend equations if using ConstantColor as blending factor.
        /// </summary>
        private Color mConstantColor;

        /// <summary>
        /// Stores the set of flags indicating which one(s) of the draw buffers must do blending.
        /// </summary>
        private bool[] mEnabledBlending;

        /// <summary>
        /// Stores the color channels blending equation.
        /// </summary>
        private BlendEquationMode[]  mRGBEquation;

        /// <summary>
        /// Stores the source color that must be blended.
        /// </summary>
        private BlendingFactorSrc[]  mRGBSource;

        /// <summary>
        /// Stores the destination color that will be blended with the source color.
        /// </summary>
        private BlendingFactorDest[] mRGBDestination;

        /// <summary>
        /// Stores the alpha channel blending equation.
        /// </summary>
        private BlendEquationMode[]  mAlphaEquation;

        /// <summary>
        /// Stores the source alpha that must be blended.
        /// </summary>
        private BlendingFactorSrc[]  mAlphaSource;

        /// <summary>
        /// Stores the destination alpha that will be blended with the source alpha.
        /// </summary>
        private BlendingFactorDest[] mAlphaDestination;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether blending is allowed for multiple draw buffers or not.
        /// </summary>
        public bool UseMultiBlending
        {
            get
            {
                return this.mUseMultiBlending;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether different blending equations must be used or not if multiple draw buffers.
        /// </summary>
        public bool UseMultipleEquations
        {
            get
            {
                return this.mUseMultipleEquations;
            }
        }

        /// <summary>
        /// Gets or sets the color used in the blend equations if using ConstantColor as blending factor.
        /// </summary>
        public Color ConstantColor
        {
            get
            {
                return this.mConstantColor;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mConstantColor, value, "ConstantColor" );
            }
        }

        /// <summary>
        /// Gets the draw buffer blending state.
        /// </summary>
        /// <param name="pIndex">The buffer index</param>
        /// <returns>The flag indicating whether the draw buffer uses blending or not.</returns>
        public bool this[BlendingStateIndex pIndex]
        {
            get
            {
                return this.mEnabledBlending[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the draw buffer RGB blending equation.
        /// </summary>
        /// <param name="pIndex">The buffer index</param>
        /// <returns>The RGB blending equation.</returns>
        public BlendEquationMode this[BlendingRGBEquationIndex pIndex]
        {
            get
            {
                return this.mRGBEquation[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the draw buffer RGB blending source factor.
        /// </summary>
        /// <param name="pIndex">The buffer index</param>
        /// <returns>The RGB blending source factor.</returns>
        public BlendingFactorSrc this[BlendingRGBSourceFactorIndex pIndex]
        {
            get
            {
                return this.mRGBSource[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the draw buffer RGB blending destination factor.
        /// </summary>
        /// <param name="pIndex">The buffer index</param>
        /// <returns>The RGB blending destination factor.</returns>
        public BlendingFactorDest this[BlendingRGBDestinationFactorIndex pIndex]
        {
            get
            {
                return this.mRGBDestination[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the draw buffer Alpha blending equation.
        /// </summary>
        /// <param name="pIndex">The buffer index</param>
        /// <returns>The Alpha blending equation.</returns>
        public BlendEquationMode this[BlendingAlphaEquationIndex pIndex]
        {
            get
            {
                return this.mAlphaEquation[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the draw buffer Alpha blending source factor.
        /// </summary>
        /// <param name="pIndex">The buffer index</param>
        /// <returns>The Alpha blending source factor.</returns>
        public BlendingFactorSrc this[BlendingAlphaSourceFactorIndex pIndex]
        {
            get
            {
                return this.mAlphaSource[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the draw buffer Alpha blending destination factor.
        /// </summary>
        /// <param name="pIndex">The buffer index</param>
        /// <returns>The Alpha blending destination factor.</returns>
        public BlendingFactorDest this[BlendingAlphaDestinationFactorIndex pIndex]
        {
            get
            {
                return this.mAlphaDestination[ pIndex ];
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BlendingDescription"/> class.
        /// </summary>
        /// <param name="pUseMultiBlending">the flag indicating whether blending is allowed for multiple draw buffers or not.</param>
        /// <param name="pUseMultipleEquations">the flag indicating whether different blending equations must be used or not if multiple draw buffers.</param>
        /// <param name="pMaxDrawBufferCount">The maximum draw buffer count to initialize the internal caches.</param>
        public BlendingDescription(bool pUseMultiBlending = false, bool pUseMultipleEquations = false, int pMaxDrawBufferCount = 4)
        {
            this.mUseMultiBlending = pUseMultiBlending;
            this.mUseMultipleEquations = pUseMultipleEquations;
            this.mConstantColor = Color.FromArgb( 0, 0, 0, 0 );

            this.mEnabledBlending = new bool[ pMaxDrawBufferCount ];
            this.mRGBEquation = new BlendEquationMode[ pMaxDrawBufferCount ];
            this.mRGBSource = new BlendingFactorSrc[ pMaxDrawBufferCount ];
            this.mRGBDestination = new BlendingFactorDest[ pMaxDrawBufferCount ];
            this.mAlphaEquation = new BlendEquationMode[ pMaxDrawBufferCount ];
            this.mAlphaSource = new BlendingFactorSrc[ pMaxDrawBufferCount ];
            this.mAlphaDestination = new BlendingFactorDest[ pMaxDrawBufferCount ];

            for
                ( int lCurr = 0; lCurr < pMaxDrawBufferCount; lCurr++ )
            {
                this.mEnabledBlending[ lCurr ] = false;
                this.mRGBEquation[ lCurr ] = BlendEquationMode.FuncAdd;
                this.mRGBSource[ lCurr ] = BlendingFactorSrc.One;
                this.mRGBDestination[ lCurr ] = BlendingFactorDest.Zero;
                this.mAlphaEquation[ lCurr ] = BlendEquationMode.FuncAdd;
                this.mAlphaSource[ lCurr ] = BlendingFactorSrc.One;
                this.mAlphaDestination[ lCurr ] = BlendingFactorDest.Zero;
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Enables or disables the blending for the single draw buffer.
        /// </summary>
        /// <param name="pEnable">The flag indicating whether to use blending or not.</param>
        public void Blend(bool pEnable)
        {
            this.mUseMultiBlending = false;
            this.mEnabledBlending[ 0 ] = pEnable;

            this.InformPropertyChangedListener( this.mUseMultiBlending, this.mUseMultiBlending, "UseMultiBlending" );
        }

        /// <summary>
        /// Enables or disables the blending for the single draw buffer.
        /// </summary>
        /// <param name="pEnable">The flag indicating whether to use blending or not.</param>
        /// <param name="pEquation">The RBG and Alpha channels blending equation</param>
        /// <param name="pSourceFactor">The RBG and Alpha channels source factor of the blending equation.</param>
        /// <param name="pDestinationFactor">The RBG and Alpha channels destination factor of the blending equation.</param>
        public void Blend(bool pEnable, BlendEquationMode pEquation, BlendingFactorSrc pSourceFactor, BlendingFactorDest pDestinationFactor)
        {
            this.mUseMultiBlending = false;
            this.mUseMultipleEquations = false;
            this.mEnabledBlending[ 0 ] = pEnable;
            this.mRGBEquation[ 0 ] = pEquation;
            this.mAlphaEquation[ 0 ] = pEquation;
            this.mRGBSource[ 0 ] = pSourceFactor;
            this.mAlphaSource[ 0 ] = pSourceFactor;
            this.mRGBDestination[ 0 ] = pDestinationFactor;
            this.mAlphaDestination[ 0 ] = pDestinationFactor;

            this.InformPropertyChangedListener( this.mUseMultiBlending, this.mUseMultiBlending, "UseMultiBlending" );
        }

        /// <summary>
        /// Enables or disables the blending for the single draw buffer.
        /// </summary>
        /// <param name="pEnable">The flag indicating whether to use blending or not.</param>
        /// <param name="pRGBEquation">The RBG and Alpha channels blending equation</param>
        /// <param name="pRGBSourceFactor">The RBG and Alpha channels source factor of the blending equation.</param>
        /// <param name="pRGBDestinationFactor">The RBG and Alpha channels destination factor of the blending equation.</param>
        /// <param name="pAlphaEquation">The RBG and Alpha channels blending equation</param>
        /// <param name="pAlphaSourceFactor">The RBG and Alpha channels source factor of the blending equation.</param>
        /// <param name="pAlphaDestinationFactor">The RBG and Alpha channels destination factor of the blending equation.</param>
        public void Blend(bool pEnable, BlendEquationMode pRGBEquation, BlendingFactorSrc pRGBSourceFactor, BlendingFactorDest pRGBDestinationFactor,
                                        BlendEquationMode pAlphaEquation, BlendingFactorSrc pAlphaSourceFactor, BlendingFactorDest pAlphaDestinationFactor)
        {
            this.mUseMultiBlending = false;
            this.mUseMultipleEquations = false;
            this.mEnabledBlending[ 0 ] = pEnable;
            this.mRGBEquation[ 0 ] = pRGBEquation;
            this.mAlphaEquation[ 0 ] = pAlphaEquation;
            this.mRGBSource[ 0 ] = pRGBSourceFactor;
            this.mAlphaSource[ 0 ] = pAlphaSourceFactor;
            this.mRGBDestination[ 0 ] = pRGBDestinationFactor;
            this.mAlphaDestination[ 0 ] = pAlphaDestinationFactor;

            this.InformPropertyChangedListener( this.mUseMultiBlending, this.mUseMultiBlending, "UseMultiBlending" );
        }

        /// <summary>
        /// Enables or disables the blending for a given draw buffer.
        /// </summary>
        /// <param name="pBufferId">The buffer Id</param>
        /// <param name="pEnable">The flag indicating whether to use blending or not.</param>
        public void Blend(BufferId pBufferId, bool pEnable)
        {
            this.mUseMultiBlending = true;
            this.mEnabledBlending[ GLHelpers.GetBufferIndex( pBufferId ) ] = pEnable;
            
            this.InformPropertyChangedListener( this.mUseMultiBlending, this.mUseMultiBlending, "UseMultiBlending" );
        }

        /// <summary>
        /// Enables or disables the blending for a given draw buffer.
        /// </summary>
        /// <param name="pBufferId">The buffer Id</param>
        /// <param name="pEnable">The flag indicating whether to use blending or not.</param>
        /// <param name="pEquation">The RBG and Alpha channels blending equation</param>
        /// <param name="pSourceFactor">The RBG and Alpha channels source factor of the blending equation.</param>
        /// <param name="pDestinationFactor">The RBG and Alpha channels destination factor of the blending equation.</param>
        public void Blend(BufferId pBufferId, bool pEnable, BlendEquationMode pEquation, BlendingFactorSrc pSourceFactor, BlendingFactorDest pDestinationFactor)
        {
            int lIndex = GLHelpers.GetBufferIndex( pBufferId );
            this.mUseMultiBlending = false;
            this.mUseMultipleEquations = false;
            this.mEnabledBlending[ lIndex ] = pEnable;
            this.mRGBEquation[ lIndex ] = pEquation;
            this.mAlphaEquation[ lIndex ] = pEquation;
            this.mRGBSource[ lIndex ] = pSourceFactor;
            this.mAlphaSource[ lIndex ] = pSourceFactor;
            this.mRGBDestination[ lIndex ] = pDestinationFactor;
            this.mAlphaDestination[ lIndex ] = pDestinationFactor;

            this.InformPropertyChangedListener( this.mUseMultiBlending, this.mUseMultiBlending, "UseMultiBlending" );
        }

        /// <summary>
        /// Enables or disables the blending for a given draw buffer.
        /// </summary>
        /// <param name="pBufferId">The buffer Id</param>
        /// <param name="pEnable">The flag indicating whether to use blending or not.</param>
        /// <param name="pRGBEquation">The RBG and Alpha channels blending equation</param>
        /// <param name="pRGBSourceFactor">The RBG and Alpha channels source factor of the blending equation.</param>
        /// <param name="pRGBDestinationFactor">The RBG and Alpha channels destination factor of the blending equation.</param>
        /// <param name="pAlphaEquation">The RBG and Alpha channels blending equation</param>
        /// <param name="pAlphaSourceFactor">The RBG and Alpha channels source factor of the blending equation.</param>
        /// <param name="pAlphaDestinationFactor">The RBG and Alpha channels destination factor of the blending equation.</param>
        public void Blend(BufferId pBufferId, bool pEnable, BlendEquationMode pRGBEquation, BlendingFactorSrc pRGBSourceFactor, BlendingFactorDest pRGBDestinationFactor,
                                                            BlendEquationMode pAlphaEquation, BlendingFactorSrc pAlphaSourceFactor, BlendingFactorDest pAlphaDestinationFactor)
        {
            int lIndex = GLHelpers.GetBufferIndex( pBufferId );
            this.mUseMultiBlending = true;
            this.mUseMultipleEquations = true;
            this.mEnabledBlending[ lIndex ] = pEnable;
            this.mRGBEquation[ lIndex ] = pRGBEquation;
            this.mAlphaEquation[ lIndex ] = pAlphaEquation;
            this.mRGBSource[ lIndex ] = pRGBSourceFactor;
            this.mAlphaSource[ lIndex ] = pAlphaSourceFactor;
            this.mRGBDestination[ lIndex ] = pRGBDestinationFactor;
            this.mAlphaDestination[ lIndex ] = pAlphaDestinationFactor;

            this.InformPropertyChangedListener( this.mUseMultiBlending, this.mUseMultiBlending, "UseMultiBlending" );
        }

        #region Methods ICloneable

        /// <summary>
        /// Clone the description.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            BlendingDescription lClone = new BlendingDescription( this.mUseMultiBlending, this.mUseMultipleEquations );
            lClone.mConstantColor = this.mConstantColor;

            for
                ( int lCurrDrawBuffer = 0; lCurrDrawBuffer < this.mEnabledBlending.Length; lCurrDrawBuffer++ )
            {
                lClone.mEnabledBlending[ lCurrDrawBuffer ] = this.mEnabledBlending[ lCurrDrawBuffer ];
                lClone.mRGBEquation[ lCurrDrawBuffer ] = this.mRGBEquation[ lCurrDrawBuffer ];
                lClone.mAlphaEquation[ lCurrDrawBuffer ] = this.mAlphaEquation[ lCurrDrawBuffer ];
                lClone.mRGBSource[ lCurrDrawBuffer ] = this.mRGBSource[ lCurrDrawBuffer ];
                lClone.mAlphaSource[ lCurrDrawBuffer ] = this.mAlphaSource[ lCurrDrawBuffer ];
                lClone.mRGBDestination[ lCurrDrawBuffer ] = this.mRGBDestination[ lCurrDrawBuffer ];
                lClone.mAlphaDestination[ lCurrDrawBuffer ] = this.mAlphaDestination[ lCurrDrawBuffer ];
            }

            return lClone;
        }

        #endregion Methods ICloneable

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Helper structure to access by index one of the owned blending RGB equation.
        /// </summary>
        public struct BlendingRGBEquationIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="BlendingRGBEquationIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The RGB equation index.</param>
            public BlendingRGBEquationIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from BlendingRGBEquationIndex to int
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(BlendingRGBEquationIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned blending RGB source factor.
        /// </summary>
        public struct BlendingRGBSourceFactorIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="BlendingRGBSourceFactorIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The RGB source factor index.</param>
            public BlendingRGBSourceFactorIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from BlendingRGBSourceFactorIndex to int
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(BlendingRGBSourceFactorIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned blending RGB destination factor.
        /// </summary>
        public struct BlendingRGBDestinationFactorIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="BlendingRGBDestinationFactorIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The RGB destination factor index.</param>
            public BlendingRGBDestinationFactorIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from BlendingRGBDestinationFactorIndex to int
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(BlendingRGBDestinationFactorIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned blending Alpha equation.
        /// </summary>
        public struct BlendingAlphaEquationIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="BlendingAlphaEquationIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The Alpha equation index.</param>
            public BlendingAlphaEquationIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from BlendingAlphaEquationIndex to int
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(BlendingAlphaEquationIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned blending Alpha source factor.
        /// </summary>
        public struct BlendingAlphaSourceFactorIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="BlendingAlphaSourceFactorIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The ALpha source factor index.</param>
            public BlendingAlphaSourceFactorIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from BlendingAlphaSourceFactorIndex to int
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(BlendingAlphaSourceFactorIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned blending Alpha destination factor.
        /// </summary>
        public struct BlendingAlphaDestinationFactorIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="BlendingAlphaDestinationFactorIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The Alpha destination factor index.</param>
            public BlendingAlphaDestinationFactorIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from BlendingAlphaDestinationFactorIndex to int
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(BlendingAlphaDestinationFactorIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned blending state
        /// </summary>
        public struct BlendingStateIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="BlendingStateIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The blending state index.</param>
            public BlendingStateIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from BlendingStateIndex to int.
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(BlendingStateIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        #endregion Inner classes
    }
}
