﻿using OpenTK.Graphics.OpenGL;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Enumerates all vertex attribute component type
    /// </summary>
    public enum AttributeType
    {
        /// <summary>
        /// Signed 8 bits based attribute.
        /// </summary>
        SBYTE_BASED = 0,

        /// <summary>
        /// Unsigned 8 bits based attribute.
        /// </summary>
        BYTE_BASED,

        /// <summary>
        /// Signed 16 bits based attribute.
        /// </summary>
        INT16_BASED,

        /// <summary>
        /// Unsigned 16 bits based attribute.
        /// </summary>
        UINT16_BASED,

        /// <summary>
        /// Signed 32 bits based attribute.
        /// </summary>
        INT_BASED,

        /// <summary>
        /// Unsigned 32 bits based attribute.
        /// </summary>
        UINT_BASED,

        /// <summary>
        /// Signed 16 bits single precision floating point based attribute.
        /// </summary>
        HALF_BASED,

        /// <summary>
        /// Signed 32 bits single precision floating point based attribute.
        /// </summary>
        FLOAT_BASED,

        /// <summary>
        /// Signed 64 bits double precision floating point based attribute.
        /// </summary>
        DOUBLE_BASED,

        /// <summary>
        /// Signed 32 bits integer based packed data type of the form 2 bits(W) 10 bits(Z) 10 bits(Y) 10 bits(X)
        /// </summary>
        INT_PACKED_BASED,

        /// <summary>
        /// Unsigned 32 bits integer based packed data type of the form 2 bits(W) 10 bits(Z) 10 bits(Y) 10 bits(X)
        /// </summary>
        UINT_PACKED_BASED,

        /// <summary>
        /// This extension provides the capability, for platforms that do
        /// not have efficient floating-point support, to input data in a
        /// fixed-point format, i.e., a scaled-integer format.
        /// </summary>
        FIXED,

        /// <summary>
        /// The amount of attribute types.
        /// </summary>
        COUNT
    }

    /// <summary>
    /// Attribute type enumeration extensions.
    /// </summary>
    public static class AttributeTypeExtensions
    {
        #region Methods

        /// <summary>
        /// Turns a Orc attribute type enumeration into a GL attribute type.
        /// </summary>
        /// <param name="pType"></param>
        /// <returns></returns>
        public static All ToGLAttributeType(this AttributeType pType)
        {
            switch 
                ( pType )
            {
                case AttributeType.BYTE_BASED:
                    return All.UnsignedByte;
                case AttributeType.INT16_BASED:
                    return All.Short;
                case AttributeType.UINT16_BASED:
                    return All.UnsignedShort;
                case AttributeType.INT_BASED:
                    return All.Int;
                case AttributeType.UINT_BASED:
                    return All.UnsignedInt;
                case AttributeType.HALF_BASED:
                    return All.HalfFloat;
                case AttributeType.FLOAT_BASED:
                    return All.Float;
                case AttributeType.DOUBLE_BASED:
                    return All.Double;
                case AttributeType.INT_PACKED_BASED:
                    return All.Int2101010Rev;
                case AttributeType.UINT_PACKED_BASED:
                    return All.UnsignedInt2101010Rev;
                case AttributeType.FIXED:
                    return All.Fixed;
            }

            return All.Byte;
        }

        #endregion Methods
    }
}
