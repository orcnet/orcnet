﻿namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Enumerates the different frame buffer states.
    /// </summary>
    public enum FrameBufferState : uint
    {
        /// <summary>
        /// The frame bugger is up to date.
        /// </summary>
        UpToDate = 0,

        /// <summary>
        /// The frame buffer has some parameters changed.
        /// </summary>
        ParametersChanged,

        /// <summary>
        /// The frame buffer has some attachments changed.
        /// </summary>
        AttachmentsChanged,

        /// <summary>
        /// The frame buffer has the read or some draw buffers changed.
        /// </summary>
        ReadDrawBuffersChanged,
    }
}
