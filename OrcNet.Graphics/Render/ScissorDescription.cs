﻿using OrcNet.Core;
using System;
using System.Collections.Generic;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Scissor description structure definition.
    /// </summary>
    public class ScissorDescription : AObservable, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the viewport(s) scissor(s) for tests.
        /// </summary>
        private Scissor[] mScissors;
        
        /// <summary>
        /// Stores the flag indicating whether only fragments inside the scissor are not discarded or not.
        /// </summary>
        private bool[] mEnabledScissors;

        /// <summary>
        /// Stores the flag indicating whether multiple scissor are used for multiple viewports.
        /// </summary>
        private bool mUseMultiScissor;

        #endregion Fields

        #region Properties
        
        /// <summary>
        /// Gets the viewport(s) scissor(s) for tests.
        /// </summary>
        public IEnumerable<Scissor> Scissors
        {
            get
            {
                return this.mScissors;
            }
        }

        /// <summary>
        /// Gets the viewport's scissor state a the given index.
        /// </summary>
        /// <param name="pIndex">The scissor's index</param>
        /// <returns>The flag indicating whether the scissor tests are enabled or not for a viewport</returns>
        public bool this[ScissorStateIndex pIndex]
        {
            get
            {
                return this.mEnabledScissors[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the flag indicating whether only fragments inside the scissor are not discarded or not.
        /// </summary>
        public Scissor this[ScissorIndex pIndex]
        {
            get
            {
                return this.mScissors[ pIndex ];
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether multiple scissor are used for multiple viewports.
        /// </summary>
        public bool UseMultiScissor
        {
            get
            {
                return this.mUseMultiScissor;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScissorDescription"/> class.
        /// </summary>
        /// <param name="pUseMultiScissor">the flag indicating whether multiple scissor are used for multiple viewports.</param>
        /// <param name="pScissors">the viewport(s) scissor(s) for tests.</param>
        /// <param name="pEnabledScissor">the flag indicating whether only fragments inside the scissor are not discarded or not.</param>
        public ScissorDescription(bool pUseMultiScissor = false, Scissor[] pScissors = null, bool[] pEnabledScissor = null)
        {
            this.mUseMultiScissor = pUseMultiScissor;
            this.mScissors = pScissors;
            this.mEnabledScissors = pEnabledScissor;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Enables or disables a single viewport scissor test.
        /// </summary>
        /// <param name="pEnable">The flag indicating whether the test will be enable for a single viewport or not.</param>
        public void ScissorTest(bool pEnable)
        {
            this.mUseMultiScissor = false;
            this.mEnabledScissors[ 0 ] = pEnable;

            this.InformPropertyChangedListener( this.mEnabledScissors, this.mEnabledScissors, "EnabledScissors" );
        }

        /// <summary>
        /// Enables or disables a single viewport scissor test.
        /// </summary>
        /// <param name="pEnable">The flag indicating whether the test will be enable for a single viewport or not.</param>
        /// <param name="pSurrogate">The scissor to set as substitute of the previous single scissor.</param>
        public void ScissorTest(bool pEnable, Scissor pSurrogate)
        {
            this.mUseMultiScissor = false;
            this.mEnabledScissors[ 0 ] = pEnable;
            this.mScissors[ 0 ] = pSurrogate;

            this.InformPropertyChangedListener( this.mEnabledScissors, this.mEnabledScissors, "EnabledScissors" );
        }

        /// <summary>
        /// Enables or disables a multi viewports scissor test.
        /// </summary>
        /// <param name="pIndex">The index of the viewport to activate the scissor test for.</param>
        /// <param name="pEnable">The flag indicating whether the test will be enable for that viewport or not.</param>
        public void ScissorTest(int pIndex, bool pEnable)
        {
            if
                ( this.mUseMultiScissor == false )
            {
                // The first exists by default, but other in the multi case induces 
                // to use extra array slots which are Null by default.
                Scissor lFirst = this.mScissors[ 0 ];
                bool lFirstState = this.mEnabledScissors[ 0 ];
                for
                    ( int lCurr = 1; lCurr < this.mScissors.Length; lCurr++ )
                {
                    this.mScissors[ lCurr ] = new Scissor( lFirst.Left, lFirst.Bottom, lFirst.Width, lFirst.Height );
                    this.mEnabledScissors[ lCurr ] = lFirstState;
                }

                this.mUseMultiScissor = true;
            }

            this.mEnabledScissors[ pIndex ] = pEnable;

            this.InformPropertyChangedListener( this.mEnabledScissors, this.mEnabledScissors, "EnabledScissors" );
        }

        /// <summary>
        /// Enables or disables a multi viewports scissor test.
        /// </summary>
        /// <param name="pIndex">The index of the viewport to activate the scissor test for.</param>
        /// <param name="pEnable">The flag indicating whether the test will be enable for that viewport or not.</param>
        /// <param name="pSurrogate">The scissor to set as substitute of the previous single scissor.</param>
        public void ScissorTest(int pIndex, bool pEnable, Scissor pSurrogate)
        {
            if
                ( this.mUseMultiScissor == false )
            {
                // The first exists by default, but other in the multi case induces 
                // to use extra array slots which are Null by default.
                Scissor lFirst = this.mScissors[ 0 ];
                bool lFirstState = this.mEnabledScissors[ 0 ];
                for
                    ( int lCurr = 1; lCurr < this.mScissors.Length; lCurr++ )
                {
                    this.mScissors[ lCurr ] = new Scissor( lFirst.Left, lFirst.Bottom, lFirst.Width, lFirst.Height );
                    this.mEnabledScissors[ lCurr ] = lFirstState;
                }

                this.mUseMultiScissor = true;
            }

            this.mEnabledScissors[ pIndex ] = pEnable;
            this.mScissors[ pIndex ] = pSurrogate;

            this.InformPropertyChangedListener( this.mEnabledScissors, this.mEnabledScissors, "EnabledScissors" );
        }

        #region Methods AObservable

        /// <summary>
        /// Delegate called on dispose.
        /// </summary>
        protected override void OnDispose()
        {
            this.mScissors = null;
            this.mEnabledScissors = null;

            base.OnDispose();
        }

        #endregion Methods AObservable

        #region Methods ICloneable

        /// <summary>
        /// Clone the description.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            Scissor[] lScissors = new Scissor[ this.mScissors.Length ];
            for
                ( int lScissor = 0; lScissor < this.mScissors.Length; lScissor++ )
            {
                Scissor lCurrent = this.mScissors[ lScissor ];
                if
                    ( lCurrent != null )
                {
                    lScissors[ lScissor ] = lCurrent.Clone() as Scissor;
                }
            }

            bool[] lEnabledScissors = new bool[ this.mEnabledScissors.Length ];
            for
                ( int lScissorState = 0; lScissorState < this.mEnabledScissors.Length; lScissorState++ )
            {
                lEnabledScissors[ lScissorState ] = this.mEnabledScissors[ lScissorState ];
            }

            ScissorDescription lClone = new ScissorDescription( this.mUseMultiScissor, lScissors, lEnabledScissors );
            return lClone;
        }

        #endregion Methods ICloneable

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Helper structure to access by index one of the owned scissor.
        /// </summary>
        public struct ScissorIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="ScissorIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The scissor index.</param>
            public ScissorIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from int to ScissorStateIndex.
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(ScissorIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        /// <summary>
        /// Helper structure to access by index one of the owned scissor state
        /// </summary>
        public struct ScissorStateIndex
        {
            #region Fields

            /// <summary>
            /// Stores the array index
            /// </summary>
            private int mIndex;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="ScissorStateIndex"/> class.
            /// </summary>
            /// <param name="pIndex">The scissor state index.</param>
            public ScissorStateIndex(int pIndex)
            {
                this.mIndex = pIndex;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// implicit cast from int to ScissorStateIndex.
            /// </summary>
            /// <param name="pIndex">The state index</param>
            public static implicit operator int(ScissorStateIndex pIndex)
            {
                return pIndex.mIndex;
            }

            #endregion Methods
        }

        #endregion Inner classes
    }
}
