﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using System;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Depth buffer test description structure definition.
    /// </summary>
    public class DepthDescription : AObservable, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether depth test must be performed or not to discard fragment
        /// based on the comparison test function.
        /// </summary>
        private bool mUseDepth;

        /// <summary>
        /// Stores the depth comparison test function to determine whether fragments must be discarded or not.
        /// </summary>
        private DepthFunction mDepthFunction;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether depth test must be performed or not to discard fragment
        /// based on the comparison test function.
        /// </summary>
        public bool UseDepth
        {
            get
            {
                return this.mUseDepth;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mUseDepth, value, "UseDepth" );
            }
        }

        /// <summary>
        /// Gets the depth comparison test function to determine whether fragments must be discarded or not.
        /// </summary>
        public DepthFunction DepthFunction
        {
            get
            {
                return this.mDepthFunction;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DepthDescription"/> class.
        /// </summary>
        /// <param name="pUseDepth">the flag indicating whether depth test must be performed or not to discard fragment based on the comparison test function.</param>
        /// <param name="pDepthFunction">the depth comparison test function to determine whether fragments must be discarded or not.</param>
        public DepthDescription(bool pUseDepth = false, DepthFunction pDepthFunction = DepthFunction.Less)
        {
            this.mUseDepth = pUseDepth;
            this.mDepthFunction = pDepthFunction;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Enables or disables depth tests.
        /// </summary>
        /// <param name="pEnable">the flag indicating whether depth test must be performed or not to discard fragment based on the comparison test function.</param>
        /// <param name="pFunction">the depth comparison test function to determine whether fragments must be discarded or not.</param>
        public void DepthTest(bool pEnable, DepthFunction pFunction)
        {
            this.mUseDepth = pEnable;
            this.mDepthFunction = pFunction;

            this.InformPropertyChangedListener( this.mUseDepth, this.mUseDepth, "UseDepth" );
        }

        #region Methods

        /// <summary>
        /// Clone the description.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            DepthDescription lClone = new DepthDescription( this.mUseDepth, this.mDepthFunction );
            return lClone;
        }

        #endregion Methods

        #endregion Methods
    }
}
