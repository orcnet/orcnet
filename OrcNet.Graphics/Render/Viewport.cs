﻿using System;
using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Viewport structure definition.
    /// </summary>
    [DebuggerDisplay("Viewport : Left = {Left}, Bottom = {Bottom}, Width = {Width}, Height = {Height}")]
    public class Viewport : ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the left X screen position.
        /// </summary>
        private float mLeft;

        /// <summary>
        /// Stores the bottom Y screen position.
        /// </summary>
        private float mBottom;

        /// <summary>
        /// Stores the viewport width from pLeft.
        /// </summary>
        private float mWidth;

        /// <summary>
        /// Stores the viewport height from pBottom.
        /// </summary>
        private float mHeight;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the left X screen position.
        /// </summary>
        public float Left
        {
            get
            {
                return this.mLeft;
            }
        }

        /// <summary>
        /// Gets the bottom Y screen position.
        /// </summary>
        public float Bottom
        {
            get
            {
                return this.mBottom;
            }
        }

        /// <summary>
        /// Gets the viewport width from pLeft.
        /// </summary>
        public float Width
        {
            get
            {
                return this.mWidth;
            }
        }

        /// <summary>
        /// Gets the viewport height from pBottom.
        /// </summary>
        public float Height
        {
            get
            {
                return this.mHeight;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Viewport"/> class.
        /// </summary>
        /// <param name="pLeft">The left X screen position</param>
        /// <param name="pBottom">The bottom Y screen position</param>
        /// <param name="pWidth">The viewport width from pLeft</param>
        /// <param name="pHeight">The viewport height from pBottom</param>
        public Viewport(float pLeft, float pBottom, float pWidth, float pHeight)
        {
            this.mLeft   = pLeft;
            this.mBottom = pBottom;
            this.mWidth  = pWidth;
            this.mHeight = pHeight;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Clone the viewport
        /// </summary>
        /// <returns>The clone</returns>
        public object Clone()
        {
            Viewport lClone = new Viewport( this.mLeft, this.mBottom, this.mWidth, this.mHeight );
            return lClone;
        }

        #endregion Methods
    }
}
