﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Helpers;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Render buffer for offscreen rendering.
    /// </summary>
    [DebuggerDisplay("RenderBuffer : Id = {BufferId}")]
    public class RenderBuffer : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the render buffer Id.
        /// </summary>
        private uint mBufferId;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the render buffer Id.
        /// </summary>
        public uint BufferId
        {
            get
            {
                return this.mBufferId;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderBuffer"/> class.
        /// </summary>
        /// <param name="pFormat">The internal render buffer format.</param>
        /// <param name="pWidth">The render buffer width</param>
        /// <param name="pHeight">The render buffer height</param>
        /// <param name="pSampleCount">The number of sample per pixel if multisampling is required.</param>
        public RenderBuffer(RenderbufferStorage pFormat, int pWidth, int pHeight, int pSampleCount = 0)
        {
            GL.GenRenderbuffers( 1, out this.mBufferId );
            GL.BindRenderbuffer( RenderbufferTarget.Renderbuffer, this.mBufferId );
            if
                ( pSampleCount > 0 )
            {
                GL.RenderbufferStorageMultisample( RenderbufferTarget.Renderbuffer, pSampleCount, pFormat, pWidth, pHeight );
            }
            else
            {
                GL.RenderbufferStorage( RenderbufferTarget.Renderbuffer, pFormat, pWidth, pHeight );
            }
            GLHelpers.GetError();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                // Releases the render buffer.
                GL.DeleteRenderbuffers( 1, ref this.mBufferId );
                GLHelpers.GetError();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods
    }
}
