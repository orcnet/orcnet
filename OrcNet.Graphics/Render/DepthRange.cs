﻿using System;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Depth range structure definition defining the near and far planes values.
    /// </summary>
    public class DepthRange : ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the far plane value.
        /// </summary>
        private double mFar;
        
        /// <summary>
        /// Stores the near plane value.
        /// </summary>
        private double mNear;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the far plane value.
        /// </summary>
        public double Far
        {
            get
            {
                return this.mFar;
            }
        }

        /// <summary>
        /// Gets the near plane value.
        /// </summary>
        public double Near
        {
            get
            {
                return this.mNear;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DepthRange"/> class.
        /// </summary>
        /// <param name="pNear">the near plane value.</param>
        /// <param name="pFar">the far plane value.</param>
        public DepthRange(double pNear, double pFar)
        {
            this.mFar  = pFar;
            this.mNear = pNear;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Clone the depth range
        /// </summary>
        /// <returns>The clone</returns>
        public object Clone()
        {
            DepthRange lClone = new DepthRange( this.mNear, this.mFar );
            return lClone;
        }

        #endregion Methods
    }
}
