﻿using OrcNet.Core;
using System;

namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Sampling description structure definition.
    /// </summary>
    public class SamplingDescription : AObservable, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether or not to use multiple fragment samples
        /// to compute the final color in the fragment stage.
        /// </summary>
        private bool mUseMultiSampling;

        /// <summary>
        /// Stores the flag indicating whether or not to sample alpha values to compute an
        /// coverage value that will be ANDed with the fragment coverage one.
        /// see mFragmentCoverage.
        /// </summary>
        private bool mSampleAlphaToCoverage;

        /// <summary>
        /// Stores the flag indicating whether each sample alpha value must be forced
        /// to the maximum value, that is, 1 meaning fully visible.
        /// </summary>
        private bool mForceOpacity;

        /// <summary>
        /// Stores the flag indicating whether or not to explicitly request that an implementation
        /// uses a minimum number of unique set of fragment computation inputs when multisampling a pixel to
        /// avoid aliasing.
        /// </summary>
        private bool mSampleShading;

        /// <summary>
        /// Stores the fragment coverage value that will be ANDed with the alpha based temporary
        /// coverage value. If 1, it will disable FragmentCoverage.
        /// </summary>
        private float mFragmentCoverage;

        /// <summary>
        /// Stores the mask used to exclude some sample or changing the coverage of a sample.
        /// </summary>
        private uint  mSampleMask;

        /// <summary>
        /// Stores the minimum amount of unique set of fragment computation inputs when multisampling
        /// a pixel to avoid aliasing. Only used if SampleShading is true.
        /// see mSampleShading
        /// </summary>
        private float mMinSampleCount;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether or not to use multiple fragment samples
        /// to compute the final color in the fragment stage.
        /// </summary>
        public bool UseMultiSampling
        {
            get
            {
                return this.mUseMultiSampling;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mUseMultiSampling, value, "UseMultiSampling" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether or not to sample alpha values to compute a
        /// coverage value that will be ANDed with the fragment coverage one.
        /// see FragmentCoverage.
        /// </summary>
        public bool SampleAlphaToCoverage
        {
            get
            {
                return this.mSampleAlphaToCoverage;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mSampleAlphaToCoverage, value, "SampleAlphaToCoverage" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether each sample alpha value must be forced
        /// to the maximum value, that is, 1 meaning fully visible.
        /// </summary>
        public bool ForceOpacity
        {
            get
            {
                return this.mForceOpacity;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mForceOpacity, value, "ForceOpacity" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether or not to explicitly request that an implementation
        /// uses a minimum number of unique set of fragment computation inputs when multisampling a pixel to
        /// avoid aliasing.
        /// </summary>
        public bool SampleShading
        {
            get
            {
                return this.mSampleShading;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mSampleShading, value, "SampleShading" );
            }
        }

        /// <summary>
        /// Gets or sets the fragment coverage value that will be ANDed with the alpha based temporary
        /// coverage value. If 1, it will disable FragmentCoverage.
        /// </summary>
        public float FragmentCoverage
        {
            get
            {
                return this.mFragmentCoverage;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mFragmentCoverage, value, "FragmentCoverage" );
            }
        }

        /// <summary>
        /// Gets or sets the mask used to exclude some sample or changing the coverage of a sample.
        /// </summary>
        public uint SampleMask
        {
            get
            {
                return this.mSampleMask;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mSampleMask, value, "SampleMask" );
            }
        }

        /// <summary>
        /// Gets or sets the minimum amount of unique set of fragment computation inputs when multisampling
        /// a pixel to avoid aliasing. Only used if SampleShading is true.
        /// see SampleShading
        /// </summary>
        public float MinSampleCount
        {
            get
            {
                return this.mMinSampleCount;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mMinSampleCount, value, "MinSampleCount" );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SamplingDescription"/> class.
        /// </summary>
        /// <param name="pUseMultiSampling">the flag indicating whether or not to use multiple fragment samples to compute the final color in the fragment stage.</param>
        /// <param name="pSampleAlphaToCoverage">the flag indicating whether or not to sample alpha values to compute a coverage value that will be ANDed with the fragment coverage one.</param>
        /// <param name="pForceOpacity">the flag indicating whether each sample alpha value must be forced to the maximum value, that is, 1 meaning fully visible.</param>
        /// <param name="pSampleShading">the flag indicating whether or not to explicitly request that an implementation uses a minimum number of unique set of fragment computation inputs when multisampling a pixel to avoid aliasing.</param>
        /// <param name="pFragmentCoverage">the fragment coverage value that will be ANDed with the alpha based temporary coverage value.</param>
        /// <param name="pSampleMask">the mask used to exclude some sample or changing the coverage of a sample.</param>
        /// <param name="pMinSampleCount">the minimum amount of unique set of fragment computation inputs when multisampling a pixel to avoid aliasing.</param>
        public SamplingDescription(bool pUseMultiSampling = true, bool pSampleAlphaToCoverage = false, bool pForceOpacity = false, bool pSampleShading = false, float pFragmentCoverage = 1.0f, uint pSampleMask = 0xFFFFFFFF, float pMinSampleCount = 1.0f)
        {
            this.mUseMultiSampling = pUseMultiSampling;
            this.mSampleAlphaToCoverage = pSampleAlphaToCoverage;
            this.mForceOpacity = pForceOpacity;
            this.mSampleShading = pSampleShading;
            this.mFragmentCoverage = pFragmentCoverage;
            this.mSampleMask = pSampleMask;
            this.mMinSampleCount = pMinSampleCount;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Clone the description.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            SamplingDescription lClone = new SamplingDescription( this.mUseMultiSampling, this.mSampleAlphaToCoverage, this.mForceOpacity, this.mSampleShading, this.mFragmentCoverage, this.mSampleMask, this.mMinSampleCount );
            return lClone;
        }

        #endregion Methods
    }
}
