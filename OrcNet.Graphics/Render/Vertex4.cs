﻿namespace OrcNet.Graphics.Render
{
    /// <summary>
    /// Definition of the <see cref="Vertex4"/> structure.
    /// Container whose size is determinable by the Marshal.SizeOf() with no padding
    /// important to send to GPU.
    /// </summary>
    public struct Vertex4
    {
        #region Properties

        /// <summary>
        /// Gets or sets the X position component
        /// </summary>
        public float X
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Y position component
        /// </summary>
        public float Y
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Z position component
        /// </summary>
        public float Z
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the W position component
        /// </summary>
        public float W
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Vertex3"/> structure.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        /// <param name="pZ">The Z component.</param>
        /// <param name="pW">The W component.</param>
        public Vertex4(float pX, float pY, float pZ, float pW)
        {
            this.X = pX;
            this.Y = pY;
            this.Z = pZ;
            this.W = pW;
        }

        #endregion Constructor
    }
}
