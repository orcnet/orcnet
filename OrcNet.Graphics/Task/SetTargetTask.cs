﻿using OrcNet.Core.Logger;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.SceneGraph;
using OrcNet.Graphics.Services;
using OrcNet.Graphics.Task.Factories;
using System.Collections.Generic;
using System.Text;

namespace OrcNet.Graphics.Task
{
    /// <summary>
    /// Set Target task class definition in charge of 
    /// modifying the attachments of a frame buffer.
    /// </summary>
    public class SetTargetTask : ATask
    {
        #region Fields

        /// <summary>
        /// Stores the offscreen framebuffer for being used in that task.
        /// </summary>
        private static readonly FrameBuffer sTargetBuffer;

        /// <summary>
        /// Stores the textures to be set to the frame buffer attachment points.
        /// </summary>
        private List<ITexture> mTextures;

        /// <summary>
        /// Stores the factory that created that task.
        /// </summary>
        private SetTargetTaskFactory mFactory;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes the static member(s) of the <see cref="SetTargetTask"/> class.
        /// </summary>
        static SetTargetTask()
        {
            sTargetBuffer = new FrameBuffer();
            sTargetBuffer.UseAsReadBuffer( BufferId.NONE );
            sTargetBuffer.UseSingleDrawBuffer( BufferId.NONE );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTargetTask"/> class.
        /// </summary>
        /// <param name="pFactory">The factory that created that task.</param>
        /// <param name="pTextures">The textures to be set to the frame buffer attachment points.</param>
        public SetTargetTask(SetTargetTaskFactory pFactory, List<ITexture> pTextures) :
        base( 0, true )
        {
            this.mTextures = pTextures;
            this.mFactory  = pFactory;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        public override bool Execute()
        {
            ISceneService lSceneManager = ServiceManager.Instance.GetService<ISceneService>();
            StringBuilder lBuilder = new StringBuilder( "SetTarget " );
            if ( this.mTextures.Count == 0 )
            {
                lBuilder.Append( "default framebuffer." );
            }
            else
            {
                for ( int lCurr = 0; lCurr < this.mTextures.Count; lCurr++ )
                {
                    ITexture lTexture = this.mTextures[ lCurr ];
                    AttachmentTarget lTarget = this.mFactory[ lCurr ];
                    lBuilder.Append( lTarget.AttachmentPoint.ToString() );
                    lBuilder.Append( " \'" );
                    lBuilder.Append( lTexture.Creator.Name );
                    lBuilder.Append( "\'" );
                }
            }

            LogManager.Instance.Log( lBuilder.ToString() );

            FrameBuffer lBuffer = sTargetBuffer;
            if ( this.mTextures.Count == 0 )
            {
                BufferId[] lAttachments = new BufferId[ 10 ]
                {
                    BufferId.COLOR0,
                    BufferId.COLOR1,
                    BufferId.COLOR2,
                    BufferId.COLOR3,
                    BufferId.COLOR4,
                    BufferId.COLOR5,
                    BufferId.COLOR6,
                    BufferId.COLOR7,
                    BufferId.STENCIL,
                    BufferId.DEPTH,
                };

                for ( int lCurr = 0; lCurr < 10; lCurr++ )
                {
                    ITexture lTexture = lBuffer.GetAttachment<ITexture>( lAttachments[ lCurr ] );
                    if ( lTexture != null )
                    {
                        lTexture.GenerateMipmaps();
                    }
                }

                IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                if ( lSceneManager != null )
                {
                    lSceneManager.CurrentFrameBuffer = lRenderService.DefaultFrameBuffer;
                }
                else
                {
                    LogManager.Instance.Log( "Missing scene service!!!", LogType.ERROR );
                }

                return true;
            }

            int lWidth  = 0;
            int lHeight = 0;
            for ( int lCurr = 0; lCurr < this.mTextures.Count; lCurr++ )
            {
                ITexture lTexture = this.mTextures[ lCurr ];
                AttachmentTarget lTarget = this.mFactory[ lCurr ];
                if ( lTexture is Texture2D )
                {
                    Texture2D lCast = lTexture as Texture2D;
                    lBuffer.Attach( lTarget.AttachmentPoint, lTexture, lTarget.Level );
                    lWidth  = lCast.Width;
                    lHeight = lCast.Height;
                }
                else if ( lTexture is Texture2DArray )
                {
                    Texture2DArray lCast = lTexture as Texture2DArray;
                    lBuffer.Attach( lTarget.AttachmentPoint, lTexture, lTarget.Level, lTarget.Layer );
                    lWidth  = lCast.Width;
                    lHeight = lCast.Height;
                }
                else if ( lTexture is TextureCube )
                {
                    TextureCube lCast = lTexture as TextureCube;
                    lBuffer.Attach( lTarget.AttachmentPoint, lTexture, lTarget.Level, lTarget.Layer );
                    lWidth  = lCast.Width;
                    lHeight = lCast.Height;
                }
                else if ( lTexture is Texture3D )
                {
                    Texture3D lCast = lTexture as Texture3D;
                    lBuffer.Attach( lTarget.AttachmentPoint, lTexture, lTarget.Level, lTarget.Layer );
                    lWidth  = lCast.Width;
                    lHeight = lCast.Height;
                }
                else
                {
                    Texture1D lCast = lTexture as Texture1D;
                    lBuffer.Attach( lTarget.AttachmentPoint, lTexture, lTarget.Level );
                    lWidth  = lCast.Width;
                    lHeight = 1;
                }
            }

            if ( lSceneManager != null )
            {
                lSceneManager.CurrentFrameBuffer = lBuffer;
                lBuffer.Parameters.SingleViewport( new Viewport( 0, 0, lWidth, lHeight ) );
            }

            return true;
        }
        
        #endregion Methods
    }
}
