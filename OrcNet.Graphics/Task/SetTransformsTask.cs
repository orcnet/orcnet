﻿using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.SceneGraph;
using OrcNet.Graphics.Services;
using OrcNet.Graphics.Task.Factories;
using System.Linq;

namespace OrcNet.Graphics.Task
{
    /// <summary>
    /// Set Transforms task class definition in charge of 
    /// sending the transformation matrices for a pass.
    /// </summary>
    public class SetTransformsTask : ATask
    {
        #region Fields

        /// <summary>
        /// Stores the node that contains the method which the Factory belongs to.
        /// </summary>
        private SceneNode mContext;

        /// <summary>
        /// Stores the node corresponding to the screen space.
        /// </summary>
        private SceneNode mScreenNode;

        /// <summary>
        /// Stores the factory that created that task.
        /// </summary>
        private SetTransformsTaskFactory mFactory;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTransformsTask"/> class.
        /// </summary>
        /// <param name="pScreenNode">The node corresponding to the screen space.</param>
        /// <param name="pContext">The node that contains the method which the Factory belongs to.</param>
        /// <param name="pFactory">The factory that created that task.</param>
        public SetTransformsTask(SceneNode pScreenNode, SceneNode pContext, SetTransformsTaskFactory pFactory) :
        base( 0, true )
        {
            this.mScreenNode = pScreenNode;
            this.mContext    = pContext;
            this.mFactory    = pFactory;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        public override bool Execute()
        {
            LogManager.Instance.Log( "SetTransforms" );

            SceneManager lSceneManager = ServiceManager.Instance.GetService<SceneManager>();
            if ( lSceneManager == null )
            {
                LogManager.Instance.Log( "Missing Scene service!!!", LogType.ERROR );
                return true;
            }

            PipelinePass lPass = null;
            if ( this.mFactory.PipelineDescription != null &&
                 this.mFactory.PipelineDescription.Users.Any() )
            {
                lPass = this.mFactory.PipelineDescription.Users.FirstOrDefault();
            }
            else
            {
                lPass = lSceneManager.CurrentPass as PipelinePass;
            }

            if ( lPass == null )
            {
                return true;
            }

            LogManager.Instance.Log( string.Format( "SetTransforms {0}", lPass.Id ) );

            if ( lPass != this.mFactory.LastPass )
            {
                this.mFactory.TimeUniform = string.IsNullOrEmpty( this.mFactory.TimeUniformName ) ? null : lPass.GetUniform<Vector2FUniform>( new UniformName( this.mFactory.TimeUniformName ) );
                this.mFactory.LocalToWorldUniform = string.IsNullOrEmpty( this.mFactory.LocalToWorldUniformName ) ? null : lPass.GetUniform<Matrix4FUniform>( new UniformName( this.mFactory.LocalToWorldUniformName ) );
                this.mFactory.LocalToScreenUniform = string.IsNullOrEmpty( this.mFactory.LocalToScreenUniformName ) ? null : lPass.GetUniform<Matrix4FUniform>( new UniformName( this.mFactory.LocalToScreenUniformName ) );
                this.mFactory.CameraToWorldUniform = string.IsNullOrEmpty( this.mFactory.CameraToWorldUniformName ) ? null : lPass.GetUniform<Matrix4FUniform>( new UniformName( this.mFactory.CameraToWorldUniformName ) );
                this.mFactory.CameraToScreenUniform = string.IsNullOrEmpty( this.mFactory.CameraToScreenUniformName ) ? null : lPass.GetUniform<Matrix4FUniform>( new UniformName( this.mFactory.CameraToScreenUniformName ) );
                this.mFactory.ScreenToCameraUniform = string.IsNullOrEmpty( this.mFactory.ScreenToCameraUniformName ) ? null : lPass.GetUniform<Matrix4FUniform>( new UniformName( this.mFactory.ScreenToCameraUniformName ) );
                this.mFactory.WorldToScreenUniform = string.IsNullOrEmpty( this.mFactory.WorldToScreenUniformName ) ? null : lPass.GetUniform<Matrix4FUniform>( new UniformName( this.mFactory.WorldToScreenUniformName ) );
                this.mFactory.WorldPosition = string.IsNullOrEmpty( this.mFactory.WorldOriginUniformName ) ? null : lPass.GetUniform<Vector3FUniform>( new UniformName( this.mFactory.WorldOriginUniformName ) );
                this.mFactory.WorldDirection = string.IsNullOrEmpty( this.mFactory.WorldZUnitUniformName ) ? null : lPass.GetUniform<Vector3FUniform>( new UniformName( this.mFactory.WorldZUnitUniformName ) );
                this.mFactory.LastPass = lPass;
            }

            if ( this.mFactory.TimeUniform != null )
            {
                this.mFactory.TimeUniform.Value = new Vector2F( (float)lSceneManager.AbsoluteTime, (float)lSceneManager.ElapsedTime );
            }

            if ( this.mFactory.LocalToWorldUniform != null )
            {
                this.mFactory.LocalToWorldUniform.Value = this.mContext.LocalToWorldTransform.Data;
            }

            if ( this.mFactory.LocalToScreenUniform != null )
            {
                if ( this.mFactory.Screen.Target.Length == 0 )
                {
                    this.mFactory.LocalToScreenUniform.Value = this.mContext.LocalToScreenTransform.Data;
                }
                else
                {
                    AMatrix<float> lLocalToScreenTransform = (this.mScreenNode.WorldToLocalTransform * this.mContext.LocalToWorldTransform);
                    this.mFactory.LocalToScreenUniform.Value = lLocalToScreenTransform.Data;
                }
            }

            if ( this.mFactory.CameraToWorldUniform != null )
            {
                SceneNode lCameraNode = lSceneManager.Camera as SceneNode;
                this.mFactory.CameraToWorldUniform.Value = lCameraNode.LocalToWorldTransform.Data;
            }

            if ( this.mFactory.CameraToScreenUniform != null )
            {
                this.mFactory.CameraToScreenUniform.Value = lSceneManager.CameraToScreenTransform.Data;
            }

            if ( this.mFactory.ScreenToCameraUniform != null )
            {
                bool lResult;
                IMatrix<float> lInverse = lSceneManager.CameraToScreenTransform.Inverse( float.Epsilon, out lResult );
                if ( lResult )
                {
                    this.mFactory.ScreenToCameraUniform.Value = lInverse.Data;
                }
            }

            if ( this.mFactory.WorldToScreenUniform != null )
            {
                if ( this.mFactory.Screen.Target.Length == 0 )
                {
                    this.mFactory.WorldToScreenUniform.Value = lSceneManager.WorldToScreenTransform.Data;
                }
                else
                {
                    AMatrix<float> lWorldToScreenTransform = this.mScreenNode.WorldToLocalTransform;
                    this.mFactory.WorldToScreenUniform.Value = lWorldToScreenTransform.Data;
                }
            }

            if ( this.mFactory.WorldPosition != null )
            {
                this.mFactory.WorldPosition.Value = this.mContext.WorldCoordinates;
            }

            if ( this.mFactory.WorldDirection != null )
            {
                Vector4<float> lDirection = this.mContext.LocalToWorldTransform.Multiply( Vector4F.UNIT_Z ) as Vector4<float>;
                this.mFactory.WorldDirection.Value = new Vector3F( -lDirection.X, -lDirection.Y, -lDirection.Z );
            }

            return true;
        }
        
        #endregion Methods
    }
}
