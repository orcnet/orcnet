﻿using OrcNet.Core.Logger;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.SceneGraph;

namespace OrcNet.Graphics.Task
{
    /// <summary>
    /// Set Pipeline Pass task class definition in charge of 
    /// modifying the current pass used to draw.
    /// </summary>
    public class SetPassTask : ATask
    {
        #region Fields

        /// <summary>
        /// Stores the pipeline pass to set.
        /// </summary>
        private PipelinePass mPass;

        /// <summary>
        /// Stores the task owner scene node
        /// </summary>
        private SceneNode    mTaskOwner;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPassTask"/> class.
        /// </summary>
        /// <param name="pPass">The pass to set.</param>
        /// <param name="pTaskOwner">The scene node task owner if uniforms must be set.</param>
        public SetPassTask(PipelinePass pPass, SceneNode pTaskOwner) :
        base( 0, true )
        {
            this.mPass = pPass;
            this.mTaskOwner = pTaskOwner;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        public override bool Execute()
        {
            if ( this.mPass != null )
            {
                ISceneService lSceneService = ServiceManager.Instance.GetService<ISceneService>();

                LogManager.Instance.Log( "Draw mesh..." );

                if ( this.mTaskOwner != null )
                {
                    foreach ( IPipelineValue lValue in this.mTaskOwner.Values )
                    {
                        IPipelineUniform lUniform = this.mPass[ new UniformName( lValue.Name ) ];
                        if ( lUniform != null )
                        {
                            lUniform.UntypedValue = lValue;
                        }
                    }
                }

                lSceneService.CurrentPass = this.mPass;
            }

            return true;
        }
        
        #endregion Methods
    }
}
