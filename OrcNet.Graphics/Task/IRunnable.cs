﻿using OrcNet.Core;
using OrcNet.Graphics.Render;
using System;

namespace OrcNet.Graphics.Task
{
    /// <summary>
    /// Runnable interface definition being a subtask of a task.
    /// </summary>
    public interface IRunnable : IMemoryProfilable, IDisposable
    {
        #region Methods

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        void Execute(FrameBuffer pFrameBuffer);

        #endregion Methods
    }
}
