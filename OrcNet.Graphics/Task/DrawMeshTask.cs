﻿using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Services;

namespace OrcNet.Graphics.Task
{
    /// <summary>
    /// Draw mesh task class definition in charge of drawing a mesh.
    /// </summary>
    public class DrawMeshTask : ATask
    {
        #region Fields

        /// <summary>
        /// Stores the mesh info to use to draw the mesh.
        /// </summary>
        private MeshBuffers mToDraw;

        /// <summary>
        /// Stores the amount of times the mesh to draw must be drawn.
        /// </summary>
        private int mDrawCount;

        #endregion Fields
        
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawMeshTask"/> class.
        /// </summary>
        /// <param name="pMesh">The mesh info to use to draw the mesh.</param>
        /// <param name="pDrawCount">The amount of times the mesh to draw must be drawn.</param>
        public DrawMeshTask(MeshBuffers pMesh, int pDrawCount) :
        base( 0, true )
        {
            this.mToDraw = pMesh;
            this.mDrawCount = pDrawCount;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        public override bool Execute()
        {
            if ( this.mToDraw != null )
            {
                SceneManager lSceneService = ServiceManager.Instance.GetService<SceneManager>();

                LogManager.Instance.Log( "Draw mesh..." );

                PipelinePass lPass       = lSceneService.CurrentPass as PipelinePass;
                FrameBuffer lFrameBuffer = lSceneService.CurrentFrameBuffer as FrameBuffer;
                if ( this.mToDraw.IndexCount == 0 )
                {
                    lFrameBuffer.Draw( lPass, this.mToDraw, this.mToDraw.PrimitiveMode, 0, this.mToDraw.VertexCount );
                }
                else
                {
                    lFrameBuffer.Draw( lPass, this.mToDraw, this.mToDraw.PrimitiveMode, 0, this.mToDraw.IndexCount );
                }
            }

            return true;
        }
        
        #endregion Methods
    }
}
