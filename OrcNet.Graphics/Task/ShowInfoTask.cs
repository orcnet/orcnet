﻿using OrcNet.Core.SceneGraph;
using OrcNet.Core.Task;
using OrcNet.Graphics.Task.Factories;

namespace OrcNet.Graphics.Task
{
    /// <summary>
    /// Show info task class definition in charge of drawing text info on the screen.
    /// </summary>
    public class ShowInfoTask : ATask
    {
        #region Fields

        /// <summary>
        /// Stores the factory that created that task.
        /// </summary>
        private ShowInfoTaskFactory mFactory;

        /// <summary>
        /// Stores the method to which the factory belongs.
        /// </summary>
        private SceneNodeMethod     mContext;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowInfoTask"/> class.
        /// </summary>
        /// <param name="pContext">The method to which the factory belongs.</param>
        /// <param name="pFactory">The factory that created that task.</param>
        public ShowInfoTask(SceneNodeMethod pContext, ShowInfoTaskFactory pFactory) :
        base( 0, true )
        {
            this.mContext = pContext;
            this.mFactory = pFactory;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        public override bool Execute()
        {
            this.mFactory.Draw( this.mContext );
            return true;
        }
        
        #endregion Methods
    }
}
