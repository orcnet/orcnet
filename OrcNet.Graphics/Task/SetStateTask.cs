﻿using OrcNet.Core.Logger;
using OrcNet.Core.Task;
using OrcNet.Graphics.Task.Factories;

namespace OrcNet.Graphics.Task
{
    /// <summary>
    /// Set State task class definition in charge of 
    /// modifying the current pass used to draw.
    /// </summary>
    public class SetStateTask : ATask
    {
        #region Fields

        /// <summary>
        /// Stores the factory that created this task.
        /// </summary>
        private SetStateTaskFactory mFactory;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetStateTask"/> class.
        /// </summary>
        /// <param name="pFactory">The factory that created this task.</param>
        public SetStateTask(SetStateTaskFactory pFactory) :
        base( 0, true )
        {
            this.mFactory = pFactory;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        public override bool Execute()
        {
            LogManager.Instance.Log( "SetState" );

            this.mFactory.ExecuteSubTasks();

            return true;
        }
        
        #endregion Methods
    }
}
