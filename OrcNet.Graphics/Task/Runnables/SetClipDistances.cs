﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set clip distances runnable sub task class definition.
    /// </summary>
    public class SetClipDistances : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new clip distances to set.
        /// </summary>
        private int mClipDistances;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(int);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetClipDistances"/> class.
        /// </summary>
        /// <param name="pClipDistances">The new clip distances to set.</param>
        public SetClipDistances(int pClipDistances)
        {
            this.mClipDistances = pClipDistances;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.ClipDistances = this.mClipDistances;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
