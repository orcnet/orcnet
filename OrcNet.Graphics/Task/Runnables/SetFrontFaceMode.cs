﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Front face mode runnable sub task class definition.
    /// </summary>
    public class SetFrontFaceMode : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag to set indicating whether or not front faces are clock wise.
        /// </summary>
        private bool mIsClockWise;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetFrontFaceMode"/> class.
        /// </summary>
        /// <param name="pIsClockWise">The new flag to set indicating whether or not front faces are clock wise.</param>
        public SetFrontFaceMode(bool pIsClockWise)
        {
            this.mIsClockWise = pIsClockWise;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.PolygonDescription.AreFrontFacesCW = this.mIsClockWise;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
