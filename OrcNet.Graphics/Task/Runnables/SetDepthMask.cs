﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Depth mask runnable sub task class definition.
    /// </summary>
    public class SetDepthMask : ARunnable
    {
        #region Fields
        
        /// <summary>
        /// Stores the flag to set indicating whether the depth buffer can be written to or not.
        /// </summary>
        private bool mAllowDepthBufferWrite;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetDepthMask"/> class.
        /// </summary>
        /// <param name="pAllowDepthBufferWrite">The flag to set indicating whether the depth buffer can be written to or not.</param>
        public SetDepthMask(bool pAllowDepthBufferWrite)
        {
            this.mAllowDepthBufferWrite = pAllowDepthBufferWrite;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.AllowDepthBufferWrite = this.mAllowDepthBufferWrite;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
