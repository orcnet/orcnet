﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set point size runnable sub task class definition.
    /// </summary>
    public class SetPointSize : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new point size to set.
        /// </summary>
        private float mPointSize;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(float);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPointSize"/> class.
        /// </summary>
        /// <param name="pPointSize">The new point size to set.</param>
        public SetPointSize(float pPointSize)
        {
            this.mPointSize = pPointSize;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.PointDescription.PointSize = this.mPointSize;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
