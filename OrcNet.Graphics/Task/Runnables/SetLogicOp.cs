﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set logic operation runnable sub task class definition.
    /// </summary>
    public class SetLogicOp : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag to set indicating whether or not to use logical operation.
        /// </summary>
        private bool mUseLogicOp;

        /// <summary>
        /// Stores the flag indicating whether or not to update the logic operation.
        /// </summary>
        private bool mUpdateOperation;

        /// <summary>
        /// Stores the new logic operation to set. (NULL means no operation update).
        /// </summary>
        private LogicOp mOperation;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool) * 2;
                lSize += sizeof(LogicOp);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetLogicOp"/> class.
        /// </summary>
        /// <param name="pUseLogicOp">The new flag to set indicating whether or not to use logical operation.</param>
        /// <param name="pOperation">The new logic operation to set. (NULL means no operation update).</param>
        public SetLogicOp(bool pUseLogicOp, LogicOp? pOperation)
        {
            this.mUseLogicOp = pUseLogicOp;
            if
                ( pOperation.HasValue )
            {
                this.mOperation = pOperation.Value;
                this.mUpdateOperation = true;
            }
            else
            {
                this.mUpdateOperation = false;
            }
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            if
                ( this.mUpdateOperation == false )
            {
                pFrameBuffer.Parameters.UseLogicalOperations = this.mUseLogicOp;
            }
            else
            {
                pFrameBuffer.Parameters.ApplyLogicalOperations( this.mUseLogicOp, this.mOperation );
            }
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
