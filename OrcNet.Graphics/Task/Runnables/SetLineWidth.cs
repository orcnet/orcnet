﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Line width runnable sub task class definition.
    /// </summary>
    public class SetLineWidth : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new line width to set.
        /// </summary>
        private float mLineWidth;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(float);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetLineWidth"/> class.
        /// </summary>
        /// <param name="pLineWidth">The new line width to set.</param>
        public SetLineWidth(float pLineWidth)
        {
            this.mLineWidth = pLineWidth;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.LineDescription.LineSize = this.mLineWidth;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
