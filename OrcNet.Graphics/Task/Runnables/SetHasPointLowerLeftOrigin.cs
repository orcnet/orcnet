﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set has point lower left origin runnable sub task class definition.
    /// </summary>
    public class SetHasPointLowerLeftOrigin : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag determining whether there is a point lower left origin or not.
        /// </summary>
        private bool mHasPointLowerLeftOrigin;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetHasPointLowerLeftOrigin"/> class.
        /// </summary>
        /// <param name="pHasPointLowerLeftOrigin">The new flag determining whether there is a point lower left origin or not.</param>
        public SetHasPointLowerLeftOrigin(bool pHasPointLowerLeftOrigin)
        {
            this.mHasPointLowerLeftOrigin = pHasPointLowerLeftOrigin;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.PointDescription.HasPointLowerLeftOrigin = this.mHasPointLowerLeftOrigin;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
