﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set blend runnable sub task class definition.
    /// </summary>
    public class SetBlend : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the attachment point blending must be set for.
        /// </summary>
        private BufferId mBufferId;

        /// <summary>
        /// Stores the new flag indicating whether or not enabling the blending.
        /// </summary>
        private bool mUseBlending;

        /// <summary>
        /// Stores the flag indicating whether the RGB equation dependencies must be set or not.
        /// </summary>
        private bool mUpdateRGBEquation;

        /// <summary>
        /// Stores the new RGB blending equation to set.
        /// </summary>
        private BlendEquationMode mRGBEquation;

        /// <summary>
        /// Stores the new RGB blending source factor to set.
        /// </summary>
        private BlendingFactorSrc mRGBSourceFactor;

        /// <summary>
        /// Stores the new RGB blending destination factor to set.
        /// </summary>
        private BlendingFactorDest mRGBDestinationFactor;

        /// <summary>
        /// Stores the flag indicating whether the Alpha equation dependencies must be set or not.
        /// </summary>
        private bool mUpdateAlphaEquation;

        /// <summary>
        /// Stores the new Alpha blending equation to set.
        /// </summary>
        private BlendEquationMode mAlphaEquation;

        /// <summary>
        /// Stores the new Alpha blending source factor to set.
        /// </summary>
        private BlendingFactorSrc mAlphaSourceFactor;

        /// <summary>
        /// Stores the new Alpha blending destination factor to set.
        /// </summary>
        private BlendingFactorDest mAlphaDestinationFactor;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool) * 3;
                lSize += sizeof(BufferId);
                lSize += sizeof(BlendEquationMode) * 2;
                lSize += sizeof(BlendingFactorSrc) * 2;
                lSize += sizeof(BlendingFactorDest) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetBlend"/> class.
        /// </summary>
        /// <param name="pBufferId">The attachment point blending must be set for. (Can be NONE)</param>
        /// <param name="pUseBlending">The new flag indicating whether or not enabling the blending.</param>
        /// <param name="pRGBEquation">The new RGB blending equation to set. (NULL means no update of the RGB equation and its dependencies)</param>
        /// <param name="pRGBSourceFactor">The new RGB blending source factor to set.</param>
        /// <param name="pRGBDestinationFactor">The new RGB blending destination factor to set.</param>
        /// <param name="pAlphaEquation">The new Alpha blending equation to set. (NULL means no update of the Alpha equation and its dependencies)</param>
        /// <param name="pAlphaSourceFactor">The new Alpha blending source factor to set.</param>
        /// <param name="pAlphaDestinationFactor">The new Alpha blending destination factor to set.</param>
        public SetBlend(BufferId pBufferId, bool pUseBlending, BlendEquationMode? pRGBEquation, BlendingFactorSrc pRGBSourceFactor, BlendingFactorDest pRGBDestinationFactor,
                                                               BlendEquationMode? pAlphaEquation, BlendingFactorSrc pAlphaSourceFactor, BlendingFactorDest pAlphaDestinationFactor)
        {
            this.mBufferId = pBufferId;
            this.mUseBlending = pUseBlending;
            if
                ( pRGBEquation.HasValue )
            {
                this.mRGBEquation = pRGBEquation.Value;
                this.mUpdateRGBEquation = true;
            }
            else
            {
                this.mUpdateRGBEquation = false;
            }
            this.mRGBSourceFactor = pRGBSourceFactor;
            this.mRGBDestinationFactor = pRGBDestinationFactor;
            if
                ( pAlphaEquation.HasValue )
            {
                this.mAlphaEquation = pAlphaEquation.Value;
                this.mUpdateAlphaEquation = true;
            }
            else
            {
                this.mUpdateAlphaEquation = false;
            }
            this.mAlphaSourceFactor = pAlphaSourceFactor;
            this.mAlphaDestinationFactor = pAlphaDestinationFactor;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            if
                ( this.mUpdateRGBEquation == false &&
                  this.mBufferId == BufferId.NONE )
            {
                pFrameBuffer.Parameters.BlendingDescription.Blend( this.mUseBlending );
            }
            else if
                ( this.mUpdateRGBEquation == false )
            {
                pFrameBuffer.Parameters.BlendingDescription.Blend( this.mBufferId, this.mUseBlending );
            }
            else if
                ( this.mUpdateAlphaEquation == false &&
                  this.mBufferId == BufferId.NONE )
            {
                pFrameBuffer.Parameters.BlendingDescription.Blend( this.mUseBlending, this.mRGBEquation, this.mRGBSourceFactor, this.mRGBDestinationFactor );
            }
            else if
                ( this.mUpdateAlphaEquation == false )
            {
                pFrameBuffer.Parameters.BlendingDescription.Blend( this.mBufferId, this.mUseBlending, this.mRGBEquation, this.mRGBSourceFactor, this.mRGBDestinationFactor );
            }
            else if
                ( this.mBufferId == BufferId.NONE )
            {
                pFrameBuffer.Parameters.BlendingDescription.Blend( this.mUseBlending, this.mRGBEquation, this.mRGBSourceFactor, this.mRGBDestinationFactor, this.mAlphaEquation, this.mAlphaSourceFactor, this.mAlphaDestinationFactor );
            }
            else
            {
                pFrameBuffer.Parameters.BlendingDescription.Blend( this.mBufferId, this.mUseBlending, this.mRGBEquation, this.mRGBSourceFactor, this.mRGBDestinationFactor, this.mAlphaEquation, this.mAlphaSourceFactor, this.mAlphaDestinationFactor );
            }
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
