﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set sample shading runnable sub task class definition.
    /// </summary>
    public class SetSampleShading : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag to set indicating whether or not to sample shading.
        /// </summary>
        private bool mSampleShading;

        /// <summary>
        /// Stores the minimum sample count to set.
        /// </summary>
        private float mMinSampleCount;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(float);
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetSampleShading"/> class.
        /// </summary>
        /// <param name="pSampleShading">The new flag to set indicating whether or not to sample shading.</param>
        /// <param name="pMinSampleCount">The minimum sample count to set.</param>
        public SetSampleShading(bool pSampleShading, float pMinSampleCount)
        {
            this.mSampleShading  = pSampleShading;
            this.mMinSampleCount = pMinSampleCount;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.SamplingDescription.SampleShading  = this.mSampleShading;
            pFrameBuffer.Parameters.SamplingDescription.MinSampleCount = this.mMinSampleCount;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
