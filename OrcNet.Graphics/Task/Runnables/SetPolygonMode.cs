﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Polygon mode runnable sub task class definition.
    /// </summary>
    public class SetPolygonMode : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether or not to cull the front faces.
        /// </summary>
        private bool mCullFront;

        /// <summary>
        /// Stores the flag indicating whether or not to cull the back faces.
        /// </summary>
        private bool mCullBack;

        /// <summary>
        /// Stores the new polygon front mode to set.
        /// </summary>
        private PolygonMode mFrontMode;

        /// <summary>
        /// Stores the new polygon back mode to set.
        /// </summary>
        private PolygonMode mBackMode;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(PolygonMode) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPolygonMode"/> class.
        /// </summary>
        /// <param name="pCullFront">The flag indicating whether or not to cull the front faces.</param>
        /// <param name="pCullBack">The flag indicating whether or not to cull the back faces.</param>
        /// <param name="pFrontMode">The new polygon front mode to set.</param>
        /// <param name="pBackMode">The new polygon back mode to set.</param>
        public SetPolygonMode(bool pCullFront, bool pCullBack, PolygonMode pFrontMode, PolygonMode pBackMode)
        {
            this.mCullFront = pCullFront;
            this.mCullBack  = pCullBack;
            this.mFrontMode = pFrontMode;
            this.mBackMode  = pBackMode;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.PolygonDescription.CullFront = this.mCullFront;
            pFrameBuffer.Parameters.PolygonDescription.FrontMode = this.mFrontMode;
            pFrameBuffer.Parameters.PolygonDescription.CullBack  = this.mCullBack;
            pFrameBuffer.Parameters.PolygonDescription.BackMode  = this.mBackMode;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
