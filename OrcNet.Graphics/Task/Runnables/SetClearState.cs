﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Clear state runnable sub task class definition.
    /// </summary>
    public class SetClearState : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether to clear color buffers or not.
        /// </summary>
        private bool mMustClearColorBuffers;

        /// <summary>
        /// Stores the flag indicating whether to clear stencil buffer or not.
        /// </summary>
        private bool mMustClearStencilBuffer;

        /// <summary>
        /// Stores the flag indicating whether to clear depth buffer or not.
        /// </summary>
        private bool mMustClearDepthBuffer;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool) * 3;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetClearState"/> class.
        /// </summary>
        /// <param name="pMustClearColorBuffers">The flag indicating whether to clear color buffers or not.</param>
        /// <param name="pMustClearStencilBuffer">The flag indicating whether to clear stencil buffer or not.</param>
        /// <param name="pMustClearDepthBuffer">The flag indicating whether to clear depth buffer or not.</param>
        public SetClearState(bool pMustClearColorBuffers, bool pMustClearStencilBuffer, bool pMustClearDepthBuffer)
        {
            this.mMustClearColorBuffers  = pMustClearColorBuffers;
            this.mMustClearStencilBuffer = pMustClearStencilBuffer;
            this.mMustClearDepthBuffer   = pMustClearDepthBuffer;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Clear( this.mMustClearColorBuffers, this.mMustClearStencilBuffer, this.mMustClearDepthBuffer );
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
