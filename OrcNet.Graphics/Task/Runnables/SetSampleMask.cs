﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set sample mask runnable sub task class definition.
    /// </summary>
    public class SetSampleMask : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new sample mask to set.
        /// </summary>
        private uint mSampleMask;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(uint);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetSampleMask"/> class.
        /// </summary>
        /// <param name="pSampleMask">The new sample mask to set.</param>
        public SetSampleMask(uint pSampleMask)
        {
            this.mSampleMask = pSampleMask;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.SamplingDescription.SampleMask = this.mSampleMask;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
