﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set depth test runnable sub task class definition.
    /// </summary>
    public class SetDepthTest : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag to set indicating whether or not to use depth test.
        /// </summary>
        private bool mUseDepth;

        /// <summary>
        /// Stores the flag indicating whether the depth function must be set or not.
        /// </summary>
        private bool mUpdateFunction;

        /// <summary>
        /// Stores the new depth function to set.
        /// </summary>
        private DepthFunction mFunction;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool) * 2;
                lSize += sizeof(DepthFunction);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetStencilTest"/> class.
        /// </summary>
        /// <param name="pUseDepth">The new flag to set indicating whether or not to use depth test.</param>
        /// <param name="pFunction">The new depth function to set. (NULL means that no function update is required)</param>
        public SetDepthTest(bool pUseDepth, DepthFunction? pFunction)
        {
            this.mUseDepth = pUseDepth;
            if
                ( pFunction.HasValue )
            {
                this.mFunction = pFunction.Value;
                this.mUpdateFunction = true;
            }
            else
            {
                this.mUpdateFunction = false;
            }
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            if
                ( this.mUpdateFunction == false )
            {
                pFrameBuffer.Parameters.DepthDescription.UseDepth = this.mUseDepth;
            }
            else
            {
                pFrameBuffer.Parameters.DepthDescription.DepthTest( this.mUseDepth, this.mFunction );
            }
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
