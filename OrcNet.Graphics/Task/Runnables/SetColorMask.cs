﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Color mask runnable sub task class definition.
    /// </summary>
    public class SetColorMask : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the attachment point the color mask must be set for.
        /// </summary>
        private BufferId mBufferId;

        /// <summary>
        /// Stores the flag to set indicating whether Can write red channel or not.
        /// </summary>
        private bool mRed;

        /// <summary>
        /// Stores the flag to set indicating whether Can write green channel or not.
        /// </summary>
        private bool mGreen;

        /// <summary>
        /// Stores the flag to set indicating whether Can write blue channel or not.
        /// </summary>
        private bool mBlue;

        /// <summary>
        /// Stores the flag to set indicating whether Can write alpha channel or not.
        /// </summary>
        private bool mAlpha;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool) * 4;
                lSize += sizeof(BufferId);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetColorMask"/> class.
        /// </summary>
        /// <param name="pBufferId">The attachment point the color mask must be set for.</param>
        /// <param name="pRed">Can write red channel</param>
        /// <param name="pGreen">Can write green channel</param>
        /// <param name="pBlue">Can write blue channel</param>
        /// <param name="pAlpha">Can write alpha channel</param>
        public SetColorMask(BufferId pBufferId, bool pRed, bool pGreen, bool pBlue, bool pAlpha)
        {
            this.mBufferId = pBufferId;
            this.mRed = pRed;
            this.mGreen = pGreen;
            this.mBlue = pBlue;
            this.mAlpha = pAlpha;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            if
                ( this.mBufferId != BufferId.NONE )
            {
                pFrameBuffer.Parameters.ColorMask( this.mBufferId, this.mRed, this.mGreen, this.mBlue, this.mAlpha );
            }
            else
            {
                pFrameBuffer.Parameters.ColorMask( this.mRed, this.mGreen, this.mBlue, this.mAlpha );
            }
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
