﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Polygon smooth runnable sub task class definition.
    /// </summary>
    public class SetPolygonSmooth : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag to set indicating whether or not to smooth polygons.
        /// </summary>
        private bool mSmoothesPolygons;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPolygonSmooth"/> class.
        /// </summary>
        /// <param name="pSmoothesPolygons">The new flag to set indicating whether or not to smooth polygons.</param>
        public SetPolygonSmooth(bool pSmoothesPolygons)
        {
            this.mSmoothesPolygons = pSmoothesPolygons;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.PolygonDescription.SmoothesPolygons = this.mSmoothesPolygons;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
