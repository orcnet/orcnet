﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set scissor test value runnable sub task class definition.
    /// </summary>
    public class SetScissorTestValue : SetScissorTest
    {
        #region Fields

        /// <summary>
        /// Stores the new scissor to set.
        /// </summary>
        private Scissor mScissor;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(float) * 4;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetScissorTestValue"/> class.
        /// </summary>
        /// <param name="pUseScissor">The new flag to set indicating whether or not to use scissor.</param>
        /// <param name="pScissor">The scissor to use.</param>
        public SetScissorTestValue(bool pUseScissor, Scissor pScissor) :
        base( pUseScissor )
        {
            this.mScissor = pScissor;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.ScissorDescription.ScissorTest( this.mUseScissor, this.mScissor );
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
