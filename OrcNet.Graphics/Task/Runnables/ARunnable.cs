﻿using OrcNet.Core;
using OrcNet.Graphics.Render;
using System;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Base abstract runnable class definition.
    /// </summary>
    public abstract class ARunnable : AMemoryProfilable, IRunnable
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ARunnable"/> class.
        /// </summary>
        protected ARunnable()
        {

        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public abstract void Execute(FrameBuffer pFrameBuffer);

        #endregion Methods IRunnable

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            // Nothing to do.
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
