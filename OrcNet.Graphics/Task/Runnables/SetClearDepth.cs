﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set clear depth runnable sub task class definition.
    /// </summary>
    public class SetClearDepth : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new clear depth to set.
        /// </summary>
        private float mClearDepth;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(float);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetClearDepth"/> class.
        /// </summary>
        /// <param name="pClearDepth">The new clear depth to set.</param>
        public SetClearDepth(float pClearDepth)
        {
            this.mClearDepth = pClearDepth;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.ClearDepth = this.mClearDepth;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
