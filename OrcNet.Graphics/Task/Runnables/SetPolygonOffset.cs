﻿using OrcNet.Core.Math;
using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Polygon offset runnable sub task class definition.
    /// </summary>
    public class SetPolygonOffset : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new polygon offset.
        /// </summary>
        private Vector2F mOffset;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(float) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPolygonOffset"/> class.
        /// </summary>
        /// <param name="pOffset">The new polygon offset.</param>
        public SetPolygonOffset(Vector2F pOffset)
        {
            this.mOffset = pOffset;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.PolygonDescription.PolygonOffset = this.mOffset;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
