﻿using OrcNet.Graphics.Render;
using System;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Polygon offsets runnable sub task class definition.
    /// </summary>
    public class SetPolygonOffsets : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag to set indicating whether or not there is point offset.
        /// </summary>
        private bool mHasPointOffset;

        /// <summary>
        /// Stores the new flag to set indicating whether or not there is line offset.
        /// </summary>
        private bool mHasLineOffset;

        /// <summary>
        /// Stores the new flag to set indicating whether or not there is polygon offset.
        /// </summary>
        private bool mHasPolygonOffset;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool) * 3;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPolygonOffsets"/> class.
        /// </summary>
        /// <param name="pHasPointOffset">The new flag to set indicating whether or not there is point offset.</param>
        /// <param name="pHasLineOffset">The new flag to set indicating whether or not there is line offset.</param>
        /// <param name="pHasPolygonOffset">The new flag to set indicating whether or not there is polygon offset.</param>
        public SetPolygonOffsets(bool pHasPointOffset, bool pHasLineOffset, bool pHasPolygonOffset)
        {
            this.mHasPointOffset   = pHasPointOffset;
            this.mHasLineOffset    = pHasLineOffset;
            this.mHasPolygonOffset = pHasPolygonOffset;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.PolygonDescription.PolygonOffsets = new Tuple<bool, bool, bool>( this.mHasPointOffset, this.mHasLineOffset, this.mHasPolygonOffset );
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
