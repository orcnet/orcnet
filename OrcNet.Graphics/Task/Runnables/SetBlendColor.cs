﻿using OrcNet.Graphics.Render;
using System.Drawing;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set blend color runnable sub task class definition.
    /// </summary>
    public class SetBlendColor : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new blend color to set.
        /// </summary>
        private Color mBlendColor;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(byte) * 4;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetBlendColor"/> class.
        /// </summary>
        /// <param name="pBlendColor">The new blend color to set.</param>
        public SetBlendColor(Color pBlendColor)
        {
            this.mBlendColor = pBlendColor;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.BlendingDescription.ConstantColor = this.mBlendColor;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
