﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set occlusion test runnable sub task class definition.
    /// </summary>
    public class SetOcclusionTest : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new occlusion query mode to set.
        /// </summary>
        private QueryMode mQueryMode;

        /// <summary>
        /// Stores the new occlusion query to set.
        /// </summary>
        private GPUQuery  mOcclusionQuery;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(QueryMode);
                lSize += this.mOcclusionQuery.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetOcclusionTest"/> class.
        /// </summary>
        /// <param name="pOcclusionQuery">The new occlusion query mode to set.</param>
        /// <param name="pQueryMode">The new occlusion query to set.</param>
        public SetOcclusionTest(GPUQuery pOcclusionQuery, QueryMode pQueryMode)
        {
            this.mQueryMode = pQueryMode;
            this.mOcclusionQuery = pOcclusionQuery;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.OcclusionDescription.OcclusionTest( this.mOcclusionQuery, this.mQueryMode );
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
