﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set clear stencil runnable sub task class definition.
    /// </summary>
    public class SetClearStencil : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new clear stencil to set.
        /// </summary>
        private int mClearStencil;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(int);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetClearStencil"/> class.
        /// </summary>
        /// <param name="pClearStencil">The new clear stencil to set.</param>
        public SetClearStencil(int pClearStencil)
        {
            this.mClearStencil = pClearStencil;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.ClearStencil = this.mClearStencil;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
