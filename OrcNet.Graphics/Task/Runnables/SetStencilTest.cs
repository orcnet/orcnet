﻿using OrcNet.Graphics.Render;
using OpenTK.Graphics.OpenGL;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set stencil test runnable sub task class definition.
    /// </summary>
    public class SetStencilTest : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag to set indicating whether or not to use stencil test.
        /// </summary>
        private bool mUseStencil;

        /// <summary>
        /// Stores the flag indicating whether or not to update the front function and its dependencies.
        /// </summary>
        private bool mUpdateFront;

        /// <summary>
        /// Stores the new stencil front function to set.
        /// </summary>
        private StencilFunction mFrontFunction;

        /// <summary>
        /// Stores the new stencil front reference to set.
        /// </summary>
        private int mFrontReference;

        /// <summary>
        /// Stores the new stencil front mask.
        /// </summary>
        private uint mFrontMask;

        /// <summary>
        /// Stores the new stencil front operation in case of failure.
        /// </summary>
        private StencilOp mFrontStencilFailure;

        /// <summary>
        /// Stores the new depth front operation in case of failure.
        /// </summary>
        private StencilOp mFrontDepthFailure;

        /// <summary>
        /// Stores the new depth front operation in case of success.
        /// </summary>
        private StencilOp mFrontDepthPass;

        /// <summary>
        /// Stores the flag indicating whether or not to update the back function and its dependencies.
        /// </summary>
        private bool mUpdateBack;

        /// <summary>
        /// Stores the new stencil back function to set.
        /// </summary>
        private StencilFunction mBackFunction;

        /// <summary>
        /// Stores the new stencil back reference to set.
        /// </summary>
        private int mBackReference;

        /// <summary>
        /// Stores the new stencil back mask.
        /// </summary>
        private uint mBackMask;

        /// <summary>
        /// Stores the new stencil back operation in case of failure.
        /// </summary>
        private StencilOp mBackStencilFailure;

        /// <summary>
        /// Stores the new depth back operation in case of success.
        /// </summary>
        private StencilOp mBackDepthFailure;

        /// <summary>
        /// Stores the new depth back operation in case of success.
        /// </summary>
        private StencilOp mBackDepthPass;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool) * 3;
                lSize += sizeof(StencilFunction) * 2;
                lSize += sizeof(int) * 2;
                lSize += sizeof(uint) * 2;
                lSize += sizeof(StencilOp) * 6;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetStencilTest"/> class.
        /// </summary>
        /// <param name="pUseStencil">The new flag to set indicating whether or not to use stencil test.</param>
        /// <param name="pFrontFunction">The new stencil front function to set.(NULL param will mean no front stencil dependencies update)</param>
        /// <param name="pFrontReference">The new stencil front reference to set.</param>
        /// <param name="pFrontMask">The new stencil front mask.</param>
        /// <param name="pFrontStencilFailure">The new stencil front operation in case of failure.</param>
        /// <param name="pFrontDepthFailure">The new depth front operation in case of failure.</param>
        /// <param name="pFrontDepthPass">The new depth front operation in case of success.</param>
        /// <param name="pBackFunction">The new stencil back function to set.(NULL param will mean no back stencil dependencies update)</param>
        /// <param name="pBackReference">The new stencil back reference to set.</param>
        /// <param name="pBackMask">The new stencil back mask.</param>
        /// <param name="pBackStencilFailure">The new stencil back operation in case of failure.</param>
        /// <param name="pBackDepthFailure">The new depth back operation in case of success.</param>
        /// <param name="pBackDepthPass">The new depth back operation in case of success.</param>
        public SetStencilTest(bool pUseStencil, StencilFunction? pFrontFunction, int pFrontReference, uint pFrontMask, StencilOp pFrontStencilFailure, StencilOp pFrontDepthFailure, StencilOp pFrontDepthPass,
                                                StencilFunction? pBackFunction, int pBackReference, uint pBackMask, StencilOp pBackStencilFailure, StencilOp pBackDepthFailure, StencilOp pBackDepthPass)
        {
            this.mUseStencil = pUseStencil;
            if
                ( pFrontFunction.HasValue )
            {
                this.mFrontFunction = pFrontFunction.Value;
                this.mUpdateFront = true;
            }
            else
            {
                this.mUpdateFront = false;
            }
            this.mFrontReference = pFrontReference;
            this.mFrontMask = pFrontMask;
            this.mFrontStencilFailure = pFrontStencilFailure;
            this.mFrontDepthFailure = pFrontDepthFailure;
            this.mFrontDepthPass = pFrontDepthPass;
            if
                ( pBackFunction.HasValue )
            {
                this.mBackFunction = pBackFunction.Value;
                this.mUpdateBack = true;
            }
            else
            {
                this.mUpdateBack = false;
            }
            this.mBackReference = pBackReference;
            this.mBackMask = pBackMask;
            this.mBackStencilFailure = pBackStencilFailure;
            this.mBackDepthFailure = pBackDepthFailure;
            this.mBackDepthPass = pBackDepthPass;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            if
                ( this.mUpdateFront == false && 
                  this.mUpdateBack == false )
            {
                pFrameBuffer.Parameters.StencilDescription.UseStencil = this.mUseStencil;
            }
            else if
                ( this.mUpdateBack == false )
            {
                pFrameBuffer.Parameters.StencilDescription.StencilTest( this.mUseStencil, this.mFrontFunction, this.mFrontReference, this.mFrontMask, this.mFrontStencilFailure, this.mFrontDepthFailure, this.mFrontDepthPass );
            }
            else
            {
                pFrameBuffer.Parameters.StencilDescription.StencilTest( this.mUseStencil, this.mFrontFunction, this.mFrontReference, this.mFrontMask, this.mFrontStencilFailure, this.mFrontDepthFailure, this.mFrontDepthPass,
                                                                                          this.mBackFunction, this.mBackReference, this.mBackMask, this.mBackStencilFailure, this.mBackDepthFailure, this.mBackDepthPass );
            }
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
