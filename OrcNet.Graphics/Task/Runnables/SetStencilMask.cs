﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Stencil mask runnable sub task class definition.
    /// </summary>
    public class SetStencilMask : ARunnable
    {
        #region Fields
        
        /// <summary>
        /// Stores the new front stencil mask to set.
        /// </summary>
        private uint mFrontStencilMask;

        /// <summary>
        /// Stores the new front stencil mask to set.
        /// </summary>
        private uint mBackStencilMask;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(uint) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetStencilMask"/> class.
        /// </summary>
        /// <param name="pFrontStencilMask">The new front stencil mask to set.</param>
        /// <param name="pBackStencilMask">The new front stencil mask to set.</param>
        public SetStencilMask(uint pFrontStencilMask, uint pBackStencilMask)
        {
            this.mFrontStencilMask = pFrontStencilMask;
            this.mBackStencilMask  = pBackStencilMask;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.FrontStencilMask = this.mFrontStencilMask;
            pFrameBuffer.Parameters.BackStencilMask  = this.mBackStencilMask;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
