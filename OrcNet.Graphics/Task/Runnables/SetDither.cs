﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set dither runnable sub task class definition.
    /// </summary>
    public class SetDither : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new flag to set indicating whether or not to use dithering.
        /// </summary>
        private bool mUseDither;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetDither"/> class.
        /// </summary>
        /// <param name="pUseDither">The new flag to set indicating whether or not to use dithering.</param>
        public SetDither(bool pUseDither)
        {
            this.mUseDither = pUseDither;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            pFrameBuffer.Parameters.UseDither = this.mUseDither;
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
