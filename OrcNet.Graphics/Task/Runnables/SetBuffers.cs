﻿using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Task.Runnables
{
    /// <summary>
    /// Set Buffers runnable sub task class definition.
    /// </summary>
    public class SetBuffers : ARunnable
    {
        #region Fields

        /// <summary>
        /// Stores the new attachment point to use as read buffer to set.
        /// </summary>
        private BufferId mReadBuffer;

        /// <summary>
        /// Stores the new attachment point to use as draw buffer to set.
        /// </summary>
        private BufferId mDrawBuffer;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(BufferId) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetBuffers"/> class.
        /// </summary>
        /// <param name="pReadBuffer">The new attachment point to use as read buffer to set.</param>
        /// <param name="pDrawBuffer">The new attachment point to use as draw buffer to set.</param>
        public SetBuffers(BufferId pReadBuffer, BufferId pDrawBuffer)
        {
            this.mReadBuffer = pReadBuffer;
            this.mDrawBuffer = pDrawBuffer;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRunnable

        /// <summary>
        /// Executes the subtask.
        /// </summary>
        /// <param name="pFrameBuffer">The frame buffer to use for that subtask.</param>
        public override void Execute(FrameBuffer pFrameBuffer)
        {
            if
                ( this.mReadBuffer != BufferId.NONE )
            {
                pFrameBuffer.UseAsReadBuffer( this.mReadBuffer );
            }

            if
                ( this.mDrawBuffer != BufferId.NONE )
            {
                if
                    ( this.mDrawBuffer == BufferId.COLOR0 ||
                      this.mDrawBuffer == BufferId.COLOR1 ||
                      this.mDrawBuffer == BufferId.COLOR2 ||
                      this.mDrawBuffer == BufferId.COLOR3 ||
                      this.mDrawBuffer == BufferId.COLOR4 ||
                      this.mDrawBuffer == BufferId.COLOR5 ||
                      this.mDrawBuffer == BufferId.COLOR6 ||
                      this.mDrawBuffer == BufferId.COLOR7 )
                {
                    pFrameBuffer.UseSingleDrawBuffer( this.mDrawBuffer );
                }
                else
                {
                    pFrameBuffer.UseMultipleDrawBuffers( this.mDrawBuffer );
                }
            }
        }

        #endregion Methods IRunnable

        #endregion Methods
    }
}
