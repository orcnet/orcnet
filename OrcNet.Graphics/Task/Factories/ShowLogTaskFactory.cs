﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Graphics.Render;
using System.Collections.Generic;
using System.Linq;

namespace OrcNet.Graphics.Task.Factories
{
    /// <summary>
    /// Show log task factory class definition for drawing text info
    /// on the screen.
    /// </summary>
    public class ShowLogTaskFactory : ShowInfoTaskFactory
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowLogTaskFactory"/> class.
        /// </summary>
        private ShowLogTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowLogTaskFactory"/> class.
        /// </summary>
        /// <param name="pFont">The font used to display the text.</param>
        /// <param name="pPass">The pass used to draw the characters.</param>
        /// <param name="pFontHeight">The font height.</param>
        /// <param name="pScreenX">The X component of the screen position of the text.</param>
        /// <param name="pScreenY">The Y component of the screen position of the text.</param>
        /// <param name="pMaxLineCount">The maximum amount of lines of text to display.</param>
        public ShowLogTaskFactory(Text.Font pFont, PipelinePass pPass, float pFontHeight, int pScreenX, int pScreenY, int pMaxLineCount) :
        base( pFont, pPass, System.Drawing.Color.White, pFontHeight, pScreenX, pScreenY, pMaxLineCount )
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Draws the information messages provided by AddOverlayInfo to the screen.
        /// </summary>
        /// <param name="pContext">The draw contextual scene node method if needed.</param>
        public override void Draw(SceneNodeMethod pContext)
        {
            LogManager.Instance.Log("Show logs...");

            ISceneService lSceneManager = ServiceManager.Instance.GetService<ISceneService>();
            if
                ( lSceneManager != null )
            {
                IEnumerable<LogMessage> lLastLogs = LogManager.Instance.CachedLogs;
                int lCount = lLastLogs.Count();
                if
                    ( lCount == 0 )
                {
                    return;
                }

                FrameBuffer lCurrent = lSceneManager.CurrentFrameBuffer as FrameBuffer;
                BlendingDescription lBlending = lCurrent.Parameters.BlendingDescription;
                lBlending.Blend( true, BlendEquationMode.FuncAdd, BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha, BlendEquationMode.FuncAdd, BlendingFactorSrc.Zero, BlendingFactorDest.One );

                sScreenMesh.Clear();

                Viewport lViewport = lCurrent.Parameters.Viewports.First();

                float lScreenX = this.mScreenX;
                float lScreenY = this.mScreenY > 0.0f ? this.mScreenY : lViewport.Height + this.mScreenY - System.Math.Min( lCount, this.mMaximumLineCount ) * this.mFontHeight;
                foreach
                    ( LogMessage lCurr in lLastLogs )
                {
                    string lLine = string.Format( "{0} : {1}", lCurr.Type, lCurr.Message );
                    switch
                        ( lCurr.Type )
                    {
                        case LogType.DEBUG:
                            {
                                this.DrawLine( lViewport, lScreenX, lScreenY, System.Drawing.Color.AliceBlue, lLine );
                            }
                            break;
                        case LogType.INFO:
                            {
                                this.DrawLine( lViewport, lScreenX, lScreenY, System.Drawing.Color.Green, lLine );
                            }
                            break;
                        case LogType.WARNING:
                            {
                                this.DrawLine( lViewport, lScreenX, lScreenY, System.Drawing.Color.Yellow, lLine );
                            }
                            break;
                        default:
                            {
                                // Errors.
                                this.DrawLine( lViewport, lScreenX, lScreenY, System.Drawing.Color.Red, lLine );
                            }
                            break;
                    }
                    lScreenY += this.mFontHeight;
                }

                lCurrent.Draw( this.mFontPass, sScreenMesh );
                lBlending.Blend( false );
            }
        }
        
        #endregion Methods
    }
}
