﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Text;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace OrcNet.Graphics.Task.Factories
{
    /// <summary>
    /// Show info task factory class definition for drawing text info
    /// on the screen.
    /// </summary>
    public class ShowInfoTaskFactory : ATaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the mesh used to draw character quads in order to display text.
        /// </summary>
        protected static Mesh<FontVertex, uint> sScreenMesh;

        /// <summary>
        /// Stores the infos displayed on the screen by topic. (e.g. FPS, Frame count, so on...)
        /// </summary>
        private static Dictionary<string, string> sScreenInfos;

        /// <summary>
        /// Stores the pass used to draw the characters.
        /// </summary>
        protected PipelinePass mFontPass;

        /// <summary>
        /// Stores the font pass uniform used to control the font texture.
        /// </summary>
        private TextureUniform mFontUniform;

        /// <summary>
        /// Stores the font used to display the text.
        /// </summary>
        private Text.Font mTextFont;

        /// <summary>
        /// Stores the font color in RGBA8 format.
        /// </summary>
        private Color mFontColor;

        /// <summary>
        /// Stores the font height.
        /// </summary>
        protected float mFontHeight;

        /// <summary>
        /// Stores the X component of the screen position of the text.
        /// </summary>
        protected int mScreenX;

        /// <summary>
        /// Stores the Y component of the screen position of the text.
        /// </summary>
        protected int mScreenY;

        /// <summary>
        /// Stores the maximum amount of lines of text to display.
        /// </summary>
        protected int mMaximumLineCount;

        /// <summary>
        /// Stores the current frame rate.
        /// </summary>
        private int mFPS;

        /// <summary>
        /// Stores the number of frames displayed since the relative task started.
        /// </summary>
        private int mFrameCount;

        /// <summary>
        /// Stores the time at which the frame counter was reset to zero.
        /// </summary>
        private double mStartTime;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(float);
                lSize += sizeof(int) * 5;
                lSize += sizeof(double);
                lSize += sizeof(byte) * 4; // Color.
                if
                    ( this.mFontPass != null )
                {
                    lSize += this.mFontPass.Size;
                }
                if
                    ( this.mFontUniform != null )
                {
                    lSize += this.mFontUniform.Size;
                }
                if
                    ( this.mTextFont != null )
                {
                    lSize += this.mTextFont.Size;
                }

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="ShowInfoTaskFactory"/> class.
        /// </summary>
        static ShowInfoTaskFactory()
        {
            sScreenMesh = new Mesh<FontVertex, uint>( PrimitiveType.Triangles, MeshUsage.CPU );
            sScreenMesh.AddAttributeType( 0, 4, AttributeType.HALF_BASED, false );
            sScreenMesh.AddAttributeType( 1, 4, AttributeType.BYTE_BASED, true );
            sScreenInfos = new Dictionary<string, string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowInfoTaskFactory"/> class.
        /// </summary>
        protected ShowInfoTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowInfoTaskFactory"/> class.
        /// </summary>
        /// <param name="pFont">The font used to display the text.</param>
        /// <param name="pPass">The pass used to draw the characters.</param>
        /// <param name="pColor">The font color in RGBA8 format.</param>
        /// <param name="pFontHeight">The font height.</param>
        /// <param name="pScreenX">The X component of the screen position of the text.</param>
        /// <param name="pScreenY">The Y component of the screen position of the text.</param>
        /// <param name="pMaxLineCount">The maximum amount of lines of text to display.</param>
        public ShowInfoTaskFactory(Text.Font pFont, PipelinePass pPass, Color pColor, float pFontHeight, int pScreenX, int pScreenY, int pMaxLineCount)
        {
            this.mFPS = 0;
            this.mFrameCount = 0;
            this.mStartTime = 0.0f;
            this.mFontPass = pPass;
            this.mFontUniform = this.mFontPass.GetUniform<TextureUniform>( new UniformName( "font" ) );
            this.mTextFont = pFont;
            this.mFontColor = pColor;
            this.mScreenX = pScreenX;
            this.mScreenY = pScreenY;
            this.mMaximumLineCount = pMaxLineCount;
            this.mFontHeight = pFontHeight;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Draws the information messages provided by AddOverlayInfo to the screen.
        /// </summary>
        /// <param name="pContext">The draw contextual scene node method if needed.</param>
        public virtual void Draw(SceneNodeMethod pContext)
        {
            LogManager.Instance.Log("Show infos...");

            ISceneService lSceneManager = ServiceManager.Instance.GetService<ISceneService>();
            if ( lSceneManager != null )
            {
                FrameBuffer lCurrent = lSceneManager.CurrentFrameBuffer as FrameBuffer;
                BlendingDescription lBlending = lCurrent.Parameters.BlendingDescription;
                lBlending.Blend( true, BlendEquationMode.FuncAdd, BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha, BlendEquationMode.FuncAdd, BlendingFactorSrc.Zero, BlendingFactorDest.One );

                Viewport lViewport = lCurrent.Parameters[ new FrameBufferParameters.ViewportIndex( 0 ) ];

                float lScreenX = this.mScreenX;
                float lScreenY = this.mScreenY > 0.0f ? this.mScreenY : lViewport.Height + this.mScreenY - this.mMaximumLineCount * this.mFontHeight;

                this.mFrameCount++;

                double lTime  = lSceneManager.AbsoluteTime;
                double lDelay = (lTime - this.mStartTime) * 1e-6;
                if ( lDelay > 1.0 )
                {
                    this.mFPS = (int)(this.mFrameCount / lDelay);
                    this.mFrameCount = 0;
                    this.mStartTime = lTime;
                }
                else if ( lDelay < 0.0f )
                {
                    // Happens when replaying recorded events.
                    this.mFPS = 0;
                    this.mFrameCount = 0;
                    this.mStartTime = lTime;
                }

                sScreenMesh.Clear();

                StringBuilder lBuilder = new StringBuilder();
                string lFPSInfo;
                if ( sScreenInfos.TryGetValue( "FPS", out lFPSInfo ) )
                {
                    lBuilder.Append( lFPSInfo );
                    lBuilder.Append( " FPS." );
                }
                else
                {
                    lBuilder.Append( this.mFPS );
                    lBuilder.Append( " FPS." );
                }

                this.DrawLine( lViewport, lScreenX, lScreenY, this.mFontColor, lBuilder.ToString() );
                lScreenY += this.mFontHeight;

                foreach ( KeyValuePair<string, string> lInfo in sScreenInfos )
                {
                    if ( lInfo.Key != "FPS" &&
                         string.IsNullOrEmpty( lInfo.Value ) == false )
                    {
                        this.DrawLine( lViewport, lScreenX, lScreenY, this.mFontColor, lInfo.Value );
                        lScreenY += this.mFontHeight;
                    }
                }

                sScreenInfos.Clear();

                this.mFontUniform.Value = this.mTextFont.Image;
                lCurrent.Draw( this.mFontPass, sScreenMesh );

                lBlending.Blend( false );
            }
        }

        #region Methods Internal

        /// <summary>
        /// Adds an overlay info to the screen. The information is mapped to a topic and will replace
        /// the previous info of that same topic if any. Therefore, all topics are cleaned each frame, so
        /// those infos must be set each frame to persist.
        /// </summary>
        /// <param name="pTopic">The topic the info to display is mapped with.</param>
        /// <param name="pInfoToDisplay">The info to display to the screen.</param>
        protected void AddOverlayInfo(string pTopic, string pInfoToDisplay)
        {
            sScreenInfos[ pTopic ] = pInfoToDisplay;
        }

        /// <summary>
        /// Draws a new line to the screen at the given screen position using the supplied color.
        /// </summary>
        /// <param name="pViewport">The frame buffer viewport in pixels.</param>
        /// <param name="pScreenX">The X component of the screen position the first character must be displayed at.</param>
        /// <param name="pScreenY">The Y component of the screen position the first character must be displayed at.</param>
        /// <param name="pLineColor">The color of the line of text in RGBA8 format.</param>
        /// <param name="pLineToDisplay">The line of text to display.</param>
        protected void DrawLine(Viewport pViewport, float pScreenX, float pScreenY, System.Drawing.Color pLineColor, string pLineToDisplay)
        {
            this.mTextFont.AddLine( pViewport, pScreenX, pScreenY, pLineToDisplay, this.mFontHeight, pLineColor, sScreenMesh );
        }

        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(ShowInfoTaskFactory pOther)
        {
            Utilities.Swap( ref this.mFontPass, ref pOther.mFontPass );
            Utilities.Swap( ref this.mFontUniform, ref pOther.mFontUniform );
            Utilities.Swap( ref this.mTextFont, ref pOther.mTextFont );
            Utilities.Swap( ref this.mFontColor, ref pOther.mFontColor );
            Utilities.Swap( ref this.mFontHeight, ref pOther.mFontHeight );
            Utilities.Swap( ref this.mScreenX, ref pOther.mScreenX );
            Utilities.Swap( ref this.mScreenY, ref pOther.mScreenY );
            Utilities.Swap( ref this.mMaximumLineCount, ref pOther.mMaximumLineCount );
            Utilities.Swap( ref this.mFPS, ref pOther.mFPS );
            Utilities.Swap( ref this.mFrameCount, ref pOther.mFrameCount );
            Utilities.Swap( ref this.mStartTime, ref pOther.mStartTime );
        }
        
        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            SceneNodeMethod lMethod = pContext as SceneNodeMethod;
            return new ShowInfoTask( lMethod, this );
        }

        #endregion Methods ATaskFactory

        #endregion Methods
    }
}
