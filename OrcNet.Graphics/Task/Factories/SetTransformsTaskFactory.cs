﻿using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.SceneGraph;
using System;

namespace OrcNet.Graphics.Task.Factories
{
    /// <summary>
    /// Set transforms task factory class definition in charge of 
    /// sending the transformation matrices for a pass.
    /// </summary>
    public class SetTransformsTaskFactory : ATaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the last used pass the uniforms come from.
        /// </summary>
        private PipelinePass mLastPass;

        /// <summary>
        /// Stores the pipeline description uniforms come from.
        /// </summary>
        private PipelineDescription mPipelineDescription;

        /// <summary>
        /// Stores the screen node to be used for transformations involving the screen space.
        /// </summary>
        private ScreenQualifier mScreen;

        /// <summary>
        /// Stores the pipeline description qualifier owning uniforms to update.
        /// </summary>
        private PipelineQualifier mDescription;

        /// <summary>
        /// Stores the time uniform.
        /// </summary>
        private Vector2FUniform mTimeUniform;

        /// <summary>
        /// Stores the local to world uniform.
        /// </summary>
        private Matrix4FUniform mLocalToWorldUniform;

        /// <summary>
        /// Stores the local to screen uniform.
        /// </summary>
        private Matrix4FUniform mLocalToScreenUniform;

        /// <summary>
        /// Stores the camera to world uniform.
        /// </summary>
        private Matrix4FUniform mCameraToWorldUniform;

        /// <summary>
        /// Stores the camera to screen uniform.
        /// </summary>
        private Matrix4FUniform mCameraToScreenUniform;

        /// <summary>
        /// Stores the screen to camera uniform.
        /// </summary>
        private Matrix4FUniform mScreenToCameraUniform;

        /// <summary>
        /// Stores the world to screen uniform.
        /// </summary>
        private Matrix4FUniform mWorldToScreenUniform;

        /// <summary>
        /// Stores the world position uniform.
        /// </summary>
        private Vector3FUniform mWorldPosition;

        /// <summary>
        /// Stores the world direction uniform.
        /// </summary>
        private Vector3FUniform mWorldDirection;

        /// <summary>
        /// Stores the uniform name containing the time of the current frame and
        /// the elapsed time since the last frame.
        /// </summary>
        private string mTimeUniformName;

        /// <summary>
        /// Stores the uniform name containing the local to world transformation.
        /// </summary>
        private string mLocalToWorldUniformName;

        /// <summary>
        /// Stores the uniform name containing the local to screen transformation.
        /// </summary>
        private string mLocalToScreenUniformName;

        /// <summary>
        /// Stores the uniform name containing the camera to world transformation.
        /// </summary>
        private string mCameraToWorldUniformName;

        /// <summary>
        /// Stores the uniform name containing the camera to screen transformation.
        /// </summary>
        private string mCameraToScreenUniformName;

        /// <summary>
        /// Stores the uniform name containing the screen to camera transformation.
        /// </summary>
        private string mScreenToCameraUniformName;

        /// <summary>
        /// Stores the uniform name containing the world to screen transformation.
        /// </summary>
        private string mWorldToScreenUniformName;

        /// <summary>
        /// Stores the uniform name containing the world coordinates of the origin of the local frame.
        /// </summary>
        private string mWorldOriginUniformName;

        /// <summary>
        /// Stores the uniform name containing the world coordinates of the unit Z unit vector of the local frame.
        /// </summary>
        private string mWorldZUnitUniformName;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                if ( this.mScreen != null )
                {
                    lSize += this.mScreen.Size;
                }
                if ( this.mDescription != null )
                {
                    lSize += this.mDescription.Size;
                }
                if ( string.IsNullOrEmpty(this.mTimeUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mTimeUniformName.Length;
                }
                if ( string.IsNullOrEmpty(this.mLocalToWorldUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mLocalToWorldUniformName.Length;
                }
                if ( string.IsNullOrEmpty(this.mLocalToScreenUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mLocalToScreenUniformName.Length;
                }
                if ( string.IsNullOrEmpty(this.mCameraToWorldUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mCameraToWorldUniformName.Length;
                }
                if ( string.IsNullOrEmpty(this.mCameraToScreenUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mCameraToScreenUniformName.Length;
                }
                if ( string.IsNullOrEmpty(this.mScreenToCameraUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mScreenToCameraUniformName.Length;
                }
                if ( string.IsNullOrEmpty(this.mWorldToScreenUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mWorldToScreenUniformName.Length;
                }
                if ( string.IsNullOrEmpty(this.mWorldOriginUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mWorldOriginUniformName.Length;
                }
                if ( string.IsNullOrEmpty(this.mWorldZUnitUniformName) == false )
                {
                    lSize += sizeof(char) * (uint)this.mWorldZUnitUniformName.Length;
                }
                
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the screen node to be used for transformations involving the screen space.
        /// </summary>
        internal ScreenQualifier Screen
        {
            get
            {
                return this.mScreen;
            }
        }

        /// <summary>
        /// Gets or sets the last used pass the uniforms come from.
        /// </summary>
        internal PipelinePass LastPass
        {
            get
            {
                return this.mLastPass;
            }
            set
            {
                this.mLastPass = value;
            }
        }

        /// <summary>
        /// Gets the pipeline description uniforms come from.
        /// </summary>
        internal PipelineDescription PipelineDescription
        {
            get
            {
                return this.mPipelineDescription;
            }
        }

        /// <summary>
        /// Gets or sets the time uniform.
        /// </summary>
        internal Vector2FUniform TimeUniform
        {
            get
            {
                return this.mTimeUniform;
            }
            set
            {
                this.mTimeUniform = value;
            }
        }

        /// <summary>
        /// Gets or sets the local to world uniform.
        /// </summary>
        internal Matrix4FUniform LocalToWorldUniform
        {
            get
            {
                return this.mLocalToWorldUniform;
            }
            set
            {
                this.mLocalToWorldUniform = value;
            }
        }

        /// <summary>
        /// Gets or sets the local to screen uniform.
        /// </summary>
        internal Matrix4FUniform LocalToScreenUniform
        {
            get
            {
                return this.mLocalToScreenUniform;
            }
            set
            {
                this.mLocalToScreenUniform = value;
            }
        }

        /// <summary>
        /// Gets or sets the camera to world uniform.
        /// </summary>
        internal Matrix4FUniform CameraToWorldUniform
        {
            get
            {
                return this.mCameraToWorldUniform;
            }
            set
            {
                this.mCameraToWorldUniform = value;
            }
        }

        /// <summary>
        /// Gets or sets the camera to screen uniform.
        /// </summary>
        internal Matrix4FUniform CameraToScreenUniform
        {
            get
            {
                return this.mCameraToScreenUniform;
            }
            set
            {
                this.mCameraToScreenUniform = value;
            }
        }

        /// <summary>
        /// Gets or sets the screen to camera uniform.
        /// </summary>
        internal Matrix4FUniform ScreenToCameraUniform
        {
            get
            {
                return this.mScreenToCameraUniform;
            }
            set
            {
                this.mScreenToCameraUniform = value;
            }
        }

        /// <summary>
        /// Gets or sets the world to screen uniform.
        /// </summary>
        internal Matrix4FUniform WorldToScreenUniform
        {
            get
            {
                return this.mWorldToScreenUniform;
            }
            set
            {
                this.mWorldToScreenUniform = value;
            }
        }

        /// <summary>
        /// Gets or sets the world position uniform.
        /// </summary>
        internal Vector3FUniform WorldPosition
        {
            get
            {
                return this.mWorldPosition;
            }
            set
            {
                this.mWorldPosition = value;
            }
        }

        /// <summary>
        /// Gets or sets the world direction uniform.
        /// </summary>
        internal Vector3FUniform WorldDirection
        {
            get
            {
                return this.mWorldDirection;
            }
            set
            {
                this.mWorldDirection = value;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the time of the current frame and
        /// the elapsed time since the last frame.
        /// </summary>
        internal string TimeUniformName
        {
            get
            {
                return this.mTimeUniformName;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the local to world transformation.
        /// </summary>
        internal string LocalToWorldUniformName
        {
            get
            {
                return this.mLocalToWorldUniformName;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the local to screen transformation.
        /// </summary>
        internal string LocalToScreenUniformName
        {
            get
            {
                return this.mLocalToScreenUniformName;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the camera to world transformation.
        /// </summary>
        internal string CameraToWorldUniformName
        {
            get
            {
                return this.mCameraToWorldUniformName;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the camera to screen transformation.
        /// </summary>
        internal string CameraToScreenUniformName
        {
            get
            {
                return this.mCameraToScreenUniformName;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the screen to camera transformation.
        /// </summary>
        internal string ScreenToCameraUniformName
        {
            get
            {
                return this.mScreenToCameraUniformName;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the world to screen transformation.
        /// </summary>
        internal string WorldToScreenUniformName
        {
            get
            {
                return this.mWorldToScreenUniformName;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the world coordinates of the origin of the local frame.
        /// </summary>
        internal string WorldOriginUniformName
        {
            get
            {
                return this.mWorldOriginUniformName;
            }
        }

        /// <summary>
        /// Gets the uniform name containing the world coordinates of the unit Z unit vector of the local frame.
        /// </summary>
        internal string WorldZUnitUniformName
        {
            get
            {
                return this.mWorldZUnitUniformName;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTransformsTaskFactory"/> class.
        /// </summary>
        private SetTransformsTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTransformsTaskFactory"/> class.
        /// </summary>
        /// <param name="pScreen">The screen node to be used for transformations involving the screen space.</param>
        /// <param name="pDescription">The pipeline description owning uniforms to update.</param>
        /// <param name="pTimeUniformName">The uniform name containing the time of the current frame and the elapsed time since the last frame.</param>
        /// <param name="pLocalToWorldUniformName">The uniform name containing the local to world transformation.</param>
        /// <param name="pLocalToScreenUniformName">The uniform name containing the local to screen transformation.</param>
        /// <param name="pCameraToWorldUniformName">The uniform name containing the camera to world transformation.</param>
        /// <param name="pCameraToScreenUniformName">The uniform name containing the camera to screen transformation.</param>
        /// <param name="pScreenToCameraUniformName">The uniform name containing the screen to camera transformation.</param>
        /// <param name="pWorldToScreenUniformName">The uniform name containing the world to screen transformation.</param>
        /// <param name="pWorldOriginUniformName">The uniform name containing the world coordinates of the origin of the local frame.</param>
        /// <param name="pWorldZUnitUniformName">The uniform name containing the world coordinates of the unit Z unit vector of the local frame.</param>
        public SetTransformsTaskFactory(ScreenQualifier pScreen, PipelineQualifier pDescription, string pTimeUniformName, string pLocalToWorldUniformName, string pLocalToScreenUniformName, 
                                                                                                 string pCameraToWorldUniformName, string pCameraToScreenUniformName, string pScreenToCameraUniformName,
                                                                                                 string pWorldToScreenUniformName, string pWorldOriginUniformName, string pWorldZUnitUniformName)
        {
            this.mScreen = pScreen;
            this.mDescription = pDescription;
            this.mTimeUniformName = pTimeUniformName;
            this.mLocalToWorldUniformName = pLocalToWorldUniformName;
            this.mLocalToScreenUniformName = pLocalToScreenUniformName;
            this.mCameraToWorldUniformName = pCameraToWorldUniformName;
            this.mCameraToScreenUniformName = pCameraToScreenUniformName;
            this.mScreenToCameraUniformName = pScreenToCameraUniformName;
            this.mWorldToScreenUniformName = pWorldToScreenUniformName;
            this.mWorldOriginUniformName = pWorldOriginUniformName;
            this.mWorldZUnitUniformName = pWorldZUnitUniformName;

            this.mTimeUniform = null;
            this.mLocalToWorldUniform = null;
            this.mLocalToScreenUniform = null;
            this.mCameraToWorldUniform = null;
            this.mCameraToScreenUniform = null;
            this.mScreenToCameraUniform = null;
            this.mWorldToScreenUniform = null;
            this.mWorldPosition = null;
            this.mWorldDirection = null;
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal
        
        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(SetTransformsTaskFactory pOther)
        {
            Utilities.Swap( ref this.mScreen, ref pOther.mScreen );
            Utilities.Swap( ref this.mDescription, ref pOther.mDescription );
            Utilities.Swap( ref this.mTimeUniformName, ref pOther.mTimeUniformName );
            Utilities.Swap( ref this.mLocalToWorldUniformName, ref pOther.mLocalToWorldUniformName );
            Utilities.Swap( ref this.mLocalToScreenUniformName, ref pOther.mLocalToScreenUniformName );
            Utilities.Swap( ref this.mCameraToWorldUniformName, ref pOther.mCameraToWorldUniformName );
            Utilities.Swap( ref this.mCameraToScreenUniformName, ref pOther.mCameraToScreenUniformName );
            Utilities.Swap( ref this.mScreenToCameraUniformName, ref pOther.mScreenToCameraUniformName );
            Utilities.Swap( ref this.mWorldToScreenUniformName, ref pOther.mWorldToScreenUniformName );
            Utilities.Swap( ref this.mWorldOriginUniformName, ref pOther.mWorldOriginUniformName );
            Utilities.Swap( ref this.mWorldZUnitUniformName, ref pOther.mWorldZUnitUniformName );

            if
                ( this.mLastPass != null )
            {
                this.mTimeUniform = string.IsNullOrEmpty( this.mTimeUniformName ) ? null : this.mLastPass.GetUniform<Vector2FUniform>( new UniformName( this.mTimeUniformName ) );
                this.mLocalToWorldUniform = string.IsNullOrEmpty( this.mLocalToWorldUniformName ) ? null : this.mLastPass.GetUniform<Matrix4FUniform>( new UniformName( this.mLocalToWorldUniformName ) );
                this.mLocalToScreenUniform = string.IsNullOrEmpty( this.mLocalToScreenUniformName ) ? null : this.mLastPass.GetUniform<Matrix4FUniform>( new UniformName( this.mLocalToScreenUniformName ) );
                this.mCameraToWorldUniform = string.IsNullOrEmpty( this.mCameraToWorldUniformName ) ? null : this.mLastPass.GetUniform<Matrix4FUniform>( new UniformName( this.mCameraToWorldUniformName ) );
                this.mCameraToScreenUniform = string.IsNullOrEmpty( this.mCameraToScreenUniformName ) ? null : this.mLastPass.GetUniform<Matrix4FUniform>( new UniformName( this.mCameraToScreenUniformName ) );
                this.mScreenToCameraUniform = string.IsNullOrEmpty( this.mScreenToCameraUniformName ) ? null : this.mLastPass.GetUniform<Matrix4FUniform>( new UniformName( this.mScreenToCameraUniformName ) );
                this.mWorldToScreenUniform = string.IsNullOrEmpty( this.mWorldToScreenUniformName ) ? null : this.mLastPass.GetUniform<Matrix4FUniform>( new UniformName( this.mWorldToScreenUniformName ) );
                this.mWorldPosition = string.IsNullOrEmpty( this.mWorldOriginUniformName ) ? null : this.mLastPass.GetUniform<Vector3FUniform>( new UniformName( this.mWorldOriginUniformName ) );
                this.mWorldDirection = string.IsNullOrEmpty( this.mWorldZUnitUniformName ) ? null : this.mLastPass.GetUniform<Vector3FUniform>( new UniformName( this.mWorldZUnitUniformName ) );
            }
        }

        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            SceneNodeMethod lMethod = pContext as SceneNodeMethod;
            ISceneNode lTarget = lMethod.Target;

            SceneNode lScreenNode = null;
            if ( string.IsNullOrEmpty( this.mLocalToScreenUniformName ) ||
                 string.IsNullOrEmpty( this.mWorldToScreenUniformName ) )
            {
                if ( this.mScreen.Target.Length > 0 )
                {
                    lScreenNode = this.mScreen.GetTarget( lTarget ) as SceneNode;
                    if ( lScreenNode == null )
                    {
                        LogManager.Instance.Log( "SetTransforms: cannot find the screen node!!!", LogType.ERROR );
                    }
                }
            }

            if ( this.mDescription.Target.Length > 0 &&
                 this.mPipelineDescription == null )
            {
                SceneNode lModuleOwner = this.mDescription.GetTarget( lTarget ) as SceneNode;
                this.mPipelineDescription = lModuleOwner.GetPipelineDescription( this.mDescription.Name ) as PipelineDescription;
                if ( this.mPipelineDescription == null )
                {
                    LogManager.Instance.Log( string.Format( "SetTransforms: cannot find the {0}.{1} pipeline description.", this.mDescription.Target, this.mDescription.Name ), LogType.ERROR );
                }
            }
            else if ( this.mDescription.Name.Length > 0 &&
                       this.mPipelineDescription == null )
            {
                IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                if ( lResourceService != null )
                {
                    IResource lResource = lResourceService.LoadResource( this.mDescription.Name );
                    if ( lResource != null )
                    {
                        this.mPipelineDescription = lResource.OwnedObject as PipelineDescription;
                    }
                }

                if ( this.mPipelineDescription == null )
                {
                    LogManager.Instance.Log( string.Format( "SetTransforms: cannot find the {0} pipeline description.", this.mDescription.Name ), LogType.ERROR );
                }
            }

            return new SetTransformsTask( lScreenNode, lTarget as SceneNode, this );
        }

        #endregion Methods ATaskFactory

        #endregion Methods
    }
}
