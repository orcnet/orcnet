﻿using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Render;
using OrcNet.Core.Resource;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.SceneGraph;
using System;

namespace OrcNet.Graphics.Task.Factories
{
    /// <summary>
    /// Set pass task factory class definition for being used to draw
    /// afterward.
    /// </summary>
    public class SetPassTaskFactory : ATaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the pipeline descriptions of the pass to be set. Each description is specified by a [Target].[PipelineDescription] qualifier.
        /// </summary>
        private PipelineQualifier[] mDescriptions;

        /// <summary>
        /// Stores the flag indicating whether or not to set uniforms of the pass using values defined in the scene node from which the task is called.
        /// </summary>
        private bool mSetUniforms;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                if
                    ( this.mDescriptions != null )
                {
                    for
                        ( int lCurr = 0; lCurr < this.mDescriptions.Length; lCurr++ )
                    {
                        lSize += this.mDescriptions[ lCurr ].Size;
                    }
                }
                
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPassTaskFactory"/> class.
        /// </summary>
        private SetPassTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPassTaskFactory"/> class.
        /// </summary>
        /// <param name="pDescriptions">The pipeline descriptions of the pass to be set. Each description is specified by a [Target].[PipelineDescription] qualifier.</param>
        /// <param name="pSetUniforms">The flag indicating whether or not to set uniforms of the pass using values defined in the scene node from which the task is called.</param>
        public SetPassTaskFactory(PipelineQualifier[] pDescriptions, bool pSetUniforms)
        {
            this.mDescriptions = pDescriptions;
            this.mSetUniforms  = pSetUniforms;
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal
        
        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(SetPassTaskFactory pOther)
        {
            Utilities.Swap( ref this.mDescriptions, ref pOther.mDescriptions );
            Utilities.Swap( ref this.mSetUniforms, ref pOther.mSetUniforms );
        }

        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            SceneNodeMethod lMethod = pContext as SceneNodeMethod;
            ISceneNode lTarget = lMethod.Target;

            string lName = null;
            PipelinePass lPass = null;
            ISceneService lSceneManager = ServiceManager.Instance.GetService<ISceneService>();
            if ( lSceneManager != null )
            {
                try
                {
                    for ( int lCurr = 0; lCurr < this.mDescriptions.Length; lCurr++ )
                    {
                        PipelineQualifier lQualifier = this.mDescriptions[ lCurr ];
                        SceneNode lCurrTarget = lQualifier.GetTarget( lTarget ) as SceneNode;
                        if ( lCurrTarget == null )
                        {
                            lName = lName + lQualifier.Name + ";";
                        }
                        else
                        {
                            IPipelineDescription lDescription = lCurrTarget.GetPipelineDescription( lQualifier.Name );
                            lName = lName + lDescription.Creator.Name + ";";
                        }
                    }

                    IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                    if ( lResourceService != null )
                    {
                        IResource lResource = lResourceService.LoadResource( lName );
                        if ( lResource != null )
                        {
                            lPass = lResource.OwnedObject as PipelinePass;
                        }
                    }
                }
                catch
                {
                    LogManager.Instance.Log( "SetPass: cannot find the pass to set!!!", LogType.ERROR );
                }
            }

            return new SetPassTask( lPass, this.mSetUniforms ? lTarget as SceneNode : null );
        }

        #endregion Methods ATaskFactory

        #endregion Methods
    }
}
