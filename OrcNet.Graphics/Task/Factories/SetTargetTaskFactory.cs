﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Render.Uniforms.Values;
using OrcNet.Graphics.SceneGraph;
using OrcNet.Graphics.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OrcNet.Graphics.Task.Factories
{
    /// <summary>
    /// Set target task factory class definition in charge of 
    /// modifying the attachments of a frame buffer.
    /// </summary>
    public class SetTargetTaskFactory : ATaskFactory
    {
        #region Fields
        
        /// <summary>
        /// Stores the frame buffer attachments to be set.
        /// </summary>
        private List<AttachmentTarget> mTargets;

        /// <summary>
        /// Stores the flag indicating whether the target textures must be 
        /// automatically resized to the default frame buffer viewport or not.
        /// </summary>
        private bool mIsAutoResized;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                if
                    ( this.mTargets != null )
                {
                    for
                        ( int lCurr = 0; lCurr < this.mTargets.Count; lCurr++ )
                    {
                        lSize += this.mTargets[ lCurr ].Size;
                    }
                }
                
                lSize += sizeof(bool);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the target at the given index.
        /// </summary>
        /// <param name="pIndex">The index of the target to get.</param>
        /// <returns>The target.</returns>
        public AttachmentTarget this[int pIndex]
        {
            get
            {
                return this.mTargets[ pIndex ];
            }
        }

        #endregion Properties

        #region Constructor
        
        /// <summary>
        /// Initializes a new instance of the <see cref="SetTargetTaskFactory"/> class.
        /// </summary>
        private SetTargetTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTargetTaskFactory"/> class.
        /// </summary>
        /// <param name="pTargets"></param>
        /// <param name="pIsAutoResized"></param>
        public SetTargetTaskFactory(List<AttachmentTarget> pTargets, bool pIsAutoResized)
        {
            this.mTargets = pTargets;
            this.mIsAutoResized = pIsAutoResized;
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal
        
        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(SetTargetTaskFactory pOther)
        {
            Utilities.Swap( ref this.mTargets, ref pOther.mTargets );
            Utilities.Swap( ref this.mIsAutoResized, ref pOther.mIsAutoResized );
        }

        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            List<ITexture> lTextures = new List<ITexture>();
            SceneNodeMethod lMethod = pContext as SceneNodeMethod;
            ISceneNode lTarget = lMethod.Target;
            
            try
            {
                for ( int lCurr = 0; lCurr < this.mTargets.Count; lCurr++ )
                {
                    ITexture lTexture = null;
                    AttachmentTarget lAttachment = this.mTargets[ lCurr ];
                    string lName = lAttachment.Texture.Name;
                    SceneNode lOwner = lAttachment.Texture.GetTarget( lTarget ) as SceneNode;
                    if ( lOwner != null )
                    {
                        int lIndex = lName.IndexOf( ':' );
                        if ( lIndex == -1 )
                        {
                            TextureValue lValue = lOwner.GetValue( lName ) as TextureValue;
                            if ( lValue != null )
                            {
                                lTexture = lValue.Value;
                            }
                        }
                        else
                        {
                            PipelineDescription lDescription = lOwner.GetPipelineDescription( lName.Substring( 0, lIndex ) ) as PipelineDescription;
                            PipelinePass lPass = lDescription.Users.FirstOrDefault();
                            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( lName.Substring( lIndex + 1 ) ) );
                            if ( lUniform != null )
                            {
                                lTexture = lUniform.Value;
                            }
                        }
                    }
                    else
                    {
                        IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                        if ( lResourceService != null )
                        {
                            IResource lResource = lResourceService.LoadResource( lName );
                            if ( lResource != null )
                            {
                                lTexture = lResource.OwnedObject as ITexture;
                            }
                        }
                    }

                    if ( lTexture != null )
                    {
                        lTextures.Add( lTexture );
                    }
                    else
                    {
                        LogManager.Instance.Log( string.Format( "Cannot retrieve the needed texture {0}", lName ) );
                    }

                    if ( this.mIsAutoResized )
                    {
                        IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                        if ( lRenderService != null )
                        {
                            FrameBuffer lBufferUsed = lRenderService.DefaultFrameBuffer;
                            Viewport lViewport = lBufferUsed.Parameters[ new FrameBufferParameters.ViewportIndex( 0 ) ];
                            Texture2D lTextureCast = lTexture as Texture2D;
                            if ( lTextureCast != null )
                            {
                                int lWidth  = (int)lViewport.Width;
                                int lHeight = (int)lViewport.Height;
                                if ( lTextureCast.Width != lWidth ||
                                     lTextureCast.Height != lHeight )
                                {
                                    LogManager.Instance.Log( string.Format( "Resizing framebuffer render buffer {0} autresizing to {1} | {2}.", lAttachment.AttachmentPoint, lWidth, lHeight ), LogType.WARNING );
                                    lTextureCast.SetTexture( lWidth, lHeight, lTextureCast.Format, PixelType.Float, new CPUBuffer( IntPtr.Zero ) );
                                }
                            }
                        }
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return new SetTargetTask( this, lTextures );
        }

        #endregion Methods ATaskFactory

        #endregion Methods
    }
}
