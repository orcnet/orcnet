﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Helpers;
using OrcNet.Core.Math;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Task.Runnables;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace OrcNet.Graphics.Task.Factories
{
    /// <summary>
    /// Set state task factory class definition for being used to draw
    /// afterward.
    /// </summary>
    public class SetStateTaskFactory : ATaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the subtasks of this task.
        /// </summary>
        private List<IRunnable> mSubtasks;
        
        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                for
                    ( int lCurr = 0; lCurr < this.mSubtasks.Count; lCurr++ )
                {
                    lSize += this.mSubtasks[ lCurr ].Size;
                }
                
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetStateTaskFactory"/> class.
        /// </summary>
        public SetStateTaskFactory()
        {
            this.mSubtasks = new List<IRunnable>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Set viewport runnable sub task.
        /// </summary>
        /// <param name="pViewport">The new viewport to the set.</param>
        public void SetViewport(Viewport pViewport)
        {
            this.AddRunnable( new SetViewport( pViewport ) );
        }

        /// <summary>
        /// Set depth range runnable sub task
        /// </summary>
        /// <param name="pRange">The new depth range to set.</param>
        public void SetDepthRange(DepthRange pRange)
        {
            this.AddRunnable( new SetDepthRange( pRange ) );
        }

        /// <summary>
        /// Set clip distances runnable sub task
        /// </summary>
        /// <param name="pClipDistances">The new clip distances to set.</param>
        public void SetClipDistances(int pClipDistances)
        {
            this.AddRunnable( new SetClipDistances( pClipDistances ) );
        }

        /// <summary>
        /// Set clear color runnable sub task
        /// </summary>
        /// <param name="pClearColor">The new clear color to set.</param>
        public void SetClearColor(Color pClearColor)
        {
            this.AddRunnable( new SetClearColor( pClearColor ) );
        }

        /// <summary>
        /// Set clear depth runnable sub task
        /// </summary>
        /// <param name="pClearDepth">The new clear depth to set.</param>
        public void SetClearDepth(float pClearDepth)
        {
            this.AddRunnable( new SetClearDepth( pClearDepth ) );
        }

        /// <summary>
        /// Set clear stencil runnable sub task
        /// </summary>
        /// <param name="pClearStencil">The new clear stencil to set.</param>
        public void SetClearStencil(int pClearStencil)
        {
            this.AddRunnable( new SetClearStencil( pClearStencil ) );
        }

        /// <summary>
        /// Set point size runnable sub task
        /// </summary>
        /// <param name="pPointSize">The new point size to set.</param>
        public void SetPointSize(float pPointSize)
        {
            this.AddRunnable( new SetPointSize( pPointSize ) );
        }

        /// <summary>
        /// Set point fade threshold size runnable sub task
        /// </summary>
        /// <param name="pFadeThresholdSize">The new point fade threshold size to set.</param>
        public void SetPointFadeThresholdSize(float pFadeThresholdSize)
        {
            this.AddRunnable( new SetPointFadeThresholdSize( pFadeThresholdSize ) );
        }

        /// <summary>
        /// Set has point lower left origin runnable sub task
        /// </summary>
        /// <param name="pHasPointLowerLeftOrigin">The new flag determining whether there is a point lower left origin or not.</param>
        public void SetHasPointLowerLeftOrigin(bool pHasPointLowerLeftOrigin)
        {
            this.AddRunnable( new SetHasPointLowerLeftOrigin( pHasPointLowerLeftOrigin ) );
        }

        /// <summary>
        /// Set Line width runnable sub task
        /// </summary>
        /// <param name="pLineWidth">The new line width to set.</param>
        public void SetLineWidth(float pLineWidth)
        {
            this.AddRunnable( new SetLineWidth( pLineWidth ) );
        }

        /// <summary>
        /// Set Line smooth runnable sub task
        /// </summary>
        /// <param name="pSmoothesLines">The new flag to set indicating whether or not to smooth line.</param>
        public void SetLineSmooth(bool pSmoothesLines)
        {
            this.AddRunnable( new SetLineSmooth( pSmoothesLines ) );
        }

        /// <summary>
        /// Set Front face mode runnable sub task
        /// </summary>
        /// <param name="pIsClockWise">The new flag to set indicating whether or not front faces are clock wise.</param>
        public void SetFrontFaceMode(bool pIsClockWise)
        {
            this.AddRunnable( new SetFrontFaceMode( pIsClockWise ) );
        }

        /// <summary>
        /// Set Polygon mode runnable sub task
        /// </summary>
        /// <param name="pCullFront">The flag indicating whether or not to cull the front faces.</param>
        /// <param name="pCullBack">The flag indicating whether or not to cull the back faces.</param>
        /// <param name="pFrontMode">The new polygon front mode to set.</param>
        /// <param name="pBackMode">The new polygon back mode to set.</param>
        public void SetPolygonMode(bool pCullFront, bool pCullBack, PolygonMode pFrontMode, PolygonMode pBackMode)
        {
            this.AddRunnable( new SetPolygonMode( pCullFront, pCullBack, pFrontMode, pBackMode ) );
        }

        /// <summary>
        /// Set Polygon smooth runnable sub task
        /// </summary>
        /// <param name="pSmoothesPolygons">The new flag to set indicating whether or not to smooth polygons.</param>
        public void SetPolygonSmooth(bool pSmoothesPolygons)
        {
            this.AddRunnable( new SetPolygonSmooth( pSmoothesPolygons ) );
        }

        /// <summary>
        /// Set Polygon offset runnable sub task
        /// </summary>
        /// <param name="pOffset">The new polygon offset.</param>
        public void SetPolygonOffset(Vector2F pOffset)
        {
            this.AddRunnable( new SetPolygonOffset( pOffset ) );
        }

        /// <summary>
        /// Set Polygon offsets runnable sub task
        /// </summary>
        /// <param name="pHasPointOffset">The new flag to set indicating whether or not there is point offset.</param>
        /// <param name="pHasLineOffset">The new flag to set indicating whether or not there is line offset.</param>
        /// <param name="pHasPolygonOffset">The new flag to set indicating whether or not there is polygon offset.</param>
        public void SetPolygonOffsets(bool pHasPointOffset, bool pHasLineOffset, bool pHasPolygonOffset)
        {
            this.AddRunnable( new SetPolygonOffsets( pHasPointOffset, pHasLineOffset, pHasPolygonOffset ) );
        }

        /// <summary>
        /// Set Multisample runnable sub task
        /// </summary>
        /// <param name="pUseMultisampling">The new flag to set indicating whether or not to use multisampling.</param>
        public void SetMultisample(bool pUseMultisampling)
        {
            this.AddRunnable( new SetMultisample( pUseMultisampling ) );
        }

        /// <summary>
        /// Set sample alpha runnable sub task
        /// </summary>
        /// <param name="pSampleAlphaToCoverage">The new flag to set indicating whether or not to sample alpha to coverage.</param>
        /// <param name="pForceOpacity">The new flag to set indicating whether or not to force opacity to 1.</param>
        public void SetSampleAlpha(bool pSampleAlphaToCoverage, bool pForceOpacity)
        {
            this.AddRunnable( new SetSampleAlpha( pSampleAlphaToCoverage, pForceOpacity ) );
        }

        /// <summary>
        /// Set sample coverage runnable sub task
        /// </summary>
        /// <param name="pFragmentCoverage">The new fragment coverage to set.</param>
        public void SetSampleCoverage(float pFragmentCoverage)
        {
            this.AddRunnable( new SetSampleCoverage( pFragmentCoverage ) );
        }

        /// <summary>
        /// Set sample mask runnable sub task
        /// </summary>
        /// <param name="pSampleMask">The new sample mask to set.</param>
        public void SetSampleMask(uint pSampleMask)
        {
            this.AddRunnable( new SetSampleMask( pSampleMask ) );
        }

        /// <summary>
        /// Set sample shading runnable sub task
        /// </summary>
        /// <param name="pSampleShading">The new flag to set indicating whether or not to sample shading.</param>
        /// <param name="pMinSampleCount">The minimum sample count to set.</param>
        public void SetSampleShading(bool pSampleShading, float pMinSampleCount)
        {
            this.AddRunnable( new SetSampleShading( pSampleShading, pMinSampleCount ) );
        }

        /// <summary>
        /// Set occlusion test runnable sub task
        /// </summary>
        /// <param name="pOcclusionQuery">The new occlusion query mode to set.</param>
        /// <param name="pQueryMode">The new occlusion query to set.</param>
        public void SetOcclusionTest(GPUQuery pOcclusionQuery, QueryMode pQueryMode)
        {
            this.AddRunnable( new SetOcclusionTest( pOcclusionQuery, pQueryMode ) );
        }

        /// <summary>
        /// Set scissor test runnable sub task
        /// </summary>
        /// <param name="pUseScissor">The new flag to set indicating whether or not to use scissor.</param>
        public void SetScissorTest(bool pUseScissor)
        {
            this.AddRunnable( new SetScissorTest( pUseScissor ) );
        }

        /// <summary>
        /// Set scissor test value runnable sub task
        /// </summary>
        /// <param name="pUseScissor">The new flag to set indicating whether or not to use scissor.</param>
        /// <param name="pScissor">The scissor to use.</param>
        public void SetScissorTest(bool pUseScissor, Scissor pScissor)
        {
            this.AddRunnable( new SetScissorTestValue( pUseScissor, pScissor ) );
        }

        /// <summary>
        /// Set stencil test runnable sub task
        /// </summary>
        /// <param name="pUseStencil">The new flag to set indicating whether or not to use stencil test.</param>
        /// <param name="pFrontFunction">The new stencil front function to set.(NULL param will mean no front stencil dependencies update)</param>
        /// <param name="pFrontReference">The new stencil front reference to set.</param>
        /// <param name="pFrontMask">The new stencil front mask.</param>
        /// <param name="pFrontStencilFailure">The new stencil front operation in case of failure.</param>
        /// <param name="pFrontDepthFailure">The new depth front operation in case of failure.</param>
        /// <param name="pFrontDepthPass">The new depth front operation in case of success.</param>
        /// <param name="pBackFunction">The new stencil back function to set.(NULL param will mean no back stencil dependencies update)</param>
        /// <param name="pBackReference">The new stencil back reference to set.</param>
        /// <param name="pBackMask">The new stencil back mask.</param>
        /// <param name="pBackStencilFailure">The new stencil back operation in case of failure.</param>
        /// <param name="pBackDepthFailure">The new depth back operation in case of success.</param>
        /// <param name="pBackDepthPass">The new depth back operation in case of success.</param>
        public void SetStencilTest(bool pUseStencil, StencilFunction? pFrontFunction, int pFrontReference, uint pFrontMask, StencilOp pFrontStencilFailure, StencilOp pFrontDepthFailure, StencilOp pFrontDepthPass,
                                                     StencilFunction? pBackFunction, int pBackReference, uint pBackMask, StencilOp pBackStencilFailure, StencilOp pBackDepthFailure, StencilOp pBackDepthPass)
        {
            this.AddRunnable( new SetStencilTest( pUseStencil, pFrontFunction, pFrontReference, pFrontMask, pFrontStencilFailure, pFrontDepthFailure, pFrontDepthPass,
                                                               pBackFunction, pBackReference, pBackMask, pBackStencilFailure, pBackDepthFailure, pBackDepthPass ) );
        }

        /// <summary>
        /// Set depth test runnable sub task
        /// </summary>
        /// <param name="pUseDepth">The new flag to set indicating whether or not to use depth test.</param>
        /// <param name="pFunction">The new depth function to set. (NULL means that no function update is required)</param>
        public void SetDepthTest(bool pUseDepth, DepthFunction? pFunction)
        {
            this.AddRunnable( new SetDepthTest( pUseDepth, pFunction ) );
        }

        /// <summary>
        /// Set blend runnable sub task
        /// </summary>
        /// <param name="pBufferId">The attachment point blending must be set for. (Can be NONE)</param>
        /// <param name="pUseBlending">The new flag indicating whether or not enabling the blending.</param>
        /// <param name="pRGBEquation">The new RGB blending equation to set. (NULL means no update of the RGB equation and its dependencies)</param>
        /// <param name="pRGBSourceFactor">The new RGB blending source factor to set.</param>
        /// <param name="pRGBDestinationFactor">The new RGB blending destination factor to set.</param>
        /// <param name="pAlphaEquation">The new Alpha blending equation to set. (NULL means no update of the Alpha equation and its dependencies)</param>
        /// <param name="pAlphaSourceFactor">The new Alpha blending source factor to set.</param>
        /// <param name="pAlphaDestinationFactor">The new Alpha blending destination factor to set.</param>
        public void SetBlend(BufferId pBufferId, bool pUseBlending, BlendEquationMode? pRGBEquation, BlendingFactorSrc pRGBSourceFactor, BlendingFactorDest pRGBDestinationFactor,
                                                                    BlendEquationMode? pAlphaEquation, BlendingFactorSrc pAlphaSourceFactor, BlendingFactorDest pAlphaDestinationFactor)
        {
            this.AddRunnable( new SetBlend( pBufferId, pUseBlending, pRGBEquation, pRGBSourceFactor, pRGBDestinationFactor,
                                                                     pAlphaEquation, pAlphaSourceFactor, pAlphaDestinationFactor ) );
        }

        /// <summary>
        /// Set blend color runnable sub task
        /// </summary>
        /// <param name="pBlendColor">The new blend color to set.</param>
        public void SetBlendColor(Color pBlendColor)
        {
            this.AddRunnable( new SetBlendColor( pBlendColor ) );
        }

        /// <summary>
        /// Set dither runnable sub task
        /// </summary>
        /// <param name="pUseDither">The new flag to set indicating whether or not to use dithering.</param>
        public void SetDither(bool pUseDither)
        {
            this.AddRunnable( new SetDither( pUseDither ) );
        }

        /// <summary>
        /// Set logic operation runnable sub task
        /// </summary>
        /// <param name="pUseLogicOp">The new flag to set indicating whether or not to use logical operation.</param>
        /// <param name="pOperation">The new logic operation to set. (NULL means no operation update).</param>
        public void SetLogicOp(bool pUseLogicOp, LogicOp? pOperation)
        {
            this.AddRunnable( new SetLogicOp( pUseLogicOp, pOperation ) );
        }

        /// <summary>
        /// Set Color mask runnable sub task
        /// </summary>
        /// <param name="pBufferId">The attachment point the color mask must be set for.</param>
        /// <param name="pRed">Can write red channel</param>
        /// <param name="pGreen">Can write green channel</param>
        /// <param name="pBlue">Can write blue channel</param>
        /// <param name="pAlpha">Can write alpha channel</param>
        public void SetColorMask(BufferId pBufferId, bool pRed, bool pGreen, bool pBlue, bool pAlpha)
        {
            this.AddRunnable( new SetColorMask( pBufferId, pRed, pGreen, pBlue, pAlpha ) );
        }

        /// <summary>
        /// Set Depth mask runnable sub task
        /// </summary>
        /// <param name="pAllowDepthBufferWrite">The flag to set indicating whether the depth buffer can be written to or not.</param>
        public void SetDepthMask(bool pAllowDepthBufferWrite)
        {
            this.AddRunnable( new SetDepthMask( pAllowDepthBufferWrite ) );
        }

        /// <summary>
        /// Set Stencil mask runnable sub task
        /// </summary>
        /// <param name="pFrontStencilMask">The new front stencil mask to set.</param>
        /// <param name="pBackStencilMask">The new front stencil mask to set.</param>
        public void SetStencilMask(uint pFrontStencilMask, uint pBackStencilMask)
        {
            this.AddRunnable( new SetStencilMask( pFrontStencilMask, pBackStencilMask ) );
        }

        /// <summary>
        /// Set Clear state runnable sub task
        /// </summary>
        /// <param name="pMustClearColorBuffers">The flag indicating whether to clear color buffers or not.</param>
        /// <param name="pMustClearStencilBuffer">The flag indicating whether to clear stencil buffer or not.</param>
        /// <param name="pMustClearDepthBuffer">The flag indicating whether to clear depth buffer or not.</param>
        public void SetClearState(bool pMustClearColorBuffers, bool pMustClearStencilBuffer, bool pMustClearDepthBuffer)
        {
            this.AddRunnable( new SetClearState( pMustClearColorBuffers, pMustClearStencilBuffer, pMustClearDepthBuffer ) );
        }

        /// <summary>
        /// Set Buffers runnable sub task
        /// </summary>
        /// <param name="pReadBuffer">The new attachment point to use as read buffer to set.</param>
        /// <param name="pDrawBuffer">The new attachment point to use as draw buffer to set.</param>
        public void SetBuffers(BufferId pReadBuffer, BufferId pDrawBuffer)
        {
            this.AddRunnable( new SetBuffers( pReadBuffer, pDrawBuffer ) );
        }

        #region Methods Internal

        /// <summary>
        /// Adds a subtask to this task.
        /// </summary>
        /// <param name="pSubTask">The subtask to add.</param>
        internal void AddRunnable(IRunnable pSubTask)
        {
            this.mSubtasks.Add( pSubTask );
        }

        /// <summary>
        /// Executes all sub tasks of this task.
        /// </summary>
        internal void ExecuteSubTasks()
        {
            ISceneService lSceneManager = ServiceManager.Instance.GetService<ISceneService>();
            if ( lSceneManager != null )
            {
                FrameBuffer lCurrent = lSceneManager.CurrentFrameBuffer as FrameBuffer;
                for ( int lCurr = 0; lCurr < this.mSubtasks.Count; lCurr++ )
                {
                    this.mSubtasks[ lCurr ].Execute( lCurrent );
                }
            }
        }

        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(SetStateTaskFactory pOther)
        {
            Utilities.Swap( ref this.mSubtasks, ref pOther.mSubtasks );
        }
        
        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            return new SetStateTask( this );
        }

        #endregion Methods ATaskFactory

        #region Methods IDisposable

        /// <summary>
        /// Delegate called on dispose.
        /// </summary>
        protected override void OnDispose()
        {
            for
                ( int lCurr = 0; lCurr < this.mSubtasks.Count; lCurr++ )
            {
                this.mSubtasks[ lCurr ].Dispose();
            }
            this.mSubtasks.Clear();

            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
