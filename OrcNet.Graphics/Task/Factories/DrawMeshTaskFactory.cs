﻿using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.SceneGraph;
using System;

namespace OrcNet.Graphics.Task.Factories
{
    /// <summary>
    /// Draw mesh task factory class definition for drawing a mesh
    /// using the current framebuffer and pipeline pass.
    /// </summary>
    public class DrawMeshTaskFactory : ATaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the [Node].[Mesh] qualified name corresponding to the node containing the mesh to draw and that mesh to draw.
        /// </summary>
        private MeshQualifier mMesh;

        /// <summary>
        /// Stores the amount of times the mesh must be drawn.
        /// </summary>
        private int mDrawCount;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += this.mMesh.Size;
                lSize += sizeof(int);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawMeshTaskFactory"/> class.
        /// </summary>
        private DrawMeshTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawMeshTaskFactory"/> class.
        /// </summary>
        /// <param name="pMesh">The [Node].[Mesh] qualified name corresponding to the node containing the mesh to draw and that mesh to draw.</param>
        /// <param name="pDrawCount">The amount of times the mesh must be drawn.</param>
        public DrawMeshTaskFactory(MeshQualifier pMesh, int pDrawCount)
        {
            this.Initialize( pMesh, pDrawCount );
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the task factory.
        /// </summary>
        /// <param name="pMesh">The [Node].[Mesh] qualified name corresponding to the node containing the mesh to draw and that mesh to draw.</param>
        /// <param name="pDrawCount">The amount of times the mesh must be drawn.</param>
        private void Initialize(MeshQualifier pMesh, int pDrawCount)
        {
            this.mMesh = pMesh;
            this.mDrawCount = pDrawCount;
        }

        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(DrawMeshTaskFactory pOther)
        {
            Utilities.Swap( ref this.mMesh, ref pOther.mMesh );
            Utilities.Swap( ref this.mDrawCount, ref pOther.mDrawCount );
        }

        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            SceneNodeMethod lMethod = pContext as SceneNodeMethod;
            ISceneNode lTarget = lMethod.Target;
            ISceneNode lThisTarget = this.mMesh.GetTarget( lTarget );
            MeshBuffers lMesh = null;
            if
                ( lThisTarget == null )
            {
                IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                if
                    ( lResourceService != null )
                {
                    IResource lResource = lResourceService.LoadResource( this.mMesh.Name + ".mesh" );
                    if
                        ( lResource != null )
                    {
                        lMesh = lResource.OwnedObject as MeshBuffers;
                    }
                }
            }
            else
            {
                lMesh = lThisTarget.GetMesh( this.mMesh.Name ) as MeshBuffers;
            }

            if
                ( lMesh == null )
            {
                LogManager.Instance.Log( string.Format( "DrawMesh: Cannot find any mesh \"{0}\" of \"{1}\"!!!", this.mMesh.Target, this.mMesh.Name ), LogType.ERROR );
            }
            
            return new DrawMeshTask( lMesh, this.mDrawCount );
        }

        #endregion Methods ATaskFactory

        #endregion Methods
    }
}
