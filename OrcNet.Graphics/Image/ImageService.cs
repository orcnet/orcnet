﻿using ImageMagick;
using OrcNet.Core.Image;
using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using System;
using System.IO;

namespace OrcNet.Graphics.Image
{
    /// <summary>
    /// Image service class definition.
    /// </summary>
    public class ImageService : AService, IImageService
    {
        #region Properties

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "ImageLoader";
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageService"/> class.
        /// </summary>
        public ImageService()
        {
            string lTempDirectory = Path.Combine( Environment.CurrentDirectory, @"..\Resources\Temp" );
            if ( Directory.Exists( lTempDirectory ) == false )
            {
                Directory.CreateDirectory( lTempDirectory );
            }

            string lCacheDirectory = Path.Combine( lTempDirectory, "Cache" );
            if ( Directory.Exists( lCacheDirectory ) == false )
            {
                Directory.CreateDirectory( lCacheDirectory );
            }

            MagickAnyCPU.CacheDirectory = lCacheDirectory; // Faster load next start up.
            MagickNET.SetTempDirectory( lTempDirectory ); // Play ground of image magic.
            //MagickNET.Initialize( @".\Resources\Magick" ); // Gives custom config file(s)
            //MagickNET.SetGhostscriptDirectory( @".\Resources\Temp\GhostScript" ); // Gives path to ghost script binaries.
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the supplied image binary data are in HDR format or not.
        /// </summary>
        /// <param name="pData">The image binary data</param>
        /// <returns>True if in HDR format, false otherwise.</returns>
        public bool IsHDRFormat(byte[] pData)
        {
            // TO DO...
            return false;
        }

        /// <summary>
        /// Loads any other image format from binary data like Jpg, Png or other LDR formats.
        /// </summary>
        /// <param name="pData">The image binary data</param>
        /// <param name="pWidth">The extracted image width</param>
        /// <param name="pHeight">The extracted image height</param>
        /// <param name="pChannels">The extracted image channel count</param>
        /// <returns>The decompressed image.</returns>
        public byte[] Load(byte[] pData, out int pWidth, out int pHeight, out int pChannels)
        {
            try
            {
                using ( MagickImage lImage = new MagickImage( pData ) )
                {
                    pWidth    = lImage.Width;
                    pHeight   = lImage.Height;
                    pChannels = lImage.ChannelCount;

                    lImage.Flip();

                    byte[] lResult = Array.ConvertAll( lImage.GetPixels().ToArray(), pElt => (byte)pElt );
                    return lResult;
                }
            }
            catch ( MagickException pEx )
            {
                pWidth    = 0;
                pHeight   = 0;
                pChannels = 0;
                LogManager.Instance.Log( pEx );
            }
            
            return pData;
        }

        /// <summary>
        /// Loads any other image format from binary data like Jpg, Png or other LDR formats.
        /// </summary>
        /// <param name="pFullpath">The image fullname</param>
        /// <param name="pWidth">The extracted image width</param>
        /// <param name="pHeight">The extracted image height</param>
        /// <param name="pChannels">The extracted image channel count</param>
        /// <returns>The decompressed image.</returns>
        public byte[] Load(string pFullpath, out int pWidth, out int pHeight, out int pChannels)
        {
            try
            {
                using ( MagickImage lImage = new MagickImage( pFullpath ) )
                {
                    pWidth    = lImage.Width;
                    pHeight   = lImage.Height;
                    pChannels = lImage.ChannelCount;

                    lImage.Flip();

                    byte[] lResult = Array.ConvertAll( lImage.GetPixels().ToArray(), pElt => (byte)pElt );
                    return lResult;
                }
            }
            catch ( MagickException pEx )
            {
                pWidth    = 0;
                pHeight   = 0;
                pChannels = 0;
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Saves the given data to the supplied filename.
        /// </summary>
        /// <param name="pData">The pixel data to save.</param>
        /// <param name="pFileName">The filename to save the pixels to.</param>
        public bool Save(byte[] pData, string pFileName)
        {
            bool lResult = false;

            try
            {
                MagickReadSettings lReadSettings = new MagickReadSettings();
                //lReadSettings.Format = MagickFormat.Raw;
                using ( MagickImage lImage = new MagickImage( pData, lReadSettings ) )
                {
                    lImage.Write( pFileName );

                    lResult = true;
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return lResult;
        }

        /// <summary>
        /// Loads an image from binary data being in radiance HDR format.
        /// </summary>
        /// <param name="pData">The image binary data</param>
        /// <param name="pWidth">The extracted image width</param>
        /// <param name="pHeight">The extracted image height</param>
        /// <param name="pChannels">The extracted image channel count</param>
        /// <returns>The decompressed HDR image.</returns>
        public byte[] LoadHDRFormat(byte[] pData, out int pWidth, out int pHeight, out int pChannels)
        {
            pWidth = 0;
            pHeight = 0;
            pChannels = 0;

            // TO DO: Modify to load HDR format, for now not supported.
            return Load( pData, out pWidth, out pHeight, out pChannels );
        }
        
        #endregion Methods
    }
}
