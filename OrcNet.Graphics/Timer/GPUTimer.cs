﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Timer;
using System;

namespace OrcNet.Graphics
{
    /// <summary>
    /// A timer to measure time and time intervals on GPU. As GPU operations
    /// are asynch, CPU time is not relevant to check the duration of
    /// such operations, so this timer will be in charge of measuring 
    /// time and intervals on the GPU.
    /// </summary>
    public class GPUTimer : BaseTimer, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the timer is disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the query id.
        /// </summary>
        private uint mQuery;
        
        #endregion Fields

        #region Properties

        #region Properties BaseTimer

        /// <summary>
        /// Gets the last recorded duration between start and end calls.
        /// </summary>
        public override double LastDuration
        {
            get
            {
                // Refresh if needed as GPU timer is asynch.
                if ( this.mLastDuration == 0 )
                {
                    this.UpdateQueryResult();
                }

                return base.LastDuration;
            }
        }

        /// <summary>
        /// Gets the average delay in microseconds.
        /// </summary>
        public override double AverageTime
        {
            get
            {
                if ( this.mCycleCount == 0 )
                {
                    return 0.0;
                }
                
                return this.mTotalDuration / (double)this.mCycleCount;
            }
        }
        
        #endregion Properties BaseTimer

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GPUTimer"/> class.
        /// </summary>
        public GPUTimer()
        {
            this.mIsDisposed = false;
            GL.GenQueries( 1, out this.mQuery );
        }

        #endregion Constructor

        #region Methods

        #region Methods IDisposable

        /// <summary>
        /// Releases unmanaged resources
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                GL.DeleteQueries( 1, ref this.mQuery );

                // If dispose called, no need to finalize before garbage collecting
                // and it will be faster.
                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods IDisposable

        #region Methods BaseTimer

        /// <summary>
        /// Starts the timer.
        /// </summary>
        /// <returns>The current time in microseconds</returns>
        public override double Start()
        {
            this.UpdateQueryResult();
            this.mCycleCount++;
            GL.BeginQuery( QueryTarget.TimeElapsed, this.mQuery );

            return 0.0;
        }

        /// <summary>
        /// Stops the timer
        /// </summary>
        /// <returns>The elasped time since the start call in microseconds.</returns>
        public override double End()
        {
            GL.EndQuery( QueryTarget.TimeElapsed );
            return this.mLastDuration;
        }
        
        #endregion Methods BaseTimer

        #region Methods Internal

        /// <summary>
        /// Updates the timer result by getting info from GPU.
        /// </summary>
        private void UpdateQueryResult()
        {
            if ( this.CycleCount > 0 )
            {
                ulong lElapsedTime;
                GL.GetQueryObject( this.mQuery, GetQueryObjectParam.QueryResult, out lElapsedTime );

                if ( lElapsedTime != 0 )
                {
                    this.mLastDuration = (double)lElapsedTime;
                    this.mTotalDuration += this.mLastDuration;
                    this.mMinDuration = System.Math.Min( this.mLastDuration, this.mMinDuration );
                    this.mMaxDuration = System.Math.Max( this.mLastDuration, this.mMaxDuration );
                }
            }
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
