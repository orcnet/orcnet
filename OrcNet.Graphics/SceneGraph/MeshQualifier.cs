﻿using OrcNet.Core.SceneGraph;

namespace OrcNet.Graphics.SceneGraph
{
    /// <summary>
    /// Mesh Qualifier structure definition owning the pair
    /// of the form [Target].[Mesh]
    /// </summary>
    public class MeshQualifier : MethodQualifier
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MeshQualifier"/> class.
        /// </summary>
        /// <param name="pQualifierPair">The qualifier pair of the form [Target].[Mesh]</param>
        public MeshQualifier(string pQualifierPair) :
        base( pQualifierPair )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MeshQualifier"/> class.
        /// </summary>
        /// <param name="pTarget">The mesh owner.</param>
        /// <param name="pName">The mesh instance.</param>
        public MeshQualifier(string pTarget, string pName) :
        base( pTarget, pName )
        {
            
        }

        #endregion Constructor
    }
}
