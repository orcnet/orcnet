﻿using OrcNet.Core;
using OrcNet.Core.Helpers;
using OrcNet.Core.Math;
using OrcNet.Core.Render;
using OrcNet.Core.Resource;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.Graphics.SceneGraph
{
    /// <summary>
    /// Scene node class definition.
    /// </summary>
    [DebuggerDisplay("IsVisible = {IsVisible}, ChildCount = {ChildCount}")]
    public class SceneNode : AMemoryProfilable, ISceneNode
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the scene node has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the Scene node creator resource.
        /// </summary>
        private IResource mCreator;

        /// <summary>
        /// Stores the flag indicating whether the node is visible or not.
        /// </summary>
        private bool mIsVisible;

        /// <summary>
        /// Stores the set of flags of this node.
        /// </summary>
        private HashSet<string> mFlags;

        /// <summary>
        /// Stores the node's children.
        /// </summary>
        private List<SceneNode> mChildren;

        /// <summary>
        /// Stores the set of methods to run on this node.
        /// </summary>
        private Dictionary<string, SceneNodeMethod> mMethods;

        /// <summary>
        /// Stores the set of field(s) of this node.
        /// </summary>
        private Dictionary<string, object> mFields;

        /// <summary>
        /// Stores the mesh(es) of this node.
        /// </summary>
        private Dictionary<string, MeshBuffers> mMeshes;

        /// <summary>
        /// Stores the pipeline values of that scene node.
        /// </summary>
        private Dictionary<string, IPipelineValue> mValues;

        /// <summary>
        /// Stores the pipeline description(s) of that scene node.
        /// </summary>
        private Dictionary<string, PipelineDescription> mDescriptions;

        /// <summary>
        /// Stores the transform from this node to the parent node.
        /// </summary>
        private IMatrix<float> mLocalToParentTransform;

        /// <summary>
        /// Stores the transform from this node to the root node.
        /// </summary>
        private IMatrix<float> mLocalToWorldTransform;

        /// <summary>
        /// Stores the transform from the root node to this node.
        /// </summary>
        private IMatrix<float> mWorldToLocalTransform;

        /// <summary>
        /// Stores the flag indicating whether the WorldToLocalTransform is valid or not.
        /// </summary>
        private bool mIsWorldToLocalValid;

        /// <summary>
        /// Stores the transform from this node to the camera node.
        /// </summary>
        private IMatrix<float> mLocalToCameraTransform;

        /// <summary>
        /// Stores the transform from this node to screen space.
        /// Actually the LocalToCameraTransform followed by the
        /// camera to screen space transform defined by the 
        /// CameraToScreenTransform of the camera node.
        /// </summary>
        private IMatrix<float> mLocalToScreenTransform;

        /// <summary>
        /// Stores the scene node bounds in local coordinates.
        /// </summary>
        private IBox<float> mLocalBounds;

        /// <summary>
        /// Stores the scene node bounds in world coordinates.
        /// </summary>
        private IBox<float> mWorldBounds;

        /// <summary>
        /// Stores the origin of the local reference frame in world coordinates.
        /// </summary>
        private IVector<float> mWorldCoordinates;

        #endregion Fields

        #region Properties

        #region Properties ISceneNode

        /// <summary>
        /// Gets a child node by index.
        /// </summary>
        /// <param name="pIndex">The child node's index to look for.</param>
        /// <returns>The child node if index in bounds, null otherwise.</returns>
        public ISceneNode this[SceneNodeIndex pIndex]
        {
            get
            {
                return this.mChildren[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the flag indicating whether this node has any children or not.
        /// </summary>
        public bool AnyChildren
        {
            get
            {
                return this.ChildCount > 0;
            }
        }

        /// <summary>
        /// Gets the child count.
        /// </summary>
        public int ChildCount
        {
            get
            {
                return this.mChildren.Count;
            }
        }

        /// <summary>
        /// Gets or sets the scene node bounds in local coordinates.
        /// </summary>
        IBox<float> ISceneNode.LocalBounds
        {
            get
            {
                return this.mLocalBounds;
            }
            set
            {
                this.mLocalBounds = value as Box3F;
            }
        }

        /// <summary>
        /// Gets the scene node bounds in world coordinates.
        /// </summary>
        IBox<float> ISceneNode.WorldBounds
        {
            get
            {
                return this.WorldBounds;
            }
        }

        /// <summary>
        /// Gets the node's children.
        /// </summary>
        public IEnumerable<ISceneNode> Children
        {
            get
            {
                return this.mChildren;
            }
        }

        /// <summary>
        /// Gets the set of flags of this node.
        /// </summary>
        public IEnumerable<string> Flags
        {
            get
            {
                return this.mFlags;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the node is visible or not.
        /// </summary>
        public bool IsVisible
        {
            get
            {
                return this.mIsVisible;
            }
            set
            {
                this.mIsVisible = value;
            }
        }

        /// <summary>
        /// Gets the mesh(es) of this node.
        /// </summary>
        public IEnumerable<IMeshBuffers> Meshes
        {
            get
            {
                return this.mMeshes.Values;
            }
        }

        /// <summary>
        /// Gets the set of methods to run on this node.
        /// </summary>
        public IEnumerable<SceneNodeMethod> Methods
        {
            get
            {
                return this.mMethods.Values;
            }
        }

        /// <summary>
        /// Gets the field(s) of this node.
        /// </summary>
        public IEnumerable<object> Fields
        {
            get
            {
                return this.mFields.Values;
            }
        }

        #endregion Properties ISceneNode

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = sizeof(bool) * 2;
                lSize += (sizeof(double) * 6) * 2; // Two bboxes.
                lSize += sizeof(double) * 3; // Coords.
                lSize += (sizeof(double) * 16) * 5; // Five matrices 4x4.
                foreach( string lFlag in this.mFlags )
                {
                    lSize += sizeof(char) * (uint)lFlag.Length;
                }
                foreach( SceneNode lChild in this.mChildren )
                {
                    lSize += lChild.Size;
                }
                foreach( KeyValuePair<string, SceneNodeMethod> lMethod in this.mMethods )
                {
                    lSize += sizeof(char) * (uint)lMethod.Key.Length;
                    lSize += lMethod.Value.Size;
                }
                foreach( KeyValuePair<string, MeshBuffers> lMesh in this.mMeshes )
                {
                    lSize += sizeof(char) * (uint)lMesh.Key.Length;
                    lSize += lMesh.Value.Size;
                }
                foreach( KeyValuePair<string, IPipelineValue> lValue in this.mValues )
                {
                    lSize += sizeof(char) * (uint)lValue.Key.Length;
                    lSize += lValue.Value.Size;
                }
                foreach( KeyValuePair<string, PipelineDescription> lDescription in this.mDescriptions )
                {
                    lSize += sizeof(char) * (uint)lDescription.Key.Length;
                    lSize += lDescription.Value.Size;
                }
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        public IResource Creator
        {
            get
            {
                return this.mCreator;
            }
            set
            {
                this.mCreator = value;
            }
        }

        #endregion Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the scene node bounds in local coordinates.
        /// </summary>
        public Box3F LocalBounds
        {
            get
            {
                return this.mLocalBounds as Box3F;
            }
            set
            {
                this.mLocalBounds = value;
            }
        }

        /// <summary>
        /// Gets the scene node bounds in world coordinates.
        /// </summary>
        public Box3F WorldBounds
        {
            get
            {
                return this.mWorldBounds as Box3F;
            }
        }

        /// <summary>
        /// Gets or sets the transform from this node to the parent node.
        /// </summary>
        public AMatrix<float> LocalToParentTransform
        {
            get
            {
                return this.mLocalToParentTransform as Matrix4F;
            }
            set
            {
                this.mLocalToParentTransform = value;
            }
        }

        /// <summary>
        /// Gets the transform from this node to the root node.
        /// </summary>
        public AMatrix<float> LocalToWorldTransform
        {
            get
            {
                return this.mLocalToWorldTransform as Matrix4F;
            }
        }

        /// <summary>
        /// Gets the transform from the root node to this node.
        /// </summary>
        public AMatrix<float> WorldToLocalTransform
        {
            get
            {
                if ( this.mIsWorldToLocalValid == false )
                {
                    bool lResult;
                    this.mWorldToLocalTransform = this.mLocalToWorldTransform.Inverse( float.Epsilon, out lResult );
                    this.mIsWorldToLocalValid = true;
                }

                return this.mWorldToLocalTransform as Matrix4F;
            }
        }

        /// <summary>
        /// Gets the transform from this node to the camera node.
        /// </summary>
        public AMatrix<float> LocalToCameraTransform
        {
            get
            {
                return this.mLocalToCameraTransform as Matrix4F;
            }
        }

        /// <summary>
        /// Gets the transform from this node to screen space.
        /// Actually the LocalToCameraTransform followed by the
        /// camera to screen space transform defined by the 
        /// CameraToScreenTransform of the camera node.
        /// </summary>
        public AMatrix<float> LocalToScreenTransform
        {
            get
            {
                return this.mLocalToScreenTransform as Matrix4F;
            }
        }

        /// <summary>
        /// Gets the origin of the local reference frame in world coordinates.
        /// </summary>
        public Vector3F WorldCoordinates
        {
            get
            {
                return this.mWorldCoordinates as Vector3F;
            }
        }

        /// <summary>
        /// Gets the pipeline values of that scene node.
        /// </summary>
        public IEnumerable<IPipelineValue> Values
        {
            get
            {
                return this.mValues.Values;
            }
        }

        /// <summary>
        /// Gets the pipeline description(s) of that scene node.
        /// </summary>
        public IEnumerable<PipelineDescription> PipelineDescriptions
        {
            get
            {
                return this.mDescriptions.Values;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneNode"/> class.
        /// </summary>
        public SceneNode()
        {
            this.mFlags = new HashSet<string>();
            this.mChildren = new List<SceneNode>();
            this.mMethods = new Dictionary<string, SceneNodeMethod>();
            this.mFields = new Dictionary<string, object>();
            this.mMeshes = new Dictionary<string, MeshBuffers>();
            this.mValues = new Dictionary<string, IPipelineValue>();
            this.mDescriptions = new Dictionary<string, PipelineDescription>();

            this.mLocalToParentTransform = Matrix4F.IDENTITY;
            this.mLocalToWorldTransform  = Matrix4F.IDENTITY;
            this.mIsWorldToLocalValid = false;
            this.mLocalBounds = new Box3F( 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );
            this.mLocalToScreenTransform = Matrix4F.IDENTITY;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Retrieves the value whose name is provided.
        /// </summary>
        /// <param name="pName">The value name.</param>
        /// <returns>The value if found, null otherwise.</returns>
        public IPipelineValue GetValue(string pName)
        {
            IPipelineValue lValue;
            if ( this.mValues.TryGetValue( pName, out lValue ) )
            {
                return lValue;
            }

            return null;
        }

        /// <summary>
        /// Adds a new value to that scene node.
        /// </summary>
        /// <param name="pValue">The new value.</param>
        public void AddValue(IPipelineValue pValue)
        {
            string lKey = pValue.Name;
            if ( this.mValues.ContainsKey( lKey ) == false )
            {
                this.mValues.Add( lKey, pValue );
            }
        }

        /// <summary>
        /// Removes a value whose is given.
        /// </summary>
        /// <param name="pName">The name of the value that must be removed.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveValue(string pName)
        {
            return this.mValues.Remove( pName );
        }

        /// <summary>
        /// Retrieves the pipeline description whose name is provided.
        /// </summary>
        /// <param name="pName">The pipeline description's name.</param>
        /// <returns>The description if found, null otherwise.</returns>
        public IPipelineDescription GetPipelineDescription(string pName)
        {
            PipelineDescription lDescription;
            if ( this.mDescriptions.TryGetValue( pName, out lDescription ) )
            {
                return lDescription;
            }

            return null;
        }

        /// <summary>
        /// Adds a new pipeline description to that scene node mapped with the given name.
        /// </summary>
        /// <param name="pName">The description's name.</param>
        /// <param name="pNewDescription">The new description.</param>
        public void AddPipelineDescription(string pName, IPipelineDescription pNewDescription)
        {
            this.mDescriptions[ pName ] = pNewDescription as PipelineDescription;
        }

        /// <summary>
        /// Removes a pipeline description mapped with the supplied name.
        /// </summary>
        /// <param name="pName">The name that description is mapped with.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemovePipelineDescription(string pName)
        {
            return this.mDescriptions.Remove( pName );
        }

        #region Methods ISceneNode

        /// <summary>
        /// Adds the supplied node as child of that node's children set.
        /// </summary>
        /// <param name="pChild">The new child node.</param>
        public void AddChild(ISceneNode pChild)
        {
            SceneNode lCast = pChild as SceneNode;
            if ( this.mChildren.Contains( lCast ) == false )
            {
                this.mChildren.Add( lCast );

                // Reset the Scene service node map cache.
                SceneManager lSceneService = ServiceManager.Instance.GetService<SceneManager>();
                if ( lSceneService != null )
                {
                    lSceneService.ClearNodeMap();
                }
            }
        }

        /// <summary>
        /// Adds a new flag to that node's flag set.
        /// </summary>
        /// <param name="pFlag">The new flag.</param>
        public void AddFlag(string pFlag)
        {
            this.mFlags.Add( pFlag );

            // Reset the Scene service node map cache.
            SceneManager lSceneService = ServiceManager.Instance.GetService<SceneManager>();
            if ( lSceneService != null )
            {
                lSceneService.ClearNodeMap();
            }
        }

        /// <summary>
        /// Adds a mesh to this node under the given name.
        /// </summary>
        /// <param name="pMeshName">The mesh name.</param>
        /// <param name="pMesh">The mesh.</param>
        public void AddMesh(string pMeshName, IMeshBuffers pMesh)
        {
            this.mMeshes[ pMeshName ] = pMesh as MeshBuffers;
            this.mLocalBounds = this.mLocalBounds.Merge( pMesh.Bounds ) as Box3F;
        }

        /// <summary>
        /// Adds a new method to that node's methods set.
        /// </summary>
        /// <param name="pMethodName">The method identifier</param>
        /// <param name="pMethod">The method to add.</param>
        public void AddMethod(string pMethodName, SceneNodeMethod pMethod)
        {
            this.RemoveMethod( pMethodName );
            this.mMethods[ pMethodName ] = pMethod;
            pMethod.Target = this;
        }

        /// <summary>
        /// Adds a new field to this node's field set.
        /// </summary>
        /// <param name="pFieldName">The field identifier.</param>
        /// <param name="pField">The field to add.</param>
        public void AddField(string pFieldName, object pField)
        {
            this.mFields[ pFieldName ] = pField;
        }

        /// <summary>
        /// Retrieves the field of this node having the specified name if any.
        /// </summary>
        /// <param name="pFieldName">The field name to look for.</param>
        /// <returns>The field, null otherwise.</returns>
        public object GetFields(string pFieldName)
        {
            object lField;
            if ( this.mFields.TryGetValue( pFieldName, out lField ) )
            {
                return lField;
            }

            return null;
        }
        
        /// <summary>
        /// Retrieves the mesh of this node whose name is provided.
        /// </summary>
        /// <param name="pMeshName">The mesh name.</param>
        /// <returns>The mesh if found, null otherwise.</returns>
        public IMeshBuffers GetMesh(string pMeshName)
        {
            MeshBuffers lMesh;
            if ( this.mMeshes.TryGetValue( pMeshName, out lMesh ) )
            {
                return lMesh;
            }

            return null;
        }

        /// <summary>
        /// Retrieves the method of this node having the specified name if any.
        /// </summary>
        /// <param name="pMethodName">The method name to look for.</param>
        /// <returns>The method, null otherwise.</returns>
        public SceneNodeMethod GetMethod(string pMethodName)
        {
            SceneNodeMethod lMethod;
            if ( this.mMethods.TryGetValue( pMethodName, out lMethod ) )
            {
                return lMethod;
            }

            return null;
        }
        
        /// <summary>
        /// Checks whether the node has the given flag or not.
        /// </summary>
        /// <param name="pFlag">Teh flag to look for.</param>
        /// <returns>True if has the flag, false otherwise.</returns>
        public bool HasFlag(string pFlag)
        {
            return this.mFlags.Contains( pFlag );
        }

        /// <summary>
        /// Checks whether the supplied node is a child of this node or not.
        /// </summary>
        /// <param name="pNode">The node to test.</param>
        /// <returns>True if the node is a child of this node, false otherwise.</returns>
        public bool HasChild(ISceneNode pNode)
        {
            return this.mChildren.Contains( pNode as SceneNode );
        }

        /// <summary>
        /// Removes a child by index from that node's children set.
        /// </summary>
        /// <param name="pIndex">The node's index to remove.</param>
        /// <returns>True if the supplied node has been removed, false otherwise.</returns>
        public bool RemoveChild(int pIndex)
        {
            if ( pIndex < 0 ||
                 pIndex >= this.ChildCount )
            {
                return false;
            }

            return this.mChildren.Remove( this.mChildren[ pIndex ] );
        }

        /// <summary>
        /// Removes the supplied child from that node's children set.
        /// </summary>
        /// <param name="pNode">The node to remove.</param>
        /// <returns>True if the supplied node has been removed, false otherwise.</returns>
        public bool RemoveChild(ISceneNode pNode)
        {
            return this.mChildren.Remove( pNode as SceneNode );
        }

        /// <summary>
        /// Removes the supplied flag from that node's flag set.
        /// </summary>
        /// <param name="pFlag">The flag to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveFlag(string pFlag)
        {
            bool lResult =this.mFlags.Remove( pFlag );

            // Reset the Scene service node map cache.
            SceneManager lSceneService = ServiceManager.Instance.GetService<SceneManager>();
            if ( lSceneService != null )
            {
                lSceneService.ClearNodeMap();
            }

            return lResult;
        }

        /// <summary>
        /// Removes the mesh having the specified name.
        /// </summary>
        /// <param name="pMeshName">The mesh name.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveMesh(string pMeshName)
        {
            return this.mMeshes.Remove( pMeshName );
        }

        /// <summary>
        /// Removes the field having the specified name.
        /// </summary>
        /// <param name="pFieldName">The field name to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveField(string pFieldName)
        {
            return this.mFields.Remove( pFieldName );
        }

        /// <summary>
        /// Removes the method having the specified name.
        /// </summary>
        /// <param name="pMethodName">The method name to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveMethod(string pMethodName)
        {
            SceneNodeMethod lMethod;
            if ( this.mMethods.TryGetValue( pMethodName, out lMethod ) )
            {
                lMethod.Target = null;
                return this.mMethods.Remove( pMethodName );
            }

            return false;
        }

        #endregion Methods ISceneNode

        #region Methods Internal

        /// <summary>
        /// Swaps that scene node with another.
        /// </summary>
        /// <param name="pOther">The other scene node to swap with.</param>
        internal void Swap(SceneNode pOther)
        {
            Utilities.Swap( ref this.mLocalToParentTransform, ref pOther.mLocalToParentTransform );
            Utilities.Swap( ref this.mFlags, ref pOther.mFlags );
            Utilities.Swap( ref this.mValues, ref pOther.mValues );
            Utilities.Swap( ref this.mDescriptions, ref pOther.mDescriptions );
            Utilities.Swap( ref this.mMeshes, ref pOther.mMeshes );
            Utilities.Swap( ref this.mMethods, ref pOther.mMethods );
            Utilities.Swap( ref this.mChildren, ref pOther.mChildren );

            foreach ( KeyValuePair<string, SceneNodeMethod> lPair in this.mMethods )
            {
                lPair.Value.Target = this;
            }

            foreach ( KeyValuePair<string, SceneNodeMethod> lPair in pOther.mMethods )
            {
                lPair.Value.Target = pOther;
            }

            SceneManager lSceneService = ServiceManager.Instance.GetService<SceneManager>();
            if ( lSceneService != null )
            {
                lSceneService.ClearNodeMap();
            }
        }

        /// <summary>
        /// Updates the local to world transform of that scene node.
        /// </summary>
        /// <param name="pParent">The parent scene node.</param>
        internal void UpdateLocalToWorldTransform(SceneNode pParent)
        {
            if ( pParent != null )
            {
                this.mLocalToWorldTransform = pParent.mLocalToWorldTransform.Multiply( this.mLocalToParentTransform );
            }

            // Updates children local to world transforms.
            for ( int lCurr = 0; lCurr < this.mChildren.Count; lCurr++ )
            {
                this.mChildren[ lCurr ].UpdateLocalToWorldTransform( this );
            }

            this.mWorldBounds = this.mLocalToWorldTransform.Multiply( this.mLocalBounds );
            this.mWorldCoordinates = this.mLocalToWorldTransform.Multiply( Vector3F.ZERO );

            // Enlarges the world bounds by the children world bounds
            for ( int lCurr = 0; lCurr < this.mChildren.Count; lCurr++ )
            {
                this.mWorldBounds = this.mWorldBounds.Merge( this.mChildren[ lCurr ].mWorldBounds ) as Box3F;
            }

            this.mIsWorldToLocalValid = false;
        }

        /// <summary>
        /// Updates the local to camera and to screen transforms of that scene node.
        /// </summary>
        /// <param name="pWorldToCameraTransform">The world to camera transform.</param>
        /// <param name="pCameraToScreenTransform">The camera to screen space transform.</param>
        internal void UpdateLocalToCameraTransform(AMatrix<float> pWorldToCameraTransform, AMatrix<float> pCameraToScreenTransform)
        {
            this.mLocalToCameraTransform = pWorldToCameraTransform.Multiply( this.mLocalToWorldTransform );
            this.mLocalToScreenTransform = pCameraToScreenTransform.Multiply( this.mLocalToCameraTransform );

            // Updates children local to camera and screen transforms.
            for ( int lCurr = 0; lCurr < this.mChildren.Count; lCurr++ )
            {
                this.mChildren[ lCurr ].UpdateLocalToCameraTransform( pWorldToCameraTransform, pCameraToScreenTransform );
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            this.mFlags.Clear();

            foreach ( SceneNode lChild in this.mChildren )
            {
                lChild.Dispose();
            }
            this.mChildren.Clear();

            foreach ( KeyValuePair<string, SceneNodeMethod> lPair in this.mMethods )
            {
                lPair.Value.Dispose();
            }
            this.mMethods.Clear();

            foreach ( KeyValuePair<string, MeshBuffers> lPair in this.mMeshes )
            {
                lPair.Value.Dispose();
            }
            this.mMeshes.Clear();

            this.mValues.Clear();

            foreach ( KeyValuePair<string, PipelineDescription> lPair in this.mDescriptions )
            {
                lPair.Value.Dispose();
            }
            this.mDescriptions.Clear();

            this.mCreator = null;
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
