﻿using OrcNet.Core;
using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.SceneGraph
{
    /// <summary>
    /// Attachment target structure definition owning a frame buffer
    /// attachment specification.
    /// </summary>
    public struct AttachmentTarget : IMemoryProfilable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the attachment point.
        /// </summary>
        public BufferId AttachmentPoint
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the texture to be attached to the attachment point.
        /// </summary>
        public UniformQualifier Texture
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the mipmap level of the texture to be attached.
        /// </summary>
        public int Level
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the layer, depth or cube face of the texture to be attached.
        /// </summary>
        public int Layer
        {
            get;
            set;
        }

        #region Properties IMemoryProfilable

        /// <summary>
        /// Gets the size of the object.
        /// </summary>
        public uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(BufferId);
                lSize += sizeof(int) * 2;
                lSize += this.Texture.Size;
                return lSize;
            }
        }

        /// <summary>
        /// Gets the object name.
        /// </summary>
        public string TypeName
        {
            get
            {
                return this.GetType().Name;
            }
        }

        /// <summary>
        /// Gets the object namespace.
        /// </summary>
        public string TypeNamespace
        {
            get
            {
                return this.GetType().Namespace;
            }
        }

        /// <summary>
        /// Gets the assembly name this object belongs to.
        /// </summary>
        public string AssemblyName
        {
            get
            {
                return this.GetType().Assembly.GetName().Name;
            }
        }

        #endregion Properties IMemoryProfilable

        #endregion Properties
    }
}
