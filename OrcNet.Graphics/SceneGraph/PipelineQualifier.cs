﻿using OrcNet.Core.SceneGraph;

namespace OrcNet.Graphics.SceneGraph
{
    /// <summary>
    /// Pipeline Qualifier structure definition owning the pair
    /// of the form [Target].[PipelineDescription]
    /// </summary>
    public class PipelineQualifier : MethodQualifier
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineQualifier"/> class.
        /// </summary>
        /// <param name="pQualifierPair">The qualifier pair of the form [Target].[PipelineDescription]</param>
        public PipelineQualifier(string pQualifierPair) :
        base( pQualifierPair )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineQualifier"/> class.
        /// </summary>
        /// <param name="pTarget">The pipeline description owner.</param>
        /// <param name="pName">The pipeline description instance.</param>
        public PipelineQualifier(string pTarget, string pName) :
        base( pTarget, pName )
        {
            
        }

        #endregion Constructor
    }
}
