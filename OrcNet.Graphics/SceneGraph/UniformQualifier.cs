﻿using OrcNet.Core.SceneGraph;

namespace OrcNet.Graphics.SceneGraph
{
    /// <summary>
    /// Uniform Qualifier structure definition owning the pair
    /// of the form [Target].[Uniform] or [Target].[PipelineDescription:Uniform]
    /// </summary>
    public class UniformQualifier : MethodQualifier
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformQualifier"/> class.
        /// </summary>
        /// <param name="pQualifierPair">The qualifier pair of the form [Target].[Uniform] or [Target].[PipelineDescription:Uniform]</param>
        public UniformQualifier(string pQualifierPair) :
        base( pQualifierPair )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UniformQualifier"/> class.
        /// </summary>
        /// <param name="pTarget">The uniform owner. [Target]</param>
        /// <param name="pName">The uniform instance name or with an extra pipeline description refering that uniform. [Uniform] or [PipelineDescription:Uniform]</param>
        public UniformQualifier(string pTarget, string pName) :
        base( pTarget, pName )
        {

        }

        #endregion Constructor
    }
}
