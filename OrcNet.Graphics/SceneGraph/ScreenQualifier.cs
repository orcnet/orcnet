﻿using OrcNet.Core.SceneGraph;

namespace OrcNet.Graphics.SceneGraph
{
    /// <summary>
    /// Screen Qualifier structure definition owning the pair
    /// of the form [Target].[ScreenName]
    /// An empty screen name means the "real" screen space of the camera node.
    /// </summary>
    public class ScreenQualifier : MethodQualifier
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenQualifier"/> class.
        /// </summary>
        /// <param name="pQualifierPair">The qualifier pair of the form [Target].[ScreenName]</param>
        public ScreenQualifier(string pQualifierPair) :
        base( pQualifierPair )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenQualifier"/> class.
        /// </summary>
        /// <param name="pTarget">The owner being the camera node if no name.</param>
        /// <param name="pName">The screen name if any.</param>
        public ScreenQualifier(string pTarget, string pName) :
        base( pTarget, pName )
        {
            
        }

        #endregion Constructor
    }
}
