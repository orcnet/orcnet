﻿namespace OrcNet.Graphics.Mesh
{
    /// <summary>
    /// Mesh usage enumeration.
    /// </summary>
    public enum MeshUsage
    {
        /// <summary>
        /// Uses CPU.
        /// </summary>
        CPU = 0,

        /// <summary>
        /// Uses GPU Static
        /// </summary>
        GPU_STATIC,

        /// <summary>
        /// Uses GPU Dynamic.
        /// </summary>
        GPU_DYNAMIC,

        /// <summary>
        /// Uses GPU Streaming.
        /// </summary>
        GPU_STREAM,

        /// <summary>
        /// The overall amount of usages.
        /// </summary>
        COUNT
    }
}
