﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Render;
using OrcNet.Core.Render.Generic;
using OrcNet.Graphics.Render;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Mesh
{
    /// <summary>
    /// Mesh class definition wrapping the mesh buffers defining
    /// the mesh content.
    /// </summary>
    /// <typeparam name="V">The vertices type.</typeparam>
    /// <typeparam name="I">The indices type.</typeparam>
    [DebuggerDisplay("Primitive = {Mode}, VertexCount = {VertexCount}, IndexCount = {IndexCount}, MeshBuffers = {Buffers}")]
    public class Mesh<V, I> : AMemoryProfilable, IMesh<V, I> where V : struct // Struct to be able to determine the size.
                                                             where I : struct
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the mesh buffers wrapped by this mesh.
        /// </summary>
        private MeshBuffers mBuffers;

        /// <summary>
        /// Stores the primitive topology determining how vertices must be interpreted.
        /// </summary>
        private PrimitiveType mMode;

        /// <summary>
        /// Stores the data draw usage.
        /// </summary>
        private MeshUsage mDataUsage;

        /// <summary>
        /// Stores the vertex buffer.
        /// </summary>
        private IBuffer mVertexBuffer;

        /// <summary>
        /// Stores the handle that provides the raw vertices IntPtr
        /// </summary>
        private GCHandle mRawVerticesPtr;

        /// <summary>
        /// Stores the raw set of vertices.
        /// </summary>
        private V[]     mRawVertices;

        /// <summary>
        /// Stores the actual vertex count different from the allocated one.
        /// </summary>
        private int     mVertexCount;

        /// <summary>
        /// Stores the handle that provides the raw indices IntPtr
        /// </summary>
        private GCHandle mRawIndicesPtr;

        /// <summary>
        /// Stores the raw set of indices.
        /// </summary>
        private I[]     mRawIndices;

        /// <summary>
        /// Stores the actual index count different from the allocated one.
        /// </summary>
        private int     mIndexCount;

        /// <summary>
        /// Stores the index buffer.
        /// </summary>
        private IBuffer mIndexBuffer;

        /// <summary>
        /// Stores the vertex index used for primitive restart, -1 meaning no restart.
        /// </summary>
        private int mPrimitiveRestart;

        /// <summary>
        /// Stores the number of vertices per patch in the mesh BUT only if PrimitiveType is PATCHES.
        /// </summary>
        private int mVerticesPerPatch;

        /// <summary>
        /// Stores the flag indicating whether the vertex buffer is invalid due to vertices changes.
        /// Induces GPU data refresh need.
        /// </summary>
        private bool mIsVertexBufferInvalid;

        /// <summary>
        /// Stores the flag indicating whether the index buffer is invalid due to indices changes.
        /// Induces GPU data refresh need.
        /// </summary>
        private bool mIsIndexBufferInvalid;

        /// <summary>
        /// Stores the flag indicating whether the mesh buffers have been created or not, thus is ready to be used.
        /// </summary>
        private bool mIsValid;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                if
                    ( this.mBuffers != null )
                {
                    lSize += this.mBuffers.Size;
                }
                lSize += sizeof(PrimitiveType);
                lSize += sizeof(MeshUsage);
                lSize += sizeof(int) * 4;
                lSize += sizeof(bool) * 3;

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the mesh buffers wrapped by this mesh.
        /// </summary>
        public MeshBuffers Buffers
        {
            get
            {
                if
                    ( this.mIsValid == false )
                {
                    this.CreateBuffers();
                }

                if
                    ( this.mDataUsage == MeshUsage.GPU_DYNAMIC ||
                      this.mDataUsage == MeshUsage.GPU_STREAM ) // Upload data if needed.
                {
                    BufferUsageHint lUsage = this.mDataUsage == MeshUsage.GPU_DYNAMIC ? BufferUsageHint.DynamicDraw : BufferUsageHint.StreamDraw;
                    if
                        ( this.mIsVertexBufferInvalid )
                    {
                        this.UploadVerticesToGPU( lUsage );
                    }

                    if
                        ( this.mIndexCount > 0 && 
                          this.mIsIndexBufferInvalid )
                    {
                        this.UploadIndicesToGPU( lUsage );
                    }
                }

                this.mBuffers.PrimitiveRestart = this.mPrimitiveRestart;
                this.mBuffers.VerticesPerPatch = this.mVerticesPerPatch;

                return this.mBuffers;
            }
        }

        /// <summary>
        /// Gets the primitive topology determining how vertices must be interpreted.
        /// </summary>
        public PrimitiveType Mode
        {
            get
            {
                return this.mMode;
            }
            set
            {
                if
                    ( this.mMode != value )
                {
                    this.mMode = value;
                }
            }
        }

        /// <summary>
        /// Gets the actual vertex count different from the allocated one.
        /// </summary>
        public int VertexCount
        {
            get
            {
                return this.mVertexCount;
            }
        }

        /// <summary>
        /// Gets the actual index count different from the allocated one.
        /// </summary>
        public int IndexCount
        {
            get
            {
                return this.mIndexCount;
            }
        }

        /// <summary>
        /// Gets the vertex index used for primitive restart, -1 meaning no restart.
        /// </summary>
        public int PrimitiveRestart
        {
            get
            {
                return this.mPrimitiveRestart;
            }
            set
            {
                if
                    ( this.mPrimitiveRestart != value )
                {
                    this.mPrimitiveRestart = value;
                }
            }
        }

        /// <summary>
        /// Gets the number of vertices per patch in the mesh BUT only if PrimitiveType is PATCHES.
        /// </summary>
        public int VerticesPerPatch
        {
            get
            {
                return this.mVerticesPerPatch;
            }
            set
            {
                if
                    ( this.mVerticesPerPatch != value )
                {
                    this.mVerticesPerPatch = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the vertex at the given array index.
        /// </summary>
        /// <param name="pIndex">The array index of the requested vertex.</param>
        /// <returns>The vertex.</returns>
        public V this[VertexIndex pIndex]
        {
            get
            {
                return this.mRawVertices[ pIndex ];
            }
            set
            {
                this.mRawVertices[ pIndex ] = value;
                this.mIsVertexBufferInvalid = true;
            }
        }

        /// <summary>
        /// Gets or sets the index at the given array index.
        /// </summary>
        /// <param name="pIndex">The array index of the requested index.</param>
        /// <returns>The index.</returns>
        public I this[IndexIndex pIndex]
        {
            get
            {
                return this.mRawIndices[ pIndex ];
            }
            set
            {
                this.mRawIndices[ pIndex ] = value;
                this.mIsIndexBufferInvalid = true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Mesh{V, I}"/> class.
        /// </summary>
        /// <param name="pMode">The primitive topology determining how vertices must be interpreted.</param>
        /// <param name="pUsage">The data draw usage.</param>
        /// <param name="pVertexCount">The amount of vertices.</param>
        /// <param name="pIndexCount">The amount of indices.</param>
        public Mesh(PrimitiveType pMode, MeshUsage pUsage, int pVertexCount = 4, int pIndexCount = 4) :
        this( new MeshBuffers(), pMode, pUsage, pVertexCount, pIndexCount )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Mesh{V, I}"/> class.
        /// </summary>
        /// <param name="pTarget">The mesh buffers wrapped by this mesh.</param>
        /// <param name="pMode">The primitive topology determining how vertices must be interpreted.</param>
        /// <param name="pUsage">The data draw usage.</param>
        /// <param name="pVertexCount">The amount of vertices.</param>
        /// <param name="pIndexCount">The amount of indices.</param>
        public Mesh(MeshBuffers pTarget, PrimitiveType pMode, MeshUsage pUsage, int pVertexCount = 4, int pIndexCount = 4)
        {
            this.mMode = pMode;
            this.mDataUsage = pUsage;
            this.mBuffers   = pTarget;
            this.mRawVertices = new V[ pVertexCount ];
            this.mVertexCount = 0;
            this.mRawIndices  = new I[ pIndexCount ];
            this.mIndexCount  = 0;
            this.mPrimitiveRestart = -1;
            this.mVerticesPerPatch = 0;
            this.mIsValid = false;
            this.mIsVertexBufferInvalid = true;
            this.mIsIndexBufferInvalid  = true;
            this.mVertexBuffer = null;
            this.mIndexBuffer  = null;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new vertex to that mesh.
        /// </summary>
        /// <param name="pNewVertex">The new vertex.</param>
        public void AddVertex(V pNewVertex)
        {
            if ( this.mVertexCount == this.mRawVertices.Length )
            {
                this.ResizeVertices( 2 * this.mRawVertices.Length );
            }

            this.mRawVertices[ this.mVertexCount++ ] = pNewVertex;
            this.mIsVertexBufferInvalid = true;
        }

        /// <summary>
        /// Adds a set of vertices to that mesh.
        /// </summary>
        /// <param name="pNewVertices">The new vertices.</param>
        public void AddVertices(V[] pNewVertices)
        {
            for ( int lCurrVertex = 0; lCurrVertex < pNewVertices.Length; lCurrVertex++ )
            {
                this.AddVertex( pNewVertices[ lCurrVertex ] );
            }
        }

        /// <summary>
        /// Adds a new index to that mesh.
        /// </summary>
        /// <param name="pIndex">The new index.</param>
        public void AddIndex(I pIndex)
        {
            if ( this.mIndexCount == this.mRawIndices.Length )
            {
                this.ResizeIndices( 2 * this.mRawIndices.Length );
            }

            this.mRawIndices[ this.mIndexCount++ ] = pIndex;
            this.mIsIndexBufferInvalid = true;
        }

        /// <summary>
        /// Adds a new attribute for vertices of that mesh.
        /// </summary>
        /// <param name="pVertexId">The vertex attribute index.</param>
        /// <param name="pComponentCount">The number of components in that kind of attribute</param>
        /// <param name="pType">The attribute type.</param>
        /// <param name="pMustNormalize">The flag indicating whether the attribute must be normalized or not.</param>
        public void AddAttributeType(int pVertexId, int pComponentCount, AttributeType pType, bool pMustNormalize)
        {
            this.mBuffers.AddAttributeBuffer( pVertexId, pComponentCount, Marshal.SizeOf( typeof(V) ), pType, pMustNormalize );
        }

        /// <summary>
        /// Resizes the vertices and indices caches but only if not smaller than current ones.
        /// </summary>
        /// <param name="pVertexCount">The new vertex count.</param>
        /// <param name="pIndexCount">The new index count.</param>
        /// <returns>True if resized, false otherwise.</returns>
        public bool Resize(int pVertexCount, int pIndexCount)
        {
            bool lResult = false;
            if ( this.mVertexCount < pVertexCount )
            {
                this.ResizeVertices( pVertexCount );
                lResult = true;
            }

            if ( this.mIndexCount < pIndexCount )
            {
                this.ResizeIndices( pIndexCount );
                lResult = true;
            }

            return lResult;
        }

        /// <summary>
        /// Clears the mesh's vertices and indices caches.
        /// </summary>
        public void Clear()
        {
            this.mVertexCount = 0;
            this.mIndexCount  = 0;
            this.mIsVertexBufferInvalid = true;
            this.mIsIndexBufferInvalid  = true;

            if ( this.mIsValid )
            {
                this.mBuffers.Reset();
                this.mBuffers.IndicesBuffer = null;
                this.mIsValid = false;
            }
        }

        /// <summary>
        /// Clears the mesh's buffers.
        /// </summary>
        public void ClearBuffers()
        {
            if ( this.mIsValid )
            {
                this.mBuffers.Reset();
                this.mIsValid = false;
            }
        }

        #region Methods Internal

        /// <summary>
        /// Creates the CPU or GPU buffers based on the current content
        /// of the vertex and index arrays.
        /// </summary>
        private void CreateBuffers()
        {
            if ( this.mVertexBuffer != null )
            {
                if ( this.mRawVerticesPtr.IsAllocated )
                {
                    this.mRawVerticesPtr.Free();
                }
                this.mVertexBuffer.Dispose();
            }

            if ( this.mIndexBuffer != null )
            {
                if ( this.mRawIndicesPtr.IsAllocated )
                {
                    this.mRawIndicesPtr.Free();
                }
                this.mIndexBuffer.Dispose();
            }

            if ( this.mDataUsage == MeshUsage.GPU_STATIC ||
                 this.mDataUsage == MeshUsage.GPU_DYNAMIC ||
                 this.mDataUsage == MeshUsage.GPU_STREAM )
            {
                this.mVertexBuffer = new GPUBuffer();
                if ( this.mDataUsage == MeshUsage.GPU_STATIC )
                {
                    this.UploadVerticesToGPU( BufferUsageHint.StaticDraw );
                }
            }
            else if ( this.mDataUsage == MeshUsage.CPU )
            {
                this.mRawVerticesPtr = GCHandle.Alloc( this.mRawVertices, GCHandleType.Pinned );
                this.mVertexBuffer = new CPUBuffer( this.mRawVerticesPtr.AddrOfPinnedObject() );
            }

            int lVertexAttributeCount = this.mBuffers.AttributesPerVertex;
            if ( lVertexAttributeCount > 0 )
            {
                for ( uint lCurrAttribute = 0; lCurrAttribute < lVertexAttributeCount; lCurrAttribute++ )
                {
                    PipelineAttributeDescription lAttribute = this.mBuffers[ lCurrAttribute ];
                    lAttribute.Data = this.mVertexBuffer;
                }
            }

            if ( this.mIndexCount > 0 )
            {
                if ( this.mDataUsage == MeshUsage.GPU_STATIC ||
                     this.mDataUsage == MeshUsage.GPU_DYNAMIC ||
                     this.mDataUsage == MeshUsage.GPU_STREAM )
                {
                    this.mIndexBuffer = new GPUBuffer();
                    if ( this.mDataUsage == MeshUsage.GPU_STATIC )
                    {
                        this.UploadIndicesToGPU( BufferUsageHint.StaticDraw );
                    }
                }
                else if ( this.mDataUsage == MeshUsage.CPU )
                {
                    this.mRawIndicesPtr = GCHandle.Alloc( this.mRawIndices, GCHandleType.Pinned );
                    this.mIndexBuffer = new CPUBuffer( this.mRawIndicesPtr.AddrOfPinnedObject() );
                }

                AttributeType lType = AttributeType.UINT_BASED;
                switch ( Marshal.SizeOf( typeof( I ) ) )
                {
                    case 1:
                        {
                            lType = AttributeType.BYTE_BASED;
                        }
                        break;
                    case 2:
                        {
                            lType = AttributeType.UINT16_BASED;
                        }
                        break;
                }
                this.mBuffers.IndicesBuffer = new PipelineAttributeDescription( 0, 1, lType, false, this.mIndexBuffer );
            }

            this.mBuffers.PrimitiveMode = this.mMode;
            this.mBuffers.VertexCount   = this.mVertexCount;
            this.mBuffers.IndexCount    = this.IndexCount;

            this.mIsValid = true;
        }

        /// <summary>
        /// Resizes the raw vertices buffer.
        /// </summary>
        /// <param name="pNewSize">The new size.</param>
        private void ResizeVertices(int pNewSize)
        {
            int lOldSize = this.mRawVertices.Length;
            V[] lNewVertices = new V[ pNewSize ];
            Array.Copy( this.mRawVertices, lNewVertices, lOldSize );
            this.mRawVertices = lNewVertices;

            if ( this.mIsValid )
            {
                this.mBuffers.Reset();
                this.mIsValid = false;
            }
        }

        /// <summary>
        /// Resizes the raw indices buffer.
        /// </summary>
        /// <param name="pNewSize">The new size.</param>
        private void ResizeIndices(int pNewSize)
        {
            int lOldSize = this.mRawIndices.Length;
            I[] lNewIndices = new I[ pNewSize ];
            Array.Copy( this.mRawIndices, lNewIndices, lOldSize );
            this.mRawIndices = lNewIndices;

            if ( this.mIsValid )
            {
                this.mBuffers.Reset();
                this.mIsValid = false;
            }
        }

        /// <summary>
        /// Sends the vertices to the GPU.
        /// </summary>
        /// <param name="pUsage">The buffer usage.</param>
        private void UploadVerticesToGPU(BufferUsageHint pUsage)
        {
            GPUBuffer lVertexBuffer = this.mVertexBuffer as GPUBuffer;
            if ( lVertexBuffer != null ) // Only if GPU buffer.
            {
                if ( this.mRawVerticesPtr.IsAllocated )
                {
                    this.mRawVerticesPtr.Free();
                }
                
                int lVertexSize = Marshal.SizeOf( typeof( V ) );
                this.mRawVerticesPtr = GCHandle.Alloc( this.mRawVertices, GCHandleType.Pinned );
                lVertexBuffer.SetData( this.mVertexCount * lVertexSize, this.mRawVerticesPtr.AddrOfPinnedObject(), pUsage );
                this.mIsVertexBufferInvalid = false;
            }
        }

        /// <summary>
        /// Sends the indices to the GPU.
        /// </summary>
        /// <param name="pUsage">The buffer usage.</param>
        private void UploadIndicesToGPU(BufferUsageHint pUsage)
        {
            GPUBuffer lIndexBuffer = this.mIndexBuffer as GPUBuffer;
            if ( lIndexBuffer != null ) // Only if GPU buffer.
            {
                if ( this.mRawIndicesPtr.IsAllocated )
                {
                    this.mRawIndicesPtr.Free();
                }

                int lIndexSize = Marshal.SizeOf( typeof( I ) );
                this.mRawIndicesPtr = GCHandle.Alloc( this.mRawIndices, GCHandleType.Pinned );
                lIndexBuffer.SetData( this.mIndexCount * lIndexSize, this.mRawIndicesPtr.AddrOfPinnedObject(), pUsage );
                this.mIsIndexBufferInvalid = false;
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                if ( this.mVertexBuffer != null )
                {
                    if ( this.mRawVerticesPtr.IsAllocated )
                    {
                        this.mRawVerticesPtr.Free();
                    }
                    this.mVertexBuffer.Dispose();
                    this.mVertexBuffer = null;
                }

                if ( this.mIndexBuffer != null )
                {
                    if ( this.mRawIndicesPtr.IsAllocated )
                    {
                        this.mRawIndicesPtr.Free();
                    }
                    this.mIndexBuffer.Dispose();
                    this.mIndexBuffer = null;
                }

                if ( this.mBuffers != null )
                {
                    this.mBuffers.Dispose();
                    this.mBuffers = null;
                }

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
