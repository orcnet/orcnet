﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Core.Helpers;
using OrcNet.Core.Math;
using OrcNet.Core.Render;
using OrcNet.Core.Resource;
using OrcNet.Core.Service;
using OrcNet.Graphics.Helpers;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.Graphics.Mesh
{
    /// <summary>
    /// Mesh buffers class definition containing a set of PipelineAttributeDescription
    /// that represent the vertices and indices of a mesh, each attribute 
    /// description being an attribute of a vertex of the mesh such as Position,
    /// Normal, Color and so on.
    /// It can then have an indices array that allows vertices to be shared
    /// between adjacent primitives in memory.
    /// If no indices array, shared vertices will be duplicated in all vertices arrays.
    /// </summary>
    [DebuggerDisplay("Primitive = {PrimitiveMode}, VertexCount = {VertexCount}, IndexCount = {IndexCount}, AttributeCount = {AttributesPerVertex}")]
    public class MeshBuffers : AMemoryProfilable, IMeshBuffers
    {
        #region Delegates

        /// <summary>
        /// Simple default attribute value setter prototype definition.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        public delegate void SimpleDefaultAttributeValueSetter(uint pIndex, object pDefaultValue);

        /// <summary>
        /// Multi default attribute value setter prototype definition.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        public delegate void MultiDefaultAttributeValueSetter(uint pIndex, object[] pDefaultValues, bool pMustNormalize);

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the simple setter delegates to set default vertex attribute value by type.
        /// </summary>
        private static Dictionary<Type, SimpleDefaultAttributeValueSetter> sSimpleSetters;

        /// <summary>
        /// Stores the multi setter delegates to set default vertex attribute values by type.
        /// </summary>
        private static Dictionary<Type, Dictionary<uint, MultiDefaultAttributeValueSetter>> sMultiSetters;

        /// <summary>
        /// Stores the flag indicating whether the buffer has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the Mesh buffers creator resource.
        /// </summary>
        private IResource mCreator;

        /// <summary>
        /// Stores the buffer(s) of vertices attributes.
        /// </summary>
        private List<PipelineAttributeDescription> mVertexAttributeBuffers;

        /// <summary>
        /// Stores the buffer of indices.
        /// </summary>
        private PipelineAttributeDescription mIndicesBuffer;

        /// <summary>
        /// Stores the bounding box.
        /// </summary>
        private Box3F mBounds;

        /// <summary>
        /// Stores how vertices must be interpreted.
        /// </summary>
        private PrimitiveType mPrimitiveMode;

        /// <summary>
        /// Stores the vertex count.
        /// </summary>
        private int mVertexCount;

        /// <summary>
        /// Stores the index count.
        /// </summary>
        private int mIndexCount;

        /// <summary>
        /// Stores the vertex index for primitive restart.
        /// </summary>
        private int mPrimitiveRestart;

        /// <summary>
        /// Stores the amount of vertices per patch but ONLY if PrimitiveType is Patches.
        /// </summary>
        private int mVerticesPerPatch;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                int lVertexAttributeCount = this.mVertexAttributeBuffers.Count;
                if
                    ( lVertexAttributeCount > 0 )
                {
                    for
                        ( int lCurr = 0; lCurr < lVertexAttributeCount; lCurr++ )
                    {
                        lSize += this.mVertexAttributeBuffers[ lCurr ].Size;
                    }
                }
                if
                    ( this.mIndicesBuffer != null )
                {
                    lSize += this.mIndicesBuffer.Size;
                }
                lSize += sizeof(float) * 6; // Bounds.
                lSize += sizeof(PrimitiveType);
                lSize += sizeof(int) * 4;

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        public IResource Creator
        {
            get
            {
                return this.mCreator;
            }
            set
            {
                this.mCreator = value;
            }
        }

        #endregion Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the flag indicating how vertices must be interpreted.
        /// </summary>
        public PrimitiveType PrimitiveMode
        {
            get
            {
                return this.mPrimitiveMode;
            }
            set
            {
                if
                    ( this.mPrimitiveMode != value )
                {
                    this.mPrimitiveMode = value;
                }
            }
        }
        
        /// <summary>
        /// Gets or sets the vertex index for primitive restart.
        /// </summary>
        public int PrimitiveRestart
        {
            get
            {
                return this.mPrimitiveRestart;
            }
            set
            {
                if
                    ( this.mPrimitiveRestart != value )
                {
                    this.mPrimitiveRestart = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount of vertices per patch but ONLY if PrimitiveType is Patches.
        /// </summary>
        public int VerticesPerPatch
        {
            get
            {
                return this.mVerticesPerPatch;
            }
            set
            {
                if
                    ( this.mVerticesPerPatch != value )
                {
                    this.mVerticesPerPatch = value;
                }
            }
        }

        /// <summary>
        /// Gets the attribute buffer at the given index.
        /// </summary>
        /// <param name="pIndex">The buffer index.</param>
        /// <returns>The attribute buffer, null if beyond bounds.</returns>
        public PipelineAttributeDescription this[uint pIndex]
        {
            get
            {
                int lIndex = (int)pIndex;
                if
                    ( lIndex < this.mVertexAttributeBuffers.Count )
                {
                    return this.mVertexAttributeBuffers[ lIndex ];
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the indices buffer.
        /// </summary>
        public PipelineAttributeDescription IndicesBuffer
        {
            get
            {
                return this.mIndicesBuffer;
            }
            set
            {
                if
                    ( this.mIndicesBuffer != null )
                {
                    this.mIndicesBuffer.Dispose();
                }

                this.mIndicesBuffer = value;
            }
        }

        /// <summary>
        /// Gets or sets the bounding box.
        /// </summary>
        public Box3F Bounds
        {
            get
            {
                return this.mBounds;
            }
            set
            {
                this.mBounds = value;
            }
        }

        #region Properties IMeshBuffers

        /// <summary>
        /// Gets or sets the vertex count
        /// </summary>
        public int VertexCount
        {
            get
            {
                return this.mVertexCount;
            }
            set
            {
                if
                    ( this.mVertexCount != value )
                {
                    this.mVertexCount = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the index count
        /// </summary>
        public int IndexCount
        {
            get
            {
                return this.mIndexCount;
            }
            set
            {
                if
                    ( this.mIndexCount != value )
                {
                    this.mIndexCount = value;
                }
            }
        }

        /// <summary>
        /// Gets the number of  attributes per vertex.
        /// </summary>
        public int AttributesPerVertex
        {
            get
            {
                return this.mVertexAttributeBuffers.Count;
            }
        }

        /// <summary>
        /// Gets the attribute buffer at the given index.
        /// </summary>
        /// <param name="pIndex">The buffer index.</param>
        /// <returns>The attribute buffer, null if beyond bounds.</returns>
        IPipelineAttributeDescription IMeshBuffers.this[uint pIndex]
        {
            get
            {
                return this[ pIndex ];
            }
        }

        /// <summary>
        /// Gets the indices buffer.
        /// </summary>
        IPipelineAttributeDescription IMeshBuffers.IndicesBuffer
        {
            get
            {
                return this.IndicesBuffer;
            }
            set
            {
                this.mIndicesBuffer = value as PipelineAttributeDescription;
            }
        }

        /// <summary>
        /// Gets or sets the bounding box.
        /// </summary>
        IBox<float> IMeshBuffers.Bounds
        {
            get
            {
                return this.Bounds;
            }
            set
            {
                this.Bounds = value as Box3F;
            }
        }

        #endregion Properties IMeshBuffers

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes the static member(s) of the <see cref="MeshBuffers"/> class.
        /// </summary>
        static MeshBuffers()
        {
            sSimpleSetters = new Dictionary<Type, SimpleDefaultAttributeValueSetter>();
            sSimpleSetters.Add( typeof(short),    SetDefaultAttribute1s );
            sSimpleSetters.Add( typeof(float),    SetDefaultAttribute1f );
            sSimpleSetters.Add( typeof(Vector2F),  SetDefaultAttribute2f );
            sSimpleSetters.Add( typeof(Vector3F),  SetDefaultAttribute3f );
            sSimpleSetters.Add( typeof(Vector4F),  SetDefaultAttribute4f );
            sSimpleSetters.Add( typeof(double),   SetDefaultAttribute1d );
            sSimpleSetters.Add( typeof(Vector2D), SetDefaultAttribute2d );
            sSimpleSetters.Add( typeof(Vector3D), SetDefaultAttribute3d );
            sSimpleSetters.Add( typeof(Vector4D), SetDefaultAttribute4d );
            sSimpleSetters.Add( typeof(int),      SetDefaultAttribute1i );
            sSimpleSetters.Add( typeof(uint),     SetDefaultAttribute1ui );
            sMultiSetters = new Dictionary<Type, Dictionary<uint, MultiDefaultAttributeValueSetter>>();
            Dictionary<uint, MultiDefaultAttributeValueSetter> lShortSettersByLength = new Dictionary<uint, MultiDefaultAttributeValueSetter>();
            lShortSettersByLength.Add( 1, SetDefaultAttribute1s );
            lShortSettersByLength.Add( 2, SetDefaultAttribute2s );
            lShortSettersByLength.Add( 3, SetDefaultAttribute3s );
            lShortSettersByLength.Add( 4, SetDefaultAttribute4s );
            sMultiSetters.Add( typeof(short), lShortSettersByLength );
            Dictionary<uint, MultiDefaultAttributeValueSetter> lFloatSettersByLength = new Dictionary<uint, MultiDefaultAttributeValueSetter>();
            lFloatSettersByLength.Add( 1, SetDefaultAttribute1f );
            lFloatSettersByLength.Add( 2, SetDefaultAttribute2f );
            lFloatSettersByLength.Add( 3, SetDefaultAttribute3f );
            lFloatSettersByLength.Add( 4, SetDefaultAttribute4f );
            sMultiSetters.Add( typeof(float), lFloatSettersByLength );
            Dictionary<uint, MultiDefaultAttributeValueSetter> lDoubleSettersByLength = new Dictionary<uint, MultiDefaultAttributeValueSetter>();
            lDoubleSettersByLength.Add( 1, SetDefaultAttribute1d );
            lDoubleSettersByLength.Add( 2, SetDefaultAttribute2d );
            lDoubleSettersByLength.Add( 3, SetDefaultAttribute3d );
            lDoubleSettersByLength.Add( 4, SetDefaultAttribute4d );
            sMultiSetters.Add( typeof(double), lDoubleSettersByLength );
            Dictionary<uint, MultiDefaultAttributeValueSetter> lSByteSettersByLength = new Dictionary<uint, MultiDefaultAttributeValueSetter>();
            lSByteSettersByLength.Add( 4, SetDefaultAttribute4b );
            sMultiSetters.Add( typeof(sbyte), lSByteSettersByLength );
            Dictionary<uint, MultiDefaultAttributeValueSetter> lByteSettersByLength = new Dictionary<uint, MultiDefaultAttributeValueSetter>();
            lByteSettersByLength.Add( 4, SetDefaultAttribute4ub );
            sMultiSetters.Add( typeof(byte), lByteSettersByLength );
            Dictionary<uint, MultiDefaultAttributeValueSetter> lUShortSettersByLength = new Dictionary<uint, MultiDefaultAttributeValueSetter>();
            lUShortSettersByLength.Add( 4, SetDefaultAttribute4us );
            sMultiSetters.Add( typeof(ushort), lUShortSettersByLength );
            Dictionary<uint, MultiDefaultAttributeValueSetter> lIntSettersByLength = new Dictionary<uint, MultiDefaultAttributeValueSetter>();
            lIntSettersByLength.Add( 4, SetDefaultAttribute4i );
            sMultiSetters.Add( typeof(int), lIntSettersByLength );
            Dictionary<uint, MultiDefaultAttributeValueSetter> lUIntSettersByLength = new Dictionary<uint, MultiDefaultAttributeValueSetter>();
            lUIntSettersByLength.Add( 4, SetDefaultAttribute4ui );
            sMultiSetters.Add( typeof(uint), lUIntSettersByLength );
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="MeshBuffers"/> class.
        /// </summary>
        /// <param name="pMode">The primitive mode.</param>
        /// <param name="pVertexCount">The vertex count</param>
        /// <param name="pIndexCount">The index count</param>
        public MeshBuffers(PrimitiveType pMode = PrimitiveType.Points, uint pVertexCount = 0, uint pIndexCount = 0)
        {
            this.mPrimitiveMode = pMode;
            this.mVertexCount   = (int)pVertexCount;
            this.mIndexCount    = (int)pIndexCount;
            this.mPrimitiveRestart = -1;
            this.mVerticesPerPatch = 0;
            this.mVertexAttributeBuffers = new List<PipelineAttributeDescription>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Swaps the mesh buffer with another.
        /// </summary>
        /// <param name="pOther">The other buffer to swap with.</param>
        public void Swap(MeshBuffers pOther)
        {
            Utilities.Swap( ref this.mPrimitiveMode, ref pOther.mPrimitiveMode );
            Utilities.Swap( ref this.mVertexCount, ref pOther.mVertexCount );
            Utilities.Swap( ref this.mIndexCount, ref pOther.mIndexCount );
            Utilities.Swap( ref this.mBounds, ref pOther.mBounds );
            Utilities.Swap( ref this.mVertexAttributeBuffers, ref pOther.mVertexAttributeBuffers );
            Utilities.Swap( ref this.mIndicesBuffer, ref pOther.mIndicesBuffer );
        }

        /// <summary>
        /// Adds a new vertex attribute buffer.
        /// </summary>
        /// <param name="pNewBuffer">The new attributes buffer.</param>
        public void AddAttributeBuffer(IPipelineAttributeDescription pNewBuffer)
        {
            this.mVertexAttributeBuffers.Add( pNewBuffer as PipelineAttributeDescription );
        }

        /// <summary>
        /// Adds a new vertex attribute buffer.
        /// This method assumes the vertex attribute is stored interleaved with others
        /// in a shared buffer.
        /// E.g. For a mesh having vertices made of a Position, a Normal and a Color attributes,
        ///      the layout will be Position, Normal and Color of the first vertex, then Position,
        ///      Normal and Color of the second, and so on...
        /// </summary>
        /// <param name="pIndex">The vertex attribute index.</param>
        /// <param name="pComponentCount">The vertex attribute component count.</param>
        /// <param name="pVertexSize">The total size of all vertices attributes</param>
        /// <param name="pType">The attribute component type.</param>
        /// <param name="pMustBeNormalized">The flag indicating whether the components must be normalized to 0-1 or not</param>
        public void AddAttributeBuffer(int pIndex, int pComponentCount, int pVertexSize, AttributeType pType, bool pMustBeNormalized)
        {
            int lOffset = 0;
            if ( this.mVertexAttributeBuffers.Count > 0 )
            {
                PipelineAttributeDescription lBuffer = this.mVertexAttributeBuffers[ this.mVertexAttributeBuffers.Count - 1 ];
                lOffset = lBuffer.Offset + lBuffer.AttributeSize;
            }

            PipelineAttributeDescription lNewBuffer = new PipelineAttributeDescription( pIndex, pComponentCount, pType, pMustBeNormalized, null, pVertexSize, lOffset );
            this.AddAttributeBuffer( lNewBuffer );
        }

        /// <summary>
        /// Adds a new vertex attribute buffer.
        /// This method assumes the vertex attribute is stored in its own buffer.
        /// </summary>
        /// <param name="pIndex">The vertex attribute index.</param>
        /// <param name="pComponentCount">The vertex attribute component count.</param>
        /// <param name="pType">The attribute component type.</param>
        /// <param name="pMustBeNormalized">The flag indicating whether the components must be normalized to 0-1 or not</param>
        public void AddAttributeBuffer(int pIndex, int pComponentCount, AttributeType pType, bool pMustBeNormalized)
        {
            PipelineAttributeDescription lNewBuffer = new PipelineAttributeDescription( pIndex, pComponentCount, pType, pMustBeNormalized, null );
            this.AddAttributeBuffer( lNewBuffer );
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default value for the attribute.</param>
        public static void SetDefaultAttributeValue<T>(uint pIndex, T pDefaultValue)
        {
            SimpleDefaultAttributeValueSetter lSetter;
            Type lValueType = pDefaultValue.GetType();
            if ( sSimpleSetters.TryGetValue( lValueType, out lSetter ) )
            {
                lSetter( pIndex, pDefaultValue );
            }
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        public static void SetDefaultAttributeValue<T>(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            if ( pDefaultValues != null &&
                 pDefaultValues.Length > 0 )
            {
                Dictionary<uint, MultiDefaultAttributeValueSetter> lSetterByLength;
                Type lValueType = pDefaultValues[ 0 ].GetType();
                if ( sMultiSetters.TryGetValue( lValueType, out lSetterByLength ) )
                {
                    MultiDefaultAttributeValueSetter lSetter;
                    if ( lSetterByLength.TryGetValue( (uint)pDefaultValues.Length, out lSetter ) )
                    {
                        lSetter( pIndex, pDefaultValues, pMustNormalize );
                    }
                }
            }
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL integer based method.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValues">The default signed integer based values.</param>
        public static void SetDefaultAttributeValueI(uint pIndex, int[] pDefaultValues)
        {
            if ( pDefaultValues != null )
            {
                switch ( pDefaultValues.Length )
                {
                    case 1:
                        GL.VertexAttribI1( pIndex, pDefaultValues[ 0 ] );
                        break;
                    case 2:
                        GL.VertexAttribI2( pIndex, pDefaultValues );
                        break;
                    case 3:
                        GL.VertexAttribI3( pIndex, pDefaultValues );
                        break;
                    case 4:
                        GL.VertexAttribI4( pIndex, pDefaultValues );
                        break;
                }
            }
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL integer based method.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValues">The default unsigned integer based values.</param>
        public static void SetDefaultAttributeValueI(uint pIndex, uint[] pDefaultValues)
        {
            if ( pDefaultValues != null )
            {
                switch ( pDefaultValues.Length )
                {
                    case 1:
                        GL.VertexAttribI1( pIndex, pDefaultValues[ 0 ] );
                        break;
                    case 2:
                        GL.VertexAttribI2( pIndex, pDefaultValues );
                        break;
                    case 3:
                        GL.VertexAttribI3( pIndex, pDefaultValues );
                        break;
                    case 4:
                        GL.VertexAttribI4( pIndex, pDefaultValues );
                        break;
                }
            }
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL integer based method.
        /// Only allowed for 4 sbytes array.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValues">The default sbyte based values.</param>
        public static void SetDefaultAttributeValueI(uint pIndex, sbyte[] pDefaultValues)
        {
            if ( pDefaultValues != null &&
                 pDefaultValues.Length == 4 )
            {
                GL.VertexAttribI4( pIndex, pDefaultValues );
            }
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL integer based method.
        /// Only allowed for 4 bytes array.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValues">The default byte based values.</param>
        public static void SetDefaultAttributeValueI(uint pIndex, byte[] pDefaultValues)
        {
            if ( pDefaultValues != null &&
                 pDefaultValues.Length == 4 )
            {
                GL.VertexAttribI4( pIndex, pDefaultValues );
            }
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL integer based method.
        /// Only allowed for 4 shorts array.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValues">The default short based values.</param>
        public static void SetDefaultAttributeValueI(uint pIndex, short[] pDefaultValues)
        {
            if ( pDefaultValues != null &&
                 pDefaultValues.Length == 4 )
            {
                GL.VertexAttribI4( pIndex, pDefaultValues );
            }
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL integer based method.
        /// Only allowed for 4 ushorts array.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValues">The default ushort based values.</param>
        public static void SetDefaultAttributeValueI(uint pIndex, ushort[] pDefaultValues)
        {
            if ( pDefaultValues != null &&
                 pDefaultValues.Length == 4 )
            {
                GL.VertexAttribI4( pIndex, pDefaultValues );
            }
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL double precision based method.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default double based values.</param>
        public static void SetDefaultAttributeValueL(uint pIndex, double pDefaultValue)
        {
            GL.VertexAttribL1( pIndex, pDefaultValue );
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL double precision based method.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default double based Vector2 value.</param>
        public static void SetDefaultAttributeValueL(uint pIndex, Vector2D pDefaultValue)
        {
            GL.VertexAttribL2( pIndex, pDefaultValue.X, pDefaultValue.Y );
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL double precision based method.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default double based Vector3 value.</param>
        public static void SetDefaultAttributeValueL(uint pIndex, Vector3D pDefaultValue)
        {
            GL.VertexAttribL3( pIndex, pDefaultValue.X, pDefaultValue.Y, pDefaultValue.Z );
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL double precision based method.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default double based Vector4 value.</param>
        public static void SetDefaultAttributeValueL(uint pIndex, Vector4D pDefaultValue)
        {
            GL.VertexAttribL4( pIndex, pDefaultValue.X, pDefaultValue.Y, pDefaultValue.Z, pDefaultValue.W );
        }

        /// <summary>
        /// Sets the default value of the attribute at the given index.
        /// Especially uses the GL double precision based method.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValues">The default double based values.</param>
        public static void SetDefaultAttributeValueL(uint pIndex, double[] pDefaultValues)
        {
            if ( pDefaultValues != null )
            {
                switch ( pDefaultValues.Length )
                {
                    case 1:
                        GL.VertexAttribL1( pIndex, pDefaultValues[ 0 ] );
                        break;
                    case 2:
                        GL.VertexAttribL2( pIndex, pDefaultValues );
                        break;
                    case 3:
                        GL.VertexAttribL3( pIndex, pDefaultValues );
                        break;
                    case 4:
                        GL.VertexAttribL4( pIndex, pDefaultValues );
                        break;
                }
            }
        }

        /// <summary>
        /// Sets the packed default value of the attribute at the given index.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default value for that attribute in packed format.</param>
        /// <param name="pIsSigned">The flag indicating whether the packed data is signed or not.</param>
        /// <param name="pMustNormalize">The flag indicating whether the default value components must be normalized or not.</param>
        public static void SetDefaultAttributeValueP1(uint pIndex, uint pDefaultValue, bool pIsSigned, bool pMustNormalize)
        {
            GL.VertexAttribP1( pIndex, pIsSigned ? PackedPointerType.Int2101010Rev : PackedPointerType.UnsignedInt2101010Rev, pMustNormalize, pDefaultValue );
        }

        /// <summary>
        /// Sets the packed default value of the attribute at the given index.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default value for that attribute in packed format.</param>
        /// <param name="pIsSigned">The flag indicating whether the packed data is signed or not.</param>
        /// <param name="pMustNormalize">The flag indicating whether the default value components must be normalized or not.</param>
        public static void SetDefaultAttributeValueP2(uint pIndex, uint pDefaultValue, bool pIsSigned, bool pMustNormalize)
        {
            GL.VertexAttribP2( pIndex, pIsSigned ? PackedPointerType.Int2101010Rev : PackedPointerType.UnsignedInt2101010Rev, pMustNormalize, pDefaultValue );
        }

        /// <summary>
        /// Sets the packed default value of the attribute at the given index.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default value for that attribute in packed format.</param>
        /// <param name="pIsSigned">The flag indicating whether the packed data is signed or not.</param>
        /// <param name="pMustNormalize">The flag indicating whether the default value components must be normalized or not.</param>
        public static void SetDefaultAttributeValueP3(uint pIndex, uint pDefaultValue, bool pIsSigned, bool pMustNormalize)
        {
            GL.VertexAttribP3( pIndex, pIsSigned ? PackedPointerType.Int2101010Rev : PackedPointerType.UnsignedInt2101010Rev, pMustNormalize, pDefaultValue );
        }

        /// <summary>
        /// Sets the packed default value of the attribute at the given index.
        /// </summary>
        /// <param name="pIndex">The attribute index.</param>
        /// <param name="pDefaultValue">The default value for that attribute in packed format.</param>
        /// <param name="pIsSigned">The flag indicating whether the packed data is signed or not.</param>
        /// <param name="pMustNormalize">The flag indicating whether the default value components must be normalized or not.</param>
        public static void SetDefaultAttributeValueP4(uint pIndex, uint pDefaultValue, bool pIsSigned, bool pMustNormalize)
        {
            GL.VertexAttribP4( pIndex, pIsSigned ? PackedPointerType.Int2101010Rev : PackedPointerType.UnsignedInt2101010Rev, pMustNormalize, pDefaultValue );
        }

        #region Methods Internal

        /// <summary>
        /// Resets the internal state associated with the mesh.
        /// </summary>
        internal void Reset()
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if
                ( lRenderService != null )
            {
                if
                    ( lRenderService.CurrentMeshBuffers == this )
                {
                    lRenderService.UseMeshBuffers( null );
                }
            }
        }

        /// <summary>
        /// Binds the buffers of the mesh to be ready to be drawn.
        /// </summary>
        internal bool Bind(ref AttributeType pIndicesType, ref IntPtr pIndices)
        {
            if
                ( this.mVertexAttributeBuffers.Count > 0 )
            {
                int lAttributeCount = this.mVertexAttributeBuffers.Count;
                for
                    ( int lCurr = lAttributeCount - 1; lCurr >= 0; --lCurr )
                {
                    PipelineAttributeDescription lAttrBuffer = this.mVertexAttributeBuffers[ lCurr ];
                    IBuffer lData = lAttrBuffer.Data;
                    lData.Bind( (int)All.ArrayBuffer );
                    int lIndex = lAttrBuffer.Index;
                    if
                        ( lAttrBuffer.UsesInteger )
                    {
                        GL.VertexAttribIPointer( lIndex, lAttrBuffer.ComponentCount, (VertexAttribIntegerType)lAttrBuffer.Type.ToGLAttributeType(), lAttrBuffer.Stride, lData.GetData( lAttrBuffer.Offset ) );
                    }
                    else if
                        ( lAttrBuffer.UsesDouble )
                    {
                        GL.VertexAttribLPointer( lIndex, lAttrBuffer.ComponentCount, (VertexAttribDoubleType)lAttrBuffer.Type.ToGLAttributeType(), lAttrBuffer.Stride, lData.GetData( lAttrBuffer.Offset ) );
                    }
                    else
                    {
                        GL.VertexAttribPointer( lIndex, lAttrBuffer.ComponentCount, (VertexAttribPointerType)lAttrBuffer.Type.ToGLAttributeType(), lAttrBuffer.MustBeNormalized, lAttrBuffer.Stride, lData.GetData( lAttrBuffer.Offset ) );
                    }
                    GL.VertexAttribDivisor( lIndex, lAttrBuffer.Divisor );
                    GL.EnableVertexAttribArray( lIndex );
                }
                GLHelpers.GetError();

                if
                    ( this.mIndicesBuffer != null )
                {
                    IBuffer lData = this.mIndicesBuffer.Data;
                    lData.Bind( (int)All.ElementArrayBuffer );
                    pIndicesType = this.mIndicesBuffer.Type;
                    pIndices = lData.GetData( this.mIndicesBuffer.Offset );
                }
                GLHelpers.GetError();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Unbinds the buffers of the mesh to allow another mesh to be drawn.
        /// </summary>
        internal void Unbind()
        {
            int lAttributeCount = this.mVertexAttributeBuffers.Count;
            for
                ( int lCurr = lAttributeCount - 1; lCurr >= 0; --lCurr )
            {
                PipelineAttributeDescription lAttrBuffer = this.mVertexAttributeBuffers[ lCurr ];
                GL.DisableVertexAttribArray( lAttrBuffer.Index );
            }
            GLHelpers.GetError();
        }

        /// <summary>
        /// Simple default attribute value setter for Short.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute1s(uint pIndex, object pDefaultValue)
        {
            short lValue = (short)pDefaultValue;
            GL.VertexAttrib1( pIndex, lValue );
        }
        
        /// <summary>
        /// Simple default attribute value setter for float.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute1f(uint pIndex, object pDefaultValue)
        {
            float lValue = (float)pDefaultValue;
            GL.VertexAttrib1( pIndex, lValue );
        }

        /// <summary>
        /// Simple default attribute value setter for float based vector2.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute2f(uint pIndex, object pDefaultValue)
        {
            Vector2F lValue = pDefaultValue as Vector2F;
            GL.VertexAttrib2( pIndex, lValue.X, lValue.Y );
        }

        /// <summary>
        /// Simple default attribute value setter for float based vector3.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute3f(uint pIndex, object pDefaultValue)
        {
            Vector3F lValue = pDefaultValue as Vector3F;
            GL.VertexAttrib3( pIndex, lValue.X, lValue.Y, lValue.Z );
        }

        /// <summary>
        /// Simple default attribute value setter for float based vector4.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute4f(uint pIndex, object pDefaultValue)
        {
            Vector4F lValue = pDefaultValue as Vector4F;
            GL.VertexAttrib4( pIndex, lValue.X, lValue.Y, lValue.Z, lValue.W );
        }

        /// <summary>
        /// Simple default attribute value setter for double.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute1d(uint pIndex, object pDefaultValue)
        {
            double lValue = (double)pDefaultValue;
            GL.VertexAttrib1( pIndex, lValue );
        }

        /// <summary>
        /// Simple default attribute value setter for double based vector2.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute2d(uint pIndex, object pDefaultValue)
        {
            Vector2D lValue = pDefaultValue as Vector2D;
            GL.VertexAttrib2( pIndex, lValue.X, lValue.Y );
        }

        /// <summary>
        /// Simple default attribute value setter for double based vector3.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute3d(uint pIndex, object pDefaultValue)
        {
            Vector3D lValue = pDefaultValue as Vector3D;
            GL.VertexAttrib3( pIndex, lValue.X, lValue.Y, lValue.Z );
        }

        /// <summary>
        /// Simple default attribute value setter for double based vector4.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute4d(uint pIndex, object pDefaultValue)
        {
            Vector4D lValue = pDefaultValue as Vector4D;
            GL.VertexAttrib4( pIndex, lValue.X, lValue.Y, lValue.Z, lValue.W );
        }

        /// <summary>
        /// Simple default attribute value setter for signed int.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute1i(uint pIndex, object pDefaultValue)
        {
            int lValue = (int)pDefaultValue;
            GL.VertexAttribI1( pIndex, lValue );
        }

        /// <summary>
        /// Simple default attribute value setter for unsigned int.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValue">The default value for that vertex attribute</param>
        private static void SetDefaultAttribute1ui(uint pIndex, object pDefaultValue)
        {
            uint lValue = (uint)pDefaultValue;
            GL.VertexAttribI1( pIndex, lValue );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of a single Short
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute1s(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            SetDefaultAttribute1s( pIndex, pDefaultValues[ 0 ] );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 2 Shorts
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute2s(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            short[] lValues = Array.ConvertAll<object, short>( pDefaultValues, pElt => (short)pElt );
            GL.VertexAttrib2( pIndex, lValues );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 3 Shorts
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute3s(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            short[] lValues = Array.ConvertAll<object, short>( pDefaultValues, pElt => (short)pElt );
            GL.VertexAttrib3( pIndex, lValues );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 4 Shorts
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute4s(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            short[] lValues = Array.ConvertAll<object, short>( pDefaultValues, pElt => (short)pElt );
            if
                ( pMustNormalize )
            {
                GL.VertexAttrib4N( pIndex, lValues );
            }
            else
            {
                GL.VertexAttrib4( pIndex, lValues );
            }
        }

        /// <summary>
        /// Multi default attribute value setter for an array of a single float
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute1f(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            SetDefaultAttribute1f( pIndex, pDefaultValues[ 0 ] );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 2 floats
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute2f(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            float[] lValues = Array.ConvertAll<object, float>( pDefaultValues, pElt => (float)pElt );
            GL.VertexAttrib2( pIndex, lValues );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 3 floats
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute3f(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            float[] lValues = Array.ConvertAll<object, float>( pDefaultValues, pElt => (float)pElt );
            GL.VertexAttrib3( pIndex, lValues );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 4 floats
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute4f(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            float[] lValues = Array.ConvertAll<object, float>( pDefaultValues, pElt => (float)pElt );
            GL.VertexAttrib4( pIndex, lValues );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of a single double
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute1d(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            SetDefaultAttribute1d( pIndex, pDefaultValues[ 0 ] );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 2 doubles
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute2d(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            double[] lValues = Array.ConvertAll<object, double>( pDefaultValues, pElt => (double)pElt );
            GL.VertexAttrib2( pIndex, lValues );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 3 doubles
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute3d(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            double[] lValues = Array.ConvertAll<object, double>( pDefaultValues, pElt => (double)pElt );
            GL.VertexAttrib3( pIndex, lValues );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 4 doubles
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute4d(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            double[] lValues = Array.ConvertAll<object, double>( pDefaultValues, pElt => (double)pElt );
            GL.VertexAttrib4( pIndex, lValues );
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 4 signed bytes.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute4b(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            sbyte[] lValues = Array.ConvertAll<object, sbyte>( pDefaultValues, pElt => (sbyte)pElt );
            if
                ( pMustNormalize )
            {
                GL.VertexAttrib4N( pIndex, lValues );
            }
            else
            {
                GL.VertexAttrib4( pIndex, lValues );
            }
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 4 unsigned bytes.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute4ub(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            byte[] lValues = Array.ConvertAll<object, byte>( pDefaultValues, pElt => (byte)pElt );
            if
                ( pMustNormalize )
            {
                GL.VertexAttrib4N( pIndex, lValues );
            }
            else
            {
                GL.VertexAttrib4( pIndex, lValues );
            }
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 4 unsigned Shorts.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute4us(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            ushort[] lValues = Array.ConvertAll<object, ushort>( pDefaultValues, pElt => (ushort)pElt );
            if
                ( pMustNormalize )
            {
                GL.VertexAttrib4N( pIndex, lValues );
            }
            else
            {
                GL.VertexAttrib4( pIndex, lValues );
            }
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 4 signed integers.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute4i(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            int[] lValues = Array.ConvertAll<object, int>( pDefaultValues, pElt => (int)pElt );
            if
                ( pMustNormalize )
            {
                GL.VertexAttrib4N( pIndex, lValues );
            }
            else
            {
                GL.VertexAttrib4( pIndex, lValues );
            }
        }

        /// <summary>
        /// Multi default attribute value setter for an array of 4 unsigned integers.
        /// </summary>
        /// <param name="pIndex">The attribute index the value is for.</param>
        /// <param name="pDefaultValues">The default values for that vertex attribute</param>
        /// <param name="pMustNormalize">The flag indicating whether to normalize the components of DefaultValues or not.</param>
        private static void SetDefaultAttribute4ui(uint pIndex, object[] pDefaultValues, bool pMustNormalize = false)
        {
            uint[] lValues = Array.ConvertAll<object, uint>( pDefaultValues, pElt => (uint)pElt );
            if
                ( pMustNormalize )
            {
                GL.VertexAttrib4N( pIndex, lValues );
            }
            else
            {
                GL.VertexAttrib4( pIndex, lValues );
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                if ( lRenderService != null )
                {
                    if ( lRenderService.CurrentMeshBuffers == this )
                    {
                        lRenderService.UseMeshBuffers( null );
                    }
                }

                int lAttributeCount = this.mVertexAttributeBuffers.Count;
                for ( int lCurr = 0; lCurr < lAttributeCount; lCurr++ )
                {
                    this.mVertexAttributeBuffers[ lCurr ].Dispose();
                }
                this.mVertexAttributeBuffers.Clear();

                if ( this.mIndicesBuffer != null )
                {
                    this.mIndicesBuffer.Dispose();
                    this.mIndicesBuffer = null;
                }

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
