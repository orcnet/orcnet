﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Graphics.SceneGraph;
using OrcNet.Graphics.Task.Factories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Set pass task factory descriptor class definition used to
    /// create SetPassTask factory based resource(s).
    /// </summary>
    public class SetPassTaskResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "setProgram";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += this.Factory.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the set pass task factory.
        /// </summary>
        public SetPassTaskFactory Factory
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPassTaskResourceDescriptor"/> class.
        /// </summary>
        private SetPassTaskResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetPassTaskResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public SetPassTaskResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            List<PipelineQualifier> lDescriptions = new List<PipelineQualifier>();
            bool lSetUniforms = false;
            XAttribute lXSetUniforms = pDescriptor.Attribute( "setUniforms" );
            if
                ( lXSetUniforms != null )
            {
                lSetUniforms = string.CompareOrdinal( lXSetUniforms.Value, "true" ) == 0;
            }

            XNode lCurrent = pDescriptor.FirstNode;
            while
                ( lCurrent != null )
            {
                XElement lElement = lCurrent as XElement;
                if
                    ( lElement != null )
                {
                    if
                        ( string.CompareOrdinal( lElement.Name(), "module" ) != 0 )
                    {
                        LogManager.Instance.Log( string.Format( "Invalid subelement \"{0}\"", lElement.Name() ) );
                    }

                    XAttribute lXName = lElement.Attribute( "name" );
                    if
                        ( lXName == null )
                    {
                        LogManager.Instance.Log( "Missing 'name' attribute" );
                    }
                    else
                    {
                        lDescriptions.Add( new PipelineQualifier( lXName.Value ) );
                    }
                }

                lCurrent = lCurrent.NextNode;
            }

            this.Factory = new SetPassTaskFactory( lDescriptions.ToArray(), lSetUniforms );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new SetPassTaskResource( pName, this );
        }

        #endregion Methods
    }
}
