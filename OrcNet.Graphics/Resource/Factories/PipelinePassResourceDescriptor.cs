﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Core.Service;
using OrcNet.Graphics.Render;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Pipeline pass resource descriptor class definition used to
    /// create pipeline pass based resource(s).
    /// </summary>
    public class PipelinePassResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "program";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += this.Pass.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the pass.
        /// </summary>
        public PipelinePass Pass
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelinePassResourceDescriptor"/> class.
        /// </summary>
        private PipelinePassResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelinePassResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public PipelinePassResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            if ( pContent != null )
            {
                this.Pass = new PipelinePass( pContent, (uint)All.Byte );
                return;
            }

            List<PipelineDescription> lDescriptions = new List<PipelineDescription>();
            XNode lCurrent = pDescriptor.FirstNode;
            while ( lCurrent != null )
            {
                XElement lElement = lCurrent as XElement;
                if ( lElement != null )
                {
                    if ( string.CompareOrdinal( lElement.Name(), "module" ) != 0 )
                    {
                        LogManager.Instance.Log( string.Format( "Invalid subelement {0}", lElement.Name() ), LogType.ERROR );
                    }
                }

                XAttribute lXName = lElement.Attribute( "name" );
                if ( lXName != null )
                {
                    PipelineDescription lDescription = null;
                    IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                    if ( lResourceService != null )
                    {
                        IResource lResource = lResourceService.LoadResource( lXName.Value );
                        if ( lResource != null )
                        {
                            lDescription = lResource.OwnedObject as PipelineDescription;
                        }
                    }

                    if ( lDescription != null )
                    {
                        lDescriptions.Add( lDescription );
                    }
                    else
                    {
                        LogManager.Instance.Log( string.Format( "Cannot find the {0} description resource!!!", lXName.Value ), LogType.ERROR );
                    }
                }
                else
                {
                    LogManager.Instance.Log( "Missing 'name' attribute!!!", LogType.ERROR );
                }
                
                lCurrent = lCurrent.NextNode;
            }

            this.Pass = new PipelinePass( lDescriptions );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new PipelinePassResource( pName, this );
        }

        #endregion Methods
    }
}
