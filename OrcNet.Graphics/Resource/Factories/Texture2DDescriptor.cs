﻿using OrcNet.Core.Resource;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Texture2D descriptor class definition used to
    /// create a Texture2D based resource(s).
    /// </summary>
    public class Texture2DDescriptor : ATextureResourceDescriptor
    {
        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int) * 2;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "texture2D";
            }
        }

        #endregion Properties IResourceFactory
        
        /// <summary>
        /// Gets the texture's width.
        /// </summary>
        public int Width
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the texture's height.
        /// </summary>
        public int Height
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2DDescriptor"/> class.
        /// </summary>
        private Texture2DDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture2DDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public Texture2DDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            XAttribute lXWidth = pDescriptor.Attribute( "width" );
            if
                ( lXWidth != null )
            {
                this.Width = int.Parse( lXWidth.Value );
            }
            else
            {
                this.Width = 1;
            }

            XAttribute lXHeight = pDescriptor.Attribute( "height" );
            if
                ( lXHeight != null )
            {
                this.Height = int.Parse( lXHeight.Value );
            }
            else
            {
                this.Height = 1;
            }
        }

        #endregion Constructor

        #region Methods
        
        #region Methods IResourceDescriptor

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new Texture2DResource( pName, this );
        }

        #endregion Methods IResourceDescriptor

        #region Methods IDisposable

        /// <summary>
        /// Releases resource's resources.
        /// </summary>
        protected override void OnDispose()
        {


            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
