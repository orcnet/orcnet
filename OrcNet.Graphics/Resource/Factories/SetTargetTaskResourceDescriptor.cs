﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.SceneGraph;
using OrcNet.Graphics.Task.Factories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Set target task factory descriptor class definition used to
    /// create SetTargetTask factory based resource(s).
    /// </summary>
    public class SetTargetTaskResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "setTarget";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += this.Factory.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the set target task factory.
        /// </summary>
        public SetTargetTaskFactory Factory
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTargetTaskResourceDescriptor"/> class.
        /// </summary>
        private SetTargetTaskResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTargetTaskResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public SetTargetTaskResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            List<AttachmentTarget> lTargets = new List<AttachmentTarget>();
            bool lIsAutoResized = false;
            XAttribute lXAutoResized = pDescriptor.Attribute( "autoResize" );
            if
                ( lXAutoResized != null )
            {
                lIsAutoResized = string.CompareOrdinal( lXAutoResized.Value, "true" ) == 0;
            }

            XNode lCurrent = pDescriptor.FirstNode;
            while
                ( lCurrent != null )
            {
                XElement lElement = lCurrent as XElement;
                if
                    ( lElement != null )
                {
                    AttachmentTarget lTarget = new AttachmentTarget();
                    if
                        ( string.CompareOrdinal( lElement.Name(), "buffer" ) != 0 )
                    {
                        LogManager.Instance.Log( string.Format( "Invalid subelement \"{0}\"", lElement.Name() ), LogType.ERROR );
                    }

                    XAttribute lXName = lElement.Attribute( "name" );
                    if
                        ( lXName == null )
                    {
                        LogManager.Instance.Log( "Missing 'name' attribute" );
                    }
                    else
                    {
                        try
                        {
                            lTarget.AttachmentPoint = Utilities.ParseEnum( lXName.Value, BufferId.NONE );
                        }
                        catch
                            ( Exception pEx )
                        {
                            LogManager.Instance.Log( pEx );
                        }

                        lTarget.Texture = new UniformQualifier( lElement.Attribute( "texture" ).Value );
                        lTarget.Level   = 0;
                        lTarget.Layer   = 0;

                        XAttribute lXLevel = lElement.Attribute( "level" );
                        if
                            ( lXLevel != null )
                        {
                            lTarget.Level = int.Parse( lXLevel.Value );
                        }

                        XAttribute lXLayer = lElement.Attribute( "layer" );
                        if
                            ( lXLayer != null )
                        {
                            lTarget.Layer = int.Parse( lXLayer.Value );
                        }

                        lTargets.Add( lTarget );
                    }
                }

                lCurrent = lCurrent.NextNode;
            }

            this.Factory = new SetTargetTaskFactory( lTargets, lIsAutoResized );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new SetTargetTaskResource( pName, this );
        }
        
        #endregion Methods
    }
}
