﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Core.Resource;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms.Values;
using OrcNet.Graphics.SceneGraph;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Scene node resource descriptor class definition used to
    /// create scene node based resource(s).
    /// </summary>
    public class SceneNodeResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "node";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += this.Node.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the scene node corresponding to that resource.
        /// </summary>
        public SceneNode Node
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneNodeResourceDescriptor"/> class.
        /// </summary>
        private SceneNodeResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneNodeResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public SceneNodeResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            this.Node = new SceneNode();

            XAttribute lXFlags = pDescriptor.Attribute( "flags" );
            if ( lXFlags != null )
            {
                string[] lFlags = lXFlags.Value.Split( ',' );
                for ( int lCurr = 0; lCurr < lFlags.Length; lCurr++ )
                {
                    string lFlag = lFlags[ lCurr ];
                    if ( string.IsNullOrEmpty( lFlag ) )
                    {
                        continue;
                    }

                    this.Node.AddFlag( lFlag );
                }
            }

            XAttribute lXName = pDescriptor.Attribute( "name" );
            if ( lXName != null )
            {
                this.Node.AddFlag( lXName.Value );
            }

            AMatrix<float> lTop = Matrix4F.IDENTITY;
            XElement lCurrent = pDescriptor.FirstNode as XElement;
            
            while ( lCurrent != null )
            {
                string lNodeValue = lCurrent.Name();
                if ( string.CompareOrdinal( lNodeValue, "translate" ) == 0 )
                {
                    float lX = 0.0f;
                    float lY = 0.0f;
                    float lZ = 0.0f;
                    XAttribute lXAttr = lCurrent.Attribute( "x" );
                    if ( lXAttr != null )
                    {
                        lX = float.Parse( lXAttr.Value );
                    }
                    XAttribute lYAttr = lCurrent.Attribute( "y" );
                    if ( lYAttr != null )
                    {
                        lY = float.Parse( lYAttr.Value );
                    }
                    XAttribute lZAttr = lCurrent.Attribute( "z" );
                    if ( lZAttr != null )
                    {
                        lZ = float.Parse( lZAttr.Value );
                    }
                    
                    lTop = lTop * Matrix4F.Translate( new Vector3F( lX, lY, lZ ) );
                }
                else if ( string.CompareOrdinal( lNodeValue, "rotatex" ) == 0 )
                {
                    float lAngleInDegrees = 0.0f;
                    XAttribute lXAngle = lCurrent.Attribute( "angle" );
                    if ( lXAngle != null )
                    {
                        lAngleInDegrees = float.Parse( lXAngle.Value );
                    }
                    lTop = lTop * Matrix4F.RotateX( lAngleInDegrees );
                }
                else if ( string.CompareOrdinal( lNodeValue, "rotatey" ) == 0 )
                {
                    float lAngleInDegrees = 0.0f;
                    XAttribute lYAngle = lCurrent.Attribute( "angle" );
                    if ( lYAngle != null )
                    {
                        lAngleInDegrees = float.Parse( lYAngle.Value );
                    }
                    lTop = lTop * Matrix4F.RotateY( lAngleInDegrees );
                }
                else if ( string.CompareOrdinal( lNodeValue, "rotatez" ) == 0 )
                {
                    float lAngleInDegrees = 0.0f;
                    XAttribute lZAngle = lCurrent.Attribute( "angle" );
                    if ( lZAngle != null )
                    {
                        lAngleInDegrees = float.Parse( lZAngle.Value );
                    }
                    lTop = lTop * Matrix4F.RotateZ( lAngleInDegrees );
                }
                else if ( string.CompareOrdinal( lNodeValue, "bounds" ) == 0 )
                {
                    bool lIsInvalid = false;
                    float lMinX = 0.0f;
                    float lMaxX = 0.0f;
                    float lMinY = 0.0f;
                    float lMaxY = 0.0f;
                    float lMinZ = 0.0f;
                    float lMaxZ = 0.0f;
                    
                    XAttribute lXMin = lCurrent.Attribute( "xmin" );
                    if ( lXMin != null )
                    {
                        lMinX = float.Parse( lXMin.Value );
                    }
                    else
                    {
                        lIsInvalid = true;
                    }
                    XAttribute lXMax = lCurrent.Attribute( "xmax" );
                    if ( lXMax != null )
                    {
                        lMaxX = float.Parse( lXMax.Value );
                    }
                    else
                    {
                        lIsInvalid = true;
                    }
                    XAttribute lYMin = lCurrent.Attribute( "ymin" );
                    if ( lYMin != null )
                    {
                        lMinY = float.Parse( lYMin.Value );
                    }
                    else
                    {
                        lIsInvalid = true;
                    }
                    XAttribute lYMax = lCurrent.Attribute( "ymax" );
                    if ( lYMax != null )
                    {
                        lMaxY = float.Parse( lYMax.Value );
                    }
                    else
                    {
                        lIsInvalid = true;
                    }
                    XAttribute lZMin = lCurrent.Attribute( "zmin" );
                    if ( lZMin != null )
                    {
                        lMinZ = float.Parse( lZMin.Value );
                    }
                    else
                    {
                        lIsInvalid = true;
                    }
                    XAttribute lZMax = lCurrent.Attribute( "zmax" );
                    if ( lZMax != null )
                    {
                        lMaxZ = float.Parse( lZMax.Value );
                    }
                    else
                    {
                        lIsInvalid = true;
                    }
                    
                    if ( lIsInvalid )
                    {
                        LogManager.Instance.Log( "Invalid bounding box!!!", LogType.ERROR );
                    }

                    this.Node.LocalBounds = new Box3F( lMinX, lMaxX, lMinY, lMaxY, lMinZ, lMaxZ );
                }
                else if ( string.CompareOrdinal( lNodeValue, 0, "uniform", 0, 7 ) == 0)
                {
                    string lType = "FLOAT";
                    XAttribute lXType = lCurrent.Attribute( "lType" );
                    if ( lXType != null )
                    {
                        lType = lXType.Value;
                    }
                    
                    int lTypeId;
                    if ( string.CompareOrdinal( lType, "BOOL" ) == 0 )
                    {
                        lTypeId = 0;
                    }
                    else if ( string.CompareOrdinal( lType, "INT" ) == 0 )
                    {
                        lTypeId = 1;
                    }
                    else if ( string.CompareOrdinal( lType, "UINT" ) == 0 )
                    {
                        lTypeId = 2;
                    }
                    else if ( string.CompareOrdinal( lType, "FLOAT" ) == 0 )
                    {
                        lTypeId = 3;
                    }
                    else
                    {
                        lTypeId = 4;
                    }
                    float lX = 0.0f;
                    float lY = 0.0f;
                    float lZ = 0.0f;
                    float lW = 0.0f;
                    ITexture lTexture = null;
                    string lId = lCurrent.Attribute( "id" ).Value;
                    int lCurrentParamCount = 0;
                    XAttribute lXComponent = lCurrent.Attribute( "x" );
                    if ( lXComponent != null )
                    {
                        lX = float.Parse( lXComponent.Value );
                        lCurrentParamCount++;
                        XAttribute lYComponent = lCurrent.Attribute( "y" );
                        if ( lYComponent != null )
                        {
                            lY = float.Parse( lYComponent.Value );
                            lCurrentParamCount++;
                            XAttribute lZComponent = lCurrent.Attribute( "z" );
                            if ( lZComponent != null )
                            {
                                lZ = float.Parse( lZComponent.Value );
                                lCurrentParamCount++;
                                XAttribute lWComponent = lCurrent.Attribute( "w" );
                                if ( lWComponent != null )
                                {
                                    lW = float.Parse( lWComponent.Value );
                                    lCurrentParamCount++;
                                }
                            }
                        }

                        switch ( lTypeId )
                        {
                            case 0:
                                switch ( lCurrentParamCount )
                                {
                                    case 1:
                                        {
                                            BoolValue lValue = new BoolValue( lId );
                                            lValue.Value = lX == 1.0f;
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 2:
                                        {
                                            Vector2FValue lValue = new Vector2FValue( lId );
                                            lValue.Value = new Vector2F( lX, lY );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 3:
                                        {
                                            Vector3FValue lValue = new Vector3FValue( lId );
                                            lValue.Value = new Vector3F( lX, lY, lZ );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 4:
                                        {
                                            Vector4FValue lValue = new Vector4FValue( lId );
                                            lValue.Value = new Vector4F( lX, lY, lZ, lW );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                }
                                break;

                            case 1:
                                switch ( lCurrentParamCount )
                                {
                                    case 1:
                                        {
                                            IntValue lValue = new IntValue( lId );
                                            lValue.Value = (int)lX;
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 2:
                                        {
                                            Vector2IValue lValue = new Vector2IValue( lId );
                                            lValue.Value = new Vector2I( (int)lX, (int)lY );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 3:
                                        {
                                            Vector3IValue lValue = new Vector3IValue( lId );
                                            lValue.Value = new Vector3I( (int)lX, (int)lY, (int)lZ );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 4:
                                        {
                                            Vector4IValue lValue = new Vector4IValue( lId );
                                            lValue.Value = new Vector4I( (int)lX, (int)lY, (int)lZ, (int)lW );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                }
                                break;
                            case 2:
                                switch ( lCurrentParamCount )
                                {
                                    case 1:
                                        {
                                            UIntValue lValue = new UIntValue( lId );
                                            lValue.Value = (uint)lX;
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 2:
                                        {
                                            Vector2UIValue lValue = new Vector2UIValue( lId );
                                            lValue.Value = new Vector2UI( (uint)lX, (uint)lY );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 3:
                                        {
                                            Vector3UIValue lValue = new Vector3UIValue( lId );
                                            lValue.Value = new Vector3UI( (uint)lX, (uint)lY, (uint)lZ );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 4:
                                        {
                                            Vector4UIValue lValue = new Vector4UIValue( lId );
                                            lValue.Value = new Vector4UI( (uint)lX, (uint)lY, (uint)lZ, (uint)lW );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                }
                                break;
                            case 3:
                                switch ( lCurrentParamCount )
                                {
                                    case 1:
                                        {
                                            FloatValue lValue = new FloatValue( lId );
                                            lValue.Value = lX;
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 2:
                                        {
                                            Vector2FValue lValue = new Vector2FValue( lId );
                                            lValue.Value = new Vector2F( lX, lY );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 3:
                                        {
                                            Vector3FValue lValue = new Vector3FValue( lId );
                                            lValue.Value = new Vector3F( lX, lY, lZ );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 4:
                                        {
                                            Vector4FValue lValue = new Vector4FValue( lId );
                                            lValue.Value = new Vector4F( lX, lY, lZ, lW );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                }
                                break;
                            case 4:
                                switch ( lCurrentParamCount )
                                {
                                    case 1:
                                        {
                                            DoubleValue lValue = new DoubleValue( lId );
                                            lValue.Value = lX;
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 2:
                                        {
                                            Vector2DValue lValue = new Vector2DValue( lId );
                                            lValue.Value = new Vector2D( lX, lY );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 3:
                                        {
                                            Vector3DValue lValue = new Vector3DValue( lId );
                                            lValue.Value = new Vector3D( lX, lY, lZ );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                    case 4:
                                        {
                                            Vector4DValue lValue = new Vector4DValue( lId );
                                            lValue.Value = new Vector4D( lX, lY, lZ, lW );
                                            this.Node.AddValue( lValue );
                                        }
                                        break;
                                }
                                break;
                        }
                    }
                    else
                    {
                        XAttribute lXTexture = lCurrent.Attribute( "texture" );
                        if ( lXTexture != null )
                        {
                            bool lIsUnknown = false;
                            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                            if ( lResourceService != null )
                            {
                                IResource lResource = lResourceService.LoadResource( lXTexture.Value );
                                if ( lResource != null )
                                {
                                    lTexture = lResource.OwnedObject as ITexture;
                                }
                            }

                            UniformType lUniformType = UniformType.SAMPLER_1D;
                            if ( lTexture is Texture1D )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_1D;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_1D;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_1D;
                                }
                            }
                            else if ( lTexture is Texture1DArray )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_1D_ARRAY;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_1D_ARRAY;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_1D_ARRAY;
                                }
                            }
                            else if ( lTexture is Texture2D )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_2D;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_2D;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_2D;
                                }
                            }
                            else if ( lTexture is Texture2DArray )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_2D_ARRAY;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_2D_ARRAY;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_2D_ARRAY;
                                }
                            }
                            else if ( lTexture is Texture2DMultisample )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_2D_MULTISAMPLE;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_2D_MULTISAMPLE;
                                }
                            }
                            else if ( lTexture is Texture2DMultisampleArray )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_2D_MULTISAMPLE_ARRAY;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_2D_MULTISAMPLE_ARRAY;
                                }
                            }
                            else if ( lTexture is Texture3D )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_3D;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_3D;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_3D;
                                }
                            }
                            else if ( lTexture is TextureBuffer )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_BUFFER;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_BUFFER;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_BUFFER;
                                }
                            }
                            else if ( lTexture is TextureCube )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_CUBE;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_CUBE;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_CUBE;
                                }
                            }
                            else if ( lTexture is TextureCubeArray )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_CUBE_MAP_ARRAY;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_CUBE_MAP_ARRAY;
                                }
                            }
                            else if ( lTexture is TextureRectangle )
                            {
                                if ( this.IsIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.INT_SAMPLER_2D_RECT;
                                }
                                else if ( this.IsUnsignedIntegerBasedTexture( lTexture ) )
                                {
                                    lUniformType = UniformType.UNSIGNED_INT_SAMPLER_2D_RECT;
                                }
                                else
                                {
                                    lUniformType = UniformType.SAMPLER_2D_RECT;
                                }
                            }
                            else
                            {
                                lIsUnknown = true;
                            }

                            if ( lIsUnknown == false )
                            {
                                this.Node.AddValue( new TextureValue( lId, lUniformType, lTexture ) );
                            }
                        }
                    }
                }
                else if ( string.CompareOrdinal( lNodeValue, "module" ) == 0 )
                {
                    string lId = lCurrent.Attribute( "id" ).Value;
                    string lValue = lCurrent.Attribute( "value" ).Value;
                    IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                    if ( lResourceService != null )
                    {
                        IResource lResource = lResourceService.LoadResource( lValue );
                        if ( lResource != null )
                        {
                            PipelineDescription lDescription = lResource.OwnedObject as PipelineDescription;
                            this.Node.AddPipelineDescription( lId, lDescription );
                        }
                    }
                }
                else if ( string.CompareOrdinal( lNodeValue, "mesh" ) == 0 )
                {
                    string lId = lCurrent.Attribute( "id" ).Value;
                    string lValue = lCurrent.Attribute( "value" ).Value;
                    IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                    if ( lResourceService != null )
                    {
                        IResource lResource = lResourceService.LoadResource( lValue );
                        if ( lResource != null )
                        {
                            MeshBuffers lMesh = lResource.OwnedObject as MeshBuffers;
                            this.Node.AddMesh( lId, lMesh );
                        }
                    }
                }
                else if ( string.CompareOrdinal( lNodeValue, "field" ) == 0 )
                {
                    string lId = lCurrent.Attribute( "id" ).Value;
                    string lValue = lCurrent.Attribute( "value" ).Value;
                    IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                    if ( lResourceService != null )
                    {
                        IResource lResource = lResourceService.LoadResource( lValue );
                        if ( lResource != null )
                        {
                            this.Node.AddField( lId, lResource.OwnedObject );
                        }
                    }
                }
                else if ( string.CompareOrdinal( lNodeValue, "method" ) == 0 )
                {
                    string lId = lCurrent.Attribute( "id" ).Value;
                    string lValue = lCurrent.Attribute( "value" ).Value;
                    IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                    if ( lResourceService != null )
                    {
                        IResource lResource = lResourceService.LoadResource( lValue );
                        if ( lResource != null )
                        {
                            ITaskFactory lTaskFactory = lResource.OwnedObject as ITaskFactory;
                            SceneNodeMethod lMethod = new SceneNodeMethod( lTaskFactory );
                            XAttribute lXEnable = lCurrent.Attribute( "enabled" );
                            if ( lXEnable != null )
                            {
                                if ( string.CompareOrdinal( lXEnable.Value, "false" ) == 0 )
                                {
                                    lMethod.IsEnabled = false;
                                }
                                else
                                {
                                    lMethod.IsEnabled = true;
                                }
                            }
                            this.Node.AddMethod( lId, lMethod );
                        }
                    }
                }
                else if ( string.CompareOrdinal( lNodeValue, "node" ) == 0 )
                {
                    SceneNode lChild = null;
                    XAttribute lXValue = lCurrent.Attribute( "value" );
                    if ( lXValue != null )
                    {
                        IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                        if ( lResourceService != null )
                        {
                            IResource lResource = lResourceService.LoadResource( lXValue.Value );
                            if ( lResource != null )
                            {
                                lChild = lResource.OwnedObject as SceneNode;
                            }
                        }
                    }
                    else
                    {
                        lChild = new SceneNodeResource( "", new SceneNodeResourceDescriptor( lCurrent, pContent, pEncoding, pStamp, pAllStamps ) ).OwnedObject;
                    }
                    
                    if ( lChild != null )
                    {
                        this.Node.AddChild( lChild );
                    }
                }
                else
                {
                    string lId = lCurrent.Attribute( "id" ).Value;
                    object lField = null;
                    IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
                    if ( lResourceService != null )
                    {
                        IResource lResource = lResourceService.LoadResource( lCurrent.Name() );
                        if ( lResource != null )
                        {
                            lField = lResource.OwnedObject;
                        }
                    }

                    if ( lField != null )
                    {
                        this.Node.AddField( lId, lField );
                    }
                    else
                    {
                        LogManager.Instance.Log( string.Format( "Unknown scene node element {0}...", lCurrent.Name() ), LogType.WARNING );
                    }
                }

                lCurrent = lCurrent.NextNode as XElement;
            }

            this.Node.LocalToParentTransform = lTop;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new SceneNodeResource( pName, this );
        }

        #region Methods Internal

        /// <summary>
        /// Checks whether the given texture is integer based or not.
        /// </summary>
        /// <param name="pTexture">The texture to check.</param>
        /// <returns>True if integer based, false otherwise.</returns>
        private bool IsIntegerBasedTexture(ITexture pTexture)
        {
            switch
                ( pTexture.InternalFormat )
            {
                case PixelInternalFormat.R8:
                case PixelInternalFormat.R8Snorm:
                case PixelInternalFormat.R16:
                case PixelInternalFormat.R16Snorm:
                case PixelInternalFormat.Rg8:
                case PixelInternalFormat.Rg8Snorm:
                case PixelInternalFormat.Rg16:
                case PixelInternalFormat.Rg16Snorm:
                case PixelInternalFormat.R3G3B2:
                case PixelInternalFormat.Rgb4:
                case PixelInternalFormat.Rgb5:
                case PixelInternalFormat.Rgb8:
                case PixelInternalFormat.Rgb8Snorm:
                case PixelInternalFormat.Rgb10:
                case PixelInternalFormat.Rgb12:
                case PixelInternalFormat.Rgb16:
                case PixelInternalFormat.Rgb16Snorm:
                case PixelInternalFormat.Rgba2:
                case PixelInternalFormat.Rgba4:
                case PixelInternalFormat.Rgb5A1:
                case PixelInternalFormat.Rgba8:
                case PixelInternalFormat.Rgba8Snorm:
                case PixelInternalFormat.Rgb10A2:
                case PixelInternalFormat.Rgb10A2ui:
                case PixelInternalFormat.Rgba12:
                case PixelInternalFormat.Rgba16:
                case PixelInternalFormat.Rgba16Snorm:
                case PixelInternalFormat.Srgb8:
                case PixelInternalFormat.Srgb8Alpha8:
                case PixelInternalFormat.R16f:
                case PixelInternalFormat.Rg16f:
                case PixelInternalFormat.Rgb16f:
                case PixelInternalFormat.Rgba16f:
                case PixelInternalFormat.R32f:
                case PixelInternalFormat.Rg32f:
                case PixelInternalFormat.Rgba32f:
                case PixelInternalFormat.R11fG11fB10f:
                case PixelInternalFormat.Rgb9E5:
                    return false;
                case PixelInternalFormat.R8i:
                case PixelInternalFormat.R16i:
                case PixelInternalFormat.R32i:
                case PixelInternalFormat.Rg8i:
                case PixelInternalFormat.Rg16i:
                case PixelInternalFormat.Rg32i:
                case PixelInternalFormat.Rgb8i:
                case PixelInternalFormat.Rgb16i:
                case PixelInternalFormat.Rgb32i:
                case PixelInternalFormat.Rgba8i:
                case PixelInternalFormat.Rgba16i:
                case PixelInternalFormat.Rgba32i:
                    return true;
                case PixelInternalFormat.R8ui:
                case PixelInternalFormat.R16ui:
                case PixelInternalFormat.R32ui:
                case PixelInternalFormat.Rg8ui:
                case PixelInternalFormat.Rg16ui:
                case PixelInternalFormat.Rg32ui:
                case PixelInternalFormat.Rgb8ui:
                case PixelInternalFormat.Rgb16ui:
                case PixelInternalFormat.Rgb32ui:
                case PixelInternalFormat.Rgba8ui:
                case PixelInternalFormat.Rgba16ui:
                case PixelInternalFormat.Rgba32ui:
                case PixelInternalFormat.CompressedRed:
                case PixelInternalFormat.CompressedRg:
                case PixelInternalFormat.CompressedRgb:
                case PixelInternalFormat.CompressedRgba:
                case PixelInternalFormat.CompressedSrgb:
                case PixelInternalFormat.CompressedRedRgtc1:
                case PixelInternalFormat.CompressedSignedRedRgtc1:
                case PixelInternalFormat.CompressedRgRgtc2:
                case PixelInternalFormat.CompressedSignedRgRgtc2:
                case PixelInternalFormat.CompressedRgbaBptcUnorm:
                //case PixelInternalFormat.CompressedSrgbAlphaBptcUnorm:
                case PixelInternalFormat.CompressedRgbBptcSignedFloat:
                case PixelInternalFormat.CompressedRgbBptcUnsignedFloat:
                case PixelInternalFormat.CompressedRgbS3tcDxt1Ext:
                case PixelInternalFormat.CompressedRgbaS3tcDxt1Ext:
                case PixelInternalFormat.CompressedRgbaS3tcDxt3Ext:
                case PixelInternalFormat.CompressedRgbaS3tcDxt5Ext:
                    return false;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the given texture is unsigned integer based or not.
        /// </summary>
        /// <param name="pTexture">The texture to check.</param>
        /// <returns>True if unsigned integer based, false otherwise.</returns>
        private bool IsUnsignedIntegerBasedTexture(ITexture pTexture)
        {
            switch
                ( pTexture.InternalFormat )
            {
                case PixelInternalFormat.R8:
                case PixelInternalFormat.R8Snorm:
                case PixelInternalFormat.R16:
                case PixelInternalFormat.R16Snorm:
                case PixelInternalFormat.Rg8:
                case PixelInternalFormat.Rg8Snorm:
                case PixelInternalFormat.Rg16:
                case PixelInternalFormat.Rg16Snorm:
                case PixelInternalFormat.R3G3B2:
                case PixelInternalFormat.Rgb4:
                case PixelInternalFormat.Rgb5:
                case PixelInternalFormat.Rgb8:
                case PixelInternalFormat.Rgb8Snorm:
                case PixelInternalFormat.Rgb10:
                case PixelInternalFormat.Rgb12:
                case PixelInternalFormat.Rgb16:
                case PixelInternalFormat.Rgb16Snorm:
                case PixelInternalFormat.Rgba2:
                case PixelInternalFormat.Rgba4:
                case PixelInternalFormat.Rgb5A1:
                case PixelInternalFormat.Rgba8:
                case PixelInternalFormat.Rgba8Snorm:
                case PixelInternalFormat.Rgb10A2:
                case PixelInternalFormat.Rgb10A2ui:
                case PixelInternalFormat.Rgba12:
                case PixelInternalFormat.Rgba16:
                case PixelInternalFormat.Rgba16Snorm:
                case PixelInternalFormat.Srgb8:
                case PixelInternalFormat.Srgb8Alpha8:
                case PixelInternalFormat.R16f:
                case PixelInternalFormat.Rg16f:
                case PixelInternalFormat.Rgb16f:
                case PixelInternalFormat.Rgba16f:
                case PixelInternalFormat.R32f:
                case PixelInternalFormat.Rg32f:
                case PixelInternalFormat.Rgba32f:
                case PixelInternalFormat.R11fG11fB10f:
                case PixelInternalFormat.Rgb9E5:
                case PixelInternalFormat.R8i:
                case PixelInternalFormat.R16i:
                case PixelInternalFormat.R32i:
                case PixelInternalFormat.Rg8i:
                case PixelInternalFormat.Rg16i:
                case PixelInternalFormat.Rg32i:
                case PixelInternalFormat.Rgb8i:
                case PixelInternalFormat.Rgb16i:
                case PixelInternalFormat.Rgb32i:
                case PixelInternalFormat.Rgba8i:
                case PixelInternalFormat.Rgba16i:
                case PixelInternalFormat.Rgba32i:
                    return false;
                case PixelInternalFormat.R8ui:
                case PixelInternalFormat.R16ui:
                case PixelInternalFormat.R32ui:
                case PixelInternalFormat.Rg8ui:
                case PixelInternalFormat.Rg16ui:
                case PixelInternalFormat.Rg32ui:
                case PixelInternalFormat.Rgb8ui:
                case PixelInternalFormat.Rgb16ui:
                case PixelInternalFormat.Rgb32ui:
                case PixelInternalFormat.Rgba8ui:
                case PixelInternalFormat.Rgba16ui:
                case PixelInternalFormat.Rgba32ui:
                    return true;
                case PixelInternalFormat.CompressedRed:
                case PixelInternalFormat.CompressedRg:
                case PixelInternalFormat.CompressedRgb:
                case PixelInternalFormat.CompressedRgba:
                case PixelInternalFormat.CompressedSrgb:
                case PixelInternalFormat.CompressedRedRgtc1:
                case PixelInternalFormat.CompressedSignedRedRgtc1:
                case PixelInternalFormat.CompressedRgRgtc2:
                case PixelInternalFormat.CompressedSignedRgRgtc2:
                case PixelInternalFormat.CompressedRgbaBptcUnorm:
                //case PixelInternalFormat.CompressedSrgbAlphaBptcUnorm:
                case PixelInternalFormat.CompressedRgbBptcSignedFloat:
                case PixelInternalFormat.CompressedRgbBptcUnsignedFloat:
                case PixelInternalFormat.CompressedRgbS3tcDxt1Ext:
                case PixelInternalFormat.CompressedRgbaS3tcDxt1Ext:
                case PixelInternalFormat.CompressedRgbaS3tcDxt3Ext:
                case PixelInternalFormat.CompressedRgbaS3tcDxt5Ext:
                    return false;
            }

            return false;
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
