﻿using OrcNet.Core.Resource;
using OrcNet.Graphics.SceneGraph;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Draw mesh task factory resource descriptor class definition used to
    /// create a DrawMeshTaskFactory based resource(s).
    /// </summary>
    public class DrawMeshTaskResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "drawMesh";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += this.Qualifier.Size;
                lSize += sizeof(int);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the mesh qualifier for the draw mesh task factory.
        /// </summary>
        public MeshQualifier Qualifier
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the number of times the mesh to draw must be drawn.
        /// </summary>
        public int DrawCount
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawMeshTaskResourceDescriptor"/> class.
        /// </summary>
        private DrawMeshTaskResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawMeshTaskResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public DrawMeshTaskResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            if
                ( pDescriptor.HasAttributes == false )
            {
                return;
            }

            string lQualifiedName = pDescriptor.Attribute( "name" ).Value;
            this.Qualifier = new MeshQualifier( lQualifiedName );

            int lDrawCount = 1;
            XAttribute lXCount = pDescriptor.Attribute( "count" );
            if
                ( lXCount != null )
            {
                lDrawCount = int.Parse( lXCount.Value );
            }

            this.DrawCount = lDrawCount;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new DrawMeshTaskResource( pName, this );
        }

        #endregion Methods
    }
}
