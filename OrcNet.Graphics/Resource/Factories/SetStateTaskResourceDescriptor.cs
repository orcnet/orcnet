﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Extensions;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Core.Resource;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Task.Factories;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Set state task factory descriptor class definition used to
    /// create SetStateTask factory based resource(s).
    /// </summary>
    public class SetStateTaskResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "setState";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += this.Factory.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the set state task factory.
        /// </summary>
        public SetStateTaskFactory Factory
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetStateTaskResourceDescriptor"/> class.
        /// </summary>
        private SetStateTaskResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetStateTaskResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public SetStateTaskResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            this.Factory = new SetStateTaskFactory();

            BufferId lReadBuffer = BufferId.NONE;
            BufferId lDrawBuffer = BufferId.NONE;

            bool lMustClearColor = false;
            XAttribute lXMustClearColor = pDescriptor.Attribute( "clearColor" );
            if ( lXMustClearColor != null )
            {
                lMustClearColor = string.CompareOrdinal( lXMustClearColor.Value, "true" ) == 0;
            }

            bool lMustClearStencil = false;
            XAttribute lXMustClearStencil = pDescriptor.Attribute( "clearStencil" );
            if ( lXMustClearStencil != null )
            {
                lMustClearStencil = string.CompareOrdinal( lXMustClearStencil.Value, "true" ) == 0;
            }

            bool lMustClearDepth = false;
            XAttribute lXMustClearDepth = pDescriptor.Attribute( "clearDepth" );
            if ( lXMustClearDepth != null )
            {
                lMustClearDepth = string.CompareOrdinal( lXMustClearDepth.Value, "true" ) == 0;
            }

            try
            {
                XAttribute lXReadBuffer = pDescriptor.Attribute( "readBuffer" );
                if ( lXReadBuffer != null )
                {
                    lReadBuffer = Utilities.ParseEnum( lXReadBuffer.Value, BufferId.NONE );
                }

                XAttribute lXDrawBuffer = pDescriptor.Attribute( "drawBuffer" );
                if ( lXDrawBuffer != null )
                {
                    string[] lBuffers = lXDrawBuffer.Value.Split( ',' );
                    for
                        ( int lCurr = 0; lCurr < lBuffers.Length; lCurr++ )
                    {
                        lDrawBuffer |= Utilities.ParseEnum( lBuffers[ lCurr ], BufferId.NONE );
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            this.Factory.SetBuffers( lReadBuffer, lDrawBuffer );

            XNode lCurrent = pDescriptor.FirstNode;
            while ( lCurrent != null )
            {
                XElement lElement = lCurrent as XElement;
                if ( lElement == null )
                {
                    lCurrent = lCurrent.NextNode;
                    continue;
                }

                string lName = lElement.Name();
                if ( string.CompareOrdinal( lName, "viewport" ) == 0 )
                {
                    Viewport lViewport = new Viewport( float.Parse( lElement.Attribute( "x" ).Value ),
                                                       float.Parse( lElement.Attribute( "y" ).Value ),
                                                       float.Parse( lElement.Attribute( "width" ).Value ),
                                                       float.Parse( lElement.Attribute( "height" ).Value ) );
                    this.Factory.SetViewport( lViewport );
                }
                else if ( string.CompareOrdinal( lName, "depthRange" ) == 0 )
                {
                    DepthRange lRange = new DepthRange( float.Parse( lElement.Attribute( "near" ).Value ),
                                                        float.Parse( lElement.Attribute( "far" ).Value ) );
                    this.Factory.SetDepthRange( lRange );
                }
                else if ( string.CompareOrdinal( lName, "clipDistances" ) == 0 )
                {
                    int lClipDistances = int.Parse( lElement.Attribute( "value" ).Value );
                    this.Factory.SetClipDistances( lClipDistances );
                }
                else if ( string.CompareOrdinal( lName, "blend" ) == 0 )
                {
                    if ( lElement.FirstAttribute == null )
                    {
                        this.Factory.SetBlend( BufferId.NONE, false, BlendEquationMode.FuncAdd, BlendingFactorSrc.One, BlendingFactorDest.One, BlendEquationMode.FuncAdd, BlendingFactorSrc.One, BlendingFactorDest.One );
                    }
                    else
                    {
                        BufferId lAttachmentPoint = BufferId.NONE;
                        bool lUseBlending = false;
                        XAttribute lXBlending = lElement.Attribute( "enable" );
                        if ( lXBlending != null )
                        {
                            lUseBlending = string.CompareOrdinal( lXBlending.Value, "true" ) == 0;
                        }

                        BlendEquationMode lRGB = BlendEquationMode.FuncAdd;
                        BlendingFactorSrc lRGBSource = BlendingFactorSrc.One;
                        BlendingFactorDest lRGBDestination = BlendingFactorDest.One;
                        BlendEquationMode lAlpha = BlendEquationMode.FuncAdd;
                        BlendingFactorSrc lAlphaSource = BlendingFactorSrc.One;
                        BlendingFactorDest lAlphaDestination = BlendingFactorDest.One;

                        XAttribute lXBuffer = lElement.Attribute( "buffer" );
                        if ( lXBuffer != null )
                        {
                            string[] lBuffers = lXBuffer.Value.Split( ',' );
                            for ( int lCurr = 0; lCurr < lBuffers.Length; lCurr++ )
                            {
                                lAttachmentPoint |= Utilities.ParseEnum( lBuffers[ lCurr ], BufferId.NONE );
                            }
                        }

                        XAttribute lXEquation = lElement.Attribute( "eq" );
                        if ( lXEquation != null )
                        {
                            BlendEquationMode? lRGBEq = this.GetEquation( lElement, "eq" );
                            if ( lRGBEq.HasValue )
                            {
                                lRGB = lRGBEq.Value;
                            }
                            BlendingFactorSrc? lRGBSr = this.GetSourceFactor( lElement, "src" );
                            if ( lRGBSr.HasValue )
                            {
                                lRGBSource = lRGBSr.Value;
                            }
                            BlendingFactorDest? lRGBDest = this.GetDestinationFactor( lElement, "dst" );
                            if ( lRGBDest.HasValue )
                            {
                                lRGBDestination = lRGBDest.Value;
                            }

                            XAttribute lXAlpha = lElement.Attribute( "alphaeq" );
                            if ( lXAlpha != null )
                            {
                                BlendEquationMode? lAlphaEq = this.GetEquation( lElement, "alphaeq" );
                                if ( lAlphaEq.HasValue )
                                {
                                    lAlpha = lAlphaEq.Value;
                                }
                                BlendingFactorSrc? lAlphaSr = this.GetSourceFactor( lElement, "alphasrc" );
                                if ( lAlphaSr.HasValue )
                                {
                                    lAlphaSource = lAlphaSr.Value;
                                }
                                BlendingFactorDest? lAlphaDest = this.GetDestinationFactor( lElement, "alphadst" );
                                if ( lAlphaDest.HasValue )
                                {
                                    lAlphaDestination = lAlphaDest.Value;
                                }
                            }
                        }

                        this.Factory.SetBlend( lAttachmentPoint, lUseBlending, lRGB, lRGBSource, lRGBDestination, 
                                                                               lAlpha, lAlphaSource, lAlphaDestination );

                        XAttribute lXRed = lElement.Attribute( "r" );
                        if ( lXRed != null )
                        {
                            float lRed   = float.Parse( lXRed.Value );
                            float lGreen = float.Parse( lElement.Attribute( "g" ).Value );
                            float lBlue  = float.Parse( lElement.Attribute( "b" ).Value );
                            float lAlph  = float.Parse( lElement.Attribute( "a" ).Value );

                            Color lBlendColor = Color.FromArgb( (int)(lAlph * 255),
                                                                (int)(lRed * 255),
                                                                (int)(lGreen * 255),
                                                                (int)(lBlue * 255) );

                            this.Factory.SetBlendColor( lBlendColor );
                        }
                    }
                }
                else if ( string.CompareOrdinal( lName, "clear" ) == 0 )
                {
                    XAttribute lXRed = lElement.Attribute( "r" );
                    if ( lXRed != null )
                    {
                        float lRed   = float.Parse( lXRed.Value );
                        float lGreen = float.Parse( lElement.Attribute( "g" ).Value );
                        float lBlue  = float.Parse( lElement.Attribute( "b" ).Value );
                        float lAlph  = float.Parse( lElement.Attribute( "a" ).Value );

                        Color lClearColor = Color.FromArgb( (int)(lAlph * 255),
                                                            (int)(lRed * 255),
                                                            (int)(lGreen * 255),
                                                            (int)(lBlue * 255) );

                        this.Factory.SetClearColor( lClearColor );
                    }

                    XAttribute lXStencil = lElement.Attribute( "stencil" );
                    if ( lXStencil != null )
                    {
                        int lClearStencil = int.Parse( lXStencil.Value );
                        this.Factory.SetClearStencil( lClearStencil );
                    }

                    XAttribute lXDepth = lElement.Attribute( "depth" );
                    if ( lXDepth != null )
                    {
                        float lClearDepth = float.Parse( lXDepth.Value );
                        this.Factory.SetClearDepth( lClearDepth );
                    }
                }
                else if ( string.CompareOrdinal( lName, "point" ) == 0 )
                {
                    XAttribute lXSize = lElement.Attribute( "size" );
                    if ( lXSize != null )
                    {
                        float lPointSize = float.Parse( lXSize.Value );
                        this.Factory.SetPointSize( lPointSize );
                    }

                    XAttribute lXThreshold = lElement.Attribute( "threshold" );
                    if ( lXThreshold != null )
                    {
                        float lPointThreshold = float.Parse( lXThreshold.Value );
                        this.Factory.SetPointFadeThresholdSize( lPointThreshold );
                    }

                    XAttribute lXOrigin = lElement.Attribute( "lowerleftorigin" );
                    if ( lXOrigin != null )
                    {
                        bool lHasLowerLeftOrigin = string.CompareOrdinal( lElement.Attribute( "clearColor" ).Value, "true" ) == 0;
                        this.Factory.SetHasPointLowerLeftOrigin( lHasLowerLeftOrigin );
                    }
                }
                else if ( string.CompareOrdinal( lName, "line" ) == 0 )
                {
                    XAttribute lXSmooth = lElement.Attribute( "smooth" );
                    if ( lXSmooth != null )
                    {
                        bool lSmoothes = string.CompareOrdinal( lXSmooth.Value, "true" ) == 0;
                        this.Factory.SetLineSmooth( lSmoothes );
                    }

                    XAttribute lXWidth = lElement.Attribute( "width" );
                    if ( lXWidth != null )
                    {
                        float lWidth = float.Parse( lXWidth.Value );
                        this.Factory.SetLineWidth( lWidth );
                    }
                }
                else if ( string.CompareOrdinal( lName, "polygon" ) == 0 )
                {
                    XAttribute lXFront = lElement.Attribute( "front" );
                    if ( lXFront != null )
                    {
                        bool[] lCulls = new bool[ 2 ];
                        PolygonMode[] lModes = new PolygonMode[ 2 ];
                        string[] lModesStr = new string[ 2 ]
                        {
                            lXFront.Value,
                            lElement.Attribute( "back" ).Value
                        };
                        for ( int lCurr = 0; lCurr < 2; lCurr++ )
                        {
                            if ( string.CompareOrdinal( lModesStr[ lCurr ], "CULL" ) == 0 )
                            {
                                lCulls[ lCurr ] = true;
                            }
                            else if ( string.CompareOrdinal( lModesStr[ lCurr ], "POINT" ) == 0 )
                            {
                                lModes[ lCurr ] = PolygonMode.Point;
                            }
                            else if ( string.CompareOrdinal( lModesStr[ lCurr ], "LINE" ) == 0 )
                            {
                                lModes[ lCurr ] = PolygonMode.Line;
                            }
                            else if ( string.CompareOrdinal( lModesStr[ lCurr ], "FILL" ) == 0 )
                            {
                                lModes[ lCurr ] = PolygonMode.Fill;
                            }
                            else
                            {
                                LogManager.Instance.Log( "Invalid cull value", LogType.ERROR );
                            }
                        }

                        this.Factory.SetPolygonMode( lCulls[ 0 ], lCulls[ 1 ], lModes[ 0 ], lModes[ 1 ] );
                    }

                    XAttribute lXClockwise = lElement.Attribute( "frontCW" );
                    if ( lXClockwise != null )
                    {
                        bool lIsClockwise = string.CompareOrdinal( lXClockwise.Value, "true" ) == 0;
                        this.Factory.SetFrontFaceMode( lIsClockwise );
                    }

                    XAttribute lXSmooth = lElement.Attribute( "smooth" );
                    if ( lXSmooth != null )
                    {
                        bool lSmoothes = string.CompareOrdinal( lXSmooth.Value, "true" ) == 0;
                        this.Factory.SetPolygonSmooth( lSmoothes );
                    }

                    XAttribute lXOffsetFactor = lElement.Attribute( "offsetFactor" );
                    if ( lXOffsetFactor != null )
                    {
                        float lFactor = float.Parse( lElement.Attribute( "factor" ).Value );
                        float lUnits  = float.Parse( lElement.Attribute( "units" ).Value );
                        this.Factory.SetPolygonOffset( new Vector2F( lFactor, lUnits ) );
                    }

                    XAttribute lXPointOffset = lElement.Attribute( "pointOffset" );
                    if ( lXPointOffset != null )
                    {
                        bool lHasPointOffset   = string.CompareOrdinal( lXPointOffset.Value, "true" ) == 0;
                        bool lHasLineOffset    = string.CompareOrdinal( lElement.Attribute( "lineOffset" ).Value, "true" ) == 0;
                        bool lHasPolygonOffset = string.CompareOrdinal( lElement.Attribute( "polygonOffset" ).Value, "true" ) == 0;
                        this.Factory.SetPolygonOffsets( lHasPointOffset, lHasLineOffset, lHasPolygonOffset );
                    }
                }
                else if ( string.CompareOrdinal( lName, "depth" ) == 0 )
                {
                    bool lUseDepth = false;
                    XAttribute lXDepth = lElement.Attribute( "enable" );
                    if ( lXDepth != null )
                    {
                        lUseDepth = string.CompareOrdinal( lXDepth.Value, "true" ) == 0;
                    }
                    
                    DepthFunction? lDepthFc = this.GetDepth( lElement, "value" );
                    this.Factory.SetDepthTest( lUseDepth, lDepthFc );
                }
                else if ( string.CompareOrdinal( lName, "stencil" ) == 0 )
                {
                    bool lUseStencil = false;
                    XAttribute lXStencil = lElement.Attribute( "enable" );
                    if ( lXStencil != null )
                    {
                        lUseStencil = string.CompareOrdinal( lXStencil.Value, "true" ) == 0;
                    }

                    int lFrontRef  = -1;
                    uint lFrontMask = 0;
                    StencilFunction? lFrontFunction = null;
                    StencilOp lFrontStencilFail = StencilOp.Keep;
                    StencilOp lFrontDepthFail   = StencilOp.Keep;
                    StencilOp lFrontDepthPass   = StencilOp.Keep;
                    XAttribute lXFrontFunction = lElement.Attribute( "ffunction" );
                    if ( lXFrontFunction != null )
                    {
                        lFrontFunction = this.GetStencil( lElement, "ffunction" );
                        lFrontRef  = int.Parse( lElement.Attribute( "fref" ).Value );
                        lFrontMask = uint.Parse( lElement.Attribute( "fmask" ).Value );
                        StencilOp? lFSFail = this.GetStencilOp( lElement, "ffail" );
                        if ( lFSFail.HasValue )
                        {
                            lFrontStencilFail = lFSFail.Value;
                        }
                        StencilOp? lFDFail = this.GetStencilOp( lElement, "fdpfail" );
                        if ( lFDFail.HasValue )
                        {
                            lFrontDepthFail = lFDFail.Value;
                        }
                        StencilOp? lFDPass = this.GetStencilOp( lElement, "fdppass" );
                        if ( lFDPass.HasValue )
                        {
                            lFrontDepthPass = lFDPass.Value;
                        }
                    }

                    int lBackRef   = -1;
                    uint lBackMask = 0;
                    StencilFunction? lBackFunction = null;
                    StencilOp lBackStencilFail = StencilOp.Keep;
                    StencilOp lBackDepthFail   = StencilOp.Keep;
                    StencilOp lBackDepthPass   = StencilOp.Keep;
                    XAttribute lXBackFunction = lElement.Attribute( "bfunction" );
                    if ( lXBackFunction != null )
                    {
                        lBackFunction = this.GetStencil( lElement, "bfunction" );
                        lBackRef  = int.Parse( lElement.Attribute( "bref" ).Value );
                        lBackMask = uint.Parse( lElement.Attribute( "bmask" ).Value );
                        StencilOp? lBSFail = this.GetStencilOp( lElement, "bfail" );
                        if ( lBSFail.HasValue )
                        {
                            lBackStencilFail = lBSFail.Value;
                        }
                        StencilOp? lBDFail = this.GetStencilOp( lElement, "bdpfail" );
                        if ( lBDFail.HasValue )
                        {
                            lBackDepthFail = lBDFail.Value;
                        }
                        StencilOp? lBDPass = this.GetStencilOp( lElement, "bdppass" );
                        if ( lBDPass.HasValue )
                        {
                            lBackDepthPass = lBDPass.Value;
                        }
                    }

                    this.Factory.SetStencilTest( lUseStencil, lFrontFunction, lFrontRef, lFrontMask, lFrontStencilFail, lFrontDepthFail, lFrontDepthPass,
                                                              lBackFunction, lBackRef, lBackMask, lBackStencilFail, lBackDepthFail, lBackDepthPass );
                }
                else if ( string.CompareOrdinal( lName, "write" ) == 0 )
                {
                    BufferId lBuffer = BufferId.NONE;
                    XAttribute lXBuffer = lElement.Attribute( "buffer" );
                    if ( lXBuffer != null )
                    {
                        string[] lBuffers = lXBuffer.Value.Split( ',' );
                        for ( int lCurr = 0; lCurr < lBuffers.Length; lCurr++ )
                        {
                            lBuffer |= Utilities.ParseEnum( lBuffers[ lCurr ], BufferId.NONE );
                        }
                    }

                    XAttribute lXRed = lElement.Attribute( "r" );
                    if ( lXRed != null )
                    {
                        bool lCanWriteRed   = string.CompareOrdinal( lXRed.Value, "true" ) == 0;
                        bool lCanWriteGreen = string.CompareOrdinal( lElement.Attribute( "g" ).Value, "true" ) == 0;
                        bool lCanWriteBlue  = string.CompareOrdinal( lElement.Attribute( "b" ).Value, "true" ) == 0;
                        bool lCanWriteAlpha = string.CompareOrdinal( lElement.Attribute( "a" ).Value, "true" ) == 0;

                        this.Factory.SetColorMask( lBuffer, lCanWriteRed, lCanWriteGreen, lCanWriteBlue, lCanWriteAlpha );
                    }

                    XAttribute lXDepth = lElement.Attribute( "d" );
                    if ( lXDepth != null )
                    {
                        bool lCanWriteDepth = string.CompareOrdinal( lXDepth.Value, "true" ) == 0;
                        this.Factory.SetDepthMask( lCanWriteDepth );
                    }

                    XAttribute lXStencil = lElement.Attribute( "fs" );
                    if ( lXStencil != null )
                    {
                        uint lFrontMask = uint.Parse( lXStencil.Value );
                        uint lBackMask  = uint.Parse( lElement.Attribute( "bs" ).Value );

                        this.Factory.SetStencilMask( lFrontMask, lBackMask );
                    }
                }
                else if ( string.CompareOrdinal( lName, "logic" ) == 0 )
                {
                    bool lUseLogicOp = false;
                    XAttribute lXLogic = lElement.Attribute( "enable" );
                    if ( lXLogic != null )
                    {
                        lUseLogicOp = string.CompareOrdinal( lXLogic.Value, "true" ) == 0;
                    }

                    LogicOp? lOperation = this.GetLogicOp( lElement, "value" );

                    this.Factory.SetLogicOp( lUseLogicOp, lOperation );
                }
                else if ( string.CompareOrdinal( lName, "scissor" ) == 0 )
                {
                    bool lUseScissor = false;
                    XAttribute lXScissor = lElement.Attribute( "enable" );
                    if ( lXScissor != null )
                    {
                        lUseScissor = string.CompareOrdinal( lXScissor.Value, "true" ) == 0;
                    }

                    XAttribute lXValue = lElement.Attribute( "x" );
                    if ( lXValue != null )
                    {
                        Scissor lScissor = new Scissor( (float)int.Parse( lXValue.Value ),
                                                        (float)int.Parse( lElement.Attribute( "y" ).Value ),
                                                        (float)int.Parse( lElement.Attribute( "width" ).Value ),
                                                        (float)int.Parse( lElement.Attribute( "height" ).Value ) );
                        this.Factory.SetScissorTest( lUseScissor, lScissor );
                    }
                    else
                    {
                        this.Factory.SetScissorTest( lUseScissor );
                    }
                }
                else if ( string.CompareOrdinal( lName, "occlusion" ) == 0 )
                {
                    GPUQuery lQuery = null;
                    QueryMode? lMode   = this.GetQueryMode( lElement, "mode" );
                    QueryTarget? lType = this.GetQueryTarget( lElement, "query" );
                    if ( lType.HasValue )
                    {
                        lQuery = new GPUQuery( lType.Value );
                    }

                    if ( lQuery != null )
                    {
                        this.Factory.SetOcclusionTest( lQuery, lMode.HasValue ? lMode.Value : QueryMode.WAIT );
                    }
                }
                else if ( string.CompareOrdinal( lName, "multisampling" ) == 0 )
                {
                    bool lUseMultisampling = false;
                    XAttribute lXMultisampling = lElement.Attribute( "enable" );
                    if ( lXMultisampling != null )
                    {
                        lUseMultisampling = string.CompareOrdinal( lXMultisampling.Value, "true" ) == 0;
                    }

                    this.Factory.SetMultisample( lUseMultisampling );

                    XAttribute lXAlphaCoverage = lElement.Attribute( "alphaToCoverage" );
                    if ( lXAlphaCoverage != null )
                    {
                        bool lAlphaToCoverage = string.CompareOrdinal( lXAlphaCoverage.Value, "true" ) == 0;
                        bool lForceOpacity    = string.CompareOrdinal( lElement.Attribute( "alphaToOne" ).Value, "true" ) == 0;

                        this.Factory.SetSampleAlpha( lAlphaToCoverage, lForceOpacity );
                    }

                    XAttribute lXCoverage = lElement.Attribute( "coverage" );
                    if ( lXCoverage != null )
                    {
                        float lCoverage = float.Parse( lXCoverage.Value );
                        this.Factory.SetSampleCoverage( lCoverage );
                    }

                    XAttribute lXMask = lElement.Attribute( "mask" );
                    if ( lXMask != null )
                    {
                        uint lMask = uint.Parse( lXMask.Value );
                        this.Factory.SetSampleMask( lMask );
                    }

                    XAttribute lXShading = lElement.Attribute( "shading" );
                    if ( lXShading != null )
                    {
                        bool lSampleShading = string.CompareOrdinal( lXShading.Value, "true" ) == 0;
                        float lMinSampleCount = float.Parse( lElement.Attribute( "min" ).Value );
                        this.Factory.SetSampleShading( lSampleShading, lMinSampleCount );
                    }
                }
                else
                {
                    LogManager.Instance.Log( "Invalid sub element!!!", LogType.ERROR );
                }

                lCurrent = lCurrent.NextNode;
            }

            this.Factory.SetClearState( lMustClearColor, lMustClearStencil, lMustClearDepth );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new SetStateTaskResource( pName, this );
        }

        #region Methods Internal

        /// <summary>
        /// Gets the blending equation
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The blending equation</returns>
        private BlendEquationMode? GetEquation(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "ADD" ) == 0 )
                    {
                        return BlendEquationMode.FuncAdd;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "SUBTRACT" ) == 0 )
                    {
                        return BlendEquationMode.FuncSubtract;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "REVERSE_SUBTRACT" ) == 0 )
                    {
                        return BlendEquationMode.FuncReverseSubtract;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "MIN" ) == 0 )
                    {
                        return BlendEquationMode.Min;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "MAX" ) == 0 )
                    {
                        return BlendEquationMode.Max;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Gets the blending source factor
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The blending source factor</returns>
        private BlendingFactorSrc? GetSourceFactor(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "ZERO" ) == 0 )
                    {
                        return BlendingFactorSrc.Zero;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE" ) == 0 )
                    {
                        return BlendingFactorSrc.One;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "SRC_COLOR" ) == 0 )
                    {
                        return BlendingFactorSrc.SrcColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_SRC_COLOR" ) == 0 )
                    {
                        return BlendingFactorSrc.OneMinusSrcColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "DST_COLOR" ) == 0 )
                    {
                        return BlendingFactorSrc.DstColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_DST_COLOR" ) == 0 )
                    {
                        return BlendingFactorSrc.OneMinusDstColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "SRC_ALPHA" ) == 0 )
                    {
                        return BlendingFactorSrc.SrcAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_SRC_ALPHA" ) == 0 )
                    {
                        return BlendingFactorSrc.OneMinusSrcAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "DST_ALPHA" ) == 0 )
                    {
                        return BlendingFactorSrc.DstAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_DST_ALPHA" ) == 0 )
                    {
                        return BlendingFactorSrc.OneMinusDstAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "CONSTANT_COLOR" ) == 0 )
                    {
                        return BlendingFactorSrc.ConstantColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_CONSTANT_COLOR" ) == 0 )
                    {
                        return BlendingFactorSrc.OneMinusConstantColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "CONSTANT_ALPHA" ) == 0 )
                    {
                        return BlendingFactorSrc.ConstantAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_CONSTANT_ALPHA" ) == 0 )
                    {
                        return BlendingFactorSrc.OneMinusConstantAlpha;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Gets the blending destination factor.
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The blending destination factor.</returns>
        private BlendingFactorDest? GetDestinationFactor(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "ZERO" ) == 0 )
                    {
                        return BlendingFactorDest.Zero;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE" ) == 0 )
                    {
                        return BlendingFactorDest.One;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "SRC_COLOR" ) == 0 )
                    {
                        return BlendingFactorDest.SrcColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_SRC_COLOR" ) == 0 )
                    {
                        return BlendingFactorDest.OneMinusSrcColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "DST_COLOR" ) == 0 )
                    {
                        return BlendingFactorDest.DstColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_DST_COLOR" ) == 0 )
                    {
                        return BlendingFactorDest.OneMinusDstColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "SRC_ALPHA" ) == 0 )
                    {
                        return BlendingFactorDest.SrcAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_SRC_ALPHA" ) == 0 )
                    {
                        return BlendingFactorDest.OneMinusSrcAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "DST_ALPHA" ) == 0 )
                    {
                        return BlendingFactorDest.DstAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_DST_ALPHA" ) == 0 )
                    {
                        return BlendingFactorDest.OneMinusDstAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "CONSTANT_COLOR" ) == 0 )
                    {
                        return BlendingFactorDest.ConstantColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_CONSTANT_COLOR" ) == 0 )
                    {
                        return BlendingFactorDest.OneMinusConstantColor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "CONSTANT_ALPHA" ) == 0 )
                    {
                        return BlendingFactorDest.ConstantAlpha;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ONE_MINUS_CONSTANT_ALPHA" ) == 0 )
                    {
                        return BlendingFactorDest.OneMinusConstantAlpha;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Gets the depth function.
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The depth function.</returns>
        private DepthFunction? GetDepth(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "NEVER" ) == 0 )
                    {
                        return DepthFunction.Never;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ALWAYS" ) == 0 )
                    {
                        return DepthFunction.Always;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "LESS" ) == 0 )
                    {
                        return DepthFunction.Less;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "LEQUAL" ) == 0 )
                    {
                        return DepthFunction.Lequal;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "EQUAL" ) == 0 )
                    {
                        return DepthFunction.Equal;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "GREATER" ) == 0 )
                    {
                        return DepthFunction.Greater;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "GEQUAL" ) == 0 )
                    {
                        return DepthFunction.Gequal;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "NOTEQUAL" ) == 0 )
                    {
                        return DepthFunction.Notequal;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Gets the stencil function.
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The stencil function.</returns>
        private StencilFunction? GetStencil(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "NEVER" ) == 0 )
                    {
                        return StencilFunction.Never;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ALWAYS" ) == 0 )
                    {
                        return StencilFunction.Always;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "LESS" ) == 0 )
                    {
                        return StencilFunction.Less;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "LEQUAL" ) == 0 )
                    {
                        return StencilFunction.Lequal;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "EQUAL" ) == 0 )
                    {
                        return StencilFunction.Equal;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "GREATER" ) == 0 )
                    {
                        return StencilFunction.Greater;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "GEQUAL" ) == 0 )
                    {
                        return StencilFunction.Gequal;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "NOTEQUAL" ) == 0 )
                    {
                        return StencilFunction.Notequal;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Gets the stencil operation.
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The stencil operation.</returns>
        private StencilOp? GetStencilOp(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "KEEP" ) == 0 )
                    {
                        return StencilOp.Keep;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "RESET" ) == 0 )
                    {
                        return StencilOp.Zero;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "REPLACE" ) == 0 )
                    {
                        return StencilOp.Replace;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "INCR" ) == 0 )
                    {
                        return StencilOp.Incr;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "DECR" ) == 0 )
                    {
                        return StencilOp.Decr;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "INVERT" ) == 0 )
                    {
                        return StencilOp.Invert;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "INCR_WRAP" ) == 0 )
                    {
                        return StencilOp.IncrWrap;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "DECR_WRAP" ) == 0 )
                    {
                        return StencilOp.DecrWrap;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Gets the logic operation.
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The logic operation.</returns>
        private LogicOp? GetLogicOp(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "CLEAR" ) == 0 )
                    {
                        return LogicOp.Clear;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "AND" ) == 0 )
                    {
                        return LogicOp.And;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "AND_REVERSE" ) == 0 )
                    {
                        return LogicOp.AndReverse;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "COPY" ) == 0 )
                    {
                        return LogicOp.Copy;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "AND_INVERTED" ) == 0 )
                    {
                        return LogicOp.AndInverted;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "NOOP" ) == 0 )
                    {
                        return LogicOp.Noop;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "XOR" ) == 0 )
                    {
                        return LogicOp.Xor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "OR" ) == 0 )
                    {
                        return LogicOp.Or;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "NOR" ) == 0 )
                    {
                        return LogicOp.Nor;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "EQUIV" ) == 0 )
                    {
                        return LogicOp.Equiv;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "NOT" ) == 0 )
                    {
                        return LogicOp.Invert;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "OR_REVERSE" ) == 0 )
                    {
                        return LogicOp.OrReverse;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "COPY_INVERTED" ) == 0 )
                    {
                        return LogicOp.CopyInverted;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "OR_INVERTED" ) == 0 )
                    {
                        return LogicOp.OrInverted;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "NAND" ) == 0 )
                    {
                        return LogicOp.Nand;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "SET" ) == 0 )
                    {
                        return LogicOp.Set;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Gets the query target.
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The query target.</returns>
        private QueryTarget? GetQueryTarget(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "PRIMITIVES_GENERATED" ) == 0 )
                    {
                        return QueryTarget.PrimitivesGenerated;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN" ) == 0 )
                    {
                        return QueryTarget.TransformFeedbackPrimitivesWritten;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "SAMPLES_PASSED" ) == 0 )
                    {
                        return QueryTarget.SamplesPassed;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "ANY_SAMPLES_PASSED" ) == 0 )
                    {
                        return QueryTarget.AnySamplesPassed;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "TIME_STAMP" ) == 0 )
                    {
                        return QueryTarget.Timestamp;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        /// <summary>
        /// Gets the query mode.
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <param name="pName">The attribute name</param>
        /// <returns>The query mode.</returns>
        private QueryMode? GetQueryMode(XElement pElement, string pName)
        {
            try
            {
                XAttribute lXAttribute = pElement.Attribute( pName );
                if ( lXAttribute != null )
                {
                    if ( string.CompareOrdinal( lXAttribute.Value, "WAIT" ) == 0 )
                    {
                        return QueryMode.WAIT;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "NO_WAIT" ) == 0 )
                    {
                        return QueryMode.NO_WAIT;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "REGION_WAIT" ) == 0 )
                    {
                        return QueryMode.REGION_WAIT;
                    }
                    else if ( string.CompareOrdinal( lXAttribute.Value, "REGION_NO_WAIT" ) == 0 )
                    {
                        return QueryMode.REGION_NO_WAIT;
                    }
                }
            }
            catch ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            return null;
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
