﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Core.Resource;
using OrcNet.Graphics.Render;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Mesh buffers descriptor class definition used to
    /// create mesh buffers based resource(s).
    /// </summary>
    [DebuggerDisplay("Mode = {Mode}, AttributeCount = {Attributes.Count},\n VertexCount = {VertexCount}, IndexCount = {IndexCount}")]
    public class MeshBuffersResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "mesh";
            }
        }

        #endregion Properties IResourceFactory

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(PrimitiveType);
                lSize += sizeof(uint) * 2;
                lSize += sizeof(float) * 6; // Bounds.
                if
                    ( this.IndicesBuffer != null )
                {
                    lSize += this.IndicesBuffer.Size;
                }
                if
                    ( this.Attributes.Count > 0 )
                {
                    for
                        ( int lCurr = 0; lCurr < this.Attributes.Count; lCurr++ )
                    {
                        lSize += this.Attributes[ lCurr ].Size;
                    }
                }

                return lSize;
            }
        }

        /// <summary>
        /// Gets the mesh's bounds.
        /// </summary>
        public Box3F Bounds
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the primitive type informing about how vertices must be interpreted.
        /// </summary>
        public PrimitiveType Mode
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the mesh buffers vertex attributes.
        /// </summary>
        public List<PipelineAttributeDescription> Attributes
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the mesh buffers indices buffer.
        /// </summary>
        public PipelineAttributeDescription IndicesBuffer
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the vertex count.
        /// </summary>
        public uint VertexCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the index count.
        /// </summary>
        public uint IndexCount
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MeshBuffersResourceDescriptor"/> class.
        /// </summary>
        private MeshBuffersResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MeshBuffersResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public MeshBuffersResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            if ( pContent == null )
            {
                LogManager.Instance.Log( "NULL mesh buffer content!!!" );
                return;
            }

            string lContent = pEncoding.GetString( pContent );
            string[] lInfo = lContent.GetWords(); // Split to have the content word by word.
            int lCurrInfoIndex = 0;
            float lMinX = float.Parse( lInfo[ lCurrInfoIndex++ ] );
            float lMaxX = float.Parse( lInfo[ lCurrInfoIndex++ ] );
            float lMinY = float.Parse( lInfo[ lCurrInfoIndex++ ] );
            float lMaxY = float.Parse( lInfo[ lCurrInfoIndex++ ] );
            float lMinZ = float.Parse( lInfo[ lCurrInfoIndex++ ] );
            float lMaxZ = float.Parse( lInfo[ lCurrInfoIndex++ ] );
            this.Bounds = new Box3F( lMinX, lMaxX, lMinY, lMaxY, lMinZ, lMaxZ );

            string lMode = lInfo[ lCurrInfoIndex++ ];
            if ( string.CompareOrdinal( lMode, "points" ) == 0 )
            {
                this.Mode = PrimitiveType.Points;
            }
            else if ( string.CompareOrdinal( lMode, "lines" ) == 0 )
            {
                this.Mode = PrimitiveType.Lines;
            }
            else if ( string.CompareOrdinal( lMode, "linesadjacency" ) == 0 )
            {
                this.Mode = PrimitiveType.LinesAdjacency;
            }
            else if ( string.CompareOrdinal( lMode, "linestrip" ) == 0 )
            {
                this.Mode = PrimitiveType.LineStrip;
            }
            else if ( string.CompareOrdinal( lMode, "linestripadjacency" ) == 0 )
            {
                this.Mode = PrimitiveType.LineStripAdjacency;
            }
            else if ( string.CompareOrdinal( lMode, "triangles" ) == 0 )
            {
                this.Mode = PrimitiveType.Triangles;
            }
            else if ( string.CompareOrdinal( lMode, "trianglesadjacency" ) == 0 )
            {
                this.Mode = PrimitiveType.TrianglesAdjacency;
            }
            else if ( string.CompareOrdinal( lMode, "trianglestrip" ) == 0 )
            {
                this.Mode = PrimitiveType.TriangleStrip;
            }
            else if ( string.CompareOrdinal( lMode, "trianglestripadjacency" ) == 0 )
            {
                this.Mode = PrimitiveType.TriangleStripAdjacency;
            }
            else if ( string.CompareOrdinal( lMode, "trianglefan" ) == 0 )
            {
                this.Mode = PrimitiveType.TriangleFan;
            }
            else
            {
                LogManager.Instance.Log( string.Format( "Invalid mesh topology \"{0}\"!!!", lMode ), LogType.ERROR );
            }

            uint lAttributeCount = uint.Parse( lInfo[ lCurrInfoIndex++ ] );
            
            int lVertexSize = 0;
            int[] lAttributeIds = new int[ lAttributeCount ];
            bool[] lAttributeNormalize = new bool[ lAttributeCount ];
            int[] lAttributeComponents = new int[ lAttributeCount ];
            AttributeType[] lAttributeTypes = new AttributeType[ lAttributeCount ];
            for ( int lCurrAttribute = 0; lCurrAttribute < lAttributeCount; lCurrAttribute++ )
            {
                lAttributeIds[ lCurrAttribute ] = int.Parse( lInfo[ lCurrInfoIndex++ ] );
                lAttributeComponents[ lCurrAttribute ] = int.Parse( lInfo[ lCurrInfoIndex++ ] );

                string lType = lInfo[ lCurrInfoIndex++ ];
                if ( string.CompareOrdinal( lType, "byte" ) == 0 )
                {
                    lAttributeTypes[ lCurrAttribute ] = AttributeType.SBYTE_BASED;
                    lVertexSize += lAttributeComponents[ lCurrAttribute ] * 1;
                }
                else if ( string.CompareOrdinal( lType, "ubyte" ) == 0 )
                {
                    lAttributeTypes[ lCurrAttribute ] = AttributeType.BYTE_BASED;
                    lVertexSize += lAttributeComponents[ lCurrAttribute ] * 1;
                }
                else if ( string.CompareOrdinal( lType, "short" ) == 0 )
                {
                    lAttributeTypes[ lCurrAttribute ] = AttributeType.INT16_BASED;
                    lVertexSize += lAttributeComponents[ lCurrAttribute ] * 2;
                }
                else if ( string.CompareOrdinal( lType, "ushort" ) == 0 )
                {
                    lAttributeTypes[ lCurrAttribute ] = AttributeType.UINT16_BASED;
                    lVertexSize += lAttributeComponents[ lCurrAttribute ] * 2;
                }
                else if
                    ( string.CompareOrdinal( lType, "int" ) == 0 )
                {
                    lAttributeTypes[ lCurrAttribute ] = AttributeType.INT_BASED;
                    lVertexSize += lAttributeComponents[ lCurrAttribute ] * 4;
                }
                else if
                    ( string.CompareOrdinal( lType, "uint" ) == 0 )
                {
                    lAttributeTypes[ lCurrAttribute ] = AttributeType.UINT_BASED;
                    lVertexSize += lAttributeComponents[ lCurrAttribute ] * 4;
                }
                else if
                    ( string.CompareOrdinal( lType, "float" ) == 0 )
                {
                    lAttributeTypes[ lCurrAttribute ] = AttributeType.FLOAT_BASED;
                    lVertexSize += lAttributeComponents[ lCurrAttribute ] * 4;
                }
                else if
                    ( string.CompareOrdinal( lType, "double" ) == 0 )
                {
                    lAttributeTypes[ lCurrAttribute ] = AttributeType.DOUBLE_BASED;
                    lVertexSize += lAttributeComponents[ lCurrAttribute ] * 8;
                }
                else
                {
                    LogManager.Instance.Log( string.Format( "Invalid mesh vertex component type \"{0}\"!!!", lType ), LogType.ERROR );
                }

                string lToNormalize = lInfo[ lCurrInfoIndex++ ];
                if
                    ( string.CompareOrdinal( lToNormalize, "true" ) == 0 )
                {
                    lAttributeNormalize[ lCurrAttribute ] = true;
                }
                else if
                    ( string.CompareOrdinal( lToNormalize, "false" ) == 0 )
                {
                    lAttributeNormalize[ lCurrAttribute ] = false;
                }
                else
                {
                    LogManager.Instance.Log( string.Format( "Invalid mesh vertex normalization \"{0}\"!!!", lToNormalize ), LogType.ERROR );
                }
            }

            this.Attributes = new List<PipelineAttributeDescription>();
            PipelineAttributeDescription lLastAttribute = null;
            for ( int lCurrAttribute = 0; lCurrAttribute < lAttributeCount; lCurrAttribute++ )
            {
                PipelineAttributeDescription lNewAttribute = new PipelineAttributeDescription( lAttributeIds[ lCurrAttribute ], 
                                                                                               lAttributeComponents[ lCurrAttribute ], 
                                                                                               lAttributeTypes[ lCurrAttribute ],
                                                                                               lAttributeNormalize[ lCurrAttribute ], 
                                                                                               null,
                                                                                               lVertexSize,
                                                                                               lLastAttribute != null ? lLastAttribute.Offset + lLastAttribute.AttributeSize : 0 );
                this.Attributes.Add( lNewAttribute );
                lLastAttribute = lNewAttribute;
            }

            this.VertexCount = uint.Parse( lInfo[ lCurrInfoIndex++ ] );

            uint lOffset = 0;
            byte[] lVertexBuffer = new byte[ this.VertexCount * lVertexSize ];
            for ( int lCurrVertex = 0; lCurrVertex < this.VertexCount; lCurrVertex++ )
            {
                for ( int lCurrAttribute = 0; lCurrAttribute < lAttributeCount; lCurrAttribute++ )
                {
                    PipelineAttributeDescription lAttribute = this.Attributes[ lCurrAttribute ];
                    for ( int lCurrComponent = 0; lCurrComponent < lAttribute.ComponentCount; lCurrComponent++ )
                    {
                        switch ( lAttribute.Type )
                        {
                            case AttributeType.SBYTE_BASED:
                                {
                                    const int lSizeInBytes = sizeof(sbyte);
                                    sbyte lValue = sbyte.Parse( lInfo[ lCurrInfoIndex++ ] );
                                    byte lToAdd = Convert.ToByte( lValue );
                                    Array.Copy( new byte[ lSizeInBytes ] { lToAdd }, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            case AttributeType.BYTE_BASED:
                                {
                                    const int lSizeInBytes = sizeof(byte);
                                    byte lValue = byte.Parse( lInfo[ lCurrInfoIndex++ ] );
                                    Array.Copy( new byte[ lSizeInBytes ] { lValue }, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            case AttributeType.INT16_BASED:
                                {
                                    const int lSizeInBytes = sizeof(short);
                                    short lValue = short.Parse( lInfo[ lCurrInfoIndex++ ] );
                                    byte[] lToAdd = BitConverter.GetBytes( lValue );
                                    Array.Copy( lToAdd, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            case AttributeType.UINT16_BASED:
                                {
                                    const int lSizeInBytes = sizeof(ushort);
                                    ushort lValue = ushort.Parse( lInfo[ lCurrInfoIndex++ ] );
                                    byte[] lToAdd = BitConverter.GetBytes( lValue );
                                    Array.Copy( lToAdd, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            case AttributeType.INT_BASED:
                                {
                                    const int lSizeInBytes = sizeof(int);
                                    int lValue = int.Parse( lInfo[ lCurrInfoIndex++ ] );
                                    byte[] lToAdd = BitConverter.GetBytes( lValue );
                                    Array.Copy( lToAdd, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            case AttributeType.UINT_BASED:
                                {
                                    const int lSizeInBytes = sizeof(uint);
                                    uint lValue = uint.Parse( lInfo[ lCurrInfoIndex++ ] );
                                    byte[] lToAdd = BitConverter.GetBytes( lValue );
                                    Array.Copy( lToAdd, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            case AttributeType.HALF_BASED:
                                {
                                    const int lSizeInBytes = sizeof(short);
                                    Half lValue = new Half( float.Parse( lInfo[ lCurrInfoIndex++ ] ) );
                                    byte[] lToAdd = Half.GetBytes( lValue );
                                    Array.Copy( lToAdd, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            case AttributeType.FLOAT_BASED:
                                {
                                    const int lSizeInBytes = sizeof(float);
                                    float lValue = float.Parse( lInfo[ lCurrInfoIndex++ ] );
                                    byte[] lToAdd = BitConverter.GetBytes( lValue );
                                    Array.Copy( lToAdd, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            case AttributeType.DOUBLE_BASED:
                                {
                                    const int lSizeInBytes = sizeof(double);
                                    double lValue = double.Parse( lInfo[ lCurrInfoIndex++ ] );
                                    byte[] lToAdd = BitConverter.GetBytes( lValue );
                                    Array.Copy( lToAdd, 0, lVertexBuffer, lOffset, lSizeInBytes );
                                    lOffset += lSizeInBytes;
                                }
                                break;
                            // Not handled...
                            case AttributeType.INT_PACKED_BASED:
                            case AttributeType.UINT_PACKED_BASED:
                            case AttributeType.FIXED:
                                {
                                    LogManager.Instance.Log( "Unsupported attribute type!!!" );
                                }
                                break;
                        }
                    }
                }
            }

            GPUBuffer lVBuffer = new GPUBuffer();
            lVBuffer.SetData( lVertexBuffer, BufferUsageHint.StaticDraw );
            for ( int lCurrAttribute = 0; lCurrAttribute < lAttributeCount; lCurrAttribute++ )
            {
                this.Attributes[ lCurrAttribute ].Data = lVBuffer;
            }

            this.IndexCount = uint.Parse( lInfo[ lCurrInfoIndex++ ] );

            if ( this.IndexCount > 0 )
            {
                uint lIndexSize;
                AttributeType lType;
                if ( this.VertexCount < 256 )
                {
                    lIndexSize = 1;
                    lType = AttributeType.BYTE_BASED;
                }
                else if ( this.VertexCount < 65536 )
                {
                    lIndexSize = 2;
                    lType = AttributeType.UINT16_BASED;
                }
                else
                {
                    lIndexSize = 4;
                    lType = AttributeType.UINT_BASED;
                }

                lOffset = 0; // Reset.
                byte[] lIndicesBuffer = new byte[ this.IndexCount * lIndexSize ];
                if ( lIndexSize == 1 )
                {
                    for ( uint lCurrIndex = 0; lCurrIndex < this.IndexCount; lCurrIndex++ )
                    {
                        const int lSizeInBytes = sizeof(byte);
                        byte lValue = byte.Parse( lInfo[ lCurrInfoIndex++ ] );
                        Array.Copy( new byte[ lSizeInBytes ] { lValue }, 0, lIndicesBuffer, lOffset, lSizeInBytes );
                        lOffset += lSizeInBytes;
                    }
                }
                else if ( lIndexSize == 2 )
                {
                    for ( uint lCurrIndex = 0; lCurrIndex < this.IndexCount; lCurrIndex++ )
                    {
                        const int lSizeInBytes = sizeof(ushort);
                        ushort lValue = ushort.Parse( lInfo[ lCurrInfoIndex++ ] );
                        byte[] lToAdd = BitConverter.GetBytes( lValue );
                        Array.Copy( lToAdd, 0, lIndicesBuffer, lOffset, lSizeInBytes );
                        lOffset += lSizeInBytes;
                    }
                }
                else
                {
                    for ( uint lCurrIndex = 0; lCurrIndex < this.IndexCount; lCurrIndex++ )
                    {
                        byte lValue   = byte.Parse( lInfo[ lCurrInfoIndex++ ] );
                        byte[] lToAdd = new byte[ lIndexSize ];
                        lToAdd[ 0 ]   = lValue;
                        Array.Copy( lToAdd, 0, lIndicesBuffer, lOffset, lIndexSize );
                        lOffset += lIndexSize;
                    }
                }

                GPUBuffer lIBuffer = new GPUBuffer();
                lIBuffer.SetData( lIndicesBuffer, BufferUsageHint.StaticDraw );
                this.IndicesBuffer = new PipelineAttributeDescription( 0, 1, lType, false, lIBuffer );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new MeshBuffersResource( pName, this );
        }

        #endregion Methods
    }
}
