﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Texture based resource descriptor abstract class definition.
    /// </summary>
    public abstract class ATextureResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int);
                lSize += sizeof(PixelInternalFormat);
                lSize += sizeof(PixelFormat);
                lSize += sizeof(PixelType);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the texture internal format.
        /// </summary>
        public PixelInternalFormat InternalFormat
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the texture format (components count)
        /// </summary>
        public PixelFormat Format
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the texture components type.
        /// </summary>
        public PixelType Type
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the texture parameters.
        /// </summary>
        public TextureParameters Parameters
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the texture buffer optional storage parameters.
        /// </summary>
        public BufferLayoutParameters BufferParameters
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ATextureResourceDescriptor"/> class.
        /// </summary>
        protected ATextureResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ATextureResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        protected ATextureResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            PixelFormat lFormat = PixelFormat.Rgba;
            PixelType lType = PixelType.UnsignedByte;
            PixelInternalFormat lInternalFormat = PixelInternalFormat.Rgba;
            this.ReadParameters( pDescriptor, ref lInternalFormat, ref lFormat, ref lType );
            this.InternalFormat = lInternalFormat;
            this.Format = lFormat;
            this.Type = lType;

            TextureParameters lParameters = new TextureParameters();
            this.ReadParameters( pDescriptor, ref lParameters );
            this.Parameters = lParameters;

            BufferLayoutParameters lBufferParameters = new BufferLayoutParameters();
            if ( pContent != null )
            {
                lBufferParameters.CompressedSize = pContent.Length;
            }
            
            this.BufferParameters = lBufferParameters;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Reads the resource parameters for a texture.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pGPUFormat">The GPU texture format.</param>
        /// <param name="pFormat">The texture components count</param>
        /// <param name="pType">The texture components type.</param>
        protected void ReadParameters(XElement pDescriptor, ref PixelInternalFormat pGPUFormat, ref PixelFormat pFormat, ref PixelType pType)
        {
            XAttribute lXInternalFormat = pDescriptor.Attribute( "internalformat" );
            if
                ( lXInternalFormat == null )
            {
                LogManager.Instance.Log( "Missing internal format attribute!!!", LogType.ERROR );
                return;
            }

            string lInternalFormat = lXInternalFormat.Value;
            if 
                ( string.CompareOrdinal( lInternalFormat, "R8" ) == 0 )
            {
                pGPUFormat = PixelInternalFormat.R8;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R8SNORM") == 0)
            {
                pGPUFormat = PixelInternalFormat.R8Snorm;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R16") == 0)
            {
                pGPUFormat = PixelInternalFormat.R16;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R16_SNORM") == 0)
            {
                pGPUFormat = PixelInternalFormat.R16Snorm;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG8") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg8;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG8_SNORM") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg8Snorm;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG16") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg16;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG16_SNORM") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg16Snorm;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R3_G3_B2") == 0)
            {
                pGPUFormat = PixelInternalFormat.R3G3B2;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB4") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb4;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB5") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb5;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB8") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb8;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB8_SNORM") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb8Snorm;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB10") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb10;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB12") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb12;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB16") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb16;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB16_SNORM") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb16Snorm;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA2") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba2;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA4") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba4;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB5_A1") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb5A1;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA8") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba8;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA8_SNORM") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba8Snorm;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB10_A2") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb10A2;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB10_A2UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb10A2ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA12") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba12;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA16") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba16;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA16_SNORM") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba16Snorm;
            }
            else if (string.CompareOrdinal(lInternalFormat, "SRGB8") == 0)
            {
                pGPUFormat = PixelInternalFormat.Srgb8;
            }
            else if (string.CompareOrdinal(lInternalFormat, "SRGB8_ALPHA8") == 0)
            {
                pGPUFormat = PixelInternalFormat.Srgb8Alpha8;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R16F") == 0)
            {
                pGPUFormat = PixelInternalFormat.R16f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG16F") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg16f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB16F") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb16f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA16F") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba16f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R32F") == 0)
            {
                pGPUFormat = PixelInternalFormat.R32f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG32F") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg32f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB32F") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb32f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA32F") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba32f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R11F_G11F_B10F") == 0)
            {
                pGPUFormat = PixelInternalFormat.R11fG11fB10f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB9_E5") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb9E5;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R8I") == 0)
            {
                pGPUFormat = PixelInternalFormat.R8i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R8UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.R8ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R16I") == 0)
            {
                pGPUFormat = PixelInternalFormat.R16i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R16UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.R16ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R32I") == 0)
            {
                pGPUFormat = PixelInternalFormat.R32i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "R32UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.R32ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG8I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg8i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG8UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg8ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG16I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg16i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG16UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg16i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG32I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg32i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RG32UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rg32ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB8I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb8i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB8UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb8ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB16I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb16i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB16UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb16ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB32I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb32i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGB32UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgb32ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA8I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba8i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA8UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba8ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA16I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba16i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA16UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba16ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA32I") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba32i;
            }
            else if (string.CompareOrdinal(lInternalFormat, "RGBA32UI") == 0)
            {
                pGPUFormat = PixelInternalFormat.Rgba32ui;
            }
            else if (string.CompareOrdinal(lInternalFormat, "DEPTH_COMPONENT16") == 0)
            {
                pGPUFormat = PixelInternalFormat.DepthComponent16;
            }
            else if (string.CompareOrdinal(lInternalFormat, "DEPTH_COMPONENT24") == 0)
            {
                pGPUFormat = PixelInternalFormat.DepthComponent24;
            }
            else if (string.CompareOrdinal(lInternalFormat, "DEPTH_COMPONENT32F") == 0)
            {
                pGPUFormat = PixelInternalFormat.DepthComponent32f;
            }
            else if (string.CompareOrdinal(lInternalFormat, "DEPTH32F_STENCIL8") == 0)
            {
                pGPUFormat = PixelInternalFormat.Depth32fStencil8;
            }
            else if (string.CompareOrdinal(lInternalFormat, "DEPTH24_STENCIL8") == 0)
            {
                pGPUFormat = PixelInternalFormat.Depth24Stencil8;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RED") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRed;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RG") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRg;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGB") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgb;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGBA") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgba;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_SRGB") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedSrgb;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RED_RGTC1") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRedRgtc1;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_SIGNED_RED_RGTC1") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedSignedRedRgtc1;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RG_RGTC2") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgRgtc2;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_SIGNED_RG_RGTC2") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedSignedRgRgtc2;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGBA_BPTC_UNORM_ARB") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgbaBptcUnorm;
            }
            //else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_SRGB_ALPHA_BPTC_UNORM_ARB") == 0)
            //{
            //    pGPUFormat = PixelInternalFormat.CompressedSrgbAlphaBptcUnorm;
            //}
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgbBptcSignedFloat;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgbBptcUnsignedFloat;
            }
            else if (string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGB_S3TC_DXT1_EXT") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgbS3tcDxt1Ext;
            }
            else if 
                ( string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGBA_S3TC_DXT1_EXT") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgbaS3tcDxt1Ext;
            }
            else if 
                ( string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGBA_S3TC_DXT3_EXT") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgbaS3tcDxt3Ext;
            }
            else if 
                ( string.CompareOrdinal(lInternalFormat, "COMPRESSED_RGBA_S3TC_DXT5_EXT") == 0)
            {
                pGPUFormat = PixelInternalFormat.CompressedRgbaS3tcDxt5Ext;
            }
            else
            {
                LogManager.Instance.Log( "Bad 'internalformat' attribute", LogType.ERROR );
                return;
            }

            XAttribute lXFormat = pDescriptor.Attribute( "format" );
            if
                ( lXFormat == null )
            {
                LogManager.Instance.Log( "Missing format attribute!!!", LogType.ERROR );
                return;
            }

            string lFormat = lXFormat.Value;
            if 
                ( string.CompareOrdinal( lFormat, "STENCIL_INDEX") == 0)
            {
                pFormat = PixelFormat.StencilIndex;
            }
            else if ( string.CompareOrdinal( lFormat, "DEPTH_COMPONENT") == 0)
            {
                pFormat = PixelFormat.DepthComponent;
            }
            else if ( string.CompareOrdinal( lFormat, "DEPTH_STENCIL") == 0)
            {
                pFormat = PixelFormat.DepthStencil;
            }
            else if ( string.CompareOrdinal( lFormat, "RED") == 0)
            {
                pFormat = PixelFormat.Red;
            }
            else if ( string.CompareOrdinal( lFormat, "GREEN") == 0)
            {
                pFormat = PixelFormat.Green;
            }
            else if ( string.CompareOrdinal( lFormat, "BLUE") == 0)
            {
                pFormat = PixelFormat.Blue;
            }
            else if ( string.CompareOrdinal( lFormat, "RG") == 0)
            {
                pFormat = PixelFormat.Rg;
            }
            else if ( string.CompareOrdinal( lFormat, "RGB") == 0)
            {
                pFormat = PixelFormat.Rgb;
            }
            else if ( string.CompareOrdinal( lFormat, "RGBA") == 0)
            {
                pFormat = PixelFormat.Rgba;
            }
            else if ( string.CompareOrdinal( lFormat, "BGR") == 0)
            {
                pFormat = PixelFormat.Bgr;
            }
            else if ( string.CompareOrdinal( lFormat, "BGRA") == 0)
            {
                pFormat = PixelFormat.Bgra;
            }
            else if ( string.CompareOrdinal( lFormat, "RED_INTEGER") == 0)
            {
                pFormat = PixelFormat.RedInteger;
            }
            else if ( string.CompareOrdinal( lFormat, "BLUE_INTEGER") == 0)
            {
                pFormat = PixelFormat.BlueInteger;
            }
            else if ( string.CompareOrdinal( lFormat, "GREEN_INTEGER") == 0)
            {
                pFormat = PixelFormat.GreenInteger;
            }
            else if ( string.CompareOrdinal( lFormat, "RG_INTEGER") == 0)
            {
                pFormat = PixelFormat.RgInteger;
            }
            else if ( string.CompareOrdinal( lFormat, "RGB_INTEGER") == 0)
            {
                pFormat = PixelFormat.RgbInteger;
            }
            else if ( string.CompareOrdinal( lFormat, "RGBA_INTEGER") == 0)
            {
                pFormat = PixelFormat.RgbaInteger;
            }
            else if ( string.CompareOrdinal( lFormat, "BGR_INTEGER") == 0)
            {
                pFormat = PixelFormat.BgrInteger;
            }
            else if ( string.CompareOrdinal( lFormat, "BGRA_INTEGER") == 0)
            {
                pFormat = PixelFormat.BgraInteger;
            }
            else
            {
                LogManager.Instance.Log( "Bad 'format' attribute", LogType.ERROR );
                return;
            }

            XAttribute lXType = pDescriptor.Attribute( "type" );
            if
                ( lXType == null )
            {
                LogManager.Instance.Log( "Missing type attribute!!!", LogType.ERROR );
                return;
            }

            string lType = lXType.Value;
            if 
                ( string.CompareOrdinal( lType, 0, "UNSIGNED_BYTE", 0, 13) == 0)
            {
                pType = PixelType.UnsignedByte;
            }
            else if (string.CompareOrdinal(lType, "BYTE") == 0)
            {
                pType = PixelType.Byte;
            }
            else if ( string.CompareOrdinal( lType, 0, "UNSIGNED_SHORT", 0, 14) == 0)
            {
                pType = PixelType.UnsignedShort;
            }
            else if (string.CompareOrdinal(lType, "SHORT") == 0)
            {
                pType = PixelType.Short;
            }
            else if ( string.CompareOrdinal( lType, 0, "UNSIGNED_INT", 0, 12) == 0)
            {
                pType = PixelType.UnsignedInt;
            }
            else if (string.CompareOrdinal(lType, "INT") == 0)
            {
                pType = PixelType.Int;
            }
            else if (string.CompareOrdinal(lType, "FLOAT") == 0)
            {
                pType = PixelType.Float;
            }
            else if (string.CompareOrdinal(lType, "HALF") == 0)
            {
                pType = PixelType.HalfFloat;
            }
            else
            {
                LogManager.Instance.Log( "Bad 'type' attribute", LogType.ERROR );
                return;
            }
        }

        /// <summary>
        /// Reads the resource parameters for a texture.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pParameters">The resulting texture parameters</param>
        protected void ReadParameters(XElement pDescriptor, ref TextureParameters pParameters)
        {
            XAttribute lXMin = pDescriptor.Attribute( "min" );
            if
                ( lXMin != null )
            {
                string lMin = lXMin.Value;
                if 
                    ( string.CompareOrdinal( lMin, "NEAREST") == 0)
                {
                    pParameters.MinFilter = TextureMinFilter.Nearest;
                }
                else if ( string.CompareOrdinal( lMin, "LINEAR") == 0)
                {
                    pParameters.MinFilter = TextureMinFilter.Linear;
                }
                else if ( string.CompareOrdinal( lMin, "NEAREST_MIPMAP_NEAREST") == 0)
                {
                    pParameters.MinFilter = TextureMinFilter.NearestMipmapNearest;
                }
                else if ( string.CompareOrdinal( lMin, "NEAREST_MIPMAP_LINEAR") == 0)
                {
                    pParameters.MinFilter = TextureMinFilter.NearestMipmapLinear;
                }
                else if ( string.CompareOrdinal( lMin, "LINEAR_MIPMAP_NEAREST") == 0)
                {
                    pParameters.MinFilter = TextureMinFilter.LinearMipmapNearest;
                }
                else if ( string.CompareOrdinal( lMin, "LINEAR_MIPMAP_LINEAR") == 0)
                {
                    pParameters.MinFilter = TextureMinFilter.LinearMipmapLinear;
                }
                else
                {
                    LogManager.Instance.Log( "Bad minification attribute", LogType.ERROR );
                    return;
                }
            }
            
            XAttribute lXMag = pDescriptor.Attribute( "mag" );
            if
                ( lXMag != null )
            {
                string lMag = lXMag.Value;
                if 
                    ( string.CompareOrdinal( lMag, "NEAREST") == 0)
                {
                    pParameters.MagFilter = TextureMagFilter.Nearest;
                }
                else if 
                    ( string.CompareOrdinal( lMag, "LINEAR") == 0)
                {
                    pParameters.MagFilter = TextureMagFilter.Linear;
                }
                else
                {
                    LogManager.Instance.Log( "Bad magnification attribute", LogType.ERROR );
                    return;
                }
            }

            XAttribute lXWraps = pDescriptor.Attribute( "wraps" );
            if
                ( lXWraps != null )
            {
                string lWraps = lXWraps.Value;
                if 
                    ( string.CompareOrdinal( lWraps, "CLAMP_TO_EDGE") == 0)
                {
                    pParameters.WrapS = TextureWrapMode.ClampToEdge;
                }
                else if 
                    ( string.CompareOrdinal( lWraps, "CLAMP_TO_BORDER") == 0)
                {
                    pParameters.WrapS = TextureWrapMode.ClampToBorder;
                }
                else if 
                    ( string.CompareOrdinal( lWraps, "REPEAT") == 0)
                {
                    pParameters.WrapS = TextureWrapMode.Repeat;
                }
                else if 
                    ( string.CompareOrdinal( lWraps, "MIRRORED_REPEAT") == 0)
                {
                    pParameters.WrapS = TextureWrapMode.MirroredRepeat;
                }
                else
                {
                    LogManager.Instance.Log( "Bad wrap S attribute", LogType.ERROR );
                    return;
                }
            }

            XAttribute lXWrapt = pDescriptor.Attribute( "wrapt" );
            if
                ( lXWrapt != null )
            {
                string lWrapt = lXWrapt.Value;
                if 
                    ( string.CompareOrdinal( lWrapt, "CLAMP_TO_EDGE") == 0)
                {
                    pParameters.WrapT = TextureWrapMode.ClampToEdge;
                }
                else if 
                    ( string.CompareOrdinal( lWrapt, "CLAMP_TO_BORDER") == 0)
                {
                    pParameters.WrapT = TextureWrapMode.ClampToBorder;
                }
                else if 
                    ( string.CompareOrdinal( lWrapt, "REPEAT") == 0)
                {
                    pParameters.WrapT = TextureWrapMode.Repeat;
                }
                else if 
                    ( string.CompareOrdinal( lWrapt, "MIRRORED_REPEAT") == 0)
                {
                    pParameters.WrapT = TextureWrapMode.MirroredRepeat;
                }
                else
                {
                    LogManager.Instance.Log( "Bad wrap T attribute", LogType.ERROR );
                    return;
                }
            }

            XAttribute lXWrapr = pDescriptor.Attribute( "wrapr" );
            if
                ( lXWrapr != null )
            {
                string lWrapr = lXWrapr.Value;
                if 
                    ( string.CompareOrdinal( lWrapr, "CLAMP_TO_EDGE") == 0)
                {
                    pParameters.WrapR = TextureWrapMode.ClampToEdge;
                }
                else if 
                    ( string.CompareOrdinal( lWrapr, "CLAMP_TO_BORDER") == 0)
                {
                    pParameters.WrapR = TextureWrapMode.ClampToBorder;
                }
                else if 
                    ( string.CompareOrdinal( lWrapr, "REPEAT") == 0)
                {
                    pParameters.WrapR = TextureWrapMode.Repeat;
                }
                else if 
                    ( string.CompareOrdinal( lWrapr, "MIRRORED_REPEAT") == 0)
                {
                    pParameters.WrapR = TextureWrapMode.MirroredRepeat;
                }
                else
                {
                    LogManager.Instance.Log( "Bad wrap R attribute", LogType.ERROR );
                    return;
                }
            }

            XAttribute lXBorderr = pDescriptor.Attribute( "borderr" );
            if
                ( lXBorderr != null )
            {
                string lBorderR = lXBorderr.Value;
                string lBorderG = pDescriptor.Attribute( "borderg" ).Value;
                string lBorderB = pDescriptor.Attribute( "borderb" ).Value;
                string lBorderA = pDescriptor.Attribute( "bordera" ).Value;

                pParameters.BorderColor = Color.FromArgb( (int)(float.Parse( lBorderA ) * 255), (int)(float.Parse( lBorderR ) * 255), (int)(float.Parse( lBorderG ) * 255), (int)(float.Parse( lBorderB ) * 255) );
            }

            XAttribute lXMinLevel = pDescriptor.Attribute( "minLevel" );
            if
                ( lXMinLevel != null )
            {
                float lLevel = float.Parse( lXMinLevel.Value );
                pParameters.MinLODIndex = (int)lLevel;
            }

            XAttribute lXMaxLevel = pDescriptor.Attribute( "maxLevel" );
            if
                ( lXMaxLevel != null )
            {
                float lLevel = float.Parse( lXMaxLevel.Value );
                pParameters.MaxLODIndex = (int)lLevel;
            }

            XAttribute lXMinLOD = pDescriptor.Attribute( "minLod" );
            if
                ( lXMinLOD != null )
            {
                float lMinLOD = float.Parse( lXMinLOD.Value );
                pParameters.LODMin = (int)lMinLOD;
            }

            XAttribute lXMaxLOD = pDescriptor.Attribute( "maxLod" );
            if
                ( lXMaxLOD != null )
            {
                float lMaxLOD = float.Parse( lXMaxLOD.Value );
                pParameters.LODMax = (int)lMaxLOD;
            }

            XAttribute lXBias = pDescriptor.Attribute( "bias" );
            if
                ( lXBias != null )
            {
                float lBias = float.Parse( lXBias.Value );
                pParameters.LODBias = lBias;
            }

            XAttribute lXAnisotropy = pDescriptor.Attribute( "anisotropy" );
            if
                ( lXAnisotropy != null )
            {
                float lAnisotropy = float.Parse( lXAnisotropy.Value );
                pParameters.MaxAnisotropy = lAnisotropy;
            }

            XAttribute lXCompare = pDescriptor.Attribute( "compare" );
            if
                ( lXCompare != null )
            {
                string lCompare = lXCompare.Value;
                if 
                    ( string.CompareOrdinal( lCompare, "LEQUAL") == 0)
                {
                    pParameters.CompareFunction = TextureComparison.Lequal;
                }
                else if 
                    ( string.CompareOrdinal( lCompare, "GEQUAL") == 0)
                {
                    pParameters.CompareFunction = TextureComparison.Gequal;
                }
                else if 
                    ( string.CompareOrdinal( lCompare, "LESS") == 0)
                {
                    pParameters.CompareFunction = TextureComparison.Less;
                }
                else if 
                    ( string.CompareOrdinal( lCompare, "GREATER") == 0)
                {
                    pParameters.CompareFunction = TextureComparison.Greater;
                }
                else if 
                    ( string.CompareOrdinal( lCompare, "EQUAL") == 0)
                {
                    pParameters.CompareFunction = TextureComparison.Equal;
                }
                else if 
                    ( string.CompareOrdinal( lCompare, "NOTEQUAL") == 0)
                {
                    pParameters.CompareFunction = TextureComparison.Notequal;
                }
                else if 
                    ( string.CompareOrdinal( lCompare, "ALWAYS") == 0)
                {
                    pParameters.CompareFunction = TextureComparison.Always;
                }
                else if 
                    ( string.CompareOrdinal( lCompare, "NEVER") == 0)
                {
                    pParameters.CompareFunction = TextureComparison.Never;
                }
                else
                {
                    LogManager.Instance.Log( "Bad compare function attribute", LogType.ERROR );
                    return;
                }
            }

            XAttribute lXSwizzle = pDescriptor.Attribute( "swizzle" );
            if
                ( lXSwizzle != null )
            {
                string lSwizzle = lXSwizzle.Value;
                if 
                    ( lSwizzle.Length == 4 )
                {
                    pParameters.Swizzle = new char[ 4 ]
                                          {
                                             lSwizzle[ 0 ],
                                             lSwizzle[ 1 ],
                                             lSwizzle[ 2 ],
                                             lSwizzle[ 3 ]
                                          };
                }
                else
                {
                    LogManager.Instance.Log( "Bad swizzle attribute", LogType.ERROR );
                    return;
                }
            }
        }

        #region Methods IDisposable

        /// <summary>
        /// Releases resource's resources.
        /// </summary>
        protected override void OnDispose()
        {
            this.Parameters = null;
            this.BufferParameters = null;

            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
