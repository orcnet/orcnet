﻿using OrcNet.Core.Resource;
using OrcNet.Core.Service;
using OrcNet.Graphics.Render;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Show info task factory descriptor class definition used to
    /// create ShowInfoTask factory based resource(s).
    /// </summary>
    public class ShowInfoTaskResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "showInfo";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(int) * 3;
                lSize += sizeof(byte) * 4; // Color.
                lSize += sizeof(float);
                lSize += this.TextFont.Size;
                lSize += this.TextPass.Size;

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the font used to draw the text.
        /// </summary>
        public Text.Font TextFont
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the pass to use to draw the text.
        /// </summary>
        public PipelinePass TextPass
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the color used to draw the text.
        /// </summary>
        public Color TextColor
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the font height.
        /// </summary>
        public float FontHeight
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the X Component of the screen position the text must be displayed at.
        /// </summary>
        public int ScreenX
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Y Component of the screen position the text must be displayed at.
        /// </summary>
        public int ScreenY
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the maximum line count for the overall text to draw.
        /// </summary>
        public int MaxLineCount
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowInfoTaskResourceDescriptor"/> class.
        /// </summary>
        private ShowInfoTaskResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowInfoTaskResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public ShowInfoTaskResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            this.ScreenX = 4;
            this.ScreenY = -4;
            this.MaxLineCount = 8;
            this.TextColor = Color.FromArgb( 255, 255, 0, 0 );

            string lFontName = "defaultFont";
            XAttribute lXFont = pDescriptor.Attribute( "font" );
            if ( lXFont != null )
            {
                lFontName = lXFont.Value;
            }

            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            if ( lResourceService != null )
            {
                IResource lFontResource = lResourceService.LoadResource( lFontName );
                if ( lFontResource != null )
                {
                    this.TextFont = lFontResource.OwnedObject as Text.Font;
                    this.FontHeight = this.TextFont.TileHeight;
                }
            }

            XAttribute lXFontSize = pDescriptor.Attribute( "fontSize" );
            if ( lXFontSize != null )
            {
                this.FontHeight = float.Parse( lXFontSize.Value );
            }

            XAttribute lXScreenX = pDescriptor.Attribute( "x" );
            if ( lXScreenX != null )
            {
                this.ScreenX = int.Parse( lXScreenX.Value );
            }

            XAttribute lXScreenY = pDescriptor.Attribute( "y" );
            if ( lXScreenY != null )
            {
                this.ScreenY = int.Parse( lXScreenY.Value );
            }

            XAttribute lXMaxLine = pDescriptor.Attribute( "maxLines" );
            if ( lXMaxLine != null )
            {
                this.MaxLineCount = int.Parse( lXMaxLine.Value );
            }

            XAttribute lXFontColor = pDescriptor.Attribute( "fontColor" );
            if ( lXFontColor != null )
            {
                string[] lColorBytes = lXFontColor.Value.Split( ',' );
                byte lRed   = byte.Parse( lColorBytes[ 0 ] );
                byte lGreen = byte.Parse( lColorBytes[ 1 ] );
                byte lBlue  = byte.Parse( lColorBytes[ 2 ] );
                this.TextColor = Color.FromArgb( 255, lRed, lGreen, lBlue );
            }

            string lFontPassName = "text;";
            XAttribute lXFontPass = pDescriptor.Attribute( "fontProgram" );
            if ( lXFontPass != null )
            {
                lFontPassName = lXFontPass.Value;
            }

            if ( lResourceService != null )
            {
                IResource lFontResource = lResourceService.LoadResource( lFontPassName );
                if ( lFontResource != null )
                {
                    this.TextPass = lFontResource.OwnedObject as PipelinePass;
                }
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new ShowInfoTaskResource( pName, this );
        }

        #endregion Methods
    }
}
