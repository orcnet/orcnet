﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Core.Resource;
using OrcNet.Core.Service;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Render.Uniforms.Values;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Pipeline description descriptor class definition used to
    /// create pipeline description based resource(s).
    /// </summary>
    public class PipelineDescriptionDescriptor : AResourceDescriptor
    {
        #region Delegates

        /// <summary>
        /// Builds the proper pipeline value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private delegate void PipelineValueDecoder(XElement pDescriptor, out IPipelineValue pValue);

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the set of pipeline value decoders.
        /// </summary>
        private static Dictionary<string, PipelineValueDecoder> sValueDecoders;

        #endregion Fields

        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "module";
            }
        }

        #endregion Properties IResourceFactory

        /// <summary>
        /// Gets the pipeline description initial value(s).
        /// </summary>
        public List<IPipelineValue> InitialValues
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the feedback mode.
        /// </summary>
        public PipelineVaryingFeedback FeedbackMode
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Gets the pipeline description source code owning stage(s).
        /// </summary>
        public string Source
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the pipeline description varying value(s) name(s).
        /// </summary>
        public string[] Varyings
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof( PipelineVaryingFeedback );
                foreach
                    ( IPipelineValue lInitial in this.InitialValues )
                {
                    lSize += lInitial.Size;
                }
                if
                    ( this.Varyings != null )
                {
                    foreach
                        ( string lVarying in this.Varyings )
                    {
                        lSize += sizeof(char) * (uint)lVarying.Length;
                    }
                }

                return lSize;
            }
        }

        /// <summary>
        /// Gets the pipeline description version
        /// </summary>
        public int Version
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes the static member(s) of the <see cref="PipelineDescriptionDescriptor"/> class.
        /// </summary>
        static PipelineDescriptionDescriptor()
        {
            sValueDecoders = new Dictionary<string, PipelineValueDecoder>();
            sValueDecoders.Add( "uniform1f",  Uniform1fValueDecoder );
            sValueDecoders.Add( "uniform1d",  Uniform1dValueDecoder );
            sValueDecoders.Add( "uniform1i",  Uniform1iValueDecoder );
            sValueDecoders.Add( "uniform1b",  Uniform1bValueDecoder );
            sValueDecoders.Add( "uniform1vi", Uniform1viValueDecoder );
            sValueDecoders.Add( "uniform2f",  Uniform2fValueDecoder );
            sValueDecoders.Add( "uniform2d",  Uniform2dValueDecoder );
            sValueDecoders.Add( "uniform2i",  Uniform2iValueDecoder );
            sValueDecoders.Add( "uniform2b",  Uniform2bValueDecoder );
            sValueDecoders.Add( "uniform2vi", Uniform2viValueDecoder );
            sValueDecoders.Add( "uniform3f",  Uniform3fValueDecoder );
            sValueDecoders.Add( "uniform3d",  Uniform3dValueDecoder );
            sValueDecoders.Add( "uniform3i",  Uniform3iValueDecoder );
            sValueDecoders.Add( "uniform3b",  Uniform3bValueDecoder );
            sValueDecoders.Add( "uniform3vi", Uniform3viValueDecoder );
            sValueDecoders.Add( "uniform4f",  Uniform4fValueDecoder );
            sValueDecoders.Add( "uniform4d",  Uniform4dValueDecoder );
            sValueDecoders.Add( "uniform4i",  Uniform4iValueDecoder );
            sValueDecoders.Add( "uniform4b",  Uniform4bValueDecoder );
            sValueDecoders.Add( "uniform4vi", Uniform4viValueDecoder );

            sValueDecoders.Add( "uniformMatrix2f",    UniformMatrix2fValueDecoder );
            sValueDecoders.Add( "uniformMatrix2d",    UniformMatrix2dValueDecoder );
            sValueDecoders.Add( "uniformMatrix2x3f",  UniformMatrix2x3fValueDecoder );
            sValueDecoders.Add( "uniformMatrix2x3d",  UniformMatrix2x3dValueDecoder );
            sValueDecoders.Add( "uniformMatrix2x4f",  UniformMatrix2x4fValueDecoder );
            sValueDecoders.Add( "uniformMatrix2x4d",  UniformMatrix2x4dValueDecoder );
            sValueDecoders.Add( "uniformMatrix3f",    UniformMatrix3fValueDecoder );
            sValueDecoders.Add( "uniformMatrix3d",    UniformMatrix3dValueDecoder );
            sValueDecoders.Add( "uniformMatrix3x2f",  UniformMatrix3x2fValueDecoder );
            sValueDecoders.Add( "uniformMatrix3x2d",  UniformMatrix3x2dValueDecoder );
            sValueDecoders.Add( "uniformMatrix3x4f",  UniformMatrix3x4fValueDecoder );
            sValueDecoders.Add( "uniformMatrix3x4d",  UniformMatrix3x4dValueDecoder );
            sValueDecoders.Add( "uniformMatrix4f",    UniformMatrix4fValueDecoder );
            sValueDecoders.Add( "uniformMatrix4d",    UniformMatrix4dValueDecoder );
            sValueDecoders.Add( "uniformMatrix4x2f",  UniformMatrix4x2fValueDecoder );
            sValueDecoders.Add( "uniformMatrix4x2d",  UniformMatrix4x2dValueDecoder );
            sValueDecoders.Add( "uniformMatrix4x3f",  UniformMatrix4x3fValueDecoder );
            sValueDecoders.Add( "uniformMatrix4x3d",  UniformMatrix4x3dValueDecoder );

            sValueDecoders.Add( "uniformSampler",    UniformTextureValueDecoder );
            sValueDecoders.Add( "uniformSubroutine", UniformSubroutineValueDecoder );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineDescriptionDescriptor"/> class.
        /// </summary>
        private PipelineDescriptionDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineDescriptionDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public PipelineDescriptionDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            this.Version = -1;
            XAttribute lXVersion = pDescriptor.Attribute( "version" );
            if ( lXVersion != null )
            {
                this.Version = int.Parse( lXVersion.Value );
            }

            this.InitialValues = new List<IPipelineValue>();
            foreach ( XElement lChild in pDescriptor.Elements() )
            {
                IPipelineValue lValue;
                string lType = lChild.Name();
                PipelineValueDecoder lDecoder;
                if ( sValueDecoders.TryGetValue( lType, out lDecoder ) )
                {
                    lDecoder( lChild, out lValue );
                    if ( lValue != null )
                    {
                        this.InitialValues.Add( lValue );
                    }
                    else
                    {
                        LogManager.Instance.Log( string.Format( "Null uniform value decoder for the {0} type!!!", lType ), LogType.ERROR );
                    }
                }
                else
                {
                    LogManager.Instance.Log( string.Format( "Missing uniform value decoder for the {0} type!!!", lType ), LogType.ERROR );
                }
            }

            StringBuilder lBuilder = new StringBuilder();

            // Any option(s)?
            XAttribute lXOptions = pDescriptor.Attribute( "options" );
            if ( lXOptions != null )
            {
                string lOptions = lXOptions.Value;
                string[] lSplittedOptions = lOptions.Split( ',' );
                foreach ( string lOption in lSplittedOptions )
                {
                    lBuilder.Append( "#define " );
                    lBuilder.Append( lOption );
                    lBuilder.AppendLine();
                }
            }

            // Provides source for stage(s).
            if ( this.Content != null )
            {
                string lSource = pEncoding.GetString( this.Content );
                if ( lXOptions != null )
                {
                    lBuilder.Append( lSource );
                    this.Source = lBuilder.ToString();
                }
                else
                {
                    this.Source = lSource;
                }
            }
            
            // Any feedback requested?
            XAttribute lXFeedback = pDescriptor.Attribute( "feedback" );
            if ( lXFeedback != null )
            {
                if ( string.Compare( lXFeedback.Value, "interleaved" )  == 0 )
                {
                    this.FeedbackMode = PipelineVaryingFeedback.INTERLEAVED_ATTRIBUTES;
                }
                else
                {
                    this.FeedbackMode = PipelineVaryingFeedback.SEPARATE_ATTRIBUTES;
                }

                // If any feedback atribute, it could have varying value(s) to look for as well.
                string lVaryingStr = pDescriptor.Attribute( "varyings" ).Value;
                this.Varyings = lVaryingStr.Split( ',' );
            }
            else
            {
                this.FeedbackMode = PipelineVaryingFeedback.ANY;
            }
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal
        
        /// <summary>
        /// Builds an uniform1 float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform1fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue = pDescriptor.Attribute( "x" );
            float lFloat;
            FloatValue lValue = new FloatValue( lName );
            if ( lXValue != null &&
                 float.TryParse( lXValue.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
            {
                lValue.Value = lFloat;
            }
            else
            {
                lValue.Value = 0.0f;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform1 double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform1dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue = pDescriptor.Attribute( "x" );
            double lDouble;
            DoubleValue lValue = new DoubleValue( lName );
            if ( lXValue != null &&
                 double.TryParse( lXValue.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
            {
                lValue.Value = lDouble;
            }
            else
            {
                lValue.Value = 0.0;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform1 int based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform1iValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue = pDescriptor.Attribute( "x" );
            int lInt;
            IntValue lValue = new IntValue( lName );
            if ( lXValue != null &&
                 int.TryParse( lXValue.Value, out lInt ) )
            {
                lValue.Value = lInt;
            }
            else
            {
                lValue.Value = 0;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform1 bool based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform1bValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue = pDescriptor.Attribute( "x" );
            bool lBool;
            BoolValue lValue = new BoolValue( lName );
            if ( lXValue != null &&
                 bool.TryParse( lXValue.Value, out lBool ) )
            {
                lValue.Value = lBool;
            }
            else
            {
                lValue.Value = false;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform1 int set based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform1viValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue = pDescriptor.Attribute( "x" );
            uint lUInt;
            UIntValue lValue = new UIntValue( lName );
            if ( lXValue != null &&
                 uint.TryParse( lXValue.Value, out lUInt ) )
            {
                lValue.Value = lUInt;
            }
            else
            {
                lValue.Value = 0;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform2 float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform2fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            float lFloat1;
            float lFloat2;
            Vector2FValue lValue = new Vector2FValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 float.TryParse( lXValue1.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat1 ) &&
                 float.TryParse( lXValue2.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat2 ) )
            {
                lValue.Value = new Vector2F( lFloat1, lFloat2 );
            }
            else
            {
                lValue.Value = new Vector2F( 0.0f );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform2 double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform2dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            double lDouble1;
            double lDouble2;
            Vector2DValue lValue = new Vector2DValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 double.TryParse( lXValue1.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble1 ) &&
                 double.TryParse( lXValue2.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble2 ) )
            {
                lValue.Value = new Vector2D( lDouble1, lDouble2 );
            }
            else
            {
                lValue.Value = new Vector2D( 0.0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform2 int based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform2iValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            // Extract ints but fill as float as ther is no such structure in OpenTK.
            int lInt1;
            int lInt2;
            Vector2IValue lValue = new Vector2IValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 int.TryParse( lXValue1.Value, out lInt1 ) &&
                 int.TryParse( lXValue2.Value, out lInt2 ) )
            {
                lValue.Value = new Vector2I( lInt1, lInt2 );
            }
            else
            {
                lValue.Value = new Vector2I( 0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform2 bool based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform2bValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            // Extract bools but fill as float as ther is no such structure in OpenTK.
            bool lBool1;
            bool lBool2;
            Vector2BValue lValue = new Vector2BValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 bool.TryParse( lXValue1.Value, out lBool1 ) &&
                 bool.TryParse( lXValue2.Value, out lBool2 ) )
            {
                lValue.Value = new Tuple<bool, bool>( lBool1, lBool2 );
            }
            else
            {
                lValue.Value = new Tuple<bool, bool>( false, false );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform2 int set based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform2viValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            // Extract uints but fill as float as ther is no such structure in OpenTK.
            uint lUInt1;
            uint lUInt2;
            Vector2UIValue lValue = new Vector2UIValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 uint.TryParse( lXValue1.Value, out lUInt1 ) &&
                 uint.TryParse( lXValue2.Value, out lUInt2 ) )
            {
                lValue.Value = new Vector2UI( lUInt1, lUInt2 );
            }
            else
            {
                lValue.Value = new Vector2UI( 0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform3 float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform3fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            float lFloat1;
            float lFloat2;
            float lFloat3;
            Vector3FValue lValue = new Vector3FValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 float.TryParse( lXValue1.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat1 ) &&
                 float.TryParse( lXValue2.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat2 ) &&
                 float.TryParse( lXValue3.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat3 ) )
            {
                lValue.Value = new Vector3F( lFloat1, lFloat2, lFloat3 );
            }
            else
            {
                lValue.Value = new Vector3F( 0.0f );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform3 double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform3dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            double lDouble1;
            double lDouble2;
            double lDouble3;
            Vector3DValue lValue = new Vector3DValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 double.TryParse( lXValue1.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble1 ) &&
                 double.TryParse( lXValue2.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble2 ) &&
                 double.TryParse( lXValue3.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble3 ) )
            {
                lValue.Value = new Vector3D( lDouble1, lDouble2, lDouble3 );
            }
            else
            {
                lValue.Value = new Vector3D( 0.0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform3 int based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform3iValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            // Extract ints but fill as float as ther is no such structure in OpenTK.
            int lInt1;
            int lInt2;
            int lInt3;
            Vector3IValue lValue = new Vector3IValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 int.TryParse( lXValue1.Value, out lInt1 ) &&
                 int.TryParse( lXValue2.Value, out lInt2 ) &&
                 int.TryParse( lXValue3.Value, out lInt3 ) )
            {
                lValue.Value = new Vector3I( lInt1, lInt2, lInt3 );
            }
            else
            {
                lValue.Value = new Vector3I( 0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform3 bool based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform3bValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            // Extract bools but fill as float as ther is no such structure in OpenTK.
            bool lBool1;
            bool lBool2;
            bool lBool3;
            Vector3BValue lValue = new Vector3BValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 bool.TryParse( lXValue1.Value, out lBool1 ) &&
                 bool.TryParse( lXValue2.Value, out lBool2 ) &&
                 bool.TryParse( lXValue3.Value, out lBool3 ) )
            {
                lValue.Value = new Tuple<bool, bool, bool>( lBool1, lBool2, lBool3 );
            }
            else
            {
                lValue.Value = new Tuple<bool, bool, bool>( false, false, false );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform3 int set based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform3viValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            // Extract uints but fill as float as ther is no such structure in OpenTK.
            uint lUInt1;
            uint lUInt2;
            uint lUInt3;
            Vector3UIValue lValue = new Vector3UIValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 uint.TryParse( lXValue1.Value, out lUInt1 ) &&
                 uint.TryParse( lXValue2.Value, out lUInt2 ) &&
                 uint.TryParse( lXValue3.Value, out lUInt3 ) )
            {
                lValue.Value = new Vector3UI( lUInt1, lUInt2, lUInt3 );
            }
            else
            {
                lValue.Value = new Vector3UI( 0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform4 float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform4fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            XAttribute lXValue4 = pDescriptor.Attribute( "w" );
            float lFloat1;
            float lFloat2;
            float lFloat3;
            float lFloat4;
            Vector4FValue lValue = new Vector4FValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 lXValue4 != null &&
                 float.TryParse( lXValue1.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat1 ) &&
                 float.TryParse( lXValue2.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat2 ) &&
                 float.TryParse( lXValue3.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat3 ) &&
                 float.TryParse( lXValue4.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat4 ) )
            {
                lValue.Value = new Vector4F( lFloat1, lFloat2, lFloat3, lFloat4 );
            }
            else
            {
                lValue.Value = new Vector4F( 0.0f );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform4 double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform4dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            XAttribute lXValue4 = pDescriptor.Attribute( "w" );
            double lDouble1;
            double lDouble2;
            double lDouble3;
            double lDouble4;
            Vector4DValue lValue = new Vector4DValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 lXValue4 != null &&
                 double.TryParse( lXValue1.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble1 ) &&
                 double.TryParse( lXValue2.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble2 ) &&
                 double.TryParse( lXValue3.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble3 ) &&
                 double.TryParse( lXValue4.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble4 ) )
            {
                lValue.Value = new Vector4D( lDouble1, lDouble2, lDouble3, lDouble4 );
            }
            else
            {
                lValue.Value = new Vector4D( 0.0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform4 int based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform4iValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            XAttribute lXValue4 = pDescriptor.Attribute( "w" );
            // Extract ints but fill as float as ther is no such structure in OpenTK.
            int lInt1;
            int lInt2;
            int lInt3;
            int lInt4;
            Vector4IValue lValue = new Vector4IValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 lXValue4 != null &&
                 int.TryParse( lXValue1.Value, out lInt1 ) &&
                 int.TryParse( lXValue2.Value, out lInt2 ) &&
                 int.TryParse( lXValue3.Value, out lInt3 ) &&
                 int.TryParse( lXValue4.Value, out lInt4 ) )
            {
                lValue.Value = new Vector4I( lInt1, lInt2, lInt3, lInt4 );
            }
            else
            {
                lValue.Value = new Vector4I( 0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform4 bool based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform4bValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            XAttribute lXValue4 = pDescriptor.Attribute( "w" );
            // Extract bools but fill as float as ther is no such structure in OpenTK.
            bool lBool1;
            bool lBool2;
            bool lBool3;
            bool lBool4;
            Vector4BValue lValue = new Vector4BValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 lXValue4 != null &&
                 bool.TryParse( lXValue1.Value, out lBool1 ) &&
                 bool.TryParse( lXValue2.Value, out lBool2 ) &&
                 bool.TryParse( lXValue3.Value, out lBool3 ) &&
                 bool.TryParse( lXValue4.Value, out lBool4 ) )
            {
                lValue.Value = new Tuple<bool, bool, bool, bool>( lBool1, lBool2, lBool3, lBool4 );
            }
            else
            {
                lValue.Value = new Tuple<bool, bool, bool, bool>( false, false, false, false );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniform4 int set based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void Uniform4viValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType = pDescriptor.Name();
            string lName = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXValue1 = pDescriptor.Attribute( "x" );
            XAttribute lXValue2 = pDescriptor.Attribute( "y" );
            XAttribute lXValue3 = pDescriptor.Attribute( "z" );
            XAttribute lXValue4 = pDescriptor.Attribute( "w" );
            // Extract uints but fill as float as ther is no such structure in OpenTK.
            uint lUInt1;
            uint lUInt2;
            uint lUInt3;
            uint lUInt4;
            Vector4UIValue lValue = new Vector4UIValue( lName );
            if ( lXValue1 != null &&
                 lXValue2 != null &&
                 lXValue3 != null &&
                 lXValue4 != null &&
                 uint.TryParse( lXValue1.Value, out lUInt1 ) &&
                 uint.TryParse( lXValue2.Value, out lUInt2 ) &&
                 uint.TryParse( lXValue3.Value, out lUInt3 ) &&
                 uint.TryParse( lXValue4.Value, out lUInt4 ) )
            {
                lValue.Value = new Vector4UI( lUInt1, lUInt2, lUInt3, lUInt4 );
            }
            else
            {
                lValue.Value = new Vector4UI( 0 );
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix2 (2x2) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix2fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix2FValue lValue = new Matrix2FValue( lName );
            if ( string.CompareOrdinal( lValues, "identity" ) == 0 )
            {
                lValue.Value = Matrix2F.IDENTITY.Data;
            }
            else if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = Matrix2F.ZERO.Data;
            }
            else
            {
                float[] lToFill = Matrix2F.ZERO.Data;
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 4 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix2f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix2f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix2 (2x2) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix2dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix2DValue lValue = new Matrix2DValue( lName );
            if ( string.CompareOrdinal( lValues, "identity" ) == 0 )
            {
                lValue.Value = Matrix2D.IDENTITY.Data;
            }
            else if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = Matrix2D.ZERO.Data;
            }
            else
            {
                double[] lToFill = Matrix2D.ZERO.Data;
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 4 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix2d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix2d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix2x3 (2x3) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix2x3fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix2x3FValue lValue = new Matrix2x3FValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new float[ 6 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
            }
            else
            {
                float[] lToFill = new float[ 6 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 6 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix2x3f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix2x3f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix2 (2x2) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix2x3dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix2x3DValue lValue = new Matrix2x3DValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new double[ 6 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            }
            else
            {
                double[] lToFill = new double[ 6 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 6 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix2x3d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix2x3d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix2x4 (2x4) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix2x4fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix2x4FValue lValue = new Matrix2x4FValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new float[ 8 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
            }
            else
            {
                float[] lToFill = new float[ 8 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 8 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix2x4f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix2x4f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix2x4 (2x4) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix2x4dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix2x4DValue lValue = new Matrix2x4DValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new double[ 8 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            }
            else
            {
                double[] lToFill = new double[ 8 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 8 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix2x4d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix2x4d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix3 (3x3) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix3fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix3FValue lValue = new Matrix3FValue( lName );
            if ( string.CompareOrdinal( lValues, "identity" ) == 0 )
            {
                lValue.Value = Matrix3F.IDENTITY.Data;
            }
            else if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = Matrix3F.ZERO.Data;
            }
            else
            {
                float[] lToFill = Matrix3F.ZERO.Data;
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 9 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix3f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix3f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix3 (3x3) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix3dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix3DValue lValue = new Matrix3DValue( lName );
            if ( string.CompareOrdinal( lValues, "identity" ) == 0 )
            {
                lValue.Value = Matrix3D.IDENTITY.Data;
            }
            else if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = Matrix3D.ZERO.Data;
            }
            else
            {
                double[] lToFill = Matrix3D.ZERO.Data;
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 9 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix3d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix3d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix3x2 (3x2) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix3x2fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix3x2FValue lValue = new Matrix3x2FValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new float[ 6 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
            }
            else
            {
                float[] lToFill = new float[ 6 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 6 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix3x2f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix3x2f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix3x2 (3x2) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix3x2dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix3x2DValue lValue = new Matrix3x2DValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new double[ 6 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            }
            else
            {
                double[] lToFill = new double[ 6 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 6 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix3x2d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix3x2d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix3x4 (3x4) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix3x4fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix3x4FValue lValue = new Matrix3x4FValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new float[ 12 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
            }
            else
            {
                float[] lToFill = new float[ 12 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 12 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix3x4f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix3x4f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix3x4 (3x4) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix3x4dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix3x4DValue lValue = new Matrix3x4DValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new double[ 12 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            }
            else
            {
                double[] lToFill = new double[ 12 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 12 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix3x4d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix3x4d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix4 (4x4) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix4fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix4FValue lValue = new Matrix4FValue( lName );
            if ( string.CompareOrdinal( lValues, "identity" ) == 0 )
            {
                lValue.Value = Matrix4F.IDENTITY.Data;
            }
            else if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = Matrix4F.ZERO.Data;
            }
            else
            {
                float[] lToFill = Matrix4F.ZERO.Data;
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 16 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix4f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix4f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix4 (4x4) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix4dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix4DValue lValue = new Matrix4DValue( lName );
            if ( string.CompareOrdinal( lValues, "identity" ) == 0 )
            {
                lValue.Value = Matrix4D.IDENTITY.Data;
            }
            else if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = Matrix4D.ZERO.Data;
            }
            else
            {
                double[] lToFill = Matrix4D.ZERO.Data;
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 16 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix4d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix4d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix4x2 (4x2) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix4x2fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix4x2FValue lValue = new Matrix4x2FValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new float[ 8 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
            }
            else
            {
                float[] lToFill = new float[ 8 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 8 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix4x2f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix4x2f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix4x2 (4x2) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix4x2dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix4x2DValue lValue = new Matrix4x2DValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new double[ 8 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            }
            else
            {
                double[] lToFill = new double[ 8 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 8 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix4x2d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix4x2d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix4x3 (4x3) float based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix4x3fValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix4x3FValue lValue = new Matrix4x3FValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new float[ 12 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
            }
            else
            {
                float[] lToFill = new float[ 12 ] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 12 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        float lFloat;
                        if ( float.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lFloat ) )
                        {
                            lToFill[ lCounter ] = lFloat;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0f;
                            LogManager.Instance.Log( "UniformMatrix4x3f decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix4x3f decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformMatrix4x3 (4x3) double based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformMatrix4x3dValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lType   = pDescriptor.Name();
            string lName   = pDescriptor.Attribute( "name" ).Value;
            string lValues = pDescriptor.Attribute( "value" ).Value;
            Matrix4x3DValue lValue = new Matrix4x3DValue( lName );
            if ( string.CompareOrdinal( lValues, "zero" ) == 0 )
            {
                lValue.Value = new double[ 12 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
            }
            else
            {
                double[] lToFill = new double[ 12 ] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                string[] lSplittedValues = lValues.Split( ',' );
                if ( lSplittedValues.Length == 12 )
                {
                    int lCounter = 0;
                    foreach ( string lSplittedValue in lSplittedValues )
                    {
                        double lDouble;
                        if ( double.TryParse( lSplittedValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lDouble ) )
                        {
                            lToFill[ lCounter ] = lDouble;
                        }
                        else
                        {
                            lToFill[ lCounter ] = 0.0;
                            LogManager.Instance.Log( "UniformMatrix4x3d decoder issue. Wrong value type so cannot cast it (zeroed)!!!", LogType.WARNING );
                        }

                        lCounter++;
                    }
                }
                else
                {
                    LogManager.Instance.Log( "UniformMatrix4x3d decoder issue. Not enough values to fill the matrix!!!", LogType.WARNING );
                }
                lValue.Value = lToFill;
            }
            pValue = lValue;
        }

        /// <summary>
        /// Builds an uniformTexture based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformTextureValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            if ( lResourceService != null )
            {
                string lName = pDescriptor.Attribute( "name" ).Value;
                IResource lResource = lResourceService.LoadResource( pDescriptor.Attribute( "texture" ).Value );
                if ( lResource != null )
                {
                    pValue = new TextureValue( lName, Render.UniformType.SAMPLER_2D, lResource.OwnedObject as ITexture );
                }
                else
                {
                    pValue = null;
                    LogManager.Instance.Log( string.Format( "Cannot found the {0} texture resource!!!", lName ), LogType.ERROR );
                }
            }
            else
            {
                pValue = null;
                LogManager.Instance.Log( "Resource service not found!!!", LogType.ERROR );
            }
        }

        /// <summary>
        /// Builds an uniformSubroutine based value given the XML element owning it.
        /// </summary>
        /// <param name="pDescriptor">The XML element</param>
        /// <param name="pValue">The resulting value.</param>
        private static void UniformSubroutineValueDecoder(XElement pDescriptor, out IPipelineValue pValue)
        {
            string lName  = pDescriptor.Attribute( "name" ).Value;
            XAttribute lXStage = pDescriptor.Attribute( "stage" );
            if ( lXStage != null )
            {
                string lStage = lXStage.Value;
                if ( string.CompareOrdinal( lStage, "VERTEX" ) == 0 )
                {
                    pValue = new SubroutineValue( lName, Render.StageType.VERTEX, pDescriptor.Attribute( "subroutine" ).Value );
                }
                else if ( string.CompareOrdinal( lStage, "TESSELATION_CONTROL" ) == 0 )
                {
                    pValue = new SubroutineValue( lName, Render.StageType.TESSELATION_CONTROL, pDescriptor.Attribute( "subroutine" ).Value );
                }
                else if ( string.CompareOrdinal( lStage, "TESSELATION_EVALUATION" ) == 0 )
                {
                    pValue = new SubroutineValue( lName, Render.StageType.TESSELATION_EVALUATION, pDescriptor.Attribute( "subroutine" ).Value );
                }
                else if ( string.CompareOrdinal( lStage, "GEOMETRY" ) == 0 )
                {
                    pValue = new SubroutineValue( lName, Render.StageType.GEOMETRY, pDescriptor.Attribute( "subroutine" ).Value );
                }
                else if ( string.CompareOrdinal( lStage, "FRAGMENT" ) == 0 )
                {
                    pValue = new SubroutineValue( lName, Render.StageType.FRAGMENT, pDescriptor.Attribute( "subroutine" ).Value );
                }
                else
                {
                    pValue = null;
                    LogManager.Instance.Log( string.Format( "Invalid shader stage {0}, so cannot create the value!!!", lStage ), LogType.ERROR );
                }
            }
            else
            {
                pValue = null;
                LogManager.Instance.Log( "No subroutine stage attribute defined, so cannot create the value!!!", LogType.ERROR );
            }
        }

        #endregion Methods Internal

        #region Methods IResourceDescriptor

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new PipelineDescriptionResource( pName, this );
        }

        #endregion Methods IResourceDescriptor

        #region Methods IDisposable

        /// <summary>
        /// Releases resource's resources.
        /// </summary>
        protected override void OnDispose()
        {
            this.InitialValues.Clear();
            this.Source   = null;
            this.Varyings = null;

            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
