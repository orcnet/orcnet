﻿using OrcNet.Core.Resource;
using OrcNet.Graphics.SceneGraph;
using OrcNet.Graphics.Task.Factories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Set transforms task factory descriptor class definition used to
    /// create SetTransformsTask factory based resource(s).
    /// </summary>
    public class SetTransformsTaskResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "setTransforms";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += this.Factory.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the set transforms task factory.
        /// </summary>
        public SetTransformsTaskFactory Factory
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTransformsTaskResourceDescriptor"/> class.
        /// </summary>
        private SetTransformsTaskResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetTransformsTaskResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public SetTransformsTaskResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            ScreenQualifier lScreen = null;
            XAttribute lXScreen = pDescriptor.Attribute( "screen" );
            if
                ( lXScreen != null )
            {
                lScreen = new ScreenQualifier( lXScreen.Value );
            }
            else
            {
                lScreen = new ScreenQualifier( "" );
            }

            PipelineQualifier lDescription = null;
            XAttribute lXDescription = pDescriptor.Attribute( "module" );
            if
                ( lXDescription != null )
            {
                lDescription = new PipelineQualifier( lXDescription.Value );
            }
            else
            {
                lDescription = new PipelineQualifier( "" );
            }

            string lTimeUniformName = null;
            XAttribute lXTime = pDescriptor.Attribute( "time" );
            if
                ( lXTime != null )
            {
                lTimeUniformName = lXTime.Value;
            }

            string lLocalToWorldUniformName = null;
            XAttribute lXLocalToWorld = pDescriptor.Attribute( "localToWorld" );
            if
                ( lXLocalToWorld != null )
            {
                lLocalToWorldUniformName = lXLocalToWorld.Value;
            }

            string lLocalToScreenUniformName = null;
            XAttribute lXLocalToScreen = pDescriptor.Attribute( "localToScreen" );
            if
                ( lXLocalToScreen != null )
            {
                lLocalToScreenUniformName = lXLocalToScreen.Value;
            }

            string lCameraToWorldUniformName = null;
            XAttribute lXCameraToWorld = pDescriptor.Attribute( "cameraToWorld" );
            if
                ( lXCameraToWorld != null )
            {
                lCameraToWorldUniformName = lXCameraToWorld.Value;
            }

            string lCameraToScreenUniformName = null;
            XAttribute lXCameraToScreen = pDescriptor.Attribute( "cameraToScreen" );
            if
                ( lXCameraToScreen != null )
            {
                lCameraToScreenUniformName = lXCameraToScreen.Value;
            }

            string lScreenToCameraUniformName = null;
            XAttribute lXScreenToCamera = pDescriptor.Attribute( "screenToCamera" );
            if
                ( lXScreenToCamera != null )
            {
                lScreenToCameraUniformName = lXScreenToCamera.Value;
            }

            string lWorldToScreenUniformName = null;
            XAttribute lXWorldToScreen = pDescriptor.Attribute( "worldToScreen" );
            if
                ( lXWorldToScreen != null )
            {
                lWorldToScreenUniformName = lXWorldToScreen.Value;
            }

            string lWorldPositionUniformName = null;
            XAttribute lXWorldPosition = pDescriptor.Attribute( "worldPos" );
            if
                ( lXWorldPosition != null )
            {
                lWorldPositionUniformName = lXWorldPosition.Value;
            }

            string lWorldDirectionUniformName = null;
            XAttribute lXWorldDirection = pDescriptor.Attribute( "worldDir" );
            if
                ( lXWorldDirection != null )
            {
                lWorldDirectionUniformName = lXWorldDirection.Value;
            }

            this.Factory = new SetTransformsTaskFactory( lScreen, lDescription, lTimeUniformName, lLocalToWorldUniformName, lLocalToScreenUniformName, lCameraToWorldUniformName, 
                                                                                lCameraToScreenUniformName, lScreenToCameraUniformName, lWorldToScreenUniformName, lWorldPositionUniformName, lWorldDirectionUniformName );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new SetTransformsTaskResource( pName, this );
        }
        
        #endregion Methods
    }
}
