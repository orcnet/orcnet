﻿using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Core.Service;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Text;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.Resource.Factories
{
    /// <summary>
    /// Font resource descriptor class definition used to
    /// create Font based resource(s).
    /// </summary>
    public class FontResourceDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "font";
            }
        }

        #endregion Properties IResourceFactory

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += this.TextFont.Size;
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets the text font.
        /// </summary>
        public Font TextFont
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FontResourceDescriptor"/> class.
        /// </summary>
        private FontResourceDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FontResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public FontResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            if ( pDescriptor.HasAttributes == false )
            {
                return;
            }

            string lTextureName = pDescriptor.Attribute( "tex" ).Value;
            Texture2D lTexture = null;
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            if ( lResourceService != null )
            {
                IResource lResource = lResourceService.LoadResource( lTextureName );
                if ( lResource != null )
                {
                    lTexture = lResource.OwnedObject as Texture2D;
                }
            }
            
            int lColumnCount = int.Parse( pDescriptor.Attribute( "nCols" ).Value );
            int lRowCount = int.Parse( pDescriptor.Attribute( "nRows" ).Value );
            int lMinValidChar = int.Parse( pDescriptor.Attribute( "minChar" ).Value );
            int lMaxValidChar = int.Parse( pDescriptor.Attribute( "maxChar" ).Value );
            int lInvalidChar = int.Parse( pDescriptor.Attribute( "invalidChar" ).Value );

            int lWidthCount = 1 + lMaxValidChar - lMinValidChar;
            int[] lCharWidths = new int[ lWidthCount ];
            
            // Parse characters widths.
            string lCharWidthsStr = pDescriptor.Attribute( "charWidths" ).Value;
            string[] lSplittedWidths = lCharWidthsStr.Split( ',' );
            bool lHasFixedWidth = lSplittedWidths.Length == 1;
            for ( int lCurr = 0; lCurr < lSplittedWidths.Length; lCurr++ )
            {
                int lCurrWidth = int.Parse( lSplittedWidths[ lCurr ] );
                lCharWidths[ lCurr ] = lCurrWidth;
            }

            for ( int lCurr = lSplittedWidths.Length; lCurr < lWidthCount; lCurr++ )
            {
                lCharWidths[ lCurr ] = lCharWidths[ lCurr - 1 ];
            }

            if ( lTexture != null )
            {
                this.TextFont = new Font( lTexture, lColumnCount, lRowCount, lMinValidChar, lMaxValidChar, lInvalidChar, lHasFixedWidth, lCharWidths );
            }
            else
            {
                LogManager.Instance.Log( "Font resource descriptor issue... Missing texture!!!", LogType.ERROR );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new FontResource( pName, this );
        }

        #endregion Methods
    }
}
