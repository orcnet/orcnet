﻿using OrcNet.Core.Resource;
using OrcNet.Graphics.Resource.Factories;
using OrcNet.Graphics.Task.Factories;

namespace OrcNet.Graphics.Resource
{
    /// <summary>
    /// Show logs task factory resource class definition.
    /// </summary>
    public class ShowLogTaskResource : AResource<ShowLogTaskFactory>
    {
        #region Properties

        /// <summary>
        /// Gets the update order of that resource.
        /// In order to be up to date properly, such a resource must wait for
        /// parent resource(s) to be up to date first.
        /// (e.g: A GLSL program must wait for its shaders to be updated which
        /// themselves need texture(s) to be ready and so on.
        /// </summary>
        public override int UpdateOrder
        {
            get
            {
                return 40;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowLogTaskResource"/> class.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pDescriptor">The resource description</param>
        public ShowLogTaskResource(string pName, ShowLogTaskResourceDescriptor pDescriptor) :
        base( pName, pDescriptor )
        {
            // Create the owned object this resource manages using
            // the description.
            this.OwnedObject = new ShowLogTaskFactory( pDescriptor.TextFont, pDescriptor.TextPass, pDescriptor.FontHeight, pDescriptor.ScreenX, pDescriptor.ScreenY, pDescriptor.MaxLineCount );
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Allow to revert any changes if something goes wrong.
        /// </summary>
        /// <param name="pOldValue">The old value to set back in that resource</param>
        protected override void Swap(AResource<ShowLogTaskFactory> pOldValue)
        {
            this.OwnedObject.Swap( pOldValue.OwnedObject );
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
