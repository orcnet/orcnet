﻿using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Resource.Factories;
using System;

namespace OrcNet.Graphics.Resource
{
    /// <summary>
    /// Pipeline pass resource class definition.
    /// </summary>
    public class PipelinePassResource : AResource<PipelinePass>
    {
        #region Properties
        
        /// <summary>
        /// Gets the update order of that resource.
        /// In order to be up to date properly, such a resource must wait for
        /// parent resource(s) to be up to date first.
        /// (e.g: A GLSL program must wait for its shaders to be updated which
        /// themselves need texture(s) to be ready and so on.
        /// </summary>
        public override int UpdateOrder
        {
            get
            {
                return 30;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelinePassResource"/> class.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pDescriptor">The resource description</param>
        public PipelinePassResource(string pName, PipelinePassResourceDescriptor pDescriptor) :
        base( pName, pDescriptor )
        {
            // Create the owned object this resource manages using
            // the description.
            this.OwnedObject = pDescriptor.Pass;
        }

        #endregion Constructor

        #region Methods

        #region Methods IResource

        /// <summary>
        /// Prepares the resource update by checking whether the update can be successful or not. 
        /// NOTE: Update is done in two phases =>
        /// first testing if the resource hierarchy can be updated successfully or not,
        /// and second updating all resources. Otherwise nothing will be updated at all.
        /// </summary>
        /// <param name="pLoader">The loader to use to prepare the resource.</param>
        /// <returns>True if the resource passed the first phase and can be updated for sure.</returns>
        public override bool PreUpdate(IResourceLoader pLoader)
        {
            // Reload the resource description.
            this.mEditedDescriptor = pLoader.ReloadResource( this.Name, this.mDescriptor );
            bool lHasChanged = this.mEditedDescriptor != null;
            if
                ( lHasChanged == false )
            {
                for
                    ( int lCurrDesc = 0; lCurrDesc < this.OwnedObject.DescriptionCount; lCurrDesc++ )
                {
                    PipelineDescription lDescription = this.OwnedObject[ new Core.Render.PipelineDescriptionIndex( lCurrDesc ) ];
                    if
                        ( lDescription.Creator != null &&
                          lDescription.Creator.IsModified )
                    {
                        lHasChanged = true;
                        break;
                    }
                }
            }

            // If the descriptor has changed.
            if
                ( lHasChanged )
            {
                this.mOldValue = null;
                try
                {
                    this.mOldValue = new PipelinePassResource( this.Name, lHasChanged ? this.mEditedDescriptor as PipelinePassResourceDescriptor : this.mDescriptor as PipelinePassResourceDescriptor );
                }
                catch
                    ( Exception pEx )
                {
                    LogManager.Instance.Log( pEx );
                }

                // If the creation is a success?
                if
                    ( this.mOldValue != null )
                {
                    // Swaps the current value with the new one.
                    this.Swap( this.mOldValue );
                    return true;
                }

                // If creation fails, do nothing..
                return false;
            }

            return true;
        }

        #endregion Methods IResource
        
        #region Methods Internal

        /// <summary>
        /// Allow to revert any changes if something goes wrong.
        /// </summary>
        /// <param name="pOldValue">The old value to set back in that resource</param>
        protected override void Swap(AResource<PipelinePass> pOldValue)
        {
            this.OwnedObject.Swap( pOldValue.OwnedObject );
        }

        #endregion Methods Internal
        
        #endregion Methods
    }
}
