﻿using OrcNet.Core.Resource;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Resource.Factories;

namespace OrcNet.Graphics.Resource
{
    /// <summary>
    /// Pipeline description resource class definition.
    /// </summary>
    public class PipelineDescriptionResource : AResource<PipelineDescription>
    {
        #region Properties
        
        /// <summary>
        /// Gets the update order of that resource.
        /// In order to be up to date properly, such a resource must wait for
        /// parent resource(s) to be up to date first.
        /// (e.g: A GLSL program must wait for its shaders to be updated which
        /// themselves need texture(s) to be ready and so on.
        /// </summary>
        public override int UpdateOrder
        {
            get
            {
                return 20;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineDescriptionResource"/> class.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pDescriptor">The resource description</param>
        public PipelineDescriptionResource(string pName, PipelineDescriptionDescriptor pDescriptor) :
        base( pName, pDescriptor )
        {
            // Create the owned object this resource manages using
            // the description.
            this.OwnedObject = new PipelineDescription( pDescriptor.Version, pDescriptor.Source );
            this.OwnedObject.FeedbackMode = pDescriptor.FeedbackMode;

            foreach ( IPipelineValue lInitialValue in pDescriptor.InitialValues )
            {
                this.OwnedObject.AddInitialValue( lInitialValue );
            }

            if ( pDescriptor.Varyings != null )
            {
                foreach ( string lVarying in pDescriptor.Varyings )
                {
                    this.OwnedObject.AddFeedbackVarying( lVarying );
                }
            }
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Allow to revert any changes if something goes wrong.
        /// </summary>
        /// <param name="pOldValue">The old value to set back in that resource</param>
        protected override void Swap(AResource<PipelineDescription> pOldValue)
        {
            this.OwnedObject.Swap( pOldValue.OwnedObject );
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
