﻿using OrcNet.Core.Resource;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Resource.Factories;

namespace OrcNet.Graphics.Resource
{
    /// <summary>
    /// Texture3D resource class definition.
    /// </summary>
    public class Texture3DResource : AResource<Texture3D>
    {
        #region Properties
        
        /// <summary>
        /// Gets the update order of that resource.
        /// In order to be up to date properly, such a resource must wait for
        /// parent resource(s) to be up to date first.
        /// (e.g: A GLSL program must wait for its shaders to be updated which
        /// themselves need texture(s) to be ready and so on.
        /// </summary>
        public override int UpdateOrder
        {
            get
            {
                return 0;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Texture3DResource"/> class.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pDescriptor">The resource description</param>
        public Texture3DResource(string pName, Texture3DDescriptor pDescriptor) :
        base( pName, pDescriptor )
        {
            // Create the owned object this resource manages using
            // the description.
            this.OwnedObject = new Texture3D( pDescriptor.Width, 
                                              pDescriptor.Height / pDescriptor.Depth,
                                              pDescriptor.Depth,
                                              pDescriptor.InternalFormat, 
                                              pDescriptor.Format, 
                                              pDescriptor.Type, 
                                              pDescriptor.Parameters, 
                                              pDescriptor.BufferParameters, 
                                              new CPUBuffer( pDescriptor.Content ) );
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Allow to revert any changes if something goes wrong.
        /// </summary>
        /// <param name="pOldValue">The old value to set back in that resource</param>
        protected override void Swap(AResource<Texture3D> pOldValue)
        {
            this.OwnedObject.Swap( pOldValue.OwnedObject );
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
