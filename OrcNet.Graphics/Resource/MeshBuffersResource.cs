﻿using OrcNet.Core.Resource;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Resource.Factories;

namespace OrcNet.Graphics.Resource
{
    /// <summary>
    /// Mesh buffers resource class definition.
    /// </summary>
    public class MeshBuffersResource : AResource<MeshBuffers>
    {
        #region Properties

        /// <summary>
        /// Gets the update order of that resource.
        /// In order to be up to date properly, such a resource must wait for
        /// parent resource(s) to be up to date first.
        /// (e.g: A GLSL program must wait for its shaders to be updated which
        /// themselves need texture(s) to be ready and so on.
        /// </summary>
        public override int UpdateOrder
        {
            get
            {
                return 0;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MeshBuffersResource"/> class.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pDescriptor">The resource description</param>
        public MeshBuffersResource(string pName, MeshBuffersResourceDescriptor pDescriptor) :
        base( pName, pDescriptor )
        {
            // Create the owned object this resource manages using
            // the description.
            this.OwnedObject = new MeshBuffers( pDescriptor.Mode, pDescriptor.VertexCount, pDescriptor.IndexCount );
            this.OwnedObject.Bounds = pDescriptor.Bounds;

            int lAttributeCount = pDescriptor.Attributes.Count;
            for ( int lCurrAttribute = 0; lCurrAttribute < lAttributeCount; lCurrAttribute++ )
            {
                PipelineAttributeDescription lNewAttribute = pDescriptor.Attributes[ lCurrAttribute ];
                this.OwnedObject.AddAttributeBuffer( lNewAttribute );
            }

            this.OwnedObject.IndicesBuffer = pDescriptor.IndicesBuffer;
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Allow to revert any changes if something goes wrong.
        /// </summary>
        /// <param name="pOldValue">The old value to set back in that resource</param>
        protected override void Swap(AResource<MeshBuffers> pOldValue)
        {
            this.OwnedObject.Swap( pOldValue.OwnedObject );
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
