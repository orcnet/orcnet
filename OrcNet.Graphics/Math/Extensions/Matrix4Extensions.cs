﻿using OpenTK;

namespace OrcNet.Graphics.Math.Extensions
{
    /// <summary>
    /// Matrix4 extensions class definition.
    /// </summary>
    public static class Matrix4Extensions
    {
        #region Methods

        /// <summary>
        /// Multiplies a Matrix4 to a Vector3.
        /// </summary>
        /// <param name="pTransform">The transform to multiply with the given vector.</param>
        /// <param name="pVector">The vector3 to transform.</param>
        /// <returns>The transformed vector3 by the given Matrix4 as transform.</returns>
        public static Vector3 Multiply(this Matrix4 pTransform, Vector3 pVector)
        {
            Vector3 lResult = new Vector3();
            float lInvW = 1.0f / (pTransform[3, 0] * pVector.X + pTransform[3, 1] * pVector.Y + pTransform[3, 2] * pVector.Z + pTransform[3, 3]);

            lResult.X = (pTransform[0, 0] * pVector.X + pTransform[0, 1] * pVector.Y + pTransform[0, 2] * pVector.Z + pTransform[0, 3]) * lInvW;
            lResult.Y = (pTransform[1, 0] * pVector.X + pTransform[1, 1] * pVector.Y + pTransform[1, 2] * pVector.Z + pTransform[1, 3]) * lInvW;
            lResult.Z = (pTransform[2, 0] * pVector.X + pTransform[2, 1] * pVector.Y + pTransform[2, 2] * pVector.Z + pTransform[2, 3]) * lInvW;

            return lResult;
        }

        /// <summary>
        /// Multiplies a Matrix4 to a Vector4.
        /// </summary>
        /// <param name="pTransform">The transform to multiply with the given vector.</param>
        /// <param name="pVector">The vector4 to transform.</param>
        /// <returns>The transformed vector4 by the given Matrix4d as transform.</returns>
        public static Vector4 Multiply(this Matrix4 pTransform, Vector4 pVector)
        {
            return new Vector4( pTransform[0, 0] * pVector.X + pTransform[0, 1] * pVector.Y + pTransform[0, 2] * pVector.Z + pTransform[0, 3] * pVector.W,
                                pTransform[1, 0] * pVector.X + pTransform[1, 1] * pVector.Y + pTransform[1, 2] * pVector.Z + pTransform[1, 3] * pVector.W,
                                pTransform[2, 0] * pVector.X + pTransform[2, 1] * pVector.Y + pTransform[2, 2] * pVector.Z + pTransform[2, 3] * pVector.W,
                                pTransform[3, 0] * pVector.X + pTransform[3, 1] * pVector.Y + pTransform[3, 2] * pVector.Z + pTransform[3, 3] * pVector.W );
        }
        
        #endregion Methods
    }
}
