﻿using OpenTK;

namespace OrcNet.Graphics.Math.Extensions
{
    /// <summary>
    /// Vector4d extensions class definition.
    /// </summary>
    public static class Vector4dExtensions
    {
        #region Methods

        /// <summary>
        /// Computes the Dot product between a Vector4d and a Vector3d.
        /// </summary>
        /// <param name="pThis">The vector4d</param>
        /// <param name="pOther">The vector3d</param>
        /// <returns>The dot result.</returns>
        public static double Dot(this Vector4d pThis, Vector3d pOther)
        {
            return (pThis.X * pOther.X + pThis.Y * pOther.Y + pThis.Z * pOther.Z + pThis.W);
        }

        /// <summary>
        /// Transforms a Vector4 double based into a Vector4 float based.
        /// </summary>
        /// <param name="pThis">The Vector4 double based.</param>
        /// <returns>The Vector4 float based.</returns>
        public static Vector4 ToFloat(this Vector4d pThis)
        {
            return new Vector4( (float)pThis.X, (float)pThis.Y, (float)pThis.Z, (float)pThis.W );
        }

        #endregion Methods
    }
}
