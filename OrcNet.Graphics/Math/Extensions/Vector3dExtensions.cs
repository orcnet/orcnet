﻿using OpenTK;

namespace OrcNet.Graphics.Math.Extensions
{
    /// <summary>
    /// Vector3d extensions class definition.
    /// </summary>
    public static class Vector3dExtensions
    {
        #region Methods

        /// <summary>
        /// Transforms a Vector3 double based into a Vector3 float based.
        /// </summary>
        /// <param name="pThis">The Vector3 double based.</param>
        /// <returns>The Vector3 float based.</returns>
        public static Vector3 ToFloat(this Vector3d pThis)
        {
            return new Vector3( (float)pThis.X, (float)pThis.Y, (float)pThis.Z );
        }

        #endregion Methods
    }
}
