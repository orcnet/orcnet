﻿using OpenTK;

namespace OrcNet.Graphics.Math.Extensions
{
    /// <summary>
    /// Matrix4d extensions class definition.
    /// </summary>
    public static class Matrix4dExtensions
    {
        #region Methods
        
        /// <summary>
        /// Multiplies a Matrix4d to a Vector3d.
        /// </summary>
        /// <param name="pTransform">The transform to multiply with the given vector.</param>
        /// <param name="pVector">The vector3d to transform.</param>
        /// <returns>The transformed vector3d by the given Matrix4d as transform.</returns>
        public static Vector3d Multiply(this Matrix4d pTransform, Vector3d pVector)
        {
            Vector3d lResult = new Vector3d();
            double lInvW = 1.0 / (pTransform[3, 0] * pVector.X + pTransform[3, 1] * pVector.Y + pTransform[3, 2] * pVector.Z + pTransform[3, 3]);

            lResult.X = (pTransform[0, 0] * pVector.X + pTransform[0, 1] * pVector.Y + pTransform[0, 2] * pVector.Z + pTransform[0, 3]) * lInvW;
            lResult.Y = (pTransform[1, 0] * pVector.X + pTransform[1, 1] * pVector.Y + pTransform[1, 2] * pVector.Z + pTransform[1, 3]) * lInvW;
            lResult.Z = (pTransform[2, 0] * pVector.X + pTransform[2, 1] * pVector.Y + pTransform[2, 2] * pVector.Z + pTransform[2, 3]) * lInvW;

            return lResult;
        }

        /// <summary>
        /// Multiplies a Matrix4d to a Vector4d.
        /// </summary>
        /// <param name="pTransform">The transform to multiply with the given vector.</param>
        /// <param name="pVector">The vector4d to transform.</param>
        /// <returns>The transformed vector4d by the given Matrix4d as transform.</returns>
        public static Vector4d Multiply(this Matrix4d pTransform, Vector4d pVector)
        {
            return new Vector4d( pTransform[0, 0] * pVector.X + pTransform[0, 1] * pVector.Y + pTransform[0, 2] * pVector.Z + pTransform[0, 3] * pVector.W,
                                 pTransform[1, 0] * pVector.X + pTransform[1, 1] * pVector.Y + pTransform[1, 2] * pVector.Z + pTransform[1, 3] * pVector.W,
                                 pTransform[2, 0] * pVector.X + pTransform[2, 1] * pVector.Y + pTransform[2, 2] * pVector.Z + pTransform[2, 3] * pVector.W,
                                 pTransform[3, 0] * pVector.X + pTransform[3, 1] * pVector.Y + pTransform[3, 2] * pVector.Z + pTransform[3, 3] * pVector.W );
        }

        /// <summary>
        /// Transforms a 4x4 double based matrix into a 4x4 float based matrix.
        /// </summary>
        /// <param name="pTransform">The 4x4 double based matrix.</param>
        /// <returns>The 4x4 float based matrix.</returns>
        public static Matrix4  ToFloat(this Matrix4d pTransform)
        {
            return new Matrix4( (float)pTransform.M11, (float)pTransform.M12, (float)pTransform.M13, (float)pTransform.M14,
                                (float)pTransform.M21, (float)pTransform.M22, (float)pTransform.M23, (float)pTransform.M24,
                                (float)pTransform.M31, (float)pTransform.M32, (float)pTransform.M33, (float)pTransform.M34,
                                (float)pTransform.M41, (float)pTransform.M42, (float)pTransform.M43, (float)pTransform.M44 );
        }

        #endregion Methods
    }
}
