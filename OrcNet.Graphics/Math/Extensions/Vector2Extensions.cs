﻿using OpenTK;

namespace OrcNet.Graphics.Math.Extensions
{
    /// <summary>
    /// Vector2 extension methods class definition.
    /// </summary>
    public static class Vector2Extensions
    {
        #region Methods

        /// <summary>
        /// Compute a 2D space cross product between two vector2.
        /// The result is still |a||b|sin(Theta), and the sign of the result will still indicate the rotation dir of theta.
        /// </summary>
        /// <param name="pThis">The left vector</param>
        /// <param name="pOther">The right vector.</param>
        /// <returns>The cross product of the two 2D vectors.</returns>
        public static double Cross(this Vector2 pThis, Vector2 pOther)
        {
            return pThis.X * pOther.Y - pThis.Y * pOther.X;
        }

        #endregion Methods
    }
}
