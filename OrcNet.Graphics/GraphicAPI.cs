﻿namespace OrcNet.Graphics
{
    /// <summary>
    /// Enumerate the differente Graphical API that 
    /// will be supported.
    /// </summary>
    public enum GraphicAPI
    {
        /// <summary>
        /// Use OpenGL are rendering device.
        /// NOTE: Vulkan will only be a version of OPENGL even if different.
        /// </summary>
        OPENGL,

        /// <summary>
        /// Use DirectX as rendering device.
        /// NOTE: DirectX12 being the Vulkan competitor, it is just another version as well.
        /// </summary>
        DIRECTX,

        /// <summary>
        /// Use Mantle as rendering device.
        /// NOTE: A bit of exoticism with that one.
        /// </summary>
        MANTLE,

        /// <summary>
        /// The amount of supported API(s)
        /// </summary>
        COUNT
    }
}
