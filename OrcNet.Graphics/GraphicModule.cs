﻿using OrcNet.Core;

namespace OrcNet.Graphics
{
    /// <summary>
    /// Definition of the <see cref="GraphicModule"/> class.
    /// </summary>
    internal sealed class GraphicModule : AModule
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphicModule"/> class.
        /// </summary>
        public GraphicModule()
        {

        }

        #endregion Constructor
    }
}
