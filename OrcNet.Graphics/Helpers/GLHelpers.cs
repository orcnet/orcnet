﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Graphics.Render;
using System.Text;

namespace OrcNet.Graphics.Helpers
{
    /// <summary>
    /// OpenGL helper methods definition.
    /// </summary>
    public static class GLHelpers
    {
        #region Fields

        /// <summary>
        /// Stores the maximum uniform buffer units allowed.
        /// </summary>
        private static uint sMaxUniformBufferUnits = 0;

        /// <summary>
        /// Stores the maximum texture units allowed.
        /// </summary>
        private static uint sMaxTextureUnits = 0;

        /// <summary>
        /// Stores the maximum amount of layer for textures array.
        /// </summary>
        private static uint sMaxTextureArrayLayerCount = 0;

        #endregion Fields

        #region Constants

        /// <summary>
        /// Stores the maximum texture units allowed by the engine.
        /// </summary>
        public const int cMAX_TEXTURE_UNITS = 64;

        #endregion COnstants

        #region Methods

        /// <summary>
        /// Gets the OpenGL major version.
        /// </summary>
        /// <returns></returns>
        public static int MajorVersion()
        {
            return GL.GetInteger( GetPName.MajorVersion );
        }

        /// <summary>
        /// Gets the OpenGL minor version.
        /// </summary>
        /// <returns></returns>
        public static int MinorVersion()
        {
            return GL.GetInteger( GetPName.MinorVersion );
        }

        /// <summary>
        /// Checks whether the compilated stage is succeeded or not.
        /// </summary>
        /// <param name="pStageId">The stage identifier</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public static bool IsStageCompilationSucceeded(int pStageId)
        {
            int lCompilationSucceeded;
            GL.GetShader( pStageId, ShaderParameter.CompileStatus, out lCompilationSucceeded );
            return lCompilationSucceeded != 0;
        }

        /// <summary>
        /// Logs stage compilation info.
        /// </summary>
        /// <param name="pStageId">The stage identifier</param>
        /// <param name="pHasError">The flag indicating whether the compilation log will be written as Error or not.</param>
        public static void LogStageCompilationFeedbacks(int pStageId, bool pHasError)
        {
            LogType lLogType = LogType.WARNING;
            if ( pHasError )
            {
                lLogType = LogType.ERROR;
            }

            string lInfo;
            GL.GetShaderInfoLog( pStageId, out lInfo );

            if ( string.IsNullOrEmpty( lInfo ) )
            {
                return;
            }

            LogManager.Instance.Log( lInfo, lLogType );
        }

        /// <summary>
        /// Retrieves the maximum allowed amount of uniform buffer units.
        /// </summary>
        /// <returns>The maximum uniform buffer units allowed.</returns>
        public static uint GetUniformMaxBufferUnits()
        {
            if ( sMaxUniformBufferUnits == 0 )
            {
                int lMaxUniformBufferBindings;
                int lMaxVertexUniformBlocks;
                int lMaxGeometryUniformBlocks;
                int lMaxFragmentUniformBlocks;
                int lMaxCombinedUniformBlocks;
                GL.GetInteger( GetPName.MaxUniformBufferBindings, out lMaxUniformBufferBindings );
                GL.GetInteger( GetPName.MaxVertexUniformBlocks,   out lMaxVertexUniformBlocks );
                GL.GetInteger( GetPName.MaxGeometryUniformBlocks, out lMaxGeometryUniformBlocks );
                GL.GetInteger( GetPName.MaxFragmentUniformBlocks, out lMaxFragmentUniformBlocks );
                GL.GetInteger( GetPName.MaxCombinedUniformBlocks, out lMaxCombinedUniformBlocks );
                sMaxUniformBufferUnits = (uint)System.Math.Min( lMaxUniformBufferBindings, 64 );
                sMaxUniformBufferUnits = System.Math.Min( sMaxUniformBufferUnits, (uint)lMaxVertexUniformBlocks );
                sMaxUniformBufferUnits = System.Math.Min( sMaxUniformBufferUnits, (uint)lMaxGeometryUniformBlocks );
                sMaxUniformBufferUnits = System.Math.Min( sMaxUniformBufferUnits, (uint)lMaxFragmentUniformBlocks );
                sMaxUniformBufferUnits = System.Math.Min( sMaxUniformBufferUnits, (uint)lMaxCombinedUniformBlocks );

                LogManager.Instance.Log( string.Format( "The max uniform buffer bindings is {0}", lMaxUniformBufferBindings ) );
            }

            return sMaxUniformBufferUnits;
        }

        /// <summary>
        /// Retrieves the maximum supported texture units.
        /// </summary>
        /// <returns>the maximum supported texture units.</returns>
        public static uint GetMaxTextureUnits()
        {
            if ( sMaxTextureUnits == 0 )
            {
                int lMaxVertexTextureImageUnits   = GL.GetInteger( GetPName.MaxVertexTextureImageUnits );
                int lMaxFragmentTextureImageUnits = GL.GetInteger( GetPName.MaxTextureImageUnits );
                int lMaxCombinedTextureImageUnits = GL.GetInteger( GetPName.MaxCombinedTextureImageUnits );

                sMaxTextureUnits = (uint)System.Math.Min( lMaxCombinedTextureImageUnits, cMAX_TEXTURE_UNITS );

                StringBuilder lLog = new StringBuilder();
                lLog.AppendLine( "        -Maximum vertex texture image units: {0}." );
                lLog.AppendLine( "        -Maximum fragment texture image units: {1}." );
                lLog.Append(     "        -Maximum combined texture image units: {2}." );
                LogManager.Instance.Log( string.Format( lLog.ToString(), lMaxVertexTextureImageUnits, lMaxFragmentTextureImageUnits, lMaxCombinedTextureImageUnits ) );
            }

            return sMaxTextureUnits;
        }
        
        /// <summary>
        /// Gets the maximum amount of layers for array textures.
        /// </summary>
        public static uint GetMaxTextureArrayLayerCount()
        {
            if ( sMaxTextureArrayLayerCount == 0 )
            {
                sMaxTextureArrayLayerCount = (uint)GL.GetInteger( GetPName.MaxArrayTextureLayers );
            }

            return sMaxTextureArrayLayerCount;
        }
        
        /// <summary>
        /// Enables or disables a GL capacity.
        /// </summary>
        /// <param name="pCapacity">The capacity</param>
        /// <param name="pEnable">The flag indicating whether the capacity must be enabled or disabled.</param>
        public static void Enable(EnableCap pCapacity, bool pEnable)
        {
            if ( pEnable )
            {
                GL.Enable( pCapacity );
            }
            else
            {
                GL.Disable( pCapacity );
            }
        }

        /// <summary>
        /// Retrieves the corresponding buffer index given a Buffer Id.
        /// </summary>
        /// <param name="pBuffer">The buffer Id (e.g: COLOR0, COLOR1, STENCIL,...)</param>
        /// <returns>The buffers array index</returns>
        public static int GetBufferIndex(BufferId pBuffer)
        {
            switch ( pBuffer )
            {
                case BufferId.COLOR0:
                    return 0;
                case BufferId.COLOR1:
                    return 1;
                case BufferId.COLOR2:
                    return 2;
                case BufferId.COLOR3:
                    return 3;
                case BufferId.COLOR4:
                    return 4;
                case BufferId.COLOR5:
                    return 5;
                case BufferId.COLOR6:
                    return 6;
                case BufferId.COLOR7:
                    return 7;
                case BufferId.STENCIL:
                    return 8;
                case BufferId.DEPTH:
                    return 9;

            }

            return -1;
        }

        /// <summary>
        /// Gets the current GL error if any.
        /// </summary>
        /// <returns>The error identifier.</returns>
        public static ErrorCode GetError()
        {
            ErrorCode lCode = GL.GetError();
            if ( lCode != ErrorCode.NoError )
            {
                // Error, so log it.
                LogManager.Instance.Log( string.Format( "GL \"{0}\" issue!!!", lCode.ToString() ), LogType.ERROR );
            }

            return lCode;
        }

        /// <summary>
        /// Gets the format size.
        /// </summary>
        /// <param name="pFormat">The format determining the components count.</param>
        /// <param name="pType">The components type.</param>
        /// <returns>The texture format size.</returns>
        public static uint GetFormatSize(PixelFormat pFormat, PixelType pType)
        {
            uint lComponentCount = GetTextureComponents( pFormat );

            switch ( pType )
            {
                case PixelType.UnsignedByte:
                case PixelType.Byte:
                    return lComponentCount;
                case PixelType.UnsignedShort:
                case PixelType.Short:
                case PixelType.HalfFloat:
                    return 2 * lComponentCount;
                case PixelType.UnsignedInt:
                case PixelType.Int:
                case PixelType.Float:
                    return 4 * lComponentCount;
                case PixelType.UnsignedByte332:
                case PixelType.UnsignedByte233Reversed:
                    return 1;
                case PixelType.UnsignedShort565:
                case PixelType.UnsignedShort565Reversed:
                case PixelType.UnsignedShort4444:
                case PixelType.UnsignedShort4444Reversed:
                case PixelType.UnsignedShort5551:
                case PixelType.UnsignedShort1555Reversed:
                    return 2;
                case PixelType.UnsignedInt8888:
                case PixelType.UnsignedInt8888Reversed:
                case PixelType.UnsignedInt1010102:
                case PixelType.UnsignedInt2101010Reversed:
                case PixelType.UnsignedInt248:
                case PixelType.UnsignedInt10F11F11FRev:
                case PixelType.UnsignedInt5999Rev:
                    return 4;
                case PixelType.Float32UnsignedInt248Rev:
                    return 8;
            }

            return 0;
        }

        /// <summary>
        /// Gets the texture components count.
        /// </summary>
        /// <param name="pFormat">The format determining the components count.</param>
        /// <returns>The texture components count.</returns>
        public static uint GetTextureComponents(PixelFormat pFormat)
        {
            switch ( pFormat )
            {
                case PixelFormat.StencilIndex:
                case PixelFormat.DepthComponent:
                case PixelFormat.Red:
                case PixelFormat.Green:
                case PixelFormat.Blue:
                case PixelFormat.RedInteger:
                case PixelFormat.BlueInteger:
                case PixelFormat.GreenInteger:
                    return 1;
                case PixelFormat.DepthStencil:
                case PixelFormat.Rg:
                case PixelFormat.RgInteger:
                    return 2;
                case PixelFormat.Rgb:
                case PixelFormat.Bgr:
                case PixelFormat.RgbInteger:
                case PixelFormat.BgrInteger:
                    return 3;
                case PixelFormat.Rgba:
                case PixelFormat.Bgra:
                case PixelFormat.RgbaInteger:
                case PixelFormat.BgraInteger:
                    return 4;
            }

            return 0;
        }
        
        #endregion Methods
    }
}
