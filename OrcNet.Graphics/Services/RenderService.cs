﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using OrcNet.Graphics.Helpers;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using System;
using System.Collections.Generic;

namespace OrcNet.Graphics.Services
{
    /// <summary>
    /// Render service class definition.
    /// </summary>
    public class RenderService : AService, IRenderService
    {
        #region Fields
        
        /// <summary>
        /// Stores the current value of the primitive restart index.
        /// </summary>
        private int mCurrentRestartIndex;

        /// <summary>
        /// Stores the current value of the patch vertices parameter.
        /// </summary>
        private int mCurrentPatchVertices;

        /// <summary>
        /// Stores the default transform feedback.
        /// </summary>
        private TransformFeedback mDefaultTransformFeedBack;

        /// <summary>
        /// Stores the current feedback session frame buffer.
        /// </summary>
        private FrameBuffer mCurrentFeedbackFrameBuffer;

        /// <summary>
        /// Stores the current feedback session transform pass to use.
        /// </summary>
        private PipelinePass mCurrentFeedbackTransform;

        /// <summary>
        /// Stores the current feedback session mode determining how vertices must be interpreted.
        /// </summary>
        private PrimitiveType mCurrentFeedbackMode;

        /// <summary>
        /// Stores the currently used pass.
        /// </summary>
        private PipelinePass mCurrentPass;

        /// <summary>
        /// Stores the currently bound mesh buffers.
        /// </summary>
        private MeshBuffers  mCurrentMeshBuffers;

        /// <summary>
        /// Stores the offset of the indices of the currently bound mesh in its indices buffer.
        /// </summary>
        private IntPtr mCurrentMeshIndicesOffset;

        /// <summary>
        /// Stores the currently bound mesh indices type.
        /// </summary>
        private AttributeType mCurrentIndicesType;

        /// <summary>
        /// Stores the current framebuffer.
        /// </summary>
        private FrameBuffer  mCurrentFrameBuffer;

        /// <summary>
        /// Stores the default, onscreen frame buffer map (one per OpenGL context).
        /// </summary>
        private FrameBufferMap mDefaultFrameBufferMap;

        /// <summary>
        /// Stores the current frame buffer parameters.
        /// </summary>
        private FrameBufferParameters mParameters;

        #endregion Fields

        #region Properties

        #region Properties IService

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "Rendering";
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties IService

        /// <summary>
        /// Gets the current transform feedback frame buffer.
        /// </summary>
        public FrameBuffer CurrentTransformFeedbackFrameBuffer
        {
            get
            {
                return this.mCurrentFeedbackFrameBuffer;
            }
        }

        /// <summary>
        /// Gets the current transform feedback pipeline pass.
        /// </summary>
        public PipelinePass CurrentTransformFeedbackPass
        {
            get
            {
                return this.mCurrentFeedbackTransform;
            }
        }

        /// <summary>
        /// Gets the default transform feedback.
        /// </summary>
        public TransformFeedback DefaultTransformFeedBack
        {
            get
            {
                if ( this.mDefaultTransformFeedBack == null )
                {
                    this.mDefaultTransformFeedBack = new TransformFeedback( true );
                }

                return this.mDefaultTransformFeedBack;
            }
        }

        /// <summary>
        /// Gets the default, onscreen frame buffer corresponding
        /// to the current OpenGL context.
        /// </summary>
        public FrameBuffer DefaultFrameBuffer
        {
            get
            {
                OpenTK.Graphics.IGraphicsContext lCurrentContext = OpenTK.Graphics.GraphicsContext.CurrentContext;
                FrameBuffer lDefault =  this.mDefaultFrameBufferMap.HasBuffer( lCurrentContext );
                if ( lDefault == null )
                {
                    lDefault = new FrameBuffer( true );
                    this.mDefaultFrameBufferMap.Map( lCurrentContext, lDefault );
                }

                return lDefault;
            }
        }

        /// <summary>
        /// Gets the currently bound mesh buffers.
        /// </summary>
        public MeshBuffers CurrentMeshBuffers
        {
            get
            {
                return this.mCurrentMeshBuffers;
            }
        }

        /// <summary>
        /// Gets the current framebuffer.
        /// </summary>
        public FrameBuffer CurrentFrameBuffer
        {
            get
            {
                return this.mCurrentFrameBuffer;
            }
        }

        /// <summary>
        /// Gets the currently used pass.
        /// </summary>
        public PipelinePass CurrentPass
        {
            get
            {
                return this.mCurrentPass;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderService"/> class.
        /// </summary>
        public RenderService()
        {
            this.mDefaultFrameBufferMap = new FrameBufferMap();
            this.mParameters = new FrameBufferParameters();
            this.mCurrentRestartIndex   = -1;
            this.mCurrentPatchVertices  = 0;
            this.mCurrentPass = null;
            this.mCurrentMeshBuffers = null;
            this.mCurrentFrameBuffer = null;
            this.mCurrentMeshIndicesOffset = IntPtr.Zero;
        }

        #endregion Constructor

        #region Methods

        #region Methods IRenderService

        /// <summary>
        /// Draws a part of a mesh one or multiple times.
        /// </summary>
        /// <param name="pMeshBuffers">The mesh buffers containing the part of the mesh to draw.</param>
        /// <param name="pMode">The primitive mode determining how vertices must be interpreted.</param>
        /// <param name="pFirst">The first vertex to draw or the first index if that mesh uses indices.</param>
        /// <param name="pCount">The numbers of vertices to draw or indices if that mesh uses indices.</param>
        /// <param name="pPrimCount">The number of times that mesh must be drawn (with geometry instanciating)</param>
        /// <param name="pBase">The base vertex to use but only used for meshes with indices.</param>
        public void Draw(MeshBuffers pMeshBuffers, PrimitiveType pMode, int pFirst, int pCount, int pPrimCount = 1, int pBase = 0)
        {
            this.UseMeshBuffers( pMeshBuffers );

            if ( pMeshBuffers.PrimitiveRestart != this.mCurrentRestartIndex )
            {
                if ( pMeshBuffers.PrimitiveRestart >= 0 )
                {
                    GL.Enable( EnableCap.PrimitiveRestart );
                    GL.PrimitiveRestartIndex( pMeshBuffers.PrimitiveRestart );
                }
                else
                {
                    GL.Disable( EnableCap.PrimitiveRestart );
                }

                this.mCurrentRestartIndex = pMeshBuffers.PrimitiveRestart;
            }

            if ( pMeshBuffers.VerticesPerPatch > 0 &&
                 pMeshBuffers.VerticesPerPatch != this.mCurrentPatchVertices )
            {
                GL.PatchParameter( PatchParameterInt.PatchVertices, pMeshBuffers.VerticesPerPatch );
            }

            if ( pMeshBuffers.IndicesBuffer == null )
            {
                if ( pPrimCount == 1 )
                {
                    GL.DrawArrays( pMode, pFirst, pCount );
                }
                else
                {
                    GL.DrawArraysInstanced( pMode, pFirst, pCount, pPrimCount );
                }
            }
            else
            {
                IntPtr lIndices = this.mCurrentMeshIndicesOffset;
                if ( pFirst > 0 )
                {
                    lIndices = this.mCurrentMeshIndicesOffset + pFirst * pMeshBuffers.IndicesBuffer.AttributeSize;
                }

                if ( pBase == 0 )
                {
                    if ( pPrimCount == 1 )
                    {
                        GL.DrawElements( pMode, pCount, (DrawElementsType)this.mCurrentIndicesType.ToGLAttributeType(), lIndices );
                    }
                    else
                    {
                        GL.DrawElementsInstanced( pMode, pCount, (DrawElementsType)this.mCurrentIndicesType.ToGLAttributeType(), lIndices, pPrimCount );
                    }
                }
                else
                {
                    if ( pPrimCount == 1 )
                    {
                        GL.DrawElementsBaseVertex( pMode, pCount, (DrawElementsType)this.mCurrentIndicesType.ToGLAttributeType(), lIndices, pBase );
                    }
                    else
                    {
                        GL.DrawElementsInstancedBaseVertex( pMode, pCount, (DrawElementsType)this.mCurrentIndicesType.ToGLAttributeType(), lIndices, pPrimCount, pBase );
                    }
                }
            }

            GLHelpers.GetError();
        }

        /// <summary>
        /// Draws several parts of a mesh. Each part being specified with a first and count parameters to be drawn.
        /// These parameters values are passed in arrays of 'pPrimCount' size.
        /// </summary>
        /// <param name="pMeshBuffers">The mesh buffers containing the parts of the mesh to draw.</param>
        /// <param name="pMode">The primitive mode determining how vertices must be interpreted.</param>
        /// <param name="pFirsts">The firsts vertex or the firsts index if that mesh uses indices for each part to draw.</param>
        /// <param name="pCounts">The numbers of vertices or indices if that mesh uses indices for each part to draw.</param>
        /// <param name="pPrimCount">The number of times that mesh must be drawn (with geometry instanciating)</param>
        /// <param name="pBases">The bases vertex to use for each part to draw but only used for meshes with indices.</param>
        public void MultiDraw(MeshBuffers pMeshBuffers, PrimitiveType pMode, int[] pFirsts, int[] pCounts, int pPrimCount, int[] pBases = null)
        {
            this.UseMeshBuffers( pMeshBuffers );

            if ( pMeshBuffers.PrimitiveRestart != this.mCurrentRestartIndex )
            {
                if ( pMeshBuffers.PrimitiveRestart >= 0 )
                {
                    GL.Enable( EnableCap.PrimitiveRestart );
                    GL.PrimitiveRestartIndex( pMeshBuffers.PrimitiveRestart );
                }
                else
                {
                    GL.Disable( EnableCap.PrimitiveRestart );
                }

                this.mCurrentRestartIndex = pMeshBuffers.PrimitiveRestart;
            }

            if ( pMeshBuffers.VerticesPerPatch > 0 &&
                 pMeshBuffers.VerticesPerPatch != this.mCurrentPatchVertices )
            {
                GL.PatchParameter( PatchParameterInt.PatchVertices, pMeshBuffers.VerticesPerPatch );
            }

            if ( pMeshBuffers.IndicesBuffer == null )
            {
                GL.MultiDrawArrays( pMode, pFirsts, pCounts, pPrimCount );
            }
            else
            {
                int lAttributeSize = pMeshBuffers.IndicesBuffer.AttributeSize;
                IntPtr[] lIndices = new IntPtr[ pPrimCount ];
                for ( int lCurr = 0; lCurr < pPrimCount; lCurr++ )
                {
                    lIndices[ lCurr ] = this.mCurrentMeshIndicesOffset + pFirsts[ lCurr ] * lAttributeSize;
                }

                if ( pBases == null )
                {
                    GL.MultiDrawElements( pMode, pCounts, (DrawElementsType)this.mCurrentIndicesType.ToGLAttributeType(), lIndices, pPrimCount );
                }
                else
                {
                    GL.MultiDrawElementsBaseVertex( pMode, pCounts, (DrawElementsType)this.mCurrentIndicesType.ToGLAttributeType(), lIndices, pPrimCount, pBases );
                }
            }

            GLHelpers.GetError();
        }

        /// <summary>
        /// Draws a part of a meshone or multiple times.
        /// </summary>
        /// <param name="pMeshBuffers">The mesh buffers containing the part of the mesh to draw.</param>
        /// <param name="pMode">The primitive mode determining how vertices must be interpreted.</param>
        /// <param name="pDrawInfo">A CPU or GPU buffer containing 32 bit integer packed draw info in the form: pCount, pPrimCount, pFirst and Base parameters.</param>
        public void DrawIndirect(MeshBuffers pMeshBuffers, PrimitiveType pMode, IBuffer pDrawInfo)
        {
            this.UseMeshBuffers( pMeshBuffers );

            if ( pMeshBuffers.PrimitiveRestart != this.mCurrentRestartIndex )
            {
                if ( pMeshBuffers.PrimitiveRestart >= 0 )
                {
                    GL.Enable( EnableCap.PrimitiveRestart );
                    GL.PrimitiveRestartIndex( pMeshBuffers.PrimitiveRestart );
                }
                else
                {
                    GL.Disable( EnableCap.PrimitiveRestart );
                }

                this.mCurrentRestartIndex = pMeshBuffers.PrimitiveRestart;
            }

            if ( pMeshBuffers.VerticesPerPatch > 0 &&
                 pMeshBuffers.VerticesPerPatch != this.mCurrentPatchVertices )
            {
                GL.PatchParameter( PatchParameterInt.PatchVertices, pMeshBuffers.VerticesPerPatch );
            }

            pDrawInfo.Bind( (int)All.DrawIndirectBuffer );
            if ( pMeshBuffers.IndicesBuffer == null )
            {
                GL.DrawArraysIndirect( pMode, pDrawInfo.GetData() );
            }
            else
            {
                GL.DrawElementsIndirect( pMode, this.mCurrentIndicesType.ToGLAttributeType(), pDrawInfo.GetData() );
            }
            pDrawInfo.Unbind( (int)All.DrawIndirectBuffer );

            GLHelpers.GetError();
        }

        /// <summary>
        /// Draws a mesh with a specific vertex count resulting from a transform feedback session.
        /// Only available with GL version >= 4.0
        /// </summary>
        /// <param name="pMeshBuffers"></param>
        /// <param name="pMode"></param>
        /// <param name="pFeedbackId"></param>
        /// <param name="pStream"></param>
        public void DrawFeedback(MeshBuffers pMeshBuffers, PrimitiveType pMode, uint pFeedbackId, uint pStream = 0)
        {
            this.UseMeshBuffers( pMeshBuffers );

            if ( pMeshBuffers.PrimitiveRestart != this.mCurrentRestartIndex )
            {
                if ( pMeshBuffers.PrimitiveRestart >= 0 )
                {
                    GL.Enable( EnableCap.PrimitiveRestart );
                    GL.PrimitiveRestartIndex( pMeshBuffers.PrimitiveRestart );
                }
                else
                {
                    GL.Disable( EnableCap.PrimitiveRestart );
                }

                this.mCurrentRestartIndex = pMeshBuffers.PrimitiveRestart;
            }

            if ( pMeshBuffers.VerticesPerPatch > 0 &&
                 pMeshBuffers.VerticesPerPatch != this.mCurrentPatchVertices )
            {
                GL.PatchParameter( PatchParameterInt.PatchVertices, pMeshBuffers.VerticesPerPatch );
            }

            GL.DrawTransformFeedbackStream( pMode, pFeedbackId, pStream );

            GLHelpers.GetError();
        }
        
        /// <summary>
        /// Uses the supplied pass to render.
        /// </summary>
        /// <param name="pPass">The pass to use for rendering.</param>
        public void UsePass(PipelinePass pPass)
        {
            if ( this.mCurrentPass != pPass )
            {
                this.mCurrentPass = pPass;
                if ( this.mCurrentPass != null )
                {
                    if ( this.mCurrentPass.PipelineId == 0 )
                    {
                        GL.UseProgram( this.mCurrentPass.PassId );
                    }
                    else
                    {
                        GL.BindProgramPipeline( this.mCurrentPass.PipelineId );
                        GL.UseProgram( 0 );
                    }

                    LogManager.Instance.Log( "Using a new pipeline pass..." );

                    this.mCurrentPass.Prepare();
                }
            }

            // If not null whatever it was already the same or not??
            // Update its uniforms.
            if ( this.mCurrentPass != null )
            {
                this.mCurrentPass.UpdateUniforms();
            }
        }

        /// <summary>
        /// Uses the supplied frame buffer as current.
        /// </summary>
        /// <param name="pBuffer">The new current frame buffer.</param>
        /// <returns>True if buffer changed, false otherwise.</returns>
        public bool UseFrameBuffer(FrameBuffer pBuffer)
        {
            bool lHasChanged = false;
            bool lIsValid    = pBuffer != null;
            if ( this.mCurrentFrameBuffer != pBuffer )
            {
                this.mCurrentFrameBuffer = pBuffer;
                if ( this.mCurrentFrameBuffer != null )
                {
                    LogManager.Instance.Log( "Changing the current Frame buffer..." );

                    GL.BindFramebuffer( FramebufferTarget.Framebuffer, pBuffer.Id );
                }

                lHasChanged = true;
            }

            if ( lIsValid )
            {
                if ( lHasChanged || 
                     ((pBuffer.State & FrameBufferState.ParametersChanged) == FrameBufferState.ParametersChanged) )
                {
                    // Updates the current cached parameters with 
                    // the new frame buffer parameters.
                    this.mParameters.Update(  pBuffer.Parameters );
                    pBuffer.ParametersUpdated();
                }

                if ( pBuffer.Id != 0 &&
                     ((pBuffer.State & FrameBufferState.AttachmentsChanged) == FrameBufferState.AttachmentsChanged) )
                {
                    pBuffer.SetAttachment();
                    pBuffer.CheckAttachment();
                    pBuffer.AttachmentsUpdated();
                }

                if ( pBuffer.Id != 0 &&
                     ((pBuffer.State & FrameBufferState.ReadDrawBuffersChanged) == FrameBufferState.ReadDrawBuffersChanged) )
                {
                    GL.ReadBuffer( (ReadBufferMode)pBuffer.ReadBuffer.ToAttachment( pBuffer.Id ) );
                    if ( pBuffer.DrawBufferCount == 1 )
                    {
                        GL.DrawBuffer( (DrawBufferMode)pBuffer[ 0 ].ToAttachment( pBuffer.Id ) );
                    }
                    else
                    {
                        int lCount = pBuffer.DrawBufferCount;
                        DrawBuffersEnum[] lModes = new DrawBuffersEnum[ lCount ];
                        for ( int lCurr = 0; lCurr < lCount; lCurr++ )
                        {
                            lModes[ lCurr ] = (DrawBuffersEnum)pBuffer[ (uint)lCurr ].ToAttachment( pBuffer.Id );
                        }
                        GL.DrawBuffers( lCount, lModes );
                    }

                    pBuffer.BuffersUpdated();
                    GLHelpers.GetError();
                }
            }

            return lHasChanged;
        }

        /// <summary>
        /// Uses the supplied mesh buffers to draw.
        /// </summary>
        /// <param name="pMeshBuffers">The mesh buffers</param>
        public void UseMeshBuffers(MeshBuffers pMeshBuffers)
        {
            if
                ( this.mCurrentMeshBuffers != pMeshBuffers )
            {
                if 
                    ( this.mCurrentMeshBuffers != null )
                {
                    this.mCurrentMeshBuffers.Unbind();
                }

                this.mCurrentMeshBuffers = pMeshBuffers;

                if 
                    ( this.mCurrentMeshBuffers != null )
                {
                    this.mCurrentMeshBuffers.Bind( ref this.mCurrentIndicesType, ref this.mCurrentMeshIndicesOffset );
                }
            }
        }

        /// <summary>
        /// Starts a transform feedback session. 
        /// Actual transforms are performed with the given pass.
        /// It then ends with a call to the so called 'EndTransformFeedback' method.
        /// GL 4.0 allows extra functions such as 'PauseTransformFeedback' and 'ResumeTransformFeedback' in the meantime 
        /// to swap with another feedback instance in which recording for example.
        /// </summary>
        /// <param name="pBuffer">The frame buffer to use for the feedback session.</param>
        /// <param name="pPass">The pipeline pass to use for the feedback session.</param>
        /// <param name="pMode">The primitive topology informing about how vertices must be interpreted.</param>
        /// <param name="pFeedback">The feedback buffers in which recorded varying variables are stored.</param>
        /// <param name="pRasterize">The flag indicating whether the transformed primitives must be rasterized or not during the session.</param>
        public void StartTransformFeedback(FrameBuffer pBuffer, PipelinePass pPass, PrimitiveType pMode, TransformFeedback pFeedback, bool pRasterize)
        {
            if
                ( pMode == PrimitiveType.Points || 
                  pMode == PrimitiveType.Lines || 
                  pMode == PrimitiveType.Triangles )
            {
                this.mCurrentFeedbackFrameBuffer = pBuffer;
                this.mCurrentFeedbackTransform   = pPass;
                this.mCurrentFeedbackMode        = pMode;

                this.UseFrameBuffer( this.mCurrentFeedbackFrameBuffer );
                this.UsePass( this.mCurrentFeedbackTransform );

                pFeedback.Bind();

                GL.BeginTransformFeedback( (TransformFeedbackPrimitiveType)pMode );

                if
                    ( pRasterize == false )
                {
                    GL.Enable( EnableCap.RasterizerDiscard );
                }
            }
        }

        /// <summary>
        /// Transforms a part of a mesh one or more times.
        /// </summary>
        /// <param name="pMesh">The mesh to transform.</param>
        /// <param name="pFirst">The first vertex or index to use for drawing the mesh.</param>
        /// <param name="pCount">The amount of vertices or indices to use for drawing the mesh.</param>
        /// <param name="pPrimCount">The number of times the mesh must be drawn (with geometry instanciating)</param>
        /// <param name="pBase">The base vertex to use but only if the mesh is using indices.</param>
        public void Transform(MeshBuffers pMesh, int pFirst, int pCount, int pPrimCount = 1, int pBase = 0)
        {
            this.UseFrameBuffer( this.mCurrentFeedbackFrameBuffer );
            this.UsePass( this.mCurrentFeedbackTransform );

            this.mCurrentFeedbackFrameBuffer.EnableConditionalRender();
            this.Draw( pMesh, this.mCurrentFeedbackMode, pFirst, pCount, pPrimCount, pBase );
            this.mCurrentFeedbackFrameBuffer.DisableConditionalRender();
        }

        /// <summary>
        /// Transforms several parts of a mesh each part being specified with a starting element, a count and a base vertex.
        /// </summary>
        /// <param name="pMesh">The mesh to transform.</param>
        /// <param name="pFirsts">An array of pPrimCount first vertex or index to use for drawing the mesh.</param>
        /// <param name="pCounts">An array of pPrimCount amount of vertices or indices to use for drawing the mesh.</param>
        /// <param name="pPrimCount">The number of times the mesh must be drawn (with geometry instanciating)</param>
        /// <param name="pBases">The base vertices to use but only if the mesh is using indices.</param>
        public void MultiTransform(MeshBuffers pMesh, int[] pFirsts, int[] pCounts, int pPrimCount, int[] pBases = null)
        {
            this.UseFrameBuffer( this.mCurrentFeedbackFrameBuffer );
            this.UsePass( this.mCurrentFeedbackTransform );

            this.mCurrentFeedbackFrameBuffer.EnableConditionalRender();
            this.MultiDraw( pMesh, this.mCurrentFeedbackMode, pFirsts, pCounts, pPrimCount, pBases );
            this.mCurrentFeedbackFrameBuffer.DisableConditionalRender();
        }

        /// <summary>
        /// Transforms a part of a mesh one or more times.
        /// </summary>
        /// <param name="pMesh">The mesh to transform.</param>
        /// <param name="pBuffer">The CPU or GPU buffer containing the start, count, primcount and base parameters formated into a 32 bits Int</param>
        public void TransformIndirect(MeshBuffers pMesh, IBuffer pBuffer)
        {
            this.UseFrameBuffer( this.mCurrentFeedbackFrameBuffer );
            this.UsePass( this.mCurrentFeedbackTransform );

            this.mCurrentFeedbackFrameBuffer.EnableConditionalRender();
            this.DrawIndirect( pMesh, this.mCurrentFeedbackMode, pBuffer );
            this.mCurrentFeedbackFrameBuffer.DisableConditionalRender();
        }

        /// <summary>
        /// Re transforms a mesh resulting from a previous transform feedback session.
        /// Only for GL version >= 4.0.
        /// </summary>
        /// <param name="pMesh">The mesh to re-transform.</param>
        /// <param name="pPreviousFeedback">The previous transform feedback session results</param>
        /// <param name="pStream">The stream to draw.</param>
        public void ReTransform(MeshBuffers pMesh, TransformFeedback pPreviousFeedback, uint pStream = 0)
        {
            if
                ( pPreviousFeedback.Id != 0 )
            {
                this.UseFrameBuffer( this.mCurrentFeedbackFrameBuffer );
                this.UsePass( this.mCurrentFeedbackTransform );

                this.mCurrentFeedbackFrameBuffer.EnableConditionalRender();
                this.DrawFeedback( pMesh, this.mCurrentFeedbackMode, pPreviousFeedback.Id, pStream );
                this.mCurrentFeedbackFrameBuffer.DisableConditionalRender();
            }
        }

        /// <summary>
        /// Pauses the current transform feedback session.
        /// Only for GL version >= 4.0.
        /// </summary>
        public void PauseTransformFeedback()
        {
            GL.PauseTransformFeedback();
        }

        /// <summary>
        /// Resumes the current transform feedback session.
        /// Only for GL version >= 4.0.
        /// </summary>
        /// <param name="pFeedback">The feedback container in which storing results of the session.</param>
        public void ResumeTransformFeedback(TransformFeedback pFeedback)
        {
            this.UseFrameBuffer( this.mCurrentFeedbackFrameBuffer );
            this.UsePass( this.mCurrentFeedbackTransform );

            pFeedback.Bind();

            GL.ResumeTransformFeedback();
        }

        /// <summary>
        /// Ends the current transform feedback session.
        /// </summary>
        public void EndTransformFeedback()
        {
            GL.Disable( EnableCap.RasterizerDiscard );
            GL.EndTransformFeedback();
            this.mCurrentFeedbackFrameBuffer = null;
            this.mCurrentFeedbackTransform   = null;
        }

        /// <summary>
        /// Resets all the renderer internal specific states
        /// NOTE: To use before and after using the OpenGL API directly for instance to avoid issues with GL global states.
        /// </summary>
        public void ResetAllStates()
        {
            LogManager.Instance.Log( "Reset all GL states..." );

            this.UseMeshBuffers( null );
            
            GL.BindBuffer( BufferTarget.ArrayBuffer, 0 );
            GL.BindBuffer( BufferTarget.ElementArrayBuffer, 0 );

            this.UseFrameBuffer( null );
            this.UsePass( null );

            TextureUnitManager.Instance.UnbindAll();
        }
        
        #endregion Methods IRenderService

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDispose()
        {
            this.mDefaultFrameBufferMap.Clear();

            this.mCurrentPass = null;
            this.mCurrentMeshBuffers = null;
            this.mCurrentFrameBuffer = null;
            this.mCurrentFeedbackFrameBuffer = null;
            this.mCurrentFeedbackTransform   = null;

            if ( this.mDefaultTransformFeedBack != null )
            {
                this.mDefaultTransformFeedBack.Dispose();
                this.mDefaultTransformFeedBack = null;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Inner Frame buffer map class definition.
        /// </summary>
        private class FrameBufferMap
        {
            #region Fields

            /// <summary>
            /// Stores the frame buffers by GL context.
            /// </summary>
            private Dictionary<OpenTK.Graphics.IGraphicsContext, FrameBuffer> mBuffers;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="FrameBufferMap"/> class.
            /// </summary>
            public FrameBufferMap()
            {
                this.mBuffers = new Dictionary<OpenTK.Graphics.IGraphicsContext, FrameBuffer>();
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// Checks if the GL context has a frame buffer linked to it or not.
            /// </summary>
            /// <param name="pContext">The GL context.</param>
            /// <returns>The frame buffer if any, Null otherwise.</returns>
            public FrameBuffer HasBuffer(OpenTK.Graphics.IGraphicsContext pContext)
            {
                if
                    ( pContext == null )
                {
                    return null;
                }

                FrameBuffer lResult;
                if
                    ( this.mBuffers.TryGetValue( pContext, out lResult ) )
                {
                    return lResult;
                }

                return null;
            }

            /// <summary>
            /// Maps a given GL context to a Frame buffer.
            /// </summary>
            /// <param name="pContext">The GL context.</param>
            /// <param name="pBuffer">The linked frame buffer.</param>
            public void Map(OpenTK.Graphics.IGraphicsContext pContext, FrameBuffer pBuffer)
            {
                this.mBuffers.Add( pContext, pBuffer );
            }

            /// <summary>
            /// Unmaps the frame buffer corresponding to the given GL context.
            /// </summary>
            /// <param name="pContext">The GL context.</param>
            /// <returns>True if found, false otherwise.</returns>
            public bool Unmap(OpenTK.Graphics.IGraphicsContext pContext)
            {
                return this.mBuffers.Remove( pContext );
            }

            /// <summary>
            /// Clears the default frame buffer cache manager.
            /// </summary>
            public void Clear()
            {
                foreach ( KeyValuePair<OpenTK.Graphics.IGraphicsContext, FrameBuffer> lPair in this.mBuffers )
                {
                    lPair.Value.Dispose();
                }
                this.mBuffers.Clear();
            }

            #endregion Methods
        }

        #endregion Inner classes
    }
}
