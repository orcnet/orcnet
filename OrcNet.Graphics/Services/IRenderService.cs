﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;

namespace OrcNet.Graphics.Services
{
    /// <summary>
    /// Rendering service in charge of managing passes.
    /// </summary>
    public interface IRenderService : IService
    {
        #region Properties

        /// <summary>
        /// Gets the current transform feedback frame buffer.
        /// </summary>
        FrameBuffer CurrentTransformFeedbackFrameBuffer
        {
            get;
        }

        /// <summary>
        /// Gets the current transform feedback pipeline pass.
        /// </summary>
        PipelinePass CurrentTransformFeedbackPass
        {
            get;
        }

        /// <summary>
        /// Gets the default transform feedback.
        /// </summary>
        TransformFeedback DefaultTransformFeedBack
        {
            get;
        }

        /// <summary>
        /// Gets the default, onscreen frame buffer corresponding
        /// to the current OpenGL context.
        /// </summary>
        FrameBuffer DefaultFrameBuffer
        {
            get;
        }

        /// <summary>
        /// Gets the currently bound mesh buffers.
        /// </summary>
        MeshBuffers CurrentMeshBuffers
        {
            get;
        }

        /// <summary>
        /// Gets the default, onscreen framebuffer.
        /// </summary>
        FrameBuffer CurrentFrameBuffer
        {
            get;
        }

        /// <summary>
        /// Gets the currently used pass.
        /// </summary>
        PipelinePass CurrentPass
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Draws a part of a mesh one or multiple times.
        /// </summary>
        /// <param name="pMeshBuffers">The mesh buffers containing the part of the mesh to draw.</param>
        /// <param name="pMode">The primitive mode determining how vertices must be interpreted.</param>
        /// <param name="pFirst">The first vertex to draw or the first index if that mesh uses indices.</param>
        /// <param name="pCount">The numbers of vertices to draw or indices if that mesh uses indices.</param>
        /// <param name="pPrimCount">The number of times that mesh must be drawn (with geometry instanciating)</param>
        /// <param name="pBase">The base vertex to use but only used for meshes with indices.</param>
        void Draw(MeshBuffers pMeshBuffers, PrimitiveType pMode, int pFirst, int pCount, int pPrimCount = 1, int pBase = 0);

        /// <summary>
        /// Draws several parts of a mesh. Each part being specified with a first and count parameters to be drawn.
        /// These parameters values are passed in arrays of 'pPrimCount' size.
        /// </summary>
        /// <param name="pMeshBuffers">The mesh buffers containing the parts of the mesh to draw.</param>
        /// <param name="pMode">The primitive mode determining how vertices must be interpreted.</param>
        /// <param name="pFirsts">The firsts vertex or the firsts index if that mesh uses indices for each part to draw.</param>
        /// <param name="pCounts">The numbers of vertices or indices if that mesh uses indices for each part to draw.</param>
        /// <param name="pPrimCount">The number of times that mesh must be drawn (with geometry instanciating)</param>
        /// <param name="pBases">The bases vertex to use for each part to draw but only used for meshes with indices.</param>
        void MultiDraw(MeshBuffers pMeshBuffers, PrimitiveType pMode, int[] pFirsts, int[] pCounts, int pPrimCount, int[] pBases = null);

        /// <summary>
        /// Draws a part of a meshone or multiple times.
        /// </summary>
        /// <param name="pMeshBuffers">The mesh buffers containing the part of the mesh to draw.</param>
        /// <param name="pMode">The primitive mode determining how vertices must be interpreted.</param>
        /// <param name="pDrawInfo">A CPU or GPU buffer containing 32 bit integer packed draw info in the form: pCount, pPrimCount, pFirst and Base parameters.</param>
        void DrawIndirect(MeshBuffers pMeshBuffers, PrimitiveType pMode, IBuffer pDrawInfo);

        /// <summary>
        /// Draws a mesh with a specific vertex count resulting from a transform feedback session.
        /// Only available with GL version >= 4.0
        /// </summary>
        /// <param name="pMeshBuffers"></param>
        /// <param name="pMode"></param>
        /// <param name="pFeedbackId"></param>
        /// <param name="pStream"></param>
        void DrawFeedback(MeshBuffers pMeshBuffers, PrimitiveType pMode, uint pFeedbackId, uint pStream = 0);

        /// <summary>
        /// Uses the supplied pass to render.
        /// </summary>
        /// <param name="pPass">The pass to use for rendering.</param>
        void UsePass(PipelinePass pPass);

        /// <summary>
        /// Uses the supplied frame buffer as current.
        /// </summary>
        /// <param name="pBuffer">The new current frame buffer.</param>
        bool UseFrameBuffer(FrameBuffer pBuffer);

        /// <summary>
        /// Uses the supplied mesh buffers to draw.
        /// </summary>
        /// <param name="pMeshBuffers">The mesh buffers</param>
        void UseMeshBuffers(MeshBuffers pMeshBuffers);

        /// <summary>
        /// Starts a transform feedback session. 
        /// Actual transforms are performed with the given pass.
        /// It then ends with a call to the so called 'EndTransformFeedback' method.
        /// GL 4.0 allows extra functions such as 'PauseTransformFeedback' and 'ResumeTransformFeedback' in the meantime 
        /// to swap with another feedback instance in which recording for example.
        /// </summary>
        /// <param name="pBuffer">The frame buffer to use for the feedback session.</param>
        /// <param name="pPass">The pipeline pass to use for the feedback session.</param>
        /// <param name="pMode">The primitive topology informing about how vertices must be interpreted.</param>
        /// <param name="pFeedback">The feedback buffers in which recorded varying variables are stored.</param>
        /// <param name="pRasterize">The flag indicating whether the transformed primitives must be rasterized or not during the session.</param>
        void StartTransformFeedback(FrameBuffer pBuffer, PipelinePass pPass, PrimitiveType pMode, TransformFeedback pFeedback, bool pRasterize);

        /// <summary>
        /// Transforms a part of a mesh one or more times.
        /// </summary>
        /// <param name="pMesh">The mesh to transform.</param>
        /// <param name="pFirst">The first vertex or index to use for drawing the mesh.</param>
        /// <param name="pCount">The amount of vertices or indices to use for drawing the mesh.</param>
        /// <param name="pPrimCount">The number of times the mesh must be drawn (with geometry instanciating)</param>
        /// <param name="pBase">The base vertex to use but only if the mesh is using indices.</param>
        void Transform(MeshBuffers pMesh, int pFirst, int pCount, int pPrimCount = 1, int pBase = 0);

        /// <summary>
        /// Transforms several parts of a mesh each part being specified with a starting element, a count and a base vertex.
        /// </summary>
        /// <param name="pMesh">The mesh to transform.</param>
        /// <param name="pFirsts">An array of pPrimCount first vertex or index to use for drawing the mesh.</param>
        /// <param name="pCounts">An array of pPrimCount amount of vertices or indices to use for drawing the mesh.</param>
        /// <param name="pPrimCount">The number of times the mesh must be drawn (with geometry instanciating)</param>
        /// <param name="pBases">The base vertices to use but only if the mesh is using indices.</param>
        void MultiTransform(MeshBuffers pMesh, int[] pFirsts, int[] pCounts, int pPrimCount, int[] pBases = null);

        /// <summary>
        /// Transforms a part of a mesh one or more times.
        /// </summary>
        /// <param name="pMesh">The mesh to transform.</param>
        /// <param name="pBuffer">The CPU or GPU buffer containing the start, count, primcount and base parameters formated into a 32 bits Int</param>
        void TransformIndirect(MeshBuffers pMesh, IBuffer pBuffer);

        /// <summary>
        /// Re transforms a mesh resulting from a previous transform feedback session.
        /// Only for GL version >= 4.0.
        /// </summary>
        /// <param name="pMesh">The mesh to re-transform.</param>
        /// <param name="pFeedback">The previous transform feedback session results</param>
        /// <param name="pStream">The stream to draw.</param>
        void ReTransform(MeshBuffers pMesh, TransformFeedback pFeedback, uint pStream = 0);

        /// <summary>
        /// Pauses the current transform feedback session.
        /// Only for GL version >= 4.0.
        /// </summary>
        void PauseTransformFeedback();

        /// <summary>
        /// Resumes the current transform feedback session.
        /// Only for GL version >= 4.0.
        /// </summary>
        /// <param name="pFeedback">The feedback container in which storing results of the session.</param>
        void ResumeTransformFeedback(TransformFeedback pFeedback);

        /// <summary>
        /// Ends the current transform feedback session.
        /// </summary>
        void EndTransformFeedback();

        /// <summary>
        /// Resets all the renderer internal specific states
        /// NOTE: To use before and after using the OpenGL API directly for instance to avoid issues with GL global states.
        /// </summary>
        void ResetAllStates();
        
        #endregion Methods
    }
}
