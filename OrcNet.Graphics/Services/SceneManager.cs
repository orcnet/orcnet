﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Datastructures;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Core.Render;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.SceneGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace OrcNet.Graphics.Services
{
    /// <summary>
    /// Scene manager service class definition.
    /// </summary>
    public class SceneManager : AService, ISceneService
    {
        #region Fields

        /// <summary>
        /// Stores the current frame count.
        /// </summary>
        private uint mFrameCount;

        /// <summary>
        /// Stores the current absolute time since the last update.
        /// </summary>
        private double mCurrentAbsoluteTime;

        /// <summary>
        /// Stores the current elasped time since the last update.
        /// </summary>
        private double mCurrentElapsedTime;

        /// <summary>
        /// Stores the current frame buffer.
        /// </summary>
        private FrameBuffer mCurrentFrameBuffer;

        /// <summary>
        /// Stores the current pipeline pass for drawing.
        /// </summary>
        private PipelinePass mCurrentPass;

        /// <summary>
        /// Stores the root node of the scene.
        /// </summary>
        private SceneNode   mRoot;

        /// <summary>
        /// Stores the camera node of the scene.
        /// </summary>
        private SceneNode   mCamera;

        /// <summary>
        /// Stores the flag that identifies the camera node in the scene graph.
        /// </summary>
        private string      mCameraFlag;

        /// <summary>
        /// Stores camera method name to use to draw the scene.
        /// </summary>
        private string      mCameraMethod;

        /// <summary>
        /// Stores the world to screen transformation.
        /// </summary>
        private AMatrix<float> mWorldToScreenTransform;

        /// <summary>
        /// Stores the camera to screen transformation.
        /// </summary>
        private AMatrix<float> mCameraToScreenTransform;

        /// <summary>
        /// Stores the camera frustum planes in world space.
        /// </summary>
        private Vector4F[]  mWorldFrustumPlanes;

        /// <summary>
        /// Stores the last task or task set that was used to draw the scene.
        /// </summary>
        private ITask       mCurrentTask;

        /// <summary>
        /// Stores the container mapping a flag to one or multiple scene node(s).
        /// </summary>
        private Multimap<string, SceneNode> mNodesMap;

        /// <summary>
        /// Stores the container mapping a loop variable to its current value.
        /// </summary>
        private Dictionary<string, SceneNode> mNodeVariables;

        /// <summary>
        /// Stores the task scheduler managing tasks to draw the scene.
        /// </summary>
        private IScheduler mScheduler;

        #endregion Fields

        #region Properties

        #region Properties IService

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "SceneManager";
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties IService

        #region Properties ISceneService

        /// <summary>
        /// Gets or sets the current frame buffer.
        /// </summary>
        public IFrameBuffer CurrentFrameBuffer
        {
            get
            {
                if ( this.mCurrentFrameBuffer == null )
                {
                    IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
                    this.mCurrentFrameBuffer = lRenderService.DefaultFrameBuffer;
                }

                return this.mCurrentFrameBuffer;
            }
            set
            {
                this.mCurrentFrameBuffer = value as FrameBuffer;
            }
        }

        /// <summary>
        /// Gets or sets the current pipeline pass for drawing.
        /// </summary>
        public IPipelinePass CurrentPass
        {
            get
            {
                return this.mCurrentPass;
            }
            set
            {
                this.mCurrentPass = value as PipelinePass;
            }
        }

        /// <summary>
        /// Gets or sets the node currently mapped with the supplied loop variable name.
        /// </summary>
        /// <param name="pName">The loop variable name</param>
        /// <returns>The mapped node to that loop variable.</returns>
        public ISceneNode this[string pName]
        {
            get
            {
                SceneNode lNode;
                if ( this.mNodeVariables.TryGetValue( pName, out lNode ) )
                {
                    return lNode;
                }

                return null;
            }
            set
            {
                this.mNodeVariables[ pName ] = value as SceneNode;
            }
        }

        /// <summary>
        /// Gets the absolute current time in micro seconds
        /// see Update.
        /// </summary>
        public double AbsoluteTime
        {
            get
            {
                return this.mCurrentAbsoluteTime;
            }
        }

        /// <summary>
        /// Gets or sets the camera node of the scene.
        /// </summary>
        public ISceneNode Camera
        {
            get
            {
                if ( this.mCamera == null )
                {
                    IEnumerable<ISceneNode> lCandidates = this.GetNodes( this.mCameraFlag );
                    this.mCamera = lCandidates.FirstOrDefault() as SceneNode;
                }

                return this.mCamera;
            }
        }

        /// <summary>
        /// Gets or sets the camera node method to call for drawing the scene.
        /// </summary>
        public string CameraMethod
        {
            get
            {
                return this.mCameraMethod;
            }
            set
            {
                this.mCameraMethod = value;
            }
        }

        /// <summary>
        /// Gets the elapsed time in micro seconds since the last update call.
        /// see Update.
        /// </summary>
        public double ElapsedTime
        {
            get
            {
                return this.mCurrentElapsedTime;
            }
        }

        /// <summary>
        /// Gets the frame count that is the number of draw calls.
        /// </summary>
        public uint FrameCount
        {
            get
            {
                return this.mFrameCount;
            }
        }

        /// <summary>
        /// Gets or sets the root node of the scene.
        /// </summary>
        public ISceneNode Root
        {
            get
            {
                return this.mRoot;
            }

            set
            {
                this.mRoot = value as SceneNode;
                this.mCamera = null;
            }
        }

        /// <summary>
        /// Gets or sets the scene task scheduler used to schedule tasks.
        /// </summary>
        public IScheduler Scheduler
        {
            get
            {
                return this.mScheduler;
            }
            set
            {
                this.mScheduler = value;
            }
        }

        #endregion Properties ISceneService

        /// <summary>
        /// Gets the world to screen transformation.
        /// </summary>
        public AMatrix<float> WorldToScreenTransform
        {
            get
            {
                return this.mWorldToScreenTransform;
            }
        }

        /// <summary>
        /// Gets or sets the camera to screen transformation.
        /// </summary>
        public AMatrix<float> CameraToScreenTransform
        {
            get
            {
                return this.mCameraToScreenTransform;
            }
            set
            {
                this.mCameraToScreenTransform = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneManager"/> class.
        /// </summary>
        public SceneManager()
        {
            this.mFrameCount = 0;
            this.mWorldToScreenTransform = new Matrix4F( 0.0f, 0.0f, 0.0f, 0.0f,
                                                         0.0f, 0.0f, 0.0f, 0.0f,
                                                         0.0f, 0.0f, 0.0f, 0.0f,
                                                         0.0f, 0.0f, 0.0f, 0.0f );
            this.mWorldFrustumPlanes = new Vector4F[ 6 ]
            {
                new Vector4F( 0.0f ),
                new Vector4F( 0.0f ),
                new Vector4F( 0.0f ),
                new Vector4F( 0.0f ),
                new Vector4F( 0.0f ),
                new Vector4F( 0.0f )
            };
            this.mNodesMap = new Multimap<string, SceneNode>();
            this.mNodeVariables = new Dictionary<string, SceneNode>();
        }

        #endregion Constructor

        #region Methods

        #region Methods ISceneService

        /// <summary>
        /// Checks the world bounds visibility regarding to the camera node.
        /// </summary>
        /// <param name="pWorldBounds">The bounds in world space.</param>
        /// <returns>The visibility state.</returns>
        public BoxVisibility IsVisible(IBox<float> pWorldBounds)
        {
            return SceneManager.GetVisibility( this.mWorldFrustumPlanes, pWorldBounds as ABox3<float> );
        }

        /// <summary>
        /// Sets the camera node of the scene.
        /// </summary>
        /// <param name="pFlag">The scene node flag that identifies the camera node.</param>
        public void SetNodeAsCamera(string pFlag)
        {
            this.mCamera = null;
            this.mCameraFlag = pFlag;

            IEnumerable<ISceneNode> lCandidates = this.GetNodes( this.mCameraFlag );
            this.mCamera = lCandidates.FirstOrDefault() as SceneNode;
        }

        /// <summary>
        /// Gets the set of nodes having the specified flag in the scene.
        /// </summary>
        /// <param name="pFlag">The flag to look for.</param>
        /// <returns>The set of nodes having the given flag.</returns>
        public IEnumerable<ISceneNode> GetNodes(string pFlag)
        {
            // Builds the cache first if needed...
            if ( this.mNodesMap.Count == 0 &&
                 this.mRoot != null )
            {
                this.BuildNodeMap( this.mRoot );
            }

            return this.mNodesMap.GetValues( pFlag );
        }

        /// <summary>
        /// Updates the scene.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute current time in micro seconds.</param>
        /// <param name="pElapsedTime">The elapsed time in micro seconds since the last update.</param>
        public void Update(double pAbsoluteTime, double pElapsedTime)
        {
            this.mCurrentAbsoluteTime = pAbsoluteTime;
            this.mCurrentElapsedTime  = pElapsedTime;

            if ( this.mRoot != null )
            {
                SceneNode lCameraNode = this.Camera as SceneNode;
                this.mRoot.UpdateLocalToWorldTransform( null );
                AMatrix<float> lCameraToScreenTransform = this.mCameraToScreenTransform;
                AMatrix<float> lWorldToCameraTransform = lCameraNode.WorldToLocalTransform;
                this.mWorldToScreenTransform = lCameraToScreenTransform * lWorldToCameraTransform;
                this.mRoot.UpdateLocalToCameraTransform( lWorldToCameraTransform, lCameraToScreenTransform );
                SceneManager.ComputeFrustumPlanesFromProjection( this.mWorldToScreenTransform, ref this.mWorldFrustumPlanes );
                this.ComputeVisibility( this.mRoot, BoxVisibility.PARTIALLY_VISIBLE );
            }
        }

        /// <summary>
        /// Executes the rendering.
        /// </summary>
        public void Render()
        {
            if ( this.mCamera != null )
            {
                SceneNodeMethod lMethod = this.mCamera.GetMethod( this.mCameraMethod );
                if ( lMethod != null )
                {
                    ITask lNewTask = null;
                    try
                    {
                        lNewTask = lMethod.CreateTask();
                    }
                    catch ( Exception pEx )
                    {
                        LogManager.Instance.Log( pEx );
                    }

                    if ( lNewTask != null )
                    {
                        this.mScheduler.Execute( lNewTask );
                        this.mCurrentTask = lNewTask;
                    }
                    else if ( this.mCurrentTask != null )
                    {
                        this.mScheduler.Execute( this.mCurrentTask );
                    }
                }
            }

            this.mFrameCount++;
        }

        #endregion Methods ISceneService

        #region Methods Internal

        /// <summary>
        /// Checks whether the point in world space is visible from the camera or not.
        /// </summary>
        /// <param name="pPointInWorldSpace">The point in world space to test.</param>
        /// <returns>True if the point is visible from the camera, false otherwise.</returns>
        internal bool IsVisible(Vector3F pPointInWorldSpace)
        {
            for ( int lCurr = 0; lCurr < 5; lCurr++ )
            {
                bool lResult;
                if ( this.mWorldFrustumPlanes[ lCurr ].DotProduct( pPointInWorldSpace, out lResult ) <= 0 )
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Projects the given screen position into the scene world.
        /// </summary>
        /// <param name="pScreenX">The X component of the screen position.</param>
        /// <param name="pScreenY">The Y Component of the screen position.</param>
        /// <returns>The projected 3D position in world space.</returns>
        internal Vector3F Project(int pScreenX, int pScreenY)
        {
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                float lWinX;
                float lWinY;
                float lWinZ = 0.0f;
                FrameBuffer lDefault = lRenderService.DefaultFrameBuffer;
                Viewport lViewPort = lDefault.Parameters.Viewports.FirstOrDefault();
                float lWidth  = lViewPort.Width;
                float lHeight = lViewPort.Height;
                float[] lWinZArray = new float[ 1 ] { lWinZ };
                GCHandle lOutputPtr = GCHandle.Alloc( lWinZArray, GCHandleType.Pinned );
                lDefault.ReadPixels( pScreenX, 
                                     (int)lHeight - pScreenY, 
                                     1, 
                                     1, 
                                     PixelFormat.DepthComponent, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );
                lWinZ = lWinZArray[ 0 ];
                lOutputPtr.Free();

                lWinX = (pScreenX * 2.0f) / lWidth - 1.0f;
                lWinY = 1.0f - (pScreenY * 2.0f) / lHeight;
                lWinZ = 2.0f * lWinZ - 1.0f;

                bool lResult;
                IMatrix<float> lScreenToWorldTransform = this.WorldToScreenTransform.Inverse( float.Epsilon, out lResult );
                Vector4F lPoint = lScreenToWorldTransform.Multiply( new Vector4F( lWinX, lWinY, lWinZ, 1.0f ) ) as Vector4F;

                return new Vector3F( lPoint.X / lPoint.W, lPoint.Y / lPoint.W, lPoint.Z / lPoint.W );
            }
            
            return Vector3F.ZERO;
        }

        /// <summary>
        /// Clears the Node map cache.
        /// </summary>
        internal void ClearNodeMap()
        {
            this.mNodesMap.Clear();
        }

        /// <summary>
        /// Computes the frustum planes from the given projection matrix.
        /// </summary>
        /// <param name="pToScreenTransform">The camera to screen projection matrix.</param>
        /// <param name="pFrustumPlanes">The resulting frustum planes in camera space.</param>
        public static void ComputeFrustumPlanesFromProjection(AMatrix<float> pToScreenTransform, ref Vector4F[] pFrustumPlanes)
        {
            float[] lData = pToScreenTransform.Data;
            
            // Extract the LEFT plane
            pFrustumPlanes[ 0 ].X = lData[12] + lData[0];
            pFrustumPlanes[ 0 ].Y = lData[13] + lData[1];
            pFrustumPlanes[ 0 ].Z = lData[14] + lData[2];
            pFrustumPlanes[ 0 ].W = lData[15] + lData[3];
            // Extract the RIGHT plaplane
            pFrustumPlanes[ 1 ].X = lData[12] - lData[0];
            pFrustumPlanes[ 1 ].Y = lData[13] - lData[1];
            pFrustumPlanes[ 1 ].Z = lData[14] - lData[2];
            pFrustumPlanes[ 1 ].W = lData[15] - lData[3];
            // Extract the BOTTOM pl plane
            pFrustumPlanes[ 2 ].X = lData[12] + lData[4];
            pFrustumPlanes[ 2 ].Y = lData[13] + lData[5];
            pFrustumPlanes[ 2 ].Z = lData[14] + lData[6];
            pFrustumPlanes[ 2 ].W = lData[15] + lData[7];
            // Extract the TOP planeane
            pFrustumPlanes[ 3 ].X = lData[12] - lData[4];
            pFrustumPlanes[ 3 ].Y = lData[13] - lData[5];
            pFrustumPlanes[ 3 ].Z = lData[14] - lData[6];
            pFrustumPlanes[ 3 ].W = lData[15] - lData[7];
            // Extract the NEAR planlane
            pFrustumPlanes[ 4 ].X = lData[12] + lData[8];
            pFrustumPlanes[ 4 ].Y = lData[13] + lData[9];
            pFrustumPlanes[ 4 ].Z = lData[14] + lData[10];
            pFrustumPlanes[ 4 ].W = lData[15] + lData[11];
            // Extract the FAR planeane
            pFrustumPlanes[ 5 ].X = lData[12] - lData[8];
            pFrustumPlanes[ 5 ].Y = lData[13] - lData[9];
            pFrustumPlanes[ 5 ].Z = lData[14] - lData[10];
            pFrustumPlanes[ 5 ].W = lData[15] - lData[11];
        }

        /// <summary>
        /// Gets the visibility of the given bounding box in the supplied frustum.
        /// </summary>
        /// <param name="pFrustumPlanes">The frustum planes the box must be tested against.</param>
        /// <param name="pBounds">The bound to test regarding to the frustum.</param>
        /// <returns>The box visibility.</returns>
        public static BoxVisibility GetVisibility(Vector4<float>[] pFrustumPlanes, ABox3<float> pBounds)
        {
            if ( pBounds == null )
            {
                return BoxVisibility.INVISIBLE;
            }

            BoxVisibility lToPlane0Visibility = SceneManager.GetVisibility( pFrustumPlanes[ 0 ], pBounds );
            if ( lToPlane0Visibility == BoxVisibility.INVISIBLE )
            {
                return BoxVisibility.INVISIBLE;
            }

            BoxVisibility lToPlane1Visibility = SceneManager.GetVisibility( pFrustumPlanes[ 1 ], pBounds );
            if ( lToPlane1Visibility == BoxVisibility.INVISIBLE )
            {
                return BoxVisibility.INVISIBLE;
            }

            BoxVisibility lToPlane2Visibility = SceneManager.GetVisibility( pFrustumPlanes[ 2 ], pBounds );
            if ( lToPlane2Visibility == BoxVisibility.INVISIBLE )
            {
                return BoxVisibility.INVISIBLE;
            }

            BoxVisibility lToPlane3Visibility = SceneManager.GetVisibility( pFrustumPlanes[ 3 ], pBounds );
            if ( lToPlane3Visibility == BoxVisibility.INVISIBLE )
            {
                return BoxVisibility.INVISIBLE;
            }

            BoxVisibility lToPlane4Visibility = SceneManager.GetVisibility( pFrustumPlanes[ 4 ], pBounds );
            if ( lToPlane4Visibility == BoxVisibility.INVISIBLE )
            {
                return BoxVisibility.INVISIBLE;
            }

            if ( lToPlane0Visibility == BoxVisibility.FULLY_VISIBLE &&
                 lToPlane1Visibility == BoxVisibility.FULLY_VISIBLE &&
                 lToPlane2Visibility == BoxVisibility.FULLY_VISIBLE &&
                 lToPlane3Visibility == BoxVisibility.FULLY_VISIBLE &&
                 lToPlane4Visibility == BoxVisibility.FULLY_VISIBLE )
            {
                return BoxVisibility.FULLY_VISIBLE;
            }

            return BoxVisibility.PARTIALLY_VISIBLE;
        }

        /// <summary>
        /// Gets the visibility of the given bounding box regarding to a frustum plane.
        /// </summary>
        /// <param name="pClip">A frustum plane equation.</param>
        /// <param name="pBounds">The bounding box to test against the frustum plane equation.</param>
        /// <returns>The box visibility.</returns>
        public static BoxVisibility GetVisibility(Vector4<float> pClip, ABox3<float> pBounds)
        {
            Vector3<float> lMinimum = pBounds.Minimum as Vector3<float>;
            Vector3<float> lMaximum = pBounds.Maximum as Vector3<float>;
            double lX0 = lMinimum.X * pClip.X;
            double lX1 = lMaximum.X * pClip.X;
            double lY0 = lMinimum.Y * pClip.Y;
            double lY1 = lMaximum.Y * pClip.Y;
            double lZ0 = lMinimum.Z * pClip.Z + pClip.W;
            double lZ1 = lMaximum.Z * pClip.Z + pClip.W;
            double lP1 = lX0 + lY0 + lZ0;
            double lP2 = lX1 + lY0 + lZ0;
            double lP3 = lX1 + lY1 + lZ0;
            double lP4 = lX0 + lY1 + lZ0;
            double lP5 = lX0 + lY0 + lZ1;
            double lP6 = lX1 + lY0 + lZ1;
            double lP7 = lX1 + lY1 + lZ1;
            double lP8 = lX0 + lY1 + lZ1;
            if ( lP1 <= 0 && 
                 lP2 <= 0 && 
                 lP3 <= 0 && 
                 lP4 <= 0 && 
                 lP5 <= 0 && 
                 lP6 <= 0 && 
                 lP7 <= 0 && 
                 lP8 <= 0 )
            {
                return BoxVisibility.INVISIBLE;
            }

            if ( lP1 > 0 && 
                 lP2 > 0 && 
                 lP3 > 0 && 
                 lP4 > 0 && 
                 lP5 > 0 && 
                 lP6 > 0 && 
                 lP7 > 0 && 
                 lP8 > 0 )
            {
                return BoxVisibility.FULLY_VISIBLE;
            }

            return BoxVisibility.PARTIALLY_VISIBLE;
        }

        /// <summary>
        /// Computes the scene node visibility.
        /// </summary>
        /// <param name="pNode">The scene node the visibility must be computed for.</param>
        /// <param name="pParentVisibility">The node's parent visibility.</param>
        private void ComputeVisibility(SceneNode pNode, BoxVisibility pParentVisibility)
        {
            if ( pParentVisibility == BoxVisibility.PARTIALLY_VISIBLE )
            {
                pParentVisibility = this.IsVisible( pNode.WorldBounds );
            }

            pNode.IsVisible = pParentVisibility != BoxVisibility.INVISIBLE;

            // Compute children visibility then.
            for ( int lCurr = 0; lCurr < pNode.ChildCount; lCurr++ )
            {
                this.ComputeVisibility( pNode[ new SceneNodeIndex( lCurr ) ] as SceneNode, pParentVisibility );
            }
        }

        /// <summary>
        /// Builds the scene nodes map cache.
        /// </summary>
        /// <param name="pRoot">The root node</param>
        private void BuildNodeMap(SceneNode pRoot)
        {
            foreach ( string lFlag in pRoot.Flags )
            {
                this.mNodesMap.Add( lFlag, pRoot );
            }

            // Add children too.
            for ( int lCurr = 0; lCurr < pRoot.ChildCount; lCurr++ )
            {
                this.BuildNodeMap( pRoot[ new SceneNodeIndex( lCurr ) ] as SceneNode );
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDispose()
        {
            if ( this.mRoot != null )
            {
                this.mRoot.Dispose();
                this.mRoot = null;
            }

            if ( this.mScheduler != null )
            {
                this.mScheduler.Dispose();
            }

            this.mNodesMap.Clear();
            this.mNodeVariables.Clear();
            this.mCurrentPass = null;
            this.mCurrentTask = null;
            this.mCurrentFrameBuffer = null;
        }
        
        #endregion Methods IDisposable

        #endregion Methods
    }
}
