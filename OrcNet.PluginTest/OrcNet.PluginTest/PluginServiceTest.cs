﻿using OrcNet.Core.Plugins;
using OrcNet.Plugins.Services;

namespace OrcNet.PluginTest
{
    /// <summary>
    /// Definition of the <see cref="PluginServiceTest"/> class.
    /// </summary>
    public class PluginServiceTest : APluginService
    {
        #region Properties

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "PluginServiceTest";
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginServiceTest"/> class.
        /// </summary>
        /// <param name="pOwner">The service's owner.</param>
        public PluginServiceTest(IPlugin pOwner) :
        base( pOwner )
        {

        }

        #endregion Constructor
    }
}
