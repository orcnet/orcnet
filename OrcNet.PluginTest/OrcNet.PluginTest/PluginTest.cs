﻿using OrcNet.Plugins;

namespace OrcNet.PluginTest
{
    /// <summary>
    /// Definition of the <see cref="PluginTest"/> class.
    /// </summary>
    public class PluginTest : APlugin
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginTest"/> class.
        /// </summary>
        /// <param name="pAssemblyPath">The assembly path.</param>
        public PluginTest(string pAssemblyPath = null) :
        base( pAssemblyPath )
        {
            
        }

        #endregion 
    }
}
