﻿namespace OrcNet.Core
{
    /// <summary>
    /// Definition of the <see cref="CoreModule"/> class.
    /// </summary>
    internal sealed class CoreModule : AModule
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CoreModule"/> class.
        /// </summary>
        public CoreModule()
        {

        }

        #endregion Constructor
    }
}
