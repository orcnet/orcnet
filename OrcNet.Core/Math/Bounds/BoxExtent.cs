﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Enumeration of the box extent(s)
    /// </summary>
    public enum BoxExtent
    {
        /// <summary>
        /// The box is Zero
        /// </summary>
        NULL = 0,

        /// <summary>
        /// The box is Finite, that is, neither Zero nor Infinite.
        /// </summary>
        FINITE,

        /// <summary>
        /// The box is Infinite.
        /// </summary>
        INFINITE
    }
}
