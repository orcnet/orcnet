﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Box2H"/> class.
    /// </summary>
    public class Box2H : ABox2<Half>
    {
        #region Properties

        #region Properties ABox2

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public override Half Width
        {
            get
            {
                Vector2H lExtents = this.Extents as Vector2H;
                return lExtents.X;
            }
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public override Half Height
        {
            get
            {
                Vector2H lExtents = this.Extents as Vector2H;
                return lExtents.Y;
            }
        }

        /// <summary>
        /// Gets the Box's area.
        /// </summary>
        public override Half Area
        {
            get
            {
                Vector2H lExtents = this.Extents as Vector2H;
                return lExtents.X * lExtents.Y;
            }
        }

        #endregion Properties ABox2

        #region Properties IBox

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public override IVector<Half> Extents
        {
            get
            {
                Vector2H lMinimum = this.mMinimum as Vector2H;
                Vector2H lMaximum = this.mMaximum as Vector2H;
                return new Vector2H( lMaximum.X - lMinimum.X, lMaximum.Y - lMinimum.Y );
            }
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public override IVector<Half> Center
        {
            get
            {
                Vector2H lMinimum = this.mMinimum as Vector2H;
                Vector2H lMaximum = this.mMaximum as Vector2H;
                return new Vector2H( ( lMinimum.X + lMaximum.X ) * new Half( 0.5f ),
                                     ( lMinimum.Y + lMaximum.Y ) * new Half( 0.5f ) );
            }
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Box2H"/> class.
        /// </summary>
        static Box2H()
        {
            sCasters.Add( typeof(Half), CastToHalf );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2H"/> class.
        /// </summary>
        public Box2H() :
        this( new Half( float.PositiveInfinity ), new Half( float.NegativeInfinity ),
              new Half( float.PositiveInfinity ), new Half( float.NegativeInfinity ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2H"/> class.
        /// </summary>
        /// <param name="pMinX">The minimum X coordinate (Equals Left)</param>
        /// <param name="pMaxX">The maximum X coordinate (Equals Right)</param>
        /// <param name="pMinY">The minimum Y coordinate (Equals Bottom)</param>
        /// <param name="pMaxY">The maximum Y coordinate (Equals Top)</param>
        public Box2H(Half pMinX, Half pMaxX, Half pMinY, Half pMaxY) :
        this( new Vector2H( pMinX, pMinY ), new Vector2H( pMaxX, pMaxY ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2H"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        public Box2H(Vector2H pMinimum, Vector2H pMaximum) :
        base( pMinimum, pMaximum )
        {
            // If one of the component is Nan or Infinite, it makes
            // the Box Infinite.
            if ( pMinimum.X.IsNan() || 
                 pMinimum.Y.IsNan() || 
                 pMaximum.X.IsNan() || 
                 pMaximum.Y.IsNan() )
            {
                this.mExtent = BoxExtent.INFINITE;
            }

            if ( pMinimum.X.IsInfinite() ||
                 pMinimum.Y.IsInfinite() ||
                 pMaximum.X.IsInfinite() ||
                 pMaximum.Y.IsInfinite() )
            {
                this.mExtent = BoxExtent.INFINITE;
            }
        }

        #endregion Constructors

        #region Methods

        #region Methods ICloneable

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected override ABox<Half> InternalClone()
        {
            return new Box2H( this.mMinimum.Clone() as Vector2H, 
                              this.mMaximum.Clone() as Vector2H );
        }

        #endregion Methods ICloneable

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public override IBox<Half> Enlarge(Half pExtentToAdd)
        {
            Vector2H lMinimum = this.mMinimum as Vector2H;
            Vector2H lMaximum = this.mMaximum as Vector2H;
            return new Box2H( new Vector2H( lMinimum.X - pExtentToAdd, lMinimum.Y - pExtentToAdd ), 
                              new Vector2H( lMaximum.X + pExtentToAdd, lMaximum.Y + pExtentToAdd ) );
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public override IBox<Half> Enlarge(IVector<Half> pPoint)
        {
            Vector2H lPoint = pPoint as Vector2H;
            if ( lPoint != null )
            {
                Vector2H lMinimum = this.mMinimum as Vector2H;
                Vector2H lMaximum = this.mMaximum as Vector2H;
                return new Box2H( new Vector2H( new Half( System.Math.Min( lPoint.X, lMinimum.X ) ), new Half( System.Math.Min( lPoint.Y, lMinimum.Y ) ) ), 
                                  new Vector2H( new Half( System.Math.Max( lPoint.X, lMaximum.X ) ), new Half( System.Math.Max( lPoint.Y, lMaximum.Y ) ) ) );
            }

            // Default answer in case of vector type mismatches.
            return this.Clone() as IBox<Half>;
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected override ABox<Half> InternalMerge(ABox<Half> pToAdd)
        {
            Box2H lOther = pToAdd as Box2H;
            if ( lOther != null )
            {
                Vector2H lMinimum = this.mMinimum as Vector2H;
                Vector2H lMaximum = this.mMaximum as Vector2H;
                Vector2H lOtherMinimum = lOther.mMinimum as Vector2H;
                Vector2H lOtherMaximum = lOther.mMaximum as Vector2H;
                return new Box2H( new Vector2H( new Half( System.Math.Min( lOtherMinimum.X, lMinimum.X ) ), new Half( System.Math.Min( lOtherMinimum.Y, lMinimum.Y ) ) ), 
                                  new Vector2H( new Half( System.Math.Max( lOtherMaximum.X, lMaximum.X ) ), new Half( System.Math.Max( lOtherMaximum.Y, lMaximum.Y ) ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as ABox<Half>;
        }

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public override bool Contains(IVector<Half> pPoint)
        {
            Vector2H lPoint = pPoint as Vector2H;
            if ( lPoint != null )
            {
                Vector2H lMinimum = this.mMinimum as Vector2H;
                Vector2H lMaximum = this.mMaximum as Vector2H;
                return lPoint.X >= lMinimum.X &&
                       lPoint.X <= lMaximum.X &&
                       lPoint.Y >= lMinimum.Y &&
                       lPoint.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public override bool Contains(IBox<Half> pBox)
        {
            Box2H lOther = pBox as Box2H;
            if ( lOther != null )
            {
                Vector2H lMinimum = this.mMinimum as Vector2H;
                Vector2H lMaximum = this.mMaximum as Vector2H;
                Vector2H lOtherMinimum = lOther.mMinimum as Vector2H;
                Vector2H lOtherMaximum = lOther.mMaximum as Vector2H;
                return lOtherMinimum.X >= lMinimum.X &&
                       lOtherMaximum.X <= lMaximum.X &&
                       lOtherMinimum.Y >= lMinimum.Y &&
                       lOtherMaximum.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public override bool Intersects(IBox<Half> pToTest)
        {
            Box2H lOther = pToTest as Box2H;
            if ( lOther != null )
            {
                Vector2H lMinimum = this.mMinimum as Vector2H;
                Vector2H lMaximum = this.mMaximum as Vector2H;
                Vector2H lOtherMinimum = lOther.mMinimum as Vector2H;
                Vector2H lOtherMaximum = lOther.mMaximum as Vector2H;
                return lOtherMaximum.X >= lMinimum.X &&
                       lOtherMinimum.X <= lMaximum.X &&
                       lOtherMaximum.Y >= lMinimum.Y &&
                       lOtherMinimum.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public override IVector<Half> GetNearestBoxPoint(IVector<Half> pPoint)
        {
            Vector2H lMinimum = this.mMinimum as Vector2H;
            Vector2H lMaximum = this.mMaximum as Vector2H;

            Vector2H lNearest = pPoint.Clone() as Vector2H;
            if ( lNearest.X < lMinimum.X )
            {
                lNearest.X = lMinimum.X;
            }
            else if ( lNearest.X > lMaximum.X )
            {
                lNearest.X = lMaximum.X;
            }

            if ( lNearest.Y < lMinimum.Y )
            {
                lNearest.Y = lMinimum.Y;
            }
            else if ( lNearest.Y > lMaximum.Y )
            {
                lNearest.Y = lMaximum.Y;
            }

            return lNearest;
        }

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public override Half DistanceTo(IVector<Half> pPoint)
        {
            return new Half( System.Math.Sqrt( this.DistanceSqTo( pPoint ) ) );
        }

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public override Half DistanceSqTo(IVector<Half> pPoint)
        {
            Vector2H lPoint = pPoint as Vector2H;
            AVector<Half> lNearest = this.GetNearestBoxPoint( lPoint ) as AVector<Half>;
            return (lPoint - lNearest).LengthSq();
        }
        
        #endregion Methods IBox

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToHalf(object pToCast)
        {
            if ( pToCast is Box2H )
            {
                return (pToCast as ICloneable).Clone();
            }
            else if ( pToCast is Box2D )
            {
                Box2D lCast = pToCast as Box2D;
                return new Box2H( lCast.Minimum.Cast<Half>() as Vector2H, 
                                  lCast.Maximum.Cast<Half>() as Vector2H );
            }
            else if ( pToCast is Box2I )
            {
                Box2I lCast = pToCast as Box2I;
                return new Box2H( lCast.Minimum.Cast<Half>() as Vector2H, 
                                  lCast.Maximum.Cast<Half>() as Vector2H );
            }
            else if ( pToCast is Box2F )
            {
                Box2F lCast = pToCast as Box2F;
                return new Box2H( lCast.Minimum.Cast<Half>() as Vector2H, 
                                  lCast.Maximum.Cast<Half>() as Vector2H );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
