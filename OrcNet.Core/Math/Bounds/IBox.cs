﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="IBox{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public interface IBox<T> : IEquatable<IBox<T>>, ICloneable where T : struct
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the Box is Null or not.
        /// </summary>
        bool IsNull
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the Box is Finite or not.
        /// </summary>
        bool IsFinite
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the Box is Infinite or not.
        /// </summary>
        bool IsInfinite
        {
            get;
        }

        /// <summary>
        /// Gets the box's minimum.
        /// </summary>
        IVector<T> Minimum
        {
            get;
        }

        /// <summary>
        /// Gets the box's maximum.
        /// </summary>
        IVector<T> Maximum
        {
            get;
        }

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        IVector<T> Extents
        {
            get;
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        IVector<T> Center
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        IBox<T> Enlarge(T pExtentToAdd);

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        IBox<T> Enlarge(IVector<T> pPoint);

        /// <summary>
        /// Merges this Box with a supplied Box
        /// </summary>
        /// <param name="pToAdd">The Box to merge with this Box</param>
        /// <returns>A copy of this Box merged with the supplied Box</returns>
        IBox<T> Merge(IBox<T> pToAdd);

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        bool Contains(IVector<T> pPoint);

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        bool Contains(IBox<T> pBox);

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        bool Intersects(IBox<T> pToTest);

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        IVector<T> GetNearestBoxPoint(IVector<T> pPoint);

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        T DistanceTo(IVector<T> pPoint);

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        T DistanceSqTo(IVector<T> pPoint);

        /// <summary>
        /// Turns this Box into another Box of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new Box of another primitive type.</returns>
        IBox<OtherT> Cast<OtherT>() where OtherT : struct;

        #endregion Methods
    }
}
