﻿using System;
using System.Collections.Generic;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the abstract <see cref="ABox2{T}"/> class.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public abstract class ABox2<T> : ABox<T> where T : struct
    {
        #region Fields

        /// <summary>
        /// Stores the set of cast delegate for the box cast method.
        /// </summary>
        protected static Dictionary<Type, CastDelegate> sCasters;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public abstract T Width
        {
            get;
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public abstract T Height
        {
            get;
        }

        /// <summary>
        /// Gets the Box's area.
        /// </summary>
        public abstract T Area
        {
            get;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="ABox2{T}"/> class.
        /// </summary>
        static ABox2()
        {
            sCasters = new Dictionary<Type, CastDelegate>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ABox2{T}"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        protected ABox2(Vector2<T> pMinimum, Vector2<T> pMaximum) :
        base( pMinimum, pMaximum )
        {
            
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether a supplied Box can be merge with this Box or not. 
        /// </summary>
        /// <param name="pOther">The box to merge this box with</param>
        /// <returns>True if the supplied Box can be merged with this Box, false otherwise.</returns>
        protected override bool CanMerge(ABox<T> pOther)
        {
            // Default Can merge with allows only 2D with 2D
            return pOther is ABox2<T>;
        }

        /// <summary>
        /// Turns this Box into another Box of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new Box of another primitive type.</returns>
        public override IBox<OtherT> Cast<OtherT>()
        {
            CastDelegate lCaster = null;
            if ( sCasters.TryGetValue( typeof(OtherT), out lCaster ) )
            {
                return lCaster( this ) as IBox<OtherT>;
            }

            return null;
        }

        #endregion Methods
    }
}
