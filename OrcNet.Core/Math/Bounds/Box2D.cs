﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Box2D"/> class.
    /// </summary>
    public class Box2D : ABox2<double>
    {
        #region Properties

        #region Properties ABox2

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public override double Width
        {
            get
            {
                Vector2D lExtents = this.Extents as Vector2D;
                return lExtents.X;
            }
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public override double Height
        {
            get
            {
                Vector2D lExtents = this.Extents as Vector2D;
                return lExtents.Y;
            }
        }

        /// <summary>
        /// Gets the Box's area.
        /// </summary>
        public override double Area
        {
            get
            {
                Vector2D lExtents = this.Extents as Vector2D;
                return lExtents.X * lExtents.Y;
            }
        }

        #endregion Properties ABox2

        #region Properties IBox

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public override IVector<double> Extents
        {
            get
            {
                Vector2D lMinimum = this.mMinimum as Vector2D;
                Vector2D lMaximum = this.mMaximum as Vector2D;
                return new Vector2D( lMaximum.X - lMinimum.X, lMaximum.Y - lMinimum.Y );
            }
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public override IVector<double> Center
        {
            get
            {
                Vector2D lMinimum = this.mMinimum as Vector2D;
                Vector2D lMaximum = this.mMaximum as Vector2D;
                return new Vector2D( ( lMinimum.X + lMaximum.X ) * 0.5f,
                                     ( lMinimum.Y + lMaximum.Y ) * 0.5f );
            }
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Box2D"/> class.
        /// </summary>
        static Box2D()
        {
            sCasters.Add( typeof(double), CastToDouble );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2D"/> class.
        /// </summary>
        public Box2D() :
        this( double.PositiveInfinity, double.NegativeInfinity,
              double.PositiveInfinity, double.NegativeInfinity )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2D"/> class.
        /// </summary>
        /// <param name="pMinX">The minimum X coordinate (Equals Left)</param>
        /// <param name="pMaxX">The maximum X coordinate (Equals Right)</param>
        /// <param name="pMinY">The minimum Y coordinate (Equals Bottom)</param>
        /// <param name="pMaxY">The maximum Y coordinate (Equals Top)</param>
        public Box2D(double pMinX, double pMaxX, double pMinY, double pMaxY) :
        this( new Vector2D( pMinX, pMinY ), new Vector2D( pMaxX, pMaxY ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2D"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        public Box2D(Vector2D pMinimum, Vector2D pMaximum) :
        base( pMinimum, pMaximum )
        {
            // If one of the component is Nan or Infinite, it makes
            // the Box Infinite.
            if ( double.IsNaN( pMinimum.X ) || 
                 double.IsNaN( pMinimum.Y ) || 
                 double.IsNaN( pMaximum.X ) || 
                 double.IsNaN( pMaximum.Y ) )
            {
                this.mExtent = BoxExtent.INFINITE;
            }

            if ( double.IsInfinity( pMinimum.X ) ||
                 double.IsInfinity( pMinimum.Y ) ||
                 double.IsInfinity( pMaximum.X ) ||
                 double.IsInfinity( pMaximum.Y ) )
            {
                this.mExtent = BoxExtent.INFINITE;
            }
        }

        #endregion Constructors

        #region Methods

        #region Methods ICloneable

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected override ABox<double> InternalClone()
        {
            return new Box2D( this.mMinimum.Clone() as Vector2D, 
                              this.mMaximum.Clone() as Vector2D );
        }

        #endregion Methods ICloneable

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public override IBox<double> Enlarge(double pExtentToAdd)
        {
            Vector2D lMinimum = this.mMinimum as Vector2D;
            Vector2D lMaximum = this.mMaximum as Vector2D;
            return new Box2D( new Vector2D( lMinimum.X - pExtentToAdd, lMinimum.Y - pExtentToAdd ), 
                              new Vector2D( lMaximum.X + pExtentToAdd, lMaximum.Y + pExtentToAdd ) );
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public override IBox<double> Enlarge(IVector<double> pPoint)
        {
            Vector2D lPoint = pPoint as Vector2D;
            if ( lPoint != null )
            {
                Vector2D lMinimum = this.mMinimum as Vector2D;
                Vector2D lMaximum = this.mMaximum as Vector2D;
                return new Box2D( new Vector2D( System.Math.Min( lPoint.X, lMinimum.X ), System.Math.Min( lPoint.Y, lMinimum.Y ) ), 
                                  new Vector2D( System.Math.Max( lPoint.X, lMaximum.X ), System.Math.Max( lPoint.Y, lMaximum.Y ) ) );
            }

            // Default answer in case of vector type mismatches.
            return this.Clone() as IBox<double>;
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected override ABox<double> InternalMerge(ABox<double> pToAdd)
        {
            Box2D lOther = pToAdd as Box2D;
            if ( lOther != null )
            {
                Vector2D lMinimum = this.mMinimum as Vector2D;
                Vector2D lMaximum = this.mMaximum as Vector2D;
                Vector2D lOtherMinimum = lOther.mMinimum as Vector2D;
                Vector2D lOtherMaximum = lOther.mMaximum as Vector2D;
                return new Box2D( new Vector2D( System.Math.Min( lOtherMinimum.X, lMinimum.X ), System.Math.Min( lOtherMinimum.Y, lMinimum.Y ) ), 
                                  new Vector2D( System.Math.Max( lOtherMaximum.X, lMaximum.X ), System.Math.Max( lOtherMaximum.Y, lMaximum.Y ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as ABox<double>;
        }

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public override bool Contains(IVector<double> pPoint)
        {
            Vector2D lPoint = pPoint as Vector2D;
            if ( lPoint != null )
            {
                Vector2D lMinimum = this.mMinimum as Vector2D;
                Vector2D lMaximum = this.mMaximum as Vector2D;
                return lPoint.X >= lMinimum.X &&
                       lPoint.X <= lMaximum.X &&
                       lPoint.Y >= lMinimum.Y &&
                       lPoint.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public override bool Contains(IBox<double> pBox)
        {
            Box2D lOther = pBox as Box2D;
            if ( lOther != null )
            {
                Vector2D lMinimum = this.mMinimum as Vector2D;
                Vector2D lMaximum = this.mMaximum as Vector2D;
                Vector2D lOtherMinimum = lOther.mMinimum as Vector2D;
                Vector2D lOtherMaximum = lOther.mMaximum as Vector2D;
                return lOtherMinimum.X >= lMinimum.X &&
                       lOtherMaximum.X <= lMaximum.X &&
                       lOtherMinimum.Y >= lMinimum.Y &&
                       lOtherMaximum.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public override bool Intersects(IBox<double> pToTest)
        {
            Box2D lOther = pToTest as Box2D;
            if ( lOther != null )
            {
                Vector2D lMinimum = this.mMinimum as Vector2D;
                Vector2D lMaximum = this.mMaximum as Vector2D;
                Vector2D lOtherMinimum = lOther.mMinimum as Vector2D;
                Vector2D lOtherMaximum = lOther.mMaximum as Vector2D;
                return lOtherMaximum.X >= lMinimum.X &&
                       lOtherMinimum.X <= lMaximum.X &&
                       lOtherMaximum.Y >= lMinimum.Y &&
                       lOtherMinimum.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public override IVector<double> GetNearestBoxPoint(IVector<double> pPoint)
        {
            Vector2D lMinimum = this.mMinimum as Vector2D;
            Vector2D lMaximum = this.mMaximum as Vector2D;

            Vector2D lNearest = pPoint.Clone() as Vector2D;
            if ( lNearest.X < lMinimum.X )
            {
                lNearest.X = lMinimum.X;
            }
            else if ( lNearest.X > lMaximum.X )
            {
                lNearest.X = lMaximum.X;
            }

            if ( lNearest.Y < lMinimum.Y )
            {
                lNearest.Y = lMinimum.Y;
            }
            else if ( lNearest.Y > lMaximum.Y )
            {
                lNearest.Y = lMaximum.Y;
            }

            return lNearest;
        }

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public override double DistanceTo(IVector<double> pPoint)
        {
            return System.Math.Sqrt( this.DistanceSqTo( pPoint ) );
        }

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public override double DistanceSqTo(IVector<double> pPoint)
        {
            Vector2D lPoint = pPoint as Vector2D;
            AVector<double> lNearest = this.GetNearestBoxPoint( lPoint ) as AVector<double>;
            return (lPoint - lNearest).LengthSq();
        }
        
        #endregion Methods IBox

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToDouble(object pToCast)
        {
            if ( pToCast is Box2D )
            {
                return (pToCast as ICloneable).Clone();
            }
            else if ( pToCast is Box2F )
            {
                Box2F lCast = pToCast as Box2F;
                return new Box2D( lCast.Minimum.Cast<double>() as Vector2D, 
                                  lCast.Maximum.Cast<double>() as Vector2D );
            }
            else if ( pToCast is Box2I )
            {
                Box2I lCast = pToCast as Box2I;
                return new Box2D( lCast.Minimum.Cast<double>() as Vector2D, 
                                  lCast.Maximum.Cast<double>() as Vector2D );
            }
            else if ( pToCast is Box2H )
            {
                Box2H lCast = pToCast as Box2H;
                return new Box2D( lCast.Minimum.Cast<double>() as Vector2D, 
                                  lCast.Maximum.Cast<double>() as Vector2D );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
