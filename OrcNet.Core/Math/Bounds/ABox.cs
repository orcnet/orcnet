﻿using System.Text;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the abstract <see cref="ABox{T}"/> class.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public abstract class ABox<T> : AMemoryProfilable, IBox<T> where T : struct
    {
        #region Delegates

        /// <summary>
        /// Delegate prototype definition of method casting a vector from one primitive type to another.
        /// </summary>
        /// <param name="pToCast">The vector to cast.</param>
        /// <returns>The casted vector.</returns>
        protected delegate object CastDelegate(object pToCast);

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the box extent state
        /// </summary>
        protected BoxExtent mExtent;

        /// <summary>
        /// Stores the box's minimum.
        /// </summary>
        protected AVector<T> mMinimum;

        /// <summary>
        /// Stores the box's maximum.
        /// </summary>
        protected AVector<T> mMaximum;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += this.mMinimum.Size;
                lSize += this.mMaximum.Size;
                lSize += sizeof(BoxExtent);
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IBox

        /// <summary>
        /// Gets the flag indicating whether the Box is Null or not.
        /// </summary>
        public bool IsNull
        {
            get
            {
                return this.mExtent == BoxExtent.NULL;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Box is Finite or not.
        /// </summary>
        public bool IsFinite
        {
            get
            {
                return this.mExtent == BoxExtent.FINITE;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Box is Infinite or not.
        /// </summary>
        public bool IsInfinite
        {
            get
            {
                return this.mExtent == BoxExtent.INFINITE;
            }
        }

        /// <summary>
        /// Gets the box's minimum.
        /// </summary>
        public IVector<T> Minimum
        {
            get
            {
                return this.mMinimum.Clone() as IVector<T>;
            }
        }

        /// <summary>
        /// Gets the box's maximum.
        /// </summary>
        public IVector<T> Maximum
        {
            get
            {
                return this.mMaximum.Clone() as IVector<T>;
            }
        }

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public abstract IVector<T> Extents
        {
            get;
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public abstract IVector<T> Center
        {
            get;
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ABox{T}"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        protected ABox(AVector<T> pMinimum, AVector<T> pMaximum)
        {
            this.mMinimum = pMinimum;
            this.mMaximum = pMaximum;
            this.mExtent  = BoxExtent.FINITE;
        }

        #endregion Constructor

        #region Methods

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public abstract IBox<T> Enlarge(T pExtentToAdd);

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public abstract IBox<T> Enlarge(IVector<T> pPoint);

        /// <summary>
        /// Checks whether a supplied Box can be merge with this Box or not. 
        /// </summary>
        /// <param name="pOther">The box to merge this box with</param>
        /// <returns>True if the supplied Box can be merged with this Box, false otherwise.</returns>
        protected abstract bool CanMerge(ABox<T> pOther);

        /// <summary>
        /// Merges this Box with a supplied Box
        /// </summary>
        /// <param name="pToAdd">The Box to merge with this Box</param>
        /// <returns>A copy of this Box merged with the supplied Box</returns>
        public IBox<T> Merge(IBox<T> pToAdd)
        {
            ABox<T> lToAdd = pToAdd as ABox<T>;
            if ( lToAdd == null ||
                 this.CanMerge( lToAdd ) == false )
            {
                return this.Clone() as IBox<T>;
            }

            if ( lToAdd.IsNull )
            {
                return this.Clone() as IBox<T>;
            }

            if ( lToAdd.IsInfinite )
            {
                return lToAdd.Clone() as IBox<T>;
            }
            
            return this.InternalMerge( lToAdd );
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected abstract ABox<T> InternalMerge(ABox<T> pToAdd);

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public abstract bool Contains(IVector<T> pPoint);

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public abstract bool Contains(IBox<T> pBox);

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public abstract bool Intersects(IBox<T> pToTest);

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public abstract IVector<T> GetNearestBoxPoint(IVector<T> pPoint);

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public abstract T DistanceTo(IVector<T> pPoint);

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public abstract T DistanceSqTo(IVector<T> pPoint);

        /// <summary>
        /// Turns this Box into another Box of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new Box of another primitive type.</returns>
        public abstract IBox<OtherT> Cast<OtherT>() where OtherT : struct;

        #endregion Methods IBox

        #region Methods Statics

        /// <summary>
        /// Extends this Box by the supplied extent
        /// </summary>
        /// <param name="pBox">The box to extent.</param>
        /// <param name="pExtent">The extent to apply.</param>
        /// <returns>The extended box.</returns>
        public static ABox<T> operator + (ABox<T> pBox, T pExtent)
        {
            return pBox.Enlarge( pExtent ) as ABox<T>;
        }

        /// <summary>
        /// Extends this Box by the supplied extent
        /// </summary>
        /// <param name="pBox">The box to extent.</param>
        /// <param name="pExtent">The extent to apply.</param>
        /// <returns>The extended box.</returns>
        public static ABox<T> operator + (T pExtent, ABox<T> pBox)
        {
            return pBox.Enlarge( pExtent ) as ABox<T>;
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pBox">The box to extent.</param>
        /// <param name="pPoint">The point to encapsulate.</param>
        /// <returns>The extended box.</returns>
        public static ABox<T> operator + (ABox<T> pBox, AVector<T> pPoint)
        {
            return pBox.Enlarge( pPoint ) as ABox<T>;
        }

        /// <summary>
        ///  Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pBox">The box to extent.</param>
        /// <param name="pPoint">The point to encapsulate.</param>
        /// <returns>The extended box.</returns>
        public static ABox<T> operator + (AVector<T> pPoint, ABox<T> pBox)
        {
            return pBox.Enlarge( pPoint ) as ABox<T>;
        }

        /// <summary>
        /// Merges the two boxes together.
        /// </summary>
        /// <param name="pFirst">The first box to merge.</param>
        /// <param name="pSecond">The second box to merge.</param>
        /// <returns>The Box containing the two boxes.</returns>
        public static ABox<T> operator + (ABox<T> pFirst, ABox<T> pSecond)
        {
            return pFirst.Merge( pSecond ) as ABox<T>;
        }

        #endregion Methods Statics

        #region Methods Equality

        /// <summary>
        /// Compares the specified instances for equality.
        /// </summary>
        /// <param name="pFirst">The first box to test.</param>
        /// <param name="pSecond">The second box to test.</param>
        /// <returns>True if both instances are equal, false otherwise.</returns>
        public static bool operator == (ABox<T> pFirst, ABox<T> pSecond)
        {
            bool lIsFirstNull  = object.ReferenceEquals( pFirst, null );
            bool lIsSecondNull = object.ReferenceEquals( pSecond, null );
            if( lIsFirstNull && lIsSecondNull ) // If both null, equal.
            {
                return true;
            }
            else if ( lIsFirstNull || lIsSecondNull ) // If one of the two is null, not equal.
            {
                return false;
            }

            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Compares the specified instances for inequality.
        /// </summary>
        /// <param name="pFirst">The first box to test.</param>
        /// <param name="pSecond">The second box to test.</param>
        /// <returns>True if both instances are not equal, false otherwise.</returns>
        public static bool operator != (ABox<T> pFirst, ABox<T> pSecond)
        {
            bool lIsFirstNull  = object.ReferenceEquals( pFirst, null );
            bool lIsSecondNull = object.ReferenceEquals( pSecond, null );
            if( lIsFirstNull && lIsSecondNull ) // If both null, equal.
            {
                return false;
            }
            else if ( lIsFirstNull || lIsSecondNull ) // If one of the two is null, not equal.
            {
                return true;
            }

            return pFirst.Equals( pSecond ) == false;
        }

        /// <summary>
        /// Checks whether this Box is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other Box to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            IBox<T> lOther = pOther as IBox<T>;
            if ( lOther != null )
            {
                return this.Equals( lOther );
            }

            return false;
        }

        /// <summary>
        /// Checks whether this Box is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other Box to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(IBox<T> pOther)
        {
            ABox<T> lOther = pOther as ABox<T>;
            if ( lOther != null )
            {
                return this.mMinimum.Equals( lOther.mMinimum ) &&
                       this.mMaximum.Equals( lOther.mMaximum );
            }

            return false;
        }
        
        /// <summary>
        /// Gets the Box's hash code.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return this.mMinimum.GetHashCode() ^ this.mMaximum.GetHashCode() ^ this.mExtent.GetHashCode();
        }

        #endregion Methods Equality

        #region Methods ICloneable

        /// <summary>
        /// Clone this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        public object Clone()
        {
            return this.InternalClone();
        }

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected abstract ABox<T> InternalClone();

        #endregion Methods ICloneable

        #region Methods ToString

        /// <summary>
        /// Turns the Box into a string.
        /// </summary>
        /// <returns>The string representation of this Box.</returns>
        public override string ToString()
        {
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.Append( "Min : " );
            lBuilder.AppendLine( this.mMinimum.ToString() );
            lBuilder.Append( "Max : " );
            lBuilder.AppendLine( this.mMaximum.ToString() );
            lBuilder.Append( "Extent : " );
            lBuilder.AppendLine( this.mExtent.ToString() );

            return lBuilder.ToString();
        }

        #endregion Methods ToString

        #endregion Methods
    }
}
