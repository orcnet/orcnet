﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Box2I"/> class.
    /// </summary>
    public class Box2I : ABox2<int>
    {
        #region Properties

        #region Properties ABox2

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public override int Width
        {
            get
            {
                Vector2I lExtents = this.Extents as Vector2I;
                return lExtents.X;
            }
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public override int Height
        {
            get
            {
                Vector2I lExtents = this.Extents as Vector2I;
                return lExtents.Y;
            }
        }

        /// <summary>
        /// Gets the Box's area.
        /// </summary>
        public override int Area
        {
            get
            {
                Vector2I lExtents = this.Extents as Vector2I;
                return lExtents.X * lExtents.Y;
            }
        }

        #endregion Properties ABox2

        #region Properties IBox

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public override IVector<int> Extents
        {
            get
            {
                Vector2I lMinimum = this.mMinimum as Vector2I;
                Vector2I lMaximum = this.mMaximum as Vector2I;
                return new Vector2I( lMaximum.X - lMinimum.X, lMaximum.Y - lMinimum.Y );
            }
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public override IVector<int> Center
        {
            get
            {
                Vector2I lMinimum = this.mMinimum as Vector2I;
                Vector2I lMaximum = this.mMaximum as Vector2I;
                return new Vector2I( ( lMinimum.X + lMaximum.X ) / 2,
                                     ( lMinimum.Y + lMaximum.Y ) / 2 );
            }
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Box2I"/> class.
        /// </summary>
        static Box2I()
        {
            sCasters.Add( typeof(int), CastToInt );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2I"/> class.
        /// </summary>
        public Box2I() :
        this( int.MaxValue, int.MinValue,
              int.MaxValue, int.MinValue )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2I"/> class.
        /// </summary>
        /// <param name="pMinX">The minimum X coordinate (Equals Left)</param>
        /// <param name="pMaxX">The maximum X coordinate (Equals Right)</param>
        /// <param name="pMinY">The minimum Y coordinate (Equals Bottom)</param>
        /// <param name="pMaxY">The maximum Y coordinate (Equals Top)</param>
        public Box2I(int pMinX, int pMaxX, int pMinY, int pMaxY) :
        this( new Vector2I( pMinX, pMinY ), new Vector2I( pMaxX, pMaxY ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2I"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        public Box2I(Vector2I pMinimum, Vector2I pMaximum) :
        base( pMinimum, pMaximum )
        {
            // Integers cannot be Nan or infinite.
        }

        #endregion Constructors

        #region Methods

        #region Methods ICloneable

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected override ABox<int> InternalClone()
        {
            return new Box2I( this.mMinimum.Clone() as Vector2I, 
                              this.mMaximum.Clone() as Vector2I );
        }

        #endregion Methods ICloneable

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public override IBox<int> Enlarge(int pExtentToAdd)
        {
            Vector2I lMinimum = this.mMinimum as Vector2I;
            Vector2I lMaximum = this.mMaximum as Vector2I;
            return new Box2I( new Vector2I( lMinimum.X - pExtentToAdd, lMinimum.Y - pExtentToAdd ), 
                              new Vector2I( lMaximum.X + pExtentToAdd, lMaximum.Y + pExtentToAdd ) );
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public override IBox<int> Enlarge(IVector<int> pPoint)
        {
            Vector2I lPoint = pPoint as Vector2I;
            if ( lPoint != null )
            {
                Vector2I lMinimum = this.mMinimum as Vector2I;
                Vector2I lMaximum = this.mMaximum as Vector2I;
                return new Box2I( new Vector2I( System.Math.Min( lPoint.X, lMinimum.X ), System.Math.Min( lPoint.Y, lMinimum.Y ) ), 
                                  new Vector2I( System.Math.Max( lPoint.X, lMaximum.X ), System.Math.Max( lPoint.Y, lMaximum.Y ) ) );
            }

            // Default answer in case of vector type mismatches.
            return this.Clone() as IBox<int>;
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected override ABox<int> InternalMerge(ABox<int> pToAdd)
        {
            Box2I lOther = pToAdd as Box2I;
            if ( lOther != null )
            {
                Vector2I lMinimum = this.mMinimum as Vector2I;
                Vector2I lMaximum = this.mMaximum as Vector2I;
                Vector2I lOtherMinimum = lOther.mMinimum as Vector2I;
                Vector2I lOtherMaximum = lOther.mMaximum as Vector2I;
                return new Box2I( new Vector2I( System.Math.Min( lOtherMinimum.X, lMinimum.X ), System.Math.Min( lOtherMinimum.Y, lMinimum.Y ) ), 
                                  new Vector2I( System.Math.Max( lOtherMaximum.X, lMaximum.X ), System.Math.Max( lOtherMaximum.Y, lMaximum.Y ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as ABox<int>;
        }

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public override bool Contains(IVector<int> pPoint)
        {
            Vector2I lPoint = pPoint as Vector2I;
            if ( lPoint != null )
            {
                Vector2I lMinimum = this.mMinimum as Vector2I;
                Vector2I lMaximum = this.mMaximum as Vector2I;
                return lPoint.X >= lMinimum.X &&
                       lPoint.X <= lMaximum.X &&
                       lPoint.Y >= lMinimum.Y &&
                       lPoint.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public override bool Contains(IBox<int> pBox)
        {
            Box2I lOther = pBox as Box2I;
            if ( lOther != null )
            {
                Vector2I lMinimum = this.mMinimum as Vector2I;
                Vector2I lMaximum = this.mMaximum as Vector2I;
                Vector2I lOtherMinimum = lOther.mMinimum as Vector2I;
                Vector2I lOtherMaximum = lOther.mMaximum as Vector2I;
                return lOtherMinimum.X >= lMinimum.X &&
                       lOtherMaximum.X <= lMaximum.X &&
                       lOtherMinimum.Y >= lMinimum.Y &&
                       lOtherMaximum.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public override bool Intersects(IBox<int> pToTest)
        {
            Box2I lOther = pToTest as Box2I;
            if ( lOther != null )
            {
                Vector2I lMinimum = this.mMinimum as Vector2I;
                Vector2I lMaximum = this.mMaximum as Vector2I;
                Vector2I lOtherMinimum = lOther.mMinimum as Vector2I;
                Vector2I lOtherMaximum = lOther.mMaximum as Vector2I;
                return lOtherMaximum.X >= lMinimum.X &&
                       lOtherMinimum.X <= lMaximum.X &&
                       lOtherMaximum.Y >= lMinimum.Y &&
                       lOtherMinimum.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public override IVector<int> GetNearestBoxPoint(IVector<int> pPoint)
        {
            Vector2I lMinimum = this.mMinimum as Vector2I;
            Vector2I lMaximum = this.mMaximum as Vector2I;

            Vector2I lNearest = pPoint.Clone() as Vector2I;
            if ( lNearest.X < lMinimum.X )
            {
                lNearest.X = lMinimum.X;
            }
            else if ( lNearest.X > lMaximum.X )
            {
                lNearest.X = lMaximum.X;
            }

            if ( lNearest.Y < lMinimum.Y )
            {
                lNearest.Y = lMinimum.Y;
            }
            else if ( lNearest.Y > lMaximum.Y )
            {
                lNearest.Y = lMaximum.Y;
            }

            return lNearest;
        }

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public override int DistanceTo(IVector<int> pPoint)
        {
            return (int)System.Math.Sqrt( (double)this.DistanceSqTo( pPoint ) );
        }

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public override int DistanceSqTo(IVector<int> pPoint)
        {
            Vector2I lPoint = pPoint as Vector2I;
            AVector<int> lNearest = this.GetNearestBoxPoint( lPoint ) as AVector<int>;
            return (lPoint - lNearest).LengthSq();
        }
        
        #endregion Methods IBox

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToInt(object pToCast)
        {
            if ( pToCast is Box2I )
            {
                return (pToCast as ICloneable).Clone();
            }
            else if ( pToCast is Box2D )
            {
                Box2D lCast = pToCast as Box2D;
                return new Box2I( lCast.Minimum.Cast<int>() as Vector2I, 
                                  lCast.Maximum.Cast<int>() as Vector2I );
            }
            else if ( pToCast is Box2F )
            {
                Box2F lCast = pToCast as Box2F;
                return new Box2I( lCast.Minimum.Cast<int>() as Vector2I, 
                                  lCast.Maximum.Cast<int>() as Vector2I );
            }
            else if ( pToCast is Box2H )
            {
                Box2H lCast = pToCast as Box2H;
                return new Box2I( lCast.Minimum.Cast<int>() as Vector2I, 
                                  lCast.Maximum.Cast<int>() as Vector2I );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
