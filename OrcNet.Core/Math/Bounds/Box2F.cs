﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Box2F"/> class.
    /// </summary>
    public class Box2F : ABox2<float>
    {
        #region Properties

        #region Properties ABox2

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public override float Width
        {
            get
            {
                Vector2F lExtents = this.Extents as Vector2F;
                return lExtents.X;
            }
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public override float Height
        {
            get
            {
                Vector2F lExtents = this.Extents as Vector2F;
                return lExtents.Y;
            }
        }

        /// <summary>
        /// Gets the Box's area.
        /// </summary>
        public override float Area
        {
            get
            {
                Vector2F lExtents = this.Extents as Vector2F;
                return lExtents.X * lExtents.Y;
            }
        }

        #endregion Properties ABox2

        #region Properties IBox

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public override IVector<float> Extents
        {
            get
            {
                Vector2F lMinimum = this.mMinimum as Vector2F;
                Vector2F lMaximum = this.mMaximum as Vector2F;
                return new Vector2F( lMaximum.X - lMinimum.X, lMaximum.Y - lMinimum.Y );
            }
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public override IVector<float> Center
        {
            get
            {
                Vector2F lMinimum = this.mMinimum as Vector2F;
                Vector2F lMaximum = this.mMaximum as Vector2F;
                return new Vector2F( ( lMinimum.X + lMaximum.X ) * 0.5f,
                                     ( lMinimum.Y + lMaximum.Y ) * 0.5f );
            }
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Box2F"/> class.
        /// </summary>
        static Box2F()
        {
            sCasters.Add( typeof(float), CastToFloat );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2F"/> class.
        /// </summary>
        public Box2F() :
        this( float.PositiveInfinity, float.NegativeInfinity,
              float.PositiveInfinity, float.NegativeInfinity )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2F"/> class.
        /// </summary>
        /// <param name="pMinX">The minimum X coordinate (Equals Left)</param>
        /// <param name="pMaxX">The maximum X coordinate (Equals Right)</param>
        /// <param name="pMinY">The minimum Y coordinate (Equals Bottom)</param>
        /// <param name="pMaxY">The maximum Y coordinate (Equals Top)</param>
        public Box2F(float pMinX, float pMaxX, float pMinY, float pMaxY) :
        this( new Vector2F( pMinX, pMinY ), new Vector2F( pMaxX, pMaxY ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box2F"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        public Box2F(Vector2F pMinimum, Vector2F pMaximum) :
        base( pMinimum, pMaximum )
        {
            // If one of the component is Nan or Infinite, it makes
            // the Box Infinite.
            if ( float.IsNaN( pMinimum.X ) || 
                 float.IsNaN( pMinimum.Y ) || 
                 float.IsNaN( pMaximum.X ) || 
                 float.IsNaN( pMaximum.Y ) )
            {
                this.mExtent = BoxExtent.INFINITE;
            }

            if ( float.IsInfinity( pMinimum.X ) ||
                 float.IsInfinity( pMinimum.Y ) ||
                 float.IsInfinity( pMaximum.X ) ||
                 float.IsInfinity( pMaximum.Y ) )
            {
                this.mExtent = BoxExtent.INFINITE;
            }
        }

        #endregion Constructors

        #region Methods

        #region Methods ICloneable

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected override ABox<float> InternalClone()
        {
            return new Box2F( this.mMinimum.Clone() as Vector2F, 
                              this.mMaximum.Clone() as Vector2F );
        }

        #endregion Methods ICloneable

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public override IBox<float> Enlarge(float pExtentToAdd)
        {
            Vector2F lMinimum = this.mMinimum as Vector2F;
            Vector2F lMaximum = this.mMaximum as Vector2F;
            return new Box2F( new Vector2F( lMinimum.X - pExtentToAdd, lMinimum.Y - pExtentToAdd ), 
                              new Vector2F( lMaximum.X + pExtentToAdd, lMaximum.Y + pExtentToAdd ) );
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public override IBox<float> Enlarge(IVector<float> pPoint)
        {
            Vector2F lPoint = pPoint as Vector2F;
            if ( lPoint != null )
            {
                Vector2F lMinimum = this.mMinimum as Vector2F;
                Vector2F lMaximum = this.mMaximum as Vector2F;
                return new Box2F( new Vector2F( System.Math.Min( lPoint.X, lMinimum.X ), System.Math.Min( lPoint.Y, lMinimum.Y ) ), 
                                  new Vector2F( System.Math.Max( lPoint.X, lMaximum.X ), System.Math.Max( lPoint.Y, lMaximum.Y ) ) );
            }

            // Default answer in case of vector type mismatches.
            return this.Clone() as IBox<float>;
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected override ABox<float> InternalMerge(ABox<float> pToAdd)
        {
            Box2F lOther = pToAdd as Box2F;
            if ( lOther != null )
            {
                Vector2F lMinimum = this.mMinimum as Vector2F;
                Vector2F lMaximum = this.mMaximum as Vector2F;
                Vector2F lOtherMinimum = lOther.mMinimum as Vector2F;
                Vector2F lOtherMaximum = lOther.mMaximum as Vector2F;
                return new Box2F( new Vector2F( System.Math.Min( lOtherMinimum.X, lMinimum.X ), System.Math.Min( lOtherMinimum.Y, lMinimum.Y ) ), 
                                  new Vector2F( System.Math.Max( lOtherMaximum.X, lMaximum.X ), System.Math.Max( lOtherMaximum.Y, lMaximum.Y ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as ABox<float>;
        }

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public override bool Contains(IVector<float> pPoint)
        {
            Vector2F lPoint = pPoint as Vector2F;
            if ( lPoint != null )
            {
                Vector2F lMinimum = this.mMinimum as Vector2F;
                Vector2F lMaximum = this.mMaximum as Vector2F;
                return lPoint.X >= lMinimum.X &&
                       lPoint.X <= lMaximum.X &&
                       lPoint.Y >= lMinimum.Y &&
                       lPoint.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public override bool Contains(IBox<float> pBox)
        {
            Box2F lOther = pBox as Box2F;
            if ( lOther != null )
            {
                Vector2F lMinimum = this.mMinimum as Vector2F;
                Vector2F lMaximum = this.mMaximum as Vector2F;
                Vector2F lOtherMinimum = lOther.mMinimum as Vector2F;
                Vector2F lOtherMaximum = lOther.mMaximum as Vector2F;
                return lOtherMinimum.X >= lMinimum.X &&
                       lOtherMaximum.X <= lMaximum.X &&
                       lOtherMinimum.Y >= lMinimum.Y &&
                       lOtherMaximum.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public override bool Intersects(IBox<float> pToTest)
        {
            Box2F lOther = pToTest as Box2F;
            if ( lOther != null )
            {
                Vector2F lMinimum = this.mMinimum as Vector2F;
                Vector2F lMaximum = this.mMaximum as Vector2F;
                Vector2F lOtherMinimum = lOther.mMinimum as Vector2F;
                Vector2F lOtherMaximum = lOther.mMaximum as Vector2F;
                return lOtherMaximum.X >= lMinimum.X &&
                       lOtherMinimum.X <= lMaximum.X &&
                       lOtherMaximum.Y >= lMinimum.Y &&
                       lOtherMinimum.Y <= lMaximum.Y;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public override IVector<float> GetNearestBoxPoint(IVector<float> pPoint)
        {
            Vector2F lMinimum = this.mMinimum as Vector2F;
            Vector2F lMaximum = this.mMaximum as Vector2F;

            Vector2F lNearest = pPoint.Clone() as Vector2F;
            if ( lNearest.X < lMinimum.X )
            {
                lNearest.X = lMinimum.X;
            }
            else if ( lNearest.X > lMaximum.X )
            {
                lNearest.X = lMaximum.X;
            }

            if ( lNearest.Y < lMinimum.Y )
            {
                lNearest.Y = lMinimum.Y;
            }
            else if ( lNearest.Y > lMaximum.Y )
            {
                lNearest.Y = lMaximum.Y;
            }

            return lNearest;
        }

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public override float DistanceTo(IVector<float> pPoint)
        {
            return (float)System.Math.Sqrt( (double)this.DistanceSqTo( pPoint ) );
        }

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public override float DistanceSqTo(IVector<float> pPoint)
        {
            Vector2F lPoint = pPoint as Vector2F;
            AVector<float> lNearest = this.GetNearestBoxPoint( lPoint ) as AVector<float>;
            return (lPoint - lNearest).LengthSq();
        }
        
        #endregion Methods IBox

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToFloat(object pToCast)
        {
            if ( pToCast is Box2F )
            {
                return (pToCast as ICloneable).Clone();
            }
            else if ( pToCast is Box2D )
            {
                Box2D lCast = pToCast as Box2D;
                return new Box2F( lCast.Minimum.Cast<float>() as Vector2F, 
                                  lCast.Maximum.Cast<float>() as Vector2F );
            }
            else if ( pToCast is Box2I )
            {
                Box2I lCast = pToCast as Box2I;
                return new Box2F( lCast.Minimum.Cast<float>() as Vector2F, 
                                  lCast.Maximum.Cast<float>() as Vector2F );
            }
            else if ( pToCast is Box2H )
            {
                Box2H lCast = pToCast as Box2H;
                return new Box2F( lCast.Minimum.Cast<float>() as Vector2F, 
                                  lCast.Maximum.Cast<float>() as Vector2F );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
