﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Box3I"/> class.
    /// </summary>
    public class Box3I : ABox3<int>
    {
        #region Properties

        #region Properties ABox3

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public override int Width
        {
            get
            {
                Vector3I lExtents = this.Extents as Vector3I;
                return lExtents.X;
            }
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public override int Height
        {
            get
            {
                Vector3I lExtents = this.Extents as Vector3I;
                return lExtents.Y;
            }
        }

        /// <summary>
        /// Gets the box's depth
        /// </summary>
        public override int Depth
        {
            get
            {
                Vector3I lExtents = this.Extents as Vector3I;
                return lExtents.Z;
            }
        }

        /// <summary>
        /// Gets the Box's volume.
        /// </summary>
        public override int Volume
        {
            get
            {
                Vector3I lExtents = this.Extents as Vector3I;
                return lExtents.X * lExtents.Y * lExtents.Z;
            }
        }
        
        #endregion Properties ABox3

        #region Properties IBox

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public override IVector<int> Extents
        {
            get
            {
                Vector3I lMinimum = this.mMinimum as Vector3I;
                Vector3I lMaximum = this.mMaximum as Vector3I;
                return new Vector3I( lMaximum.X - lMinimum.X, lMaximum.Y - lMinimum.Y, lMaximum.Z - lMinimum.Z );
            }
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public override IVector<int> Center
        {
            get
            {
                Vector3I lMinimum = this.mMinimum as Vector3I;
                Vector3I lMaximum = this.mMaximum as Vector3I;
                return new Vector3I( ( lMinimum.X + lMaximum.X ) / 2,
                                     ( lMinimum.Y + lMaximum.Y ) / 2,
                                     ( lMinimum.Z + lMaximum.Z ) / 2 );
            }
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Box3I"/> class.
        /// </summary>
        static Box3I()
        {
            sCasters.Add( typeof(int), CastToInt );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3I"/> class.
        /// </summary>
        public Box3I() :
        this( int.MaxValue, int.MinValue,
              int.MaxValue, int.MinValue,
              int.MaxValue, int.MinValue )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3I"/> class.
        /// </summary>
        /// <param name="pMinX">The minimum X coordinate (Equals Left)</param>
        /// <param name="pMaxX">The maximum X coordinate (Equals Right)</param>
        /// <param name="pMinY">The minimum Y coordinate (Equals Bottom)</param>
        /// <param name="pMaxY">The maximum Y coordinate (Equals Top)</param>
        /// <param name="pMinZ">The minimum Z coordinate (Equals Front)</param>
        /// <param name="pMaxZ">The maximum Z coordinate (Equals Back)</param>
        public Box3I(int pMinX, int pMaxX, int pMinY, int pMaxY, int pMinZ, int pMaxZ) :
        this( new Vector3I( pMinX, pMinY, pMinZ ), new Vector3I( pMaxX, pMaxY, pMaxZ ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3I"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        public Box3I(Vector3I pMinimum, Vector3I pMaximum) :
        base( pMinimum, pMaximum )
        {
            // Integers cannot be Nan or infinite.
        }

        #endregion Constructors

        #region Methods

        #region Methods ICloneable

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected override ABox<int> InternalClone()
        {
            return new Box3I( this.mMinimum.Clone() as Vector3I, 
                              this.mMaximum.Clone() as Vector3I );
        }
        
        #endregion Methods ICloneable

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public override IBox<int> Enlarge(int pExtentToAdd)
        {
            Vector3I lMinimum = this.mMinimum as Vector3I;
            Vector3I lMaximum = this.mMaximum as Vector3I;
            return new Box3I( new Vector3I( lMinimum.X - pExtentToAdd, lMinimum.Y - pExtentToAdd, lMinimum.Z - pExtentToAdd ), 
                              new Vector3I( lMaximum.X + pExtentToAdd, lMaximum.Y + pExtentToAdd, lMaximum.Z + pExtentToAdd ) );
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public override IBox<int> Enlarge(IVector<int> pPoint)
        {
            Vector3I lPoint = pPoint as Vector3I;
            if ( lPoint != null )
            {
                Vector3I lMinimum = this.mMinimum as Vector3I;
                Vector3I lMaximum = this.mMaximum as Vector3I;
                return new Box3I( new Vector3I( System.Math.Min( lPoint.X, lMinimum.X ), System.Math.Min( lPoint.Y, lMinimum.Y ), System.Math.Min( lPoint.Z, lMinimum.Z ) ), 
                                  new Vector3I( System.Math.Max( lPoint.X, lMaximum.X ), System.Math.Max( lPoint.Y, lMaximum.Y ), System.Math.Max( lPoint.Z, lMaximum.Z ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as IBox<int>;
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected override ABox<int> InternalMerge(ABox<int> pToAdd)
        {
            Box3I lOther = pToAdd as Box3I;
            if ( lOther != null )
            {
                Vector3I lMinimum = this.mMinimum as Vector3I;
                Vector3I lMaximum = this.mMaximum as Vector3I;
                Vector3I lOtherMinimum = lOther.mMinimum as Vector3I;
                Vector3I lOtherMaximum = lOther.mMaximum as Vector3I;
                return new Box3I( new Vector3I( System.Math.Min( lOtherMinimum.X, lMinimum.X ), System.Math.Min( lOtherMinimum.Y, lMinimum.Y ), System.Math.Min( lOtherMinimum.Z, lMinimum.Z ) ), 
                                  new Vector3I( System.Math.Max( lOtherMaximum.X, lMaximum.X ), System.Math.Max( lOtherMaximum.Y, lMaximum.Y ), System.Math.Max( lOtherMaximum.Z, lMaximum.Z ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as ABox<int>;
        }

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public override bool Contains(IVector<int> pPoint)
        {
            Vector3I lPoint = pPoint as Vector3I;
            if ( lPoint != null )
            {
                Vector3I lMinimum = this.mMinimum as Vector3I;
                Vector3I lMaximum = this.mMaximum as Vector3I;
                return lPoint.X >= lMinimum.X &&
                       lPoint.X <= lMaximum.X &&
                       lPoint.Y >= lMinimum.Y &&
                       lPoint.Y <= lMaximum.Y && 
                       lPoint.Z >= lMinimum.Z && 
                       lPoint.Z <= lMaximum.Z;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public override bool Contains(IBox<int> pBox)
        {
            Box3I lOther = pBox as Box3I;
            if ( lOther != null )
            {
                Vector3I lMinimum = this.mMinimum as Vector3I;
                Vector3I lMaximum = this.mMaximum as Vector3I;
                Vector3I lOtherMinimum = lOther.mMinimum as Vector3I;
                Vector3I lOtherMaximum = lOther.mMaximum as Vector3I;
                return lOtherMinimum.X >= lMinimum.X &&
                       lOtherMaximum.X <= lMaximum.X &&
                       lOtherMinimum.Y >= lMinimum.Y &&
                       lOtherMaximum.Y <= lMaximum.Y &&
                       lOtherMinimum.Z >= lMinimum.Z &&
                       lOtherMaximum.Z <= lMaximum.Z;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public override bool Intersects(IBox<int> pToTest)
        {
            Box3I lOther = pToTest as Box3I;
            if ( lOther != null )
            {
                Vector3I lMinimum = this.mMinimum as Vector3I;
                Vector3I lMaximum = this.mMaximum as Vector3I;
                Vector3I lOtherMinimum = lOther.mMinimum as Vector3I;
                Vector3I lOtherMaximum = lOther.mMaximum as Vector3I;
                if ( lOtherMaximum.X < lMinimum.X || lOtherMinimum.X > lMaximum.X ||
                     lOtherMaximum.Y < lMinimum.Y || lOtherMinimum.Y > lMaximum.Y ||
                     lOtherMaximum.Z < lMinimum.Z || lOtherMinimum.Z > lMaximum.Z )
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public override IVector<int> GetNearestBoxPoint(IVector<int> pPoint)
        {
            Vector3I lMinimum = this.mMinimum as Vector3I;
            Vector3I lMaximum = this.mMaximum as Vector3I;

            Vector3I lNearest = pPoint.Clone() as Vector3I;
            if ( lNearest.X < lMinimum.X )
            {
                lNearest.X = lMinimum.X;
            }
            else if ( lNearest.X > lMaximum.X )
            {
                lNearest.X = lMaximum.X;
            }

            if ( lNearest.Y < lMinimum.Y )
            {
                lNearest.Y = lMinimum.Y;
            }
            else if ( lNearest.Y > lMaximum.Y )
            {
                lNearest.Y = lMaximum.Y;
            }

            if ( lNearest.Z < lMinimum.Z )
            {
                lNearest.Z = lMinimum.Z;
            }
            else if ( lNearest.Z > lMaximum.Z )
            {
                lNearest.Z = lMaximum.Z;
            }

            return lNearest;
        }

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public override int DistanceTo(IVector<int> pPoint)
        {
            return (int)System.Math.Sqrt( this.DistanceSqTo( pPoint ) );
        }

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public override int DistanceSqTo(IVector<int> pPoint)
        {
            Vector3I lPoint = pPoint as Vector3I;
            AVector<int> lNearest = this.GetNearestBoxPoint( lPoint ) as AVector<int>;
            return (lPoint - lNearest).LengthSq();
        }
        
        #endregion Methods IBox
        
        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToInt(object pToCast)
        {
            if ( pToCast is Box3I )
            {
                return (pToCast as ICloneable).Clone();
            }
            else if ( pToCast is Box3D )
            {
                Box3D lCast = pToCast as Box3D;
                return new Box3I( lCast.Minimum.Cast<int>() as Vector3I, 
                                  lCast.Maximum.Cast<int>() as Vector3I );
            }
            else if ( pToCast is Box3F )
            {
                Box3F lCast = pToCast as Box3F;
                return new Box3I( lCast.Minimum.Cast<int>() as Vector3I, 
                                  lCast.Maximum.Cast<int>() as Vector3I );
            }
            else if ( pToCast is Box3H )
            {
                Box3H lCast = pToCast as Box3H;
                return new Box3I( lCast.Minimum.Cast<int>() as Vector3I, 
                                  lCast.Maximum.Cast<int>() as Vector3I );
            }

            return null;
        }

        #endregion Methods Caster

        #region Methods Operators

        /// <summary>
        /// Returns a copy of the given bounding box transformed
        /// by the transformation represented by the given matrix.
        /// </summary>
        /// <param name="pTransform">The transform to apply to the bounding box.</param>
        /// <param name="pBounds">The bounding box to transform.</param>
        /// <returns>The transformed bounding box.</returns>
        public static Box3I operator * (Matrix4I pTransform, Box3I pBounds)
        {
            Vector3I lMinimum = pBounds.mMinimum as Vector3I;
            Vector3I lMaximum = pBounds.mMaximum as Vector3I;

            IBox<int> lResult = new Box3I();
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3I( lMinimum.X, lMinimum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3I( lMaximum.X, lMinimum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3I( lMinimum.X, lMaximum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3I( lMaximum.X, lMaximum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3I( lMinimum.X, lMinimum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3I( lMaximum.X, lMinimum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3I( lMinimum.X, lMaximum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3I( lMaximum.X, lMaximum.Y, lMaximum.Z ) ) );

            return lResult as Box3I;
        }

        #endregion Methods Operators

        #endregion Methods
    }
}
