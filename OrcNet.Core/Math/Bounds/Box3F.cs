﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Box3F"/> class.
    /// </summary>
    public class Box3F : ABox3<float>
    {
        #region Properties

        #region Properties ABox3

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public override float Width
        {
            get
            {
                Vector3F lExtents = this.Extents as Vector3F;
                return lExtents.X;
            }
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public override float Height
        {
            get
            {
                Vector3F lExtents = this.Extents as Vector3F;
                return lExtents.Y;
            }
        }

        /// <summary>
        /// Gets the box's depth
        /// </summary>
        public override float Depth
        {
            get
            {
                Vector3F lExtents = this.Extents as Vector3F;
                return lExtents.Z;
            }
        }

        /// <summary>
        /// Gets the Box's volume.
        /// </summary>
        public override float Volume
        {
            get
            {
                Vector3F lExtents = this.Extents as Vector3F;
                return lExtents.X * lExtents.Y * lExtents.Z;
            }
        }
        
        #endregion Properties ABox3

        #region Properties IBox

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public override IVector<float> Extents
        {
            get
            {
                Vector3F lMinimum = this.mMinimum as Vector3F;
                Vector3F lMaximum = this.mMaximum as Vector3F;
                return new Vector3F( lMaximum.X - lMinimum.X, lMaximum.Y - lMinimum.Y, lMaximum.Z - lMinimum.Z );
            }
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public override IVector<float> Center
        {
            get
            {
                Vector3F lMinimum = this.mMinimum as Vector3F;
                Vector3F lMaximum = this.mMaximum as Vector3F;
                return new Vector3F( ( lMinimum.X + lMaximum.X ) * 0.5f,
                                     ( lMinimum.Y + lMaximum.Y ) * 0.5f,
                                     ( lMinimum.Z + lMaximum.Z ) * 0.5f );
            }
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Box3F"/> class.
        /// </summary>
        static Box3F()
        {
            sCasters.Add( typeof(float), CastToFloat );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3F"/> class.
        /// </summary>
        public Box3F() :
        this( float.PositiveInfinity, float.NegativeInfinity,
              float.PositiveInfinity, float.NegativeInfinity,
              float.PositiveInfinity, float.NegativeInfinity )
        {
            this.mExtent = BoxExtent.INFINITE;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3F"/> class.
        /// </summary>
        /// <param name="pMinX">The minimum X coordinate (Equals Left)</param>
        /// <param name="pMaxX">The maximum X coordinate (Equals Right)</param>
        /// <param name="pMinY">The minimum Y coordinate (Equals Bottom)</param>
        /// <param name="pMaxY">The maximum Y coordinate (Equals Top)</param>
        /// <param name="pMinZ">The minimum Z coordinate (Equals Front)</param>
        /// <param name="pMaxZ">The maximum Z coordinate (Equals Back)</param>
        public Box3F(float pMinX, float pMaxX, float pMinY, float pMaxY, float pMinZ, float pMaxZ) :
        this( new Vector3F( pMinX, pMinY, pMinZ ), new Vector3F( pMaxX, pMaxY, pMaxZ ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3F"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        public Box3F(Vector3F pMinimum, Vector3F pMaximum) :
        base( pMinimum, pMaximum )
        {
            // If one of the component is Nan or Infinite, it makes
            // the Box Infinite.
            if ( float.IsNaN( pMinimum.X ) || 
                 float.IsNaN( pMinimum.Y ) || 
                 float.IsNaN( pMinimum.Z ) || 
                 float.IsNaN( pMaximum.X ) ||
                 float.IsNaN( pMaximum.Y ) || 
                 float.IsNaN( pMaximum.Z ) )
            {
                this.mExtent = BoxExtent.INFINITE;
            }

            if ( float.IsInfinity( pMinimum.X ) ||
                 float.IsInfinity( pMinimum.Y ) ||
                 float.IsInfinity( pMinimum.Z ) ||
                 float.IsInfinity( pMaximum.X ) ||
                 float.IsInfinity( pMaximum.Y ) ||
                 float.IsInfinity( pMaximum.Z ) )
            {
                this.mExtent = BoxExtent.INFINITE;
            }
        }

        #endregion Constructors

        #region Methods

        #region Methods ICloneable

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected override ABox<float> InternalClone()
        {
            return new Box3F( this.mMinimum.Clone() as Vector3F, 
                              this.mMaximum.Clone() as Vector3F );
        }
        
        #endregion Methods ICloneable

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public override IBox<float> Enlarge(float pExtentToAdd)
        {
            Vector3F lMinimum = this.mMinimum as Vector3F;
            Vector3F lMaximum = this.mMaximum as Vector3F;
            return new Box3F( new Vector3F( lMinimum.X - pExtentToAdd, lMinimum.Y - pExtentToAdd, lMinimum.Z - pExtentToAdd ), 
                              new Vector3F( lMaximum.X + pExtentToAdd, lMaximum.Y + pExtentToAdd, lMaximum.Z + pExtentToAdd ) );
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public override IBox<float> Enlarge(IVector<float> pPoint)
        {
            Vector3F lPoint = pPoint as Vector3F;
            if ( lPoint != null )
            {
                Vector3F lMinimum = this.mMinimum as Vector3F;
                Vector3F lMaximum = this.mMaximum as Vector3F;
                return new Box3F( new Vector3F( System.Math.Min( lPoint.X, lMinimum.X ), System.Math.Min( lPoint.Y, lMinimum.Y ), System.Math.Min( lPoint.Z, lMinimum.Z ) ), 
                                  new Vector3F( System.Math.Max( lPoint.X, lMaximum.X ), System.Math.Max( lPoint.Y, lMaximum.Y ), System.Math.Max( lPoint.Z, lMaximum.Z ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as IBox<float>;
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected override ABox<float> InternalMerge(ABox<float> pToAdd)
        {
            Box3F lOther = pToAdd as Box3F;
            if ( lOther != null )
            {
                Vector3F lMinimum = this.mMinimum as Vector3F;
                Vector3F lMaximum = this.mMaximum as Vector3F;
                Vector3F lOtherMinimum = lOther.mMinimum as Vector3F;
                Vector3F lOtherMaximum = lOther.mMaximum as Vector3F;
                return new Box3F( new Vector3F( System.Math.Min( lOtherMinimum.X, lMinimum.X ), System.Math.Min( lOtherMinimum.Y, lMinimum.Y ), System.Math.Min( lOtherMinimum.Z, lMinimum.Z ) ), 
                                  new Vector3F( System.Math.Max( lOtherMaximum.X, lMaximum.X ), System.Math.Max( lOtherMaximum.Y, lMaximum.Y ), System.Math.Max( lOtherMaximum.Z, lMaximum.Z ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as ABox<float>;
        }

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public override bool Contains(IVector<float> pPoint)
        {
            Vector3F lPoint = pPoint as Vector3F;
            if ( lPoint != null )
            {
                Vector3F lMinimum = this.mMinimum as Vector3F;
                Vector3F lMaximum = this.mMaximum as Vector3F;
                return lPoint.X >= lMinimum.X &&
                       lPoint.X <= lMaximum.X &&
                       lPoint.Y >= lMinimum.Y &&
                       lPoint.Y <= lMaximum.Y && 
                       lPoint.Z >= lMinimum.Z && 
                       lPoint.Z <= lMaximum.Z;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public override bool Contains(IBox<float> pBox)
        {
            Box3F lOther = pBox as Box3F;
            if ( lOther != null )
            {
                Vector3F lMinimum = this.mMinimum as Vector3F;
                Vector3F lMaximum = this.mMaximum as Vector3F;
                Vector3F lOtherMinimum = lOther.mMinimum as Vector3F;
                Vector3F lOtherMaximum = lOther.mMaximum as Vector3F;
                return lOtherMinimum.X >= lMinimum.X &&
                       lOtherMaximum.X <= lMaximum.X &&
                       lOtherMinimum.Y >= lMinimum.Y &&
                       lOtherMaximum.Y <= lMaximum.Y &&
                       lOtherMinimum.Z >= lMinimum.Z &&
                       lOtherMaximum.Z <= lMaximum.Z;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public override bool Intersects(IBox<float> pToTest)
        {
            Box3F lOther = pToTest as Box3F;
            if ( lOther != null )
            {
                Vector3F lMinimum = this.mMinimum as Vector3F;
                Vector3F lMaximum = this.mMaximum as Vector3F;
                Vector3F lOtherMinimum = lOther.mMinimum as Vector3F;
                Vector3F lOtherMaximum = lOther.mMaximum as Vector3F;
                if ( lOtherMaximum.X < lMinimum.X || lOtherMinimum.X > lMaximum.X ||
                     lOtherMaximum.Y < lMinimum.Y || lOtherMinimum.Y > lMaximum.Y ||
                     lOtherMaximum.Z < lMinimum.Z || lOtherMinimum.Z > lMaximum.Z )
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public override IVector<float> GetNearestBoxPoint(IVector<float> pPoint)
        {
            Vector3F lMinimum = this.mMinimum as Vector3F;
            Vector3F lMaximum = this.mMaximum as Vector3F;

            Vector3F lNearest = pPoint.Clone() as Vector3F;
            if ( lNearest.X < lMinimum.X )
            {
                lNearest.X = lMinimum.X;
            }
            else if ( lNearest.X > lMaximum.X )
            {
                lNearest.X = lMaximum.X;
            }

            if ( lNearest.Y < lMinimum.Y )
            {
                lNearest.Y = lMinimum.Y;
            }
            else if ( lNearest.Y > lMaximum.Y )
            {
                lNearest.Y = lMaximum.Y;
            }

            if ( lNearest.Z < lMinimum.Z )
            {
                lNearest.Z = lMinimum.Z;
            }
            else if ( lNearest.Z > lMaximum.Z )
            {
                lNearest.Z = lMaximum.Z;
            }

            return lNearest;
        }

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public override float DistanceTo(IVector<float> pPoint)
        {
            return (float)System.Math.Sqrt( (double)this.DistanceSqTo( pPoint ) );
        }

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public override float DistanceSqTo(IVector<float> pPoint)
        {
            Vector3F lPoint = pPoint as Vector3F;
            AVector<float> lNearest = this.GetNearestBoxPoint( lPoint ) as AVector<float>;
            return (lPoint - lNearest).LengthSq();
        }
        
        #endregion Methods IBox
        
        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToFloat(object pToCast)
        {
            if ( pToCast is Box3F )
            {
                return (pToCast as ICloneable).Clone();
            }
            else if ( pToCast is Box3D )
            {
                Box3D lCast = pToCast as Box3D;
                return new Box3F( lCast.Minimum.Cast<float>() as Vector3F, 
                                  lCast.Maximum.Cast<float>() as Vector3F );
            }
            else if ( pToCast is Box3I )
            {
                Box3I lCast = pToCast as Box3I;
                return new Box3F( lCast.Minimum.Cast<float>() as Vector3F, 
                                  lCast.Maximum.Cast<float>() as Vector3F );
            }
            else if ( pToCast is Box3H )
            {
                Box3H lCast = pToCast as Box3H;
                return new Box3F( lCast.Minimum.Cast<float>() as Vector3F, 
                                  lCast.Maximum.Cast<float>() as Vector3F );
            }

            return null;
        }

        #endregion Methods Caster

        #region Methods Operators

        /// <summary>
        /// Returns a copy of the given bounding box transformed
        /// by the transformation represented by the given matrix.
        /// </summary>
        /// <param name="pTransform">The transform to apply to the bounding box.</param>
        /// <param name="pBounds">The bounding box to transform.</param>
        /// <returns>The transformed bounding box.</returns>
        public static Box3F operator * (Matrix4F pTransform, Box3F pBounds)
        {
            Vector3F lMinimum = pBounds.mMinimum as Vector3F;
            Vector3F lMaximum = pBounds.mMaximum as Vector3F;

            IBox<float> lResult = new Box3F();
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3F( lMinimum.X, lMinimum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3F( lMaximum.X, lMinimum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3F( lMinimum.X, lMaximum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3F( lMaximum.X, lMaximum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3F( lMinimum.X, lMinimum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3F( lMaximum.X, lMinimum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3F( lMinimum.X, lMaximum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3F( lMaximum.X, lMaximum.Y, lMaximum.Z ) ) );

            return lResult as Box3F;
        }

        #endregion Methods Operators

        #endregion Methods
    }
}
