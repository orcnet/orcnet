﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Box3H"/> class.
    /// </summary>
    public class Box3H : ABox3<Half>
    {
        #region Properties

        #region Properties ABox3

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public override Half Width
        {
            get
            {
                Vector3H lExtents = this.Extents as Vector3H;
                return lExtents.X;
            }
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public override Half Height
        {
            get
            {
                Vector3H lExtents = this.Extents as Vector3H;
                return lExtents.Y;
            }
        }

        /// <summary>
        /// Gets the box's depth
        /// </summary>
        public override Half Depth
        {
            get
            {
                Vector3H lExtents = this.Extents as Vector3H;
                return lExtents.Z;
            }
        }

        /// <summary>
        /// Gets the Box's volume.
        /// </summary>
        public override Half Volume
        {
            get
            {
                Vector3H lExtents = this.Extents as Vector3H;
                return lExtents.X * lExtents.Y * lExtents.Z;
            }
        }
        
        #endregion Properties ABox3

        #region Properties IBox

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public override IVector<Half> Extents
        {
            get
            {
                Vector3H lMinimum = this.mMinimum as Vector3H;
                Vector3H lMaximum = this.mMaximum as Vector3H;
                return new Vector3H( lMaximum.X - lMinimum.X, lMaximum.Y - lMinimum.Y, lMaximum.Z - lMinimum.Z );
            }
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public override IVector<Half> Center
        {
            get
            {
                Vector3H lMinimum = this.mMinimum as Vector3H;
                Vector3H lMaximum = this.mMaximum as Vector3H;
                return new Vector3H( ( lMinimum.X + lMaximum.X ) * new Half( 0.5f ),
                                     ( lMinimum.Y + lMaximum.Y ) * new Half( 0.5f ),
                                     ( lMinimum.Z + lMaximum.Z ) * new Half( 0.5f ) );
            }
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Box3H"/> class.
        /// </summary>
        static Box3H()
        {
            sCasters.Add( typeof(Half), CastToHalf );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3H"/> class.
        /// </summary>
        public Box3H() :
        this( new Half( float.PositiveInfinity ), new Half( float.NegativeInfinity ),
              new Half( float.PositiveInfinity ), new Half( float.NegativeInfinity ),
              new Half( float.PositiveInfinity ), new Half( float.NegativeInfinity ) )
        {
            this.mExtent = BoxExtent.INFINITE;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3H"/> class.
        /// </summary>
        /// <param name="pMinX">The minimum X coordinate (Equals Left)</param>
        /// <param name="pMaxX">The maximum X coordinate (Equals Right)</param>
        /// <param name="pMinY">The minimum Y coordinate (Equals Bottom)</param>
        /// <param name="pMaxY">The maximum Y coordinate (Equals Top)</param>
        /// <param name="pMinZ">The minimum Z coordinate (Equals Front)</param>
        /// <param name="pMaxZ">The maximum Z coordinate (Equals Back)</param>
        public Box3H(Half pMinX, Half pMaxX, Half pMinY, Half pMaxY, Half pMinZ, Half pMaxZ) :
        this( new Vector3H( pMinX, pMinY, pMinZ ), new Vector3H( pMaxX, pMaxY, pMaxZ ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3H"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        public Box3H(Vector3H pMinimum, Vector3H pMaximum) :
        base( pMinimum, pMaximum )
        {
            // If one of the component is Nan or Infinite, it makes
            // the Box Infinite.
            if ( pMinimum.X.IsNan() || 
                 pMinimum.Y.IsNan() || 
                 pMinimum.Z.IsNan() || 
                 pMaximum.X.IsNan() ||
                 pMaximum.Y.IsNan() || 
                 pMaximum.Z.IsNan() )
            {
                this.mExtent = BoxExtent.INFINITE;
            }

            if ( pMinimum.X.IsInfinite() ||
                 pMinimum.Y.IsInfinite() ||
                 pMinimum.Z.IsInfinite() ||
                 pMaximum.X.IsInfinite() ||
                 pMaximum.Y.IsInfinite() ||
                 pMaximum.Z.IsInfinite() )
            {
                this.mExtent = BoxExtent.INFINITE;
            }
        }

        #endregion Constructors

        #region Methods

        #region Methods ICloneable

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected override ABox<Half> InternalClone()
        {
            return new Box3H( this.mMinimum.Clone() as Vector3H, 
                              this.mMaximum.Clone() as Vector3H );
        }
        
        #endregion Methods ICloneable

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public override IBox<Half> Enlarge(Half pExtentToAdd)
        {
            Vector3H lMinimum = this.mMinimum as Vector3H;
            Vector3H lMaximum = this.mMaximum as Vector3H;
            return new Box3H( new Vector3H( lMinimum.X - pExtentToAdd, lMinimum.Y - pExtentToAdd, lMinimum.Z - pExtentToAdd ), 
                              new Vector3H( lMaximum.X + pExtentToAdd, lMaximum.Y + pExtentToAdd, lMaximum.Z + pExtentToAdd ) );
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public override IBox<Half> Enlarge(IVector<Half> pPoint)
        {
            Vector3H lPoint = pPoint as Vector3H;
            if ( lPoint != null )
            {
                Vector3H lMinimum = this.mMinimum as Vector3H;
                Vector3H lMaximum = this.mMaximum as Vector3H;
                return new Box3H( new Vector3H( new Half( System.Math.Min( lPoint.X, lMinimum.X ) ), new Half( System.Math.Min( lPoint.Y, lMinimum.Y ) ), new Half( System.Math.Min( lPoint.Z, lMinimum.Z ) ) ), 
                                  new Vector3H( new Half( System.Math.Max( lPoint.X, lMaximum.X ) ), new Half( System.Math.Max( lPoint.Y, lMaximum.Y ) ), new Half( System.Math.Max( lPoint.Z, lMaximum.Z ) ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as IBox<Half>;
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected override ABox<Half> InternalMerge(ABox<Half> pToAdd)
        {
            Box3H lOther = pToAdd as Box3H;
            if ( lOther != null )
            {
                Vector3H lMinimum = this.mMinimum as Vector3H;
                Vector3H lMaximum = this.mMaximum as Vector3H;
                Vector3H lOtherMinimum = lOther.mMinimum as Vector3H;
                Vector3H lOtherMaximum = lOther.mMaximum as Vector3H;
                return new Box3H( new Vector3H( new Half( System.Math.Min( lOtherMinimum.X, lMinimum.X ) ), new Half( System.Math.Min( lOtherMinimum.Y, lMinimum.Y ) ), new Half( System.Math.Min( lOtherMinimum.Z, lMinimum.Z ) ) ), 
                                  new Vector3H( new Half( System.Math.Max( lOtherMaximum.X, lMaximum.X ) ), new Half( System.Math.Max( lOtherMaximum.Y, lMaximum.Y ) ), new Half( System.Math.Max( lOtherMaximum.Z, lMaximum.Z ) ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as ABox<Half>;
        }

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public override bool Contains(IVector<Half> pPoint)
        {
            Vector3H lPoint = pPoint as Vector3H;
            if ( lPoint != null )
            {
                Vector3H lMinimum = this.mMinimum as Vector3H;
                Vector3H lMaximum = this.mMaximum as Vector3H;
                return lPoint.X >= lMinimum.X &&
                       lPoint.X <= lMaximum.X &&
                       lPoint.Y >= lMinimum.Y &&
                       lPoint.Y <= lMaximum.Y && 
                       lPoint.Z >= lMinimum.Z && 
                       lPoint.Z <= lMaximum.Z;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public override bool Contains(IBox<Half> pBox)
        {
            Box3H lOther = pBox as Box3H;
            if ( lOther != null )
            {
                Vector3H lMinimum = this.mMinimum as Vector3H;
                Vector3H lMaximum = this.mMaximum as Vector3H;
                Vector3H lOtherMinimum = lOther.mMinimum as Vector3H;
                Vector3H lOtherMaximum = lOther.mMaximum as Vector3H;
                return lOtherMinimum.X >= lMinimum.X &&
                       lOtherMaximum.X <= lMaximum.X &&
                       lOtherMinimum.Y >= lMinimum.Y &&
                       lOtherMaximum.Y <= lMaximum.Y &&
                       lOtherMinimum.Z >= lMinimum.Z &&
                       lOtherMaximum.Z <= lMaximum.Z;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public override bool Intersects(IBox<Half> pToTest)
        {
            Box3H lOther = pToTest as Box3H;
            if ( lOther != null )
            {
                Vector3H lMinimum = this.mMinimum as Vector3H;
                Vector3H lMaximum = this.mMaximum as Vector3H;
                Vector3H lOtherMinimum = lOther.mMinimum as Vector3H;
                Vector3H lOtherMaximum = lOther.mMaximum as Vector3H;
                if ( lOtherMaximum.X < lMinimum.X || lOtherMinimum.X > lMaximum.X ||
                     lOtherMaximum.Y < lMinimum.Y || lOtherMinimum.Y > lMaximum.Y ||
                     lOtherMaximum.Z < lMinimum.Z || lOtherMinimum.Z > lMaximum.Z )
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public override IVector<Half> GetNearestBoxPoint(IVector<Half> pPoint)
        {
            Vector3H lMinimum = this.mMinimum as Vector3H;
            Vector3H lMaximum = this.mMaximum as Vector3H;

            Vector3H lNearest = pPoint.Clone() as Vector3H;
            if ( lNearest.X < lMinimum.X )
            {
                lNearest.X = lMinimum.X;
            }
            else if ( lNearest.X > lMaximum.X )
            {
                lNearest.X = lMaximum.X;
            }

            if ( lNearest.Y < lMinimum.Y )
            {
                lNearest.Y = lMinimum.Y;
            }
            else if ( lNearest.Y > lMaximum.Y )
            {
                lNearest.Y = lMaximum.Y;
            }

            if ( lNearest.Z < lMinimum.Z )
            {
                lNearest.Z = lMinimum.Z;
            }
            else if ( lNearest.Z > lMaximum.Z )
            {
                lNearest.Z = lMaximum.Z;
            }

            return lNearest;
        }

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public override Half DistanceTo(IVector<Half> pPoint)
        {
            return new Half( System.Math.Sqrt( this.DistanceSqTo( pPoint ) ) );
        }

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public override Half DistanceSqTo(IVector<Half> pPoint)
        {
            Vector3H lPoint = pPoint as Vector3H;
            AVector<Half> lNearest = this.GetNearestBoxPoint( lPoint ) as AVector<Half>;
            return (lPoint - lNearest).LengthSq();
        }
        
        #endregion Methods IBox
        
        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToHalf(object pToCast)
        {
            if ( pToCast is Box3H )
            {
                return (pToCast as ICloneable).Clone();
            }
            else if ( pToCast is Box3D )
            {
                Box3D lCast = pToCast as Box3D;
                return new Box3H( lCast.Minimum.Cast<Half>() as Vector3H, 
                                  lCast.Maximum.Cast<Half>() as Vector3H );
            }
            else if ( pToCast is Box3I )
            {
                Box3I lCast = pToCast as Box3I;
                return new Box3H( lCast.Minimum.Cast<Half>() as Vector3H, 
                                  lCast.Maximum.Cast<Half>() as Vector3H );
            }
            else if ( pToCast is Box3F )
            {
                Box3F lCast = pToCast as Box3F;
                return new Box3H( lCast.Minimum.Cast<Half>() as Vector3H, 
                                  lCast.Maximum.Cast<Half>() as Vector3H );
            }

            return null;
        }

        #endregion Methods Caster

        #region Methods Operators

        /// <summary>
        /// Returns a copy of the given bounding box transformed
        /// by the transformation represented by the given matrix.
        /// </summary>
        /// <param name="pTransform">The transform to apply to the bounding box.</param>
        /// <param name="pBounds">The bounding box to transform.</param>
        /// <returns>The transformed bounding box.</returns>
        public static Box3H operator * (Matrix4H pTransform, Box3H pBounds)
        {
            Vector3H lMinimum = pBounds.mMinimum as Vector3H;
            Vector3H lMaximum = pBounds.mMaximum as Vector3H;

            IBox<Half> lResult = new Box3H();
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3H( lMinimum.X, lMinimum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3H( lMaximum.X, lMinimum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3H( lMinimum.X, lMaximum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3H( lMaximum.X, lMaximum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3H( lMinimum.X, lMinimum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3H( lMaximum.X, lMinimum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3H( lMinimum.X, lMaximum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3H( lMaximum.X, lMaximum.Y, lMaximum.Z ) ) );

            return lResult as Box3H;
        }

        #endregion Methods Operators

        #endregion Methods
    }
}
