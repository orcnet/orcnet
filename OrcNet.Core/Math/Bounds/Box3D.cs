﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Box3D"/> class.
    /// </summary>
    public class Box3D : ABox3<double>
    {
        #region Properties

        #region Properties ABox3

        /// <summary>
        /// Gets the Box's width
        /// </summary>
        public override double Width
        {
            get
            {
                Vector3D lExtents = this.Extents as Vector3D;
                return lExtents.X;
            }
        }

        /// <summary>
        /// Gets the box's height
        /// </summary>
        public override double Height
        {
            get
            {
                Vector3D lExtents = this.Extents as Vector3D;
                return lExtents.Y;
            }
        }

        /// <summary>
        /// Gets the box's depth
        /// </summary>
        public override double Depth
        {
            get
            {
                Vector3D lExtents = this.Extents as Vector3D;
                return lExtents.Z;
            }
        }

        /// <summary>
        /// Gets the Box's volume.
        /// </summary>
        public override double Volume
        {
            get
            {
                Vector3D lExtents = this.Extents as Vector3D;
                return lExtents.X * lExtents.Y * lExtents.Z;
            }
        }
        
        #endregion Properties ABox3

        #region Properties IBox

        /// <summary>
        /// Gets the box's size as vector corresponding to the box's dimensions.
        /// </summary>
        public override IVector<double> Extents
        {
            get
            {
                Vector3D lMinimum = this.mMinimum as Vector3D;
                Vector3D lMaximum = this.mMaximum as Vector3D;
                return new Vector3D( lMaximum.X - lMinimum.X, lMaximum.Y - lMinimum.Y, lMaximum.Z - lMinimum.Z );
            }
        }

        /// <summary>
        /// Gets the box's center.
        /// </summary>
        public override IVector<double> Center
        {
            get
            {
                Vector3D lMinimum = this.mMinimum as Vector3D;
                Vector3D lMaximum = this.mMaximum as Vector3D;
                return new Vector3D( ( lMinimum.X + lMaximum.X ) * 0.5,
                                     ( lMinimum.Y + lMaximum.Y ) * 0.5,
                                     ( lMinimum.Z + lMaximum.Z ) * 0.5 );
            }
        }

        #endregion Properties IBox

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Box3D"/> class.
        /// </summary>
        static Box3D()
        {
            sCasters.Add( typeof(double), CastToDouble );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3D"/> class.
        /// </summary>
        public Box3D() :
        this( double.PositiveInfinity, double.NegativeInfinity,
              double.PositiveInfinity, double.NegativeInfinity,
              double.PositiveInfinity, double.NegativeInfinity )
        {
            this.mExtent = BoxExtent.INFINITE;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3D"/> class.
        /// </summary>
        /// <param name="pMinX">The minimum X coordinate (Equals Left)</param>
        /// <param name="pMaxX">The maximum X coordinate (Equals Right)</param>
        /// <param name="pMinY">The minimum Y coordinate (Equals Bottom)</param>
        /// <param name="pMaxY">The maximum Y coordinate (Equals Top)</param>
        /// <param name="pMinZ">The minimum Z coordinate (Equals Front)</param>
        /// <param name="pMaxZ">The maximum Z coordinate (Equals Back)</param>
        public Box3D(double pMinX, double pMaxX, double pMinY, double pMaxY, double pMinZ, double pMaxZ) :
        this( new Vector3D( pMinX, pMinY, pMinZ ), new Vector3D( pMaxX, pMaxY, pMaxZ ) )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Box3D"/> class.
        /// </summary>
        /// <param name="pMinimum">The box's minimum.</param>
        /// <param name="pMaximum">The box's maximum.</param>
        public Box3D(Vector3D pMinimum, Vector3D pMaximum) :
        base( pMinimum, pMaximum )
        {
            // If one of the component is Nan or Infinite, it makes
            // the Box Infinite.
            if ( double.IsNaN( pMinimum.X ) || 
                 double.IsNaN( pMinimum.Y ) || 
                 double.IsNaN( pMinimum.Z ) || 
                 double.IsNaN( pMaximum.X ) ||
                 double.IsNaN( pMaximum.Y ) || 
                 double.IsNaN( pMaximum.Z ) )
            {
                this.mExtent = BoxExtent.INFINITE;
            }

            if ( double.IsInfinity( pMinimum.X ) ||
                 double.IsInfinity( pMinimum.Y ) ||
                 double.IsInfinity( pMinimum.Z ) ||
                 double.IsInfinity( pMaximum.X ) ||
                 double.IsInfinity( pMaximum.Y ) ||
                 double.IsInfinity( pMaximum.Z ) )
            {
                this.mExtent = BoxExtent.INFINITE;
            }
        }

        #endregion Constructors

        #region Methods

        #region Methods ICloneable

        /// <summary>
        /// Clones this Box.
        /// </summary>
        /// <returns>The Box's clone.</returns>
        protected override ABox<double> InternalClone()
        {
            return new Box3D( this.mMinimum.Clone() as Vector3D, 
                              this.mMaximum.Clone() as Vector3D );
        }
        
        #endregion Methods ICloneable

        #region Methods IBox

        /// <summary>
        /// Extends this Box by the supplied extent.
        /// </summary>
        /// <param name="pExtentToAdd">The extent to add to that Box</param>
        /// <returns>A copy of that Box extended by the supplied extent</returns>
        public override IBox<double> Enlarge(double pExtentToAdd)
        {
            Vector3D lMinimum = this.mMinimum as Vector3D;
            Vector3D lMaximum = this.mMaximum as Vector3D;
            return new Box3D( new Vector3D( lMinimum.X - pExtentToAdd, lMinimum.Y - pExtentToAdd, lMinimum.Z - pExtentToAdd ), 
                              new Vector3D( lMaximum.X + pExtentToAdd, lMaximum.Y + pExtentToAdd, lMaximum.Z + pExtentToAdd ) );
        }

        /// <summary>
        /// Extends this Box so it contains the given point.
        /// </summary>
        /// <param name="pPoint">The point that must be inside the new box.</param>
        /// <returns>A copy of this Box enlarged so it contains the point.</returns>
        public override IBox<double> Enlarge(IVector<double> pPoint)
        {
            Vector3D lPoint = pPoint as Vector3D;
            if ( lPoint != null )
            {
                Vector3D lMinimum = this.mMinimum as Vector3D;
                Vector3D lMaximum = this.mMaximum as Vector3D;
                return new Box3D( new Vector3D( System.Math.Min( lPoint.X, lMinimum.X ), System.Math.Min( lPoint.Y, lMinimum.Y ), System.Math.Min( lPoint.Z, lMinimum.Z ) ), 
                                  new Vector3D( System.Math.Max( lPoint.X, lMaximum.X ), System.Math.Max( lPoint.Y, lMaximum.Y ), System.Math.Max( lPoint.Z, lMaximum.Z ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as IBox<double>;
        }

        /// <summary>
        /// Internal method which merges this Box with a supplied Box2D
        /// </summary>
        /// <param name="pToAdd">The Box2D to add to that Box2D</param>
        /// <returns>A copy of that Box2D merged the supplied Box2D</returns>
        protected override ABox<double> InternalMerge(ABox<double> pToAdd)
        {
            Box3D lOther = pToAdd as Box3D;
            if ( lOther != null )
            {
                Vector3D lMinimum = this.mMinimum as Vector3D;
                Vector3D lMaximum = this.mMaximum as Vector3D;
                Vector3D lOtherMinimum = lOther.mMinimum as Vector3D;
                Vector3D lOtherMaximum = lOther.mMaximum as Vector3D;
                return new Box3D( new Vector3D( System.Math.Min( lOtherMinimum.X, lMinimum.X ), System.Math.Min( lOtherMinimum.Y, lMinimum.Y ), System.Math.Min( lOtherMinimum.Z, lMinimum.Z ) ), 
                                  new Vector3D( System.Math.Max( lOtherMaximum.X, lMaximum.X ), System.Math.Max( lOtherMaximum.Y, lMaximum.Y ), System.Math.Max( lOtherMaximum.Z, lMaximum.Z ) ) );
            }

            // Default answer in case of box type mismatches.
            return this.Clone() as ABox<double>;
        }

        /// <summary>
        /// Checks whether the supplied point is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pPoint">The point to test inside this box.</param>
        /// <returns>True if contains the point, false otherwise.</returns>
        public override bool Contains(IVector<double> pPoint)
        {
            Vector3D lPoint = pPoint as Vector3D;
            if ( lPoint != null )
            {
                Vector3D lMinimum = this.mMinimum as Vector3D;
                Vector3D lMaximum = this.mMaximum as Vector3D;
                return lPoint.X >= lMinimum.X &&
                       lPoint.X <= lMaximum.X &&
                       lPoint.Y >= lMinimum.Y &&
                       lPoint.Y <= lMaximum.Y && 
                       lPoint.Z >= lMinimum.Z && 
                       lPoint.Z <= lMaximum.Z;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box is contained
        /// in this Box or not.
        /// </summary>
        /// <param name="pBox">The Box to test against this box.</param>
        /// <returns>True if contains the Box, false otherwise.</returns>
        public override bool Contains(IBox<double> pBox)
        {
            Box3D lOther = pBox as Box3D;
            if ( lOther != null )
            {
                Vector3D lMinimum = this.mMinimum as Vector3D;
                Vector3D lMaximum = this.mMaximum as Vector3D;
                Vector3D lOtherMinimum = lOther.mMinimum as Vector3D;
                Vector3D lOtherMaximum = lOther.mMaximum as Vector3D;
                return lOtherMinimum.X >= lMinimum.X &&
                       lOtherMaximum.X <= lMaximum.X &&
                       lOtherMinimum.Y >= lMinimum.Y &&
                       lOtherMaximum.Y <= lMaximum.Y &&
                       lOtherMinimum.Z >= lMinimum.Z &&
                       lOtherMaximum.Z <= lMaximum.Z;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the supplied Box intersects
        /// that Box or not.
        /// </summary>
        /// <param name="pToTest">The Box to test against "this"</param>
        /// <returns>True if intersects, false otherwise.</returns>
        public override bool Intersects(IBox<double> pToTest)
        {
            Box3D lOther = pToTest as Box3D;
            if ( lOther != null )
            {
                Vector3D lMinimum = this.mMinimum as Vector3D;
                Vector3D lMaximum = this.mMaximum as Vector3D;
                Vector3D lOtherMinimum = lOther.mMinimum as Vector3D;
                Vector3D lOtherMaximum = lOther.mMaximum as Vector3D;
                if ( lOtherMaximum.X < lMinimum.X || lOtherMinimum.X > lMaximum.X ||
                     lOtherMaximum.Y < lMinimum.Y || lOtherMinimum.Y > lMaximum.Y ||
                     lOtherMaximum.Z < lMinimum.Z || lOtherMinimum.Z > lMaximum.Z )
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Retrieves the nearest point of this Box from the supplied point.
        /// </summary>
        /// <param name="pPoint">The point regrading to which must be the nearest from.</param>
        /// <returns>The nearest Box's point.</returns>
        public override IVector<double> GetNearestBoxPoint(IVector<double> pPoint)
        {
            Vector3D lMinimum = this.mMinimum as Vector3D;
            Vector3D lMaximum = this.mMaximum as Vector3D;

            Vector3D lNearest = pPoint.Clone() as Vector3D;
            if ( lNearest.X < lMinimum.X )
            {
                lNearest.X = lMinimum.X;
            }
            else if ( lNearest.X > lMaximum.X )
            {
                lNearest.X = lMaximum.X;
            }

            if ( lNearest.Y < lMinimum.Y )
            {
                lNearest.Y = lMinimum.Y;
            }
            else if ( lNearest.Y > lMaximum.Y )
            {
                lNearest.Y = lMaximum.Y;
            }

            if ( lNearest.Z < lMinimum.Z )
            {
                lNearest.Z = lMinimum.Z;
            }
            else if ( lNearest.Z > lMaximum.Z )
            {
                lNearest.Z = lMaximum.Z;
            }

            return lNearest;
        }

        /// <summary>
        /// Computes the minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The minimum distance from this box to the point.</returns>
        public override double DistanceTo(IVector<double> pPoint)
        {
            return System.Math.Sqrt( this.DistanceSqTo( pPoint ) );
        }

        /// <summary>
        /// Computes the squared minimum distance between this Box and the given point.
        /// </summary>
        /// <param name="pPoint">The point regarding to which the distance must be computed.</param>
        /// <returns>The squared minimum distance from this box to the point.</returns>
        public override double DistanceSqTo(IVector<double> pPoint)
        {
            Vector3D lPoint = pPoint as Vector3D;
            AVector<double> lNearest = this.GetNearestBoxPoint( lPoint ) as AVector<double>;
            return (lPoint - lNearest).LengthSq();
        }
        
        #endregion Methods IBox
        
        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToDouble(object pToCast)
        {
            if ( pToCast is Box3D )
            {
                return (pToCast as ICloneable).Clone();
            }
            else if ( pToCast is Box3F )
            {
                Box3F lCast = pToCast as Box3F;
                return new Box3D( lCast.Minimum.Cast<double>() as Vector3D, 
                                  lCast.Maximum.Cast<double>() as Vector3D );
            }
            else if ( pToCast is Box3I )
            {
                Box3I lCast = pToCast as Box3I;
                return new Box3D( lCast.Minimum.Cast<double>() as Vector3D, 
                                  lCast.Maximum.Cast<double>() as Vector3D );
            }
            else if ( pToCast is Box3H )
            {
                Box3H lCast = pToCast as Box3H;
                return new Box3D( lCast.Minimum.Cast<double>() as Vector3D, 
                                  lCast.Maximum.Cast<double>() as Vector3D );
            }

            return null;
        }

        #endregion Methods Caster

        #region Methods Operators

        /// <summary>
        /// Returns a copy of the given bounding box transformed
        /// by the transformation represented by the given matrix.
        /// </summary>
        /// <param name="pTransform">The transform to apply to the bounding box.</param>
        /// <param name="pBounds">The bounding box to transform.</param>
        /// <returns>The transformed bounding box.</returns>
        public static Box3D operator * (Matrix4D pTransform, Box3D pBounds)
        {
            Vector3D lMinimum = pBounds.mMinimum as Vector3D;
            Vector3D lMaximum = pBounds.mMaximum as Vector3D;

            IBox<double> lResult = new Box3D();
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3D( lMinimum.X, lMinimum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3D( lMaximum.X, lMinimum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3D( lMinimum.X, lMaximum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3D( lMaximum.X, lMaximum.Y, lMinimum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3D( lMinimum.X, lMinimum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3D( lMaximum.X, lMinimum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3D( lMinimum.X, lMaximum.Y, lMaximum.Z ) ) );
            lResult = lResult.Enlarge( pTransform.Multiply( new Vector3D( lMaximum.X, lMaximum.Y, lMaximum.Z ) ) );

            return lResult as Box3D;
        }

        #endregion Methods Operators

        #endregion Methods
    }
}
