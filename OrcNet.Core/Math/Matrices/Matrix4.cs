﻿using System;
using System.Collections.Generic;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Matrix4{T}"/> class.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public abstract class Matrix4<T> : AMatrix<T> where T : struct
    {
        #region Fields

        /// <summary>
        /// Stores the constant component count.
        /// </summary>
        protected const int cComponentCount = 16;

        /// <summary>
        /// Stores the set of cast delegate for the matrix cast method.
        /// </summary>
        protected static Dictionary<Type, CastDelegate> sCasters;

        /// <summary>
        /// Stores the set of vector creators delegates.
        /// </summary>
        private static Dictionary<Type, VectorCreatorDelegate> sVectorCreators;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the value at (0, 0).
        /// </summary>
        public T M00
        {
            get
            {
                return this.mData[ 0, 0 ];
            }
            set
            {
                this.mData[ 0, 0 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (0, 1).
        /// </summary>
        public T M01
        {
            get
            {
                return this.mData[ 0, 1 ];
            }
            set
            {
                this.mData[ 0, 1 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (0, 2).
        /// </summary>
        public T M02
        {
            get
            {
                return this.mData[ 0, 2 ];
            }
            set
            {
                this.mData[ 0, 2 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (0, 3).
        /// </summary>
        public T M03
        {
            get
            {
                return this.mData[ 0, 3 ];
            }
            set
            {
                this.mData[ 0, 3 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (1, 0).
        /// </summary>
        public T M10
        {
            get
            {
                return this.mData[ 1, 0 ];
            }
            set
            {
                this.mData[ 1, 0 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (1, 1).
        /// </summary>
        public T M11
        {
            get
            {
                return this.mData[ 1, 1 ];
            }
            set
            {
                this.mData[ 1, 1 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (1, 2).
        /// </summary>
        public T M12
        {
            get
            {
                return this.mData[ 1, 2 ];
            }
            set
            {
                this.mData[ 1, 2 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (1, 3).
        /// </summary>
        public T M13
        {
            get
            {
                return this.mData[ 1, 3 ];
            }
            set
            {
                this.mData[ 1, 3 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (2, 0).
        /// </summary>
        public T M20
        {
            get
            {
                return this.mData[ 2, 0 ];
            }
            set
            {
                this.mData[ 2, 0 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (2, 1).
        /// </summary>
        public T M21
        {
            get
            {
                return this.mData[ 2, 1 ];
            }
            set
            {
                this.mData[ 2, 1 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (2, 2).
        /// </summary>
        public T M22
        {
            get
            {
                return this.mData[ 2, 2 ];
            }
            set
            {
                this.mData[ 2, 2 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (2, 3).
        /// </summary>
        public T M23
        {
            get
            {
                return this.mData[ 2, 3 ];
            }
            set
            {
                this.mData[ 2, 3 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (3, 0).
        /// </summary>
        public T M30
        {
            get
            {
                return this.mData[ 3, 0 ];
            }
            set
            {
                this.mData[ 3, 0 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (3, 1).
        /// </summary>
        public T M31
        {
            get
            {
                return this.mData[ 3, 1 ];
            }
            set
            {
                this.mData[ 3, 1 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (3, 2).
        /// </summary>
        public T M32
        {
            get
            {
                return this.mData[ 3, 2 ];
            }
            set
            {
                this.mData[ 3, 2 ] = value;
            }
        }

        /// <summary>
        /// Gets the value at (3, 3).
        /// </summary>
        public T M33
        {
            get
            {
                return this.mData[ 3, 3 ];
            }
            set
            {
                this.mData[ 3, 3 ] = value;
            }
        }

        #region Properties IMatrix

        /// <summary>
        /// Gets the matrix component count.
        /// </summary>
        public override int ComponentCount
        {
            get
            {
                return cComponentCount;
            }
        }

        /// <summary>
        /// Gets or sets the row values.
        /// </summary>
        /// <param name="pRow">The row index.</param>
        /// <returns>The row values as vector, null if unknown primitive type.</returns>
        public override IVector<T> this[RowIndex pRow]
        {
            get
            {
                Vector4<T> lRow = null;
                if ( pRow < 0 ||
                     pRow >= 4 )
                {
                    return null;
                }

                VectorCreatorDelegate lBuilder;
                if( sVectorCreators.TryGetValue( typeof(T), out lBuilder ) )
                {
                    lRow = lBuilder() as Vector4<T>;
                    lRow.X = this.mData[ pRow, 0 ];
                    lRow.Y = this.mData[ pRow, 1 ];
                    lRow.Z = this.mData[ pRow, 2 ];
                    lRow.W = this.mData[ pRow, 3 ];
                }

                return lRow;
            }
            set
            {
                if ( pRow < 0 ||
                     pRow >= 4 )
                {
                    return;
                }

                Vector4<T> lCast = value as Vector4<T>;
                if ( lCast != null )
                {
                    this.mData[ pRow, 0 ] = lCast.X;
                    this.mData[ pRow, 1 ] = lCast.Y;
                    this.mData[ pRow, 2 ] = lCast.Z;
                    this.mData[ pRow, 3 ] = lCast.W;
                }
            }
        }

        /// <summary>
        /// Gets or sets the column values.
        /// </summary>
        /// <param name="pColumn">The column index.</param>
        /// <returns>The column values as vector, null if unknown primitive type.</returns>
        public override IVector<T> this[ColumnIndex pColumn]
        {
            get
            {
                Vector4<T> lColumn = null;
                if ( pColumn < 0 ||
                     pColumn >= 4 )
                {
                    return null;
                }
                
                VectorCreatorDelegate lBuilder;
                if( sVectorCreators.TryGetValue( typeof(T), out lBuilder ) )
                {
                    lColumn = lBuilder() as Vector4<T>;
                    lColumn.X = this.mData[ 0, pColumn ];
                    lColumn.Y = this.mData[ 1, pColumn ];
                    lColumn.Z = this.mData[ 2, pColumn ];
                    lColumn.W = this.mData[ 3, pColumn ];
                }

                return lColumn;
            }
            set
            {
                if ( pColumn < 0 ||
                     pColumn >= 4 )
                {
                    return;
                }

                Vector4<T> lCast = value as Vector4<T>;
                if ( lCast != null )
                {
                    this.mData[ 0, pColumn ] = lCast.X;
                    this.mData[ 1, pColumn ] = lCast.Y;
                    this.mData[ 2, pColumn ] = lCast.Z;
                    this.mData[ 3, pColumn ] = lCast.W;
                }
            }
        }

        #endregion Properties IMatrix

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Matrix4{T}"/> class.
        /// </summary>
        static Matrix4()
        {
            sCasters = new Dictionary<Type, CastDelegate>();
            sVectorCreators = new Dictionary<Type, VectorCreatorDelegate>();
            sVectorCreators.Add( typeof(float), FloatVectorCreator );
            sVectorCreators.Add( typeof(double), DoubleVectorCreator );
            sVectorCreators.Add( typeof(int), IntVectorCreator );
            sVectorCreators.Add( typeof(Half), HalfVectorCreator );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4{T}"/> class.
        /// </summary>
        /// <param name="p00">The (0, 0) value of the matrix.</param>
        /// <param name="p01">The (0, 1) value of the matrix.</param>
        /// <param name="p02">The (0, 2) value of the matrix.</param>
        /// <param name="p03">The (0, 3) value of the matrix.</param>
        /// <param name="p10">The (1, 0) value of the matrix.</param>
        /// <param name="p11">The (1, 1) value of the matrix.</param>
        /// <param name="p12">The (1, 2) value of the matrix.</param>
        /// <param name="p13">The (1, 3) value of the matrix.</param>
        /// <param name="p20">The (2, 0) value of the matrix.</param>
        /// <param name="p21">The (2, 1) value of the matrix.</param>
        /// <param name="p22">The (2, 2) value of the matrix.</param>
        /// <param name="p23">The (2, 3) value of the matrix.</param>
        /// <param name="p30">The (3, 0) value of the matrix.</param>
        /// <param name="p31">The (3, 1) value of the matrix.</param>
        /// <param name="p32">The (3, 2) value of the matrix.</param>
        /// <param name="p33">The (3, 3) value of the matrix.</param>
        protected Matrix4(T p00, T p01, T p02, T p03,
                          T p10, T p11, T p12, T p13,
                          T p20, T p21, T p22, T p23,
                          T p30, T p31, T p32, T p33)
        {
            this.mData = new MatrixCache<T>( cComponentCount );
            this.mData[ 0, 0 ] = p00;
            this.mData[ 0, 1 ] = p01;
            this.mData[ 0, 2 ] = p02;
            this.mData[ 0, 3 ] = p03;
            this.mData[ 1, 0 ] = p10;
            this.mData[ 1, 1 ] = p11;
            this.mData[ 1, 2 ] = p12;
            this.mData[ 1, 3 ] = p13;
            this.mData[ 2, 0 ] = p20;
            this.mData[ 2, 1 ] = p21;
            this.mData[ 2, 2 ] = p22;
            this.mData[ 2, 3 ] = p23;
            this.mData[ 3, 0 ] = p30;
            this.mData[ 3, 1 ] = p31;
            this.mData[ 3, 2 ] = p32;
            this.mData[ 3, 3 ] = p33;
        }

        #endregion Constructor

        #region Methods

        #region Methods IMatrix

        /// <summary>
        /// Turns this matrix into another matrix of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new matrix of another primitive type, null if cannot cast into the given primitive type.</returns>
        public override IMatrix<OtherT> Cast<OtherT>()
        {
            CastDelegate lCaster = null;
            if ( sCasters.TryGetValue( typeof(OtherT), out lCaster ) )
            {
                return lCaster( this ) as IMatrix<OtherT>;
            }

            return null;
        }

        #endregion Methods IMatrix

        #region Methods VectorCreators

        /// <summary>
        /// Creates a float based vector2.
        /// </summary>
        private static object FloatVectorCreator()
        {
            return new Vector4F();
        }

        /// <summary>
        /// Creates a double based vector2.
        /// </summary>
        private static object DoubleVectorCreator()
        {
            return new Vector4D();
        }

        /// <summary>
        /// Creates a int based vector2.
        /// </summary>
        private static object IntVectorCreator()
        {
            return new Vector4I();
        }

        /// <summary>
        /// Creates a half based vector2.
        /// </summary>
        private static object HalfVectorCreator()
        {
            return new Vector4H();
        }

        #endregion Methods VectorCreators
        
        #endregion Methods
    }
}
