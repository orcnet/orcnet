﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Matrix4F"/> class.
    /// </summary>
    public class Matrix4F : Matrix4<float>
    {
        #region Properties

        /// <summary>
        /// Gets a Zero matrix.
        /// </summary>
        public static Matrix4F ZERO
        {
            get
            {
                return new Matrix4F( 0.0f );
            }
        }

        /// <summary>
        /// Gets an identity matrix.
        /// </summary>
        public static Matrix4F IDENTITY
        {
            get
            {
                return new Matrix4F();
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(float) * cComponentCount;
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #region Properties IMatrix

        /// <summary>
        /// Gets the matrix determinant.
        /// </summary>
        public override float Determinant
        {
            get
            {
                return this.mData[0, 0] * ComputeMinor( this, 1, 2, 3, 1, 2, 3 ) -
                       this.mData[0, 1] * ComputeMinor( this, 1, 2, 3, 0, 2, 3 ) +
                       this.mData[0, 2] * ComputeMinor( this, 1, 2, 3, 0, 1, 3 ) -
                       this.mData[0, 3] * ComputeMinor( this, 1, 2, 3, 0, 1, 2 );
            }
        }

        /// <summary>
        /// Gets the matrix trace.
        /// </summary>
        public override float Trace
        {
            get
            {
                return this.mData[0, 0] + this.mData[1, 1] + this.mData[2, 2] + this.mData[3, 3];
            }
        }

        #endregion Properties IMatrix

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Matrix4F"/> class.
        /// </summary>
        static Matrix4F()
        {
            sCasters.Add( typeof(float), CastToFloat );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4F"/> class.
        /// Identity matrix by default.
        /// </summary>
        public Matrix4F() :
        this( 1.0f, 0.0f, 0.0f, 0.0f, 
              0.0f, 1.0f, 0.0f, 0.0f,
              0.0f, 0.0f, 1.0f, 0.0f,
              0.0f, 0.0f, 0.0f, 1.0f )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4F"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Matrix4F(float pValue) :
        this( pValue, pValue, pValue, pValue, 
              pValue, pValue, pValue, pValue, 
              pValue, pValue, pValue, pValue,
              pValue, pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4F"/> class.
        /// </summary>
        /// <param name="p00">The (0, 0) value of the matrix.</param>
        /// <param name="p01">The (0, 1) value of the matrix.</param>
        /// <param name="p02">The (0, 2) value of the matrix.</param>
        /// <param name="p03">The (0, 3) value of the matrix.</param>
        /// <param name="p10">The (1, 0) value of the matrix.</param>
        /// <param name="p11">The (1, 1) value of the matrix.</param>
        /// <param name="p12">The (1, 2) value of the matrix.</param>
        /// <param name="p13">The (1, 3) value of the matrix.</param>
        /// <param name="p20">The (2, 0) value of the matrix.</param>
        /// <param name="p21">The (2, 1) value of the matrix.</param>
        /// <param name="p22">The (2, 2) value of the matrix.</param>
        /// <param name="p23">The (2, 3) value of the matrix.</param>
        /// <param name="p30">The (3, 0) value of the matrix.</param>
        /// <param name="p31">The (3, 1) value of the matrix.</param>
        /// <param name="p32">The (3, 2) value of the matrix.</param>
        /// <param name="p33">The (3, 3) value of the matrix.</param>
        public Matrix4F(float p00, float p01, float p02, float p03,
                        float p10, float p11, float p12, float p13,
                        float p20, float p21, float p22, float p23,
                        float p30, float p31, float p32, float p33) :
        base( p00, p01, p02, p03, 
              p10, p11, p12, p13, 
              p20, p21, p22, p23,
              p30, p31, p32, p33 )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Statics

        /// <summary>
        /// Creates a new translation matrix from the given translation vector.
        /// </summary>
        /// <param name="pTranslation">THe translation vector.</param>
        /// <returns>The translation matrix.</returns>
        public static Matrix4F Translate(Vector3F pTranslation)
        {
            return new Matrix4F( 1.0f, 0.0f, 0.0f, pTranslation.X,
                                 0.0f, 1.0f, 0.0f, pTranslation.Y,
                                 0.0f, 0.0f, 1.0f, pTranslation.Z,
                                 0.0f, 0.0f, 0.0f, 1.0f );
        }

        /// <summary>
        /// Creates a new rotation matrix around the X axis with the given angle in degrees.
        /// </summary>
        /// <param name="pAngleInDegrees">The angle in degrees.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4F RotateX(float pAngleInDegrees)
        {
            float lCosAngle = (float)System.Math.Cos( MathUtility.ToRadians( pAngleInDegrees ) );
            float lSinAngle = (float)System.Math.Sin( MathUtility.ToRadians( pAngleInDegrees ) );
            return new Matrix4F( 1.0f, 0.0f, 0.0f, 0.0f,
                                 0.0f, lCosAngle, -lSinAngle, 0.0f,
                                 0.0f, lSinAngle, lCosAngle, 0.0f,
                                 0.0f, 0.0f, 0.0f, 1.0f);
        }

        /// <summary>
        /// Creates a new rotation matrix around the Y axis with the given angle in degrees.
        /// </summary>
        /// <param name="pAngleInDegrees">The angle in degrees.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4F RotateY(float pAngleInDegrees)
        {
            float lCosAngle = (float)System.Math.Cos( MathUtility.ToRadians( pAngleInDegrees ) );
            float lSinAngle = (float)System.Math.Sin( MathUtility.ToRadians( pAngleInDegrees ) );
            return new Matrix4F( lCosAngle, 0.0f, lSinAngle, 0.0f,
                                 0.0f, 1.0f, 0.0f, 0.0f,
                                 -lSinAngle, 0.0f, lCosAngle, 0.0f,
                                 0.0f, 0.0f, 0.0f, 1.0f );
        }

        /// <summary>
        /// Creates a new rotation matrix around the Z axis with the given angle in degrees.
        /// </summary>
        /// <param name="pAngleInDegrees">The angle in degrees.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4F RotateZ(float pAngleInDegrees)
        {
            float lCosAngle = (float)System.Math.Cos( MathUtility.ToRadians( pAngleInDegrees ) );
            float lSinAngle = (float)System.Math.Sin( MathUtility.ToRadians( pAngleInDegrees ) );
            return new Matrix4F( lCosAngle, -lSinAngle, 0.0f, 0.0f,
                                 lSinAngle, lCosAngle, 0.0f, 0.0f,
                                 0.0f, 0.0f, 1.0f, 0.0f,
                                 0.0f, 0.0f, 0.0f, 1.0f );
        }

        /// <summary>
        /// Creates a perspective projection matrix given the projection parameters such as the field of view, aspect ratio,
        /// the near and far clipping planes.
        /// </summary>
        /// <param name="pFovy">The field of view in degrees.</param>
        /// <param name="pAspect">The aspect ratio.</param>
        /// <param name="pNear">The near clipping plane.</param>
        /// <param name="pFar">The far clipping plane.</param>
        /// <returns>The projection matrix.</returns>
        public static Matrix4F PerspectiveProjection(float pFovy, float pAspect, float pNear, float pFar)
        {
            float f = (float)(1.0 / System.Math.Tan( MathUtility.ToRadians( pFovy ) / 2 ));
            return new Matrix4F( f / pAspect, 0.0f, 0.0f, 0.0f,
                                 0.0f, f, 0.0f, 0.0f,
                                 0.0f, 0.0f, (pFar + pNear) / (pNear - pFar), (2 * pFar * pNear) / (pNear - pFar),
                                 0.0f, 0.0f, -1.0f, 0.0f );
        }

        /// <summary>
        /// Creates an orthographic projection matrix given the projection parameters such as the right, left, top, bottom, near and far clipping planes.
        /// </summary>
        /// <param name="pRight">The right clipping plane.</param>
        /// <param name="pLeft">The left clipping plane.</param>
        /// <param name="pTop">The top clipping plane.</param>
        /// <param name="pBottom">The bottom clipping plane.</param>
        /// <param name="pNear">The near clipping plane.</param>
        /// <param name="pFar">The far clipping plane.</param>
        /// <returns>The projection matrix.</returns>
        public static Matrix4F OrthographicProjection(float pRight, float pLeft, float pTop, float pBottom, float pNear, float pFar)
        {
            float lTx = -(pRight + pLeft) / (pRight - pLeft);
            float lTy = -(pTop + pBottom) / (pTop - pBottom);
            float lTz = -(pFar + pNear) / (pFar - pNear);
            return new Matrix4F( 2.0f / (pRight - pLeft), 0.0f, 0.0f, lTx,
                                 0.0f, 2.0f / (pTop - pBottom), 0.0f, lTy,
                                 0.0f, 0.0f, -2.0f / (pFar - pNear), lTz,
                                 0.0f, 0.0f,  0.0f, 1.0f );
        }

        #endregion Methods Statics

        #region Methods Computation

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        protected override AMatrix<float> InternalAdd(AMatrix<float> pOther)
        {
            Matrix4F lOther = pOther as Matrix4F;
            Matrix4F lResult = new Matrix4F();

            lResult.mData[0, 0] = this.mData[0, 0] + lOther.mData[0, 0];
            lResult.mData[0, 1] = this.mData[0, 1] + lOther.mData[0, 1];
            lResult.mData[0, 2] = this.mData[0, 2] + lOther.mData[0, 2];
            lResult.mData[0, 3] = this.mData[0, 3] + lOther.mData[0, 3];

            lResult.mData[1, 0] = this.mData[1, 0] + lOther.mData[1, 0];
            lResult.mData[1, 1] = this.mData[1, 1] + lOther.mData[1, 1];
            lResult.mData[1, 2] = this.mData[1, 2] + lOther.mData[1, 2];
            lResult.mData[1, 3] = this.mData[1, 3] + lOther.mData[1, 3];

            lResult.mData[2, 0] = this.mData[2, 0] + lOther.mData[2, 0];
            lResult.mData[2, 1] = this.mData[2, 1] + lOther.mData[2, 1];
            lResult.mData[2, 2] = this.mData[2, 2] + lOther.mData[2, 2];
            lResult.mData[2, 3] = this.mData[2, 3] + lOther.mData[2, 3];

            lResult.mData[3, 0] = this.mData[3, 0] + lOther.mData[3, 0];
            lResult.mData[3, 1] = this.mData[3, 1] + lOther.mData[3, 1];
            lResult.mData[3, 2] = this.mData[3, 2] + lOther.mData[3, 2];
            lResult.mData[3, 3] = this.mData[3, 3] + lOther.mData[3, 3];

            return lResult;
        }

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        protected override AMatrix<float> InternalSubtract(AMatrix<float> pOther)
        {
            Matrix4F lOther = pOther as Matrix4F;
            Matrix4F lResult = new Matrix4F();

            lResult.mData[0, 0] = this.mData[0, 0] - lOther.mData[0, 0];
            lResult.mData[0, 1] = this.mData[0, 1] - lOther.mData[0, 1];
            lResult.mData[0, 2] = this.mData[0, 2] - lOther.mData[0, 2];
            lResult.mData[0, 3] = this.mData[0, 3] - lOther.mData[0, 3];

            lResult.mData[1, 0] = this.mData[1, 0] - lOther.mData[1, 0];
            lResult.mData[1, 1] = this.mData[1, 1] - lOther.mData[1, 1];
            lResult.mData[1, 2] = this.mData[1, 2] - lOther.mData[1, 2];
            lResult.mData[1, 3] = this.mData[1, 3] - lOther.mData[1, 3];

            lResult.mData[2, 0] = this.mData[2, 0] - lOther.mData[2, 0];
            lResult.mData[2, 1] = this.mData[2, 1] - lOther.mData[2, 1];
            lResult.mData[2, 2] = this.mData[2, 2] - lOther.mData[2, 2];
            lResult.mData[2, 3] = this.mData[2, 3] - lOther.mData[2, 3];

            lResult.mData[3, 0] = this.mData[3, 0] - lOther.mData[3, 0];
            lResult.mData[3, 1] = this.mData[3, 1] - lOther.mData[3, 1];
            lResult.mData[3, 2] = this.mData[3, 2] - lOther.mData[3, 2];
            lResult.mData[3, 3] = this.mData[3, 3] - lOther.mData[3, 3];

            return lResult;
        }

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<float> InternalMultiply(float pScale)
        {
            Matrix4F lResult = new Matrix4F();
            
            lResult.mData[0, 0] = this.mData[0, 0] * pScale;
            lResult.mData[0, 1] = this.mData[0, 1] * pScale;
            lResult.mData[0, 2] = this.mData[0, 2] * pScale;
            lResult.mData[0, 3] = this.mData[0, 3] * pScale;

            lResult.mData[1, 0] = this.mData[1, 0] * pScale;
            lResult.mData[1, 1] = this.mData[1, 1] * pScale;
            lResult.mData[1, 2] = this.mData[1, 2] * pScale;
            lResult.mData[1, 3] = this.mData[1, 3] * pScale;

            lResult.mData[2, 0] = this.mData[2, 0] * pScale;
            lResult.mData[2, 1] = this.mData[2, 1] * pScale;
            lResult.mData[2, 2] = this.mData[2, 2] * pScale;
            lResult.mData[2, 3] = this.mData[2, 3] * pScale;

            lResult.mData[3, 0] = this.mData[3, 0] * pScale;
            lResult.mData[3, 1] = this.mData[3, 1] * pScale;
            lResult.mData[3, 2] = this.mData[3, 2] * pScale;
            lResult.mData[3, 3] = this.mData[3, 3] * pScale;

            return lResult;
        }

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        protected override AVector<float> InternalMultiply(AVector<float> pVector)
        {
            Vector3F lVector3 = pVector as Vector3F;
            if ( lVector3 != null )
            {
                float lInvW = 1.0f / (this.mData[3, 0] * lVector3.X + this.mData[3, 1] * lVector3.Y + this.mData[3, 2] * lVector3.Z + this.mData[3, 3]);
                return new Vector3F( (this.mData[0, 0] * lVector3.X + this.mData[0, 1] * lVector3.Y + this.mData[0, 2] * lVector3.Z + this.mData[0, 3]) * lInvW,
                                     (this.mData[1, 0] * lVector3.X + this.mData[1, 1] * lVector3.Y + this.mData[1, 2] * lVector3.Z + this.mData[1, 3]) * lInvW,
                                     (this.mData[2, 0] * lVector3.X + this.mData[2, 1] * lVector3.Y + this.mData[2, 2] * lVector3.Z + this.mData[2, 3]) * lInvW );
            }

            Vector4F lVector4 = pVector as Vector4F;
            if ( lVector4 != null )
            {
                return new Vector4F( this.mData[0, 0] * lVector4.X + this.mData[0, 1] * lVector4.Y + this.mData[0, 2] * lVector4.Z + this.mData[0, 3] * lVector4.W,
                                     this.mData[1, 0] * lVector4.X + this.mData[1, 1] * lVector4.Y + this.mData[1, 2] * lVector4.Z + this.mData[1, 3] * lVector4.W,
                                     this.mData[2, 0] * lVector4.X + this.mData[2, 1] * lVector4.Y + this.mData[2, 2] * lVector4.Z + this.mData[2, 3] * lVector4.W,
                                     this.mData[3, 0] * lVector4.X + this.mData[3, 1] * lVector4.Y + this.mData[3, 2] * lVector4.Z + this.mData[3, 3] * lVector4.W );
            }
            
            // Default answer.
            return pVector.Clone() as AVector<float>;
        }

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pMatrix">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<float> InternalMultiply(AMatrix<float> pMatrix)
        {
            Matrix4F lOther = pMatrix as Matrix4F;
            Matrix4F lResult = new Matrix4F();

            lResult.mData[0, 0] = this.mData[0, 0] * lOther.mData[0, 0] + this.mData[0, 1] * lOther.mData[1, 0] + this.mData[0, 2] * lOther.mData[2, 0] + this.mData[0, 3] * lOther.mData[3, 0];
            lResult.mData[0, 1] = this.mData[0, 0] * lOther.mData[0, 1] + this.mData[0, 1] * lOther.mData[1, 1] + this.mData[0, 2] * lOther.mData[2, 1] + this.mData[0, 3] * lOther.mData[3, 1];
            lResult.mData[0, 2] = this.mData[0, 0] * lOther.mData[0, 2] + this.mData[0, 1] * lOther.mData[1, 2] + this.mData[0, 2] * lOther.mData[2, 2] + this.mData[0, 3] * lOther.mData[3, 2];
            lResult.mData[0, 3] = this.mData[0, 0] * lOther.mData[0, 3] + this.mData[0, 1] * lOther.mData[1, 3] + this.mData[0, 2] * lOther.mData[2, 3] + this.mData[0, 3] * lOther.mData[3, 3];

            lResult.mData[1, 0] = this.mData[1, 0] * lOther.mData[0, 0] + this.mData[1, 1] * lOther.mData[1, 0] + this.mData[1, 2] * lOther.mData[2, 0] + this.mData[1, 3] * lOther.mData[3, 0];
            lResult.mData[1, 1] = this.mData[1, 0] * lOther.mData[0, 1] + this.mData[1, 1] * lOther.mData[1, 1] + this.mData[1, 2] * lOther.mData[2, 1] + this.mData[1, 3] * lOther.mData[3, 1];
            lResult.mData[1, 2] = this.mData[1, 0] * lOther.mData[0, 2] + this.mData[1, 1] * lOther.mData[1, 2] + this.mData[1, 2] * lOther.mData[2, 2] + this.mData[1, 3] * lOther.mData[3, 2];
            lResult.mData[1, 3] = this.mData[1, 0] * lOther.mData[0, 3] + this.mData[1, 1] * lOther.mData[1, 3] + this.mData[1, 2] * lOther.mData[2, 3] + this.mData[1, 3] * lOther.mData[3, 3];

            lResult.mData[2, 0] = this.mData[2, 0] * lOther.mData[0, 0] + this.mData[2, 1] * lOther.mData[1, 0] + this.mData[2, 2] * lOther.mData[2, 0] + this.mData[2, 3] * lOther.mData[3, 0];
            lResult.mData[2, 1] = this.mData[2, 0] * lOther.mData[0, 1] + this.mData[2, 1] * lOther.mData[1, 1] + this.mData[2, 2] * lOther.mData[2, 1] + this.mData[2, 3] * lOther.mData[3, 1];
            lResult.mData[2, 2] = this.mData[2, 0] * lOther.mData[0, 2] + this.mData[2, 1] * lOther.mData[1, 2] + this.mData[2, 2] * lOther.mData[2, 2] + this.mData[2, 3] * lOther.mData[3, 2];
            lResult.mData[2, 3] = this.mData[2, 0] * lOther.mData[0, 3] + this.mData[2, 1] * lOther.mData[1, 3] + this.mData[2, 2] * lOther.mData[2, 3] + this.mData[2, 3] * lOther.mData[3, 3];

            lResult.mData[3, 0] = this.mData[3, 0] * lOther.mData[0, 0] + this.mData[3, 1] * lOther.mData[1, 0] + this.mData[3, 2] * lOther.mData[2, 0] + this.mData[3, 3] * lOther.mData[3, 0];
            lResult.mData[3, 1] = this.mData[3, 0] * lOther.mData[0, 1] + this.mData[3, 1] * lOther.mData[1, 1] + this.mData[3, 2] * lOther.mData[2, 1] + this.mData[3, 3] * lOther.mData[3, 1];
            lResult.mData[3, 2] = this.mData[3, 0] * lOther.mData[0, 2] + this.mData[3, 1] * lOther.mData[1, 2] + this.mData[3, 2] * lOther.mData[2, 2] + this.mData[3, 3] * lOther.mData[3, 2];
            lResult.mData[3, 3] = this.mData[3, 0] * lOther.mData[0, 3] + this.mData[3, 1] * lOther.mData[1, 3] + this.mData[3, 2] * lOther.mData[2, 3] + this.mData[3, 3] * lOther.mData[3, 3];

            return lResult;
        }

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        protected override ABox<float> InternalMultiply(ABox<float> pBox)
        {
            Box3F lBox = pBox as Box3F;
            if ( lBox != null )
            {
                Vector3F lMinimum = lBox.Minimum as Vector3F;
                Vector3F lMaximum = lBox.Maximum as Vector3F;
                IBox<float> lResult = new Box3F();
                lResult = lResult.Enlarge( this.Multiply( new Vector3F( lMinimum.X, lMinimum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3F( lMaximum.X, lMinimum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3F( lMinimum.X, lMaximum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3F( lMaximum.X, lMaximum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3F( lMinimum.X, lMinimum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3F( lMaximum.X, lMinimum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3F( lMinimum.X, lMaximum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3F( lMaximum.X, lMaximum.Y, lMaximum.Z ) ) );

                return lResult as ABox<float>;
            }

            // Default answer in case of box type mismatches.
            return pBox.Clone() as ABox<float>;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        protected override AMatrix<float> InternalTranspose()
        {
            return new Matrix4F( this.mData[0, 0], this.mData[1, 0], this.mData[2, 0], this.mData[3, 0],
                                 this.mData[0, 1], this.mData[1, 1], this.mData[2, 1], this.mData[3, 1],
                                 this.mData[0, 2], this.mData[1, 2], this.mData[2, 2], this.mData[3, 2],
                                 this.mData[0, 3], this.mData[1, 3], this.mData[2, 3], this.mData[3, 3] );
        }

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        protected override AMatrix<float> InternalInverse(float pTolerance, out bool pResult)
        {
            pResult = true;
            return this.Adjoint() * ( 1.0f / this.Determinant );
        }

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AMatrix<float> InternalOpposite()
        {
            Matrix4F lResult = new Matrix4F();
            for ( int lRow = 0; lRow < 4; lRow++ )
            {
                for ( int lCol = 0; lCol < 4; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = -this.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Turns this matrix into a 3x3 matrix.
        /// </summary>
        /// <returns>The new matrix 3x3</returns>
        public Matrix3F To3x3()
        {
            return new Matrix3F( this.mData[0, 0], this.mData[0, 1], this.mData[0, 2],
                                 this.mData[1, 0], this.mData[1, 1], this.mData[1, 2],
                                 this.mData[2, 0], this.mData[2, 1], this.mData[2, 2] );
        }

        /// <summary>
        /// Computes the adjoint of this matrix.
        /// </summary>
        /// <returns>The adjoint matrix.</returns>
        public Matrix4F Adjoint()
        {
            return new Matrix4F( ComputeMinor( this, 1, 2, 3, 1, 2, 3),
                                 -ComputeMinor( this, 0, 2, 3, 1, 2, 3),
                                 ComputeMinor( this, 0, 1, 3, 1, 2, 3),
                                 -ComputeMinor( this, 0, 1, 2, 1, 2, 3),

                                 -ComputeMinor( this, 1, 2, 3, 0, 2, 3),
                                 ComputeMinor( this, 0, 2, 3, 0, 2, 3),
                                 -ComputeMinor( this, 0, 1, 3, 0, 2, 3),
                                 ComputeMinor( this, 0, 1, 2, 0, 2, 3),

                                 ComputeMinor( this, 1, 2, 3, 0, 1, 3),
                                 -ComputeMinor( this, 0, 2, 3, 0, 1, 3),
                                 ComputeMinor( this, 0, 1, 3, 0, 1, 3),
                                 -ComputeMinor( this, 0, 1, 2, 0, 1, 3),

                                 -ComputeMinor( this, 1, 2, 3, 0, 1, 2),
                                 ComputeMinor( this, 0, 2, 3, 0, 1, 2),
                                 -ComputeMinor( this, 0, 1, 3, 0, 1, 2),
                                 ComputeMinor( this, 0, 1, 2, 0, 1, 2));
        }

        #endregion Methods Computation

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AMatrix<float> InternalClone()
        {
            return new Matrix4F( this.mData[ 0, 0 ], this.mData[ 0, 1 ], this.mData[ 0, 2 ], this.mData[ 0, 3 ],
                                 this.mData[ 1, 0 ], this.mData[ 1, 1 ], this.mData[ 1, 2 ], this.mData[ 1, 3 ],
                                 this.mData[ 2, 0 ], this.mData[ 2, 1 ], this.mData[ 2, 2 ], this.mData[ 2, 3 ],
                                 this.mData[ 3, 0 ], this.mData[ 3, 1 ], this.mData[ 3, 2 ], this.mData[ 3, 3 ] );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToFloat(object pToCast)
        {
            if ( pToCast is Matrix4<float> )
            {
                return (pToCast as Matrix4<float>).Clone();
            }
            else if ( pToCast is Matrix4<double> )
            {
                Matrix4<double> lCast = pToCast as Matrix4<double>;
                return new Matrix4F( (float)lCast.M00, (float)lCast.M01, (float)lCast.M02, (float)lCast.M03,
                                     (float)lCast.M10, (float)lCast.M11, (float)lCast.M12, (float)lCast.M13,
                                     (float)lCast.M20, (float)lCast.M21, (float)lCast.M22, (float)lCast.M23,
                                     (float)lCast.M30, (float)lCast.M31, (float)lCast.M32, (float)lCast.M33 );
            }
            else if ( pToCast is Matrix4<int> )
            {
                Matrix4<int> lCast = pToCast as Matrix4<int>;
                return new Matrix4F( (float)lCast.M00, (float)lCast.M01, (float)lCast.M02, (float)lCast.M03,
                                     (float)lCast.M10, (float)lCast.M11, (float)lCast.M12, (float)lCast.M13,
                                     (float)lCast.M20, (float)lCast.M21, (float)lCast.M22, (float)lCast.M23,
                                     (float)lCast.M30, (float)lCast.M31, (float)lCast.M32, (float)lCast.M33 );
            }
            else if ( pToCast is Matrix4<Half> )
            {
                Matrix4<Half> lCast = pToCast as Matrix4<Half>;
                return new Matrix4F( (float)lCast.M00, (float)lCast.M01, (float)lCast.M02, (float)lCast.M03,
                                     (float)lCast.M10, (float)lCast.M11, (float)lCast.M12, (float)lCast.M13,
                                     (float)lCast.M20, (float)lCast.M21, (float)lCast.M22, (float)lCast.M23,
                                     (float)lCast.M30, (float)lCast.M31, (float)lCast.M32, (float)lCast.M33 );
            }

            return null;
        }

        #endregion Methods Caster

        #region Methods Internal

        /// <summary>
        /// Computes a Minor of the given matrix using the specified indexed values.
        /// </summary>
        /// <param name="pMatrix"></param>
        /// <param name="r0"></param>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <param name="c0"></param>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        private static float ComputeMinor(Matrix4F pMatrix, int r0, int r1, int r2, int c0, int c1, int c2)
        {
            return pMatrix.mData[r0, c0] * ( pMatrix.mData[r1, c1] * pMatrix.mData[r2, c2] - pMatrix.mData[r2, c1] * pMatrix.mData[r1, c2] ) -
                   pMatrix.mData[r0, c1] * ( pMatrix.mData[r1, c0] * pMatrix.mData[r2, c2] - pMatrix.mData[r2, c0] * pMatrix.mData[r1, c2] ) +
                   pMatrix.mData[r0, c2] * ( pMatrix.mData[r1, c0] * pMatrix.mData[r2, c1] - pMatrix.mData[r2, c0] * pMatrix.mData[r1, c1] );
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
