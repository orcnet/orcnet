﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="MatrixCache{T}"/> class.
    /// </summary>
    public class MatrixCache<T> : IEquatable<MatrixCache<T>> where T : struct
    {
        #region Fields

        /// <summary>
        /// Stores the cache width and height. Square.
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the matrix row data row major by default.
        /// </summary>
        private T[,] mData;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the data as 1D array.
        /// </summary>
        public T[] Data
        {
            get
            {
                int lLength = this.mWidth * this.mWidth;
                int lSizeInBytes = Marshal.SizeOf( typeof(T) ) * lLength;

                T[] lFlattened = new T[ lLength ]; // Copy flattened.
                Buffer.BlockCopy( this.mData, 0, lFlattened, 0, lSizeInBytes );

                return lFlattened;
            }
        }

        /// <summary>
        /// Gets the data at the given row and column.
        /// </summary>
        /// <returns>The data.</returns>
        public T this[int pRow, int pColumn]
        {
            get
            {
                return this.mData[ pRow, pColumn ];
            }
            set
            {
                this.mData[ pRow, pColumn ] = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MatrixCache{T}"/> class.
        /// </summary>
        /// <param name="pWidthSquared">The width and height merged into a squared value</param>
        public MatrixCache(int pWidthSquared)
        {
            this.mWidth = (int)System.Math.Sqrt( (double)pWidthSquared );
            this.mData  = new T[ this.mWidth, this.mWidth ];
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Turns this matrix cache into a string.
        /// </summary>
        /// <returns>The string representation of this matrix cache.</returns>
        public override string ToString()
        {
            StringBuilder lBuilder = new StringBuilder( "(" );
            lBuilder.Append( this.mData[ 0, 0 ] );

            int lStartColumn = 1;
            int lLength = this.mWidth;
            for ( int lCurrx = 0; lCurrx < lLength; lCurrx++ )
            {
                for ( int lCurry = lStartColumn; lCurry < lLength; lCurry++ )
                {
                    lBuilder.Append( ", " );
                    lBuilder.Append( this.mData[ lCurrx, lCurry ] );
                }

                lStartColumn = 0;
            }

            lBuilder.Append( ")" );
            return lBuilder.ToString();
        }

        /// <summary>
        /// Compares the specified instances for equality.
        /// </summary>
        /// <param name="pFirst">The first matrix cache to test.</param>
        /// <param name="pSecond">The second matrix cache to test.</param>
        /// <returns>True if both instances are equal, false otherwise.</returns>
        public static bool operator == (MatrixCache<T> pFirst, MatrixCache<T> pSecond)
        {
            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Compares the specified instances for inequality.
        /// </summary>
        /// <param name="pFirst">The first matrix cache to test.</param>
        /// <param name="pSecond">The second matrix cache to test.</param>
        /// <returns>True if both instances are not equal, false otherwise.</returns>
        public static bool operator != (MatrixCache<T> pFirst, MatrixCache<T> pSecond)
        {
            return pFirst.Equals( pSecond ) == false;
        }

        /// <summary>
        /// Checks whether this matrix cache is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other matrix cache to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            MatrixCache<T> lOther = pOther as MatrixCache<T>;
            if ( lOther != null )
            {
                return this.Equals( lOther );
            }

            return false;
        }

        /// <summary>
        /// Gets the hashcode.
        /// </summary>
        /// <returns>The hashcode.</returns>
        public override int GetHashCode()
        {
            return this.mData.GetHashCode() ^ this.mWidth.GetHashCode();
        }

        /// <summary>
        /// Checks whether this matrix cache is equel to another or not.
        /// </summary>
        /// <param name="pOther">The other matrix cache to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(MatrixCache<T> pOther)
        {
            if ( this.mWidth != pOther.mWidth )
            {
                return false;
            }

            int lLength = this.mWidth;
            for ( int lCurrx = 0; lCurrx < lLength; lCurrx++ )
            {
                for ( int lCurry = 0; lCurry < lLength; lCurry++ )
                {
                    if ( this.mData[ lCurrx, lCurry ].Equals( pOther.mData[ lCurrx, lCurry ] ) == false )
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        #endregion Methods
    }
}
