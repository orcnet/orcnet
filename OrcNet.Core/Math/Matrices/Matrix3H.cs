﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Matrix3H"/> class.
    /// </summary>
    public class Matrix3H : Matrix3<Half>
    {
        #region Properties

        /// <summary>
        /// Gets a Zero matrix.
        /// </summary>
        public static Matrix3H ZERO
        {
            get
            {
                return new Matrix3H( new Half( 0.0f ) );
            }
        }

        /// <summary>
        /// Gets an identity matrix.
        /// </summary>
        public static Matrix3H IDENTITY
        {
            get
            {
                return new Matrix3H();
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(ushort) * cComponentCount;
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #region Properties IMatrix

        /// <summary>
        /// Gets the matrix determinant.
        /// </summary>
        public override Half Determinant
        {
            get
            {
                Half lCofactor00 = this.mData[ 1, 1 ] * this.mData[ 2, 2 ] - this.mData[ 1, 2 ] * this.mData[ 2, 1 ];
                Half lCofactor10 = this.mData[ 1, 2 ] * this.mData[ 2, 0 ] - this.mData[ 1, 0 ] * this.mData[ 2, 2 ];
                Half lCofactor20 = this.mData[ 1, 0 ] * this.mData[ 2, 1 ] - this.mData[ 1, 1 ] * this.mData[ 2, 0 ];
                Half lDet = this.mData[ 0, 0 ] * lCofactor00 +
                            this.mData[ 0, 1 ] * lCofactor10 +
                            this.mData[ 0, 2 ] * lCofactor20;

                return lDet;
            }
        }

        /// <summary>
        /// Gets the matrix trace.
        /// </summary>
        public override Half Trace
        {
            get
            {
                return this.mData[0, 0] + this.mData[1, 1] + this.mData[2, 2];
            }
        }

        #endregion Properties IMatrix

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Matrix3H"/> class.
        /// </summary>
        static Matrix3H()
        {
            sCasters.Add( typeof(Half), CastToHalf );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3H"/> class.
        /// Identity matrix by default.
        /// </summary>
        public Matrix3H() :
        this( new Half( 1.0f ), new Half( 0.0f ), new Half( 0.0f ), 
              new Half( 0.0f ), new Half( 1.0f ), new Half( 0.0f ),
              new Half( 0.0f ), new Half( 0.0f ), new Half( 1.0f ) )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3H"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Matrix3H(Half pValue) :
        this( pValue, pValue, pValue, 
              pValue, pValue, pValue, 
              pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3H"/> class.
        /// </summary>
        /// <param name="p00">The (0, 0) value of the matrix.</param>
        /// <param name="p01">The (0, 1) value of the matrix.</param>
        /// <param name="p02">The (0, 2) value of the matrix.</param>
        /// <param name="p10">The (1, 0) value of the matrix.</param>
        /// <param name="p11">The (1, 1) value of the matrix.</param>
        /// <param name="p12">The (1, 2) value of the matrix.</param>
        /// <param name="p20">The (2, 0) value of the matrix.</param>
        /// <param name="p21">The (2, 1) value of the matrix.</param>
        /// <param name="p22">The (2, 2) value of the matrix.</param>
        public Matrix3H(Half p00, Half p01, Half p02,
                        Half p10, Half p11, Half p12,
                        Half p20, Half p21, Half p22) :
        base( p00, p01, p02, 
              p10, p11, p12, 
              p20, p21, p22 )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Computation

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        protected override AMatrix<Half> InternalAdd(AMatrix<Half> pOther)
        {
            Matrix3H lOther = pOther as Matrix3H;
            Matrix3H lResult = new Matrix3H();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] + lOther.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        protected override AMatrix<Half> InternalSubtract(AMatrix<Half> pOther)
        {
            Matrix3H lOther = pOther as Matrix3H;
            Matrix3H lResult = new Matrix3H();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] - lOther.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<Half> InternalMultiply(Half pScale)
        {
            Matrix3H lResult = new Matrix3H();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] * pScale;
                }
            }

            return lResult;
        }

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        protected override AVector<Half> InternalMultiply(AVector<Half> pVector)
        {
            Vector2H lVector2 = pVector as Vector2H;
            if ( lVector2 != null )
            {
                Vector3H l2To3 = new Vector3H( lVector2.X, lVector2.Y, new Half( 1.0f ) );
                Vector3H lProd = new Vector3H();
                for ( int lRow = 0; lRow < 3; lRow++ )
                {
                    lProd[lRow] = this.mData[lRow, 0] * l2To3[0] +
                                  this.mData[lRow, 1] * l2To3[1] +
                                  this.mData[lRow, 2] * l2To3[2];
                }

                return new Vector2H( lProd.X, lProd.Y ) / lProd.Z;
            }

            Vector3H lVector3 = pVector as Vector3H;
            if ( lVector3 != null )
            {
                Vector3H lResult = new Vector3H();
                for ( int lRow = 0; lRow < 3; lRow++ )
                {
                    lResult[lRow] = this.mData[lRow, 0] * lVector3[0] +
                                    this.mData[lRow, 1] * lVector3[1] +
                                    this.mData[lRow, 2] * lVector3[2];
                }

                return lResult;
            }
            
            // Default answer.
            return pVector.Clone() as AVector<Half>;
        }

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pMatrix">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<Half> InternalMultiply(AMatrix<Half> pMatrix)
        {
            Matrix3H lOther = pMatrix as Matrix3H;
            Matrix3H lResult = new Matrix3H();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[lRow, lCol] = this.mData[lRow, 0] * lOther.mData[0, lCol] +
                                                this.mData[lRow, 1] * lOther.mData[1, lCol] +
                                                this.mData[lRow, 2] * lOther.mData[2, lCol];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        protected override ABox<Half> InternalMultiply(ABox<Half> pBox)
        {
            Box3H lBox = pBox as Box3H;
            if ( lBox != null )
            {
                Vector3H lMinimum = lBox.Minimum as Vector3H;
                Vector3H lMaximum = lBox.Maximum as Vector3H;
                IBox<Half> lResult = new Box3H();
                lResult = lResult.Enlarge( this.Multiply( new Vector3H( lMinimum.X, lMinimum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3H( lMaximum.X, lMinimum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3H( lMinimum.X, lMaximum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3H( lMaximum.X, lMaximum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3H( lMinimum.X, lMinimum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3H( lMaximum.X, lMinimum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3H( lMinimum.X, lMaximum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3H( lMaximum.X, lMaximum.Y, lMaximum.Z ) ) );

                return lResult as ABox<Half>;
            }

            // Default answer in case of box type mismatches.
            return pBox.Clone() as ABox<Half>;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        protected override AMatrix<Half> InternalTranspose()
        {
            Matrix3H lResult = new Matrix3H();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lCol, lRow ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        protected override AMatrix<Half> InternalInverse(Half pTolerance, out bool pResult)
        {
            Matrix3H lResult = new Matrix3H();
            // Invert a 3x3 using cofactors.  This is about 8 times faster than
            // the Numerical Recipes code which uses Gaussian elimination.
            lResult.mData[0, 0] = this.mData[1, 1] * this.mData[2, 2] - this.mData[1, 2] * this.mData[2, 1];
            lResult.mData[0, 1] = this.mData[0, 2] * this.mData[2, 1] - this.mData[0, 1] * this.mData[2, 2];
            lResult.mData[0, 2] = this.mData[0, 1] * this.mData[1, 2] - this.mData[0, 2] * this.mData[1, 1];
            lResult.mData[1, 0] = this.mData[1, 2] * this.mData[2, 0] - this.mData[1, 0] * this.mData[2, 2];
            lResult.mData[1, 1] = this.mData[0, 0] * this.mData[2, 2] - this.mData[0, 2] * this.mData[2, 0];
            lResult.mData[1, 2] = this.mData[0, 2] * this.mData[1, 0] - this.mData[0, 0] * this.mData[1, 2];
            lResult.mData[2, 0] = this.mData[1, 0] * this.mData[2, 1] - this.mData[1, 1] * this.mData[2, 0];
            lResult.mData[2, 1] = this.mData[0, 1] * this.mData[2, 0] - this.mData[0, 0] * this.mData[2, 1];
            lResult.mData[2, 2] = this.mData[0, 0] * this.mData[1, 1] - this.mData[0, 1] * this.mData[1, 0];

            Half lDet = this.mData[0, 0] * lResult.mData[0, 0] +
                         this.mData[0, 1] * lResult.mData[1, 0] +
                         this.mData[0, 2] * lResult.mData[2, 0];

            if ( System.Math.Abs( lDet ) <= pTolerance )
            {
                pResult = false;
                return null;
            }

            Half lInvDet = new Half( 1.0f ) / lDet;
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[lRow, lCol] *= lInvDet;
                }
            }

            pResult = true;

            return lResult;
        }

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AMatrix<Half> InternalOpposite()
        {
            Matrix3H lResult = new Matrix3H();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = new Half( -this.mData[ lRow, lCol ] );
                }
            }

            return lResult;
        }

        #endregion Methods Computation

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AMatrix<Half> InternalClone()
        {
            return new Matrix3H( this.mData[ 0, 0 ], this.mData[ 0, 1 ], this.mData[ 0, 2 ],
                                 this.mData[ 1, 0 ], this.mData[ 1, 1 ], this.mData[ 1, 2 ],
                                 this.mData[ 2, 0 ], this.mData[ 2, 1 ], this.mData[ 2, 2 ] );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToHalf(object pToCast)
        {
            if ( pToCast is Matrix3<Half> )
            {
                return (pToCast as Matrix3<Half>).Clone();
            }
            else if ( pToCast is Matrix3<double> )
            {
                Matrix3<double> lCast = pToCast as Matrix3<double>;
                return new Matrix3H( new Half( lCast.M00 ), new Half( lCast.M01 ), new Half( lCast.M02 ),
                                     new Half( lCast.M10 ), new Half( lCast.M11 ), new Half( lCast.M12 ),
                                     new Half( lCast.M20 ), new Half( lCast.M21 ), new Half( lCast.M22 ) );
            }
            else if ( pToCast is Matrix3<int> )
            {
                Matrix3<int> lCast = pToCast as Matrix3<int>;
                return new Matrix3H( new Half( lCast.M00 ), new Half( lCast.M01 ), new Half( lCast.M02 ),
                                     new Half( lCast.M10 ), new Half( lCast.M11 ), new Half( lCast.M12 ),
                                     new Half( lCast.M20 ), new Half( lCast.M21 ), new Half( lCast.M22 ) );
            }
            else if ( pToCast is Matrix3<float> )
            {
                Matrix3<float> lCast = pToCast as Matrix3<float>;
                return new Matrix3H( new Half( lCast.M00 ), new Half( lCast.M01 ), new Half( lCast.M02 ),
                                     new Half( lCast.M10 ), new Half( lCast.M11 ), new Half( lCast.M12 ),
                                     new Half( lCast.M20 ), new Half( lCast.M21 ), new Half( lCast.M22 ) );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
