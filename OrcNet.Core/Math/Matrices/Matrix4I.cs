﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Matrix4I"/> class.
    /// </summary>
    public class Matrix4I : Matrix4<int>
    {
        #region Properties

        /// <summary>
        /// Gets a Zero matrix.
        /// </summary>
        public static Matrix4I ZERO
        {
            get
            {
                return new Matrix4I( 0 );
            }
        }

        /// <summary>
        /// Gets an identity matrix.
        /// </summary>
        public static Matrix4I IDENTITY
        {
            get
            {
                return new Matrix4I();
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(int) * cComponentCount;
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #region Properties IMatrix

        /// <summary>
        /// Gets the matrix determinant.
        /// </summary>
        public override int Determinant
        {
            get
            {
                return this.mData[0, 0] * ComputeMinor( this, 1, 2, 3, 1, 2, 3 ) -
                       this.mData[0, 1] * ComputeMinor( this, 1, 2, 3, 0, 2, 3 ) +
                       this.mData[0, 2] * ComputeMinor( this, 1, 2, 3, 0, 1, 3 ) -
                       this.mData[0, 3] * ComputeMinor( this, 1, 2, 3, 0, 1, 2 );
            }
        }

        /// <summary>
        /// Gets the matrix trace.
        /// </summary>
        public override int Trace
        {
            get
            {
                return this.mData[0, 0] + this.mData[1, 1] + this.mData[2, 2] + this.mData[3, 3];
            }
        }

        #endregion Properties IMatrix

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Matrix4I"/> class.
        /// </summary>
        static Matrix4I()
        {
            sCasters.Add( typeof(int), CastToInt );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4I"/> class.
        /// Identity matrix by default.
        /// </summary>
        public Matrix4I() :
        this( 1, 0, 0, 0, 
              0, 1, 0, 0,
              0, 0, 1, 0,
              0, 0, 0, 1 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4I"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Matrix4I(int pValue) :
        this( pValue, pValue, pValue, pValue, 
              pValue, pValue, pValue, pValue, 
              pValue, pValue, pValue, pValue,
              pValue, pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4I"/> class.
        /// </summary>
        /// <param name="p00">The (0, 0) value of the matrix.</param>
        /// <param name="p01">The (0, 1) value of the matrix.</param>
        /// <param name="p02">The (0, 2) value of the matrix.</param>
        /// <param name="p03">The (0, 3) value of the matrix.</param>
        /// <param name="p10">The (1, 0) value of the matrix.</param>
        /// <param name="p11">The (1, 1) value of the matrix.</param>
        /// <param name="p12">The (1, 2) value of the matrix.</param>
        /// <param name="p13">The (1, 3) value of the matrix.</param>
        /// <param name="p20">The (2, 0) value of the matrix.</param>
        /// <param name="p21">The (2, 1) value of the matrix.</param>
        /// <param name="p22">The (2, 2) value of the matrix.</param>
        /// <param name="p23">The (2, 3) value of the matrix.</param>
        /// <param name="p30">The (3, 0) value of the matrix.</param>
        /// <param name="p31">The (3, 1) value of the matrix.</param>
        /// <param name="p32">The (3, 2) value of the matrix.</param>
        /// <param name="p33">The (3, 3) value of the matrix.</param>
        public Matrix4I(int p00, int p01, int p02, int p03,
                        int p10, int p11, int p12, int p13,
                        int p20, int p21, int p22, int p23,
                        int p30, int p31, int p32, int p33) :
        base( p00, p01, p02, p03, 
              p10, p11, p12, p13, 
              p20, p21, p22, p23,
              p30, p31, p32, p33 )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Statics

        /// <summary>
        /// Creates a new translation matrix from the given translation vector.
        /// </summary>
        /// <param name="pTranslation">THe translation vector.</param>
        /// <returns>The translation matrix.</returns>
        public static Matrix4I Translate(Vector3I pTranslation)
        {
            return new Matrix4I( 1, 0, 0, pTranslation.X,
                                 0, 1, 0, pTranslation.Y,
                                 0, 0, 1, pTranslation.Z,
                                 0, 0, 0, 1 );
        }

        /// <summary>
        /// Creates a new rotation matrix around the X axis with the given angle in degrees.
        /// </summary>
        /// <param name="pAngleInDegrees">The angle in degrees.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4I RotateX(int pAngleInDegrees)
        {
            int lCosAngle = (int)System.Math.Cos( MathUtility.ToRadians( pAngleInDegrees ) );
            int lSinAngle = (int)System.Math.Sin( MathUtility.ToRadians( pAngleInDegrees ) );
            return new Matrix4I( 1, 0, 0, 0,
                                 0, lCosAngle, -lSinAngle, 0,
                                 0, lSinAngle, lCosAngle, 0,
                                 0, 0, 0, 1);
        }

        /// <summary>
        /// Creates a new rotation matrix around the Y axis with the given angle in degrees.
        /// </summary>
        /// <param name="pAngleInDegrees">The angle in degrees.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4I RotateY(int pAngleInDegrees)
        {
            int lCosAngle = (int)System.Math.Cos( MathUtility.ToRadians( pAngleInDegrees ) );
            int lSinAngle = (int)System.Math.Sin( MathUtility.ToRadians( pAngleInDegrees ) );
            return new Matrix4I( lCosAngle, 0, lSinAngle, 0,
                                 0, 1, 0, 0,
                                 -lSinAngle, 0, lCosAngle, 0,
                                 0, 0, 0, 1 );
        }

        /// <summary>
        /// Creates a new rotation matrix around the Z axis with the given angle in degrees.
        /// </summary>
        /// <param name="pAngleInDegrees">The angle in degrees.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4I RotateZ(int pAngleInDegrees)
        {
            int lCosAngle = (int)System.Math.Cos( MathUtility.ToRadians( pAngleInDegrees ) );
            int lSinAngle = (int)System.Math.Sin( MathUtility.ToRadians( pAngleInDegrees ) );
            return new Matrix4I( lCosAngle, -lSinAngle, 0, 0,
                                 lSinAngle, lCosAngle, 0, 0,
                                 0, 0, 1, 0,
                                 0, 0, 0, 1 );
        }

        /// <summary>
        /// Creates a perspective projection matrix given the projection parameters such as the field of view, aspect ratio,
        /// the near and far clipping planes.
        /// </summary>
        /// <param name="pFovy">The field of view in degrees.</param>
        /// <param name="pAspect">The aspect ratio.</param>
        /// <param name="pNear">The near clipping plane.</param>
        /// <param name="pFar">The far clipping plane.</param>
        /// <returns>The projection matrix.</returns>
        public static Matrix4I PerspectiveProjection(int pFovy, int pAspect, int pNear, int pFar)
        {
            int f = (int)(1.0 / System.Math.Tan( MathUtility.ToRadians( pFovy ) / 2 ));
            return new Matrix4I( f / pAspect, 0, 0, 0,
                                 0, f, 0, 0,
                                 0, 0, (pFar + pNear) / (pNear - pFar), (2 * pFar * pNear) / (pNear - pFar),
                                 0, 0, -1, 0 );
        }

        /// <summary>
        /// Creates an orthographic projection matrix given the projection parameters such as the right, left, top, bottom, near and far clipping planes.
        /// </summary>
        /// <param name="pRight">The right clipping plane.</param>
        /// <param name="pLeft">The left clipping plane.</param>
        /// <param name="pTop">The top clipping plane.</param>
        /// <param name="pBottom">The bottom clipping plane.</param>
        /// <param name="pNear">The near clipping plane.</param>
        /// <param name="pFar">The far clipping plane.</param>
        /// <returns>The projection matrix.</returns>
        public static Matrix4I OrthographicPorjection(int pRight, int pLeft, int pTop, int pBottom, int pNear, int pFar)
        {
            int lTx = -(pRight + pLeft) / (pRight - pLeft);
            int lTy = -(pTop + pBottom) / (pTop - pBottom);
            int lTz = -(pFar + pNear) / (pFar - pNear);
            return new Matrix4I( 2 / (pRight - pLeft), 0, 0, lTx,
                                 0, 2 / (pTop - pBottom), 0, lTy,
                                 0, 0, -2 / (pFar - pNear), lTz,
                                 0, 0,  0, 1 );
        }

        #endregion Methods Statics

        #region Methods Computation

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        protected override AMatrix<int> InternalAdd(AMatrix<int> pOther)
        {
            Matrix4I lOther = pOther as Matrix4I;
            Matrix4I lResult = new Matrix4I();

            lResult.mData[0, 0] = this.mData[0, 0] + lOther.mData[0, 0];
            lResult.mData[0, 1] = this.mData[0, 1] + lOther.mData[0, 1];
            lResult.mData[0, 2] = this.mData[0, 2] + lOther.mData[0, 2];
            lResult.mData[0, 3] = this.mData[0, 3] + lOther.mData[0, 3];

            lResult.mData[1, 0] = this.mData[1, 0] + lOther.mData[1, 0];
            lResult.mData[1, 1] = this.mData[1, 1] + lOther.mData[1, 1];
            lResult.mData[1, 2] = this.mData[1, 2] + lOther.mData[1, 2];
            lResult.mData[1, 3] = this.mData[1, 3] + lOther.mData[1, 3];

            lResult.mData[2, 0] = this.mData[2, 0] + lOther.mData[2, 0];
            lResult.mData[2, 1] = this.mData[2, 1] + lOther.mData[2, 1];
            lResult.mData[2, 2] = this.mData[2, 2] + lOther.mData[2, 2];
            lResult.mData[2, 3] = this.mData[2, 3] + lOther.mData[2, 3];

            lResult.mData[3, 0] = this.mData[3, 0] + lOther.mData[3, 0];
            lResult.mData[3, 1] = this.mData[3, 1] + lOther.mData[3, 1];
            lResult.mData[3, 2] = this.mData[3, 2] + lOther.mData[3, 2];
            lResult.mData[3, 3] = this.mData[3, 3] + lOther.mData[3, 3];

            return lResult;
        }

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        protected override AMatrix<int> InternalSubtract(AMatrix<int> pOther)
        {
            Matrix4I lOther = pOther as Matrix4I;
            Matrix4I lResult = new Matrix4I();

            lResult.mData[0, 0] = this.mData[0, 0] - lOther.mData[0, 0];
            lResult.mData[0, 1] = this.mData[0, 1] - lOther.mData[0, 1];
            lResult.mData[0, 2] = this.mData[0, 2] - lOther.mData[0, 2];
            lResult.mData[0, 3] = this.mData[0, 3] - lOther.mData[0, 3];

            lResult.mData[1, 0] = this.mData[1, 0] - lOther.mData[1, 0];
            lResult.mData[1, 1] = this.mData[1, 1] - lOther.mData[1, 1];
            lResult.mData[1, 2] = this.mData[1, 2] - lOther.mData[1, 2];
            lResult.mData[1, 3] = this.mData[1, 3] - lOther.mData[1, 3];

            lResult.mData[2, 0] = this.mData[2, 0] - lOther.mData[2, 0];
            lResult.mData[2, 1] = this.mData[2, 1] - lOther.mData[2, 1];
            lResult.mData[2, 2] = this.mData[2, 2] - lOther.mData[2, 2];
            lResult.mData[2, 3] = this.mData[2, 3] - lOther.mData[2, 3];

            lResult.mData[3, 0] = this.mData[3, 0] - lOther.mData[3, 0];
            lResult.mData[3, 1] = this.mData[3, 1] - lOther.mData[3, 1];
            lResult.mData[3, 2] = this.mData[3, 2] - lOther.mData[3, 2];
            lResult.mData[3, 3] = this.mData[3, 3] - lOther.mData[3, 3];

            return lResult;
        }

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<int> InternalMultiply(int pScale)
        {
            Matrix4I lResult = new Matrix4I();
            
            lResult.mData[0, 0] = this.mData[0, 0] * pScale;
            lResult.mData[0, 1] = this.mData[0, 1] * pScale;
            lResult.mData[0, 2] = this.mData[0, 2] * pScale;
            lResult.mData[0, 3] = this.mData[0, 3] * pScale;

            lResult.mData[1, 0] = this.mData[1, 0] * pScale;
            lResult.mData[1, 1] = this.mData[1, 1] * pScale;
            lResult.mData[1, 2] = this.mData[1, 2] * pScale;
            lResult.mData[1, 3] = this.mData[1, 3] * pScale;

            lResult.mData[2, 0] = this.mData[2, 0] * pScale;
            lResult.mData[2, 1] = this.mData[2, 1] * pScale;
            lResult.mData[2, 2] = this.mData[2, 2] * pScale;
            lResult.mData[2, 3] = this.mData[2, 3] * pScale;

            lResult.mData[3, 0] = this.mData[3, 0] * pScale;
            lResult.mData[3, 1] = this.mData[3, 1] * pScale;
            lResult.mData[3, 2] = this.mData[3, 2] * pScale;
            lResult.mData[3, 3] = this.mData[3, 3] * pScale;

            return lResult;
        }

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        protected override AVector<int> InternalMultiply(AVector<int> pVector)
        {
            Vector3I lVector3 = pVector as Vector3I;
            if ( lVector3 != null )
            {
                int lInvW = 1 / (this.mData[3, 0] * lVector3.X + this.mData[3, 1] * lVector3.Y + this.mData[3, 2] * lVector3.Z + this.mData[3, 3]);
                return new Vector3I( (this.mData[0, 0] * lVector3.X + this.mData[0, 1] * lVector3.Y + this.mData[0, 2] * lVector3.Z + this.mData[0, 3]) * lInvW,
                                     (this.mData[1, 0] * lVector3.X + this.mData[1, 1] * lVector3.Y + this.mData[1, 2] * lVector3.Z + this.mData[1, 3]) * lInvW,
                                     (this.mData[2, 0] * lVector3.X + this.mData[2, 1] * lVector3.Y + this.mData[2, 2] * lVector3.Z + this.mData[2, 3]) * lInvW );
            }

            Vector4I lVector4 = pVector as Vector4I;
            if ( lVector4 != null )
            {
                return new Vector4I( this.mData[0, 0] * lVector4.X + this.mData[0, 1] * lVector4.Y + this.mData[0, 2] * lVector4.Z + this.mData[0, 3] * lVector4.W,
                                     this.mData[1, 0] * lVector4.X + this.mData[1, 1] * lVector4.Y + this.mData[1, 2] * lVector4.Z + this.mData[1, 3] * lVector4.W,
                                     this.mData[2, 0] * lVector4.X + this.mData[2, 1] * lVector4.Y + this.mData[2, 2] * lVector4.Z + this.mData[2, 3] * lVector4.W,
                                     this.mData[3, 0] * lVector4.X + this.mData[3, 1] * lVector4.Y + this.mData[3, 2] * lVector4.Z + this.mData[3, 3] * lVector4.W );
            }
            
            // Default answer.
            return pVector.Clone() as AVector<int>;
        }

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pMatrix">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<int> InternalMultiply(AMatrix<int> pMatrix)
        {
            Matrix4I lOther = pMatrix as Matrix4I;
            Matrix4I lResult = new Matrix4I();

            lResult.mData[0, 0] = this.mData[0, 0] * lOther.mData[0, 0] + this.mData[0, 1] * lOther.mData[1, 0] + this.mData[0, 2] * lOther.mData[2, 0] + this.mData[0, 3] * lOther.mData[3, 0];
            lResult.mData[0, 1] = this.mData[0, 0] * lOther.mData[0, 1] + this.mData[0, 1] * lOther.mData[1, 1] + this.mData[0, 2] * lOther.mData[2, 1] + this.mData[0, 3] * lOther.mData[3, 1];
            lResult.mData[0, 2] = this.mData[0, 0] * lOther.mData[0, 2] + this.mData[0, 1] * lOther.mData[1, 2] + this.mData[0, 2] * lOther.mData[2, 2] + this.mData[0, 3] * lOther.mData[3, 2];
            lResult.mData[0, 3] = this.mData[0, 0] * lOther.mData[0, 3] + this.mData[0, 1] * lOther.mData[1, 3] + this.mData[0, 2] * lOther.mData[2, 3] + this.mData[0, 3] * lOther.mData[3, 3];

            lResult.mData[1, 0] = this.mData[1, 0] * lOther.mData[0, 0] + this.mData[1, 1] * lOther.mData[1, 0] + this.mData[1, 2] * lOther.mData[2, 0] + this.mData[1, 3] * lOther.mData[3, 0];
            lResult.mData[1, 1] = this.mData[1, 0] * lOther.mData[0, 1] + this.mData[1, 1] * lOther.mData[1, 1] + this.mData[1, 2] * lOther.mData[2, 1] + this.mData[1, 3] * lOther.mData[3, 1];
            lResult.mData[1, 2] = this.mData[1, 0] * lOther.mData[0, 2] + this.mData[1, 1] * lOther.mData[1, 2] + this.mData[1, 2] * lOther.mData[2, 2] + this.mData[1, 3] * lOther.mData[3, 2];
            lResult.mData[1, 3] = this.mData[1, 0] * lOther.mData[0, 3] + this.mData[1, 1] * lOther.mData[1, 3] + this.mData[1, 2] * lOther.mData[2, 3] + this.mData[1, 3] * lOther.mData[3, 3];

            lResult.mData[2, 0] = this.mData[2, 0] * lOther.mData[0, 0] + this.mData[2, 1] * lOther.mData[1, 0] + this.mData[2, 2] * lOther.mData[2, 0] + this.mData[2, 3] * lOther.mData[3, 0];
            lResult.mData[2, 1] = this.mData[2, 0] * lOther.mData[0, 1] + this.mData[2, 1] * lOther.mData[1, 1] + this.mData[2, 2] * lOther.mData[2, 1] + this.mData[2, 3] * lOther.mData[3, 1];
            lResult.mData[2, 2] = this.mData[2, 0] * lOther.mData[0, 2] + this.mData[2, 1] * lOther.mData[1, 2] + this.mData[2, 2] * lOther.mData[2, 2] + this.mData[2, 3] * lOther.mData[3, 2];
            lResult.mData[2, 3] = this.mData[2, 0] * lOther.mData[0, 3] + this.mData[2, 1] * lOther.mData[1, 3] + this.mData[2, 2] * lOther.mData[2, 3] + this.mData[2, 3] * lOther.mData[3, 3];

            lResult.mData[3, 0] = this.mData[3, 0] * lOther.mData[0, 0] + this.mData[3, 1] * lOther.mData[1, 0] + this.mData[3, 2] * lOther.mData[2, 0] + this.mData[3, 3] * lOther.mData[3, 0];
            lResult.mData[3, 1] = this.mData[3, 0] * lOther.mData[0, 1] + this.mData[3, 1] * lOther.mData[1, 1] + this.mData[3, 2] * lOther.mData[2, 1] + this.mData[3, 3] * lOther.mData[3, 1];
            lResult.mData[3, 2] = this.mData[3, 0] * lOther.mData[0, 2] + this.mData[3, 1] * lOther.mData[1, 2] + this.mData[3, 2] * lOther.mData[2, 2] + this.mData[3, 3] * lOther.mData[3, 2];
            lResult.mData[3, 3] = this.mData[3, 0] * lOther.mData[0, 3] + this.mData[3, 1] * lOther.mData[1, 3] + this.mData[3, 2] * lOther.mData[2, 3] + this.mData[3, 3] * lOther.mData[3, 3];

            return lResult;
        }

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        protected override ABox<int> InternalMultiply(ABox<int> pBox)
        {
            Box3I lBox = pBox as Box3I;
            if ( lBox != null )
            {
                Vector3I lMinimum = lBox.Minimum as Vector3I;
                Vector3I lMaximum = lBox.Maximum as Vector3I;
                IBox<int> lResult = new Box3I();
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMinimum.X, lMinimum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMaximum.X, lMinimum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMinimum.X, lMaximum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMaximum.X, lMaximum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMinimum.X, lMinimum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMaximum.X, lMinimum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMinimum.X, lMaximum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMaximum.X, lMaximum.Y, lMaximum.Z ) ) );

                return lResult as ABox<int>;
            }

            // Default answer in case of box type mismatches.
            return pBox.Clone() as ABox<int>;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        protected override AMatrix<int> InternalTranspose()
        {
            return new Matrix4I( this.mData[0, 0], this.mData[1, 0], this.mData[2, 0], this.mData[3, 0],
                                 this.mData[0, 1], this.mData[1, 1], this.mData[2, 1], this.mData[3, 1],
                                 this.mData[0, 2], this.mData[1, 2], this.mData[2, 2], this.mData[3, 2],
                                 this.mData[0, 3], this.mData[1, 3], this.mData[2, 3], this.mData[3, 3] );
        }

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        protected override AMatrix<int> InternalInverse(int pTolerance, out bool pResult)
        {
            pResult = true;
            return this.Adjoint() * ( 1 / this.Determinant );
        }

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AMatrix<int> InternalOpposite()
        {
            Matrix4I lResult = new Matrix4I();
            for ( int lRow = 0; lRow < 4; lRow++ )
            {
                for ( int lCol = 0; lCol < 4; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = -this.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Turns this matrix into a 3x3 matrix.
        /// </summary>
        /// <returns>The new matrix 3x3</returns>
        public Matrix3I To3x3()
        {
            return new Matrix3I( this.mData[0, 0], this.mData[0, 1], this.mData[0, 2],
                                 this.mData[1, 0], this.mData[1, 1], this.mData[1, 2],
                                 this.mData[2, 0], this.mData[2, 1], this.mData[2, 2] );
        }

        /// <summary>
        /// Computes the adjoint of this matrix.
        /// </summary>
        /// <returns>The adjoint matrix.</returns>
        public Matrix4I Adjoint()
        {
            return new Matrix4I( ComputeMinor( this, 1, 2, 3, 1, 2, 3),
                                 -ComputeMinor( this, 0, 2, 3, 1, 2, 3),
                                 ComputeMinor( this, 0, 1, 3, 1, 2, 3),
                                 -ComputeMinor( this, 0, 1, 2, 1, 2, 3),

                                 -ComputeMinor( this, 1, 2, 3, 0, 2, 3),
                                 ComputeMinor( this, 0, 2, 3, 0, 2, 3),
                                 -ComputeMinor( this, 0, 1, 3, 0, 2, 3),
                                 ComputeMinor( this, 0, 1, 2, 0, 2, 3),

                                 ComputeMinor( this, 1, 2, 3, 0, 1, 3),
                                 -ComputeMinor( this, 0, 2, 3, 0, 1, 3),
                                 ComputeMinor( this, 0, 1, 3, 0, 1, 3),
                                 -ComputeMinor( this, 0, 1, 2, 0, 1, 3),

                                 -ComputeMinor( this, 1, 2, 3, 0, 1, 2),
                                 ComputeMinor( this, 0, 2, 3, 0, 1, 2),
                                 -ComputeMinor( this, 0, 1, 3, 0, 1, 2),
                                 ComputeMinor( this, 0, 1, 2, 0, 1, 2));
        }

        #endregion Methods Computation

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AMatrix<int> InternalClone()
        {
            return new Matrix4I( this.mData[ 0, 0 ], this.mData[ 0, 1 ], this.mData[ 0, 2 ], this.mData[ 0, 3 ],
                                 this.mData[ 1, 0 ], this.mData[ 1, 1 ], this.mData[ 1, 2 ], this.mData[ 1, 3 ],
                                 this.mData[ 2, 0 ], this.mData[ 2, 1 ], this.mData[ 2, 2 ], this.mData[ 2, 3 ],
                                 this.mData[ 3, 0 ], this.mData[ 3, 1 ], this.mData[ 3, 2 ], this.mData[ 3, 3 ] );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToInt(object pToCast)
        {
            if ( pToCast is Matrix4<int> )
            {
                return (pToCast as Matrix4<int>).Clone();
            }
            else if ( pToCast is Matrix4<double> )
            {
                Matrix4<double> lCast = pToCast as Matrix4<double>;
                return new Matrix4I( (int)lCast.M00, (int)lCast.M01, (int)lCast.M02, (int)lCast.M03,
                                     (int)lCast.M10, (int)lCast.M11, (int)lCast.M12, (int)lCast.M13,
                                     (int)lCast.M20, (int)lCast.M21, (int)lCast.M22, (int)lCast.M23,
                                     (int)lCast.M30, (int)lCast.M31, (int)lCast.M32, (int)lCast.M33 );
            }
            else if ( pToCast is Matrix4<float> )
            {
                Matrix4<float> lCast = pToCast as Matrix4<float>;
                return new Matrix4I( (int)lCast.M00, (int)lCast.M01, (int)lCast.M02, (int)lCast.M03,
                                     (int)lCast.M10, (int)lCast.M11, (int)lCast.M12, (int)lCast.M13,
                                     (int)lCast.M20, (int)lCast.M21, (int)lCast.M22, (int)lCast.M23,
                                     (int)lCast.M30, (int)lCast.M31, (int)lCast.M32, (int)lCast.M33 );
            }
            else if ( pToCast is Matrix4<Half> )
            {
                Matrix4<Half> lCast = pToCast as Matrix4<Half>;
                return new Matrix4I( (int)lCast.M00, (int)lCast.M01, (int)lCast.M02, (int)lCast.M03,
                                     (int)lCast.M10, (int)lCast.M11, (int)lCast.M12, (int)lCast.M13,
                                     (int)lCast.M20, (int)lCast.M21, (int)lCast.M22, (int)lCast.M23,
                                     (int)lCast.M30, (int)lCast.M31, (int)lCast.M32, (int)lCast.M33 );
            }

            return null;
        }

        #endregion Methods Caster

        #region Methods Internal

        /// <summary>
        /// Computes a Minor of the given matrix using the specified indexed values.
        /// </summary>
        /// <param name="pMatrix"></param>
        /// <param name="r0"></param>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <param name="c0"></param>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        private static int ComputeMinor(Matrix4I pMatrix, int r0, int r1, int r2, int c0, int c1, int c2)
        {
            return pMatrix.mData[r0, c0] * ( pMatrix.mData[r1, c1] * pMatrix.mData[r2, c2] - pMatrix.mData[r2, c1] * pMatrix.mData[r1, c2] ) -
                   pMatrix.mData[r0, c1] * ( pMatrix.mData[r1, c0] * pMatrix.mData[r2, c2] - pMatrix.mData[r2, c0] * pMatrix.mData[r1, c2] ) +
                   pMatrix.mData[r0, c2] * ( pMatrix.mData[r1, c0] * pMatrix.mData[r2, c1] - pMatrix.mData[r2, c0] * pMatrix.mData[r1, c1] );
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
