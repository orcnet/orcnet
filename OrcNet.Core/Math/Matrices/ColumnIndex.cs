﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Helper structure to access by index one of the matrix column.
    /// </summary>
    public struct ColumnIndex
    {
        #region Fields

        /// <summary>
        /// Stores the column index
        /// </summary>
        private int mIndex;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnIndex"/> class.
        /// </summary>
        /// <param name="pIndex">The column index.</param>
        public ColumnIndex(int pIndex)
        {
            this.mIndex = pIndex;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from ColumnIndex to int
        /// </summary>
        /// <param name="pIndex">The column index</param>
        public static implicit operator int(ColumnIndex pIndex)
        {
            return pIndex.mIndex;
        }

        #endregion Methods
    }
}
