﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Matrix2I"/> class.
    /// </summary>
    public class Matrix2I : Matrix2<int>
    {
        #region Properties

        /// <summary>
        /// Gets a Zero matrix.
        /// </summary>
        public static Matrix2I ZERO
        {
            get
            {
                return new Matrix2I( 0 );
            }
        }

        /// <summary>
        /// Gets an identity matrix.
        /// </summary>
        public static Matrix2I IDENTITY
        {
            get
            {
                return new Matrix2I();
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(int) * cComponentCount;
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #region Properties IMatrix

        /// <summary>
        /// Gets the matrix determinant.
        /// </summary>
        public override int Determinant
        {
            get
            {
                return this.mData[0, 0] * this.mData[1, 1] - this.mData[1, 0] * this.mData[0, 1];
            }
        }

        /// <summary>
        /// Gets the matrix trace.
        /// </summary>
        public override int Trace
        {
            get
            {
                return this.mData[0, 0] + this.mData[1, 1];
            }
        }

        #endregion Properties IMatrix

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Matrix2I"/> class.
        /// </summary>
        static Matrix2I()
        {
            sCasters.Add( typeof(int), CastToInt );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2I"/> class.
        /// Identity matrix by default.
        /// </summary>
        public Matrix2I() :
        this( 1, 0, 0, 1 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2I"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Matrix2I(int pValue) :
        this( pValue, pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2I"/> class.
        /// </summary>
        /// <param name="p00">The (0, 0) value of the matrix.</param>
        /// <param name="p01">The (0, 1) value of the matrix.</param>
        /// <param name="p10">The (1, 0) value of the matrix.</param>
        /// <param name="p11">The (1, 1) value of the matrix.</param>
        public Matrix2I(int p00, int p01,
                        int p10, int p11) :
        base( p00, p01, p10, p11 )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Computation

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        protected override AMatrix<int> InternalAdd(AMatrix<int> pOther)
        {
            Matrix2I lOther = pOther as Matrix2I;
            Matrix2I lResult = new Matrix2I();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] + lOther.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        protected override AMatrix<int> InternalSubtract(AMatrix<int> pOther)
        {
            Matrix2I lOther = pOther as Matrix2I;
            Matrix2I lResult = new Matrix2I();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] - lOther.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<int> InternalMultiply(int pScale)
        {
            Matrix2I lResult = new Matrix2I();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] * pScale;
                }
            }

            return lResult;
        }

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        protected override AVector<int> InternalMultiply(AVector<int> pVector)
        {
            Vector2I lVector = pVector as Vector2I;
            Vector2I lResult = new Vector2I();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                lResult[ lRow ] = this.mData[ lRow, 0 ] * lVector.X + this.mData[ lRow, 1 ] * lVector.Y;
            }

            return lResult;
        }

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pMatrix">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<int> InternalMultiply(AMatrix<int> pMatrix)
        {
            Matrix2I lOther = pMatrix as Matrix2I;
            Matrix2I lResult = new Matrix2I();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++)
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, 0 ] * lOther.mData[ 0, lCol ] + this.mData[ lRow, 1 ] * lOther.mData[ 1, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        protected override ABox<int> InternalMultiply(ABox<int> pBox)
        {
            Box2I lBox = pBox as Box2I;
            if ( lBox != null )
            {
                Vector2I lMinimum = lBox.Minimum as Vector2I;
                Vector2I lMaximum = lBox.Maximum as Vector2I;
                IBox<int> lResult = new Box2I();
                lResult = lResult.Enlarge( this.Multiply( new Vector2I( lMinimum.X, lMinimum.Y ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector2I( lMaximum.X, lMinimum.Y ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector2I( lMinimum.X, lMaximum.Y ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector2I( lMaximum.X, lMaximum.Y ) ) );

                return lResult as ABox<int>;
            }

            // Default answer in case of box type mismatches.
            return pBox.Clone() as ABox<int>;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        protected override AMatrix<int> InternalTranspose()
        {
            Matrix2I lResult = new Matrix2I();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lCol, lRow ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        protected override AMatrix<int> InternalInverse(int pTolerance, out bool pResult)
        {
            int lDeterminant = this.Determinant;

            if ( System.Math.Abs( lDeterminant ) <= pTolerance )
            {
                pResult = false;
                return null;
            }

            Matrix2I lResult = new Matrix2I();

            int lInvDet = 1 / lDeterminant;

            lResult.mData[ 0, 0 ] =  this.mData[ 1, 1 ] * lInvDet;
            lResult.mData[ 0, 1 ] = -this.mData[ 0, 1 ] * lInvDet;
            lResult.mData[ 1, 0 ] = -this.mData[ 1, 0 ] * lInvDet;
            lResult.mData[ 1, 1 ] =  this.mData[ 0, 0 ] * lInvDet;
            pResult = true;

            return lResult;
        }

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AMatrix<int> InternalOpposite()
        {
            Matrix2I lResult = new Matrix2I();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = -this.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        #endregion Methods Computation

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AMatrix<int> InternalClone()
        {
            return new Matrix2I( this.mData[ 0, 0 ], this.mData[ 0, 1 ],
                                 this.mData[ 1, 0 ], this.mData[ 1, 1 ] );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToInt(object pToCast)
        {
            if ( pToCast is Matrix2<int> )
            {
                return (pToCast as Matrix2<int>).Clone();
            }
            else if ( pToCast is Matrix2<float> )
            {
                Matrix2<float> lCast = pToCast as Matrix2<float>;
                return new Matrix2I( (int)lCast.M00, (int)lCast.M01,
                                     (int)lCast.M10, (int)lCast.M11 );
            }
            else if ( pToCast is Matrix2<double> )
            {
                Matrix2<double> lCast = pToCast as Matrix2<double>;
                return new Matrix2I( (int)lCast.M00, (int)lCast.M01,
                                     (int)lCast.M10, (int)lCast.M11 );
            }
            else if ( pToCast is Matrix2<uint> )
            {
                Matrix2<uint> lCast = pToCast as Matrix2<uint>;
                return new Matrix2I( (int)lCast.M00, (int)lCast.M01,
                                     (int)lCast.M10, (int)lCast.M11 );
            }
            else if ( pToCast is Matrix2<Half> )
            {
                Matrix2<Half> lCast = pToCast as Matrix2<Half>;
                return new Matrix2I( (int)lCast.M00, (int)lCast.M01,
                                     (int)lCast.M10, (int)lCast.M11 );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
