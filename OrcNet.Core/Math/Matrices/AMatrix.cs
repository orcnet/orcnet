﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the abstract base <see cref="AMatrix{T}"/> class.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public abstract class AMatrix<T> : AMemoryProfilable, IMatrix<T> where T : struct
    {
        #region Delegates

        /// <summary>
        /// Delegate prototype definition of method casting a matrix from one primitive type to another.
        /// </summary>
        /// <param name="pToCast">The matrix to cast.</param>
        /// <returns>The casted matrix.</returns>
        protected delegate object CastDelegate(object pToCast);

        /// <summary>
        /// Delegate prototype for vector creators functions.
        /// </summary>
        protected delegate object VectorCreatorDelegate();

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the matrix data.
        /// </summary>
        protected MatrixCache<T> mData;

        #endregion Fields

        #region Properties

        #region Properties IMatrix

        /// <summary>
        /// Gets the vector component count.
        /// </summary>
        public abstract int ComponentCount
        {
            get;
        }

        /// <summary>
        /// Gets the matrix determinant.
        /// </summary>
        public abstract T Determinant
        {
            get;
        }

        /// <summary>
        /// Gets the matrix trace.
        /// </summary>
        public abstract T Trace
        {
            get;
        }

        /// <summary>
        /// Gets the flatten vector data.
        /// </summary>
        public T[] Data
        {
            get
            {
                return this.mData.Data;
            }
        }

        /// <summary>
        /// Gets or sets the row values.
        /// </summary>
        /// <param name="pRow">The row index.</param>
        /// <returns>The row values as vector.</returns>
        public abstract IVector<T> this[RowIndex pRow]
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the column values.
        /// </summary>
        /// <param name="pColumn">The column index.</param>
        /// <returns>The column values as vector.</returns>
        public abstract IVector<T> this[ColumnIndex pColumn]
        {
            get;
            set;
        }

        #endregion Properties IMatrix

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AMatrix{T}"/> class.
        /// </summary>
        protected AMatrix()
        {

        }

        #endregion Constructor

        #region Methods

        #region Methods IMatrix

        /// <summary>
        /// Turns this matrix into another vector of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new matrix of another primitive type.</returns>
        public abstract IMatrix<OtherT> Cast<OtherT>() where OtherT : struct;

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        public IMatrix<T> Add(IMatrix<T> pOther)
        {
            AMatrix<T> lOther = pOther as AMatrix<T>;
            if ( lOther != null )
            {
                return this.InternalAdd( lOther );
            }

            // Returns this matrix by default as addition failure result.
            return this.Clone() as IMatrix<T>;
        }

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        public IMatrix<T> Subtract(IMatrix<T> pOther)
        {
            AMatrix<T> lOther = pOther as AMatrix<T>;
            if ( lOther != null )
            {
                return this.InternalSubtract( lOther );
            }

            // Returns this matrix by default as subtraction failure result.
            return this.Clone() as IMatrix<T>;
        }

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        public IMatrix<T> Multiply(T pScale)
        {
            return this.InternalMultiply( pScale );
        }

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        public IVector<T> Multiply(IVector<T> pVector)
        {
            AVector<T> lVector = pVector as AVector<T>;
            if ( lVector != null )
            {
                return this.InternalMultiply( lVector );
            }

            // Returns a non-modified clone vector by default as multiplication failure result.
            return pVector.Clone() as IVector<T>;
        }

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pMatrix">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        public IMatrix<T> Multiply(IMatrix<T> pMatrix)
        {
            AMatrix<T> lScale = pMatrix as AMatrix<T>;
            if ( lScale != null )
            {
                return this.InternalMultiply( lScale );
            }

            // Returns this matrix by default as multiplication failure result.
            return this.Clone() as IMatrix<T>;
        }

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        public IBox<T> Multiply(IBox<T> pBox)
        {
            ABox<T> lBox = pBox as ABox<T>;
            if ( lBox != null )
            {
                return this.InternalMultiply( lBox );
            }

            return pBox.Clone() as IBox<T>;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        public IMatrix<T> Transpose()
        {
            return this.InternalTranspose();
        }

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        public IMatrix<T> Inverse(T pTolerance, out bool pResult)
        {
            return this.InternalInverse( pTolerance, out pResult );
        }

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new opposite matrix.</returns>
        public IMatrix<T> Opposite()
        {
            return this.InternalOpposite();
        }

        #endregion Methods IMatrix

        #region Methods Computation

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        protected abstract AMatrix<T> InternalAdd(AMatrix<T> pOther);

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        protected abstract AMatrix<T> InternalSubtract(AMatrix<T> pOther);

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected abstract AMatrix<T> InternalMultiply(T pScale);

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        protected abstract AVector<T> InternalMultiply(AVector<T> pVector);

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pScale">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected abstract AMatrix<T> InternalMultiply(AMatrix<T> pScale);

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        protected abstract ABox<T> InternalMultiply(ABox<T> pBox);

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        protected abstract AMatrix<T> InternalTranspose();

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        protected abstract AMatrix<T> InternalInverse(T pTolerance, out bool pResult);

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected abstract AMatrix<T> InternalOpposite();

        #endregion Methods Computation

        #region Methods Equality

        /// <summary>
        /// Compares the specified instances for equality.
        /// </summary>
        /// <param name="pFirst">The first vector to test.</param>
        /// <param name="pSecond">The second vector to test.</param>
        /// <returns>True if both instances are equal, false otherwise.</returns>
        public static bool operator == (AMatrix<T> pFirst, AMatrix<T> pSecond)
        {
            bool lIsFirstNull  = object.ReferenceEquals( pFirst, null );
            bool lIsSecondNull = object.ReferenceEquals( pSecond, null );
            if( lIsFirstNull && lIsSecondNull ) // If both null, equal.
            {
                return true;
            }
            else if ( lIsFirstNull || lIsSecondNull ) // If one of the two is null, not equal.
            {
                return false;
            }

            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Compares the specified instances for inequality.
        /// </summary>
        /// <param name="pFirst">The first vector to test.</param>
        /// <param name="pSecond">The second vector to test.</param>
        /// <returns>True if both instances are not equal, false otherwise.</returns>
        public static bool operator != (AMatrix<T> pFirst, AMatrix<T> pSecond)
        {
            bool lIsFirstNull  = object.ReferenceEquals( pFirst, null );
            bool lIsSecondNull = object.ReferenceEquals( pSecond, null );
            if( lIsFirstNull && lIsSecondNull ) // If both null, equal.
            {
                return false;
            }
            else if ( lIsFirstNull || lIsSecondNull ) // If one of the two is null, not equal.
            {
                return true;
            }

            return pFirst.Equals( pSecond ) == false;
        }

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            IMatrix<T> lOther = pOther as IMatrix<T>;
            if ( lOther != null )
            {
                return this.Equals( lOther );
            }

            return false;
        }

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(IMatrix<T> pOther)
        {
            AMatrix<T> lOther = pOther as AMatrix<T>;
            if ( lOther != null && 
                 this.mData == lOther.mData )
            {
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// Gets the vector's hash code.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return this.mData.GetHashCode();
        }

        #endregion Methods Equality

        #region Methods Statics

        /// <summary>
        /// Adds two matrices together.
        /// </summary>
        /// <param name="pFirst">The first matrix to add</param>
        /// <param name="pSecond">The second matrix to add</param>
        /// <returns>The new matrix result of the addition.</returns>
        public static AMatrix<T> operator + (AMatrix<T> pFirst, AMatrix<T> pSecond)
        {
            return pFirst.InternalAdd( pSecond );
        }

        /// <summary>
        /// Subtracts two matrices together.
        /// </summary>
        /// <param name="pFirst">The first matrix to subtract</param>
        /// <param name="pSecond">The second matrix to subtract</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        public static AMatrix<T> operator - (AMatrix<T> pFirst, AMatrix<T> pSecond)
        {
            return pFirst.InternalSubtract( pSecond );
        }

        /// <summary>
        /// Multiplies the given matrix by the given scalar.
        /// </summary>
        /// <param name="pToMultiply">The matrix to multiply.</param>
        /// <param name="pScale">The scalar to multiply by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        public static AMatrix<T> operator * (AMatrix<T> pToMultiply, T pScale)
        {
            return pToMultiply.InternalMultiply( pScale );
        }

        /// <summary>
        /// Multiplies the given matrix by the given scalar.
        /// </summary>
        /// <param name="pToMultiply">The matrix to multiply.</param>
        /// <param name="pScale">The scalar to multiply by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        public static AMatrix<T> operator * (T pScale, AMatrix<T> pToMultiply)
        {
            return pToMultiply.InternalMultiply( pScale );
        }

        /// <summary>
        /// Transforms the given vector by the given matrix.
        /// </summary>
        /// <param name="pToMultiply">The matrix to multiply.</param>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        public static AVector<T> operator * (AMatrix<T> pToMultiply, AVector<T> pVector)
        {
            return pToMultiply.InternalMultiply( pVector );
        }

        /// <summary>
        /// Transforms the given vector by the given matrix.
        /// </summary>
        /// <param name="pToMultiply">The matrix to multiply.</param>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        public static AVector<T> operator * (AVector<T> pVector, AMatrix<T> pToMultiply)
        {
            return pToMultiply.InternalMultiply( pVector );
        }

        /// <summary>
        /// Multiplies two matrices together.
        /// </summary>
        /// <param name="pFirst">The first matrix to multiply</param>
        /// <param name="pSecond">The second matrix to multiply</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        public static AMatrix<T> operator * (AMatrix<T> pFirst, AMatrix<T> pSecond)
        {
            return pFirst.InternalMultiply( pSecond );
        }
        
        /// <summary>
        /// Gets the opposite of the given matrix.
        /// </summary>
        /// <param name="pToInverse">The matrix to get the opposite from.</param>
        /// <returns>The new opposite matrix.</returns>
        public static AMatrix<T> operator - (AMatrix<T> pToInverse)
        {
            return pToInverse.InternalOpposite();
        }

        /// <summary>
        /// Implicit cast from Matrix[T] into a T[].
        /// </summary>
        /// <param name="pMatrix">The matrix to cast into a T[]</param>
        public static implicit operator T[](AMatrix<T> pMatrix)
        {
            return pMatrix.Data;
        }

        #endregion Methods Statics

        #region Methods ICloneable

        /// <summary>
        /// Clone this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        public object Clone()
        {
            return this.InternalClone();
        }

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected abstract AMatrix<T> InternalClone();

        #endregion Methods ICloneable

        #region Methods ToString

        /// <summary>
        /// Turns the vector into a string.
        /// </summary>
        /// <returns>The string representation of theis vector.</returns>
        public override string ToString()
        {
            return this.mData.ToString();
        }

        #endregion Methods ToString

        #endregion Methods
    }
}
