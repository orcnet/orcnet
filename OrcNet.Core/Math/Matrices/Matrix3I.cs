﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Matrix3I"/> class.
    /// </summary>
    public class Matrix3I : Matrix3<int>
    {
        #region Properties

        /// <summary>
        /// Gets a Zero matrix.
        /// </summary>
        public static Matrix3I ZERO
        {
            get
            {
                return new Matrix3I( 0 );
            }
        }

        /// <summary>
        /// Gets an identity matrix.
        /// </summary>
        public static Matrix3I IDENTITY
        {
            get
            {
                return new Matrix3I();
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(int) * cComponentCount;
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #region Properties IMatrix

        /// <summary>
        /// Gets the matrix determinant.
        /// </summary>
        public override int Determinant
        {
            get
            {
                int lCofactor00 = this.mData[ 1, 1 ] * this.mData[ 2, 2 ] - this.mData[ 1, 2 ] * this.mData[ 2, 1 ];
                int lCofactor10 = this.mData[ 1, 2 ] * this.mData[ 2, 0 ] - this.mData[ 1, 0 ] * this.mData[ 2, 2 ];
                int lCofactor20 = this.mData[ 1, 0 ] * this.mData[ 2, 1 ] - this.mData[ 1, 1 ] * this.mData[ 2, 0 ];
                int lDet = this.mData[ 0, 0 ] * lCofactor00 +
                             this.mData[ 0, 1 ] * lCofactor10 +
                             this.mData[ 0, 2 ] * lCofactor20;

                return lDet;
            }
        }

        /// <summary>
        /// Gets the matrix trace.
        /// </summary>
        public override int Trace
        {
            get
            {
                return this.mData[0, 0] + this.mData[1, 1] + this.mData[2, 2];
            }
        }

        #endregion Properties IMatrix

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Matrix3I"/> class.
        /// </summary>
        static Matrix3I()
        {
            sCasters.Add( typeof(int), CastToInt );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3I"/> class.
        /// Identity matrix by default.
        /// </summary>
        public Matrix3I() :
        this( 1, 0, 0, 
              0, 1, 0,
              0, 0, 1 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3I"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Matrix3I(int pValue) :
        this( pValue, pValue, pValue, 
              pValue, pValue, pValue, 
              pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix3I"/> class.
        /// </summary>
        /// <param name="p00">The (0, 0) value of the matrix.</param>
        /// <param name="p01">The (0, 1) value of the matrix.</param>
        /// <param name="p02">The (0, 2) value of the matrix.</param>
        /// <param name="p10">The (1, 0) value of the matrix.</param>
        /// <param name="p11">The (1, 1) value of the matrix.</param>
        /// <param name="p12">The (1, 2) value of the matrix.</param>
        /// <param name="p20">The (2, 0) value of the matrix.</param>
        /// <param name="p21">The (2, 1) value of the matrix.</param>
        /// <param name="p22">The (2, 2) value of the matrix.</param>
        public Matrix3I(int p00, int p01, int p02,
                        int p10, int p11, int p12,
                        int p20, int p21, int p22) :
        base( p00, p01, p02, 
              p10, p11, p12, 
              p20, p21, p22 )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Computation

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        protected override AMatrix<int> InternalAdd(AMatrix<int> pOther)
        {
            Matrix3I lOther = pOther as Matrix3I;
            Matrix3I lResult = new Matrix3I();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] + lOther.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        protected override AMatrix<int> InternalSubtract(AMatrix<int> pOther)
        {
            Matrix3I lOther = pOther as Matrix3I;
            Matrix3I lResult = new Matrix3I();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] - lOther.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<int> InternalMultiply(int pScale)
        {
            Matrix3I lResult = new Matrix3I();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] * pScale;
                }
            }

            return lResult;
        }

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        protected override AVector<int> InternalMultiply(AVector<int> pVector)
        {
            Vector2I lVector2 = pVector as Vector2I;
            if ( lVector2 != null )
            {
                Vector3I l2To3 = new Vector3I( lVector2.X, lVector2.Y, 1 );
                Vector3I lProd = new Vector3I();
                for ( int lRow = 0; lRow < 3; lRow++ )
                {
                    lProd[lRow] = this.mData[lRow, 0] * l2To3[0] +
                                  this.mData[lRow, 1] * l2To3[1] +
                                  this.mData[lRow, 2] * l2To3[2];
                }

                return new Vector2I( lProd.X, lProd.Y ) / lProd.Z;
            }

            Vector3I lVector3 = pVector as Vector3I;
            if ( lVector3 != null )
            {
                Vector3I lResult = new Vector3I();
                for ( int lRow = 0; lRow < 3; lRow++ )
                {
                    lResult[lRow] = this.mData[lRow, 0] * lVector3[0] +
                                    this.mData[lRow, 1] * lVector3[1] +
                                    this.mData[lRow, 2] * lVector3[2];
                }

                return lResult;
            }
            
            // Default answer.
            return pVector.Clone() as AVector<int>;
        }

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pMatrix">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<int> InternalMultiply(AMatrix<int> pMatrix)
        {
            Matrix3I lOther = pMatrix as Matrix3I;
            Matrix3I lResult = new Matrix3I();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[lRow, lCol] = this.mData[lRow, 0] * lOther.mData[0, lCol] +
                                                this.mData[lRow, 1] * lOther.mData[1, lCol] +
                                                this.mData[lRow, 2] * lOther.mData[2, lCol];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        protected override ABox<int> InternalMultiply(ABox<int> pBox)
        {
            Box3I lBox = pBox as Box3I;
            if ( lBox != null )
            {
                Vector3I lMinimum = lBox.Minimum as Vector3I;
                Vector3I lMaximum = lBox.Maximum as Vector3I;
                IBox<int> lResult = new Box3I();
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMinimum.X, lMinimum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMaximum.X, lMinimum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMinimum.X, lMaximum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMaximum.X, lMaximum.Y, lMinimum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMinimum.X, lMinimum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMaximum.X, lMinimum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMinimum.X, lMaximum.Y, lMaximum.Z ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector3I( lMaximum.X, lMaximum.Y, lMaximum.Z ) ) );

                return lResult as ABox<int>;
            }

            // Default answer in case of box type mismatches.
            return pBox.Clone() as ABox<int>;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        protected override AMatrix<int> InternalTranspose()
        {
            Matrix3I lResult = new Matrix3I();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lCol, lRow ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        protected override AMatrix<int> InternalInverse(int pTolerance, out bool pResult)
        {
            Matrix3I lResult = new Matrix3I();
            // Invert a 3x3 using cofactors.  This is about 8 times faster than
            // the Numerical Recipes code which uses Gaussian elimination.
            lResult.mData[0, 0] = this.mData[1, 1] * this.mData[2, 2] - this.mData[1, 2] * this.mData[2, 1];
            lResult.mData[0, 1] = this.mData[0, 2] * this.mData[2, 1] - this.mData[0, 1] * this.mData[2, 2];
            lResult.mData[0, 2] = this.mData[0, 1] * this.mData[1, 2] - this.mData[0, 2] * this.mData[1, 1];
            lResult.mData[1, 0] = this.mData[1, 2] * this.mData[2, 0] - this.mData[1, 0] * this.mData[2, 2];
            lResult.mData[1, 1] = this.mData[0, 0] * this.mData[2, 2] - this.mData[0, 2] * this.mData[2, 0];
            lResult.mData[1, 2] = this.mData[0, 2] * this.mData[1, 0] - this.mData[0, 0] * this.mData[1, 2];
            lResult.mData[2, 0] = this.mData[1, 0] * this.mData[2, 1] - this.mData[1, 1] * this.mData[2, 0];
            lResult.mData[2, 1] = this.mData[0, 1] * this.mData[2, 0] - this.mData[0, 0] * this.mData[2, 1];
            lResult.mData[2, 2] = this.mData[0, 0] * this.mData[1, 1] - this.mData[0, 1] * this.mData[1, 0];

            int lDet = this.mData[0, 0] * lResult.mData[0, 0] +
                       this.mData[0, 1] * lResult.mData[1, 0] +
                       this.mData[0, 2] * lResult.mData[2, 0];

            if ( System.Math.Abs( lDet ) <= pTolerance )
            {
                pResult = false;
                return null;
            }

            int lInvDet = 1 / lDet;
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[lRow, lCol] *= lInvDet;
                }
            }

            pResult = true;

            return lResult;
        }

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AMatrix<int> InternalOpposite()
        {
            Matrix3I lResult = new Matrix3I();
            for ( int lRow = 0; lRow < 3; lRow++ )
            {
                for ( int lCol = 0; lCol < 3; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = -this.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        #endregion Methods Computation

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AMatrix<int> InternalClone()
        {
            return new Matrix3I( this.mData[ 0, 0 ], this.mData[ 0, 1 ], this.mData[ 0, 2 ],
                                 this.mData[ 1, 0 ], this.mData[ 1, 1 ], this.mData[ 1, 2 ],
                                 this.mData[ 2, 0 ], this.mData[ 2, 1 ], this.mData[ 2, 2 ] );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToInt(object pToCast)
        {
            if ( pToCast is Matrix3<int> )
            {
                return (pToCast as Matrix3<int>).Clone();
            }
            else if ( pToCast is Matrix3<double> )
            {
                Matrix3<double> lCast = pToCast as Matrix3<double>;
                return new Matrix3I( (int)lCast.M00, (int)lCast.M01, (int)lCast.M02,
                                     (int)lCast.M10, (int)lCast.M11, (int)lCast.M12,
                                     (int)lCast.M20, (int)lCast.M21, (int)lCast.M22 );
            }
            else if ( pToCast is Matrix3<float> )
            {
                Matrix3<float> lCast = pToCast as Matrix3<float>;
                return new Matrix3I( (int)lCast.M00, (int)lCast.M01, (int)lCast.M02,
                                     (int)lCast.M10, (int)lCast.M11, (int)lCast.M12,
                                     (int)lCast.M20, (int)lCast.M21, (int)lCast.M22 );
            }
            else if ( pToCast is Matrix3<Half> )
            {
                Matrix3<Half> lCast = pToCast as Matrix3<Half>;
                return new Matrix3I( (int)lCast.M00, (int)lCast.M01, (int)lCast.M02,
                                     (int)lCast.M10, (int)lCast.M11, (int)lCast.M12,
                                     (int)lCast.M20, (int)lCast.M21, (int)lCast.M22 );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
