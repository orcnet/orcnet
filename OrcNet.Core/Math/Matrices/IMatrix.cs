﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="IMatrix{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public interface IMatrix<T> : IEquatable<IMatrix<T>>, ICloneable where T : struct
    {
        #region Properties

        /// <summary>
        /// Gets the vector component count.
        /// </summary>
        int ComponentCount
        {
            get;
        }

        /// <summary>
        /// Gets the matrix determinant.
        /// </summary>
        T Determinant
        {
            get;
        }

        /// <summary>
        /// Gets the matrix trace.
        /// </summary>
        T Trace
        {
            get;
        }

        /// <summary>
        /// Gets the flatten vector data.
        /// </summary>
        T[] Data
        {
            get;
        }

        /// <summary>
        /// Gets or sets the row values.
        /// </summary>
        /// <param name="pRow">The row index.</param>
        /// <returns>The row values as vector.</returns>
        IVector<T> this[RowIndex pRow]
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the column values.
        /// </summary>
        /// <param name="pColumn">The column index.</param>
        /// <returns>The column values as vector.</returns>
        IVector<T> this[ColumnIndex pColumn]
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns this matrix into another matrix of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new matrix of another primitive type.</returns>
        IMatrix<OtherT> Cast<OtherT>() where OtherT : struct;

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        IMatrix<T> Add(IMatrix<T> pOther);

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        IMatrix<T> Subtract(IMatrix<T> pOther);

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        IMatrix<T> Multiply(T pScale);

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        IVector<T> Multiply(IVector<T> pVector);

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pMatrix">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        IMatrix<T> Multiply(IMatrix<T> pMatrix);

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        IBox<T> Multiply(IBox<T> pBox);

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        IMatrix<T> Transpose();

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        IMatrix<T> Inverse(T pTolerance, out bool pResult);

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new opposite matrix.</returns>
        IMatrix<T> Opposite();

        #endregion Methods
    }
}
