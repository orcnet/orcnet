﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Helper structure to access by index one of the matrix row.
    /// </summary>
    public struct RowIndex
    {
        #region Fields

        /// <summary>
        /// Stores the column index
        /// </summary>
        private int mIndex;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RowIndex"/> class.
        /// </summary>
        /// <param name="pIndex">The row index.</param>
        public RowIndex(int pIndex)
        {
            this.mIndex = pIndex;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from RowIndex to int
        /// </summary>
        /// <param name="pIndex">The row index</param>
        public static implicit operator int(RowIndex pIndex)
        {
            return pIndex.mIndex;
        }

        #endregion Methods
    }
}
