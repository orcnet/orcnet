﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Matrix2D"/> class.
    /// </summary>
    public class Matrix2D : Matrix2<double>
    {
        #region Properties

        /// <summary>
        /// Gets a Zero matrix.
        /// </summary>
        public static Matrix2D ZERO
        {
            get
            {
                return new Matrix2D( 0.0 );
            }
        }

        /// <summary>
        /// Gets an identity matrix.
        /// </summary>
        public static Matrix2D IDENTITY
        {
            get
            {
                return new Matrix2D();
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(double) * cComponentCount;
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #region Properties IMatrix

        /// <summary>
        /// Gets the matrix determinant.
        /// </summary>
        public override double Determinant
        {
            get
            {
                return this.mData[0, 0] * this.mData[1, 1] - this.mData[1, 0] * this.mData[0, 1];
            }
        }

        /// <summary>
        /// Gets the matrix trace.
        /// </summary>
        public override double Trace
        {
            get
            {
                return this.mData[0, 0] + this.mData[1, 1];
            }
        }

        #endregion Properties IMatrix

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Matrix2D"/> class.
        /// </summary>
        static Matrix2D()
        {
            sCasters.Add( typeof(double), CastToDouble );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2D"/> class.
        /// Identity matrix by default.
        /// </summary>
        public Matrix2D() :
        this( 1.0, 0.0, 0.0, 1.0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2D"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Matrix2D(double pValue) :
        this( pValue, pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix2D"/> class.
        /// </summary>
        /// <param name="p00">The (0, 0) value of the matrix.</param>
        /// <param name="p01">The (0, 1) value of the matrix.</param>
        /// <param name="p10">The (1, 0) value of the matrix.</param>
        /// <param name="p11">The (1, 1) value of the matrix.</param>
        public Matrix2D(double p00, double p01,
                        double p10, double p11) :
        base( p00, p01, p10, p11 )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Computation

        /// <summary>
        /// Adds this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to add to this one.</param>
        /// <returns>The new matrix result of the addition.</returns>
        protected override AMatrix<double> InternalAdd(AMatrix<double> pOther)
        {
            Matrix2D lOther = pOther as Matrix2D;
            Matrix2D lResult = new Matrix2D();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] + lOther.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Subtracts this matrix and the given one together.
        /// </summary>
        /// <param name="pOther">The other matrix to subtract to this one.</param>
        /// <returns>The new matrix result of the subtraction.</returns>
        protected override AMatrix<double> InternalSubtract(AMatrix<double> pOther)
        {
            Matrix2D lOther = pOther as Matrix2D;
            Matrix2D lResult = new Matrix2D();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] - lOther.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Multiplies this matrix by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<double> InternalMultiply(double pScale)
        {
            Matrix2D lResult = new Matrix2D();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, lCol ] * pScale;
                }
            }

            return lResult;
        }

        /// <summary>
        /// Transforms the given vector by this matrix.
        /// </summary>
        /// <param name="pVector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        protected override AVector<double> InternalMultiply(AVector<double> pVector)
        {
            Vector2D lVector = pVector as Vector2D;
            Vector2D lResult = new Vector2D();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                lResult[ lRow ] = this.mData[ lRow, 0 ] * lVector.X + this.mData[ lRow, 1 ] * lVector.Y;
            }

            return lResult;
        }

        /// <summary>
        /// Multiplies this matrix by the given one.
        /// </summary>
        /// <param name="pMatrix">The other matrix this matrix must be multiplied by.</param>
        /// <returns>The new matrix result of the multiplication.</returns>
        protected override AMatrix<double> InternalMultiply(AMatrix<double> pMatrix)
        {
            Matrix2D lOther = pMatrix as Matrix2D;
            Matrix2D lResult = new Matrix2D();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++)
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lRow, 0 ] * lOther.mData[ 0, lCol ] + this.mData[ lRow, 1 ] * lOther.mData[ 1, lCol ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Transforms the given box by this matrix.
        /// </summary>
        /// <param name="pBox">The box to transform.</param>
        /// <returns>The new box result of the transformation.</returns>
        protected override ABox<double> InternalMultiply(ABox<double> pBox)
        {
            Box2D lBox = pBox as Box2D;
            if ( lBox != null )
            {
                Vector2D lMinimum = lBox.Minimum as Vector2D;
                Vector2D lMaximum = lBox.Maximum as Vector2D;
                IBox<double> lResult = new Box2D();
                lResult = lResult.Enlarge( this.Multiply( new Vector2D( lMinimum.X, lMinimum.Y ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector2D( lMaximum.X, lMinimum.Y ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector2D( lMinimum.X, lMaximum.Y ) ) );
                lResult = lResult.Enlarge( this.Multiply( new Vector2D( lMaximum.X, lMaximum.Y ) ) );

                return lResult as ABox<double>;
            }

            // Default answer in case of box type mismatches.
            return pBox.Clone() as ABox<double>;
        }

        /// <summary>
        /// Transposes this matrix.
        /// </summary>
        /// <returns>The transposed matrix.</returns>
        protected override AMatrix<double> InternalTranspose()
        {
            Matrix2D lResult = new Matrix2D();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = this.mData[ lCol, lRow ];
                }
            }

            return lResult;
        }

        /// <summary>
        /// Inverses this matrix.
        /// </summary>
        /// <param name="pTolerance">The tolerance determining whether this matrix has an inverse or not.</param>
        /// <param name="pResult">True if has an inverse, false otherwise.</param>
        /// <returns>The new inversed matrix.</returns>
        protected override AMatrix<double> InternalInverse(double pTolerance, out bool pResult)
        {
            double lDeterminant = this.Determinant;

            if ( System.Math.Abs( lDeterminant ) <= pTolerance )
            {
                pResult = false;
                return null;
            }

            Matrix2D lResult = new Matrix2D();

            double lInvDet = 1 / lDeterminant;

            lResult.mData[ 0, 0 ] =  this.mData[ 1, 1 ] * lInvDet;
            lResult.mData[ 0, 1 ] = -this.mData[ 0, 1 ] * lInvDet;
            lResult.mData[ 1, 0 ] = -this.mData[ 1, 0 ] * lInvDet;
            lResult.mData[ 1, 1 ] =  this.mData[ 0, 0 ] * lInvDet;
            pResult = true;

            return lResult;
        }

        /// <summary>
        /// Gets the opposite matrix of this matrix.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AMatrix<double> InternalOpposite()
        {
            Matrix2D lResult = new Matrix2D();
            for ( int lRow = 0; lRow < 2; lRow++ )
            {
                for ( int lCol = 0; lCol < 2; lCol++ )
                {
                    lResult.mData[ lRow, lCol ] = -this.mData[ lRow, lCol ];
                }
            }

            return lResult;
        }

        #endregion Methods Computation

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AMatrix<double> InternalClone()
        {
            return new Matrix2D( this.mData[ 0, 0 ], this.mData[ 0, 1 ],
                                 this.mData[ 1, 0 ], this.mData[ 1, 1 ] );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToDouble(object pToCast)
        {
            if ( pToCast is Matrix2<double> )
            {
                return (pToCast as Matrix2<double>).Clone();
            }
            else if ( pToCast is Matrix2<float> )
            {
                Matrix2<double> lCast = pToCast as Matrix2<double>;
                return new Matrix2D( lCast.M00, lCast.M01,
                                     lCast.M10, lCast.M11 );
            }
            else if ( pToCast is Matrix2<int> )
            {
                Matrix2<int> lCast = pToCast as Matrix2<int>;
                return new Matrix2D( lCast.M00, lCast.M01,
                                     lCast.M10, lCast.M11 );
            }
            else if ( pToCast is Matrix2<uint> )
            {
                Matrix2<uint> lCast = pToCast as Matrix2<uint>;
                return new Matrix2D( lCast.M00, lCast.M01,
                                     lCast.M10, lCast.M11 );
            }
            else if ( pToCast is Matrix2<Half> )
            {
                Matrix2<Half> lCast = pToCast as Matrix2<Half>;
                return new Matrix2D( lCast.M00, lCast.M01,
                                     lCast.M10, lCast.M11 );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
