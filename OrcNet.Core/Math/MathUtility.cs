﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="MathUtility"/> class.
    /// </summary>
    public static class MathUtility
    {
        #region Constants

        /// <summary>
        /// Stores the factor converting degrees to radians.
        /// </summary>
        private const double cToRadians = 0.01745329251994329576923690768489;

        /// <summary>
        /// Stores the factor converting radians to degrees.
        /// </summary>
        private const double cToDegrees = 57.295779513082320876798154814105;

        #endregion Constants

        #region Methods

        /// <summary>
        /// Transforms an angle in radians into an angle in degrees.
        /// </summary>
        /// <param name="pInRadians">The angle in radians.</param>
        /// <returns>The angle in degrees.</returns>
        public static double ToDegrees(double pInRadians)
        {
            return pInRadians * cToDegrees;
        }

        /// <summary>
        /// Transforms an angle in degrees into an angle in radians.
        /// </summary>
        /// <param name="pInDegrees">The angle in degrees.</param>
        /// <returns>The angle in radians.</returns>
        public static double ToRadians(double pInDegrees)
        {
            return pInDegrees * cToRadians;
        }

        #endregion Methods
    }
}
