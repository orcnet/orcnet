﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Half"/> structure.
    /// </summary>
    public struct Half : IEquatable<Half>
    {
        #region Fields

        /// <summary>
        /// Stores the half value stored on a unsigned integer of 16bits
        /// </summary>
        private ushort mValue;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Half"/> class.
        /// </summary>
        /// <param name="pValue">A floating point 32 bit value.</param>
        public Half(double pValue) :
        this( (float)pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Half"/> class.
        /// </summary>
        /// <param name="pValue">A floating point 32 bit value.</param>
        public Half(float pValue)
        {
            this.mValue = FloatToHalf( pValue );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether this half is invalid or not.
        /// </summary>
        /// <returns>True if non numeric, false otherwise.</returns>
        public bool IsNan()
        {
            return this.Exponent() == 31 && 
                   this.Mantissa() != 0;
        }

        /// <summary>
        /// Checks whether this half is not an infinite or Nan value.
        /// </summary>
        /// <returns>True if neither infinite nor Nan, false otherwise.</returns>
        public bool IsFinite()
        {
            return this.Exponent() != 31;
        }

        /// <summary>
        /// Checks whether this half is infinite or not.
        /// </summary>
        /// <returns>True if infinite, false otherwise.</returns>
        public bool IsInfinite()
        {
            return this.Exponent() == 31 && 
                   this.Mantissa() == 0;
        }

        /// <summary>
        /// Checks whether this half is a non-denormal or not.
        /// </summary>
        /// <returns>True if normal, false otherwise.</returns>
        public bool IsNormal()
        {
            return this.IsFinite() && 
                  ( this.Exponent() != 0 || this.Mantissa() == 0 );
        }

        #region Methods Equality

        /// <summary>
        /// Checks whether this half is equal to the given half.
        /// </summary>
        /// <param name="pOther">The other half to test.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(Half pOther)
        {
            return this.mValue == pOther.mValue;
        }

        /// <summary>
        /// Compares the specified instances for equality.
        /// </summary>
        /// <param name="pFirst">The first half to test.</param>
        /// <param name="pSecond">The second half to test.</param>
        /// <returns>True if both instances are equal, false otherwise.</returns>
        public static bool operator == (Half pFirst, Half pSecond)
        {
            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Compares the specified instances for inequality.
        /// </summary>
        /// <param name="pFirst">The first half to test.</param>
        /// <param name="pSecond">The second half to test.</param>
        /// <returns>True if both instances are not equal, false otherwise.</returns>
        public static bool operator != (Half pFirst, Half pSecond)
        {
            return pFirst.Equals( pSecond ) == false;
        }

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            if ( pOther is Half == false )
            {
                return false;
            }

            return this.Equals( (Half)pOther );
        }

        /// <summary>
        /// Gets the half hashcode.
        /// </summary>
        /// <returns>The hashcode.</returns>
        public override int GetHashCode()
        {
            return this.mValue.GetHashCode();
        }

        #endregion Methods Equality

        #region Methods Statics

        /// <summary>
        /// Adds two halfs together.
        /// </summary>
        /// <param name="pFirst">The first half to add</param>
        /// <param name="pSecond">The second half to add</param>
        /// <returns>The new half result of the addition.</returns>
        public static Half operator + (Half pFirst, Half pSecond)
        {
            return new Half( (float)pFirst + pSecond );
        }

        /// <summary>
        /// Subtracts two halfs together.
        /// </summary>
        /// <param name="pFirst">The first half to subtract</param>
        /// <param name="pSecond">The second half to subtract</param>
        /// <returns>The new half result of the subtraction.</returns>
        public static Half operator - (Half pFirst, Half pSecond)
        {
            return new Half( (float)pFirst - pSecond );
        }

        /// <summary>
        /// Multiplies the two halfs together.
        /// </summary>
        /// <param name="pFirst">The first half to multiply</param>
        /// <param name="pSecond">The second half to multiply</param>
        /// <returns>The new half result of the multiplication.</returns>
        public static Half operator * (Half pFirst, Half pSecond)
        {
            return new Half( (float)pFirst * pSecond );
        }

        /// <summary>
        /// Divides two halfs together.
        /// </summary>
        /// <param name="pFirst">The first half to divide</param>
        /// <param name="pSecond">The second half to divide</param>
        /// <returns>The new half result of the division.</returns>
        public static Half operator / (Half pFirst, Half pSecond)
        {
            return new Half( (float)pFirst / pSecond );
        }

        /// <summary>
        /// Inplicit half to float cast operator.
        /// </summary>
        /// <param name="pToCast">The half to cast</param>
        public static implicit operator float(Half pToCast)
        {
            return HalfToFloat( pToCast.mValue );
        }

        /// <summary>
        /// Gets the Half as an array of bytes.
        /// </summary>
        /// <param name="pHalf">The Half to convert.</param>
        /// <returns>The input as byte array.</returns>
        public static byte[] GetBytes(Half pHalf)
        {
            return BitConverter.GetBytes( pHalf.mValue );
        }

        /// <summary>
        /// Converts an array of bytes into Half.
        /// </summary>
        /// <param name="pValue">A Half in it's byte[] representation.</param>
        /// <param name="pStartIndex">The starting position within value.</param>
        /// <returns>A new Half instance.</returns>
        public static Half FromBytes(byte[] pValue, int pStartIndex)
        {
            Half lResult;
            lResult.mValue = BitConverter.ToUInt16( pValue, pStartIndex );
            return lResult;
        }

        /// <summary>
        /// Converts the given float into a half value.
        /// </summary>
        /// <param name="pToConvert">The float to convert.</param>
        /// <returns>The half value.</returns>
        private static ushort FloatToHalf(float pToConvert)
        {
            int lToConvert;
            unsafe
            {
                lToConvert = *(int*)&pToConvert;
            }

            // Our floating point number, F, is represented by the bit pattern in integer i.
            // Disassemble that bit pattern into the sign, S, the exponent, E, and the significand, M.
            // Shift S into the position where it will go in in the resulting half number.
            // Adjust E, accounting for the different exponent bias of float and half (127 versus 15).

            int sign = (lToConvert >> 16) & 0x00008000;
            int exponent = ((lToConvert >> 23) & 0x000000ff) - (127 - 15);
            int mantissa = lToConvert & 0x007fffff;

            // Now reassemble S, E and M into a half:

            if (exponent <= 0)
            {
                if (exponent < -10)
                {
                    // E is less than -10. The absolute value of F is less than Half.MinValue
                    // (F may be a small normalized float, a denormalized float or a zero).
                    //
                    // We convert F to a half zero with the same sign as F.

                    return (ushort)sign;
                }

                // E is between -10 and 0. F is a normalized float whose magnitude is less than Half.MinNormalizedValue.
                //
                // We convert F to a denormalized half.

                // Add an explicit leading 1 to the significand.

                mantissa = mantissa | 0x00800000;

                // Round to M to the nearest (10+E)-bit value (with E between -10 and 0); in case of a tie, round to the nearest even value.
                //
                // Rounding may cause the significand to overflow and make our number normalized. Because of the way a half's bits
                // are laid out, we don't have to treat this case separately; the code below will handle it correctly.

                int t = 14 - exponent;
                int a = (1 << (t - 1)) - 1;
                int b = (mantissa >> t) & 1;

                mantissa = (mantissa + a + b) >> t;

                // Assemble the half from S, E (==zero) and M.

                return (ushort)(sign | mantissa);
            }
            else if (exponent == 0xff - (127 - 15))
            {
                if (mantissa == 0)
                {
                    // F is an infinity; convert F to a half infinity with the same sign as F.

                    return (ushort)(sign | 0x7c00);
                }
                else
                {
                    // F is a NAN; we produce a half NAN that preserves the sign bit and the 10 leftmost bits of the
                    // significand of F, with one exception: If the 10 leftmost bits are all zero, the NAN would turn 
                    // into an infinity, so we have to set at least one bit in the significand.

                    mantissa >>= 13;
                    return (ushort)(sign | 0x7c00 | mantissa | ((mantissa == 0) ? 1 : 0));
                }
            }
            else
            {
                // E is greater than zero.  F is a normalized float. We try to convert F to a normalized half.

                // Round to M to the nearest 10-bit value. In case of a tie, round to the nearest even value.

                mantissa = mantissa + 0x00000fff + ((mantissa >> 13) & 1);

                if ((mantissa & 0x00800000) == 1)
                {
                    mantissa = 0;        // overflow in significand,
                    exponent += 1;        // adjust exponent
                }

                // exponent overflow
                if (exponent > 30) throw new ArithmeticException("Half: Hardware floating-point overflow.");

                // Assemble the half from S, E and M.

                return (ushort)(sign | (exponent << 10) | (mantissa >> 13));
            }
        }

        /// <summary>
        /// Converts the given half value into a float.
        /// </summary>
        /// <param name="pToConvert">The half value to convert.</param>
        /// <returns>The float value.</returns>
        private static float HalfToFloat(ushort pToConvert)
        {
            int sign = (pToConvert >> 15) & 0x00000001;
            int exponent = (pToConvert >> 10) & 0x0000001f;
            int mantissa = pToConvert & 0x000003ff;

            if (exponent == 0)
            {
                if (mantissa == 0)
                {
                    // Plus or minus zero

                    return sign << 31;
                }
                else
                {
                    // Denormalized number -- renormalize it

                    while ((mantissa & 0x00000400) == 0)
                    {
                        mantissa <<= 1;
                        exponent -= 1;
                    }

                    exponent += 1;
                    mantissa &= ~0x00000400;
                }
            }
            else if (exponent == 31)
            {
                if (mantissa == 0)
                {
                    // Positive or negative infinity

                    return (sign << 31) | 0x7f800000;
                }
                else
                {
                    // Nan -- preserve sign and significand bits

                    return (sign << 31) | 0x7f800000 | (mantissa << 13);
                }
            }

            // Normalized number

            exponent = exponent + (127 - 15);
            mantissa = mantissa << 13;

            // Assemble S, E and M.

            int lResult = (sign << 31) | (exponent << 23) | mantissa;

            unsafe
            {
                return *(float*)&lResult;
            }
        }

        #endregion Methods Statics
        
        /// <summary>
        /// Gets the half value 5bits 15-biased exponent.
        /// </summary>
        /// <returns>The exponent part.</returns>
        private int Exponent()
        {
            return (this.mValue & 0x7C00) >> 10;
        }

        /// <summary>
        /// Gets the half value 10bits mantissa.
        /// </summary>
        /// <returns>The mantissa part.</returns>
        private int Mantissa()
        {
            return this.mValue & 0x03ff;
        }

        #endregion Methods
    }
}
