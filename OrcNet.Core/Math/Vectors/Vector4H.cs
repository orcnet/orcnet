﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Vector4H"/> class.
    /// </summary>
    public class Vector4H : Vector4<Half>
    {
        #region Properties

        /// <summary>
        /// Gets the Zero vector.
        /// </summary>
        public static Vector4H ZERO
        {
            get
            {
                return new Vector4H( new Half( 0.0f ) );
            }
        }

        /// <summary>
        /// Gets the X unit vector.
        /// </summary>
        public static Vector4H UNIT_X
        {
            get
            {
                return new Vector4H( new Half( 1.0f ), new Half( 0.0f ), new Half( 0.0f ), new Half( 0.0f ) );
            }
        }

        /// <summary>
        /// Gets the Y unit vector.
        /// </summary>
        public static Vector4H UNIT_Y
        {
            get
            {
                return new Vector4H( new Half( 0.0f ), new Half( 1.0f ), new Half( 0.0f ), new Half( 0.0f ) );
            }
        }

        /// <summary>
        /// Gets the Z unit vector.
        /// </summary>
        public static Vector4H UNIT_Z
        {
            get
            {
                return new Vector4H( new Half( 0.0f ), new Half( 0.0f ), new Half( 1.0f ), new Half( 0.0f ) );
            }
        }

        /// <summary>
        /// Gets the W unit vector.
        /// </summary>
        public static Vector4H UNIT_W
        {
            get
            {
                return new Vector4H( new Half( 0.0f ), new Half( 0.0f ), new Half( 0.0f ), new Half( 1.0f ) );
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(ushort) * 4; // four floating point components.
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #endregion properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Vector4H"/> class.
        /// </summary>
        static Vector4H()
        {
            sCasters.Add( typeof(Half), CastToHalf );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4H"/> class.
        /// </summary>
        public Vector4H() :
        this( new Half( 0.0f ) )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4H"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Vector4H(Half pValue) :
        this( pValue, pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4H"/> class.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        /// <param name="pZ">The Z component.</param>
        /// <param name="pW">The W component.</param>
        public Vector4H(Half pX, Half pY, Half pZ, Half pW) :
        base( pX, pY, pZ, pW )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods IVector

        /// <summary>
        /// Gets the vector length.
        /// </summary>
        /// <returns>The vector's length.</returns>
        public override Half Length()
        {
            return new Half( System.Math.Sqrt( this.LengthSq() ) );
        }

        /// <summary>
        /// Gets the vector length squared.
        /// </summary>
        /// <returns>The vector's length squared.</returns>
        public override Half LengthSq()
        {
            return this.X * this.X + this.Y * this.Y + this.Z * this.Z + this.W * this.W;
        }

        /// <summary>
        /// Gets the scalar product of this vector and the given one.
        /// </summary>
        /// <param name="pOther">The other vector the dot product must be found with.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The scalar product</returns>
        public override Half DotProduct(IVector<Half> pOther, out bool pResult)
        {
            pResult = false;

            Vector4<Half> lOther = pOther as Vector4<Half>;
            if ( lOther != null )
            {
                pResult = true;
                return this.X * lOther.X + this.Y * lOther.Y + this.Z * lOther.Z + this.W * lOther.W;
            }

            return new Half( 0.0f );
        }

        /// <summary>
        /// Gets the vector product of this vector and the given one, that is, the perpendicular vector.
        /// </summary>
        /// <param name="pOther">The second vector involved in the vector product.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The vector perpendicular to the two others</returns>
        public override IVector<Half> CrossProduct(IVector<Half> pOther, out bool pResult)
        {
            pResult = false;

            Vector4<Half> lOther = pOther as Vector4<Half>;
            if ( lOther != null )
            {
                pResult = true;
                return new Vector4H( this.Y * lOther.Z - this.Z * lOther.Y,
                                     this.Z * lOther.X - this.X * lOther.Z,
                                     this.X * lOther.Y - this.Y * lOther.X,
                                     new Half( 0.0f ) ); // Cross product give us a direction, not a point.
            }

            return new Vector4H();
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <returns>The normalized vector.</returns>
        public override IVector<Half> Normalize()
        {
            Half lScale = new Half( 1.0f ) / this.Length();
            return new Vector4H( this.X * lScale, this.Y * lScale, this.Z * lScale, this.W * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector.</returns>
        public override IVector<Half> Normalize(out Half pPreviousLength)
        {
            pPreviousLength = this.Length();
            Half lScale = new Half( 1.0f ) / pPreviousLength;
            return new Vector4H( this.X * lScale, this.Y * lScale, this.Z * lScale, this.W * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the given length.
        /// </summary>
        /// <param name="pLength">The length this vector must be normalized to.</param>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector to the given length.</returns>
        public override IVector<Half> Normalize(Half pLength, out Half pPreviousLength)
        {
            pPreviousLength = this.Length();
            Half lScale = pLength / pPreviousLength;
            return new Vector4H( this.X * lScale, this.Y * lScale, this.Z * lScale, this.W * lScale );
        }
        
        #endregion Methods IVector

        #region Methods Computation

        /// <summary>
        /// Adds this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to add to this one.</param>
        /// <returns>The new vector result of the addition.</returns>
        protected override AVector<Half> InternalAdd(AVector<Half> pOther)
        {
            Vector4<Half> lOther = pOther as Vector4<Half>;
            return new Vector4H( this.X + lOther.X, this.Y + lOther.Y, this.Z + lOther.Z, this.W + lOther.W );
        }

        /// <summary>
        /// Subtracts this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to subtract to this one.</param>
        /// <returns>The new vector result of the subtraction.</returns>
        protected override AVector<Half> InternalSubtract(AVector<Half> pOther)
        {
            Vector4<Half> lOther = pOther as Vector4<Half>;
            return new Vector4H( this.X - lOther.X, this.Y - lOther.Y, this.Z - lOther.Z, this.W - lOther.W );
        }

        /// <summary>
        /// Multiplies this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<Half> InternalMultiply(Half pScale)
        {
            return new Vector4H( this.X * pScale, this.Y * pScale, this.Z * pScale, this.W * pScale );
        }

        /// <summary>
        /// Multiplies this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<Half> InternalMultiply(AVector<Half> pScale)
        {
            Vector4<Half> lOther = pScale as Vector4<Half>;
            return new Vector4H( this.X * lOther.X, this.Y * lOther.Y, this.Z * lOther.Z, this.W * lOther.W );
        }

        /// <summary>
        /// Divides this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<Half> InternalDivide(Half pScale)
        {
            Half lScale = System.Math.Abs( pScale ) > float.Epsilon ? pScale : new Half( 1.0f ); // Avoid to divide by zero.

            return new Vector4H( this.X / lScale, this.Y / lScale, this.Z / lScale, this.W / lScale );
        }

        /// <summary>
        /// Divides this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<Half> InternalDivide(AVector<Half> pScale)
        {
            Vector4<Half> lOther = pScale as Vector4<Half>;
            Half lScaleX = System.Math.Abs( lOther.X ) > float.Epsilon ? lOther.X : new Half( 1.0f ); // Avoid to divide by zero.
            Half lScaleY = System.Math.Abs( lOther.Y ) > float.Epsilon ? lOther.Y : new Half( 1.0f ); // Avoid to divide by zero.
            Half lScaleZ = System.Math.Abs( lOther.Z ) > float.Epsilon ? lOther.Z : new Half( 1.0f ); // Avoid to divide by zero.
            Half lScaleW = System.Math.Abs( lOther.W ) > float.Epsilon ? lOther.W : new Half( 1.0f ); // Avoid to divide by zero.

            return new Vector4H( this.X / lScaleX, this.Y / lScaleY, this.Z / lScaleZ, this.W / lScaleW );
        }

        /// <summary>
        /// Inverses this vector.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AVector<Half> InternalInverse()
        {
            return new Vector4H( new Half( -this.X ), new Half( -this.Y ), new Half( -this.Z ), this.W );
        }

        #endregion Methods Computation

        #region Methods Equality

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        protected override bool Equals(AVector<Half> pOther)
        {
            Vector4H lOther = pOther as Vector4H;
            if ( lOther != null )
            {
                return this.X == lOther.X &&
                       this.Y == lOther.Y &&
                       this.Z == lOther.Z &&
                       this.W == lOther.W;
            }

            return false;
        }

        #endregion Methods Equality

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AVector<Half> InternalClone()
        {
            return new Vector4H( this.X, this.Y, this.Z, this.W );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToHalf(object pToCast)
        {
            if ( pToCast is Vector4<Half> )
            {
                return (pToCast as Vector4<Half>).Clone();
            }
            else if ( pToCast is Vector4<double> )
            {
                Vector4<double> lCast = pToCast as Vector4<double>;
                return new Vector4H( new Half( lCast.X ), new Half( lCast.Y ), new Half( lCast.Z ), new Half( lCast.W ) );
            }
            else if ( pToCast is Vector4<int> )
            {
                Vector4<int> lCast = pToCast as Vector4<int>;
                return new Vector4H( new Half( lCast.X ), new Half( lCast.Y ), new Half( lCast.Z ), new Half( lCast.W ) );
            }
            else if ( pToCast is Vector4<uint> )
            {
                Vector4<uint> lCast = pToCast as Vector4<uint>;
                return new Vector4H( new Half( lCast.X ), new Half( lCast.Y ), new Half( lCast.Z ), new Half( lCast.W ) );
            }
            else if ( pToCast is Vector4<float> )
            {
                Vector4<float> lCast = pToCast as Vector4<float>;
                return new Vector4H( new Half( lCast.X ), new Half( lCast.Y ), new Half( lCast.Z ), new Half( lCast.W ) );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
