﻿using System;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="IVector{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public interface IVector<T> : IEquatable<IVector<T>>, ICloneable where T : struct
    {
        #region Properties

        /// <summary>
        /// Gets the vector component count.
        /// </summary>
        int ComponentCount
        {
            get;
        }

        /// <summary>
        /// Gets the flatten vector data.
        /// </summary>
        T[] Data
        {
            get;
        }

        /// <summary>
        /// Gets or sets the vector value at the specified index.
        /// </summary>
        /// <param name="pIndex">The index to set the value at.</param>
        /// <returns>The value at the specified index.</returns>
        T this[int pIndex]
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Gets the vector length.
        /// </summary>
        /// <returns>The vector's length.</returns>
        T Length();

        /// <summary>
        /// Gets the vector length squared.
        /// </summary>
        /// <returns>The vector's length squared.</returns>
        T LengthSq();

        /// <summary>
        /// Gets the scalar product of this vector and the given one.
        /// </summary>
        /// <param name="pOther">The other vector the dot product must be found with.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The scalar product</returns>
        T DotProduct(IVector<T> pOther, out bool pResult);

        /// <summary>
        /// Gets the vector product of this vector and the given one, that is, the perpendicular vector.
        /// </summary>
        /// <param name="pOther">The second vector involved in the vector product.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The vector perpendicular to the two others</returns>
        IVector<T> CrossProduct(IVector<T> pOther, out bool pResult);

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <returns>The normalized vector.</returns>
        IVector<T> Normalize();

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector.</returns>
        IVector<T> Normalize(out T pPreviousLength);

        /// <summary>
        /// Normalizes this vector to the given length.
        /// </summary>
        /// <param name="pLength">The length this vector must be normalized to.</param>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector to the given length.</returns>
        IVector<T> Normalize(T pLength, out T pPreviousLength);
        
        /// <summary>
        /// Turns this vector into another vector of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new vector of another primitive type.</returns>
        IVector<OtherT> Cast<OtherT>() where OtherT : struct;

        /// <summary>
        /// Adds this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to add to this one.</param>
        /// <returns>The new vector result of the addition.</returns>
        IVector<T> Add(IVector<T> pOther);

        /// <summary>
        /// Subtracts this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to subtract to this one.</param>
        /// <returns>The new vector result of the subtraction.</returns>
        IVector<T> Subtract(IVector<T> pOther);

        /// <summary>
        /// Multiplies this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        IVector<T> Multiply(T pScale);

        /// <summary>
        /// Multiplies this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        IVector<T> Multiply(IVector<T> pScale);

        /// <summary>
        /// Divides this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        IVector<T> Divide(T pScale);

        /// <summary>
        /// Divides this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        IVector<T> Divide(IVector<T> pScale);

        /// <summary>
        /// Inverses this vector.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        IVector<T> Inverse();

        #endregion Methods
    }
}
