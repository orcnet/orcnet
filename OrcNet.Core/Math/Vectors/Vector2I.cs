﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Vector2I"/> class.
    /// </summary>
    public class Vector2I : Vector2<int>
    {
        #region Properties

        /// <summary>
        /// Gets the Zero vector.
        /// </summary>
        public static Vector2I ZERO
        {
            get
            {
                return new Vector2I( 0 );
            }
        }

        /// <summary>
        /// Gets the X unit vector.
        /// </summary>
        public static Vector2I UNIT_X
        {
            get
            {
                return new Vector2I( 1, 0 );
            }
        }

        /// <summary>
        /// Gets the Y unit vector.
        /// </summary>
        public static Vector2I UNIT_Y
        {
            get
            {
                return new Vector2I( 0, 1 );
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(int) * 2; // two floating point components.
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #endregion properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Vector2I"/> class.
        /// </summary>
        static Vector2I()
        {
            sCasters.Add( typeof(int), CastToInt );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2I"/> class.
        /// </summary>
        public Vector2I() :
        this( 0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2I"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Vector2I(int pValue) :
        this( pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2I"/> class.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        public Vector2I(int pX, int pY) :
        base( pX, pY )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods IVector

        /// <summary>
        /// Gets the vector length.
        /// </summary>
        /// <returns>The vector's length.</returns>
        public override int Length()
        {
            return (int)System.Math.Sqrt( (double)this.LengthSq() );
        }

        /// <summary>
        /// Gets the vector length squared.
        /// </summary>
        /// <returns>The vector's length squared.</returns>
        public override int LengthSq()
        {
            return this.X * this.X + this.Y * this.Y;
        }

        /// <summary>
        /// Gets the scalar product of this vector and the given one.
        /// </summary>
        /// <param name="pOther">The other vector the dot product must be found with.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The scalar product</returns>
        public override int DotProduct(IVector<int> pOther, out bool pResult)
        {
            pResult = false;

            Vector2<int> lOther = pOther as Vector2<int>;
            if ( lOther != null )
            {
                pResult = true;
                return this.X * lOther.X + this.Y * lOther.Y;
            }

            return 0;
        }

        /// <summary>
        /// Gets the vector product of this vector and the given one, that is, the perpendicular vector.
        /// </summary>
        /// <param name="pOther">The second vector involved in the vector product.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The vector perpendicular to the two others</returns>
        public override IVector<int> CrossProduct(IVector<int> pOther, out bool pResult)
        {
            pResult = false;

            Vector2<int> lOther = pOther as Vector2<int>;
            if ( lOther != null )
            {
                pResult = true;
                return new Vector2I( this.X * lOther.Y - this.Y * lOther.X );
            }

            return new Vector2I();
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <returns>The normalized vector.</returns>
        public override IVector<int> Normalize()
        {
            int lScale = 1 / this.Length();
            return new Vector2I( this.X * lScale, this.Y * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector.</returns>
        public override IVector<int> Normalize(out int pPreviousLength)
        {
            pPreviousLength = this.Length();
            int lScale = 1 / pPreviousLength;
            return new Vector2I( this.X * lScale, this.Y * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the given length.
        /// </summary>
        /// <param name="pLength">The length this vector must be normalized to.</param>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector to the given length.</returns>
        public override IVector<int> Normalize(int pLength, out int pPreviousLength)
        {
            pPreviousLength = this.Length();
            int lScale = pLength / pPreviousLength;
            return new Vector2I( this.X * lScale, this.Y * lScale );
        }
        
        #endregion Methods IVector

        #region Methods Computation

        /// <summary>
        /// Adds this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to add to this one.</param>
        /// <returns>The new vector result of the addition.</returns>
        protected override AVector<int> InternalAdd(AVector<int> pOther)
        {
            Vector2<int> lOther = pOther as Vector2<int>;
            return new Vector2I( this.X + lOther.X, this.Y + lOther.Y );
        }

        /// <summary>
        /// Subtracts this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to subtract to this one.</param>
        /// <returns>The new vector result of the subtraction.</returns>
        protected override AVector<int> InternalSubtract(AVector<int> pOther)
        {
            Vector2<int> lOther = pOther as Vector2<int>;
            return new Vector2I( this.X - lOther.X, this.Y - lOther.Y );
        }

        /// <summary>
        /// Multiplies this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<int> InternalMultiply(int pScale)
        {
            return new Vector2I( this.X * pScale, this.Y * pScale );
        }

        /// <summary>
        /// Multiplies this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<int> InternalMultiply(AVector<int> pScale)
        {
            Vector2<int> lOther = pScale as Vector2<int>;
            return new Vector2I( this.X * lOther.X, this.Y * lOther.Y );
        }

        /// <summary>
        /// Divides this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<int> InternalDivide(int pScale)
        {
            int lScale = pScale != 0 ? pScale : 1; // Avoid to divide by zero.

            return new Vector2I( this.X / lScale, this.Y / lScale );
        }

        /// <summary>
        /// Divides this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<int> InternalDivide(AVector<int> pScale)
        {
            Vector2<int> lOther = pScale as Vector2<int>;
            int lScaleX = lOther.X != 0 ? lOther.X : 1; // Avoid to divide by zero.
            int lScaleY = lOther.Y != 0 ? lOther.Y : 1; // Avoid to divide by zero.

            return new Vector2I( this.X / lScaleX, this.Y / lScaleY );
        }

        /// <summary>
        /// Inverses this vector.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AVector<int> InternalInverse()
        {
            return new Vector2I( -this.X, -this.Y );
        }

        #endregion Methods Computation

        #region Methods Equality

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        protected override bool Equals(AVector<int> pOther)
        {
            Vector2I lOther = pOther as Vector2I;
            if ( lOther != null )
            {
                return this.X == lOther.X &&
                       this.Y == lOther.Y;
            }

            return false;
        }

        #endregion Methods Equality

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AVector<int> InternalClone()
        {
            return new Vector2I( this.X, this.Y );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToInt(object pToCast)
        {
            if ( pToCast is Vector2<float> )
            {
                Vector2<float> lCast = pToCast as Vector2<float>;
                return new Vector2I( (int)lCast.X, (int)lCast.Y );
            }
            else if ( pToCast is Vector2<double> )
            {
                Vector2<double> lCast = pToCast as Vector2<double>;
                return new Vector2I( (int)lCast.X, (int)lCast.Y );
            }
            else if ( pToCast is Vector2<int> )
            {
                return (pToCast as Vector2<int>).Clone();
            }
            else if ( pToCast is Vector2<uint> )
            {
                Vector2<uint> lCast = pToCast as Vector2<uint>;
                return new Vector2I( (int)lCast.X, (int)lCast.Y );
            }
            else if ( pToCast is Vector2<Half> )
            {
                Vector2<Half> lCast = pToCast as Vector2<Half>;
                return new Vector2I( (int)lCast.X, (int)lCast.Y );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
