﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Vector4F"/> class.
    /// </summary>
    public class Vector4F : Vector4<float>
    {
        #region Properties

        /// <summary>
        /// Gets the Zero vector.
        /// </summary>
        public static Vector4F ZERO
        {
            get
            {
                return new Vector4F( 0.0f );
            }
        }

        /// <summary>
        /// Gets the X unit vector.
        /// </summary>
        public static Vector4F UNIT_X
        {
            get
            {
                return new Vector4F( 1.0f, 0.0f, 0.0f, 0.0f );
            }
        }

        /// <summary>
        /// Gets the Y unit vector.
        /// </summary>
        public static Vector4F UNIT_Y
        {
            get
            {
                return new Vector4F( 0.0f, 1.0f, 0.0f, 0.0f );
            }
        }

        /// <summary>
        /// Gets the Z unit vector.
        /// </summary>
        public static Vector4F UNIT_Z
        {
            get
            {
                return new Vector4F( 0.0f, 0.0f, 1.0f, 0.0f );
            }
        }

        /// <summary>
        /// Gets the W unit vector.
        /// </summary>
        public static Vector4F UNIT_W
        {
            get
            {
                return new Vector4F( 0.0f, 0.0f, 0.0f, 1.0f );
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(float) * 4; // four floating point components.
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #endregion properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Vector4F"/> class.
        /// </summary>
        static Vector4F()
        {
            sCasters.Add( typeof(float), CastToFloat );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4F"/> class.
        /// </summary>
        public Vector4F() :
        this( 0.0f )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4F"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Vector4F(float pValue) :
        this( pValue, pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector4F"/> class.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        /// <param name="pZ">The Z component.</param>
        /// <param name="pW">The W component.</param>
        public Vector4F(float pX, float pY, float pZ, float pW) :
        base( pX, pY, pZ, pW )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods IVector

        /// <summary>
        /// Gets the vector length.
        /// </summary>
        /// <returns>The vector's length.</returns>
        public override float Length()
        {
            return (float)System.Math.Sqrt( (double)this.LengthSq() );
        }

        /// <summary>
        /// Gets the vector length squared.
        /// </summary>
        /// <returns>The vector's length squared.</returns>
        public override float LengthSq()
        {
            return this.X * this.X + this.Y * this.Y + this.Z * this.Z + this.W * this.W;
        }

        /// <summary>
        /// Gets the scalar product of this vector and the given one.
        /// </summary>
        /// <param name="pOther">The other vector the dot product must be found with.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The scalar product</returns>
        public override float DotProduct(IVector<float> pOther, out bool pResult)
        {
            pResult = false;

            Vector4<float> lOther = pOther as Vector4<float>;
            if ( lOther != null )
            {
                pResult = true;
                return this.X * lOther.X + this.Y * lOther.Y + this.Z * lOther.Z + this.W * lOther.W;
            }

            return 0.0f;
        }

        /// <summary>
        /// Gets the vector product of this vector and the given one, that is, the perpendicular vector.
        /// </summary>
        /// <param name="pOther">The second vector involved in the vector product.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The vector perpendicular to the two others</returns>
        public override IVector<float> CrossProduct(IVector<float> pOther, out bool pResult)
        {
            pResult = false;

            Vector4<float> lOther = pOther as Vector4<float>;
            if ( lOther != null )
            {
                pResult = true;
                return new Vector4F( this.Y * lOther.Z - this.Z * lOther.Y,
                                     this.Z * lOther.X - this.X * lOther.Z,
                                     this.X * lOther.Y - this.Y * lOther.X,
                                     0.0f ); // Cross product give us a direction, not a point.
            }

            return new Vector4F();
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <returns>The normalized vector.</returns>
        public override IVector<float> Normalize()
        {
            float lScale = 1.0f / this.Length();
            return new Vector4F( this.X * lScale, this.Y * lScale, this.Z * lScale, this.W * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector.</returns>
        public override IVector<float> Normalize(out float pPreviousLength)
        {
            pPreviousLength = this.Length();
            float lScale = 1.0f / pPreviousLength;
            return new Vector4F( this.X * lScale, this.Y * lScale, this.Z * lScale, this.W * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the given length.
        /// </summary>
        /// <param name="pLength">The length this vector must be normalized to.</param>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector to the given length.</returns>
        public override IVector<float> Normalize(float pLength, out float pPreviousLength)
        {
            pPreviousLength = this.Length();
            float lScale = pLength / pPreviousLength;
            return new Vector4F( this.X * lScale, this.Y * lScale, this.Z * lScale, this.W * lScale );
        }
        
        #endregion Methods IVector

        #region Methods Computation

        /// <summary>
        /// Adds this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to add to this one.</param>
        /// <returns>The new vector result of the addition.</returns>
        protected override AVector<float> InternalAdd(AVector<float> pOther)
        {
            Vector4<float> lOther = pOther as Vector4<float>;
            return new Vector4F( this.X + lOther.X, this.Y + lOther.Y, this.Z + lOther.Z, this.W + lOther.W );
        }

        /// <summary>
        /// Subtracts this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to subtract to this one.</param>
        /// <returns>The new vector result of the subtraction.</returns>
        protected override AVector<float> InternalSubtract(AVector<float> pOther)
        {
            Vector4<float> lOther = pOther as Vector4<float>;
            return new Vector4F( this.X - lOther.X, this.Y - lOther.Y, this.Z - lOther.Z, this.W - lOther.W );
        }

        /// <summary>
        /// Multiplies this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<float> InternalMultiply(float pScale)
        {
            return new Vector4F( this.X * pScale, this.Y * pScale, this.Z * pScale, this.W * pScale );
        }

        /// <summary>
        /// Multiplies this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<float> InternalMultiply(AVector<float> pScale)
        {
            Vector4<float> lOther = pScale as Vector4<float>;
            return new Vector4F( this.X * lOther.X, this.Y * lOther.Y, this.Z * lOther.Z, this.W * lOther.W );
        }

        /// <summary>
        /// Divides this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<float> InternalDivide(float pScale)
        {
            float lScale = System.Math.Abs( pScale ) > float.Epsilon ? pScale : 1.0f; // Avoid to divide by zero.

            return new Vector4F( this.X / lScale, this.Y / lScale, this.Z / lScale, this.W / lScale );
        }

        /// <summary>
        /// Divides this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<float> InternalDivide(AVector<float> pScale)
        {
            Vector4<float> lOther = pScale as Vector4<float>;
            float lScaleX = System.Math.Abs( lOther.X ) > float.Epsilon ? lOther.X : 1.0f; // Avoid to divide by zero.
            float lScaleY = System.Math.Abs( lOther.Y ) > float.Epsilon ? lOther.Y : 1.0f; // Avoid to divide by zero.
            float lScaleZ = System.Math.Abs( lOther.Z ) > float.Epsilon ? lOther.Z : 1.0f; // Avoid to divide by zero.
            float lScaleW = System.Math.Abs( lOther.W ) > float.Epsilon ? lOther.W : 1.0f; // Avoid to divide by zero.

            return new Vector4F( this.X / lScaleX, this.Y / lScaleY, this.Z / lScaleZ, this.W / lScaleW );
        }

        /// <summary>
        /// Inverses this vector.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AVector<float> InternalInverse()
        {
            return new Vector4F( -this.X, -this.Y, -this.Z, this.W );
        }

        #endregion Methods Computation

        #region Methods Equality

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        protected override bool Equals(AVector<float> pOther)
        {
            Vector4F lOther = pOther as Vector4F;
            if ( lOther != null )
            {
                return this.X == lOther.X &&
                       this.Y == lOther.Y &&
                       this.Z == lOther.Z &&
                       this.W == lOther.W;
            }

            return false;
        }

        #endregion Methods Equality

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AVector<float> InternalClone()
        {
            return new Vector4F( this.X, this.Y, this.Z, this.W );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToFloat(object pToCast)
        {
            if ( pToCast is Vector4<float> )
            {
                return (pToCast as Vector4<float>).Clone();
            }
            else if ( pToCast is Vector4<double> )
            {
                Vector4<double> lCast = pToCast as Vector4<double>;
                return new Vector4F( (float)lCast.X, (float)lCast.Y, (float)lCast.Z, (float)lCast.W );
            }
            else if ( pToCast is Vector4<int> )
            {
                Vector4<int> lCast = pToCast as Vector4<int>;
                return new Vector4F( lCast.X, lCast.Y, lCast.Z, lCast.W );
            }
            else if ( pToCast is Vector4<uint> )
            {
                Vector4<uint> lCast = pToCast as Vector4<uint>;
                return new Vector4F( lCast.X, lCast.Y, lCast.Z, lCast.W );
            }
            else if ( pToCast is Vector4<Half> )
            {
                Vector4<Half> lCast = pToCast as Vector4<Half>;
                return new Vector4F( lCast.X, lCast.Y, lCast.Z, lCast.W );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
