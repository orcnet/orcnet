﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Vector3I"/> class.
    /// </summary>
    public class Vector3I : Vector3<int>
    {
        #region Properties

        /// <summary>
        /// Gets the Zero vector.
        /// </summary>
        public static Vector3I ZERO
        {
            get
            {
                return new Vector3I( 0 );
            }
        }

        /// <summary>
        /// Gets the X unit vector.
        /// </summary>
        public static Vector3I UNIT_X
        {
            get
            {
                return new Vector3I( 1, 0, 0 );
            }
        }

        /// <summary>
        /// Gets the Y unit vector.
        /// </summary>
        public static Vector3I UNIT_Y
        {
            get
            {
                return new Vector3I( 0, 1, 0 );
            }
        }

        /// <summary>
        /// Gets the Z unit vector.
        /// </summary>
        public static Vector3I UNIT_Z
        {
            get
            {
                return new Vector3I( 0, 0, 1 );
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(int) * 3; // three floating point components.
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #endregion properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Vector3I"/> class.
        /// </summary>
        static Vector3I()
        {
            sCasters.Add( typeof(int), CastToInt );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3I"/> class.
        /// </summary>
        public Vector3I() :
        this( 0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3I"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Vector3I(int pValue) :
        this( pValue, pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3I"/> class.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        /// <param name="pZ">The Z component.</param>
        public Vector3I(int pX, int pY, int pZ) :
        base( pX, pY, pZ )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods IVector

        /// <summary>
        /// Gets the vector length.
        /// </summary>
        /// <returns>The vector's length.</returns>
        public override int Length()
        {
            return (int)System.Math.Sqrt( (double)this.LengthSq() );
        }

        /// <summary>
        /// Gets the vector length squared.
        /// </summary>
        /// <returns>The vector's length squared.</returns>
        public override int LengthSq()
        {
            return this.X * this.X + this.Y * this.Y + this.Z * this.Z;
        }

        /// <summary>
        /// Gets the scalar product of this vector and the given one.
        /// </summary>
        /// <param name="pOther">The other vector the dot product must be found with.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The scalar product</returns>
        public override int DotProduct(IVector<int> pOther, out bool pResult)
        {
            pResult = false;

            Vector3<int> lOther = pOther as Vector3<int>;
            if ( lOther != null )
            {
                pResult = true;
                return this.X * lOther.X + this.Y * lOther.Y + this.Z * lOther.Z;
            }

            return 0;
        }

        /// <summary>
        /// Gets the vector product of this vector and the given one, that is, the perpendicular vector.
        /// </summary>
        /// <param name="pOther">The second vector involved in the vector product.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The vector perpendicular to the two others</returns>
        public override IVector<int> CrossProduct(IVector<int> pOther, out bool pResult)
        {
            pResult = false;

            Vector3<int> lOther = pOther as Vector3<int>;
            if ( lOther != null )
            {
                pResult = true;
                return new Vector3I( this.Y * lOther.Z - this.Z * lOther.Y,
                                     this.Z * lOther.X - this.X * lOther.Z,
                                     this.X * lOther.Y - this.Y * lOther.X );
            }

            return new Vector3I();
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <returns>The normalized vector.</returns>
        public override IVector<int> Normalize()
        {
            int lScale = 1 / this.Length();
            return new Vector3I( this.X * lScale, this.Y * lScale, this.Z * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector.</returns>
        public override IVector<int> Normalize(out int pPreviousLength)
        {
            pPreviousLength = this.Length();
            int lScale = 1 / pPreviousLength;
            return new Vector3I( this.X * lScale, this.Y * lScale, this.Z * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the given length.
        /// </summary>
        /// <param name="pLength">The length this vector must be normalized to.</param>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector to the given length.</returns>
        public override IVector<int> Normalize(int pLength, out int pPreviousLength)
        {
            pPreviousLength = this.Length();
            int lScale = pLength / pPreviousLength;
            return new Vector3I( this.X * lScale, this.Y * lScale, this.Z * lScale );
        }
        
        #endregion Methods IVector

        #region Methods Computation

        /// <summary>
        /// Adds this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to add to this one.</param>
        /// <returns>The new vector result of the addition.</returns>
        protected override AVector<int> InternalAdd(AVector<int> pOther)
        {
            Vector3<int> lOther = pOther as Vector3<int>;
            return new Vector3I( this.X + lOther.X, this.Y + lOther.Y, this.Z + lOther.Z );
        }

        /// <summary>
        /// Subtracts this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to subtract to this one.</param>
        /// <returns>The new vector result of the subtraction.</returns>
        protected override AVector<int> InternalSubtract(AVector<int> pOther)
        {
            Vector3<int> lOther = pOther as Vector3<int>;
            return new Vector3I( this.X - lOther.X, this.Y - lOther.Y, this.Z - lOther.Z );
        }

        /// <summary>
        /// Multiplies this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<int> InternalMultiply(int pScale)
        {
            return new Vector3I( this.X * pScale, this.Y * pScale, this.Z * pScale );
        }

        /// <summary>
        /// Multiplies this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<int> InternalMultiply(AVector<int> pScale)
        {
            Vector3<int> lOther = pScale as Vector3<int>;
            return new Vector3I( this.X * lOther.X, this.Y * lOther.Y, this.Z * lOther.Z );
        }

        /// <summary>
        /// Divides this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<int> InternalDivide(int pScale)
        {
            int lScale = System.Math.Abs( pScale ) > float.Epsilon ? pScale : 1; // Avoid to divide by zero.

            return new Vector3I( this.X / lScale, this.Y / lScale, this.Z / lScale );
        }

        /// <summary>
        /// Divides this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<int> InternalDivide(AVector<int> pScale)
        {
            Vector3<int> lOther = pScale as Vector3<int>;
            int lScaleX = System.Math.Abs( lOther.X ) > float.Epsilon ? lOther.X : 1; // Avoid to divide by zero.
            int lScaleY = System.Math.Abs( lOther.Y ) > float.Epsilon ? lOther.Y : 1; // Avoid to divide by zero.
            int lScaleZ = System.Math.Abs( lOther.Z ) > float.Epsilon ? lOther.Z : 1; // Avoid to divide by zero.

            return new Vector3I( this.X / lScaleX, this.Y / lScaleY, this.Z / lScaleZ );
        }

        /// <summary>
        /// Inverses this vector.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AVector<int> InternalInverse()
        {
            return new Vector3I( -this.X, -this.Y, -this.Z );
        }

        #endregion Methods Computation

        #region Methods Equality

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        protected override bool Equals(AVector<int> pOther)
        {
            Vector3I lOther = pOther as Vector3I;
            if ( lOther != null )
            {
                return this.X == lOther.X &&
                       this.Y == lOther.Y &&
                       this.Z == lOther.Z;
            }

            return false;
        }

        #endregion Methods Equality

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AVector<int> InternalClone()
        {
            return new Vector3I( this.X, this.Y, this.Z );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToInt(object pToCast)
        {
            if ( pToCast is Vector3<int> )
            {
                return (pToCast as Vector3<int>).Clone();
            }
            else if ( pToCast is Vector3<uint> )
            {
                Vector3<uint> lCast = pToCast as Vector3<uint>;
                return new Vector3I( (int)lCast.X, (int)lCast.Y, (int)lCast.Z );
            }
            else if ( pToCast is Vector3<double> )
            {
                Vector3<double> lCast = pToCast as Vector3<double>;
                return new Vector3I( (int)lCast.X, (int)lCast.Y, (int)lCast.Z );
            }
            else if ( pToCast is Vector3<float> )
            {
                Vector3<float> lCast = pToCast as Vector3<float>;
                return new Vector3I( (int)lCast.X, (int)lCast.Y, (int)lCast.Z );
            }
            else if ( pToCast is Vector3<Half> )
            {
                Vector3<Half> lCast = pToCast as Vector3<Half>;
                return new Vector3I( (int)lCast.X, (int)lCast.Y, (int)lCast.Z );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
