﻿using System.Linq;
using System.Text;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the abstract <see cref="AVector{T}"/> class.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public abstract class AVector<T> : AMemoryProfilable, IVector<T> where T : struct
    {
        #region Delegates

        /// <summary>
        /// Delegate prototype definition of method casting a vector from one primitive type to another.
        /// </summary>
        /// <param name="pToCast">The vector to cast.</param>
        /// <returns>The casted vector.</returns>
        protected delegate object CastDelegate(object pToCast);

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the vector data.
        /// </summary>
        protected T[] mData;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flatten vector data.
        /// </summary>
        public T[] Data
        {
            get
            {
                return this.mData.ToArray(); // Copy.
            }
        }

        /// <summary>
        /// Gets or sets the vector value at the specified index.
        /// </summary>
        /// <param name="pIndex">The index to set the value at.</param>
        /// <returns>The value at the specified index.</returns>
        public T this[int pIndex]
        {
            get
            {
                return this.mData[ pIndex ];
            }
            set
            {
                this.mData[ pIndex ] = value;
            }
        }

        /// <summary>
        /// Gets the vector component count.
        /// </summary>
        public abstract int ComponentCount
        {
            get;
        }

        #endregion Properties

        #region Constructor
        
        /// <summary>
        /// Initializes a new instance of the <see cref="AVector{T}"/> class.
        /// </summary>
        protected AVector()
        {

        }

        #endregion Constructor

        #region Methods

        #region Methods IVector

        /// <summary>
        /// Gets the vector length.
        /// </summary>
        /// <returns>The vector's length.</returns>
        public abstract T Length();

        /// <summary>
        /// Gets the vector length squared.
        /// </summary>
        /// <returns>The vector's length squared.</returns>
        public abstract T LengthSq();

        /// <summary>
        /// Gets the scalar product of this vector and the given one.
        /// </summary>
        /// <param name="pOther">The other vector the dot product must be found with.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The scalar product</returns>
        public abstract T DotProduct(IVector<T> pOther, out bool pResult);

        /// <summary>
        /// Gets the vector product of this vector and the given one, that is, the perpendicular vector.
        /// </summary>
        /// <param name="pOther">The second vector involved in the vector product.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The vector perpendicular to the two others, -1 if the given vector is not allowed.</returns>
        public abstract IVector<T> CrossProduct(IVector<T> pOther, out bool pResult);

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <returns>The normalized vector.</returns>
        public abstract IVector<T> Normalize();

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector.</returns>
        public abstract IVector<T> Normalize(out T pPreviousLength);

        /// <summary>
        /// Normalizes this vector to the given length.
        /// </summary>
        /// <param name="pLength">The length this vector must be normalized to.</param>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector to the given length.</returns>
        public abstract IVector<T> Normalize(T pLength, out T pPreviousLength);

        /// <summary>
        /// Turns this vector into another vector of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new vector of another primitive type, null if cannot cast into the given primitive type.</returns>
        public abstract IVector<OtherT> Cast<OtherT>() where OtherT : struct;

        /// <summary>
        /// Adds this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to add to this one.</param>
        /// <returns>The new vector result of the addition.</returns>
        public IVector<T> Add(IVector<T> pOther)
        {
            AVector<T> lOther = pOther as AVector<T>;
            if ( lOther != null )
            {
                return this.InternalAdd( lOther );
            }

            // Returns this vector by default as addition failure result.
            return this.Clone() as IVector<T>;
        }

        /// <summary>
        /// Subtracts this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to subtract to this one.</param>
        /// <returns>The new vector result of the subtraction.</returns>
        public IVector<T> Subtract(IVector<T> pOther)
        {
            AVector<T> lOther = pOther as AVector<T>;
            if ( lOther != null )
            {
                return this.InternalSubtract( lOther );
            }

            // Returns this vector by default as subtraction failure result.
            return this.Clone() as IVector<T>;
        }

        /// <summary>
        /// Multiplies this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        public IVector<T> Multiply(T pScale)
        {
            return this.InternalMultiply( pScale );
        }

        /// <summary>
        /// Multiplies this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        public IVector<T> Multiply(IVector<T> pScale)
        {
            AVector<T> lScale = pScale as AVector<T>;
            if ( lScale != null )
            {
                return this.InternalMultiply( lScale );
            }

            // Returns this vector by default as multiplication failure result.
            return this.Clone() as IVector<T>;
        }

        /// <summary>
        /// Divides this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        public IVector<T> Divide(T pScale)
        {
            return this.InternalDivide( pScale );
        }

        /// <summary>
        /// Divides this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        public IVector<T> Divide(IVector<T> pScale)
        {
            AVector<T> lScale = pScale as AVector<T>;
            if ( lScale != null )
            {
                return this.InternalDivide( lScale );
            }

            // Returns this vector by default as division failure result.
            return this.Clone() as IVector<T>;
        }

        /// <summary>
        /// Inverses this vector.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        public IVector<T> Inverse()
        {
            return this.InternalInverse();
        }

        #endregion Methods IVector

        #region Methods Computation

        /// <summary>
        /// Adds this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to add to this one.</param>
        /// <returns>The new vector result of the addition.</returns>
        protected abstract AVector<T> InternalAdd(AVector<T> pOther);

        /// <summary>
        /// Subtracts this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to subtract to this one.</param>
        /// <returns>The new vector result of the subtraction.</returns>
        protected abstract AVector<T> InternalSubtract(AVector<T> pOther);

        /// <summary>
        /// Multiplies this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected abstract AVector<T> InternalMultiply(T pScale);

        /// <summary>
        /// Multiplies this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected abstract AVector<T> InternalMultiply(AVector<T> pScale);

        /// <summary>
        /// Divides this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected abstract AVector<T> InternalDivide(T pScale);

        /// <summary>
        /// Divides this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected abstract AVector<T> InternalDivide(AVector<T> pScale);

        /// <summary>
        /// Inverses this vector.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected abstract AVector<T> InternalInverse();

        #endregion Methods Computation

        #region Methods Equality

        /// <summary>
        /// Compares the specified instances for equality.
        /// </summary>
        /// <param name="pFirst">The first vector to test.</param>
        /// <param name="pSecond">The second vector to test.</param>
        /// <returns>True if both instances are equal, false otherwise.</returns>
        public static bool operator == (AVector<T> pFirst, AVector<T> pSecond)
        {
            bool lIsFirstNull  = object.ReferenceEquals( pFirst, null );
            bool lIsSecondNull = object.ReferenceEquals( pSecond, null );
            if( lIsFirstNull && lIsSecondNull ) // If both null, equal.
            {
                return true;
            }
            else if ( lIsFirstNull || lIsSecondNull ) // If one of the two is null, not equal.
            {
                return false;
            }

            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Compares the specified instances for inequality.
        /// </summary>
        /// <param name="pFirst">The first vector to test.</param>
        /// <param name="pSecond">The second vector to test.</param>
        /// <returns>True if both instances are not equal, false otherwise.</returns>
        public static bool operator != (AVector<T> pFirst, AVector<T> pSecond)
        {
            bool lIsFirstNull  = object.ReferenceEquals( pFirst, null );
            bool lIsSecondNull = object.ReferenceEquals( pSecond, null );
            if( lIsFirstNull && lIsSecondNull ) // If both null, equal.
            {
                return false;
            }
            else if ( lIsFirstNull || lIsSecondNull ) // If one of the two is null, not equal.
            {
                return true;
            }

            return pFirst.Equals( pSecond ) == false;
        }

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            IVector<T> lOther = pOther as IVector<T>;
            if ( lOther != null )
            {
                return this.Equals( lOther );
            }

            return false;
        }

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(IVector<T> pOther)
        {
            AVector<T> lOther = pOther as AVector<T>;
            if ( lOther != null && 
                 this.ComponentCount == lOther.ComponentCount )
            {
                return this.Equals( lOther );
            }

            return false;
        }

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        protected abstract bool Equals(AVector<T> pOther);
        
        /// <summary>
        /// Gets the vector's hash code.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            int lHashcode = this.mData[ 0 ].GetHashCode();
            for ( int lCurr = 1; lCurr < this.ComponentCount; lCurr++ )
            {
                lHashcode ^= this.mData[ lCurr ].GetHashCode();
            }

            return lHashcode;
        }

        #endregion Methods Equality

        #region Methods Statics

        /// <summary>
        /// Adds two vectors together.
        /// </summary>
        /// <param name="pFirst">The first vector to add</param>
        /// <param name="pSecond">The second vector to add</param>
        /// <returns>The new vector result of the addition.</returns>
        public static AVector<T> operator + (AVector<T> pFirst, AVector<T> pSecond)
        {
            return pFirst.InternalAdd( pSecond );
        }

        /// <summary>
        /// Subtracts two vectors together.
        /// </summary>
        /// <param name="pFirst">The first vector to subtract</param>
        /// <param name="pSecond">The second vector to subtract</param>
        /// <returns>The new vector result of the subtraction.</returns>
        public static AVector<T> operator - (AVector<T> pFirst, AVector<T> pSecond)
        {
            return pFirst.InternalSubtract( pSecond );
        }

        /// <summary>
        /// Multiplies the given vector by the given scalar.
        /// </summary>
        /// <param name="pToMultiply">The vector to multiply.</param>
        /// <param name="pScale">The scalar to multiply by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        public static AVector<T> operator * (AVector<T> pToMultiply, T pScale)
        {
            return pToMultiply.InternalMultiply( pScale );
        }

        /// <summary>
        /// Multiplies the given vector by the given scalar.
        /// </summary>
        /// <param name="pToMultiply">The vector to multiply.</param>
        /// <param name="pScale">The scalar to multiply by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        public static AVector<T> operator * (T pScale, AVector<T> pToMultiply)
        {
            return pToMultiply.InternalMultiply( pScale );
        }

        /// <summary>
        /// Divides the given vector by the given scalar.
        /// </summary>
        /// <param name="pToDivide">The vector to divide.</param>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        public static AVector<T> operator / (AVector<T> pToDivide, T pScale)
        {
            return pToDivide.InternalDivide( pScale );
        }

        /// <summary>
        /// Divides the given vector by the given scalar.
        /// </summary>
        /// <param name="pToDivide">The vector to divide.</param>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        public static AVector<T> operator / (T pScale, AVector<T> pToDivide)
        {
            return pToDivide.InternalDivide( pScale );
        }

        /// <summary>
        /// Inverses the given vector.
        /// </summary>
        /// <param name="pToInverse">The vector to inverse.</param>
        /// <returns>The new inversed vector.</returns>
        public static AVector<T> operator - (AVector<T> pToInverse)
        {
            return pToInverse.InternalInverse();
        }

        #endregion Methods Statics

        #region Methods ICloneable

        /// <summary>
        /// Clone this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        public object Clone()
        {
            return this.InternalClone();
        }

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected abstract AVector<T> InternalClone();

        #endregion Methods ICloneable

        #region Methods ToString

        /// <summary>
        /// Turns the vector into a string.
        /// </summary>
        /// <returns>The string representation of theis vector.</returns>
        public override string ToString()
        {
            StringBuilder lBuilder = new StringBuilder( "(" );
            lBuilder.Append( this.mData[ 0 ].ToString() );
            for ( int lCurr = 1; lCurr < this.ComponentCount; lCurr++ )
            {
                lBuilder.Append( ", " );
                lBuilder.Append( this.mData[ lCurr ].ToString() );
            }
            lBuilder.Append( ")" );

            return lBuilder.ToString();
        }

        #endregion Methods ToString

        #endregion Methods
    }
}
