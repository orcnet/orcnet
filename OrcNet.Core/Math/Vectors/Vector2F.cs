﻿namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Vector2F"/> class.
    /// </summary>
    public class Vector2F : Vector2<float>
    {
        #region Properties

        /// <summary>
        /// Gets the Zero vector.
        /// </summary>
        public static Vector2F ZERO
        {
            get
            {
                return new Vector2F( 0.0f );
            }
        }

        /// <summary>
        /// Gets the X unit vector.
        /// </summary>
        public static Vector2F UNIT_X
        {
            get
            {
                return new Vector2F( 1.0f, 0.0f );
            }
        }

        /// <summary>
        /// Gets the Y unit vector.
        /// </summary>
        public static Vector2F UNIT_Y
        {
            get
            {
                return new Vector2F( 0.0f, 1.0f );
            }
        }

        #region Properties AmemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize = sizeof(float) * 2; // two floating point components.
                return lSize;
            }
        }

        #endregion Properties AmemoryProfilable

        #endregion properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Vector2F"/> class.
        /// </summary>
        static Vector2F()
        {
            sCasters.Add( typeof(float), CastToFloat );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2F"/> class.
        /// </summary>
        public Vector2F() :
        this( 0.0f )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2F"/> class.
        /// </summary>
        /// <param name="pValue">The value to set to all components.</param>
        public Vector2F(float pValue) :
        this( pValue, pValue )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2F"/> class.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        public Vector2F(float pX, float pY) :
        base( pX, pY )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods IVector

        /// <summary>
        /// Gets the vector length.
        /// </summary>
        /// <returns>The vector's length.</returns>
        public override float Length()
        {
            return (float)System.Math.Sqrt( (double)this.LengthSq() );
        }

        /// <summary>
        /// Gets the vector length squared.
        /// </summary>
        /// <returns>The vector's length squared.</returns>
        public override float LengthSq()
        {
            return this.X * this.X + this.Y * this.Y;
        }

        /// <summary>
        /// Gets the scalar product of this vector and the given one.
        /// </summary>
        /// <param name="pOther">The other vector the dot product must be found with.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The scalar product</returns>
        public override float DotProduct(IVector<float> pOther, out bool pResult)
        {
            pResult = false;

            Vector2<float> lOther = pOther as Vector2<float>;
            if ( lOther != null )
            {
                pResult = true;
                return this.X * lOther.X + this.Y * lOther.Y;
            }

            return 0.0f;
        }

        /// <summary>
        /// Gets the vector product of this vector and the given one, that is, the perpendicular vector.
        /// </summary>
        /// <param name="pOther">The second vector involved in the vector product.</param>
        /// <param name="pResult">True if successful, false otherwise (e.g: bad other vector type)</param>
        /// <returns>The vector perpendicular to the two others</returns>
        public override IVector<float> CrossProduct(IVector<float> pOther, out bool pResult)
        {
            pResult = false;

            Vector2<float> lOther = pOther as Vector2<float>;
            if ( lOther != null )
            {
                pResult = true;
                return new Vector2F( this.X * lOther.Y - this.Y * lOther.X );
            }

            return new Vector2F();
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <returns>The normalized vector.</returns>
        public override IVector<float> Normalize()
        {
            float lScale = 1.0f / this.Length();
            return new Vector2F( this.X * lScale, this.Y * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the unit circle.
        /// </summary>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector.</returns>
        public override IVector<float> Normalize(out float pPreviousLength)
        {
            pPreviousLength = this.Length();
            float lScale = 1.0f / pPreviousLength;
            return new Vector2F( this.X * lScale, this.Y * lScale );
        }

        /// <summary>
        /// Normalizes this vector to the given length.
        /// </summary>
        /// <param name="pLength">The length this vector must be normalized to.</param>
        /// <param name="pPreviousLength">Returns the previous length.</param>
        /// <returns>The normalized vector to the given length.</returns>
        public override IVector<float> Normalize(float pLength, out float pPreviousLength)
        {
            pPreviousLength = this.Length();
            float lScale = pLength / pPreviousLength;
            return new Vector2F( this.X * lScale, this.Y * lScale );
        }
        
        #endregion Methods IVector

        #region Methods Computation

        /// <summary>
        /// Adds this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to add to this one.</param>
        /// <returns>The new vector result of the addition.</returns>
        protected override AVector<float> InternalAdd(AVector<float> pOther)
        {
            Vector2<float> lOther = pOther as Vector2<float>;
            return new Vector2F( this.X + lOther.X, this.Y + lOther.Y );
        }

        /// <summary>
        /// Subtracts this vector and the given one together.
        /// </summary>
        /// <param name="pOther">The other vector to subtract to this one.</param>
        /// <returns>The new vector result of the subtraction.</returns>
        protected override AVector<float> InternalSubtract(AVector<float> pOther)
        {
            Vector2<float> lOther = pOther as Vector2<float>;
            return new Vector2F( this.X - lOther.X, this.Y - lOther.Y );
        }

        /// <summary>
        /// Multiplies this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<float> InternalMultiply(float pScale)
        {
            return new Vector2F( this.X * pScale, this.Y * pScale );
        }

        /// <summary>
        /// Multiplies this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be multiplied by.</param>
        /// <returns>The new vector result of the multiplication.</returns>
        protected override AVector<float> InternalMultiply(AVector<float> pScale)
        {
            Vector2<float> lOther = pScale as Vector2<float>;
            return new Vector2F( this.X * lOther.X, this.Y * lOther.Y );
        }

        /// <summary>
        /// Divides this vector by the given scalar.
        /// </summary>
        /// <param name="pScale">The scalar this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<float> InternalDivide(float pScale)
        {
            float lScale = System.Math.Abs( pScale ) > float.Epsilon ? pScale : 1.0f; // Avoid to divide by zero.

            return new Vector2F( this.X / lScale, this.Y / lScale );
        }

        /// <summary>
        /// Divides this vector by the given one.
        /// </summary>
        /// <param name="pScale">The other vector this vector must be divided by.</param>
        /// <returns>The new vector result of the division.</returns>
        protected override AVector<float> InternalDivide(AVector<float> pScale)
        {
            Vector2<float> lOther = pScale as Vector2<float>;
            float lScaleX = System.Math.Abs( lOther.X ) > float.Epsilon ? lOther.X : 1.0f; // Avoid to divide by zero.
            float lScaleY = System.Math.Abs( lOther.Y ) > float.Epsilon ? lOther.Y : 1.0f; // Avoid to divide by zero.

            return new Vector2F( this.X / lScaleX, this.Y / lScaleY );
        }

        /// <summary>
        /// Inverses this vector.
        /// </summary>
        /// <returns>The new inversed vector.</returns>
        protected override AVector<float> InternalInverse()
        {
            return new Vector2F( -this.X, -this.Y );
        }

        #endregion Methods Computation

        #region Methods Equality

        /// <summary>
        /// Checks whether this vector is equal to the given one.
        /// </summary>
        /// <param name="pOther">The other vector to compare.</param>
        /// <returns>True if equal, false otherwise.</returns>
        protected override bool Equals(AVector<float> pOther)
        {
            Vector2F lOther = pOther as Vector2F;
            if ( lOther != null )
            {
                return this.X == lOther.X &&
                       this.Y == lOther.Y;
            }

            return false;
        }

        #endregion Methods Equality

        #region Methods ICloneable

        /// <summary>
        /// Clones this vector.
        /// </summary>
        /// <returns>The vector's clone.</returns>
        protected override AVector<float> InternalClone()
        {
            return new Vector2F( this.X, this.Y );
        }

        #endregion Methods ICloneable

        #region Methods Caster

        /// <summary>
        /// Cast the given object into 
        /// </summary>
        /// <param name="pToCast"></param>
        /// <returns>The casted vector, null if not a vector castable.</returns>
        private static object CastToFloat(object pToCast)
        {
            if ( pToCast is Vector2<float> )
            {
                return (pToCast as Vector2<float>).Clone();
            }
            else if ( pToCast is Vector2<double> )
            {
                Vector2<double> lCast = pToCast as Vector2<double>;
                return new Vector2F( (float)lCast.X, (float)lCast.Y );
            }
            else if ( pToCast is Vector2<int> )
            {
                Vector2<int> lCast = pToCast as Vector2<int>;
                return new Vector2F( lCast.X, lCast.Y );
            }
            else if ( pToCast is Vector2<uint> )
            {
                Vector2<uint> lCast = pToCast as Vector2<uint>;
                return new Vector2F( lCast.X, lCast.Y );
            }
            else if ( pToCast is Vector2<Half> )
            {
                Vector2<Half> lCast = pToCast as Vector2<Half>;
                return new Vector2F( lCast.X, lCast.Y );
            }

            return null;
        }

        #endregion Methods Caster

        #endregion Methods
    }
}
