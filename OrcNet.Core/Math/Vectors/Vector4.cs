﻿using System;
using System.Collections.Generic;

namespace OrcNet.Core.Math
{
    /// <summary>
    /// Definition of the <see cref="Vector3{T}"/> class.
    /// </summary>
    /// <typeparam name="T">The primitive type.</typeparam>
    public abstract class Vector4<T> : AVector<T> where T : struct
    {
        #region Fields

        /// <summary>
        /// Stores the set of cast delegate for the vector cast method.
        /// </summary>
        protected static Dictionary<Type, CastDelegate> sCasters;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the X component.
        /// </summary>
        public T X
        {
            get
            {
                return this.mData[ 0 ];
            }
            set
            {
                this.mData[ 0 ] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Y component.
        /// </summary>
        public T Y
        {
            get
            {
                return this.mData[ 1 ];
            }
            set
            {
                this.mData[ 1 ] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Z component.
        /// </summary>
        public T Z
        {
            get
            {
                return this.mData[ 2 ];
            }
            set
            {
                this.mData[ 2 ] = value;
            }
        }

        /// <summary>
        /// Gets or sets the W component.
        /// </summary>
        public T W
        {
            get
            {
                return this.mData[ 3 ];
            }
            set
            {
                this.mData[ 3 ] = value;
            }
        }

        /// <summary>
        /// Gets the vector component count.
        /// </summary>
        public override int ComponentCount
        {
            get
            {
                return 4;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Vector4{T}"/> class.
        /// </summary>
        static Vector4()
        {
            sCasters = new Dictionary<Type, CastDelegate>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2{T}"/> class.
        /// </summary>
        /// <param name="pX">The X component.</param>
        /// <param name="pY">The Y component.</param>
        /// <param name="pZ">The Z component.</param>
        /// <param name="pW">The W component.</param>
        protected Vector4(T pX, T pY, T pZ, T pW)
        {
            this.mData = new T[ 4 ] { pX, pY, pZ, pW };
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Turns this vector into another vector of a different primitive type.
        /// </summary>
        /// <typeparam name="OtherT">The new primitive type.</typeparam>
        /// <returns>The new vector of another primitive type, null if cannot cast into the given primitive type.</returns>
        public override IVector<OtherT> Cast<OtherT>()
        {
            CastDelegate lCaster = null;
            if ( sCasters.TryGetValue( typeof(OtherT), out lCaster ) )
            {
                return lCaster( this ) as IVector<OtherT>;
            }

            return null;
        }

        #endregion Methods
    }
}
