﻿using System;

namespace OrcNet.Core.Profiling
{
    /// <summary>
    /// Base profiler interface definition.
    /// </summary>
    public interface IProfiler : IDisposable
    {

    }
}
