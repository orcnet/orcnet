﻿using OrcNet.Core.Service;

namespace OrcNet.Core.Profiling
{
    /// <summary>
    /// Base abstract profiler class definition.
    /// </summary>
    public abstract class AProfiler : AService, IProfiler
    {
        #region Fields

        /// <summary>
        /// Stores the profiler name.
        /// </summary>
        protected string mName;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AProfiler"/> class.
        /// </summary>
        protected AProfiler()
        {
            this.mName = "Profiler";
        }

        #endregion Constructor
    }
}
