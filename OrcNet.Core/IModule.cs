﻿namespace OrcNet.Core
{
    /// <summary>
    /// Definition of the <see cref="IModule"/> class.
    /// Different from a plugin.
    /// </summary>
    public interface IModule
    {
        #region Properties

        /// <summary>
        /// Gets the module name.
        /// </summary>
        string Name
        {
            get;
        }

        /// <summary>
        /// Gets the module version.
        /// </summary>
        string Version
        {
            get;
        }

        #endregion Properties
    }
}
