﻿using System;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// Base logger interface definition.
    /// </summary>
    public interface ILogger : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the logger type.
        /// </summary>
        LoggerType Type
        {
            get;
        }

        /// <summary>
        /// Gets the minimal log type this logger supports.
        /// </summary>
        LogType MinType
        {
            get;
        }

        /// <summary>
        /// Gets the maximal log type this logger supports.
        /// </summary>
        LogType MaxType
        {
            get;
        }

        #endregion Properties

        #region Methods
        
        /// <summary>
        /// Logs a message log.
        /// </summary>
        /// <param name="pLog">The specified log</param>
        void Log(LogMessage pLog);

        #endregion Methods
    }
}
