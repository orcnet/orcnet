﻿using System.IO;
using System.Xml.Linq;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// XML logger class definition.
    /// </summary>
    public class XMLLogger : AFileLogger
    {
        #region Properties

        /// <summary>
        /// Gets the logger type.
        /// </summary>
        public override LoggerType Type
        {
            get
            {
                return LoggerType.XML;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="XMLLogger"/> class.
        /// </summary>
        /// <param name="pPath">The file path</param>
        public XMLLogger(string pPath) :
        base( pPath )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods ILogger

        /// <summary>
        /// Logs a message log.
        /// </summary>
        /// <param name="pLog">The specified log</param>
        public override void Log(LogMessage pLog)
        {
            XElement lRoot = null;
            if ( File.Exists( this.mFullPath.FullName ) == false )
            {
                if ( Directory.Exists( this.mFullPath.DirectoryName ) == false )
                {
                    Directory.CreateDirectory( this.mFullPath.DirectoryName );
                }

                using ( File.Create( this.mFullPath.FullName ) )
                { }

                lRoot = new XElement( "Logs" );
            }
            else
            {
                lRoot = XElement.Load( this.mFullPath.FullName );
            }

            int lLogType = (int)pLog.Type;
            if ( lLogType >= (int)this.mMinType &&
                 lLogType <= (int)this.mMaxType )
            {
                XElement lNewLog = pLog.ToXml();
                lRoot.Add( lNewLog );
                lRoot.Save( this.mFullPath.FullName );
            }
        }

        #endregion Methods ILogger

        #endregion Methods
    }
}
