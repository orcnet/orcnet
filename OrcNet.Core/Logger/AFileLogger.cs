﻿using System;
using System.IO;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// File based logger class definition.
    /// </summary>
    public abstract class AFileLogger : ALogger
    {
        #region Fields

        /// <summary>
        /// Stores the full path info where to write the log.
        /// </summary>
        protected FileInfo mFullPath;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the logger fullpath.
        /// </summary>
        public string FullPath
        {
            get
            {
                return this.mFullPath.FullName;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AFileLogger"/> class.
        /// </summary>
        /// <param name="pPath">The file path</param>
        protected AFileLogger(string pPath)
        {
            this.mFullPath = new FileInfo( Path.Combine( Environment.CurrentDirectory, pPath ) );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on object disposing.
        /// </summary>
        protected override void OnDispose()
        {
            if ( File.Exists( this.mFullPath.FullName ) )
            {
                // Transfer the file to keep the last one somewhere else.
                string lLogDir = this.mFullPath.DirectoryName;
                string lLastLogName = this.mFullPath.Name.Replace( this.mFullPath.Extension, "Lastlog" + this.mFullPath.Extension );
                string lLastLogFullPath = Path.Combine( lLogDir, lLastLogName );
                File.Copy( this.mFullPath.FullName, lLastLogFullPath, true );

                // Clean the log file to assure the next run use a new one.
                File.Delete( this.mFullPath.FullName );
            }

            base.OnDispose();
        }

        #endregion Methods
    }
}
