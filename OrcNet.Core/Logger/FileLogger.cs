﻿using System.IO;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// File logger class definition
    /// </summary>
    public class FileLogger : AFileLogger
    {
        #region Properties

        /// <summary>
        /// Gets the logger type.
        /// </summary>
        public override LoggerType Type
        {
            get
            {
                return LoggerType.TXT;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FileLogger"/> class.
        /// </summary>
        /// <param name="pPath">The file path</param>
        public FileLogger(string pPath) :
        base( pPath )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods ILogger

        /// <summary>
        /// Logs a message log.
        /// </summary>
        /// <param name="pLog">The specified log</param>
        public override void Log(LogMessage pLog)
        {
            if ( File.Exists( this.mFullPath.FullName ) == false )
            {
                if ( Directory.Exists( this.mFullPath.DirectoryName ) == false )
                {
                    Directory.CreateDirectory( this.mFullPath.DirectoryName );
                }

                using ( File.Create( this.mFullPath.FullName ) )
                { }
            }

            int lLogType = (int)pLog.Type;
            if ( lLogType >= (int)this.mMinType &&
                 lLogType <= (int)this.mMaxType )
            {
                using ( StreamWriter lFile = new StreamWriter( this.mFullPath.FullName, true ) )
                {
                    // To string auto format.
                    lFile.WriteLine( "-------------------------------------------------------------------------------------------------------------------------" );
                    lFile.WriteLine( pLog.ToString() );
                }
            }
        }

        #endregion Methods ILogger

        #endregion Methods
    }
}
