﻿namespace OrcNet.Core.Logger
{
    /// <summary>
    /// Enumerate the available logger types.
    /// </summary>
    public enum LoggerType : int
    {
        /// <summary>
        /// No logger at all.
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Log to the console.
        /// </summary>
        CONSOLE = 0x01,

        /// <summary>
        /// Log to a text file.
        /// </summary>
        TXT     = 0x02,

        /// <summary>
        /// Log to a Xml file.
        /// </summary>
        XML     = 0x04,

        /// <summary>
        /// Log to a Html file.
        /// </summary>
        HTML    = 0x08
    }
}
