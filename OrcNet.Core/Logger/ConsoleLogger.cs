﻿using System;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// Base logger class definition
    /// </summary>
    public class ConsoleLogger : ALogger
    {
        #region Fields
        
        /// <summary>
        /// Stores once the console log colors.
        /// </summary>
        private static readonly ConsoleColor[] sLogColors;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the logger type.
        /// </summary>
        public override LoggerType Type
        {
            get
            {
                return LoggerType.CONSOLE;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="ConsoleLogger"/> class.
        /// </summary>
        static ConsoleLogger()
        {
            sLogColors = new ConsoleColor[ (int)LogType.COUNT ];
            sLogColors[ (int)LogType.INFO ]    = ConsoleColor.Gray;
            sLogColors[ (int)LogType.DEBUG ]   = ConsoleColor.Cyan;
            sLogColors[ (int)LogType.WARNING ] = ConsoleColor.Yellow;
            sLogColors[ (int)LogType.ERROR ]   = ConsoleColor.Red;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleLogger"/> class.
        /// </summary>
        public ConsoleLogger()
        {
            
        }

        #endregion Constructor

        #region Methods
        
        #region Methods ILogger

        /// <summary>
        /// Logs a message log.
        /// </summary>
        /// <param name="pLog">The specified log</param>
        public override void Log(LogMessage pLog)
        {
            int lLogType = (int)pLog.Type;
            if
                ( lLogType >= (int)this.mMinType && 
                  lLogType <= (int)this.mMaxType )
            {
                Console.ForegroundColor = sLogColors[ lLogType ];
                Console.WriteLine( pLog.ToString() );
                Console.ResetColor();
            }
        }

        #endregion Methods ILogger

        #endregion Methods
    }
}
