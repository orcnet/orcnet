﻿using System;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// Log message class definition
    /// </summary>
    public class LogMessage : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the log type.
        /// </summary>
        private LogType mType;

        /// <summary>
        /// Stores the message.
        /// </summary>
        private string  mMessage;

        /// <summary>
        /// Stores the stack trace if logging from an exception.
        /// </summary>
        private string  mStackTrace;

        /// <summary>
        /// Stores the log context if any.
        /// </summary>
        private object  mContext;

        /// <summary>
        /// Stores the severity attribute name.
        /// </summary>
        private const string cSeverityAttribute = "severity";

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the log type
        /// </summary>
        public LogType Type
        {
            get
            {
                return this.mType;
            }
            set
            {
                this.mType = value;
            }
        }

        /// <summary>
        /// Gets the log message.
        /// </summary>
        public string Message
        {
            get
            {
                return this.mMessage;
            }
        }

        /// <summary>
        /// Gets the log context if any.
        /// </summary>
        public object Context
        {
            get
            {
                return this.mContext;
            }
        }

        /// <summary>
        /// Gets the stack trace if log has been triggered from an exception.
        /// </summary>
        public string StackTrace
        {
            get
            {
                return this.mStackTrace;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="pMessage">The log message (Empty if default cst used)</param>
        /// <param name="pType">The log type (DEBUG by default)</param>
        /// <param name="pContext">The log context (NULL by default)</param>
        /// <param name="pStackTrace">The stack trace if log come from an exception.</param>
        public LogMessage(string pMessage, LogType pType = LogType.DEBUG, object pContext = null, string pStackTrace = null)
        {
            this.mType       = pType;
            this.mMessage    = pMessage;
            this.mContext    = pContext;
            this.mStackTrace = pStackTrace;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Convert the log into Xml element.
        /// </summary>
        /// <returns></returns>
        public XElement ToXml()
        {
            XElement lRoot = new XElement( "Type" );
            lRoot.SetAttributeValue( cSeverityAttribute, this.mType.ToString() );
            lRoot.Add( new XElement( "Message", this.mMessage ) );
            if ( this.mContext != null )
            {
                lRoot.Add( new XElement( "Source", this.mContext.ToString() ) );
            }
            if ( string.IsNullOrEmpty( this.mStackTrace ) == false )
            {
                lRoot.Add( new XElement( "Message", this.mStackTrace ) );
            }
            return lRoot;
        }
        
        /// <summary>
        /// Format the log message into a single string.
        /// </summary>
        /// <returns>The message formatted into a string.</returns>
        public override string ToString()
        {
            StringBuilder lBuilder = new StringBuilder( string.Format( "{0} : {1}", 
                                                                       this.mType,
                                                                       this.mMessage ) );
            if ( this.mContext != null )
            {
                lBuilder.AppendLine();
                lBuilder.Append( this.mContext.ToString() );
            }
            if ( string.IsNullOrEmpty( this.mStackTrace ) == false )
            {
                lBuilder.AppendLine();
                lBuilder.Append( this.mStackTrace );
            }

            return lBuilder.ToString();
        }

        /// <summary>
        /// Allow to pass a string as Log message.
        /// NOTE: The default Log type will be DEBUG.
        /// </summary>
        /// <param name="pMessage">The message to log</param>
        public static implicit operator LogMessage(string pMessage)
        {
            return new LogMessage( pMessage );
        }

        /// <summary>
        /// Releases unmanaged resources
        /// </summary>
        public void Dispose()
        {
            // Whatever it could be, the context can cause leaks if not null
            this.mContext    = null;
            this.mMessage    = null;
            this.mStackTrace = null;

            GC.SuppressFinalize( this );
        }

        #endregion Methods
    }
}
