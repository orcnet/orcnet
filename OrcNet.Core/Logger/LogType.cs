﻿namespace OrcNet.Core.Logger
{
    /// <summary>
    /// Enumerate the different log types.
    /// </summary>
    public enum LogType : int
    {
        /// <summary>
        /// Log an info message.
        /// </summary>
        INFO    = 0,

        /// <summary>
        /// Log a debug message
        /// </summary>
        DEBUG   = 1,

        /// <summary>
        /// Log a warning message
        /// </summary>
        WARNING = 2,

        /// <summary>
        /// Log an error message
        /// </summary>
        ERROR   = 3,

        /// <summary>
        /// Amount of log types.
        /// </summary>
        COUNT
    }
}
