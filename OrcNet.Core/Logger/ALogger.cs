﻿using System;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// Base abstract class definition.
    /// </summary>
    public abstract class ALogger : ILogger
    {
        #region Fields

        /// <summary>
        /// Stores the minimal log type this logger supports.
        /// </summary>
        protected LogType mMinType;

        /// <summary>
        /// Stores the maximal log type this logger supports.
        /// </summary>
        protected LogType mMaxType;

        #endregion Fields

        #region Properties

        #region Properties ILogger

        /// <summary>
        /// Gets the logger type.
        /// </summary>
        public abstract LoggerType Type
        {
            get;
        }

        /// <summary>
        /// Gets the minimal log type this logger supports.
        /// </summary>
        public LogType MinType
        {
            get
            {
                return this.mMinType;
            }
        }

        /// <summary>
        /// Gets the minimal log type this logger supports.
        /// </summary>
        public LogType MaxType
        {
            get
            {
                return this.mMaxType;
            }
        }

        #endregion Properties ILogger

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ALogger"/> class.
        /// </summary>
        protected ALogger()
        {
            // logger logs everything by default.
            this.mMinType = LogType.INFO;
            this.mMaxType = LogType.ERROR;
        }

        #endregion Constructor

        #region Methods

        #region Methods IDisposable

        /// <summary>
        /// Releases unmanaged resources
        /// </summary>
        public void Dispose()
        {
            // Process overriden disposal.
            this.OnDispose();

            // Optimize garbage collection by suppressing finalization
            // if dispose explicit call.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Delegate called on object disposing.
        /// </summary>
        protected virtual void OnDispose()
        {

        }

        #endregion Methods IDisposable

        #region Methods ILogger

        /// <summary>
        /// Logs a message log.
        /// </summary>
        /// <param name="pLog">The specified log</param>
        public abstract void Log(LogMessage pLog);

        #endregion Methods ILogger

        #endregion Methods
    }
}
