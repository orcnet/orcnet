﻿using System.IO;
using System.Text;
using System.Web.UI;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// HTML logger class definition.
    /// </summary>
    public class HTMLLogger : AFileLogger
    {
        #region Properties

        /// <summary>
        /// Gets the logger type.
        /// </summary>
        public override LoggerType Type
        {
            get
            {
                return LoggerType.HTML;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="HTMLLogger"/> class.
        /// </summary>
        /// <param name="pPath">The file path</param>
        public HTMLLogger(string pPath) :
        base( pPath )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods ILogger

        /// <summary>
        /// Logs a message log.
        /// </summary>
        /// <param name="pLog">The specified log</param>
        public override void Log(LogMessage pLog)
        {
            bool lMustAddStyle = false;
            if ( File.Exists( this.mFullPath.FullName ) == false )
            {
                if ( Directory.Exists( this.mFullPath.DirectoryName ) == false )
                {
                    Directory.CreateDirectory( this.mFullPath.DirectoryName );
                }

                using ( File.Create( this.mFullPath.FullName ) )
                { }

                // Create the header and style.
                lMustAddStyle = true;
            }

            int lLogType = (int)pLog.Type;
            if ( lLogType >= (int)this.mMinType &&
                 lLogType <= (int)this.mMaxType )
            {
                using ( StreamWriter lFile = new StreamWriter( this.mFullPath.FullName, true, Encoding.UTF8 ) )
                {
                    using ( HtmlTextWriter lWriter = new HtmlTextWriter( lFile ) )
                    {
                        if ( lMustAddStyle )
                        {
                            lWriter.RenderBeginTag( HtmlTextWriterTag.Head );
                            lWriter.RenderBeginTag( HtmlTextWriterTag.Style );
                            for( LogType lCurr = LogType.INFO; lCurr < LogType.COUNT; lCurr++ )
                            {
                                lWriter.WriteLine( string.Format( ".{0} {1}", lCurr.ToString().ToUpper(), "{" ) );
                                lWriter.WriteLine( string.Format( "color: {0};", this.GetLogHtmlColor( lCurr ) ) );
                                lWriter.WriteLine( "}" );
                            }
                            lWriter.RenderEndTag(); // End Style tag
                            lWriter.RenderEndTag(); // End Head tag
                        }
                        lWriter.WriteLine(); // New line.

                        lWriter.AddAttribute( HtmlTextWriterAttribute.Class, pLog.Type.ToString() );
                        lWriter.RenderBeginTag( HtmlTextWriterTag.Div );
                    
                        lWriter.RenderBeginTag( HtmlTextWriterTag.H1 );
                        lWriter.Write( "Message :" );
                        lWriter.RenderEndTag(); // End Message title

                        lWriter.RenderBeginTag( HtmlTextWriterTag.P );
                        lWriter.Write( pLog.Message );
                        lWriter.RenderEndTag(); // End Message paragraph

                        if ( pLog.Context != null )
                        {
                            lWriter.RenderBeginTag( HtmlTextWriterTag.H1 );
                            lWriter.Write( "Source :" );
                            lWriter.RenderEndTag(); // End Source title

                            lWriter.RenderBeginTag( HtmlTextWriterTag.P );
                            lWriter.Write( pLog.Context.ToString() );
                            lWriter.RenderEndTag(); // End Source paragraph
                        }

                        if ( string.IsNullOrEmpty( pLog.StackTrace ) == false )
                        {
                            lWriter.RenderBeginTag( HtmlTextWriterTag.H1 );
                            lWriter.Write( "StackTrace :" );
                            lWriter.RenderEndTag(); // End StackTrace title

                            lWriter.RenderBeginTag( HtmlTextWriterTag.P );
                            lWriter.Write( pLog.StackTrace );
                            lWriter.RenderEndTag(); // End StackTrace paragraph
                        }

                        lWriter.RenderEndTag(); // End Div
                    }
                }
            }
        }

        /// <summary>
        /// Gets the html log type color.
        /// </summary>
        /// <param name="pType">The log type.</param>
        /// <returns>The html color as string.</returns>
        private string GetLogHtmlColor(LogType pType)
        {
            switch ( pType )
            {
                case LogType.INFO:
                    return "green";
                case LogType.DEBUG:
                    return "blue";
                case LogType.WARNING:
                    return "yellow";
                case LogType.ERROR:
                    return "red";
                default:
                    return "black";
            }
        }

        #endregion Methods ILogger

        #endregion Methods
    }
}
