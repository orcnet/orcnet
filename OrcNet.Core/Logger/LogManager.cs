﻿using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace OrcNet.Core.Logger
{
    /// <summary>
    /// Log manager singleton class definition.
    /// Thread-safe.
    /// </summary>
    public sealed class LogManager : IDisposable
    {
        #region Delegates

        /// <summary>
        /// Logger delegate creator prototype definition
        /// </summary>
        /// <param name="pManager">The manager</param>
        public delegate void CreateLoggerDelegate(LogManager pManager);

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the log manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile LogManager sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the logger factory delegates by logger type.
        /// </summary>
        private static Dictionary<LoggerType, CreateLoggerDelegate> sLoggerFactories;

        /// <summary>
        /// Stores the flag indicating whether logs must be cached or not (to be displayed for instance)
        /// </summary>
        private bool mCacheLogs;

        /// <summary>
        /// Stores the maximum cache size for cached logs if enabled.
        /// </summary>
        private byte mMaxCacheSize;

        /// <summary>
        /// Stores the logs cache.
        /// </summary>
        private ConcurrentQueue<LogMessage> mLogsCache;

        /// <summary>
        /// Stores the amount of message that has been logged.
        /// </summary>
        private int mMessageCounter = 0;

        /// <summary>
        /// Stores the current loggers to use.
        /// </summary>
        private List<ILogger> mCurrentLoggers;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the Log manager handle.
        /// </summary>
        public static LogManager Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if ( sInstance == null )
                {
                    // Lock on
                    lock ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if ( sInstance == null )
                        {
                            sInstance = new LogManager();
                        }
                    }
                }

                return sInstance;
            }
        }

        /// <summary>
        /// Gets the cached log message(s)
        /// </summary>
        public IEnumerable<LogMessage> CachedLogs
        {
            get
            {
                return this.mLogsCache;
            }
        }

        /// <summary>
        /// Gets the amount of log that has been logged.
        /// </summary>
        public int MessageCount
        {
            get
            {
                return this.mMessageCounter;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="LogManager"/> class.
        /// </summary>
        static LogManager()
        {
            sLoggerFactories = new Dictionary<LoggerType, CreateLoggerDelegate>();
            sLoggerFactories.Add( LoggerType.CONSOLE, CreateConsoleLogger );
            sLoggerFactories.Add( LoggerType.TXT,     CreateTXTLogger     );
            sLoggerFactories.Add( LoggerType.XML,     CreateXMLLogger     );
            sLoggerFactories.Add( LoggerType.HTML,    CreateHTMLLogger    );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogManager"/> class.
        /// </summary>
        /// <param name="pCacheLogs">The flag indicating whether logs must be cached or not (to be displayed for instance)</param>
        /// <param name="pMaxCacheSize">The maximum cache size for cached logs if enabled.</param>
        private LogManager(bool pCacheLogs = false, byte pMaxCacheSize = 50)
        {
            this.mCacheLogs = pCacheLogs;
            this.mCurrentLoggers = new List<ILogger>();
            this.mLogsCache = new ConcurrentQueue<LogMessage>();
            this.mMaxCacheSize = pMaxCacheSize;

            // Initialize default.
            this.Initialize( LoggerType.NONE );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes the required loggers to use when logging a log message.
        /// E.g: Initialize( LoggerType.CONSOLE | LoggerType.XML ); only log in the console
        /// and an Xml file. Releases any previously created loggers different from those ones.
        /// </summary>
        /// <param name="pLoggers">The logger(s) flags</param>
        public void Initialize(LoggerType pLoggers)
        {
            // Lok for logger to create
            List<LoggerType> lToCreate = new List<LoggerType>();
            foreach ( LoggerType lType in Enum.GetValues( typeof( LoggerType ) ) )
            {
                if ( lType == LoggerType.NONE )
                {
                    continue;
                }

                if ( (pLoggers & lType) == lType )
                {
                    lToCreate.Add( lType );
                }
            }

            lock ( sSyncRoot )
            { 
                // Releases previous.
                foreach ( ILogger lLogger in this.mCurrentLoggers.ToArray() )
                {
                    if ( (pLoggers & lLogger.Type) == lLogger.Type )
                    {
                        // Keep it but remove it from the ToCreate list
                        lToCreate.Remove( lLogger.Type );
                    }
                    else
                    {
                        lLogger.Dispose();
                        this.mCurrentLoggers.Remove( lLogger );
                    }
                }

                // Create the one not existing yet.
                foreach ( LoggerType lType in lToCreate )
                {
                    sLoggerFactories[ lType ]( this );
                }
            }
        }

        /// <summary>
        /// Log a message straight.
        /// NOTE: If string implicit cast used, Log type is defaulted to DEBUG.
        /// </summary>
        /// <param name="pMessage"></param>
        public void Log(LogMessage pMessage)
        {
            if ( this.mCacheLogs )
            {
                if ( this.mLogsCache.Count == this.mMaxCacheSize )
                {
                    LogMessage lMessage;
                    this.mLogsCache.TryDequeue( out lMessage );
                }

                this.mLogsCache.Enqueue( pMessage );
            }

            lock ( sSyncRoot )
            {
                this.mMessageCounter++;

                foreach ( ILogger lLogger in this.mCurrentLoggers )
                {
                    lLogger.Log( pMessage );
                }
            }
        }

        /// <summary>
        /// Log a message with log type customization.
        /// </summary>
        /// <param name="pMessage">The log message</param>
        /// <param name="pType">The log type (DEBUG by default)</param>
        public void Log(LogMessage pMessage, LogType pType = LogType.DEBUG)
        {
            pMessage.Type = pType;
            this.Log( pMessage );
        }

        /// <summary>
        /// Log a message due to an exception.
        /// </summary>
        /// <param name="pException">The exception</param>
        /// <param name="pType">The log type (ERROR by default for exceptions)</param>
        public void Log(Exception pException, LogType pType = LogType.ERROR)
        {
            using ( LogMessage lNewLog = new LogMessage( pException.Message,
                                                         pType,
                                                         pException.Source,
                                                         pException.StackTrace ) )
            {
                this.Log( lNewLog );
            }
        }

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            foreach ( ILogger lLogger in this.mCurrentLoggers )
            {
                lLogger.Dispose();
            }
            this.mCurrentLoggers.Clear();
        }

        #endregion Methods IDisposable

        #region Methods Internal

        /// <summary>
        /// Creates a console logger
        /// </summary>
        /// <param name="pManager">The manager</param>
        private static void CreateConsoleLogger(LogManager pManager)
        {
            pManager.mCurrentLoggers.Add( new ConsoleLogger() );
        }

        /// <summary>
        /// Creates a TXT logger
        /// </summary>
        /// <param name="pManager">The manager</param>
        private static void CreateTXTLogger(LogManager pManager)
        {
            pManager.mCurrentLoggers.Add( new FileLogger( @"..\Resources\Support\OrcNet.txt" ) );
        }

        /// <summary>
        /// Creates a XML logger
        /// </summary>
        /// <param name="pManager">The manager</param>
        private static void CreateXMLLogger(LogManager pManager)
        {
            pManager.mCurrentLoggers.Add( new XMLLogger( @"..\Resources\Support\OrcNet.xml" ) );
        }

        /// <summary>
        /// Creates a HTML logger
        /// </summary>
        /// <param name="pManager">The manager</param>
        private static void CreateHTMLLogger(LogManager pManager)
        {
            pManager.mCurrentLoggers.Add( new HTMLLogger( @"..\Resources\Support\OrcNet.html" ) );
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
