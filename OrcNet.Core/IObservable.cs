﻿using System;

namespace OrcNet.Core
{
    /// <summary>
    /// Base observable interface definition.
    /// NOTE: Disposable as the observable has to release Observer(s)
    /// if not manually removed before its death.
    /// </summary>
    public interface IObservable : IDisposable
    {
        #region Methods

        /// <summary>
        /// Add a new observer to this observable for being 
        /// notified it had property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        void AddPropertyObserver(IObserver pObserver);

        /// <summary>
        /// Remove an observer to this observable to stop
        /// notifying it on property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        void RemovePropertyObserver(IObserver pObserver);
        
        #endregion Methods
    }
}
