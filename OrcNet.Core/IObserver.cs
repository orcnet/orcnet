﻿namespace OrcNet.Core
{
    /// <summary>
    /// Base observer interface definition.
    /// </summary>
    public interface IObserver
    {
        #region Methods

        /// <summary>
        /// Delegate called on observed object property changes.
        /// </summary>
        /// <param name="pSender">The observed object</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        void OnObservablePropertyChanged(IObservable pSender, PropertyChangedEventArgs pEventArgs);

        #endregion Methods
    }
}
