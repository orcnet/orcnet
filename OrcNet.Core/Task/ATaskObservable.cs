﻿using System.Collections.Generic;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Base abstract task observable class definition.
    /// </summary>
    public abstract class ATaskObservable : AObservable, ITaskObservable
    {
        #region Fields

        /// <summary>
        /// Stores the set of task observer(s) to notify on task changes.
        /// </summary>
        private List<ITaskObserver> mTaskObservers;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ATaskObservable"/> class.
        /// </summary>
        protected ATaskObservable()
        {
            this.mTaskObservers = new List<ITaskObserver>();
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Notifies observer(s) that the observable task changed.
        /// </summary>
        /// <param name="pState">The new task state.</param>
        /// <param name="pIsDone">The flag indicating whether the task is done or not.</param>
        protected void NotifyStateChanged(TaskState pState, bool pIsDone = false)
        {
            foreach
                ( ITaskObserver lObserver in this.mTaskObservers )
            {
                lObserver.OnTaskStateChanged( this, pState, pIsDone );
            }
        }

        /// <summary>
        /// Notifies observer(s) that the observable task completion date changed.
        /// </summary>
        /// <param name="pDate">The new completion date.</param>
        protected void NotifyCompletionDateChanged(uint pDate)
        {
            foreach
                ( ITaskObserver lObserver in this.mTaskObservers )
            {
                lObserver.OnTaskCompletionDateChanged( this, pDate );
            }
        }

        #endregion Methods Internal

        #region Methods ITaskObservable

        /// <summary>
        /// Add a new observer to this observable for being 
        /// notified about task changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        public void AddTaskObserver(ITaskObserver pObserver)
        {
            if
                ( this.mTaskObservers.Contains( pObserver ) == false )
            {
                this.mTaskObservers.Add( pObserver );
            }
        }
        
        /// <summary>
        /// Remove an observer to this observable to stop
        /// notifying it on task changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        public void RemoveTaskObserver(ITaskObserver pObserver)
        {
            if
                ( this.mTaskObservers.Contains( pObserver ) )
            {
                this.mTaskObservers.Remove( pObserver );
            }
        }
        
        #endregion Methods ITaskObservable

        #region Methods IDisposable

        /// <summary>
        /// Delegate called on dispose.
        /// </summary>
        protected override void OnDispose()
        {
            // Free task observer(s) refs to avoid leaks.
            this.mTaskObservers.Clear();

            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
