﻿using System;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Task dependency exception thrown if a task is removed
    /// from its set but still have dependencies to some other tasks.
    /// </summary>
    public class TaskDependencyException : Exception
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskDependencyException"/> class.
        /// </summary>
        /// <param name="pMessage">The message to display to the user.</param>
        public TaskDependencyException(string pMessage) :
        base( pMessage )
        {

        }

        #endregion Constructor
    }
}
