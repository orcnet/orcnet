﻿using OrcNet.Core.Resource;
using System;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Base scheduler interface in charge of sorting and executing tasks.
    /// </summary>
    public interface IScheduler : IResourceCreatable, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether GPU prefetching is supported or not.
        /// </summary>
        bool SupportsGPUPrefetch
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether CPU prefetching is supported or not.
        /// </summary>
        bool SupportsCPUPrefetch
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Schedule a new task or task set whose deadline is not immediate but ONLY if prefetching
        /// of the given task(s) is supported.
        /// </summary>
        /// <param name="pTask">A simple task or set of tasks whose deadline is not immediate</param>
        /// <returns>True if scheduling succeeded, false otherwise.</returns>
        bool Schedule(ITask pTask);

        /// <summary>
        /// Schedule a task or task set reexecution
        /// </summary>
        /// <param name="pTask">A simple task or set of tasks</param>
        /// <param name="pState">The task state.</param>
        /// <param name="pDeadline">The frame number before which the task must be re executed.</param>
        /// <returns>True if scheduling succeeded, false otherwise.</returns>
        bool ReSchedule(ITask pTask, TaskState pState, uint pDeadline);

        /// <summary>
        /// Executes the supplied task or task set until all immediate deadline 
        /// are completed.
        /// </summary>
        /// <param name="pTask">A simple task or set of tasks</param>
        void Execute(ITask pTask);

        /// <summary>
        /// Cleans up the given task's parent and children dependencies.
        /// </summary>
        /// <param name="pTask">The task to clean up.</param>
        void Cleanup(ITask pTask);

        #endregion Methods
    }
}
