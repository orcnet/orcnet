﻿using OrcNet.Core.Resource;
using System;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Base task factory interface definition.
    /// </summary>
    public interface ITaskFactory : IResourceCreatable, IMemoryProfilable, IDisposable
    {
        #region Methods

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        ITask Create(IComparable pContext);

        #endregion Methods
    }
}
