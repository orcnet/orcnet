﻿using System;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Base task interface definition.
    /// </summary>
    public interface ITask : ITaskObservable
    {
        #region Properties

        /// <summary>
        /// Gets an unique identifier for that task.
        /// </summary>
        int Id
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the task is done or not.
        /// </summary>
        bool IsDone
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the Task is a GPU task or not.
        /// </summary>
        bool IsGPUTask
        {
            get;
        }

        /// <summary>
        /// Gets the task's dead line.
        /// </summary>
        uint DeadLine
        {
            get;
        }

        /// <summary>
        /// Gets the task's completion date.
        /// </summary>
        uint CompletionDate
        {
            get;
        }

        /// <summary>
        /// Gets the task's expected duration.
        /// </summary>
        double ExpectedDuration
        {
            get;
        }

        /// <summary>
        /// Gets the task's complexity. This number is used to estimate the
        /// duration d of this task as d= k * complexity, where k is estimated based on
        /// the actual duration and complexity of previous tasks of the same type
        /// </summary>
        int Complexity
        {
            get;
        }

        /// <summary>
        /// Gets the task contextual object that could be shared between tasks.
        /// For GPU tasks, it allows to sort by context to avoid switches.
        /// </summary>
        IComparable Context
        {
            get;
        }

        #endregion Properties

        #region Methods
        
        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        bool Execute();
        
        #endregion Methods
    }
}
