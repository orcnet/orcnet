﻿using OrcNet.Core.Logger;
using System.Collections.Generic;
using System.Linq;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Task set containing sub tasks and their dependencies if any.
    /// </summary>
    public class TaskSet : ATask, ITaskContainer, ITaskObserver
    {
        #region Fields

        /// <summary>
        /// Stores the overall set of sub-tasks of this task set.
        /// </summary>
        private HashSet<ITask> mTasks;

        /// <summary>
        /// Stores the set of tasks with no parents.
        /// </summary>
        private HashSet<ITask> mOrphanTasks;

        /// <summary>
        /// Stores the set of tasks with no children.
        /// </summary>
        private HashSet<ITask> mLeafTasks;

        /// <summary>
        /// Stores the set of predecessors by task.
        /// </summary>
        private Dictionary<ITask, HashSet<ITask>> mParentByTask;

        /// <summary>
        /// Stores the set of successors by task.
        /// </summary>
        private Dictionary<ITask, HashSet<ITask>> mChildrenByTask;

        #endregion Fields

        #region Properties

        #region Properties ITaskContainer

        /// <summary>
        /// Gets the flag indicating whether the task container has tasks or not.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.mTasks.Count == 0;
            }
        }

        /// <summary>
        /// Gets all the contained tasks.
        /// </summary>
        public IEnumerable<ITask> Tasks
        {
            get
            {
                return this.mTasks;
            }
        }

        /// <summary>
        /// Gets the set of parent tasks, that is, the ones that have no parents.
        /// </summary>
        public IEnumerable<ITask> ParentTasks
        {
            get
            {
                return this.mOrphanTasks;
            }
        }

        /// <summary>
        /// Gets the set of leaf tasks, that is, the ones that have no children.
        /// </summary>
        public IEnumerable<ITask> LeafTasks
        {
            get
            {
                return this.mLeafTasks;
            }
        }

        #endregion Properties ITaskContainer

        #region Properties ATask

        /// <summary>
        /// Gets the task's complexity. This number is used to estimate the
        /// duration d of this task as d= k * complexity, where k is estimated based on
        /// the actual duration and complexity of previous tasks of the same type
        /// </summary>
        public override int Complexity
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// Gets the task's predecessors completion date.
        /// </summary>
        public override uint PredecessorsCompletionDate
        {
            get
            {
                return base.PredecessorsCompletionDate;
            }

            set
            {
                base.PredecessorsCompletionDate = value;

                // Transfers the new predecessors completion date to all
                // the set Orphan tasks.
                foreach
                    ( ATask lOrphanTask in this.mOrphanTasks )
                {
                    lOrphanTask.PredecessorsCompletionDate = value;
                }
            }
        }

        #endregion Properties ATask
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskSet"/> class.
        /// </summary>
        public TaskSet() :
        base( 0, false )
        {
            this.mTasks = new HashSet<ITask>();
            this.mOrphanTasks = new HashSet<ITask>();
            this.mLeafTasks   = new HashSet<ITask>();
            this.mParentByTask   = new Dictionary<ITask, HashSet<ITask>>();
            this.mChildrenByTask = new Dictionary<ITask, HashSet<ITask>>();
        }

        #endregion Constructor

        #region Methods
        
        #region Methods ITaskContainer

        /// <summary>
        /// Adds a new task to the container.
        /// </summary>
        /// <param name="pNewTask">The new task</param>
        public void AddTask(ITask pNewTask)
        {
            // Add only none task set or task set that have sub tasks to execute.
            TaskSet lTest = pNewTask as TaskSet;
            if
                ( lTest == null ||
                  lTest.IsEmpty == false )
            {
                // If not in the set yet
                if
                    ( this.mTasks.Contains( pNewTask ) == false )
                {
                    // Listen task state and changes.
                    pNewTask.AddTaskObserver( this );

                    // Store it.
                    this.mTasks.Add( pNewTask );
                    this.mOrphanTasks.Add( pNewTask );
                    this.mLeafTasks.Add( pNewTask );
                }
            }
        }

        /// <summary>
        /// Removes a task from the container.
        /// </summary>
        /// <param name="pTask">The task to remove</param>
        /// <exception cref="TaskDependencyException">Thrown if the task being removed still have dependencies to parent(s) or children.</exception>
        public void RemoveTask(ITask pTask)
        {
            // If not in the set yet
            if
                ( this.mTasks.Contains( pTask ) )
            {
                // Stop listening task state and changes.
                pTask.RemoveTaskObserver( this );

                // Store it.
                this.mTasks.Remove( pTask );
                this.mOrphanTasks.Add( pTask );
                this.mLeafTasks.Add( pTask );

                HashSet<ITask> lParents = null;
                if
                    ( this.mParentByTask.TryGetValue( pTask, out lParents ) )
                {
                    throw new TaskDependencyException(" TaskSet.RemoveTask The removed task still have link to parent(s) but is being removed !!!" );
                }

                HashSet<ITask> lChildren = null;
                if
                    ( this.mChildrenByTask.TryGetValue( pTask, out lChildren ) )
                {
                    throw new TaskDependencyException(" TaskSet.RemoveTask The removed task still have link to children but is being removed !!!" );
                }
            }
        }

        /// <summary>
        /// Retrieves the parent tasks of a supplied task to know what have to be done
        /// before that task be done.
        /// </summary>
        /// <param name="pTask">The task for which parents are needed.</param>
        /// <returns>The set of parents tasks. Empty if no parents</returns>
        public IEnumerable<ITask> GetParentsOf(ITask pTask)
        {
            HashSet<ITask> lParents = null;
            if ( this.mParentByTask.TryGetValue( pTask, out lParents ) )
            {
                return lParents;
            }

            // Just prevent the user from crashing if directly iterating on that method.
            return new List<ITask>();
        }

        /// <summary>
        /// Retrieves the children tasks of a supplied task to know what tasks are waiting
        /// for that task to be done.
        /// </summary>
        /// <param name="pTask">The parent task children are needed.</param>
        /// <returns>The set of children tasks. Empty if leaf task.</returns>
        public IEnumerable<ITask> GetChildrenOf(ITask pTask)
        {
            HashSet<ITask> lChildren = null;
            if ( this.mChildrenByTask.TryGetValue( pTask, out lChildren ) )
            {
                return lChildren;
            }

            // Just prevent the user from crashing if directly iterating on that method.
            return new List<ITask>();
        }

        /// <summary>
        /// Creates a dependency between two tasks.
        /// </summary>
        /// <param name="pFrom">The task the dependency starts from</param>
        /// <param name="pTo">The task the dependency ends to</param>
        public void CreateDependency(ITask pFrom, ITask pTo)
        {
            // Checks if noth are task of that set first.
            if ( this.mTasks.Contains( pFrom ) &&
                 this.mTasks.Contains( pTo ) )
            {
                // Update hierarchy caches.
                HashSet<ITask> lParents = null;
                if ( this.mParentByTask.TryGetValue( pFrom, out lParents ))
                {
                    // Add pTo as parent of pFrom.
                    lParents.Add( pTo );
                }
                else
                {
                    this.mParentByTask[ pFrom ] = new HashSet<ITask>() { pTo };
                }

                HashSet<ITask> lChildren = null;
                if ( this.mChildrenByTask.TryGetValue( pTo, out lChildren ) )
                {
                    // And pFrom as children of pTo
                    lChildren.Add( pFrom );
                }
                else
                {
                    this.mChildrenByTask[ pTo ] = new HashSet<ITask>() { pFrom };
                }

                this.mOrphanTasks.Remove( pFrom );
                this.mLeafTasks.Remove( pTo );
            }

            // Else do not care.
        }

        /// <summary>
        /// Destroys a dependency between two tasks.
        /// </summary>
        /// <param name="pFrom">The task the dependency starts from</param>
        /// <param name="pTo">The task the dependency ends to</param>
        public void DestroyDependency(ITask pFrom, ITask pTo)
        {
            // Checks if noth are task of that set first.
            if ( this.mTasks.Contains( pFrom ) &&
                 this.mTasks.Contains( pTo ) )
            {
                // Update hierarchy caches.
                HashSet<ITask> lParents = null;
                if ( this.mParentByTask.TryGetValue( pFrom, out lParents ))
                {
                    // Remove pTo as parent of pFrom.
                    lParents.Remove( pTo );

                    if ( lParents.Count == 0 )
                    {
                        // No parent anymore, so becomes Orphan
                        this.mParentByTask.Remove( pFrom );
                        this.mOrphanTasks.Add( pFrom );
                    }
                }

                HashSet<ITask> lChildren = null;
                if ( this.mChildrenByTask.TryGetValue( pTo, out lChildren ) )
                {
                    // Remove pFrom as children of pTo
                    lChildren.Remove( pFrom );

                    if ( lChildren.Count == 0 )
                    {
                        // No more children, so becomes a leaf.
                        this.mChildrenByTask.Remove( pTo );
                        this.mLeafTasks.Add( pTo );
                    }
                }
            }

            // Else do not care.
        }

        /// <summary>
        /// Removes all dependencies of the given sub task but
        /// return the deleted task's dependencies.
        /// </summary>
        /// <param name="pTask">The sub task of this set the dependencies must be removed.</param>
        /// <param name="pDeletedDependencies">The deleted task's dependencies.</param>
        public void RemoveAndGetDependencies(ITask pTask, ref HashSet<ITask> pDeletedDependencies)
        {
            pDeletedDependencies.Clear(); // TO DO : check if must clear or let previous in ... in.

            HashSet<ITask> lParents = null;
            if ( this.mParentByTask.TryGetValue( pTask, out lParents ))
            {
                foreach ( ITask lParent in lParents )
                {
                    pDeletedDependencies.Add( lParent );

                    HashSet<ITask> lChildren = null;
                    if ( this.mChildrenByTask.TryGetValue( lParent, out lChildren ) )
                    {
                        bool lRemoved = lChildren.Remove( pTask );
                        if ( lRemoved == false )
                        {
                            // Error.
                            LogManager.Instance.Log( "No inverse dependencies for the task being removed!!!", LogType.ERROR );
                        }

                        // If no more children, it must be added to leaf tasks.
                        if ( lChildren.Count == 0 )
                        {
                            this.mChildrenByTask.Remove( lParent );
                            this.mLeafTasks.Add( lParent );
                        }
                    }
                }

                // Remove the set of sub tasks from the set.
                this.mParentByTask.Remove( pTask );

                // No more parent so make it orphan.
                this.mOrphanTasks.Add( pTask );
            }
        }

        /// <summary>
        /// Removes all created dependencies between tasks of this set.
        /// </summary>
        public void RemoveAllDependencies()
        {
            // Remove parent and children dependencies and
            // starts fresh with all as Orphans and leafs as no more dependencies.
            this.mOrphanTasks = new HashSet<ITask>( this.mTasks );
            this.mLeafTasks   = new HashSet<ITask>( this.mTasks );
            this.mParentByTask.Clear();
            this.mChildrenByTask.Clear();
        }

        #endregion Methods ITaskContainer

        #region Methods ATask

        /// <summary>
        /// Prepares this task before its execution. This method is called when the
        /// task is scheduled to be executed. It can perform work that cannot be
        /// executed during the task execution itself, such as scheduling new tasks
        /// for execution (indeed, once a set of tasks has been scheduled for
        /// execution, this set cannot be modified, i.e. a task cannot schedule the
        /// execution of a task that was not previously in this set).
        /// </summary>
        /// <param name="pInitialized">The tasks already initialized to avoid initializing the same several times</param>
        public override void Prepare(ref HashSet<ITask> pInitialized)
        {
            // Prepare the set's sub tasks if only if not Initialized yet.
            if ( pInitialized.Contains( this ) == false )
            {
                pInitialized.Add( this );

                // Initialize all its sub tasks as well.
                foreach ( ATask lSubTask in this.mTasks )
                {
                    lSubTask.Prepare( ref pInitialized );
                }
            }
        }

        /// <summary>
        /// Task's pre execution.
        /// </summary>
        public override void PreExecute()
        {
            // Nothing to do.
        }

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        public override bool Execute()
        {
            return true;
        }

        /// <summary>
        /// Task's post execution.
        /// </summary>
        public override void PostExecution()
        {
            // Nothing to do.
        }

        /// <summary>
        /// Update the task state.
        /// </summary>
        /// <param name="pIsDone">Whether the task is done or not</param>
        /// <param name="pCompletionDate">The completion date if the task is done.</param>
        /// <param name="pState">The new task state.</param>
        public override void UpdateState(bool pIsDone, uint pCompletionDate, TaskState pState = TaskState.DATA_NEEDED)
        {
            base.UpdateState( pIsDone, pCompletionDate, pState );

            if ( pIsDone == false )
            {
                // If not done due to the fact that this task has to be re executed
                // all its sub tasks must be re executed as well.
                // On the other hand, if it is simply a request for that task's data
                // result, only the Leaf tasks of that task must be re executed.
                IEnumerable<ITask> lToReexecuteTasks = null;
                if
                    ( pState == TaskState.DEPENDENCY_CHANGED )
                {
                    lToReexecuteTasks = this.mTasks;
                }
                else
                {
                    lToReexecuteTasks = this.mLeafTasks;
                }

                // Update each task to re execute state.
                foreach
                    ( ATask lToReexecuteTask in lToReexecuteTasks )
                {
                    lToReexecuteTask.UpdateState( pIsDone, pCompletionDate, pState );
                }
            }
        }

        #endregion Methods ATask

        #region Methods ITaskObserver

        /// <summary>
        /// Delegate called on observed task state changes.
        /// </summary>
        /// <param name="pSender">The observed task</param>
        /// <param name="pState">The new task state.</param>
        /// <param name="pIsDone">The flag indicating whether the task is done or not.</param>
        public void OnTaskStateChanged(ITaskObservable pSender, TaskState pState, bool pIsDone)
        {
            ATask lSender = pSender as ATask;
            if
                ( this.mTasks.Contains( lSender ) )
            {
                if
                    ( pIsDone == false )
                {
                    if
                        ( pState != TaskState.DATA_NEEDED )
                    {
                        this.UpdateState( false, 0, pState );

                        // As the result of the task is needed again, its children that depends on their
                        // parent result must be re executed as well so inform them.
                        foreach
                            ( ATask lChild in this.GetChildrenOf( lSender ) )
                        {
                            lChild.UpdateState( false, 0, TaskState.DEPENDENCY_CHANGED );
                        }
                    }
                }
                else
                {
                    // Updates the parent(s) completion date of that child task
                    this.OnTaskCompletionDateChanged( lSender, lSender.CompletionDate );

                    // Check if still any tasks not done 
                    // to skip the below completion notification.
                    if
                        ( this.mTasks.Any( pElt => pElt.IsDone == false ) )
                    {
                        return;
                    }

                    // Inform the task set and all its sub tasks are completed.
                    this.UpdateState( true, this.CompletionDate );
                }
            }
        }

        /// <summary>
        /// Delegate called on observed task completion date changes.
        /// </summary>
        /// <param name="pSender">The observed task</param>
        /// <param name="pDate">The new completion date.</param>
        public void OnTaskCompletionDateChanged(ITaskObservable pSender, uint pDate)
        {
            ITask lSender = pSender as ITask;
            bool lAnyChildren   = false;
            this.CompletionDate = System.Math.Max( this.CompletionDate, pDate );
            foreach
                ( ATask lTask in this.GetChildrenOf( lSender ) )
            {
                lTask.PredecessorsCompletionDate = pDate;
                lAnyChildren = true;
            }

            // If no children, inform the listeners of that task completion
            // that its completion date has changed.
            if
                ( lAnyChildren == false )
            {
                this.NotifyCompletionDateChanged( pDate );
            }
        }

        /// <summary>
        /// Delegate called on observed object property changes.
        /// </summary>
        /// <param name="pSender">The observed object</param>
        /// <param name="pEventArgs">The property changed event arguments.</param>
        public void OnObservablePropertyChanged(IObservable pSender, PropertyChangedEventArgs pEventArgs)
        {
            // Nothing to do.
        }

        #endregion Methods ITaskObserver

        #region Methods IDisposable

        /// <summary>
        /// Delegate called on dispose.
        /// </summary>
        protected override void OnDispose()
        {
            foreach ( ITask lTask in this.mTasks )
            {
                lTask.RemoveTaskObserver( this );
            }
            this.mTasks.Clear();
            this.mLeafTasks.Clear();
            this.mOrphanTasks.Clear();
            this.mParentByTask.Clear();
            this.mChildrenByTask.Clear();

            base.OnDispose();
        }
        
        #endregion Methods IDisposable

        #endregion Methods
    }
}
