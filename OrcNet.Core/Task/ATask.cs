﻿using System;
using System.Collections.Generic;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Task class definition.
    /// </summary>
    public abstract class ATask : ATaskObservable, ITask
    {
        #region Fields

        /// <summary>
        /// Stores the static task counter assuring a unique and incrmentale Identifier to each task.
        /// </summary>
        private static int sTaskCounter = 0;

        /// <summary>
        /// Stores the synchronization object.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the unique task's identifier.
        /// </summary>
        private int     mId;

        /// <summary>
        /// Stores the task context.
        /// </summary>
        private IComparable mContext;

        /// <summary>
        /// Stores the task's expected duration.
        /// </summary>
        private double  mExpectedDuration;

        /// <summary>
        /// Stores the flag indicating whether the task is done or not.
        /// </summary>
        private bool    mIsDone;

        /// <summary>
        /// Stores the flag indicating whether the Task is a GPU task or not.
        /// </summary>
        private bool    mIsGPUTask;

        /// <summary>
        /// Stores the task's deadline.
        /// </summary>
        private uint    mDeadLine;

        /// <summary>
        /// Stores the task's completion date.
        /// </summary>
        private uint    mCompletionDate;

        /// <summary>
        /// Stores the task's predecessors completion date.
        /// </summary>
        private uint    mPredecessorsCompletionDate;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets an unique identifier for that task.
        /// </summary>
        public int Id
        {
            get
            {
                return this.mId;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the task is done or not.
        /// </summary>
        public bool IsDone
        {
            get
            {
                return this.mIsDone;
            }
            protected set
            {
                this.mIsDone = value;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Task is a GPU task or not.
        /// </summary>
        public bool IsGPUTask
        {
            get
            {
                return this.mIsGPUTask;
            }
        }

        /// <summary>
        /// Gets the task's dead line.
        /// </summary>
        public uint DeadLine
        {
            get
            {
                return this.mDeadLine;
            }
            internal set
            {
                this.mDeadLine = System.Math.Min( this.mDeadLine, value );
            }
        }

        /// <summary>
        /// Gets the task's expected duration.
        /// </summary>
        public double ExpectedDuration
        {
            get
            {
                return this.mExpectedDuration;
            }
            protected set
            {
                this.mExpectedDuration = value;
            }
        }

        /// <summary>
        /// Gets the task's completion date.
        /// </summary>
        public uint CompletionDate
        {
            get
            {
                return this.mCompletionDate;
            }
            protected set
            {
                this.mCompletionDate = value;
            }
        }

        /// <summary>
        /// Gets the task's predecessors completion date.
        /// </summary>
        public virtual uint PredecessorsCompletionDate
        {
            get
            {
                return this.mPredecessorsCompletionDate;
            }
            set
            {
                this.mPredecessorsCompletionDate = System.Math.Max( this.mPredecessorsCompletionDate, value );
            }
        }

        /// <summary>
        /// Gets the task contextual object that could be shared between tasks.
        /// For GPU tasks, it allows to sort by context to avoid switches.
        /// </summary>
        public virtual IComparable Context
        {
            get
            {
                return this.mContext;
            }
            protected set
            {
                this.mContext = value;
            }
        }

        /// <summary>
        /// Gets the task's complexity. This number is used to estimate the
        /// duration d of this task as d= k * complexity, where k is estimated based on
        /// the actual duration and complexity of previous tasks of the same type
        /// </summary>
        public virtual int Complexity
        {
            get
            {
                return 1;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ATask"/> class.
        /// </summary>
        /// <param name="pDeadLine">The task's deadline</param>
        /// <param name="pIsGPUTask">The flag indicating whether this task must be processed on the GPU or not.</param>
        protected ATask(uint pDeadLine, bool pIsGPUTask = false)
        {
            this.mId        = this.NextId();
            this.mContext   = null;
            this.mIsDone    = false;
            this.mDeadLine  = pDeadLine;
            this.mIsGPUTask = pIsGPUTask;
            this.mCompletionDate = 0;
            this.mExpectedDuration = -1.0;
            this.mPredecessorsCompletionDate = 1;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Provide the next unique task identifier.
        /// </summary>
        /// <returns>The next id.</returns>
        private int NextId()
        {
            return sTaskCounter++;
        }

        /// <summary>
        /// Prepares this task before its execution. This method is called when the
        /// task is scheduled to be executed. It can perform work that cannot be
        /// executed during the task execution itself, such as scheduling new tasks
        /// for execution (indeed, once a set of tasks has been scheduled for
        /// execution, this set cannot be modified, i.e. a task cannot schedule the
        /// execution of a task that was not previously in this set).
        /// </summary>
        /// <param name="pInitialized">The tasks already initialized to avoid initializing the same several times</param>
        public virtual void Prepare(ref HashSet<ITask> pInitialized)
        {
            // Nothing to do.
        }

        /// <summary>
        /// Task's pre execution.
        /// </summary>
        public virtual void PreExecute()
        {
            // Nothing to do.
        }

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <returns>True if the task's result is different from the previous task result.</returns>
        public abstract bool Execute();

        /// <summary>
        /// Task's post execution.
        /// </summary>
        public virtual void PostExecution()
        {
            // Nothing to do.
        }

        /// <summary>
        /// Update the task state.
        /// </summary>
        /// <param name="pIsDone">Whether the task is done or not</param>
        /// <param name="pCompletionDate">The completion date if the task is done.</param>
        /// <param name="pState">The new task state.</param>
        public virtual void UpdateState(bool pIsDone, uint pCompletionDate, TaskState pState = TaskState.DATA_NEEDED)
        {
            if ( this.IsDone != pIsDone )
            {
                this.IsDone = pIsDone;
                if ( pIsDone || 
                     pState != TaskState.DEPENDENCY_CHANGED )
                {
                    this.CompletionDate = pCompletionDate;
                }

                this.NotifyStateChanged( pState, pIsDone );
            }
        }

        #region Methods IDisposable

        /// <summary>
        /// Delegate called on dispose.
        /// </summary>
        protected override void OnDispose()
        {
            // Releases the context ref.
            this.mContext = null;

            base.OnDispose();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
