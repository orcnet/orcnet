﻿using System;
using System.Collections.Generic;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Base task container interface definition.
    /// </summary>
    public interface ITaskContainer : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the task container has tasks or not.
        /// </summary>
        bool IsEmpty
        {
            get;
        }

        /// <summary>
        /// Gets all the contained tasks.
        /// </summary>
        IEnumerable<ITask> Tasks
        {
            get;
        }

        /// <summary>
        /// Gets the set of parent tasks, that is, the ones that have no parents.
        /// </summary>
        IEnumerable<ITask> ParentTasks
        {
            get;
        }

        /// <summary>
        /// Gets the set of leaf tasks, that is, the ones that have no children.
        /// </summary>
        IEnumerable<ITask> LeafTasks
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new task to the container.
        /// </summary>
        /// <param name="pNewTask">The new task</param>
        void AddTask(ITask pNewTask);

        /// <summary>
        /// Removes a task from the container.
        /// </summary>
        /// <param name="pTask">The task to remove</param>
        void RemoveTask(ITask pTask);

        /// <summary>
        /// Retrieves the parent tasks of a supplied task to know what have to be done
        /// before that task be done.
        /// </summary>
        /// <param name="pTask">The task for which parents are needed.</param>
        /// <returns>The set of parents tasks. Empty if no parents</returns>
        IEnumerable<ITask> GetParentsOf(ITask pTask);

        /// <summary>
        /// Retrieves the children tasks of a supplied task to know what tasks are waiting
        /// for that task to be done.
        /// </summary>
        /// <param name="pTask">The parent task children are needed.</param>
        /// <returns>The set of children tasks. Empty if leaf task.</returns>
        IEnumerable<ITask> GetChildrenOf(ITask pTask);

        /// <summary>
        /// Creates a dependency between two tasks.
        /// </summary>
        /// <param name="pFrom">The task the dependency starts from</param>
        /// <param name="pTo">The task the dependency ends to</param>
        void CreateDependency(ITask pFrom, ITask pTo);

        /// <summary>
        /// Destroys a dependency between two tasks.
        /// </summary>
        /// <param name="pFrom">The task the dependency starts from</param>
        /// <param name="pTo">The task the dependency ends to</param>
        void DestroyDependency(ITask pFrom, ITask pTo);

        /// <summary>
        /// Removes all created dependencies between tasks of this set.
        /// </summary>
        void RemoveAllDependencies();

        #endregion Methods
    }
}
