﻿namespace OrcNet.Core.Task
{
    /// <summary>
    /// Enumerate the different task state(s)
    /// </summary>
    public enum TaskState
    {
        /// <summary>
        /// The data used by the task and produced by a predecessor task has changed
        /// </summary>
        DEPENDENCY_CHANGED = 0,

        /// <summary>
        /// The data used by this task but not produced by another task has changed
        /// </summary>
        DATA_CHANGED,

        /// <summary>
        /// The result of this task is needed again by a successor task of this task
        /// </summary>
        DATA_NEEDED,

        /// <summary>
        /// The amount of state(s)
        /// </summary>
        COUNT
    }
}
