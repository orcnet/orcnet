﻿using System;
using System.Collections.Generic;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Base task profiler interface definition.
    /// </summary>
    public interface ITaskProfiler : IService
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the profiler is actually
        /// profiling some task type(s)
        /// </summary>
        bool IsProfiling
        {
            get;
        }

        /// <summary>
        /// Gets the set of task whose execution time must be monitored.
        /// </summary>
        IEnumerable<Type> MonitoredTasks
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new task type to profile.
        /// </summary>
        /// <param name="pToProfile">The task type to profile</param>
        void ProfileTask(Type pToProfile);

        /// <summary>
        /// Resets the profiler statistics
        /// </summary>
        void ResetStatistics();

        /// <summary>
        /// Updates the profiler global statistics by task type.
        /// </summary>
        /// <param name="pTask">The new task completed</param>
        /// <param name="pNewDuration">The new task duration to take into account.</param>
        void UpdateGlobalStatistics(ITask pTask, double pNewDuration);

        /// <summary>
        /// Updates the profiler per frame statistics by task type.
        /// </summary>
        /// <param name="pTask">The new task completed</param>
        /// <param name="pNewDuration">The new task duration to take into account.</param>
        void UpdateFrameStatistics(ITask pTask, double pNewDuration);

        /// <summary>
        /// Prefetches some statistic info. If prefetched enough
        /// previous will be written to file and prefetch new info.
        /// </summary>
        /// <param name="pScheduleDuration">The duration task schedulation took.</param>
        /// <param name="pTotalDuration">The overall thread work duration</param>
        void PrefetchStatistics(double pScheduleDuration, double pTotalDuration);

        /// <summary>
        /// Displays debug information about profiled task(s)
        /// </summary>
        void DisplaysDebug();

        #endregion Methods
    }
}
