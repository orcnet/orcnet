﻿namespace OrcNet.Core.Task
{
    /// <summary>
    /// Base task observable interface definition.
    /// </summary>
    public interface ITaskObservable : IObservable
    {
        #region Methods

        /// <summary>
        /// Add a new observer to this observable for being 
        /// notified about task changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        void AddTaskObserver(ITaskObserver pObserver);

        /// <summary>
        /// Remove an observer to this observable to stop
        /// notifying it on task changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        void RemoveTaskObserver(ITaskObserver pObserver);
        
        #endregion Methods
    }
}
