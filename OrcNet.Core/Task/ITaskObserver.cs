﻿namespace OrcNet.Core.Task
{
    /// <summary>
    /// Base task observer interface definition.
    /// </summary>
    public interface ITaskObserver : IObserver
    {
        #region Methods

        /// <summary>
        /// Delegate called on observed task state changes.
        /// </summary>
        /// <param name="pSender">The observed task</param>
        /// <param name="pState">The new task state.</param>
        /// <param name="pIsDone">The flag indicating whether the task is done or not.</param>
        void OnTaskStateChanged(ITaskObservable pSender, TaskState pState, bool pIsDone);

        /// <summary>
        /// Delegate called on observed task completion date changes.
        /// </summary>
        /// <param name="pSender">The observed task</param>
        /// <param name="pDate">The new completion date.</param>
        void OnTaskCompletionDateChanged(ITaskObservable pSender, uint pDate);

        #endregion Methods
    }
}
