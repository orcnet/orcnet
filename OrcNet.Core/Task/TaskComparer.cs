﻿using System.Collections.Generic;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Task comparer class definition a task from another
    /// </summary>
    public class TaskComparer : IComparer<ITask>
    {
        #region Methods

        /// <summary>
        /// Compare the first task with the second one to determine
        /// whether the second one is a predecessor, same deadline or
        /// successor regarding to the first one.
        /// </summary>
        /// <param name="pFirst">The first task</param>
        /// <param name="pSecond">The second task</param>
        /// <returns>-1 if second is predecessor, 1 if second is successor, 0 if at same time</returns>
        public int Compare(ITask pFirst, ITask pSecond)
        {
            int lFirstDuration  = (int)pFirst.ExpectedDuration;
            int lSecondDuration = (int)pSecond.ExpectedDuration;
            if ( lFirstDuration == lSecondDuration )
            {
                int lFirstId  = pFirst.Id;
                int lSecondId = pSecond.Id;
                return lFirstId.CompareTo( lSecondId );
            }

            return lFirstDuration.CompareTo( lSecondDuration );
        }

        #endregion Methods
    }
}
