﻿// if defined, prefecth threads work only on tasks for future frames
// otherwise they can also execute tasks for the current frame, in parallel
// with main thread
#define STRICT_PREFETCH

using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Core.Service;
using OrcNet.Core.Thread;
using OrcNet.Core.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace OrcNet.Core.Task
{
    /// <summary>
    /// Parallel scheduler offering multi threaded task execution(s).
    /// </summary>
    public class ParallelScheduler : IScheduler
    {
        #region Inner Struct

        /// <summary>
        /// Task key struct definition used a key of the task map and contains
        /// the deadline and its context.
        /// </summary>
        private struct TaskKey : IComparable<TaskKey>
        {
            #region Fields

            /// <summary>
            /// Stores the task deadline.
            /// </summary>
            private uint        mDeadline;

            /// <summary>
            /// Stores the task deadline.
            /// </summary>
            private IComparable mContext;

            #endregion Fields

            #region Properties

            /// <summary>
            /// Gets the task deadline.
            /// </summary>
            public uint Deadline
            {
                get
                {
                    return this.mDeadline;
                }
            }

            /// <summary>
            /// Gets the task deadline.
            /// </summary>
            public object Context
            {
                get
                {
                    return this.mContext;
                }
            }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="TaskKey"/> class.
            /// </summary>
            /// <param name="pDeadline">The task deadline</param>
            /// <param name="pContext">The task context</param>
            public TaskKey(uint pDeadline, IComparable pContext)
            {
                this.mDeadline = pDeadline;
                this.mContext  = pContext;
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// Methods used to compare a TaskKey from another.
            /// </summary>
            /// <param name="pOther">The other TaskKey to compare with</param>
            /// <returns></returns>
            public int CompareTo(TaskKey pOther)
            {
                uint lThisDeadline  = this.mDeadline;
                uint lOtherDeadline = pOther.mDeadline;

                // If the deadline is the same.
                if ( lThisDeadline == lOtherDeadline )
                {
                    if ( ReferenceEquals( this.mContext, null ) &&
                         ReferenceEquals( pOther.mContext, null ) )
                    {
                        // Same position.
                        return 0;
                    }
                    else if ( ReferenceEquals( this.mContext, null ) == false )
                    {
                        // Test the context by using Comparison (NOTE: pOther.mContext can be Null)
                        return this.mContext.CompareTo( pOther.mContext );
                    }

                    // Else, this.mContext is Null but the other doesn't so follows pOther in the sort order.
                    return 1;
                }

                return lThisDeadline.CompareTo( lOtherDeadline) ;
            }

            #endregion Methods
        }

        /// <summary>
        /// Task key comparer struct definition.
        /// </summary>
        private struct TaskKeyComparer : IComparer<TaskKey>
        {
            #region Methods

            /// <summary>
            /// Compare a task key with another.
            /// </summary>
            /// <param name="pFirst">The first task key</param>
            /// <param name="pSecond">The second task key</param>
            /// <returns></returns>
            public int Compare(TaskKey pFirst, TaskKey pSecond)
            {
                return pFirst.CompareTo( pSecond );
            }

            #endregion Methods
        }

        #endregion Inner Struct

        #region Fields

        /// <summary>
        /// Synchronizer for thread safety.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the scheduler creator.
        /// </summary>
        private IResource                                   mCreator;

        /// <summary>
        /// Stores the signal thread(s) will wait for to be informed 
        /// that new CPU or GPU tasks are ready for being executed.
        /// </summary>
        AutoResetEvent                                      mNewTasksReady;

        /// <summary>
        /// Stores the signal thread(s) will wait for to be informed 
        /// that new CPU (ONLY) tasks are ready for being executed.
        /// </summary>
        AutoResetEvent                                      mNewCPUTasksReady;

        /// <summary>
        /// Stores the tasks ready to be executed, that is, all their parent(s) are completed.
        /// </summary>
        private SortedDictionary<TaskKey, SortedSet<ITask>> mAllReadyTasks;

        /// <summary>
        /// Stores the CPU only tasks ready to be executed.
        /// </summary>
        private SortedDictionary<TaskKey, SortedSet<ITask>> mCPUReadyTasks;

        /// <summary>
        /// Stores the parent tasks of tasks that remain to be executed.
        /// </summary>
        private Dictionary<ITask, HashSet<ITask>>           mParentTasks;

        /// <summary>
        /// Stores the flattened parents tasks from a given task, that is, whatever the hierarchy level.
        /// </summary>
        private Dictionary<ITask, HashSet<ITask>>           mFlattenedParentTasks;

        /// <summary>
        /// Stores the children tasks of tasks that remain to be executed.
        /// </summary>
        private Dictionary<ITask, HashSet<ITask>>           mChildrenTasks;

        /// <summary>
        /// Stores the flattened children tasks from a given task, that is, whatever the hierarchy level.
        /// </summary>
        private Dictionary<ITask, HashSet<ITask>>           mFlattenedChildrenTasks;

        /// <summary>
        /// Stores the set of tasks that must be executed at the current frame.
        /// </summary>
        private HashSet<ITask>                              mImmediateTasks;

        /// <summary>
        /// Stores the prefetching tasks that must be executed later on.
        /// </summary>
        private HashSet<ITask>                              mPrefetchingTasks;

        /// <summary>
        /// Stores the set of threads used to execute tasks.
        /// </summary>
        private List<System.Threading.Thread>               mThreads;

        /// <summary>
        /// Stores the minimum number of prefetching tasks to execute per frame.
        /// </summary>
        private int                                         mPrefetchRate;

        /// <summary>
        /// Stores the maximum number of pending prefetching tasks.
        /// </summary>
        private int                                         mMaxPrefetchCacheSize;

        /// <summary>
        /// Stores the target frame duration in micro seconds.
        /// Can be 0 if no fixed frame rate.
        /// </summary>
        private double                                      mFramePeriod;

        /// <summary>
        /// Stores the time in micro seconds at the end of the last 
        /// thread job executing task(s).
        /// </summary>
        private double                                      mLastExecutionTime;

        /// <summary>
        /// Stores the logical time used for task completion dates.
        /// It is incremented by one after each task execution.
        /// </summary>
        private uint                                        mTime;

        /// <summary>
        /// Stores the flag indicating whether the scheduler must be stopped or not.
        /// </summary>
        private bool                                        mMustStop;

        #endregion Fields

        #region Properties

        #region Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        public IResource Creator
        {
            get
            {
                return this.mCreator;
            }
            set
            {
                this.mCreator = value;
            }
        }

        #endregion Properties IResourceCreatable

        /// <summary>
        /// Gets the flag indicating whether CPU prefetching is supported or not.
        /// </summary>
        public bool SupportsCPUPrefetch
        {
            get
            {
                if ( this.mPrefetchRate > 0 ||
                     this.mFramePeriod > 0.0 ||
                     this.mThreads.Any() )
                {
                    if ( this.mThreads.Count == 0 )
                    {
                        return this.mPrefetchingTasks.Count < this.mMaxPrefetchCacheSize;
                    }

                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether GPU prefetching is supported or not.
        /// </summary>
        public bool SupportsGPUPrefetch
        {
            get
            {
                if ( this.mPrefetchRate > 0 ||
                     this.mFramePeriod > 0.0 )
                {
                    return this.mPrefetchingTasks.Count < this.mMaxPrefetchCacheSize;
                }

                return false;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ParallelScheduler"/> class.
        /// </summary>
        /// <param name="pThreadCount">The amount of execution threads</param>
        /// <param name="pPrefetchRate">The minimum number of prefetching tasks to execute per frame.</param>
        /// <param name="pMaxPrefetchCacheSize">The maximum number of pending prefetching tasks.</param>
        /// <param name="pFrameRate">The frame rate those tasks must be processed at</param>
        public ParallelScheduler(int pThreadCount = 0, int pPrefetchRate = 0, int pMaxPrefetchCacheSize = 0, double pFrameRate = 0.0)
        {
            this.Initialize( pThreadCount, pPrefetchRate, pMaxPrefetchCacheSize, pFrameRate );
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initialize the scheduler.
        /// </summary>
        /// <param name="pThreadCount">The amount of execution threads</param>
        /// <param name="pPrefetchRate">The minimum number of prefetching tasks to execute per frame.</param>
        /// <param name="pMaxPrefetchCacheSize">The maximum number of pending prefetching tasks.</param>
        /// <param name="pFrameRate">The frame rate those tasks must be processed at</param>
        private void Initialize(int pThreadCount, int pPrefetchRate, int pMaxPrefetchCacheSize, double pFrameRate)
        {
            if ( pPrefetchRate > 0 ||
                 pFrameRate > 0.0 )
            {
                if ( pMaxPrefetchCacheSize == 0 )
                {
                    throw new ArgumentException( "Invalid scheduler argument. No prefetch and frame rate but it needs at least a maximum prefetch cache size !!!" );
                }
            }

            this.mMustStop = false;
            this.mImmediateTasks   = new HashSet<ITask>();
            this.mPrefetchingTasks = new HashSet<ITask>();
            this.mParentTasks      = new Dictionary<ITask, HashSet<ITask>>();
            this.mChildrenTasks    = new Dictionary<ITask, HashSet<ITask>>();
            this.mCPUReadyTasks    = new SortedDictionary<TaskKey, SortedSet<ITask>>( new TaskKeyComparer() );
            this.mAllReadyTasks    = new SortedDictionary<TaskKey, SortedSet<ITask>>( new TaskKeyComparer() );

            // Create internal flattened caches for parents and children.
            this.mFlattenedParentTasks   = new Dictionary<ITask, HashSet<ITask>>();
            this.mFlattenedChildrenTasks = new Dictionary<ITask, HashSet<ITask>>();

            // Create thread(s)
            this.mThreads          = new List<System.Threading.Thread>( pThreadCount );
            for ( int lCurrentThread = 0; lCurrentThread < pThreadCount; lCurrentThread++ )
            {
                string lNewThreadName = "Task worker " + lCurrentThread.ToString();
                System.Threading.Thread lNewThread = ThreadFactory.CreateThread( lNewThreadName, this.DoWork );
                this.mThreads[ lCurrentThread ] = lNewThread;
            }

            // Set scheduler time and rate info.
            this.mLastExecutionTime    = 0.0;
            this.mTime                 = 2;
            this.mPrefetchRate         = pPrefetchRate;
            this.mMaxPrefetchCacheSize = pMaxPrefetchCacheSize;
            this.mFramePeriod          = 0.0;
            if ( System.Math.Abs( pFrameRate ) > double.Epsilon )
            {
                this.mFramePeriod = 1e6 / pFrameRate;
            }

            this.mNewTasksReady    = new AutoResetEvent( false );
            this.mNewCPUTasksReady = new AutoResetEvent( false );
        }

        /// <summary>
        /// Recursive function adding a task and its sub tasks if task set whatever their level with
        /// their dependencies as well.
        /// </summary>
        /// <param name="pTask">The new task to add and recursively look for sub tasks</param>
        /// <param name="pToExecuteTasks">The growing set of tasks to execute</param>
        private void AddTask(ITask pTask, ref HashSet<ITask> pToExecuteTasks)
        {
            // NOTE: Mutex must be locked before calling that method.

            // Check if not already in.
            if ( pToExecuteTasks.Contains( pTask ) )
            {
                return;
            }

            pToExecuteTasks.Add( pTask );

            // Do not go any further if task done.
            if ( pTask.IsDone )
            {
                return;
            }

            TaskSet lSet = pTask as TaskSet;
            if ( lSet == null )
            {
                if ( pTask.DeadLine == 0 )
                {
                    this.mImmediateTasks.Add( pTask );
                }
                else
                {
                    this.mPrefetchingTasks.Add( pTask );
                }

                InsertTask( pTask, ref this.mAllReadyTasks );

#if (STRICT_PREFETCH)

                if ( pTask.IsGPUTask == false &&
                     pTask.DeadLine > 0 )
                {

#else
                if ( pTask.IsGPUTask == false )
                {
#endif
                    InsertTask( pTask, ref this.mCPUReadyTasks );
                }
            }
            else
            {
                // First clear any related flattened cache for that set.
                this.mFlattenedParentTasks[ lSet ]   = new HashSet<ITask>();
                this.mFlattenedChildrenTasks[ lSet ] = new HashSet<ITask>();

                foreach ( ITask lTask in lSet.Tasks )
                {
                    this.AddTask( lTask, ref pToExecuteTasks );
                }

                // Flatten Parent and Child tasks into internal caches.
                foreach ( ITask lOrphanTask in lSet.ParentTasks )
                {
                    // Check whether there is also Sub task set
                    // else, add as normal
                    TaskSet lSubSet = lOrphanTask as TaskSet;
                    if ( lSubSet == null )
                    {
                        this.mFlattenedParentTasks[ lSet ].Add( lOrphanTask );
                    }
                    else
                    {
                        this.mFlattenedParentTasks[ lSet ].UnionWith( this.mFlattenedParentTasks[ lSubSet ] );
                    }
                }

                // Check whether there is also Sub task set
                // else, add as normal
                foreach ( ITask lChildTask in lSet.LeafTasks )
                {
                    TaskSet lSubSet = lChildTask as TaskSet;
                    if ( lSubSet == null )
                    {
                        this.mFlattenedChildrenTasks[ lSet ].Add( lChildTask );
                    }
                    else
                    {
                        this.mFlattenedChildrenTasks[ lSet ].UnionWith( this.mFlattenedChildrenTasks[ lSubSet ] );
                    }
                }

                // Now updates dependencies between tasks
                foreach ( ITask lTask in lSet.Tasks )
                {
                    if ( lTask.IsDone )
                    {
                        continue;
                    }

                    foreach ( ITask lChild in lSet.GetChildrenOf( lTask ) )
                    {
                        this.AddDependency( lChild, lTask );
                    }
                }
            }
        }

        /// <summary>
        /// Add a new dependency between two tasks.
        /// </summary>
        /// <param name="pFrom">The source of the dependency</param>
        /// <param name="pTo">The destination of the dependency</param>
        private void AddDependency(ITask pFrom, ITask pTo)
        {
            // NOTE: Mutex must be locked before calling that method.

            TaskSet lFromSet = pFrom as TaskSet;
            if ( lFromSet != null )
            {
                HashSet<ITask> lFlattenedParents;
                if ( this.mFlattenedParentTasks.TryGetValue( lFromSet, out lFlattenedParents ) )
                {
                    foreach ( ITask lFlattenedParent in lFlattenedParents )
                    {
                        this.AddDependency( lFlattenedParent, pTo );
                    }
                }
            }
            else
            {
                TaskSet lToSet = pTo as TaskSet;
                if ( lToSet != null )
                {
                    HashSet<ITask> lFlattenedChildren;
                    if ( this.mFlattenedChildrenTasks.TryGetValue( lToSet, out lFlattenedChildren ) )
                    {
                        foreach ( ITask lFlattenedChild in lFlattenedChildren )
                        {
                            this.AddDependency( pFrom, lFlattenedChild );
                        }
                    }
                }
                else
                {
                    RemoveTask( pFrom, ref this.mAllReadyTasks );
                    RemoveTask( pFrom, ref this.mCPUReadyTasks );

                    if ( this.mParentTasks.ContainsKey( pFrom ) == false )
                    {
                        // First time creation, [UnknownKey] only works on Set operation.
                        this.mParentTasks[ pFrom ] = new HashSet<ITask>();
                    }

                    this.mParentTasks[ pFrom ].Add( pTo );

                    if ( this.mChildrenTasks.ContainsKey( pTo ) == false )
                    {
                        // First time creation, [UnknownKey] only works on Set operation.
                        this.mChildrenTasks[ pTo ] = new HashSet<ITask>();
                    }

                    this.mChildrenTasks[ pTo ].Add( pFrom );

                    HashSet<ITask> lVisited = new HashSet<ITask>();
                    this.SetDeadline( pTo, pFrom.DeadLine, ref lVisited );

                    if ( pFrom.DeadLine < pTo.DeadLine )
                    {
                        throw new InvalidOperationException( "Invalid dependency between tasks!!! Deadline of child must be greater than the deadline of its parent..." );
                    }
                }
            }
        }

        /// <summary>
        /// Set the new deadline of a task and recursively update the parents deadline
        /// in order to have a smaller deadline that this child task then.
        /// </summary>
        /// <param name="pTask">The task to update the deadline to</param>
        /// <param name="pDeadline">The new task's deadline</param>
        /// <param name="pVisitedTasks">The already visited task.</param>
        private void SetDeadline(ITask pTask, uint pDeadline, ref HashSet<ITask> pVisitedTasks)
        {
            if ( pVisitedTasks.Contains( pTask ) )
            {
                return;
            }

            pVisitedTasks.Add( pTask );

            TaskSet lSet = pTask as TaskSet;
            if ( lSet != null )
            {
                foreach ( ITask lTask in lSet.Tasks )
                {
                    this.SetDeadline( lTask, pDeadline, ref pVisitedTasks );
                }
            }

            if ( pTask.DeadLine > pDeadline )
            {
                bool lHasRemovedFromAllCache = RemoveTask( pTask, ref this.mAllReadyTasks );
                bool lHasRemovedFromCPUCache = RemoveTask( pTask, ref this.mCPUReadyTasks );

                (pTask as ATask).DeadLine = pDeadline;

                if ( lHasRemovedFromAllCache )
                {
                    InsertTask( pTask, ref this.mAllReadyTasks );
                }
                if ( lHasRemovedFromCPUCache )
                {
                    if ( pTask.IsGPUTask )
                    {
                        throw new InvalidOperationException( "Cannot process a GPU based task as CPU one..." );
                    }

#if (STRICT_PREFETCH)
                    if 
                        ( pTask.DeadLine > 0 )
                    {
                        InsertTask( pTask, ref this.mCPUReadyTasks );
                    }
#else
                    InsertTask( pTask, ref this.mCPUReadyTasks );
#endif
                }

                // Update parents deadline accordingly to the new task deadline.
                HashSet<ITask> lParents;
                if ( this.mParentTasks.TryGetValue( pTask, out lParents ) )
                {
                    foreach ( ITask lParent in lParents )
                    {
                        this.SetDeadline( lParent, pDeadline, ref pVisitedTasks );
                    }
                }
            }
        }

        /// <summary>
        /// Finalizes a completed task what removes any dependencies and allow new tasks ready
        /// to be executed to be added in corresponding containers for being processed.
        /// </summary>
        /// <param name="pCompleted">The completed task</param>
        /// <param name="pHasChanged">The flag indicating whether the task has changed since its last execution.</param>
        private void OnTaskCompleted(ITask pCompleted, bool pHasChanged)
        {
            ATask lTask = pCompleted as ATask;
            lock ( sSyncRoot )
            {
                uint lCompletionDate = lTask.CompletionDate;
                if ( pHasChanged )
                {
                    lCompletionDate = this.mTime;
                }

                HashSet<ITask> lChildren;
                if ( this.mChildrenTasks.TryGetValue( lTask, out lChildren ) )
                {
                    // Iterate over the successors of the task
                    foreach ( ITask lChild in lChildren )
                    {
                        HashSet<ITask> lParentsOfChild;
                        if ( this.mParentTasks.TryGetValue( lChild, out lParentsOfChild ) )
                        {
                            // Remove the task from children's parents set.
                            lParentsOfChild.Remove( lTask );

                            // If it was the only parent of the child, so now that child
                            // if executable.
                            if ( lParentsOfChild.Count == 0 )
                            {
                                this.mParentTasks.Remove( lChild );

                                // Then add it to the set of Ready tasks.
                                InsertTask( lChild, ref this.mAllReadyTasks );

                                // Informs thread(s) that a new task has to be executed.
                                this.mNewTasksReady.Set();

#if (STRICT_PREFETCH)
                                if ( lChild.IsGPUTask == false && 
                                     lChild.DeadLine > 0 )
                                {
#else
                                if ( lChild.IsGPUTask == false )
                                {
#endif
                                    // Add it to the CPU ready tasks set.
                                    InsertTask( lChild, ref this.mCPUReadyTasks );

                                    // Informs thread(s) that a new CPU task has to be executed.
                                    this.mNewCPUTasksReady.Set();
                                }
                            }
                        }
                    }
                    this.mChildrenTasks.Remove( lTask );
                }

                this.mPrefetchingTasks.Remove( lTask );

                // Finally, mark that task as done.
                lTask.UpdateState( true, lCompletionDate );

                // Update by one as a task has been completed.
                this.mTime++;
            }
        }

        /// <summary>
        /// All the scheduler threads will run that callback in charge of executing 
        /// ready tasks.
        /// </summary>
        private void DoWork()
        {
            // Create a timer to time work.
            BaseTimer lTimer = new BaseTimer();

            // Loop execution until the scheduler is needed no more.
            while ( this.mMustStop == false )
            {
                ITask lTask = null;
                lock ( sSyncRoot )
                {
                    // Make thread(s) wait for a CPU ready signal while
                    // no CPU tasks to execute. Those thread(s) cannot process
                    // GPU tasks due to OpenGL supporting only one thread at
                    // a time.
                    while ( this.mCPUReadyTasks.Count == 0 &&
                            this.mMustStop == false )
                    {
                        this.mNewCPUTasksReady.WaitOne();
                    }

                    if ( this.mMustStop == false )
                    {
                        if ( this.mCPUReadyTasks.Any() )
                        {
                            // Selects the first ready task.
                            KeyValuePair<TaskKey, SortedSet<ITask>> lFirst = this.mCPUReadyTasks.First();
                            if ( lFirst.Value.Any() )
                            {
                                lTask = lFirst.Value.First();

#if (STRICT_PREFETCH)
                                if ( lTask.DeadLine == 0 )
                                {
                                    LogManager.Instance.Log( "The task deadline", LogType.WARNING );
                                }
#endif
                                // Remove the task from the set so that other 
                                // thread(s) will do not care about it.
                                if ( lTask.DeadLine == 0 )
                                {
                                    this.mImmediateTasks.Remove( lTask );
                                }

                                RemoveTask( lTask, ref this.mAllReadyTasks );
                                RemoveTask( lTask, ref this.mCPUReadyTasks );
                            }
                        }
                    }
                }

                // 
                if ( this.mMustStop == false )
                {
                    ATask lCast = lTask as ATask;
                    if ( lCast != null &&
                         lCast.IsGPUTask == false )
                    {
                        bool lHasChanged = false;
                        if ( lCast.IsDone == false )
                        {
                            LogManager.Instance.Log( string.Format( "Scheduler prefetching a {0} task...", lCast.GetType().Name ) );

                            if ( lCast.CompletionDate >= lCast.PredecessorsCompletionDate )
                            {
                                // The task is up to date, so nothing to do.
                            }
                            else if ( this.mFramePeriod > 0.0 )
                            {
                                lTimer.Start();
                                lHasChanged = lCast.Execute();
                                double lDuration = lTimer.End();

                                // [PROFILER] Figure out how to profile there using lDuration.
                            }
                            else
                            {
                                lHasChanged = lCast.Execute();
                            }
                        }

                        this.OnTaskCompleted( lTask, lHasChanged );
                    }
                    else
                    {
                        LogManager.Instance.Log( "Attempting to execute a GPU task in CPU thread(s) !!!", LogType.ERROR );
                    }
                }
            }
        }
        
        /// <summary>
        /// Helper method inserting a task to the ready tasks set.
        /// </summary>
        /// <param name="pNewTask">The new task</param>
        /// <param name="pReadyTasks">The ready tasks</param>
        private static void InsertTask(ITask pNewTask, ref SortedDictionary<TaskKey, SortedSet<ITask>> pReadyTasks)
        {
            TaskKey lKey = new TaskKey( pNewTask.DeadLine, pNewTask.Context );
            if ( pReadyTasks.ContainsKey( lKey ) == false )
            {
                // First time creation, [UnknownKey] only works on Set operation.
                pReadyTasks[ lKey ] = new SortedSet<ITask>( new TaskComparer() );
            }

            pReadyTasks[ lKey ].Add( pNewTask );
        }

        /// <summary>
        /// Helper method removing a task to the ready tasks set.
        /// </summary>
        /// <param name="pOldTask">The old task</param>
        /// <param name="pReadyTasks">The ready tasks</param>
        private static bool RemoveTask(ITask pOldTask, ref SortedDictionary<TaskKey, SortedSet<ITask>> pReadyTasks)
        {
            SortedSet<ITask> lTasksOfSameDeadline;
            TaskKey lKey = new TaskKey( pOldTask.DeadLine, pOldTask.Context );
            if ( pReadyTasks.TryGetValue( lKey, out lTasksOfSameDeadline ) )
            {
                bool lResult = lTasksOfSameDeadline.Remove( pOldTask );
                if ( lTasksOfSameDeadline.Count == 0 )
                {
                    pReadyTasks.Remove( lKey );
                }

                return lResult;
            }

            return false;
        }

        /// <summary>
        /// Helper method in charge of retrieving the first task
        /// </summary>
        /// <param name="pReadyTasks">The ready tasks</param>
        /// <param name="pContext">The task's contextual object</param>
        private static ITask FirstOrDefaultTask(SortedDictionary<TaskKey, SortedSet<ITask>> pReadyTasks, object pContext)
        {
            if ( pReadyTasks.Any() )
            {
                KeyValuePair<TaskKey, SortedSet<ITask>> lFirstPair = pReadyTasks.First();
                if ( lFirstPair.Value.Any() )
                {
                    // The first being the one with the smallest deadline, get it
                    uint lDeadline = lFirstPair.Key.Deadline;

                    // And build a new key to find another task having the same deadline 
                    // but the supplied context.
                    SortedSet<ITask> lTasks;
                    TaskKey lNextKey = new TaskKey( lDeadline, pContext as IComparable );
                    if ( pReadyTasks.TryGetValue( lNextKey, out lTasks ) )
                    {
                        // If anything found, return the first task of the set.
                        return lTasks.FirstOrDefault();
                    }

                    // If nothing found... but the context was not null
                    // another try will look for a task with an empty context.
                    // This will prevent a context switch from happening.
                    if ( pContext != null )
                    {
                        SortedSet<ITask> lNullContextTasks;
                        TaskKey lNullContextKey = new TaskKey( lDeadline, null );
                        if ( pReadyTasks.TryGetValue( lNullContextKey, out lNullContextTasks ) )
                        {
                            return lNullContextTasks.FirstOrDefault();
                        }
                    }

                    // In all cases, returns the first one of the sorted set
                    return lFirstPair.Value.FirstOrDefault();
                }
            }

            return null;
        }

        /// <summary>
        /// Set a task type to the profiler for being profiled.
        /// </summary>
        /// <param name="pTaskType"></param>
        internal void ProfileTask(Type pTaskType)
        {
            ITaskProfiler lProfiler = ServiceManager.Instance.GetService<ITaskProfiler>();
            if ( lProfiler != null )
            {
                lProfiler.ProfileTask( pTaskType );
            }
        }

        /// <summary>
        /// Swaps the scheduler with another.
        /// </summary>
        /// <param name="pOther">The other one to swap with.</param>
        internal void Swap(ParallelScheduler pOther)
        {
            // Nothing to do.
        }

        #endregion Methods Internal

        #region Methods IScheduler
        
        /// <summary>
        /// Schedule a new task or task set whose deadline is not immediate but ONLY if prefetching
        /// of the given task(s) is supported.
        /// </summary>
        /// <param name="pTask">A simple task or set of tasks whose deadline is not immediate</param>
        /// <returns>True if scheduling succeeded, false otherwise.</returns>
        public bool Schedule(ITask pTask)
        {
            ATask lTask = pTask as ATask;

            HashSet<ITask> lInitialized = new HashSet<ITask>();
            lTask.Prepare( ref lInitialized );
            lock ( sSyncRoot )
            {
                HashSet<ITask> lToExecuteTasks = new HashSet<ITask>();
                bool lHasNoCPUTasks = this.mCPUReadyTasks.Count == 0;
                this.AddTask( pTask, ref lToExecuteTasks );

                // Notifies thread(s) about that new ready task.
                this.mNewTasksReady.Set();

                // If there were no ready CPU tasks before and some are ready now
                // Notify particularly CPU new task signal.
                if ( lHasNoCPUTasks && 
                     this.mCPUReadyTasks.Any() )
                {
                    this.mNewCPUTasksReady.Set();
                }

                // Log if soemthing wrong happened
                if ( this.mAllReadyTasks.Count == 0 )
                {
                    LogManager.Instance.Log( "Attempted to schedule a new task but nothing has been added !!!" , LogType.ERROR );
                }
            }

            return true;
        }

        /// <summary>
        /// Schedule a task or task set reexecution
        /// </summary>
        /// <param name="pTask">A simple task or set of tasks</param>
        /// <param name="pState">The task state.</param>
        /// <param name="pDeadline">The frame number before which the task must be re executed.</param>
        /// <returns>True if scheduling succeeded, false otherwise.</returns>
        public bool ReSchedule(ITask pTask, TaskState pState, uint pDeadline)
        {
            ATask lTask = pTask as ATask;
            bool lResult = true;
            lock ( sSyncRoot )
            {
                lTask.UpdateState( false, 0, pState );
                if ( pState == TaskState.DATA_NEEDED )
                {
                    // Update recursively all tasks deadline starting from that task
                    // until the deeper child.
                    HashSet<ITask> lVisited = new HashSet<ITask>();
                    this.SetDeadline( pTask, pDeadline, ref lVisited );
                }
            }

            return lResult;
        }

        /// <summary>
        /// Executes the supplied task or task set until all immediate deadline 
        /// are completed.
        /// </summary>
        /// <param name="pTask">A simple task or set of tasks</param>
        public void Execute(ITask pTask)
        {
            BaseTimer lTimer = new BaseTimer();
            lTimer.Start();

            this.Schedule( pTask );

            double lScheduleDuration = lTimer.End();

            // Log debug info.
            LogManager.Instance.Log( string.Format( "Executing {0} tasks!!!\n\t{1} pending ready tasks of which {2} are CPU based.\n\t{3} dependencies between tasks.", this.mImmediateTasks.Count, this.mAllReadyTasks.Count, this.mCPUReadyTasks.Count, this.mParentTasks.Count + this.mChildrenTasks.Count ) );

            int lExecutedTaskCount = 0;
            int lPrefetchedTask    = 0;
            int lContextSwitches   = 0;
            double lDeadline       = 0.0;
            ATask lPreviousGPUTask = null;
            TimeSpan lTimeOut = TimeSpan.Zero;

            // [PROFILER] Clean any previous statistics
            ITaskProfiler lProfiler = ServiceManager.Instance.GetService<ITaskProfiler>();
            if ( lProfiler != null )
            {
                lProfiler.ResetStatistics();
            }

            if ( this.mFramePeriod > 0.0 )
            {
                // If there is a fixed framerate, we compute the deadline for the end
                // of this method; this is the time at the end of the last call to this
                // method, plus the delay for one frame (minus a small margin)
                lDeadline = this.mLastExecutionTime + this.mFramePeriod - 1000.0;
                // subtrating the current time gives the time interval for this methd
                double lDelay = lDeadline - lTimer.Start();
                // Compute the deadline timeout that will be pass to the thread signal to stop waiting after the timeout.
                lTimeOut = TimeSpan.FromMilliseconds( lDelay / 1000.0 );
            }

            bool lLoop = true;
            // Then, loop to execute all tasks.
            while ( lLoop )
            {
                // First step is to find or wait for ready task(s)
                ATask lTask = null;
                lock ( sSyncRoot )
                {
                    if ( this.mImmediateTasks.Count == 0 &&
                         this.mFramePeriod > 0.0 )
                    {
                        // If the tasks for the current frame are completed, and if we have
                        // a fixed framerate, we can use the time until the deadline to
                        // execute some tasks for next few frames
                        while ( this.mAllReadyTasks.Count == 0 &&
                                lTimer.Start() < lDeadline )
                        {
                            // Wait for any ready CPU or GPU task(s)
                            // and stop waiting once the deadline is passed.
                            this.mNewTasksReady.WaitOne( lTimeOut );
                        }
                    }
                    else
                    {
                        // Here either some tasks for the current frame are not completed
                        // or they are all completed but we do not have a fixed framerate
                        while ( this.mImmediateTasks.Any() && 
                                (this.mAllReadyTasks.Count == 0 || this.mAllReadyTasks.First().Key.Deadline > 0) )
                        {
                            // While some tasks for the current frame remain to be executed,
                            // and while the set of ready tasks to be executed is empty or
                            // contains only tasks for the next frames (Deadline > 0), just wait
                            this.mNewTasksReady.WaitOne();
                        }
                    }

                    // If the deadline is passed or if all the tasks for the current frame
                    // are completed, there may not be any task ready to be executed
                    if ( this.mAllReadyTasks.Any() )
                    {
                        // In the case there is at least one we pick one, if possible with the
                        // same execution context as the last executed GPU task
                        lTask = FirstOrDefaultTask( this.mAllReadyTasks, lPreviousGPUTask == null ? null : lPreviousGPUTask.Context ) as ATask;
                        if ( lTask.DeadLine != 0 )
                        {
                            // if this task is for the next frames, then all tasks for the
                            // current frame should now be completed (tasks are sorted in
                            // such a way that tasks for the current frame are executed first)
                            if ( this.mImmediateTasks.Any() )
                            {
                                LogManager.Instance.Log( "WARNING: There is still some immediate tasks to process!!!" );
                            }

                            // Checks whether the required minimum number of
                            // prefetching tasks per frame has been reached, 
                            // else this available prefetching task will be
                            // executed.
                            if ( lPrefetchedTask >= this.mPrefetchRate )
                            {
                                // if we do not have a fixed framerate, or if the time remaining
                                // until the deadline is less than the expected duration for this
                                // task, we should stop here; setting t to NULL will force this
                                if ( this.mFramePeriod == 0.0 || 
                                     lTimer.Start() + lTask.ExpectedDuration > lDeadline )
                                {
                                    lTask = null;
                                }
                            }
                        }
                        if ( lTask != null )
                        {
                            // If there is finally a task to execute, remove it from the
                            // sets that may contain it (but not update the dependencies 
                            // yet as this will be done after the task execution in OnTaskCompleted)
                            this.mImmediateTasks.Remove( lTask );
                            RemoveTask( lTask, ref this.mAllReadyTasks );
                            RemoveTask( lTask, ref this.mCPUReadyTasks );
                        }
                    }

                    // Now, the lock can be removed since the sharde data will not be read or 
                    // modified until OnTaskCompleted is called; also the selected
                    // task lTask cannot be seleted by another thread, since it has been removed
                    // from the task sets.
                }

                if ( lTask == null )
                {
                    // Stop looping...
                    lLoop = false;
                    continue; // Skip the rest.
                }

                bool lHasChanged = false;
                if ( lTask.IsDone == false )
                {
                    LogManager.Instance.Log( string.Format( "{0} a {1} task...", lTask.DeadLine > 0 ? "Prefetching" : "Running", lTask.GetType().Name ) );

                    // Checks for GPU tasks.
                    if ( lTask.IsGPUTask )
                    {
                        // If a GPU task, sets the execution context.
                        if ( lPreviousGPUTask == null )
                        {
                            // Is first one?
                            lTask.PreExecute();
                        }
                        else if ( lPreviousGPUTask.Context != lTask.Context )
                        {
                            lContextSwitches++;
                            lPreviousGPUTask.PostExecution();
                            lTask.PreExecute();
                        }

                        // Keep it in cache as the last GPU task.
                        lPreviousGPUTask = lTask;
                    }

                    if ( lTask.CompletionDate >= lTask.PredecessorsCompletionDate )
                    {
                        // Nothing to do as the task is already up to date...
                    }
                    else if ( this.mFramePeriod > 0.0 ||
                              (lProfiler != null && lProfiler.IsProfiling) )
                    {
                        // [PROFILER]
                        // if fixed framerate, measures the execution time
                        // of each task in order to get statistics about tasks, used to
                        // get estimated durations for future tasks
                        lTimer.Start();
                        lHasChanged = lTask.Execute();
                        double lExecutionTime = lTimer.End();

                        // [PROFILER]
                        // Update global statistics.
                        lProfiler.UpdateGlobalStatistics( lTask, lExecutionTime );

                        if ( lProfiler != null && 
                             lProfiler.IsProfiling )
                        {
                            // [PROFILER]
                            // Update frame statistics.
                            lProfiler.UpdateFrameStatistics( lTask, lExecutionTime );
                        }
                    }
                    else
                    {
                        lHasChanged = lTask.Execute();
                    }

                    lExecutedTaskCount++;
                    if ( lTask.DeadLine > 0 )
                    {
                        lPrefetchedTask++;
                    }
                }

                // Updates the task dependencies, and signals other thread(s) when
                // new tasks become ready to be executed
                this.OnTaskCompleted( lTask, lHasChanged );
            }

            if ( lPreviousGPUTask != null )
            {
                // restores the context after the last executed GPU task
                lPreviousGPUTask.PostExecution();
                lPreviousGPUTask = null;
            }

            LogManager.Instance.Log( string.Format( "Finished {0} tasks.\n{1} context switches.", lExecutedTaskCount, lContextSwitches ) );
            
            if ( this.mFramePeriod > 0.0 )
            {
                // [PROFILER] 
                // Log statistics.
                if ( lProfiler != null )
                {
                    lProfiler.DisplaysDebug();
                }

                double lTimeInMs = lTimer.Start();
                if ( lTimeInMs < lDeadline )
                {
                    // if we have a fixed framerate and if we still have some time before
                    // the deadline, we should wait until this deadline is passed, otherwise
                    // the framerate would increase
                    int lMillisecondsToSleep = (int)System.Math.Floor((lDeadline - lTimeInMs) / 1e3);
                    System.Threading.Thread.Sleep( lMillisecondsToSleep );
                }
            }

            // [PROFILER]
            // Prefetches statistics.
            if ( lProfiler != null &&
                 lProfiler.IsProfiling )
            {
                double lTotalDuration = lTimer.Start() - this.mLastExecutionTime;
                lProfiler.PrefetchStatistics( lScheduleDuration, lTotalDuration );
            }

            // Measures the current time at the end of this method
            // in order to compute a deadline for the next call to that thread job delegate.
            this.mLastExecutionTime = lTimer.Start();
        }

        /// <summary>
        /// Cleans up the given task's parent and children dependencies.
        /// </summary>
        /// <param name="pTask">The task to clean up.</param>
        public void Cleanup(ITask pTask)
        {
            // First clear any related flattened cache for that set.
            if ( this.mFlattenedParentTasks.ContainsKey( pTask ) )
            {
                // Just clear the attached tasks if any.
                this.mFlattenedParentTasks[ pTask ].Clear();
            }
            else
            {
                // First time creation, [UnknownKey] only works on Set operation.
                this.mFlattenedParentTasks[ pTask ] = new HashSet<ITask>();
            }
                
            if ( this.mFlattenedChildrenTasks.ContainsKey( pTask ) )
            {
                // Just clear the attached tasks if any.
                this.mFlattenedChildrenTasks[ pTask ].Clear();
            }
            else
            {
                // First time creation, [UnknownKey] only works on Set operation.
                this.mFlattenedChildrenTasks[ pTask ] = new HashSet<ITask>();
            }
        }

        #endregion Methods IScheduler

        #region Methods IDisposable

        /// <summary>
        /// Releases scheduler resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Inner method releasing scheduler resources.
        /// </summary>
        private void OnDispose()
        {
            lock ( sSyncRoot )
            {
                this.mMustStop = true;
                this.mNewCPUTasksReady.Set();
            }

            foreach ( System.Threading.Thread lThread in this.mThreads )
            {
                lThread.Join();
            }
            this.mThreads.Clear();

            this.mNewCPUTasksReady.Close();
            this.mNewCPUTasksReady.Dispose();

            this.mNewTasksReady.Close();
            this.mNewTasksReady.Dispose();

            this.mImmediateTasks.Clear();
            this.mPrefetchingTasks.Clear();
            this.mParentTasks.Clear();
            this.mChildrenTasks.Clear();
            this.mAllReadyTasks.Clear();
            this.mCPUReadyTasks.Clear();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
