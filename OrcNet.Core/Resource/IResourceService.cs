﻿using System.Xml.Linq;

namespace OrcNet.Core.Resource
{
    /// <summary>
    /// Base resource service interface definition.
    /// </summary>
    public interface IResourceService : IService
    {
        #region Properties

        /// <summary>
        /// Gets the resource loader used to get resource description.
        /// </summary>
        IResourceLoader Loader
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Modifies the service resource loader and maximum allowed unused resource count.
        /// </summary>
        /// <param name="pLoader">The resource loader used to get resource description.</param>
        /// <param name="pMaxUnusedCount">The maximum unused resource count for LRU cache</param>
        void ModifyLoader(IResourceLoader pLoader, uint pMaxUnusedCount = 0);

        /// <summary>
        /// Loads a resource by its name.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The resource, Null otherwise.</returns>
        IResource LoadResource(string pName);

        /// <summary>
        /// Loads a resource by its Xml descriptor.
        /// </summary>
        /// <param name="pDescriptor">The descriptor Xml element.</param>
        /// <returns>The resource, Null otherwise.</returns>
        IResource LoadResource(XElement pDescriptor);

        /// <summary>
        /// Updates the loaded resources if any have changed.
        /// This is an atomic process, either every single resources are updated
        /// else nothing at all.
        /// </summary>
        /// <returns>The flag indicating whether the update has been successful or not.</returns>
        bool UpdateResources();

        /// <summary>
        /// Invalidates a resource by setting it first as unused resource
        /// and removing it if not in cache or as pending unused until it
        /// reaches the maximum unused cache size and getting the oldest.
        /// see LRU cache principle.
        /// </summary>
        /// <param name="pToInvalidate">The resource to invalidate.</param>
        void InvalidateResource(IResource pToInvalidate);

        /// <summary>
        /// Closes the resource service by disabling allowed resource cache.
        /// </summary>
        void Close();

        #endregion Methods
    }
}
