﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Image;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource.Factories;
using OrcNet.Core.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Core.Resource.Loaders
{
    /// <summary>
    /// XML based resource loader class definition.
    /// </summary>
    public class XMLResourceLoader : IResourceLoader
    {
        #region Fields

        /// <summary>
        /// Stores the XML type keys for fast checking of XElement type support.
        /// </summary>
        private static HashSet<string> sResourceDataKeys;

        /// <summary>
        /// Stores the directory paths where individual 
        /// resource descriptor files can be looked for.
        /// </summary>
        private List<string> mPaths;

        /// <summary>
        /// Stores the archives directory paths where other
        /// resource descriptor files can be looked for.
        /// </summary>
        private List<string> mArchives;

        /// <summary>
        /// Stores the cache that maps archives files to archives content
        /// and last modification time on disk.
        /// </summary>
        private Dictionary<string, Tuple<XElement, DateTime>> mCache;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes the static member(s) of the <see cref="XMLResourceLoader"/> class.
        /// </summary>
        static XMLResourceLoader()
        {
            sResourceDataKeys = new HashSet<string>();
            sResourceDataKeys.Add( "texture1D" );
            sResourceDataKeys.Add( "texture1DArray" );
            sResourceDataKeys.Add( "texture2D" );
            sResourceDataKeys.Add( "texture2DArray" );
            sResourceDataKeys.Add( "texture3D" );
            sResourceDataKeys.Add( "textureCube" );
            sResourceDataKeys.Add( "textureCubeArray" );
            sResourceDataKeys.Add( "textureRectangle" );
            sResourceDataKeys.Add( "module" );
            sResourceDataKeys.Add( "mesh" );
            sResourceDataKeys.Add( "program" );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XMLResourceLoader"/> class.
        /// </summary>
        public XMLResourceLoader()
        {
            this.mPaths = new List<string>();
            this.mArchives = new List<string>();
            this.mCache = new Dictionary<string, Tuple<XElement, DateTime>>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Add a new search path where resource descriptors can be looked for.
        /// </summary>
        /// <param name="pNewPath">The new path loader could look for descriptors</param>
        public void AddPath(string pNewPath)
        {
            this.mPaths.Add( pNewPath );
        }

        /// <summary>
        /// Adds a new archive file where resource descriptors can be looked for.
        /// </summary>
        /// <param name="pArchive">The new archive file name loader could look for descriptors</param>
        public void AddArchive(string pArchive)
        {
            this.mArchives.Add( pArchive );
        }

        #region Methods Internal

        /// <summary>
        /// Checks whether the file is a texture file or not
        /// </summary>
        /// <param name="pName">The file name</param>
        /// <returns>True if it is a texture file, false otherwise.</returns>
        private bool IsTextureFile(string pName)
        {
            int lStartExtensionIndex = pName.Length - 4; // With the dot.
            if ( lStartExtensionIndex > 0 )
            {
                string lExtension = pName.Substring( lStartExtensionIndex );
                if ( string.CompareOrdinal( lExtension, ".jpg" ) == 0 ||
                     string.CompareOrdinal( lExtension, ".png" ) == 0 ||
                     string.CompareOrdinal( lExtension, ".bmp" ) == 0 ||
                     string.CompareOrdinal( lExtension, ".tga" ) == 0 )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Checks whether the file is a mesh file or not.
        /// </summary>
        /// <param name="pName">The file name</param>
        /// <returns>True if it is a mesh file, false otherwise.</returns>
        private bool IsMeshFile(string pName)
        {
            bool lCanBeMeshFile = pName.Length > 5; // At least enough chars for the .mesh extension.
            if ( lCanBeMeshFile )
            {
                string lExpectedExtension = pName.Substring( pName.Length - 5 );
                if ( string.CompareOrdinal( lExpectedExtension, ".mesh" ) == 0 )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Checks whether the file is a module(s) file or not.
        /// </summary>
        /// <param name="pName">The file name</param>
        /// <returns>True if it is a module file, false otherwise.</returns>
        private bool IsModuleFile(string pName)
        {
            // If any ';' in the file name and it means it is 
            // a module file of the form "module1;module;module3;..."
            if ( pName.IndexOf( ';' ) != -1 )
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Looks for the XML part of the resource descriptor of a supplied name in the
        /// archive files first or in directories then.
        /// </summary>
        /// <param name="pName">The file name for which the XML descriptor is required.</param>
        /// <param name="pLastWrite">The last modification time of the resource descriptor</param>
        /// <param name="pLog">The flag indicating whether to log or not on error.</param>
        /// <returns>The XML descriptor if found, null otherwise.</returns>
        private XElement FindDescriptor(string pName, ref DateTime pLastWrite, bool pLog = true)
        {
            foreach ( string lArchiveName in this.mArchives )
            {
                DateTime? lLastWrite;
                XElement lArchive   = this.LoadArchive( lArchiveName, out lLastWrite );
                if ( lLastWrite == null ||
                     lLastWrite.HasValue == false )
                {
                    lLastWrite = pLastWrite;
                }

                if ( lArchive != null )
                {
                    XElement lDescriptor = FindDescriptor( lArchive, pName );
                    if ( lDescriptor != null )
                    {
                        if ( lLastWrite.Value == pLastWrite )
                        {
                            // If the last modification time is equal to the last known
                            // modification time, return null.
                            return null;
                        }
                        else
                        {
                            pLastWrite = lLastWrite.Value;
                            return lDescriptor;
                        }
                    }
                }
            }

            // If nothing returned yet, look into directories then.
            foreach ( string lDirectory in this.mPaths )
            {
                string lPath = Path.Combine( lDirectory, pName + ".xml" );
                if ( File.Exists( lPath ) )
                {
                    DateTime lLastWrite = File.GetLastWriteTime( lPath );
                    if ( lLastWrite == pLastWrite )
                    {
                        // If the last modification time is equal to the last known
                        // modification time, return null.
                        return null;
                    }
                    else
                    {
                        pLastWrite = lLastWrite;
                    }

                    // Load the file.
                    try
                    {
                        XElement lDescriptor = XElement.Load( lPath );

                        LogManager.Instance.Log( string.Format( "Loaded file \"{0}\"", lPath ), LogType.INFO );

                        return lDescriptor;
                    }
                    catch ( Exception pEx )
                    {
                        LogManager.Instance.Log( pEx );
                    }
                } // Skip if simply doesn t exist
            }

            if ( pLog )
            {
                LogManager.Instance.Log( string.Format( "Cannot find the resource \"{0}\"!!!", pName ), LogType.ERROR );
            }

            return null;
        }

        /// <summary>
        /// Looks for the XML part of the resource descriptor of a supplied name in a given
        /// archive file.
        /// </summary>
        /// <param name="pArchive">The archive element in which look for the XML resource descriptor.</param>
        /// <param name="pName">The file name for which the XML descriptor is required.</param>
        /// <returns>The XML descriptor if found, null otherwise.</returns>
        private static XElement FindDescriptor(XElement pArchive, string pName)
        {
            if ( pArchive != null )
            {
                // look for the descriptor in the archive content
                foreach ( XElement lChild in pArchive.Elements() )
                {
                    XAttribute lNameAttr = lChild.Attribute( "name" );
                    if ( lNameAttr != null )
                    {
                        if ( string.CompareOrdinal( lNameAttr.Value, pName ) == 0 )
                        {
                            // Returns a deep clone of that matching element.
                            return new XElement( lChild );
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Helper method building up a texture descriptor object
        /// </summary>
        /// <param name="pName">The texture name</param>
        /// <returns>The texture XML descriptor element</returns>
        private static XElement BuildTextureDescriptor(string pName)
        {
            int lIndex1  = pName.IndexOf( '-' );
            int lIndex2  = pName.IndexOf( '-' , lIndex1 + 1 );
            string lSize = pName.Substring( lIndex1 + 1, lIndex2 - lIndex1 - 1 );
            int lIndex3  = pName.IndexOf( '-' , lIndex2 + 1 );
            string lInternalFormat   = pName.Substring( lIndex2 + 1, lIndex3 - lIndex2 - 1 );

            XElement lTextureElement = new XElement( "texture2D" );
            lTextureElement.SetAttributeValue( "name", pName );
            lTextureElement.SetAttributeValue( "internalformat", lInternalFormat );
            lTextureElement.SetAttributeValue( "width", lSize );
            lTextureElement.SetAttributeValue( "height", lSize );
            lTextureElement.SetAttributeValue( "format", "RED" );
            lTextureElement.SetAttributeValue( "type", "UNSIGNED_BYTE" );
            lTextureElement.SetAttributeValue( "min", "NEAREST" );
            lTextureElement.SetAttributeValue( "mag", "NEAREST" );
            return lTextureElement;
        }

        /// <summary>
        /// Helper method building up a program descriptor object
        /// </summary>
        /// <param name="pName">The program name</param>
        /// <returns>The program XML descriptor element</returns>
        private static XElement BuildProgramDescriptor(string pName)
        {
            XElement lProgramElement = new XElement( "program" );
            lProgramElement.SetAttributeValue( "name", pName );
            int lStart = 0;
            int lIndex = 0;
            while ( (lIndex = pName.IndexOf( ';', lStart )) != -1 )
            {
                string lModule = pName.Substring( lStart, lIndex - lStart );
                XElement lModuleElement = new XElement( "module" );
                lModuleElement.SetAttributeValue( "name", lModule );
                lProgramElement.Add( lModuleElement );
                lStart = lIndex + 1;
            }
            
            return lProgramElement;
        }

        /// <summary>
        /// Loads the archive file of the given name.
        /// </summary>
        /// <param name="pName">The archive file</param>
        /// <param name="pTime"></param>
        /// <returns></returns>
        private XElement LoadArchive(string pName, out DateTime? pTime)
        {
            pTime = null;
            
            Tuple<XElement, DateTime> lArchive;
            if ( this.mCache.TryGetValue( pName, out lArchive ) )
            {
                pTime = lArchive.Item2;

                // If the last modification time is equal to the last time in cache?
                DateTime lLastWrite = File.GetLastWriteTime( pName );
                if ( (lLastWrite - lArchive.Item2).TotalSeconds == 0 )
                {
                    // Return the element in cache.
                    return lArchive.Item1;
                }
            }

            // Else try to load the archive.
            XElement lRoot = null;
            try
            {
                lRoot = XElement.Load( pName );
            }
            catch
            {
                lRoot = null;
            }

            if ( lRoot != null &&
                 lRoot.IsEmpty == false )
            {
                LogManager.Instance.Log( string.Format( "Loaded file \"{0}\".", pName ), LogType.INFO );

                DateTime lLastWrite = File.GetLastWriteTime( pName );

                pTime = lLastWrite;

                // Put the new value into the cache.
                this.mCache[ pName ] = new Tuple<XElement, DateTime>( lRoot, lLastWrite );

                return lRoot;
            }

            LogManager.Instance.Log( string.Format( "File \"{0}\" not found or loading failed!!!", pName ), LogType.ERROR );

            return null;
        }

        /// <summary>
        /// Loads the descriptor either ASCII or binary data.
        /// </summary>
        /// <param name="pElement">The XML element of the descriptor.</param>
        /// <param name="pEncoding">The returned data encoding.</param>
        /// <param name="pAllStamps">The set of modified times of files having the same description as this one.</param>
        /// <returns>The ASCII or Binary part of the descriptor, null if error.</returns>
        private byte[] LoadData(XElement pElement, out Encoding pEncoding, ref List<Tuple<string, DateTime>> pAllStamps)
        {
            string lName = pElement.Name();
            // If any ASCII or binary part?
            if ( sResourceDataKeys.Contains( lName ) )
            {
                string lToReturn = null;
                string lFullPath = null;

                // First, get the name of the file.
                XAttribute lSourceAttr = pElement.Attribute( "source" );
                if ( lSourceAttr == null && // No source file name
                     string.CompareOrdinal( lName, "program" ) == 0 ) // And is program descriptor?
                {
                    lToReturn = pElement.Attribute( "name" ).Value + ".bin";
                    lFullPath = this.FindFile( lToReturn );
                    if ( lFullPath == null )
                    {
                        pEncoding = null;
                        return null;
                    }
                }
                else if ( lSourceAttr == null ) // No source file name ONLY?
                {
                    // If neither a module, nor a mash and that their is a width attribute?
                    if ( string.CompareOrdinal( lName, "module" ) != 0 &&
                         string.CompareOrdinal( lName, "mesh" ) != 0 &&
                         pElement.Attribute( "width" ) != null )
                    {
                        // It is a texture and such elements can have no binary part 
                        // provided and its dimensions being specified in the XML part,
                        // so returns Null straight.
                        pEncoding = null;
                        return null;
                    }

                    // If a module, and no stage described, no source becomes a problem.
                    if ( (string.CompareOrdinal( lName, "module" ) == 0 &&
                         (pElement.Attribute( "vertex" ) != null ||
                          pElement.Attribute( "tessControl" ) != null ||
                          pElement.Attribute( "tessEvaluation" ) != null ||
                          pElement.Attribute( "geometry" ) != null ||
                          pElement.Attribute( "fragment" ) != null) ) == false )
                    {
                        // As a module may not have source attribute, it has to be one of the above
                        // attribute instead, else it is an invalid element.
                        LogManager.Instance.Log( "Missing source attribute" );

                        pEncoding = null;
                        return null;
                    }
                }

                // Then, test whether the modification time changed or not.
                // And if not, do not load yet.
                bool lNeedToLoad = false;
                if ( pAllStamps.Count == 0 ) // If not loaded yet??
                {
                    lNeedToLoad = true;
                }
                else
                {
                    // Else, load it only the file has changed by checking its modification time.
                    foreach ( Tuple<string, DateTime> lStamp in pAllStamps )
                    {
                        DateTime lLastWrite = File.GetLastWriteTime( lStamp.Item1 );
                        if ( (lLastWrite - lStamp.Item2).TotalSeconds != 0 )
                        {
                            lNeedToLoad = true;
                            break;
                        }
                    }
                }

                if ( lNeedToLoad == false )
                {
                    pEncoding = null;
                    return null;
                }

                StringBuilder lDataBuilder = new StringBuilder();
                // Support special cases for modules made of separated files.
                if ( string.CompareOrdinal( lName, "module" ) == 0 &&
                     lSourceAttr == null )
                {
                    pAllStamps.Clear();
                    XAttribute lVertexAttr = null;
                    if ( (lVertexAttr = pElement.Attribute( "vertex" )) != null )
                    {
                        Encoding lVertexEncoding;
                        string lVertexPath = this.FindFile( lVertexAttr.Value );
                        byte[] lBaseData   = this.LoadFile( lVertexPath, out lVertexEncoding );
                        string lVertexSource = lVertexEncoding.GetString( this.LoadShaderData( pElement, lVertexPath, lBaseData, lVertexEncoding, ref pAllStamps ) );
                        if ( lVertexSource.Contains( "_VERTEX_" ) == false )
                        {
                            // If the user forgot the stage definition using #ifdef _VERTEX_,
                            // add it for that user.
                            lDataBuilder.AppendLine( "#ifdef _VERTEX_" );
                            lDataBuilder.AppendLine( lVertexSource );
                            lDataBuilder.AppendLine( "#endif" );
                        }
                        else
                        {
                            lDataBuilder.Append( lVertexSource );
                        }
                    }

                    XAttribute lTesselationControlAttr = null;
                    if ( (lTesselationControlAttr = pElement.Attribute( "tessControl" )) != null )
                    {
                        Encoding lTesselationControlEncoding;
                        string lTesselationControlPath = this.FindFile( lTesselationControlAttr.Value );
                        byte[] lBaseData               = this.LoadFile( lTesselationControlPath, out lTesselationControlEncoding );
                        string lTessControlSource = lTesselationControlEncoding.GetString( this.LoadShaderData( pElement, lTesselationControlPath, lBaseData, lTesselationControlEncoding, ref pAllStamps ) );
                        if ( lTessControlSource.Contains( "_TESS_CONTROL_" ) == false )
                        {
                            // If the user forgot the stage definition using #ifdef _TESS_CONTROL_,
                            // add it for that user.
                            lDataBuilder.AppendLine( "#ifdef _TESS_CONTROL_" );
                            lDataBuilder.AppendLine( lTessControlSource );
                            lDataBuilder.AppendLine( "#endif" );
                        }
                        else
                        {
                            lDataBuilder.Append( lTessControlSource );
                        }
                    }

                    XAttribute lTesselationEvalAttr = null;
                    if ( (lTesselationEvalAttr = pElement.Attribute( "tessEvaluation" ) ) != null )
                    {
                        Encoding lTesselationEvalEncoding;
                        string lTesselationEvalPath = this.FindFile( lTesselationEvalAttr.Value );
                        byte[] lBaseData            = this.LoadFile( lTesselationEvalPath, out lTesselationEvalEncoding );
                        string lTessEvalSource = lTesselationEvalEncoding.GetString( this.LoadShaderData( pElement, lTesselationEvalPath, lBaseData, lTesselationEvalEncoding, ref pAllStamps ) );
                        if ( lTessEvalSource.Contains( "_TESS_EVAL_" ) == false )
                        {
                            // If the user forgot the stage definition using #ifdef _TESS_EVAL_,
                            // add it for that user.
                            lDataBuilder.AppendLine( "#ifdef _TESS_EVAL_" );
                            lDataBuilder.AppendLine( lTessEvalSource );
                            lDataBuilder.AppendLine( "#endif" );
                        }
                        else
                        {
                            lDataBuilder.Append( lTessEvalSource );
                        }
                    }

                    XAttribute lGeometryAttr = null;
                    if ( (lGeometryAttr = pElement.Attribute( "geometry" ) ) != null )
                    {
                        Encoding lGeometryEncoding;
                        string lGeometryPath = this.FindFile( lGeometryAttr.Value );
                        byte[] lBaseData     = this.LoadFile( lGeometryPath, out lGeometryEncoding );
                        string lGeometrySource = lGeometryEncoding.GetString( this.LoadShaderData( pElement, lGeometryPath, lBaseData, lGeometryEncoding, ref pAllStamps ) );
                        if ( lGeometrySource.Contains( "_GEOMETRY_" ) == false )
                        {
                            // If the user forgot the stage definition using #ifdef _GEOMETRY_,
                            // add it for that user.
                            lDataBuilder.AppendLine( "#ifdef _GEOMETRY_" );
                            lDataBuilder.AppendLine( lGeometrySource );
                            lDataBuilder.AppendLine( "#endif" );
                        }
                        else
                        {
                            lDataBuilder.Append( lGeometrySource );
                        }
                    }

                    XAttribute lFragmentAttr = null;
                    if ( (lFragmentAttr = pElement.Attribute( "fragment" ) ) != null )
                    {
                        Encoding lFragmentEncoding;
                        string lFragmentPath = this.FindFile( lFragmentAttr.Value );
                        byte[] lBaseData     = this.LoadFile( lFragmentPath, out lFragmentEncoding );
                        string lFragmentSource = lFragmentEncoding.GetString( this.LoadShaderData( pElement, lFragmentPath, lBaseData, lFragmentEncoding, ref pAllStamps ) );
                        if ( lFragmentSource.Contains( "_FRAGMENT_" ) == false )
                        {
                            // If the user forgot the stage definition using #ifdef _FRAGMENT_,
                            // add it for that user.
                            lDataBuilder.AppendLine( "#ifdef _FRAGMENT_" );
                            lDataBuilder.AppendLine( lFragmentSource );
                            lDataBuilder.AppendLine( "#endif" );
                        }
                        else
                        {
                            lDataBuilder.Append( lFragmentSource );
                        }
                    }

                    pEncoding = Encoding.Unicode;
                    return Encoding.Unicode.GetBytes( lDataBuilder.ToString() );
                }

                string lPath = null;
                if ( pAllStamps.Count == 0 )
                {
                    lPath = this.FindFile( lSourceAttr.Value );
                }
                else
                {
                    lPath = pAllStamps[ 0 ].Item1;
                }

                Encoding lEncoding;
                byte[] lData = this.LoadFile( lPath, out lEncoding );
                pEncoding = lEncoding;
                pAllStamps.Clear();

                // And then process it depending on the type of the resource
                if ( string.CompareOrdinal( lName, "module" ) == 0 )
                {
                    // for a shader resource the ASCII part can reference other files
                    // via #include directives; we need to load them and to substitute
                    // their content
                    return this.LoadShaderData( pElement, lPath, lData, lEncoding, ref pAllStamps );
                }
                else if ( string.CompareOrdinal( lName, "mesh" ) == 0 ||
                          string.CompareOrdinal( lName, "program" ) == 0 )
                {
                    // for a mesh or compiled program resource, no processing is needed
                    DateTime lLastWrite = File.GetLastWriteTime( lPath );
                    pAllStamps.Add( new Tuple<string, DateTime>( lPath, lLastWrite ) );
                    return lData;
                }
                else
                {
                    // For a texture, decompress the file (PNG, JPG, etc)
                    return this.LoadTextureData( pElement, lPath, lData, pAllStamps );
                }
            }

            pEncoding = null;
            return null;
        }

        /// <summary>
        /// Loads shader data from file using the path
        /// </summary>
        /// <param name="pElement">The XML description element</param>
        /// <param name="pShaderPath">The shader full path</param>
        /// <param name="pData">The shader loaded data.</param>
        /// <param name="pDataEncoding">The data encoding (Use FileExtensions.GetEncoding to find out)</param>
        /// <param name="pAllStamps">The set of modification times of files having a same description.</param>
        /// <returns></returns>
        private byte[] LoadShaderData(XElement pElement, string pShaderPath, byte[] pData, Encoding pDataEncoding, ref List<Tuple<string, DateTime>> pAllStamps)
        {
            DateTime lLastWrite = File.GetLastWriteTime( pShaderPath );
            pAllStamps.Add( new Tuple<string, DateTime>( pShaderPath, lLastWrite ) );
            
            string lDataStr = pDataEncoding.GetString( pData );
            if ( lDataStr.Contains( "#include" ) == false )
            {
                // If shader has no include, return it straight.
                return pData;
            }

            // Else, load the referenced file(s)
            StringBuilder lResult = new StringBuilder();
            bool lIsComment = false;
            bool lIsLineComment = false;
            int lCurrent = 0;
            int lDataSize = lDataStr.Length;
            while ( lCurrent < lDataSize )
            {
                if ( lIsComment == false )
                { 
                    // Is inside a comment?
                    if ( lCurrent + 1 < lDataSize )
                    {
                        if ( lDataStr[ lCurrent ] == '/' && 
                             lDataStr[ lCurrent + 1 ] == '*' )
                        {
                            // if a comment start '/*' it is a comment
                            lResult.Append( lDataStr, lCurrent, 2 );
                            lIsComment = true;
                            lIsLineComment = false;
                            lCurrent += 2;
                            continue;
                        }
                        else if ( lDataStr[ lCurrent ] == '/' && 
                                  lDataStr[ lCurrent + 1 ] == '/' )
                        {
                            // Second comment style supported : '//'
                            lResult.Append( lDataStr, lCurrent, 2 );
                            lIsComment = true;
                            lIsLineComment = true;
                            lCurrent += 2;
                            continue;
                        }
                    }

                    if ( lCurrent + 8 <= lDataSize && 
                         string.CompareOrdinal( lDataStr, lCurrent, "#include", 0, 8 ) == 0 )
                    {
                        // If a #include is found
                        int lStartIncludeName = lDataStr.IndexOf( '\"', lCurrent );
                        if ( lStartIncludeName != -1 )
                        {
                            // Find the end of the include
                            int lEndIncludeName = lDataStr.IndexOf( '\"', lStartIncludeName + 1 );
                            if ( lEndIncludeName != -1 )
                            {
                                // First extracts the referenced file name
                                string lIncludesFileName = lDataStr.Substring( lStartIncludeName + 1, lEndIncludeName - lStartIncludeName - 1 );
                                string lIncludedFileFullName;

                                // Then find the absolute name of this file
                                lIncludedFileFullName = this.FindFile( lIncludesFileName );
                                if ( lIncludedFileFullName == null )
                                {
                                    LogManager.Instance.Log( string.Format( "Cannot find the included shader file \"{0}\"!!!", lIncludedFileFullName ), LogType.ERROR );
                                    continue;
                                }

                                Encoding lIncludedEncoding;
                                // Then load the content of the referenced file
                                byte[] lIncludedData   = this.LoadFile( lIncludedFileFullName, out lIncludedEncoding );

                                // Then analyze its content with a recursive call
                                // to process the #include directives that this file may
                                // in turn contain
                                byte[] lIncludedShader = this.LoadShaderData( pElement, lIncludedFileFullName, lIncludedData, lIncludedEncoding, ref pAllStamps );
                                
                                // Finally append this included shader content to the
                                // resulting data, instead of the #include directive itself
                                lResult.Append( lIncludedEncoding.GetString( lIncludedShader ) );

                                lCurrent = lEndIncludeName + 1;
                                continue;
                            }
                        }
                    }
                }
                else
                {
                    if ( lIsLineComment )
                    { 
                        // if in a line comment
                        if ( lDataStr[ lCurrent ] == '\n' )
                        { 
                            // and find a newline we exit the comment
                            lResult.Append( lDataStr, lCurrent++, 1 );
                            lIsComment = false;
                            continue;
                        }
                    }
                    else if ( lCurrent + 1 < lDataSize && 
                              lDataStr[ lCurrent ] == '*' && 
                              lDataStr[ lCurrent + 1 ] == '/' )
                    {
                        // likewise, if we find a '*/' in a block comment, we exit it
                        lResult.Append( lDataStr, lCurrent, 2 );
                        lIsComment = false;
                        lCurrent += 2;
                        continue;
                    }
                }

                lResult.Append( lDataStr, lCurrent++, 1 );
            }
            
            // Refresh the pData
            byte[] lNewData = pDataEncoding.GetBytes( lResult.ToString() );

            return lNewData;
        }

        /// <summary>
        /// Decompress and refresh texture data from file using path.
        /// </summary>
        /// <param name="pElement">The XML description element</param>
        /// <param name="pTexturePath">The texture full path</param>
        /// <param name="pData">The texture loaded data.</param>
        /// <param name="pAllStamps">The set of modification times of files having a same description.</param>
        /// <returns></returns>
        private byte[] LoadTextureData(XElement pElement, string pTexturePath, byte[] pData, List<Tuple<string, DateTime>> pAllStamps)
        {
            int lWidth     = 0;
            int lHeight    = 0;
            int lChannels  = 0;
            byte[] lResult = null;
            IImageService lImageService = ServiceManager.Instance.GetService<IImageService>();
            if ( lImageService != null )
            {
                //lResult = lImageService.Load( pData, out lWidth, out lHeight, out lChannels );
                lResult = lImageService.Load( pTexturePath, out lWidth, out lHeight, out lChannels );
            }

            if ( lResult == null )
            {
                LogManager.Instance.Log( string.Format( "Cannot load texture file \"{0}\"!!!", pTexturePath ) );
                return null;
            }

            pElement.SetAttributeValue( "width", lWidth );
            if ( pElement.Attribute( "height" ) == null )
            {
                pElement.SetAttributeValue( "height", lHeight );
            }

            switch ( lChannels )
            {
                case 1:
                    {
                        if ( pElement.Attribute( "format" ) == null )
                        {
                            pElement.SetAttributeValue( "format", "RED" );
                        }
                    }
                    break;
                case 2:
                    {
                        if ( pElement.Attribute( "format" ) == null )
                        {
                            pElement.SetAttributeValue( "format", "RG" );
                        }
                    }
                    break;
                case 3:
                    {
                        if ( pElement.Attribute( "format" ) == null )
                        {
                            pElement.SetAttributeValue( "format", "RGB" );
                        }
                    }
                    break;
                case 4:
                    {
                        if ( pElement.Attribute( "format" ) == null )
                        {
                            pElement.SetAttributeValue( "format", "RGBA" );
                        }
                    }
                    break;
            }
            
            pElement.SetAttributeValue( "type", "UNSIGNED_BYTE" );
            
            DateTime lLastWrite = File.GetLastWriteTime( pTexturePath );
            pAllStamps.Add( new Tuple<string, DateTime>( pTexturePath, lLastWrite ) );
            
            return lResult;
        }

        /// <summary>
        /// Retrieves the supplied path in a set of directories.
        /// </summary>
        /// <param name="pFileName">The file name (relative?)</param>
        /// <returns>The absolute file name, or Null of not found.</returns>
        protected string FindFile(string pFileName)
        {
            foreach ( string lPath in this.mPaths )
            {
                string lFullPath = Path.Combine( lPath, pFileName );
                if ( File.Exists( lFullPath ) )
                {
                    return lFullPath;
                }
            }

            LogManager.Instance.Log( string.Format( "Cannot find the \"{0}\" file!!!", pFileName ), LogType.ERROR );

            return null;
        }

        /// <summary>
        /// Loads the content of a file in binary format.
        /// </summary>
        /// <param name="pName">The file name.</param>
        /// <param name="pFileEncoding">The file encoding.</param>
        /// <returns>The set of unsigned bytes</returns>
        protected byte[] LoadFile(string pName, out Encoding pFileEncoding)
        {
            try
            {
                byte[] lContent = File.ReadAllBytes( pName );
                pFileEncoding = FileExtensions.GetEncoding( pName );
                return lContent;
            }
            catch ( Exception pEx)
            {
                LogManager.Instance.Log( pEx );
                pFileEncoding = null;
                return null;
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Releases resources
        /// </summary>
        protected virtual void OnDispose()
        {
            this.mPaths.Clear();
            this.mCache.Clear();
            this.mArchives.Clear();
        }

        #endregion Methods IDisposable

        #region Methods IResourceLoader

        /// <summary>
        /// Finds a resource by its name.
        /// </summary>
        /// <param name="pName"></param>
        /// <returns>The fullpath of the resource, null otherwise.</returns>
        public string FindResource(string pName)
        {
            return this.FindFile( pName );
        }

        /// <summary>
        /// Loads a resource description, that is, its info and content.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <returns>The resource description, Null otherwise.</returns>
        public AResourceDescriptor LoadResource(string pName)
        {
            DateTime lLastWrite = new DateTime( 0 );
            XElement lDescriptor = null;
            if ( string.CompareOrdinal( pName, 0, "renderbuffer", 0, 12 ) == 0 )
            {
                // Resource names of the form "renderbuffer-X-Y" describe texture
                // resources that are not described by any file, either for the XML part
                // or for the binary part. The XML part is generated from the resource
                // name, and the binary part is NULL
                lDescriptor = BuildTextureDescriptor( pName );
            }
            else if ( this.IsTextureFile( pName ) )
            {
                // 2D texture resources can be loaded directly from an image file; the
                // texture parameters (internal format, filters, etc) then get default values
                lDescriptor = new XElement( "texture2D" );
                lDescriptor.SetAttributeValue( "name", pName );
                lDescriptor.SetAttributeValue( "source", pName );
                lDescriptor.SetAttributeValue( "internalformat", "RGBA8" );
                lDescriptor.SetAttributeValue( "min", "LINEAR_MIPMAP_LINEAR" );
                lDescriptor.SetAttributeValue( "mag", "LINEAR" );
                lDescriptor.SetAttributeValue( "wraps", "REPEAT" );
                lDescriptor.SetAttributeValue( "wrapt", "REPEAT" );
            }
            else if ( this.IsModuleFile( pName ) )
            {
                // Resource names of the form "module1;module;module3;..." describe
                // program resources that may not be described by any file, either for the
                // XML part or for the binary part. The XML part is generated from the
                // resource name, and the binary part is NULL (unless a compiled program
                // exists for this program)
                lDescriptor = this.FindDescriptor( pName, ref lLastWrite, false );
                if ( lDescriptor == null )
                {
                    lDescriptor = BuildProgramDescriptor( pName );
                }
            }
            else if ( this.IsMeshFile( pName ) )
            {
                lDescriptor = new XElement( "mesh" );
                lDescriptor.SetAttributeValue( "source", pName );
            }
            else
            {
                // Last chance.
                lDescriptor = this.FindDescriptor( pName, ref lLastWrite );
            }

            if ( lDescriptor != null )
            {
                Encoding lEncoding;
                List<Tuple<string, DateTime>> lAllStamps = new List<Tuple<string, DateTime>>();
                byte[] lData = this.LoadData( lDescriptor, out lEncoding, ref lAllStamps );
                return DescriptorFactory.Instance.CreateDescriptor( lDescriptor.Name(), lDescriptor, lData, lEncoding, lLastWrite, lAllStamps ) as AResourceDescriptor;
            }

            return null;
        }

        /// <summary>
        /// Loads again a resource description.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pCurrentDescription">The current resource description.</param>
        /// <returns>The resource description, Null otherwise.</returns>
        public virtual AResourceDescriptor ReloadResource(string pName, AResourceDescriptor pCurrentDescription)
        {
            DateTime lLastWrite = pCurrentDescription.LastWrite;

            XElement lDescriptor = null;
            if ( string.CompareOrdinal( pName, 0, "renderbuffer", 0, 12 ) != 0 &&
                 this.IsTextureFile( pName ) == false &&
                 this.IsModuleFile( pName ) == false &&
                 string.CompareOrdinal( pCurrentDescription.Descriptor.Name(), "mesh" ) != 0 )
            {
                // Resources whose XML part is described in a file (see LoadResource)
                // First test if the XML part has changed or not. If it has changed
                // the descriptor contains the new value, and stamp the new last modification time
                lDescriptor = this.FindDescriptor( pName, ref lLastWrite );
            }

            if ( lDescriptor == null )
            {
                // Create a deep clone.
                lDescriptor = new XElement( pCurrentDescription.Descriptor );
            }

            List<Tuple<string, DateTime>> lAllStamps = pCurrentDescription.AllStamps;
            if ( (pCurrentDescription.LastWrite - lLastWrite).TotalSeconds != 0 )
            {
                // If the XML part has changed the files describing the binary part may
                // no longer be the same, so we clear the corresponding modification
                // time vector to force a reloading of the binary part.
                lAllStamps.Clear();
            }

            Encoding lEncoding;
            byte[] lData = this.LoadData( lDescriptor, out lEncoding, ref lAllStamps );
            if ( pCurrentDescription.Equals( lDescriptor, lLastWrite, lAllStamps ) == false )
            {
                return DescriptorFactory.Instance.CreateDescriptor( lDescriptor.Name(), lDescriptor, lData, lEncoding, lLastWrite, lAllStamps ) as AResourceDescriptor;
            }

            return null;
        }

        #endregion Methods IResourceLoader

        #region Methods ToString

        /// <summary>
        /// Turns this resource loader into a string.
        /// </summary>
        /// <returns>The string.</returns>
        public override string ToString()
        {
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.Append( "Archives : " );
            bool lAnyArchive = false;
            foreach ( string lPath in this.mArchives )
            {
                lBuilder.AppendLine( lPath );
                lAnyArchive = true;
            }

            if ( lAnyArchive == false )
            {
                lBuilder.AppendLine( "0 Archives..." );
            }
            
            lBuilder.Append( "Paths : " );
            bool lAnyPath = false;
            foreach ( string lPath in this.mPaths )
            {
                lBuilder.AppendLine( lPath );
                lAnyPath = true;
            }

            if ( lAnyPath == false )
            {
                lBuilder.AppendLine( "0 Paths..." );
            }

            return lBuilder.ToString();
        }

        #endregion Methods ToString

        #endregion Methods
    }
}
