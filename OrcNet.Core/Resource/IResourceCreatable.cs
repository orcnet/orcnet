﻿using System;

namespace OrcNet.Core.Resource
{
    /// <summary>
    /// Base resource creatable interface exposing
    /// a handle to the resource that created the creatable.
    /// </summary>
    public interface IResourceCreatable : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        IResource Creator
        {
            get;
            set;
        }

        #endregion Properties
    }
}
