﻿using System;

namespace OrcNet.Core.Resource
{
    /// <summary>
    /// Base resource interface definition in charge of
    /// creating a creatable resource object.
    /// </summary>
    public interface IResource : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the resource owned object.
        /// </summary>
        IResourceCreatable OwnedObject
        {
            get;
        }

        /// <summary>
        /// Gets the resource's name.
        /// </summary>
        string Name
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the resource has changed or not.
        /// </summary>
        bool IsModified
        {
            get;
        }

        /// <summary>
        /// Gets the update order of that resource.
        /// In order to be up to date properly, such a resource must wait for
        /// parent resource(s) to be up to date first.
        /// (e.g: A GLSL program must wait for its shaders to be updated which
        /// themselves need texture(s) to be ready and so on.
        /// </summary>
        int UpdateOrder
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Prepares the resource update by checking whether the update can be successful or not. 
        /// NOTE: Update is done in two phases =>
        /// first testing if the resource hierarchy can be updated successfully or not,
        /// and second updating all resources. Otherwise nothing will be updated at all.
        /// </summary>
        /// <param name="pLoader">The loader to use to prepare the resource.</param>
        /// <returns>True if the resource passed the first phase and can be updated for sure.</returns>
        bool PreUpdate(IResourceLoader pLoader);

        /// <summary>
        /// Updates the resource.
        /// </summary>
        /// <param name="pIsValid">The flag indicating whether the resource update can be done or not. Else revert preparation work.</param>
        void Update(bool pIsValid);

        #endregion Methods
    }
}
