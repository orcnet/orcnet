﻿using System;

namespace OrcNet.Core.Resource
{
    /// <summary>
    /// Resource loader interface definition.
    /// </summary>
    public interface IResourceLoader : IDisposable
    {
        #region Methods

        /// <summary>
        /// Finds a resource by its name.
        /// </summary>
        /// <param name="pName"></param>
        /// <returns>The fullpath of the resource, null otherwise.</returns>
        string FindResource(string pName);

        /// <summary>
        /// Loads a resource description, that is, its info and content.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <returns>The resource description, Null otherwise.</returns>
        AResourceDescriptor LoadResource(string pName);

        /// <summary>
        /// Loads again a resource description.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pCurrentDescription">The current resource description.</param>
        /// <returns>The resource description, Null otherwise.</returns>
        AResourceDescriptor ReloadResource(string pName, AResourceDescriptor pCurrentDescription);

        #endregion Methods
    }
}
