﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource.Factories;
using OrcNet.Core.Resource.Loaders;
using OrcNet.Core.Service;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace OrcNet.Core.Resource
{
    /// <summary>
    /// Resource manager class definition owning all the used
    /// resources whatever it is a texture, shader program and so on..
    /// </summary>
    public class ResourceManager : AService, IResourceService
    {
        #region Fields
        
        /// <summary>
        /// Stores the maximum unused resource count for the LRU cache.
        /// </summary>
        private uint mMaxUnusedCount;

        /// <summary>
        /// Stores the resource loader used to get resource description.
        /// </summary>
        private IResourceLoader mLoader;
        
        /// <summary>
        /// Stores the unused resources sorted by date of last use in the LRU cache.
        /// </summary>
        private List<IResource> mUnusedResources;

        /// <summary>
        /// Stores the set of resources by name. Each resource being associated with its update order.
        /// </summary>
        private Dictionary<string, Tuple<int, IResource>> mResources;

        #endregion Fields

        #region Properties

        #region Properties IService

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "ResourceManager";
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties IService

        /// <summary>
        /// Gets the resource loader used to get resource description.
        /// </summary>
        public IResourceLoader Loader
        {
            get
            {
                return this.mLoader;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceManager"/> class.
        /// </summary>
        public ResourceManager()
        {
            this.mLoader = new XMLResourceLoader();
            this.mMaxUnusedCount  = 0;
            this.mUnusedResources = new List<IResource>();
            this.mResources = new Dictionary<string, Tuple<int, IResource>>();
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Invalidates a resource by setting it first as unused resource
        /// and removing it if not in cache or as pending unused until it
        /// reaches the maximum unused cache size and getting the oldest.
        /// </summary>
        /// <param name="pToInvalidate">The resource to invalidate.</param>
        public void InvalidateResource(IResource pToInvalidate)
        {
            if
                ( this.mMaxUnusedCount > 0 )
            {
                Tuple<int, IResource> lResource = null;
                if
                    ( this.mResources.TryGetValue( pToInvalidate.Name, out lResource ) == false )
                {
                    // Put it into the unused cache but check if not going beyond
                    // cache allowed size and remove the oldest if so.
                    if
                        ( this.mUnusedResources.Count == this.mMaxUnusedCount )
                    {
                        this.mUnusedResources.Remove( this.mUnusedResources[ 0 ] );
                    }

                    this.mUnusedResources.Add( pToInvalidate );
                }
                else
                {
                    // If not in the resource cache, it means it is either an external
                    // unknown resource, or a resource we attempted twice to kill, so 
                    // remove it.
                    this.RemoveResource( pToInvalidate );
                }
            }
            else
            {
                // If no cache of unused resource(s) allowed,
                // remove it straight.
                this.RemoveResource( pToInvalidate );
            }
        }

        /// <summary>
        /// Removes a resource from the service.
        /// </summary>
        /// <param name="pToRemove">The resource to remove.</param>
        private void RemoveResource(IResource pToRemove)
        {
            // Removes this resource from the resources map
            if
                ( this.mResources.Remove( pToRemove.Name ) )
            {
                LogManager.Instance.Log( string.Format( "Removed the resource \"{0}\"!!!", pToRemove.Name ) );
            }
        }

        #endregion Methods Internal

        #region Methods IResourceService

        /// <summary>
        /// Modifies the service resource loader and maximum allowed unused resource count.
        /// </summary>
        /// <param name="pLoader">The resource loader used to get resource description.</param>
        /// <param name="pMaxUnusedCount">The maximum unused resource count for LRU cache</param>
        public void ModifyLoader(IResourceLoader pLoader, uint pMaxUnusedCount = 0)
        {
            this.mLoader = pLoader;
            this.mMaxUnusedCount = pMaxUnusedCount;

            // Just show to the user the loader info that will be used.
            LogManager.Instance.Log( pLoader.ToString(), LogType.INFO );
        }

        /// <summary>
        /// Loads a resource by its name.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The resource, Null otherwise.</returns>
        public IResource LoadResource(string pName)
        {
            Tuple<int, IResource> lResource = null;
            if ( this.mResources.TryGetValue( pName, out lResource ) )
            {
                // If already loaded, check if in unused resource list first.
                // Remove it from unused resource cache as it will be reused.
                bool lResult = this.mUnusedResources.Remove( lResource.Item2 );

                // Return the resource.
                return lResource.Item2;
            }

            // Else load the resource.
            LogManager.Instance.Log( string.Format( "Loading the resource \"{0}\"...", pName ) );

            IResource lNewResource = null;
            AResourceDescriptor lDescriptor = this.mLoader.LoadResource( pName );
            if ( lDescriptor != null )
            {
                // Create the resource from the description.
                lNewResource = lDescriptor.Create( pName );

                // If creation successful, register it.
                if ( lNewResource != null )
                {
                    this.mResources[ pName ] = new Tuple<int, IResource>( lNewResource.UpdateOrder, lNewResource );
                    return lNewResource;
                }
            }

            LogManager.Instance.Log( string.Format( "Missing or invalid resource \"{0}\"!!!", pName ) );

            return null;
        }

        /// <summary>
        /// Loads a resource by its Xml descriptor.
        /// </summary>
        /// <param name="pDescriptor">The descriptor Xml element.</param>
        /// <returns>The resource, Null otherwise.</returns>
        public IResource LoadResource(XElement pDescriptor)
        {
            string lName;
            XAttribute lXName = pDescriptor.Attribute( "name" );
            if ( lXName == null )
            {
                // Build a default one.
                lName = string.Format( "{0}{1}", pDescriptor.Name(), this.mResources.Count );
            }
            else
            {
                lName = lXName.Value;
            }

            AResourceDescriptor lDescriptor = DescriptorFactory.Instance.CreateDescriptor( pDescriptor.Name(), 
                                                                                           pDescriptor, 
                                                                                           null, 
                                                                                           System.Text.Encoding.ASCII, 
                                                                                           DateTime.Now, 
                                                                                           new List<Tuple<string, DateTime>>() ) as AResourceDescriptor;
            if ( lDescriptor != null )
            {
                IResource lResource = lDescriptor.Create( lName );
                if ( lResource != null )
                {
                    this.mResources.Add( lName, new Tuple<int, IResource>( lResource.UpdateOrder, lResource ) );
                    return lResource;
                }
            }
            else
            {
                LogManager.Instance.Log( string.Format( "Missing or invalid resource {0}!!!", pDescriptor.Name ), LogType.ERROR );
            }

            return null;
        }

        /// <summary>
        /// Updates the loaded resources if any have changed.
        /// This is an atomic process, either every single resources are updated
        /// else nothing at all.
        /// </summary>
        /// <returns>The flag indicating whether the update has been successful or not.</returns>
        public bool UpdateResources()
        {
            LogManager.Instance.Log( "Updating resource(s)..." );

            bool lIsValid = true;
            // First step, prepare resource and check if all succeed as real update
            // will occur only if all resource(s) are valid.
            foreach ( Tuple<int, IResource> lResource in this.mResources.Values )
            {
                lIsValid &= lResource.Item2.PreUpdate( this.mLoader );
            }

            // The second step either update if valid, or revert if something
            // goes wrong to invalidate previous resource(s) changes.
            foreach ( Tuple<int, IResource> lResource in this.mResources.Values )
            {
                lResource.Item2.Update( lIsValid );
            }

            if ( lIsValid == false )
            {
                LogManager.Instance.Log( "Resource(s) update FAILED!!!", LogType.ERROR );
            }

            LogManager.Instance.Log( string.Format( "{0} resource(s) used.\n{1} unused resource(s).", this.mResources.Count, this.mUnusedResources.Count ) );

            return lIsValid;
        }

        /// <summary>
        /// Closes the resource service by disabling allowed resource cache.
        /// </summary>
        public void Close()
        {
            this.mMaxUnusedCount = 0;
        }

        #endregion Methods IResourceService

        #region Methods IDisposable

        /// <summary>
        /// Releases manager resources.
        /// </summary>
        protected override void OnDispose()
        {
            if ( this.mLoader != null )
            {
                this.mLoader.Dispose();
            }

            foreach ( Tuple<int, IResource> lResource in this.mResources.Values )
            {
                lResource.Item2.Dispose();
            }
            this.mResources.Clear();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
