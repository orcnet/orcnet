﻿namespace OrcNet.Core.Resource
{
    /// <summary>
    /// Resource factory base interface definition.
    /// </summary>
    public interface IResourceFactory
    {
        #region Properties

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        string FactoryType
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        IResource Create(string pName);

        #endregion Methods
    }
}
