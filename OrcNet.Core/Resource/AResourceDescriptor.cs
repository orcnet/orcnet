﻿using OrcNet.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Core.Resource
{
    /// <summary>
    /// Resource descriptor class definition containing data to build
    /// a concret resource. It is made of a XML element and an optional
    /// ASCII or Binary data section.
    /// E.g: -For a texture, the XML part is the texture options such as Format, filter, LOD and so on.
    /// while the binary data will contain the texture data itself.
    ///      -For a shader, the XML part is the default value(s) of uniform(s)
    /// while the binary data will contain the shader source code to compile.
    /// So on...
    /// </summary>
    public abstract class AResourceDescriptor : AMemoryProfilable, IResourceFactory, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the constant XML tag.
        /// </summary>
        protected const string cNameTag = "name";

        /// <summary>
        /// Stores the resource's name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the resource content.
        /// </summary>
        private byte[] mContent;

        /// <summary>
        /// Stores the encoding used to create the byte array.
        /// </summary>
        private Encoding mEncoding;

        /// <summary>
        /// Stores the XML description part whose tag is the type of the resource.
        /// E.g: Texture1D, Texture2D, Shader, Program, Mesh, so on...
        /// </summary>
        private readonly XElement mDescriptor;

        /// <summary>
        /// Stores the last modification time this description comes from.
        /// </summary>
        private readonly DateTime mLastSTamp;

        /// <summary>
        /// Stores the set of all modification times of all files having such descriptions.
        /// </summary>
        private readonly List<Tuple<string, DateTime>> mAllStamps;

        #endregion Fields

        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public abstract string FactoryType
        {
            get;
        }

        #endregion Properties IResourceFactory

        /// <summary>
        /// Gets the resource's name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                if
                    ( this.mContent != null )
                {
                    lSize += (uint)(this.mContent.Length * sizeof(byte));
                }
                lSize += (uint)(cNameTag.Length * sizeof(char));
                lSize += (uint)(this.Name.Length * sizeof(char));
                // Roughly give a size for the set and a datetime
                uint lDateTimeSize = sizeof(byte) * 8; // Saw in a doc...
                lSize += lDateTimeSize;
                uint lAllStampsStrSize = (uint)this.mAllStamps.Count * (256 * sizeof(char)); // Worst case string is 256 max.
                uint lAllStampsDTSize  = (uint)this.mAllStamps.Count * lDateTimeSize;
                lSize += lAllStampsStrSize;
                lSize += lAllStampsDTSize;

                return lSize;
            }
        }

        /// <summary>
        /// Gets the resource descriptor.
        /// </summary>
        public XElement Descriptor
        {
            get
            {
                return this.mDescriptor;
            }
        }

        /// <summary>
        /// Gets the encoding used to create the byte array.
        /// </summary>
        public Encoding Encoding
        {
            get
            {
                return this.mEncoding;
            }
        }

        /// <summary>
        /// Gets the resource content.
        /// </summary>
        public byte[] Content
        {
            get
            {
                return this.mContent;
            }
        }

        /// <summary>
        /// Gets the last mofification time of the resource related file.
        /// </summary>
        internal DateTime LastWrite
        {
            get
            {
                return this.mLastSTamp;
            }
        }

        /// <summary>
        /// Gets the set of all modification times of files having a same descriptor.
        /// </summary>
        internal List<Tuple<string, DateTime>> AllStamps
        {
            get
            {
                // Copy of.
                return new List<Tuple<string, DateTime>>( this.mAllStamps );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AResourceDescriptor"/> class.
        /// </summary>
        protected AResourceDescriptor()
        {
            this.mName = string.Empty;
            this.mAllStamps = new List<Tuple<string, DateTime>>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AResourceDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        protected AResourceDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps)
        {
            XAttribute lNameAttr = pDescriptor.Attribute( cNameTag );
            if
                ( lNameAttr != null )
            {
                this.mName = lNameAttr.Value;
            }
            else
            {
                this.mName = string.Empty;
            }

            this.mDescriptor = pDescriptor;
            this.mContent    = pContent;
            this.mEncoding   = pEncoding;
            this.mLastSTamp  = pStamp;
            this.mAllStamps  = pAllStamps;
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Checks whether two descriptions are equals or not.
        /// </summary>
        /// <param name="pElement">The other XML description part</param>
        /// <param name="pStamp">The other last modification time.</param>
        /// <param name="pAllStamps">The other set of all stamps of files having the same description.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(XElement pElement, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps)
        {
            // If all stamps diff?
            if
                ( pAllStamps.Count != this.mAllStamps.Count )
            {
                return false;
            }

            // If any modification times mismatch?
            int lCounter = 0;
            foreach
                ( Tuple<string, DateTime> lStamp in this.mAllStamps )
            {
                if
                    ( (lStamp.Item2 - this.mAllStamps[ lCounter ].Item2).TotalSeconds != 0 )
                {
                    return false;
                }
                lCounter++;
            }

            // If no time diff of that related file?
            if
                ( (pStamp - this.mLastSTamp).TotalSeconds == 0 )
            {
                return true;
            }

            return Equals( pElement, this.Descriptor );
        }

        /// <summary>
        /// Checks whether two XML description elements are equal or not.
        /// </summary>
        /// <param name="pFirst">The first XML description element</param>
        /// <param name="pSecond">The Second XML description element</param>
        /// <returns>True of equal, false otherwise.</returns>
        public static bool Equals(XElement pFirst, XElement pSecond)
        {
            // If any element tag diff?
            if
                ( string.CompareOrdinal( pFirst.Name(), pSecond.Name() ) != 0 )
            {
                return false;
            }

            XAttribute lFirstAttribute  = pFirst.FirstAttribute;
            XAttribute lSecondAttribute = pSecond.FirstAttribute;
            while
                ( lFirstAttribute != null )
            {
                if
                    ( lSecondAttribute == null )
                {
                    return false;
                }
                if 
                    ( string.CompareOrdinal( lFirstAttribute.Name.ToString(), lSecondAttribute.Name.ToString() ) != 0 )
                {
                    return false;
                }

                if 
                    ( string.CompareOrdinal( lFirstAttribute.Value, lSecondAttribute.Value ) != 0 )
                {
                    return false;
                }

                lFirstAttribute  = lFirstAttribute.NextAttribute;
                lSecondAttribute = lSecondAttribute.NextAttribute;
            }

            if
                ( lSecondAttribute != null )
            {
                return false;
            }

            XElement lFirstChild  = pFirst.FirstNode as XElement;
            XElement lSecondChild = pSecond.FirstNode as XElement;
            while
                ( lFirstChild != null )
            {
                if 
                    ( lSecondChild == null )
                {
                    return false;
                }

                if 
                    ( Equals( lFirstChild, lSecondChild ) == false )
                {
                    return false;
                }

                lFirstChild  = lFirstChild.NextNode as XElement;
                lSecondChild = lSecondChild.NextNode as XElement;
            }

            return lSecondChild == null;
        }

        #endregion Methods Internal

        #region Methods IResourceFactory

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public abstract IResource Create(string pName);

        #endregion Methods IResourceFactory

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Releases resource's resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            this.mContent    = null;
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
