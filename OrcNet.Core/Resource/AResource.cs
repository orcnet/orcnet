﻿using OrcNet.Core.Logger;
using System;
using System.Diagnostics;

namespace OrcNet.Core.Resource
{
    /// <summary>
    /// Base abstract resource class definition.
    /// </summary>
    /// <typeparam name="TClass">The object type that resource owns.</typeparam>
    [DebuggerDisplay("Name = {Name}, IsModified = {IsModified}, OwnedObject = {OwnedObject}")]
    public abstract class AResource<TClass> : AMemoryProfilable, IResource where TClass : class, IResourceCreatable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object has been disposed or not.
        /// </summary>
        private bool                  mIsDisposed;

        /// <summary>
        /// Stores the owned object that resource manages.
        /// </summary>
        private TClass                mOwnedObject;

        /// <summary>
        /// Stores the resource's name.
        /// </summary>
        private string                mName;

        /// <summary>
        /// Stores the old resource value.
        /// </summary>
        protected AResource<TClass>   mOldValue;
        
        /// <summary>
        /// Stores the resource's description.
        /// </summary>
        protected AResourceDescriptor mDescriptor;

        /// <summary>
        /// Stores the resource's edited descriptor when preparing the resource.
        /// </summary>
        protected AResourceDescriptor mEditedDescriptor;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the resource owned object.
        /// </summary>
        public TClass OwnedObject
        {
            get
            {
                return this.mOwnedObject;
            }
            protected set
            {
                this.mOwnedObject = value;
                // Set the owned object creator as that resource.
                this.mOwnedObject.Creator = this;
            }
        }

        /// <summary>
        /// Gets the resource owned object.
        /// </summary>
        IResourceCreatable IResource.OwnedObject
        {
            get
            {
                return this.OwnedObject;
            }
        }

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                if ( this.mOldValue != null )
                {
                    lSize += this.mOldValue.Size;
                }

                if ( this.mDescriptor != null )
                {
                    lSize += this.mDescriptor.Size;
                }

                if ( this.mEditedDescriptor != null )
                {
                    lSize += this.mEditedDescriptor.Size;
                }

                return lSize;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the resource has changed or not.
        /// </summary>
        public bool IsModified
        {
            get
            {
                return this.mOldValue != null;
            }
        }

        /// <summary>
        /// Gets the resource's name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the update order of that resource.
        /// In order to be up to date properly, such a resource must wait for
        /// parent resource(s) to be up to date first.
        /// (e.g: A GLSL program must wait for its shaders to be updated which
        /// themselves need texture(s) to be ready and so on.
        /// </summary>
        public abstract int UpdateOrder
        {
            get;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AResource{TClass}"/> class.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pDescriptor">The resource description</param>
        protected AResource(string pName, AResourceDescriptor pDescriptor)
        {
            this.mName        = pName;
            this.mDescriptor  = pDescriptor;
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Allow to revert any changes if something goes wrong.
        /// </summary>
        /// <param name="pOldValue">The old value to set back in that resource</param>
        protected virtual void Swap(AResource<TClass> pOldValue)
        {
            // To override...
        }

        #endregion Methods Internal

        #region Methods IResource

        /// <summary>
        /// Prepares the resource update by checking whether the update can be successful or not. 
        /// NOTE: Update is done in two phases =>
        /// first testing if the resource hierarchy can be updated successfully or not,
        /// and second updating all resources. Otherwise nothing will be updated at all.
        /// </summary>
        /// <param name="pLoader">The loader to use to prepare the resource.</param>
        /// <returns>True if the resource passed the first phase and can be updated for sure.</returns>
        public virtual bool PreUpdate(IResourceLoader pLoader)
        {
            // Reload the resource description.
            this.mEditedDescriptor = pLoader.ReloadResource( this.mName, this.mDescriptor );

            // If the descriptor has changed.
            if
                ( this.mEditedDescriptor != null )
            {
                this.mOldValue = null;
                try
                {
                    this.mOldValue = this.mEditedDescriptor.Create( this.mName ) as AResource<TClass>;
                }
                catch
                    ( Exception pEx )
                {
                    LogManager.Instance.Log( pEx );
                }

                // If the creation is a success?
                if
                    ( this.mOldValue != null )
                {
                    // Swaps the current value with the new one.
                    this.Swap( this.mOldValue );
                    return true;
                }

                // If creation fails, do nothing..
                return false;
            }

            return true;
        }

        /// <summary>
        /// Updates the resource.
        /// </summary>
        /// <param name="pIsValid">The flag indicating whether the resource update can be done or not. Else revert preparation work.</param>
        public void Update(bool pIsValid)
        {
            // If it is a valid resource?
            if
                ( pIsValid )
            {
                // And description has changed?
                if
                    ( this.mEditedDescriptor != null )
                {
                    // Set the new description as the current one.
                    this.mDescriptor = this.mEditedDescriptor;

                    LogManager.Instance.Log( string.Format( "Resource {0} updated successfully!!!", this.mName ) );
                }
            }
            // If must abort changes?
            else
            {
                // And resource has changed?
                if
                    ( this.mOldValue != null )
                {
                    // Revert the resource value.
                    this.Swap( this.mOldValue );
                }
            }

            // Releases ref whatever it happens.
            this.mOldValue = null;
            this.mEditedDescriptor = null;
        }

        #endregion Methods IResource

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resource's resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            if ( this.mDescriptor != null )
            {
                this.mDescriptor.Dispose();
                this.mDescriptor = null;
            }

            if ( this.mEditedDescriptor != null )
            {
                this.mEditedDescriptor.Dispose();
                this.mEditedDescriptor = null;
            }

            if ( this.mOwnedObject != null )
            {
                this.mOwnedObject.Dispose();
                this.mOwnedObject = null;
            }

            this.mOldValue = null;
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
