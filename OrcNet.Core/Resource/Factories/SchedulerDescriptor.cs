﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Core.Resource.Factories
{
    /// <summary>
    /// Scheduler descriptor class definition used to
    /// create scheduler based resource(s).
    /// </summary>
    public class SchedulerDescriptor : AResourceDescriptor
    {
        #region Fields

        /// <summary>
        /// The prefetch rate XML tag
        /// </summary>
        private const string cPrefetchRateTag = "prefetchRate";

        /// <summary>
        /// The prefetch rate XML tag
        /// </summary>
        private const string cPrefetchSizeTag = "prefetchSize";

        /// <summary>
        /// The prefetch rate XML tag
        /// </summary>
        private const string cFpsTag = "fps";

        /// <summary>
        /// The prefetch rate XML tag
        /// </summary>
        private const string cThreadCountTag = "threadCount";

        #endregion Fields

        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "multithreadScheduler";
            }
        }

        #endregion Properties IResourceFactory

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += (uint)(cPrefetchRateTag.Length * sizeof(byte));
                lSize += (uint)(cPrefetchSizeTag.Length * sizeof(byte));
                lSize += (uint)(cFpsTag.Length * sizeof(byte));
                lSize += (uint)(cThreadCountTag.Length * sizeof(byte));
                lSize += sizeof(double) + sizeof(int) * 3;

                return lSize;
            }
        }

        /// <summary>
        /// Gets the frame rate.
        /// </summary>
        public double FrameRate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the prefetch rate.
        /// </summary>
        public int PrefetchRate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the prefetch size.
        /// </summary>
        public int PrefetchSize
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the thread count.
        /// </summary>
        public int ThreadCount
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SchedulerDescriptor"/> class.
        /// </summary>
        private SchedulerDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SchedulerDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public SchedulerDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            this.PrefetchRate = 0;
            this.PrefetchSize = 0;
            this.ThreadCount  = 0;
            this.FrameRate    = 0.0;
            if
                ( pDescriptor.HasAttributes )
            {
                XAttribute lPrefetchRateAttr = pDescriptor.Attribute( cPrefetchRateTag );
                if
                    ( lPrefetchRateAttr != null )
                {
                    this.PrefetchRate = int.Parse( lPrefetchRateAttr.Value );
                }

                XAttribute lPrefetchSizeAttr = pDescriptor.Attribute( cPrefetchSizeTag );
                if
                    ( lPrefetchSizeAttr != null )
                {
                    this.PrefetchSize = int.Parse( lPrefetchSizeAttr.Value );
                }

                XAttribute lFpsAttr = pDescriptor.Attribute( cFpsTag );
                if
                    ( lFpsAttr != null )
                {
                    this.FrameRate = double.Parse( lFpsAttr.Value );
                }

                XAttribute lThreadCountAttr = pDescriptor.Attribute( cThreadCountTag );
                if
                    ( lThreadCountAttr != null )
                {
                    this.ThreadCount = int.Parse( lThreadCountAttr.Value );
                }
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new SchedulerResource( pName, this );
        }

        #endregion Methods
    }
}
