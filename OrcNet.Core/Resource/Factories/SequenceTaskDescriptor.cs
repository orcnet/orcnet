﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Task;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Core.Resource.Factories
{
    /// <summary>
    /// Sequence task descriptor class definition used to
    /// sequence task based resource(s).
    /// </summary>
    public class SequenceTaskDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "sequence";
            }
        }

        #endregion Properties IResourceFactory

        /// <summary>
        /// Stores the set of sub tasks that must be executed in sequence.
        /// </summary>
        public List<ITaskFactory> SubTaskFactories
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceTaskDescriptor"/> class.
        /// </summary>
        private SequenceTaskDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceTaskDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public SequenceTaskDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            this.SubTaskFactories = new List<ITaskFactory>();
            if ( pDescriptor.HasAttributes )
            {
                // Nothing to do...
            }

            foreach ( XElement lChild in pDescriptor.Elements() )
            {
                IResourceFactory lDescriptor = DescriptorFactory.Instance.CreateDescriptor( lChild.Name(), lChild, pContent, pEncoding, pStamp, pAllStamps );
                if ( lDescriptor != null )
                {
                    IResource lResource = lDescriptor.Create( "" );
                    this.SubTaskFactories.Add( lResource.OwnedObject as ITaskFactory );
                }
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new SequenceTaskResource( pName, this );
        }

        #endregion Methods
    }
}
