﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Core.Resource.Factories
{
    /// <summary>
    /// Method task descriptor class definition used to
    /// create method task based resource(s).
    /// </summary>
    public class MethodTaskDescriptor : AResourceDescriptor
    {
        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "callMethod";
            }
        }

        #endregion Properties IResourceFactory

        /// <summary>
        /// Gets the method task qualifier.
        /// </summary>
        public string Qualifier
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodTaskDescriptor"/> class.
        /// </summary>
        private MethodTaskDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodTaskDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public MethodTaskDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            this.Qualifier = "";
            if
                ( pDescriptor.HasAttributes )
            {
                XAttribute lQualifierAttr = pDescriptor.Attribute( cNameTag );
                if
                    ( lQualifierAttr != null )
                {
                    this.Qualifier = lQualifierAttr.Value;
                }
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new MethodTaskResource( pName, this );
        }

        #endregion Methods
    }
}
