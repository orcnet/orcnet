﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Core.Resource.Factories
{
    /// <summary>
    /// Descriptor factory manager
    /// </summary>
    internal sealed class DescriptorFactory
    {
        #region Fields

        /// <summary>
        /// Stores the service manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile DescriptorFactory sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the descriptor type by descriptor identifier for being automatically created. 
        /// </summary>
        private static Dictionary<string, Type> sDescriptorMap;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the service manager handle.
        /// </summary>
        public static DescriptorFactory Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if
                    ( sInstance == null )
                {
                    // Lock on
                    lock
                        ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if
                            ( sInstance == null )
                        {
                            sInstance = new DescriptorFactory();
                        }
                    }
                }

                return sInstance;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="DescriptorFactory"/> class.
        /// </summary>
        static DescriptorFactory()
        {
            sDescriptorMap = new Dictionary<string, Type>();
            foreach ( Type lDescriptorType in typeof(IResourceFactory).GetAllInheritedTypes() )
            {
                try
                {
                    ConstructorInfo lCst = lDescriptorType.GetConstructor( BindingFlags.Instance | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
                    if ( lCst == null )
                    {
                        LogManager.Instance.Log( "No default private constructor for the {0} resource descriptor factory!!!", LogType.ERROR );
                        continue;
                    }

                    // Create a default descriptor of that type to know the Factory type of an instance.
                    IResourceFactory lDescriptor = lCst.Invoke( null) as IResourceFactory; //Activator.CreateInstance( lDescriptorType, lFakeDesc, null, lFakeEncoding, lFakeTime, lFakeStamps ) as IResourceFactory;
                    sDescriptorMap[ lDescriptor.FactoryType ] = lDescriptorType;
                }
                catch ( Exception pEx )
                {
                    LogManager.Instance.Log( pEx );
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorFactory"/> class.
        /// </summary>
        private DescriptorFactory()
        {
            
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates a descriptor using its type identifier.
        /// </summary>
        /// <param name="pTypeId">The descriptor type identifier.</param>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        /// <returns>The corresponding descriptor, null if unknown type identifier.</returns>
        internal IResourceFactory CreateDescriptor(string pTypeId, XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps)
        {
            Type lDescriptorType = null;
            if ( sDescriptorMap.TryGetValue( pTypeId, out lDescriptorType ) )
            {
                return Activator.CreateInstance( lDescriptorType, pDescriptor, pContent, pEncoding, pStamp, pAllStamps ) as IResourceFactory;
            }

            return null;
        }

        #endregion Methods
    }
}
