﻿using OrcNet.Core.Extensions;
using OrcNet.Core.SceneGraph;
using OrcNet.Core.Task;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Core.Resource.Factories
{
    /// <summary>
    /// Loop task descriptor class definition used to
    /// create loop task based resource(s).
    /// </summary>
    public class LoopTaskDescriptor : AResourceDescriptor
    {
        #region Fields

        /// <summary>
        /// The variable name XML tag
        /// </summary>
        private const string cVariableTag    = "var";

        /// <summary>
        /// The flag name XML tag
        /// </summary>
        private const string cFlagTag        = "flag";

        /// <summary>
        /// The only visible XML tag
        /// </summary>
        private const string cOnlyVisibleTag = "culling";

        /// <summary>
        /// The parallelize XML tag
        /// </summary>
        private const string cParallelizeTag = "parallel";

        #endregion Fields

        #region Properties

        #region Properties IResourceFactory

        /// <summary>
        /// Gets the resource factory type.
        /// </summary>
        public override string FactoryType
        {
            get
            {
                return "foreach";
            }
        }

        #endregion Properties IResourceFactory

        /// <summary>
        /// Gets the loop task variable name.
        /// </summary>
        public string VariableName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the flag that specifies the nodes that loop must be applied for.
        /// </summary>
        public string Flag
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the flag indicating whether the loop task must be only applied to visible nodes or not.
        /// </summary>
        public bool OnlyVisible
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the flag indicating whether the loop must be processed in parallel or not.
        /// </summary>
        public bool Parallelize
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the loop task's task factory to apply to all node.
        /// </summary>
        public ITaskFactory SubTaskFactory
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LoopTaskDescriptor"/> class.
        /// </summary>
        private LoopTaskDescriptor()
        {
            // Only for internal management.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoopTaskDescriptor"/> class.
        /// </summary>
        /// <param name="pDescriptor">The resource XML descriptor</param>
        /// <param name="pContent">The resource content</param>
        /// <param name="pEncoding">The encoding used to format the byte array.</param>
        /// <param name="pStamp">The last modification time of the file this description comes from</param>
        /// <param name="pAllStamps">The set of all modification times of all files having such descriptions</param>
        public LoopTaskDescriptor(XElement pDescriptor, byte[] pContent, Encoding pEncoding, DateTime pStamp, List<Tuple<string, DateTime>> pAllStamps) :
        base( pDescriptor, pContent, pEncoding, pStamp, pAllStamps )
        {
            this.VariableName = "";
            this.Flag = "";
            this.OnlyVisible = false;
            this.Parallelize = false;
            if
                ( pDescriptor.HasAttributes )
            {
                XAttribute lVariableAttr = pDescriptor.Attribute( cVariableTag );
                if
                    ( lVariableAttr != null )
                {
                    this.VariableName = lVariableAttr.Value;
                }

                XAttribute lFlagAttr = pDescriptor.Attribute( cFlagTag );
                if
                    ( lFlagAttr != null )
                {
                    this.Flag = lFlagAttr.Value;
                }

                XAttribute lOnlyVisibleAttr = pDescriptor.Attribute( cOnlyVisibleTag );
                if
                    ( lOnlyVisibleAttr != null )
                {
                    this.OnlyVisible = bool.Parse( lOnlyVisibleAttr.Value );
                }

                XAttribute lParallelizeAttr = pDescriptor.Attribute( cParallelizeTag );
                if
                    ( lParallelizeAttr != null )
                {
                    this.Parallelize = bool.Parse( lParallelizeAttr.Value );
                }
            }

            List<ITaskFactory> lSubTaskFactories = new List<ITaskFactory>();
            foreach
                ( XElement lChild in pDescriptor.Elements() )
            {
                IResourceFactory lDescriptor = DescriptorFactory.Instance.CreateDescriptor( lChild.Name(), lChild, pContent, pEncoding, pStamp, pAllStamps );
                if
                    ( lDescriptor != null )
                {
                    IResource lResource = lDescriptor.Create( "" );
                    lSubTaskFactories.Add( lResource.OwnedObject as ITaskFactory );
                }
            }

            if
                ( lSubTaskFactories.Count == 1 )
            {
                this.SubTaskFactory = lSubTaskFactories[ 0 ];
            }
            else
            {
                // If more than one sub task, aggregates tasks into a manager sequence task.
                this.SubTaskFactory = new SequenceTaskFactory( lSubTaskFactories );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates an instance of the resource.
        /// </summary>
        /// <param name="pName">The resource's name.</param>
        /// <returns>The new resource corresponding to the factory.</returns>
        public override IResource Create(string pName)
        {
            return new LoopTaskResource( pName, this );
        }

        #endregion Methods
    }
}
