﻿using OrcNet.Core.Math;
using OrcNet.Core.Render;
using OrcNet.Core.Resource;
using System.Collections.Generic;

namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// Base scene node interface definition.
    /// </summary>
    public interface ISceneNode : IResourceCreatable, IMemoryProfilable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the node is visible or not.
        /// </summary>
        bool IsVisible
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the flag indicating whether this node has any children or not.
        /// </summary>
        bool AnyChildren
        {
            get;
        }

        /// <summary>
        /// Gets the child count.
        /// </summary>
        int ChildCount
        {
            get;
        }

        /// <summary>
        /// Gets or sets the scene node local bounds.
        /// </summary>
        IBox<float> LocalBounds
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the scene node bounds in world coordinates.
        /// </summary>
        IBox<float> WorldBounds
        {
            get;
        }

        /// <summary>
        /// Gets a child node by index.
        /// </summary>
        /// <param name="pIndex">The child node's index to look for.</param>
        /// <returns>The child node if index in bounds, null otherwise.</returns>
        ISceneNode this[SceneNodeIndex pIndex]
        {
            get;
        }

        /// <summary>
        /// Gets the node's children.
        /// </summary>
        IEnumerable<ISceneNode> Children
        {
            get;
        }

        /// <summary>
        /// Gets the set of flags of this node.
        /// </summary>
        IEnumerable<string> Flags
        {
            get;
        }

        /// <summary>
        /// Gets the set of methods to run on this node.
        /// </summary>
        IEnumerable<SceneNodeMethod> Methods
        {
            get;
        }

        /// <summary>
        /// Gets the field(s) of this node.
        /// </summary>
        IEnumerable<object> Fields
        {
            get;
        }

        /// <summary>
        /// Gets the mesh(es) of this node.
        /// </summary>
        IEnumerable<IMeshBuffers> Meshes
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Checks whether the supplied node is a child of this node or not.
        /// </summary>
        /// <param name="pNode">The node to test.</param>
        /// <returns>True if the node is a child of this node, false otherwise.</returns>
        bool HasChild(ISceneNode pNode);

        /// <summary>
        /// Adds the supplied node as child of that node's children set.
        /// </summary>
        /// <param name="pChild">The new child node.</param>
        void AddChild(ISceneNode pChild);

        /// <summary>
        /// Removes the supplied child from that node's children set.
        /// </summary>
        /// <param name="pNode">The node to remove.</param>
        /// <returns>True if the supplied node has been removed, false otherwise.</returns>
        bool RemoveChild(ISceneNode pNode);

        /// <summary>
        /// Removes a child by index from that node's children set.
        /// </summary>
        /// <param name="pIndex">The node's index to remove.</param>
        /// <returns>True if the supplied node has been removed, false otherwise.</returns>
        bool RemoveChild(int pIndex);

        /// <summary>
        /// Checks whether the node has the given flag or not.
        /// </summary>
        /// <param name="pFlag">Teh flag to look for.</param>
        /// <returns>True if has the flag, false otherwise.</returns>
        bool HasFlag(string pFlag);

        /// <summary>
        /// Adds a new flag to that node's flag set.
        /// </summary>
        /// <param name="pFlag">The new flag.</param>
        void AddFlag(string pFlag);

        /// <summary>
        /// Removes the supplied flag from that node's flag set.
        /// </summary>
        /// <param name="pFlag">The flag to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemoveFlag(string pFlag);

        /// <summary>
        /// Retrieves the method of this node having the specified name if any.
        /// </summary>
        /// <param name="pMethodName">The method name to look for.</param>
        /// <returns>The method, null otherwise.</returns>
        SceneNodeMethod GetMethod(string pMethodName);

        /// <summary>
        /// Adds a new method to that node's methods set.
        /// </summary>
        /// <param name="pMethodName">The method identifier</param>
        /// <param name="pMethod">The method to add.</param>
        void AddMethod(string pMethodName, SceneNodeMethod pMethod);

        /// <summary>
        /// Removes the method having the specified name.
        /// </summary>
        /// <param name="pMethodName">The method name to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemoveMethod(string pMethodName);

        /// <summary>
        /// Retrieves the field of this node having the specified name if any.
        /// </summary>
        /// <param name="pFieldName">The field name to look for.</param>
        /// <returns>The field, null otherwise.</returns>
        object GetFields(string pFieldName);

        /// <summary>
        /// Adds a new field to this node's field set.
        /// </summary>
        /// <param name="pFieldName">The field identifier.</param>
        /// <param name="pField">The field to add.</param>
        void AddField(string pFieldName, object pField);

        /// <summary>
        /// Removes the field having the specified name.
        /// </summary>
        /// <param name="pFieldName">The field name to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemoveField(string pFieldName);

        /// <summary>
        /// Retrieves the mesh of this node whose name is provided.
        /// </summary>
        /// <param name="pMeshName">The mesh name.</param>
        /// <returns>The mesh if found, null otherwise.</returns>
        IMeshBuffers GetMesh(string pMeshName);

        /// <summary>
        /// Adds a mesh to this node under the given name.
        /// </summary>
        /// <param name="pMeshName">The mesh name.</param>
        /// <param name="pMesh">The mesh.</param>
        void AddMesh(string pMeshName, IMeshBuffers pMesh);

        /// <summary>
        /// Removes the mesh having the specified name.
        /// </summary>
        /// <param name="pMeshName">The mesh name.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemoveMesh(string pMeshName);

        /// <summary>
        /// Retrieves the pipeline description whose name is provided.
        /// </summary>
        /// <param name="pName">The pipeline description's name.</param>
        /// <returns>The description if found, null otherwise.</returns>
        IPipelineDescription GetPipelineDescription(string pName);

        /// <summary>
        /// Adds a new pipeline description to that scene node mapped with the given name.
        /// </summary>
        /// <param name="pName">The description's name.</param>
        /// <param name="pNewDescription">The new description.</param>
        void AddPipelineDescription(string pName, IPipelineDescription pNewDescription);

        /// <summary>
        /// Removes a pipeline description mapped with the supplied name.
        /// </summary>
        /// <param name="pName">The name that description is mapped with.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemovePipelineDescription(string pName);

        #endregion Methods
    }
}
