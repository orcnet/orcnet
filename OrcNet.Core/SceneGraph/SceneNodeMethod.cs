﻿using OrcNet.Core.Task;
using System;
using System.Diagnostics;

namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// A scene node method to run on a scene node. 
    /// It can be a basic task or multiple tasks in sequence, 
    /// loop or other actions. Such action uses task factory 
    /// that could be shared between different actions to get
    /// task(s) that must be ran to execute that action depending
    /// on the context.
    /// </summary>
    [DebuggerDisplay("IsEnabled = {IsEnabled}")]
    public class SceneNodeMethod : AMemoryProfilable, IComparable, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the method is enabled or not.
        /// </summary>
        private bool mIsEnabled;

        /// <summary>
        /// Stores the scene node this method belongs to.
        /// </summary>
        private ISceneNode mTarget;

        /// <summary>
        /// Stores the method's body.
        /// </summary>
        private ITaskFactory mBody;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = sizeof(bool);
                if( this.mBody != null )
                {
                    lSize += this.mBody.Size;
                }
                if( this.mTarget != null )
                {
                    lSize += this.mTarget.Size;
                }

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets or sets the flag indicating whether the method is enabled or not.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return this.mIsEnabled;
            }
            set
            {
                this.mIsEnabled = value;
            }
        }

        /// <summary>
        /// Gets the scene node this method belongs to.
        /// </summary>
        public ISceneNode Target
        {
            get
            {
                return this.mTarget;
            }
            set
            {
                this.mTarget = value;
            }
        }

        /// <summary>
        /// Gets the method's body.
        /// </summary>
        public ITaskFactory Body
        {
            get
            {
                return this.mBody;
            }
        }

        #endregion Properties

        #region Constructor
        
        /// <summary>
        /// Initializes a new instance of the <see cref="SceneNodeMethod"/> class.
        /// </summary>
        /// <param name="pBody">The method body to execute.</param>
        public SceneNodeMethod(ITaskFactory pBody)
        {
            this.mIsEnabled = true;
            this.mBody = pBody;
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Creates the task to run as method body of this.
        /// </summary>
        /// <returns>The task to execute.</returns>
        public ITask CreateTask()
        {
            return this.mBody.Create( this );
        }
        
        #region Methods IComparable

        /// <summary>
        /// Compares this instance with another.
        /// </summary>
        /// <param name="pOther">The other instance.</param>
        /// <returns>0 if equal, -1 otherwise.</returns>
        public int CompareTo(object pOther)
        {
            SceneNodeMethod lOther = pOther as SceneNodeMethod;
            if
                ( lOther == null )
            {
                return -1;
            }

            if
                ( this.mTarget == lOther.mTarget &&
                  this.mBody   == lOther.mBody )
            {
                return 0;
            }

            return -1;
        }

        #endregion Methods IComparable

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            this.mTarget = null;
            this.mBody   = null;
            this.mIsEnabled = false;
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
