﻿using OrcNet.Core.Service;
using System.Collections.Generic;
using System.Linq;

namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// Method Qualifier structure definition owning the pair
    /// of the form [Target].[MethodName]
    /// </summary>
    public class MethodQualifier : AMemoryProfilable
    {
        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                if
                    ( string.IsNullOrEmpty( this.Target ) == false )
                {
                    lSize += sizeof(char) * (uint)this.Target.Length;
                }
                if
                    ( string.IsNullOrEmpty( this.Name ) == false )
                {
                    lSize += sizeof(char) * (uint)this.Name.Length;
                }
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        /// <summary>
        /// Gets or sets the Target part of the qualified name structure.
        /// </summary>
        public string Target
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the Name part of the qualified name structure.
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodQualifier"/> class.
        /// </summary>
        /// <param name="pQualifierPair">The qualifier pair of the form [Target].[Name]</param>
        public MethodQualifier(string pQualifierPair)
        {
            string[] lPair = pQualifierPair.Split( '.' );
            if
                ( lPair.Length == 2 )
            {
                this.Target = lPair[ 0 ];
                this.Name   = lPair[ 1 ];
            }
            else
            {
                this.Target = "";
                this.Name   = pQualifierPair;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodQualifier"/> class.
        /// </summary>
        /// <param name="pTarget"></param>
        /// <param name="pName"></param>
        public MethodQualifier(string pTarget, string pName)
        {
            this.Target = pTarget;
            this.Name   = pName;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the qualified name's target scene node
        /// </summary>
        /// <param name="pContext">The node</param>
        /// <returns>The target node designated by this qualified name.</returns>
        public ISceneNode GetTarget(ISceneNode pContext)
        {
            if ( string.IsNullOrEmpty( this.Target ) )
            {
                return null;
            }
            else if ( string.CompareOrdinal( this.Target, "this" ) == 0 )
            {
                return pContext;
            }

            ISceneService lSceneService = ServiceManager.Instance.GetService<ISceneService>();
            if ( lSceneService != null )
            {
                if ( this.Target.StartsWith( "$" ) )
                {
                    string lVariableName = this.Target.Substring( 1 );
                    return lSceneService[ lVariableName ];
                }
                else
                {
                    IEnumerable<ISceneNode> lNodesWithTarget = lSceneService.GetNodes( this.Target );
                    return lNodesWithTarget.FirstOrDefault();
                }
            }

            return null;
        }

        #endregion Methods
    }
}
