﻿using OrcNet.Core.Resource;
using OrcNet.Core.Task;
using System;

namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// Base abstract task factory class definition.
    /// </summary>
    public abstract class ATaskFactory : AMemoryProfilable, ITaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the task factory creator.
        /// </summary>
        private IResource mCreator;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                return 0;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IResourceCreatable

        /// <summary>
        /// Gets or sets the creatable's creator
        /// </summary>
        public IResource Creator
        {
            get
            {
                return this.mCreator;
            }
            set
            {
                this.mCreator = value;
            }
        }

        #endregion Properties IResourceCreatable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ATaskFactory"/> class.
        /// </summary>
        protected ATaskFactory()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public abstract ITask Create(IComparable pContext);

        #region Methods IDisposable

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Delegate called on dispose.
        /// </summary>
        protected virtual void OnDispose()
        {
            this.mCreator = null;
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
