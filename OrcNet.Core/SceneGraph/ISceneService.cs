﻿using OrcNet.Core.Math;
using OrcNet.Core.Render;
using OrcNet.Core.Task;
using System.Collections.Generic;

namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// Base scene service interface definition.
    /// NOTE: The scene manager of the Engine.
    /// </summary>
    public interface ISceneService : IService
    {
        #region Properties

        /// <summary>
        /// Gets or sets the current frame buffer.
        /// </summary>
        IFrameBuffer CurrentFrameBuffer
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the current pipeline pass for drawing.
        /// </summary>
        IPipelinePass CurrentPass
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the node currently mapped with the supplied loop variable name.
        /// </summary>
        /// <param name="pName">The loop variable name</param>
        /// <returns>The mapped node to that loop variable.</returns>
        ISceneNode this[string pName]
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the root node of the scene.
        /// </summary>
        ISceneNode Root
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the camera node of the scene.
        /// </summary>
        ISceneNode Camera
        {
            get;
        }

        /// <summary>
        /// Gets or sets the camera node method to call for drawing the scene.
        /// </summary>
        string CameraMethod
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the scene task scheduler used to schedule tasks.
        /// </summary>
        IScheduler Scheduler
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the frame count that is the number of draw calls.
        /// </summary>
        uint FrameCount
        {
            get;
        }

        /// <summary>
        /// Gets the absolute current time in micro seconds
        /// see Update.
        /// </summary>
        double AbsoluteTime
        {
            get;
        }

        /// <summary>
        /// Gets the elapsed time in micro seconds since the last update call.
        /// see Update.
        /// </summary>
        double ElapsedTime
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Checks the world bounds visibility regarding to the camera node.
        /// </summary>
        /// <param name="pWorldBounds">The bounds in world space.</param>
        /// <returns>The visibility state.</returns>
        BoxVisibility IsVisible(IBox<float> pWorldBounds);

        /// <summary>
        /// Sets the camera node of the scene.
        /// </summary>
        /// <param name="pFlag">The scene node flag that identifies the camera node.</param>
        void SetNodeAsCamera(string pFlag);

        /// <summary>
        /// Gets the set of nodes having the specified flag in the scene.
        /// </summary>
        /// <param name="pFlag">The flag to look for.</param>
        /// <returns>The set of nodes having the given flag.</returns>
        IEnumerable<ISceneNode> GetNodes(string pFlag);
        
        /// <summary>
        /// Updates the scene.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute current time in micro seconds.</param>
        /// <param name="pElapsedTime">The elapsed time in micro seconds since the last update.</param>
        void Update(double pAbsoluteTime, double pElapsedTime);

        /// <summary>
        /// Executes the rendering.
        /// </summary>
        void Render();

        #endregion Methods
    }
}
