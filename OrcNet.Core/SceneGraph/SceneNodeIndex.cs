﻿namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// Helper structure to access by index one of the vertices.
    /// </summary>
    public struct SceneNodeIndex
    {
        #region Fields

        /// <summary>
        /// Stores the array index
        /// </summary>
        private int mIndex;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneNodeIndex"/> class.
        /// </summary>
        /// <param name="pIndex">The scene node index.</param>
        public SceneNodeIndex(int pIndex)
        {
            this.mIndex = pIndex;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from SceneNodeIndex to int
        /// </summary>
        /// <param name="pIndex">The scene node index</param>
        public static implicit operator int(SceneNodeIndex pIndex)
        {
            return pIndex.mIndex;
        }

        #endregion Methods
    }
}
