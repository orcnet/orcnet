﻿using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Task;
using System;

namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// Method task factory class definition for running a method
    /// on a scene node.
    /// </summary>
    public class MethodTaskFactory : ATaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the [Node].[MethodName] qualified name corresponding to the method
        /// to run on the scene node.
        /// </summary>
        private MethodQualifier mMethod;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodTaskFactory"/> class.
        /// </summary>
        private MethodTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodTaskFactory"/> class.
        /// </summary>
        /// <param name="pMethod">The [Node].[MethodName] qualified name corresponding to the method to run on the scene node.</param>
        public MethodTaskFactory(MethodQualifier pMethod)
        {
            this.Initialize( pMethod );
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the task factory.
        /// </summary>
        /// <param name="pMethod">The [Node].[MethodName] qualified name corresponding to the method to run on the scene node.</param>
        private void Initialize(MethodQualifier pMethod)
        {
            this.mMethod = pMethod;
        }

        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(MethodTaskFactory pOther)
        {
            Utilities.Swap(ref this.mMethod, ref pOther.mMethod );
        }

        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            SceneNodeMethod lMethod = pContext as SceneNodeMethod;
            ISceneNode lTarget = lMethod.Target;
            ISceneNode lThisTarget = this.mMethod.GetTarget( lTarget );
            if ( lThisTarget != null ) // If matched?
            {
                SceneNodeMethod lSameMethod = lThisTarget.GetMethod( this.mMethod.Name ); // And that node really still has that method to run?
                if ( lSameMethod != null )
                {
                    if ( lSameMethod.IsEnabled )
                    {
                        return lSameMethod.CreateTask();
                    }
                    else
                    {
                        return new TaskSet();
                    }
                }
            }

            LogManager.Instance.Log( string.Format( "MethodTask: Cannot find any method \"{0}\" to the target \"{1}\"!!!", this.mMethod.Name, this.mMethod.Target ), LogType.ERROR );

            return null;
        }

        #endregion Methods ATaskFactory

        #endregion Methods
    }
}
