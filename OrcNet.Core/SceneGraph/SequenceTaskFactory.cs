﻿using OrcNet.Core.Helpers;
using OrcNet.Core.Task;
using System;
using System.Collections.Generic;

namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// Sequence task factory class definition for aggregating a set of tasks in
    /// order to run it as a sequence.
    /// </summary>
    public class SequenceTaskFactory : ATaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the set of sub tasks that must be executed in sequence.
        /// </summary>
        private List<ITaskFactory> mSubTaskFactories;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceTaskFactory"/> class.
        /// </summary>
        private SequenceTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceTaskFactory"/> class.
        /// </summary>
        /// <param name="pSubTaskFactories">The set of sub task(s) that must be executed in sequence</param>
        public SequenceTaskFactory(List<ITaskFactory> pSubTaskFactories)
        {
            this.Initialize( pSubTaskFactories );
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the task factory.
        /// </summary>
        /// <param name="pSubTaskFactories">The set of sub task(s) that must be executed in sequence</param>
        private void Initialize(List<ITaskFactory> pSubTaskFactories)
        {
            this.mSubTaskFactories = pSubTaskFactories;
        }

        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(SequenceTaskFactory pOther)
        {
            this.mSubTaskFactories.Swap( pOther.mSubTaskFactories );
        }

        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            int lSubTaskCount = this.mSubTaskFactories.Count;
            if ( lSubTaskCount == 1 )
            {
                return this.mSubTaskFactories[ 0 ].Create( pContext );
            }

            ITask lPrevious = null;
            TaskSet lSet = new TaskSet();
            for ( int lCurr = 0; lCurr < lSubTaskCount; lCurr++ )
            {
                ITaskFactory lTaskFactory = this.mSubTaskFactories[ lCurr ];
                ITask lSubTask = lTaskFactory.Create( pContext );
                TaskSet lCast = lSubTask as TaskSet;
                if ( lCast == null ||
                     lCast.IsEmpty == false ) // Check if simple task or a set with tasks in it.
                {
                    lSet.AddTask( lSubTask );
                    if ( lPrevious != null )
                    {
                        lSet.CreateDependency( lSubTask, lPrevious );
                    }

                    lPrevious = lSubTask;
                }
            }

            return lSet;
        }

        #endregion Methods ATaskFactory

        #endregion Methods
    }
}
