﻿using OrcNet.Core.Helpers;
using OrcNet.Core.Service;
using OrcNet.Core.Task;
using System;
using System.Collections.Generic;

namespace OrcNet.Core.SceneGraph
{
    /// <summary>
    /// Loop task factory class definition for executing a task on
    /// a set of scene nodes.
    /// </summary>
    public class LoopTaskFactory : ATaskFactory
    {
        #region Fields

        /// <summary>
        /// Stores the task(s) that must be executed on each node.
        /// </summary>
        ITaskFactory   mSubTaskFactory;

        /// <summary>
        /// Stores the variable name (id)
        /// </summary>
        private string mVariableName;

        /// <summary>
        /// Stores the variable flag specifying scene nodes to which that loop must be applied.
        /// </summary>
        private string mVariableFlag;

        /// <summary>
        /// Stores the flag indicating whether the loop must be done only on visible nodes or not.
        /// </summary>
        private bool   mOnlyVisible;

        /// <summary>
        /// Stores the flag indicating whether the loop must be processed in parallel or not.
        /// </summary>
        private bool   mParallelize;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LoopTaskFactory"/> class.
        /// </summary>
        private LoopTaskFactory()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoopTaskFactory"/> class.
        /// </summary>
        /// <param name="pVariableName">The variable name. (id)</param>
        /// <param name="pFlag">The flag that specifies the nodes that loop must be applied for.</param>
        /// <param name="pOnlyVisible">The flag indicating whether the loop must be done only on visible nodes or not.</param>
        /// <param name="pParallelize">The flag indicating whether the loop must be processed in parallel or not.</param>
        /// <param name="pSubTaskFactory">The task(s) that must be executed on each node.</param>
        public LoopTaskFactory(string pVariableName, string pFlag, bool pOnlyVisible, bool pParallelize, ITaskFactory pSubTaskFactory)
        {
            this.Initialize( pVariableName, pFlag, pOnlyVisible, pParallelize, pSubTaskFactory );
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the task factory.
        /// </summary>
        /// <param name="pVariableName">The variable name. (id)</param>
        /// <param name="pFlag">The flag that specifies the nodes that loop must be applied for.</param>
        /// <param name="pOnlyVisible">The flag indicating whether the loop must be done only on visible nodes or not.</param>
        /// <param name="pParallelize">The flag indicating whether the loop must be processed in parallel or not.</param>
        /// <param name="pSubTaskFactory">The set of sub task(s) that must be executed on each node.</param>
        private void Initialize(string pVariableName, string pFlag, bool pOnlyVisible, bool pParallelize, ITaskFactory pSubTaskFactory)
        {
            this.mVariableName   = pVariableName;
            this.mVariableFlag   = pFlag;
            this.mOnlyVisible    = pOnlyVisible;
            this.mParallelize    = pParallelize;
            this.mSubTaskFactory = pSubTaskFactory;
        }

        /// <summary>
        /// Swaps that factory with another.
        /// </summary>
        /// <param name="pOther">The other factory.</param>
        internal void Swap(LoopTaskFactory pOther)
        {
            Utilities.Swap( ref this.mVariableName,  ref pOther.mVariableName    );
            Utilities.Swap( ref this.mVariableFlag,  ref pOther.mVariableFlag    );
            Utilities.Swap( ref this.mOnlyVisible,   ref pOther.mOnlyVisible     );
            Utilities.Swap( ref this.mSubTaskFactory, ref pOther.mSubTaskFactory );
        }

        #endregion Methods Internal

        #region Methods ATaskFactory

        /// <summary>
        /// Creates a new task using that factory.
        /// </summary>
        /// <param name="pContext">The contextual object </param>
        /// <returns>The new task</returns>
        public override ITask Create(IComparable pContext)
        {
            ISceneService lSceneService = ServiceManager.Instance.GetService<ISceneService>();
            if ( lSceneService != null )
            {
                List<ISceneNode> lNodes = new List<ISceneNode>();
                foreach ( ISceneNode lNodeWithThatFlag in lSceneService.GetNodes( this.mVariableFlag ) )
                {
                    if ( this.mOnlyVisible == false ||
                         lNodeWithThatFlag.IsVisible )
                    {
                        lNodes.Add( lNodeWithThatFlag );
                    }
                }

                int lNodeCount = lNodes.Count;
                if ( lNodeCount == 1 )
                {
                    lSceneService[ this.mVariableName ] = lNodes[ 0 ];
                    return this.mSubTaskFactory.Create( pContext );
                }
                else
                {
                    TaskSet lResult = new TaskSet();
                    ITask lPreviousTask = null;
                    for ( int lCurr = 0; lCurr < lNodeCount; lCurr++ )
                    {
                        ISceneNode lNode = lNodes[ lCurr ];
                        lSceneService[ this.mVariableName ] = lNode;
                        ITask lNextTask = this.mSubTaskFactory.Create( pContext );
                        TaskSet lCast = lNextTask as TaskSet;
                        if ( lCast == null || 
                             lCast.IsEmpty == false ) // Only add sub task that have something to do.
                        {
                            lResult.AddTask( lNextTask );
                            if ( this.mParallelize == false &&
                                 lPreviousTask != null )
                            {
                                lResult.CreateDependency( lNextTask, lPreviousTask );
                            }

                            lPreviousTask = lNextTask;
                        }
                    }

                    return lResult;
                }
            }

            return null;
        }

        #endregion Methods ATaskFactory

        #endregion Methods
    }
}
