﻿using System.Collections.Generic;

namespace OrcNet.Core.Datastructures
{
    /// <summary>
    /// Base multimap interface definition.
    /// </summary>
    /// <typeparam name="TKey">The key type.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    public interface IMultimap<TKey, TValue> : IDictionary<TKey, HashSet<TValue>>, IEnumerable<KeyValuePair<TKey, HashSet<TValue>>>
    {
        #region Properties



        #endregion Properties
    }
}
