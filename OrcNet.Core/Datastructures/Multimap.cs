﻿using System.Collections.Generic;

namespace OrcNet.Core.Datastructures
{
    /// <summary>
    /// Multimap class definition allowing to have multiple values mapped
    /// to the very same key.
    /// </summary>
    /// <typeparam name="TKey">The key type.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    public class Multimap<TKey, TValue> : Dictionary<TKey, HashSet<TValue>>, IMultimap<TKey, TValue>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Multimap{TKey, TValue}"/> class.
        /// </summary>
        public Multimap() :
        base()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Multimap{TKey, TValue}"/> class.
        /// </summary>
        /// <param name="pCapacity">The starting container's capacity.</param>
        public Multimap(int pCapacity) :
        base( pCapacity )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Multimap{TKey, TValue}"/> class.
        /// </summary>
        /// <param name="pMultimap">The other map to build that one with.</param>
        public Multimap(IMultimap<TKey, TValue> pMultimap) :
        base( pMultimap )
        {
            
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds the specified value under the specified key
        /// </summary>
        /// <param name="pKey">The key.</param>
        /// <param name="pValue">The value.</param>
        public void Add(TKey pKey, TValue pValue)
        {
            if
                ( object.ReferenceEquals( pKey, null ) )
            {
                return;
            }

            HashSet<TValue> lMappedValues = null;
            if 
                ( this.TryGetValue( pKey, out lMappedValues ) == false )
            {
                lMappedValues = new HashSet<TValue>();
                base.Add( pKey, lMappedValues );
            }
            
            lMappedValues.Add( pValue );
        }
        
        /// <summary>
        /// Determines whether this multimap contains the specified value for the specified key 
        /// </summary>
        /// <param name="pKey">The key.</param>
        /// <param name="pValue">The value.</param>
        /// <returns>true if the value is stored for the specified key in this dictionary, false otherwise</returns>
        public bool ContainsValue(TKey pKey, TValue pValue)
        {
            if
                ( object.ReferenceEquals( pKey, null ) )
            {
                return false;
            }
            
            HashSet<TValue> lMappedValues = null;
            if
                ( this.TryGetValue( pKey, out lMappedValues ) )
            {
                return lMappedValues.Contains( pValue );
            }

            return false;
        }
        
        /// <summary>
        /// Removes the specified value for the specified key.
        /// Note that the key will be removed if no more mapped value(s).
        /// </summary>
        /// <param name="pKey">The key.</param>
        /// <param name="pValue">The value.</param>
        public void Remove(TKey pKey, TValue pValue)
        {
            if
                ( object.ReferenceEquals( pKey, null ) )
            {
                return;
            }

            HashSet<TValue> lMappedValues = null;
            if 
                ( this.TryGetValue( pKey, out lMappedValues ) )
            {
                lMappedValues.Remove( pValue );
                if
                    ( lMappedValues.Count == 0 )
                {
                    this.Remove( pKey );
                }
            }
        }
        
        /// <summary>
        /// Merges the specified multimap into this one.
        /// </summary>
        /// <param name="pToMerge">To merge with.</param>
        public void Merge(IMultimap<TKey, TValue> pToMerge)
        {
            if 
                ( pToMerge == null )
            {
                return;
            }

            foreach
                ( KeyValuePair<TKey, HashSet<TValue>> lPair in pToMerge )
            {
                foreach 
                    ( TValue lValue in lPair.Value )
                {
                    this.Add( lPair.Key, lValue );
                }
            }
        }
        
        /// <summary>
        /// Gets the values for the specified key. 
        /// This method is useful if you want to avoid an exception for key value retrieval and you can't use TryGetValue
        /// (e.g. in lambdas)
        /// </summary>
        /// <param name="pKey">The key.</param>
        /// <returns>The mapped values if the key is found, empty set otherwise.</returns>
        public HashSet<TValue> GetValues(TKey pKey)
        {
            if
                ( object.ReferenceEquals( pKey, null ) )
            {
                return new HashSet<TValue>();
            }

            HashSet<TValue> lMappedValues = null;
            if 
                ( this.TryGetValue( pKey, out lMappedValues ) == false )
            {
                lMappedValues = new HashSet<TValue>(); // Empty if not found.
            }

            return lMappedValues;
        }

        #endregion Methods
    }
}
