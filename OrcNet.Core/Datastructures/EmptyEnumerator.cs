﻿using System.Collections;
using System.Collections.Generic;

namespace OrcNet.Core.Datastructures
{
    /// <summary>
    /// Definition of an EMpty enumerator.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EmptyEnumerator<T> : IEnumerator<T>
    {
        #region Fields

        /// <summary>
        /// Stores the constant empty enumerator.
        /// </summary>
        private static readonly IEnumerator sEmpty = new EmptyEnumerator<object>();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the global instance of an empty enumerator.
        /// </summary>
        public static IEnumerator Empty
        {
            get
            {
                return sEmpty;
            }
        }

        /// <summary>
        /// Gets the current element.
        /// </summary>
        public T Current
        {
            get
            {
                return default(T);
            }
        }
        
        /// <summary>
        /// Gets the current element.
        /// </summary>
        object IEnumerator.Current
        {
            get
            {
                return this.Current;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EmptyEnumerator{T}"/> class.
        /// </summary>
        public EmptyEnumerator()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {

        }

        /// <summary>
        /// Moves next.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            return false;
        }

        /// <summary>
        /// Resets 
        /// </summary>
        public void Reset()
        {
        }

        #endregion Methods
    }
}
