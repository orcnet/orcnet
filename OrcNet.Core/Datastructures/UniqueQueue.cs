﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.Core.Datastructures
{
    /// <summary>
    /// Queue not permitting duplicates.
    /// </summary>
    /// <typeparam name="T">The queue inner type.</typeparam>
    public class UniqueQueue<T> : IQueue<T>
    {
        #region Fields

        /// <summary>
        /// Stores the inner queue cache.
        /// </summary>
        private readonly Queue<T> mCache;

        /// <summary>
        /// Stores the set containing already added elements to avoid duplicates.
        /// </summary>
        private readonly HashSet<T> mAlreadyAdded;

        #endregion Fields

        #region Properties

        #region Properties ICollection

        /// <summary>
        /// Gets the queue element count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mCache.Count;
            }
        }

        /// <summary>
        /// Gets the queue sync root.
        /// </summary>
        object ICollection.SyncRoot
        {
            get
            {
                return (this.mCache as ICollection).SyncRoot;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the queue is synchronized or not.
        /// </summary>
        bool ICollection.IsSynchronized
        {
            get
            {
                return (this.mCache as ICollection).IsSynchronized;
            }
        }

        #endregion Properties ICollection

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UniqueQueue{T}"/> class.
        /// </summary>
        public UniqueQueue()
        {
            this.mCache = new Queue<T>();
            this.mAlreadyAdded = new HashSet<T>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UniqueQueue{T}"/> class.
        /// </summary>
        /// <param name="pCapacity">The starting cache capacity.</param>
        public UniqueQueue(int pCapacity)
        {
            this.mCache = new Queue<T>( pCapacity );
            this.mAlreadyAdded = new HashSet<T>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Empty the queue.
        /// </summary>
        public void Clear()
        {
            this.mCache.Clear();
            this.mAlreadyAdded.Clear();
        }

        /// <summary>
        /// Checks whether the given element is in the queue or not.
        /// </summary>
        /// <param name="pElement">The element to look for.</param>
        /// <returns>True if contained, false otherwise.</returns>
        public bool Contains(T pElement)
        {
            // Faster search than Queue<T>.
            return this.mAlreadyAdded.Contains( pElement );
        }

        /// <summary>
        /// Adds a new element into the queue.
        /// </summary>
        /// <param name="pElement">The new element to add.</param>
        /// <returns>True if not already in, false otherwise.</returns>
        public bool Enqueue(T pElement)
        {
            if ( this.mAlreadyAdded.Add( pElement ) )
            {
                // Unknown so add it.
                this.mCache.Enqueue( pElement );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Pops the oldest element in the queue.
        /// </summary>
        /// <returns>The popped element.</returns>
        public T Dequeue()
        {
            if ( this.mCache.Count == 0 )
            {
                return default(T);
            }

            T lElement = this.mCache.Dequeue();
            this.mAlreadyAdded.Remove( lElement );
            return lElement;
        }

        /// <summary>
        /// Gets the oldest element without popping it.
        /// </summary>
        /// <returns>The peeked element.</returns>
        public T Peek()
        {
            return this.mCache.Peek();
        }
        
        /// <summary>
        /// Turns the queue into an array.
        /// </summary>
        /// <returns>The resulting array.</returns>
        public T[] ToArray()
        {
            return this.mCache.ToArray();
        }

        #region Methods IEnumerable

        /// <summary>
        /// Gets the queue typed enumerator.
        /// </summary>
        /// <returns>The typed enumerator.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return this.mCache.GetEnumerator();
        }

        /// <summary>
        /// Gets the queue untyped enumerator.
        /// </summary>
        /// <returns>The untyped enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion Methods IEnumerable

        #region Methods ICollection

        /// <summary>
        /// Copies the queue into the given array starting at the given index.
        /// </summary>
        /// <param name="pArray">The array to fill.</param>
        /// <param name="pStartingIndex">The array starting index.</param>
        void ICollection.CopyTo(Array pArray, int pStartingIndex)
        {
            (this.mCache as ICollection).CopyTo( pArray, pStartingIndex );
        }

        #endregion Methods ICollection

        #endregion Methods
    }
}
