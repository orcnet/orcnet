﻿using System.Collections;
using System.Collections.Generic;

namespace OrcNet.Core.Datastructures
{
    /// <summary>
    /// Definition of the <see cref="IQueue{T}"/> interface.
    /// </summary>
    public interface IQueue<T> : IEnumerable<T>, ICollection, IEnumerable
    {
        #region Methods

        /// <summary>
        /// Adds a new element into the queue.
        /// </summary>
        /// <param name="pElement">The new element to add.</param>
        /// <returns>True if not already in, false otherwise.</returns>
        bool Enqueue(T pElement);

        /// <summary>
        /// Pops the oldest element in the queue.
        /// </summary>
        /// <returns>The popped element.</returns>
        T Dequeue();

        #endregion Methods
    }
}
