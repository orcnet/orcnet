﻿using System.Diagnostics;

namespace OrcNet.Core.Timer
{
    /// <summary>
    /// Base timer to measure time and time intervals.
    /// </summary>
    public class BaseTimer
    {
        #region Fields

        /// <summary>
        /// Stores the time of last call to Start or Reset.
        /// </summary>
        protected double  mCurrentTime;

        /// <summary>
        /// Stores the flag indicating whether the timer is running or not.
        /// </summary>
        protected bool    mIsRunning;

        /// <summary>
        /// Stores the amount of call to start since the last reset.
        /// </summary>
        protected int     mCycleCount;

        /// <summary>
        /// Stores the accumulated elapsed time.
        /// </summary>
        protected double  mTotalDuration;

        /// <summary>
        /// Stores the lowest duration between a start and a stop calls in micro seconds.
        /// </summary>
        protected double  mMinDuration;

        /// <summary>
        /// Stores the highest duration between a start and a stop calls in micro seconds.
        /// </summary>
        protected double  mMaxDuration;

        /// <summary>
        /// Stores the last recorded duration between start and end calls.
        /// </summary>
        protected double  mLastDuration;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the timer is currently running or not
        /// that is, has a start value.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return this.mIsRunning;
            }
        }

        /// <summary>
        /// Gets the amount of call to start since the last reset.
        /// </summary>
        public int CycleCount
        {
            get
            {
                return this.mCycleCount;
            }
        }

        /// <summary>
        /// Gets the average delay at every call to stop in microseconds.
        /// This won't be accurate if the timer is not stopped.
        /// </summary>
        public virtual double AverageTime
        {
            get
            {
                if ( this.mCycleCount == 0 )
                {
                    return 0.0;
                }

                if ( this.IsRunning )
                {
                    // Assure accuracy by stopping what will update TotalDuration.
                    this.End();
                }

                return this.mTotalDuration / (double)this.mCycleCount;
            }
        }

        /// <summary>
        /// Gets the lowest duration between a start and a stop calls in micro seconds.
        /// </summary>
        public double MinDuration
        {
            get
            {
                return this.mMinDuration;
            }
        }

        /// <summary>
        /// Gets the highest duration between a start and a stop calls in micro seconds.
        /// </summary>
        public double MaxDuration
        {
            get
            {
                return this.mMaxDuration;
            }
        }

        /// <summary>
        /// Gets the last recorded duration between start and end calls.
        /// </summary>
        public virtual double LastDuration
        {
            get
            {
                return this.mLastDuration;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseTimer"/> class.
        /// </summary>
        public BaseTimer()
        {
            this.mCycleCount    = 0;
            this.mIsRunning     = false;
            this.mMinDuration   = 1e9;
            this.mMaxDuration   = 0.0;
            this.mLastDuration  = 0.0;
            this.mTotalDuration = 0.0;
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Starts the timer.
        /// </summary>
        /// <returns>The current time in microseconds</returns>
        public virtual double Start()
        {
            this.mIsRunning = true;
            this.mCycleCount++;
            this.mCurrentTime = this.GetCurrentTime();
            return this.mCurrentTime;
        }

        /// <summary>
        /// Stops the timer
        /// </summary>
        /// <returns>The elasped time since the start call in microseconds.</returns>
        public virtual double End()
        {
            this.mLastDuration = this.GetCurrentTime() - this.mCurrentTime;
            this.mTotalDuration += this.mLastDuration;
            this.mMinDuration = System.Math.Min( this.mLastDuration, this.mMinDuration );
            this.mMaxDuration = System.Math.Max( this.mLastDuration, this.mMaxDuration );
            this.mIsRunning = false;
            return this.mLastDuration;
        }

        /// <summary>
        /// Resets ans starts immediately the timer
        /// </summary>
        public void Reset()
        {
            this.Start();
            this.mCycleCount    = 0;
            this.mIsRunning     = false;
            this.mMinDuration   = 1e9;
            this.mMaxDuration   = 0.0;
            this.mLastDuration  = 0.0;
            this.mTotalDuration = 0.0;
        }

        /// <summary>
        /// Returns the current time in microseconds.
        /// </summary>
        /// <returns>The current time in microseconds</returns>
        protected double GetCurrentTime()
        {
            // GetTimestamp is the managed version of QueryPerformanceCounter.
            double lTimeStamp = (double)Stopwatch.GetTimestamp();
            // Frequency is the managed version of QueryPerformanceFrequency.
            return lTimeStamp / (double)Stopwatch.Frequency * 1e6;
        }

        #endregion Methods
    }
}
