﻿using System;

namespace OrcNet.Core.Plugins
{
    /// <summary>
    /// Definition of the <see cref="CachePolicyType"/> enumeration.
    /// </summary>
    [Flags]
    public enum CachePolicyType
    {
        /// <summary>
        /// Checks for file operation(s) to respond accordingly.
        /// </summary>
        FileWatch = 1,

        /// <summary>
        /// Checks for elapsed time from plugin load time to remove or reload each interval.
        /// </summary>
        TimeInterval = 2
    }
}
