﻿namespace OrcNet.Core.Plugins
{
    /// <summary>
    /// Definition of the <see cref="CachePolicy"/> class.
    /// </summary>
    public class CachePolicy
    {
        #region Properties

        /// <summary>
        /// Gets or sets the cache behavior regarding to plugins.
        /// </summary>
        public CachePolicyType PolicyType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the interval after which plugins will be removed from cache or reloaded 
        /// (depending on AutoReloadOnCacheExpiration value) if type of cache is TimeInterval
        /// </summary>
        public double CacheExpirationInterval
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag indicating whether to resets assembly load time every time assembly is accessed by GetPlugin method or not.
        /// </summary>
        public bool AllowSlidingExpiration
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the time interval to start listening for changes on the same file (resolves multiple changed even raising issue of FileSystemWatcher)
        /// </summary>
        public double FilesystemWatcherDelay
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the flag indicating whether assemblies will be loaded to cache each expiration interval or simply removed
        /// </summary>
        public bool AutoReloadOnCacheExpiration
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CachePolicy"/> class.
        /// </summary>
        public CachePolicy()
        {
            this.PolicyType = CachePolicyType.FileWatch;
            this.FilesystemWatcherDelay  = 1000;
            this.AutoReloadOnCacheExpiration = true;
            this.CacheExpirationInterval = 5000;
            this.AllowSlidingExpiration  = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates a new CachePolicy instance which is based on time interval expiration.
        /// </summary>
        /// <param name="pAutoReloadOnCacheExpiration">The flag indicating whether assemblies will be loaded to cache each expiration interval or simply removed</param>
        /// <param name="pCacheExpirationInterval">The interval after which plugins will be removed from cache or reloaded.</param>
        /// <param name="pAllowSlidingExpiration">The flag indicating whether to resets assembly load time every time assembly is accessed by GetPlugin method or not.</param>
        /// <returns>The new time interval based cache policy.</returns>
        public static CachePolicy GetTimeIntervalCachePolicy(bool pAutoReloadOnCacheExpiration = true, double pCacheExpirationInterval = 5000, bool pAllowSlidingExpiration = false)
        {
            return new CachePolicy()
            {
                PolicyType = CachePolicyType.TimeInterval,
                AutoReloadOnCacheExpiration = pAutoReloadOnCacheExpiration,
                CacheExpirationInterval     = pCacheExpirationInterval,
                AllowSlidingExpiration      = pAllowSlidingExpiration
            };
        }

        /// <summary>
        /// Creates a new CachePolicy instance which is based on plugin assembly file changes
        /// </summary>
        /// <param name="pFilesystemWatcherDelay">The delay to wait before to check for assembly changes.</param>
        /// <returns>The new assembly file changes based cache policy.</returns>
        public static CachePolicy GetFileWatchCachePolicy(double pFilesystemWatcherDelay = 1000)
        {
            return new CachePolicy()
            {
                PolicyType = CachePolicyType.FileWatch,
                FilesystemWatcherDelay = pFilesystemWatcherDelay
            };
        }

        /// <summary>
        /// Creates a new CachePolicy instance which is based on time interval expiration and plugin assembly file changes.
        /// </summary>
        /// <param name="pAutoReloadOnCacheExpiration">The flag indicating whether assemblies will be loaded to cache each expiration interval or simply removed</param>
        /// <param name="pCacheExpirationInterval">The interval after which plugins will be removed from cache or reloaded.</param>
        /// <param name="pAllowSlidingExpiration">The flag indicating whether to resets assembly load time every time assembly is accessed by GetPlugin method or not.</param>
        /// <param name="pFilesystemWatcherDelay">The delay to wait before to check for assembly changes.</param>
        /// <returns>The new hybrid cache policy.</returns>
        public static CachePolicy GetHybridCachePolicy(bool pAutoReloadOnCacheExpiration = true, double pCacheExpirationInterval = 5000, bool pAllowSlidingExpiration = false, double pFilesystemWatcherDelay = 1000)
        {
            return new CachePolicy()
            {
                PolicyType = CachePolicyType.TimeInterval | CachePolicyType.FileWatch,
                AutoReloadOnCacheExpiration = pAutoReloadOnCacheExpiration,
                CacheExpirationInterval     = pCacheExpirationInterval,
                AllowSlidingExpiration      = pAllowSlidingExpiration,
                FilesystemWatcherDelay      = pFilesystemWatcherDelay
            };

        }

        #endregion Methods
    }
}
