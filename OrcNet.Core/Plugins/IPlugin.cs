﻿using System;

namespace OrcNet.Core.Plugins
{
    /// <summary>
    /// Definition of the <see cref="IPlugin"/> class.
    /// </summary>
    public interface IPlugin : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the plugin's services.
        /// </summary>
        IPluginService[] Services
        {
            get;
        }

        /// <summary>
        /// Gets the file path of the plugin assembly.
        /// </summary>
        string AssemblyLocation
        {
            get;
        }

        /// <summary>
        /// Gets the plugin's configuration.
        /// </summary>
        PluginConfiguration PluginConfiguration
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Initializes the plugin and its services.
        /// </summary>
        /// <returns></returns>
        bool Initialize();

        /// <summary>
        /// Gets a plugin's service corresponding to the given generic type. Can be from whatever plugin loaded.
        /// </summary>
        /// <typeparam name="T">The service type.</typeparam>
        /// <returns>The plugin service, null otherwise.</returns>
        T GetService<T>() where T : class, IPluginService;

        #endregion Methods
    }
}
