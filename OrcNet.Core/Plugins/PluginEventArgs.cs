﻿using System;

namespace OrcNet.Core.Plugins
{
    /// <summary>
    /// Definition of the <see cref="PluginEventArgs"/> class.
    /// </summary>
    public class PluginEventArgs: EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the plugin assembly's path that raised the event.
        /// </summary>
        private string mAssemblyPath;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the plugin assembly's path that raised the event.
        /// </summary>
        public string AssemblyPath
        {
            get
            {
                return this.mAssemblyPath;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginEventArgs"/> class.
        /// </summary>
        /// <param name="pAssemblyPath">The plugin assembly's path that raised the event.</param>
        public PluginEventArgs(string pAssemblyPath)
        {
            this.mAssemblyPath = pAssemblyPath;
        }

        #endregion Constructor
    }
}
