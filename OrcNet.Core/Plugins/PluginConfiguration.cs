﻿namespace OrcNet.Core.Plugins
{
    /// <summary>
    /// Definition of the <see cref="PluginConfiguration"/> class.
    /// </summary>
    public sealed class PluginConfiguration
    {
        #region Fields

        /// <summary>
        /// Stores the plugins folder.
        /// </summary>
        private string mPluginsFolder;

        /// <summary>
        /// Stores the flag indicating whether assembly file handle must be kept or not.
        /// </summary>
        private bool mKeepFileHandle;

        /// <summary>
        /// Stores the flag indicating whether the plugins folder must be created or not.
        /// </summary>
        private bool mCreatePluginsFolder;

        /// <summary>
        /// Stores the plugins configuration cache.
        /// </summary>
        private ConfigurationCache mCache;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the plugins folder.
        /// </summary>
        public string PluginsFolder
        {
            get
            {
                return this.mPluginsFolder;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether assembly file handle must be kept or not.
        /// </summary>
        public bool KeepFileHandle
        {
            get
            {
                return this.mKeepFileHandle;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the plugins folder must be created or not.
        /// </summary>
        public bool CreatePluginsFolder
        {
            get
            {
                return this.mCreatePluginsFolder;
            }
        }

        /// <summary>
        /// Gets the plugins configuration cache.
        /// </summary>
        public ConfigurationCache Cache
        {
            get
            {
                return this.mCache;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginConfiguration"/> class.
        /// </summary>
        /// <param name="pPluginFolder">The plugins folder.</param>
        /// <param name="pCreatePluginFolder">The flag indicating whether the plugins folder must be created or not.</param>
        /// <param name="pKeepFileHandle">The flag indicating whether assembly file handle must be kept or not.</param>
        /// <param name="pCache">The plugins configuration cache.</param>
        public PluginConfiguration(string pPluginFolder = "Plugins", bool pCreatePluginFolder = true, bool pKeepFileHandle = false, ConfigurationCache pCache = null)
        {
            this.mPluginsFolder = pPluginFolder;
            this.mKeepFileHandle = pKeepFileHandle;
            this.mCreatePluginsFolder = pCreatePluginFolder;
            this.mCache = pCache;

            if ( this.mCache == null )
            {
                this.mCache = new ConfigurationCache();
            }
        }

        #endregion Constructor
    }
}
