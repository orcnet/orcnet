﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OrcNet.Core.Plugins
{
    /// <summary>
    /// Plugin event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The event sender.</param>
    /// <param name="pEventArg">The event arguments.</param>
    public delegate void PluginEventDelegate(object pSender, PluginEventArgs pEventArg);

    /// <summary>
    /// Definition of the <see cref="IPluginManager"/> interface. 
    /// </summary>
    public interface IPluginManager : IService, IDisposable
    {
        #region Events

        /// <summary>
        /// Event triggerd on plugin loaded. (depending on the policy, can occur multiple times)
        /// </summary>
        event PluginEventDelegate PluginLoaded;

        /// <summary>
        /// Event triggered on plugin unloaded. (depending on the policy, can occur multiple times)
        /// </summary>
        event PluginEventDelegate PluginUnloaded;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the plugins default configuration.
        /// </summary>
        PluginConfiguration DefaultConfiguration
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the plugins folder must be created or not.
        /// </summary>
        bool CreatePluginsFolder
        {
            get;
        }

        /// <summary>
        /// Gets the path of the folder where plugin assemblies are stored
        /// </summary>
        string PluginsFolder
        {
            get;
        }

        /// <summary>
        /// Gets the plugin manager cache policy.
        /// </summary>
        CachePolicy CachePolicy
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the file handle must be kept or not.
        /// </summary>
        bool KeepFileHandle
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Initializes the plugin manager.
        /// NOTE: If parameters remain null, by default, a configuration file will be read in "Resources".
        /// </summary>
        /// <param name="pPluginsFolder">The plugin folder path.</param>
        /// <param name="pCachePolicy">The plugin manager cache policy.</param>
        /// <param name="pCreatePluginsFolder">The flag indicating whether the plugins folder must be created or not.</param>
        /// <param name="pKeepFileHandle">The flag indicating whether the file handle must be kept or not.</param>
        /// <returns>True if successful, false otherwise.</returns>
        bool Initialize(string pPluginsFolder = null, CachePolicy pCachePolicy = null, bool? pCreatePluginsFolder = null, bool? pKeepFileHandle = null);

        /// <summary>
        /// Retrieves all plugins of the given base type corresponding to the supplied name.
        /// </summary>
        /// <typeparam name="T">Type of plugin base implementing IPlugin interface.</typeparam>
        /// <param name="pPluginName">The plugin assembly filename or full path to plugin assembly</param>
        /// <param name="pSubfolder">An optional subfolder name where plugin assembly is located inside the plugins folder</param>
        /// <returns>The set of plugins.</returns>
        IEnumerable<T> GetPlugin<T>(string pPluginName, string pSubfolder = null) where T : class, IPlugin;

        /// <summary>
        /// Retrieves all plugins of the given base type corresponding to the supplied name.
        /// </summary>
        /// <param name="pPluginName">The plugin assembly filename or full path to plugin assembly</param>
        /// <param name="pSubfolder">An optional subfolder name where plugin assembly is located inside the plugins folder</param>
        /// <returns>The set of plugins.</returns>
        IEnumerable<IPlugin> GetPlugin(string pPluginName, string pSubfolder = null);

        /// <summary>
        /// Retrieves all plugins corresponding to the supplied assembly file information.
        /// </summary>
        /// <param name="pPluginAssemblyFileInfo">The plugin assembly file info.</param>
        /// <returns>The set of plugins.</returns>
        IEnumerable<IPlugin> GetPlugin(FileInfo pPluginAssemblyFileInfo);

        /// <summary>
        /// Retrieves all plugins of the given base type corresponding to the supplied assembly file information.
        /// </summary>
        /// <typeparam name="T">The type of the plugin base implementing IPlugin interface</typeparam>
        /// <param name="pPluginAssemblyFileInfo">The plugin assembly file info.</param>
        /// <returns>The set of plugins.</returns>
        IEnumerable<T> GetPlugin<T>(FileInfo pPluginAssemblyFileInfo) where T : class, IPlugin;

        /// <summary>
        /// Loads every single plugins in the target plugin folder given either through the config file or as initial parameter.
        /// </summary>
        /// <returns>The amount of loaded plugins. 0 meaning no plugins loaded.</returns>
        int LoadAllPlugins();

        #endregion Methods
    }
}
