﻿namespace OrcNet.Core.Plugins
{
    /// <summary>
    /// Definition of the <see cref="IPluginService"/> interface.
    /// </summary>
    public interface IPluginService : IService
    {
        #region Properties

        /// <summary>
        /// Gets the plugin service owner.
        /// </summary>
        IPlugin Owner
        {
            get;
        }

        #endregion Properties
    }
}
