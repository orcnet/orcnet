﻿namespace OrcNet.Core.Plugins
{
    /// <summary>
    /// Definition of the <see cref="ConfigurationCache"/> class.
    /// </summary>
    public sealed class ConfigurationCache
    {
        #region Fields

        /// <summary>
        /// Stores the plugin cache policy.
        /// </summary>
        private CachePolicyType mType;

        /// <summary>
        /// Stores the flag indicating whether to resets assembly load time every time assembly is accessed by GetPlugin method or not.
        /// </summary>
        private bool mAllowSlidingExpiration;

        /// <summary>
        /// Stores the interval after which plugins will be removed from cache or reloaded 
        /// (depending on AutoReloadOnCacheExpiration value) if type of cache is TimeInterval
        /// </summary>
        private double mCacheExpirationInterval;

        /// <summary>
        /// Stores the time interval to start listening for changes on the same file (resolves multiple changed even raising issue of FileSystemWatcher)
        /// </summary>
        private double mFilesystemWatcherDelay;

        /// <summary>
        /// Stores the flag indicating whether assemblies will be loaded to cache each expiration interval or simply removed
        /// </summary>
        private bool mAutoReloadOnCacheExpiration;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the plugin cache policy.
        /// </summary>
        public CachePolicyType Type
        {
            get
            {
                return this.mType;
            }
        }

        /// <summary>
        /// Gets the interval after which plugins will be removed from cache or reloaded 
        /// (depending on AutoReloadOnCacheExpiration value) if type of cache is TimeInterval
        /// </summary>
        public double CacheExpirationInterval
        {
            get
            {
                return this.mCacheExpirationInterval;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether to resets assembly load time every time assembly is accessed by GetPlugin method or not.
        /// </summary>
        public bool AllowSlidingExpiration
        {
            get
            {
                return this.mAllowSlidingExpiration;
            }
        }

        /// <summary>
        /// Gets the time interval to start listening for changes on the same file (resolves multiple changed even raising issue of FileSystemWatcher)
        /// </summary>
        public double FilesystemWatcherDelay
        {
            get
            {
                return this.mFilesystemWatcherDelay;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether assemblies will be loaded to cache each expiration interval or simply removed
        /// </summary>
        public bool AutoReloadOnCacheExpiration
        {
            get
            {
                return this.mAutoReloadOnCacheExpiration;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationCache"/> class.
        /// </summary>
        /// <param name="pType">The plugin cache policy.</param>
        /// <param name="pCacheExpirationInterval">The interval after which plugins will be removed from cache or reloaded</param>
        /// <param name="pAllowSlidingExpiration">The flag indicating whether to resets assembly load time every time assembly is accessed by GetPlugin method or not.</param>
        /// <param name="pAutoReloadOnCacheExpiration">The flag indicating whether assemblies will be loaded to cache each expiration interval or simply removed.</param>
        /// <param name="pFilesystemWatcherDelay">The time interval to start listening for changes on the same file (resolves multiple changed even raising issue of FileSystemWatcher)</param>
        public ConfigurationCache(CachePolicyType pType = CachePolicyType.FileWatch, double pCacheExpirationInterval = 5000, bool pAllowSlidingExpiration = true, bool pAutoReloadOnCacheExpiration = true, double pFilesystemWatcherDelay = 1000)
        {
            this.mType = pType;
            this.mCacheExpirationInterval     = pCacheExpirationInterval;
            this.mAllowSlidingExpiration      = pAllowSlidingExpiration;
            this.mAutoReloadOnCacheExpiration = pAutoReloadOnCacheExpiration;
            this.mFilesystemWatcherDelay      = pFilesystemWatcherDelay;
        }

        #endregion Constructor
    }
}
