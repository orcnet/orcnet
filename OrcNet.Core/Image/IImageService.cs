﻿namespace OrcNet.Core.Image
{
    /// <summary>
    /// Base image service interface definition.
    /// </summary>
    public interface IImageService : IService
    {
        #region Methods

        /// <summary>
        /// Checks whether the supplied image binary data are in HDR format or not.
        /// </summary>
        /// <param name="pData">The image binary data</param>
        /// <returns>True if in HDR format, false otherwise.</returns>
        bool IsHDRFormat(byte[] pData);

        /// <summary>
        /// Loads an image from binary data being in radiance HDR format.
        /// </summary>
        /// <param name="pData">The image binary data</param>
        /// <param name="pWidth">The extracted image width</param>
        /// <param name="pHeight">The extracted image height</param>
        /// <param name="pChannels">The extracted image channel count</param>
        /// <returns>The decompressed HDR image.</returns>
        byte[] LoadHDRFormat(byte[] pData, out int pWidth, out int pHeight, out int pChannels);

        /// <summary>
        /// Loads any other image format from binary data like Jpg, Png or other LDR formats.
        /// </summary>
        /// <param name="pData">The image binary data</param>
        /// <param name="pWidth">The extracted image width</param>
        /// <param name="pHeight">The extracted image height</param>
        /// <param name="pChannels">The extracted image channel count</param>
        /// <returns>The decompressed image.</returns>
        byte[] Load(byte[] pData, out int pWidth, out int pHeight, out int pChannels);

        /// <summary>
        /// Loads any other image format from binary data like Jpg, Png or other LDR formats.
        /// </summary>
        /// <param name="pFullpath">The image fullname</param>
        /// <param name="pWidth">The extracted image width</param>
        /// <param name="pHeight">The extracted image height</param>
        /// <param name="pChannels">The extracted image channel count</param>
        /// <returns>The decompressed image.</returns>
        byte[] Load(string pFullpath, out int pWidth, out int pHeight, out int pChannels);

        /// <summary>
        /// Saves the given data to the supplied filename.
        /// </summary>
        /// <param name="pData">The pixel data to save.</param>
        /// <param name="pFileName">The filename to save the pixels to.</param>
        bool Save(byte[] pData, string pFileName);

        #endregion Methods
    }
}
