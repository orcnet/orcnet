﻿using System;

namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="KeyEventArgs"/> class.
    /// </summary>
    public class KeyEventArgs : EventArgs
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the Control button has been pressed.
        /// </summary>
        public bool IsCtrl
        {
            get
            {
                return (this.Modifiers & KeyModifiers.Ctrl) == KeyModifiers.Ctrl;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Alt button has been pressed.
        /// </summary>
        public bool IsAlt
        {
            get
            {
                return (this.Modifiers & KeyModifiers.Alt) == KeyModifiers.Alt;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Shift button has been pressed.
        /// </summary>
        public bool IsShift
        {
            get
            {
                return (this.Modifiers & KeyModifiers.Shift) == KeyModifiers.Shift;
            }
        }

        /// <summary>
        /// Gets the key [0, 255]
        /// </summary>
        public byte Key
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the key modifier(s).
        /// </summary>
        public KeyModifiers Modifiers
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyEventArgs"/> class.
        /// </summary>
        /// <param name="pKey">The key [0, 255]</param>
        /// <param name="pModifiers">The key modifier(s).</param>
        public KeyEventArgs(byte pKey, KeyModifiers pModifiers)
        {
            this.Key = pKey;
            this.Modifiers = pModifiers;
        }

        #endregion Constructor
    }
}
