﻿using System;

namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="IView"/> interface.
    /// </summary>
    public interface IView : IRenderable, IResizable, IInputHandler, IDisposable
    {
        #region Methods

        /// <summary>
        /// Initializes the view.
        /// </summary>
        /// <param name="pParameters">The view parameters.</param>
        void InitializeView(ViewParameters pParameters);
        
        #endregion Methods
    }
}
