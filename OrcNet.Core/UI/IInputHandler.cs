﻿namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="IInputHandler"/> interface.
    /// </summary>
    public interface IInputHandler
    {
        #region Methods

        /// <summary>
        /// Delegate called on idle time.
        /// </summary>
        void OnIdle();

        /// <summary>
        /// Mouse down event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        void OnMouseDown(object pSender, MouseEventArgs pEventArgs);

        /// <summary>
        /// Mouse up event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        void OnMouseUp(object pSender, MouseEventArgs pEventArgs);

        /// <summary>
        /// Mouse move event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        void OnMouseMove(object pSender, MouseEventArgs pEventArgs);

        /// <summary>
        /// Mouse wheel event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        void OnMouseWheel(object pSender, MouseEventArgs pEventArgs);

        /// <summary>
        /// Key down event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        void OnKeyDown(object pSender, KeyEventArgs pEventArgs);

        /// <summary>
        /// Key up event handler
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        void OnKeyUp(object pSender, KeyEventArgs pEventArgs);

        #endregion Methods
    }
}
