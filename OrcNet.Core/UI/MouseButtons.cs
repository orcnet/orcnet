﻿namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="MouseButtons"/> enumeration.
    /// </summary>
    public enum MouseButtons
    {
        /// <summary>
        /// No mouse button.
        /// </summary>
        None = 0,

        /// <summary>
        /// Left mouse button.
        /// </summary>
        Left,

        /// <summary>
        /// Middle mouse button.
        /// </summary>
        Middle,

        /// <summary>
        /// Right mouse button.
        /// </summary>
        Right,

        /// <summary>
        /// First mouse XButton
        /// </summary>
        XButton1,

        /// <summary>
        /// Second mouse XButton
        /// </summary>
        XButton2
    }
}
