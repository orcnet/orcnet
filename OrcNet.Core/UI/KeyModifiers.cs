﻿namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="KeyModifiers"/> enumeration.
    /// ORing-able.
    /// </summary>
    public enum KeyModifiers : int
    {
        /// <summary>
        /// No modifiers.
        /// </summary>
        None = 0x00,

        /// <summary>
        /// Shift modifier.
        /// </summary>
        Shift = 0x01,

        /// <summary>
        /// Control modifier.
        /// </summary>
        Ctrl = 0x02,

        /// <summary>
        /// Alt modifier.
        /// </summary>
        Alt = 0x04
    }
}
