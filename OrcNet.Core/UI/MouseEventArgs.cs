﻿using System;

namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="MouseEventArgs"/> class.
    /// </summary>
    public class MouseEventArgs : EventArgs
    {
        #region Properties

        /// <summary>
        /// Gets the mouse X screen position.
        /// </summary>
        public int X
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the mouse Y screen position.
        /// </summary>
        public int Y
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the mouse button.
        /// </summary>
        public MouseButtons Button
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the click count.
        /// </summary>
        public int ClickCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the mouse wheel signed delta.
        /// </summary>
        public int Delta
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        /// <param name="pButton">The mouse button.</param>
        /// <param name="pClickCount">The click count.</param>
        /// <param name="pX">The mouse X screen position.</param>
        /// <param name="pY">The mouse Y screen position.</param>
        /// <param name="pDelta">The mouse wheel signed delta.</param>
        public MouseEventArgs(MouseButtons pButton, int pClickCount, int pX, int pY, int pDelta)
        {
            this.X = pX;
            this.Y = pY;
            this.Delta = pDelta;
            this.Button = pButton;
            this.ClickCount = pClickCount;
        }

        #endregion Constructor
    }
}
