﻿namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="IResizable"/> interface.
    /// </summary>
    public interface IResizable
    {
        #region Properties

        /// <summary>
        /// Gets the element width.
        /// </summary>
        int Width
        {
            get;
        }

        /// <summary>
        /// Gets the element height.
        /// </summary>
        int Height
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Resize the element.
        /// </summary>
        /// <param name="pWidth">The window width.</param>
        /// <param name="pHeight">The window height.</param>
        void Resize(int pWidth, int pHeight);

        #endregion Methods
    }
}
