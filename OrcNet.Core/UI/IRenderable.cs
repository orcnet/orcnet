﻿namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="IRenderable"/> interface.
    /// </summary>
    public interface IRenderable
    {
        #region Methods

        /// <summary>
        /// Render the renderable.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute time.</param>
        /// <param name="pDeltaTime">The elasped time since last render.</param>
        void Render(double pAbsoluteTime, double pDeltaTime);

        #endregion Methods
    }
}
