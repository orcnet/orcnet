﻿using System;

namespace OrcNet.Core.UI
{
    /// <summary>
    /// Definition of the <see cref="ViewParameters"/> class.
    /// </summary>
    public class ViewParameters
    {
        #region Fields

        /// <summary>
        /// Stores the view update frequency in Hz.
        /// </summary>
        private double mFrequency;

        /// <summary>
        /// Stores the view width.
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the view height.
        /// </summary>
        private int mHeight;

        /// <summary>
        /// Stores the OpenGL minor version.
        /// </summary>
        private int mMinor;

        /// <summary>
        /// Stores the OpenGL major version.
        /// </summary>
        private int mMajor;

        /// <summary>
        /// Stores the flag indicating whether the debug log must be processed or not.
        /// </summary>
        private bool mMustDebug;

        /// <summary>
        /// Stores the flag indicating whether an alpha channel must be used or not.
        /// </summary>
        private bool mUseAlpha;

        /// <summary>
        /// Stores the flag indicating whether a depth buffer must be used or not.
        /// </summary>
        private bool mUseDepth;

        /// <summary>
        /// Stores the flag indicating whether stencil buffer must be used or not.
        /// </summary>
        private bool mUseStencil;

        /// <summary>
        /// Stores the flag indicating whether multisampling must be used or not.
        /// </summary>
        private bool mUseMultiSampling;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the view update frequency in Hz.
        /// </summary>
        public double Frequency
        {
            get
            {
                return this.mFrequency;
            }
            set
            {
                this.mFrequency = value;
            }
        }

        /// <summary>
        /// Gets the view width.
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
        }

        /// <summary>
        /// Gets the view height.
        /// </summary>
        public int Height
        {
            get
            {
                return this.mHeight;
            }
        }

        /// <summary>
        /// Gets the OpenGL version.
        /// Format : Tuple[Major, Minor]
        /// </summary>
        public Tuple<int, int> Version
        {
            get
            {
                return new Tuple<int, int>( this.mMajor, this.mMinor );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the debug log must be processed or not.
        /// </summary>
        public bool MustDebug
        {
            get
            {
                return this.mMustDebug;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether an alpha channel must be used or not.
        /// </summary>
        public bool UseAlpha
        {
            get
            {
                return this.mUseAlpha;
            }
            set
            {
                this.mUseAlpha = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether a depth buffer must be used or not.
        /// </summary>
        public bool UseDepth
        {
            get
            {
                return this.mUseDepth;
            }
            set
            {
                this.mUseDepth = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether stencil buffer must be used or not.
        /// </summary>
        public bool UseStencil
        {
            get
            {
                return this.mUseStencil;
            }
            set
            {
                this.mUseStencil = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether multisampling must be used or not.
        /// </summary>
        public bool UseMultiSampling
        {
            get
            {
                return this.mUseMultiSampling;
            }
            set
            {
                this.mUseMultiSampling = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewParameters"/> class.
        /// </summary>
        public ViewParameters()
        {
            this.mFrequency = 20; // 20Hz by default.
            this.mWidth  = 640;
            this.mHeight = 480;
            this.mMajor  = 3;
            this.mMinor  = 3;
            this.mMustDebug  = false;
            this.mUseAlpha   = false;
            this.mUseDepth   = false;
            this.mUseStencil = false;
            this.mUseMultiSampling = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Sets the version parameters.
        /// </summary>
        /// <param name="pMajor">The OpenGL major version.</param>
        /// <param name="pMinor">The OpenGL minor version.</param>
        /// <param name="pMustDebug">The flag indicating whether the debug log must be processed or not.</param>
        public ViewParameters SetVersion(int pMajor, int pMinor, bool pMustDebug = false)
        {
            this.mMajor = pMajor;
            this.mMinor = pMinor;
            this.mMustDebug = pMustDebug;

            return this;
        }

        /// <summary>
        /// Sets the view size parameters
        /// </summary>
        /// <param name="pWidth">The width</param>
        /// <param name="pHeight">The height.</param>
        public ViewParameters SetSize(int pWidth, int pHeight)
        {
            this.mWidth  = pWidth;
            this.mHeight = pHeight;

            return this;
        }

        #endregion Methods
    }
}
