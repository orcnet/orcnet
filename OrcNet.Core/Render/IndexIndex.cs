﻿namespace OrcNet.Core.Render
{
    /// <summary>
    /// Helper structure to access by index one of the indices.
    /// </summary>
    public struct IndexIndex
    {
        #region Fields

        /// <summary>
        /// Stores the array index
        /// </summary>
        private int mIndex;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexIndex"/> class.
        /// </summary>
        /// <param name="pIndex">The index index.</param>
        public IndexIndex(int pIndex)
        {
            this.mIndex = pIndex;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from VertexIndex to int
        /// </summary>
        /// <param name="pIndex">The index's index</param>
        public static implicit operator int(IndexIndex pIndex)
        {
            return pIndex.mIndex;
        }

        #endregion Methods
    }
}
