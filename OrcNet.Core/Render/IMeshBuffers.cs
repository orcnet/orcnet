﻿using OrcNet.Core.Math;
using OrcNet.Core.Resource;
using System;

namespace OrcNet.Core.Render
{
    /// <summary>
    /// Base Mesh buffers interface definition exposing a set of PipelineAttributeDescription
    /// that represent the vertices and indices of a mesh, each attribute 
    /// description being an attribute of a vertex of the mesh such as Position,
    /// Normal, Color and so on.
    /// </summary>
    public interface IMeshBuffers : IResourceCreatable, IMemoryProfilable, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the vertex count
        /// </summary>
        int VertexCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the index count
        /// </summary>
        int IndexCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the number of attributes per vertex.
        /// </summary>
        int AttributesPerVertex
        {
            get;
        }

        /// <summary>
        /// Gets the attribute buffer at the given index.
        /// </summary>
        /// <param name="pIndex">The buffer index.</param>
        /// <returns>The attribute buffer, null if beyond bounds.</returns>
        IPipelineAttributeDescription this[uint pIndex]
        {
            get;
        }

        /// <summary>
        /// Gets the indices buffer.
        /// </summary>
        IPipelineAttributeDescription IndicesBuffer
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the bounding box.
        /// </summary>
        IBox<float> Bounds
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new vertex attribute buffer.
        /// </summary>
        /// <param name="pNewBuffer">The new attributes buffer.</param>
        void AddAttributeBuffer(IPipelineAttributeDescription pNewBuffer);

        #endregion Methods
    }
}
