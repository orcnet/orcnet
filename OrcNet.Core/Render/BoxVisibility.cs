﻿namespace OrcNet.Core.Render
{
    /// <summary>
    /// Box visibility enumeration definition informing
    /// how a Box is regarding to a frustum.
    /// </summary>
    public enum BoxVisibility
    {
        /// <summary>
        /// The bounding box is fully visible
        /// </summary>
        FULLY_VISIBLE = 0,

        /// <summary>
        /// The bounding box is partially visible
        /// </summary>
        PARTIALLY_VISIBLE,

        /// <summary>
        /// The bounding box is invisible
        /// </summary>
        INVISIBLE
    }
}
