﻿using OrcNet.Core.Resource;
using System;

namespace OrcNet.Core.Render
{
    /// <summary>
    /// Base interface for a render pass used to draw a mseh.
    /// </summary>
    public interface IPipelinePass : IResourceCreatable, IMemoryProfilable, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the pass is the current being used or not.
        /// </summary>
        bool IsCurrent
        {
            get;
        }

        /// <summary>
        /// Gets the pass Identifier.
        /// </summary>
        uint Id
        {
            get;
        }

        /// <summary>
        /// Gets the pipeline description at the given index.
        /// </summary>
        /// <param name="pIndex">The description index.</param>
        /// <returns></returns>
        IPipelineDescription this[PipelineDescriptionIndex pIndex]
        {
            get;
        }

        #endregion Properties
    }
}
