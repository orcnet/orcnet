﻿namespace OrcNet.Core.Render
{
    /// <summary>
    /// Pipeline description index structure definition.
    /// </summary>
    public struct PipelineDescriptionIndex
    {
        #region Fields

        /// <summary>
        /// Stores the description index
        /// </summary>
        private int mIndex;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineDescriptionIndex"/> class.
        /// </summary>
        /// <param name="pIndex">The description index.</param>
        public PipelineDescriptionIndex(int pIndex)
        {
            this.mIndex = pIndex;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from PipelineDescriptionIndex to int.
        /// </summary>
        /// <param name="pIndex">The description index</param>
        public static implicit operator int(PipelineDescriptionIndex pIndex)
        {
            return pIndex.mIndex;
        }

        #endregion Methods
    }
}
