﻿using System;

namespace OrcNet.Core.Render
{
    /// <summary>
    /// Base interface mesh class definition.
    /// </summary>
    public interface IMesh : IMemoryProfilable, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the actual vertex count different from the allocated one.
        /// </summary>
        int VertexCount
        {
            get;
        }

        /// <summary>
        /// Gets the actual index count different from the allocated one.
        /// </summary>
        int IndexCount
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Resizes the vertices and indices caches but only if not smaller than current ones.
        /// </summary>
        /// <param name="pVertexCount">The new vertex count.</param>
        /// <param name="pIndexCount">The new index count.</param>
        /// <returns>True if resized, false otherwise.</returns>
        bool Resize(int pVertexCount, int pIndexCount);

        /// <summary>
        /// Clears the mesh's vertices and indices caches.
        /// </summary>
        void Clear();

        #endregion Methods
    }
}
