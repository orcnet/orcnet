﻿namespace OrcNet.Core.Render
{
    /// <summary>
    /// Helper structure to access by index one of the vertices.
    /// </summary>
    public struct VertexIndex
    {
        #region Fields

        /// <summary>
        /// Stores the array index
        /// </summary>
        private int mIndex;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="VertexIndex"/> class.
        /// </summary>
        /// <param name="pIndex">The vertex index.</param>
        public VertexIndex(int pIndex)
        {
            this.mIndex = pIndex;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from VertexIndex to int
        /// </summary>
        /// <param name="pIndex">The vertex index</param>
        public static implicit operator int(VertexIndex pIndex)
        {
            return pIndex.mIndex;
        }

        #endregion Methods
    }
}
