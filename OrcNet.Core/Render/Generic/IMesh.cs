﻿namespace OrcNet.Core.Render.Generic
{
    /// <summary>
    /// Base generic mesh interface definition 
    /// </summary>
    /// <typeparam name="V"></typeparam>
    /// <typeparam name="I"></typeparam>
    public interface IMesh<V, I> : IMesh where V : struct // Struct to be able to determine the size.
                                         where I : struct
    {
        #region Properties

        /// <summary>
        /// Gets the vertex at the given array index.
        /// </summary>
        /// <param name="pIndex">The array index of the requested vertex.</param>
        /// <returns>The vertex.</returns>
        V this[VertexIndex pIndex]
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the index at the given array index.
        /// </summary>
        /// <param name="pIndex">The array index of the requested index.</param>
        /// <returns>The index.</returns>
        I this[IndexIndex pIndex]
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new vertex to that mesh.
        /// </summary>
        /// <param name="pNewVertex">The new vertex.</param>
        void AddVertex(V pNewVertex);

        /// <summary>
        /// Adds a set of vertices to that mesh.
        /// </summary>
        /// <param name="pNewVertices">The new vertices.</param>
        void AddVertices(V[] pNewVertices);

        /// <summary>
        /// Adds a new index to that mesh.
        /// </summary>
        /// <param name="pIndex">The new index.</param>
        void AddIndex(I pIndex);

        #endregion Methods
    }
}
