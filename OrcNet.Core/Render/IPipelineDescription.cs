﻿using OrcNet.Core.Resource;
using System;

namespace OrcNet.Core.Render
{
    /// <summary>
    /// Base pipeline description interface.
    /// For now more like a string type interface as OpenGL and DirectX differs
    /// at this level but both handle such a description.
    /// E.g. 
    ///     - OpenGL requires the user provides all stages code.
    ///     - DirectX contains all stages in the same shader.
    /// </summary>
    public interface IPipelineDescription : IResourceCreatable, IMemoryProfilable, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether teh description is valid or not.
        /// That is every stages are Valid as well.
        /// </summary>
        bool IsValid
        {
            get;
        }

        #endregion Properties
    }
}
