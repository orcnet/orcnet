﻿using System;

namespace OrcNet.Core.Render
{
    /// <summary>
    /// Base frame buffer interface definition.
    /// </summary>
    public interface IFrameBuffer : IObserver, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the frame buffer Id.
        /// </summary>
        uint Id
        {
            get;
        }

        #endregion Properties
    }
}
