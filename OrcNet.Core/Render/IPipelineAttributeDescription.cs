﻿using System;

namespace OrcNet.Core.Render
{
    /// <summary>
    /// Base pipeline attribute description interface definition.
    /// </summary>
    public interface IPipelineAttributeDescription : IMemoryProfilable, IDisposable
    {
        #region Properties

        /// <summary>
        /// Stores the flag indicating whether the components must be normalized or not.
        /// </summary>
        bool MustBeNormalized
        {
            get;
        }

        /// <summary>
        /// Gets the vertex attribute index.
        /// </summary>
        int Index
        {
            get;
        }

        /// <summary>
        /// Gets a vertex attribute's value size
        /// </summary>
        int AttributeSize
        {
            get;
        }

        /// <summary>
        /// Gets the vertex attribute components count.
        /// </summary>
        int ComponentCount
        {
            get;
        }
        
        /// <summary>
        /// Gets the untyped buffer that contains actual vertex data.
        /// </summary>
        object UntypedData
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the offset between two consecutive attributes in the data buffer.
        /// </summary>
        int Stride
        {
            get;
        }

        /// <summary>
        /// Gets the offset of the first attribute value in the data buffer.
        /// </summary>
        int Offset
        {
            get;
        }
        
        #endregion Properties
    }
}
