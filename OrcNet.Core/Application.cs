﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Helpers;
using OrcNet.Core.Logger;
using OrcNet.Core.Plugins;
using OrcNet.Core.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace OrcNet.Core
{
    /// <summary>
    /// Delegate prototype used to inform application shut down.
    /// </summary>
    /// <param name="pSender"></param>
    /// <param name="pArgs"></param>
    public delegate void ShutdownDelegate(Application pSender, EventArgs pArgs);

    /// <summary>
    /// Definition of the <see cref="Application"/> class.
    /// This class contains the method(s) able to initialize and configurate
    /// the overall application.
    /// </summary>
    public sealed class Application
    {
        #region Fields
        
        /// <summary>
        /// Stores the Application unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile Application sInstance;

        /// <summary>
        /// Stores the sync root to lock on the Application rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the flag indicating whether the application is shutting down or not.
        /// </summary>
        private bool mIsShuttingDown;

        /// <summary>
        /// Stores the registered services
        /// </summary>
        private Dictionary<Type, IModule> mCoreModules;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on application shutting down.
        /// </summary>
        public event ShutdownDelegate Shuttingdown;

        /// <summary>
        /// Event fired on application shut down.
        /// </summary>
        public event ShutdownDelegate Shutdown;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the application is shutting down or not.
        /// </summary>
        public bool IsShuttingDown
        {
            get
            {
                return this.mIsShuttingDown;
            }
            internal set
            {
                if ( this.mIsShuttingDown == value )
                {
                    return;
                }
                
                this.mIsShuttingDown = value;
            }
        }

        /// <summary>
        /// Gets the Application handle.
        /// </summary>
        public static Application Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if ( sInstance == null )
                {
                    // Lock on
                    lock ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if ( sInstance == null )
                        {
                            sInstance = new Application();
                        }
                    }
                }

                return sInstance;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        private Application()
        {
            this.mCoreModules = new Dictionary<Type, IModule>();
            AppDomain.CurrentDomain.ProcessExit += this.OnProcessExit;
            AppDomain.CurrentDomain.AssemblyLoad += this.OnAssemblyLoad;
        }
        
        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes the application, loggers and services.
        /// </summary>
        /// <param name="pLogger">The logger type(s) to use. (e.g: console, txt, xml, html or multiple by ORing)</param>
        /// <returns></returns>
        public bool Initialize(LoggerType pLogger)
        {
            // Initializes loggers.
            LogManager.Instance.Initialize( pLogger );

            // Assure all dlls of the current working directory are loaded in the Application Domain.
            string[] lDlls = Directory.GetFiles( Environment.CurrentDirectory, "*.dll" );
            for ( int lCurr = 0; lCurr < lDlls.Length; lCurr++ )
            {
                try
                {
                    string lPath = lDlls[ lCurr ];
                    if ( CLR_Utilities.IsManaged( lPath ) ) // Load into the AppDomain only if managed.
                    {
                        Assembly.LoadFrom( lPath );
                    }
                }
                catch ( Exception pEx )
                {
                    // Warn only (Not error) as it can still work but can finally be a problem to check.
                    LogManager.Instance.Log( pEx, LogType.WARNING );
                }
            }

            // Creates all core modules which will auto register their core services for each.
            IEnumerable<Type> lModuleTypes = typeof(IModule).GetAllInheritedTypes();
            foreach ( Type lModuleType in lModuleTypes )
            {
                IModule lNewModule = this.CreateModule( lModuleType );
                if ( lNewModule != null )
                {
                    this.mCoreModules.Add( lModuleType, lNewModule );
                    LogManager.Instance.Log( string.Format( "Registered the module {0}", lNewModule.Name != null ? lNewModule.Name : lModuleType.Name ), LogType.INFO );
                }
            }
            
            IPluginManager lPluginService = ServiceManager.Instance.GetService<IPluginManager>();
            if ( lPluginService != null )
            {
                // Initializes the plugin service.
                if ( lPluginService.Initialize() ) // Default parameters loaded from config file. ELSE give them there...
                {
                    // Loads all plugins.
                    int lPluginCount = lPluginService.LoadAllPlugins();
                    LogManager.Instance.Log( string.Format( "Loaded {0} plugins...", lPluginCount ) );
                }
            }

            return false;
        }

        /// <summary>
        /// Shut down the application.
        /// </summary>
        public void ShutDown()
        {
            this.ShutDown( 0 );
        }

        /// <summary>
        /// Shut down the application.
        /// </summary>
        /// <param name="pExitCode"></param>
        public void ShutDown(int pExitCode)
        {
            this.ShutdownInternal( pExitCode );

            Environment.Exit( pExitCode );
        }

        /// <summary>
        /// Shutdown the application.
        /// </summary>
        /// <param name="pExitCode"></param>
        internal void ShutdownInternal(int pExitCode)
        {
            if ( this.IsShuttingDown )
            {
                return;
            }

            this.NotifyShuttingdown( null );

            this.IsShuttingDown = true;

            this.NotifyShutdown( null );
        }

        /// <summary>
        /// Delegate called on assembly load to create module on new load if not already
        /// </summary>
        /// <param name="pSender">The application domain.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnAssemblyLoad(object pSender, AssemblyLoadEventArgs pEventArgs)
        {
            
        }

        /// <summary>
        /// Delegate called on process exit.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pArgs"></param>
        private void OnProcessExit(object pSender, EventArgs pArgs)
        {
            this.ShutdownInternal( 0 );
        }

        /// <summary>
        /// Creates a new module from its type.
        /// </summary>
        /// <param name="pModuleType">The module type.</param>
        /// <returns>The new module, Null if creation failure or already exists.</returns>
        private IModule CreateModule(Type pModuleType)
        {
            if ( pModuleType == null )
            {
                // Skip if null.
                return null;
            }

            if ( this.mCoreModules.ContainsKey( pModuleType ) )
            {
                // Skip if already there.
                return null;
            }

            try
            {
                ConstructorInfo lConstructor = pModuleType.GetConstructor( new Type[] { } );
                IModule lNewModule = lConstructor.Invoke( null ) as IModule;

                return lNewModule;
            }
            catch
            {
                LogManager.Instance.Log( string.Format( "Failed to create the module {0}", pModuleType.Name ), LogType.ERROR );
            }

            return null;
        }

        /// <summary>
        /// Notifies application shutting down.
        /// </summary>
        private void NotifyShuttingdown(EventArgs pArgs)
        {
            if ( this.Shuttingdown != null )
            {
                this.Shuttingdown( this, pArgs );
            }
        }

        /// <summary>
        /// Notifies application shut down.
        /// </summary>
        private void NotifyShutdown(EventArgs pArgs)
        {
            if ( this.Shutdown != null )
            {
                this.Shutdown( this, pArgs );
            }
        }

        #endregion Methods
    }
}
