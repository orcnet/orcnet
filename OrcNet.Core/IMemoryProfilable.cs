﻿namespace OrcNet.Core
{
    /// <summary>
    /// Base interface informing an object is relevant for memory
    /// profiling and provides the corresponding info(s).
    /// </summary>
    public interface IMemoryProfilable
    {
        #region Properties

        /// <summary>
        /// Gets the size of the object.
        /// </summary>
        uint Size
        {
            get;
        }

        /// <summary>
        /// Gets the object name.
        /// </summary>
        string TypeName
        {
            get;
        }

        /// <summary>
        /// Gets the object namespace.
        /// </summary>
        string TypeNamespace
        {
            get;
        }

        /// <summary>
        /// Gets the assembly name this object belongs to.
        /// </summary>
        string AssemblyName
        {
            get;
        }

        #endregion Properties
    }
}
