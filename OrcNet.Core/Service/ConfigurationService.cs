﻿using OrcNet.Core.Configuration;
using OrcNet.Core.Logger;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Linq;

namespace OrcNet.Core.Service
{
    /// <summary>
    /// Definition of the <see cref="ConfigurationService"/> class.
    /// </summary>
    internal class ConfigurationService : AService, IConfigurationService
    {
        #region Fields

        /// <summary>
        /// Stores the constant identifier Xml attribute tag.
        /// </summary>
        private const string cIdTag = "id";

        /// <summary>
        /// Stores the constant configurable Xml tag.
        /// </summary>
        private const string cConfigurableTag = "Configurable";

        /// <summary>
        /// Stores the constant configuration Xml tag.
        /// </summary>
        private const string cConfigurationTag = "Configuration";

        /// <summary>
        /// Stores the current configuration path.
        /// </summary>
        private string mConfigurationPath;

        /// <summary>
        /// Stores the flag indicating whether the configuration is dirty or not.
        /// </summary>
        private bool mIsDirty;

        /// <summary>
        /// Stores the overall application configuration items
        /// </summary>
        private Dictionary<string, IConfigurationItem> mConfigurationItems;

        /// <summary>
        /// Stores the overall application configurable items
        /// </summary>
        private Dictionary<string, IConfigurableItem> mConfigurableItems;

        /// <summary>
        /// Stores the flag indicating whether the configuration is being saved or not.
        /// </summary>
        private bool mIsSaving = false;

        /// <summary>
        /// Stores the flag indicating whether the configuration is being loaded or not.
        /// </summary>
        private bool mIsLoading = false;

        /// <summary>
        /// Stores the configuration service save worker.
        /// </summary>
        private BackgroundWorker mSaveWorker;

        /// <summary>
        /// Stores the configuration service load worker.
        /// </summary>
        private BackgroundWorker mLoadWorker;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on configutaion item(s) saves.
        /// </summary>
        public event ConfigurationServiceDelegate Saved;

        /// <summary>
        /// Event fired on configutaion item(s) loads.
        /// </summary>
        public event ConfigurationServiceDelegate Loaded;
        
        /// <summary>
        /// Event fired on configuration changed.
        /// </summary>
        public event ConfigurationServiceItemDelegate ConfigurationUpdated;
        
        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the current configuration path.
        /// </summary>
        public string ConfigurationPath
        {
            get
            {
                return this.mConfigurationPath;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the configuration is dirty or not.
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return this.mIsDirty;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the configuration is being saved or not.
        /// </summary>
        public bool IsSaving
        {
            get
            {
                return this.mIsSaving;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the configuration is being loaded or not.
        /// </summary>
        public bool IsLoading
        {
            get
            {
                return this.mIsLoading;
            }
        }

        /// <summary>
        /// Gets the configuration item from its identifier
        /// </summary>
        /// <param name="pId">The unique configuration item identifier.</param>
        /// <returns>The configuration item, null otherwise.</returns>
        public IConfigurationItem this[ConfigurationName pId]
        {
            get
            {
                IConfigurationItem lItem;
                if ( this.mConfigurationItems.TryGetValue( pId, out lItem ) )
                {
                    return lItem;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the configurable item from its identifier
        /// </summary>
        /// <param name="pId">The unique configurable item identifier.</param>
        /// <returns>The configurable item, null otherwise.</returns>
        public IConfigurableItem this[ConfigurableName pId]
        {
            get
            {
                IConfigurableItem lItem;
                if ( this.mConfigurableItems.TryGetValue( pId, out lItem ) )
                {
                    return lItem;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the overall application configuration items
        /// </summary>
        public IEnumerable<IConfigurationItem> ConfigurationItems
        {
            get
            {
                return this.mConfigurationItems.Values;
            }
        }

        /// <summary>
        /// Gets the overall application configurable items
        /// </summary>
        public IEnumerable<IConfigurableItem> ConfigurableItems
        {
            get
            {
                return this.mConfigurableItems.Values;
            }
        }

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "ConfigurationService";
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationService"/> class.
        /// </summary>
        public ConfigurationService()
        {
            this.mIsDirty = false;
            this.mConfigurationPath  = string.Format( @"{0}\{1}", Environment.CurrentDirectory, @"..\Resources\Configuration\OrcNet.app" );
            this.mConfigurableItems  = new Dictionary<string, IConfigurableItem>();
            this.mConfigurationItems = new Dictionary<string, IConfigurationItem>();

            this.mSaveWorker = new BackgroundWorker();
            this.mSaveWorker.DoWork += this.OnSaveWorkerStarted;
            this.mSaveWorker.RunWorkerCompleted += this.OnSaveWorkerCompleted;

            this.mLoadWorker = new BackgroundWorker();
            this.mLoadWorker.DoWork += this.OnLoadWorkerStarted;
            this.mLoadWorker.RunWorkerCompleted += this.OnLoadWorkerCompleted;

            //// Loads at start up the default configuration.
            //this.Load( this.mConfigurationPath );
        }
        
        #endregion Constructor

        #region Methods

        /// <summary>
        /// Registers a configurable items.
        /// </summary>
        /// <param name="pConfigurable">The configurable item.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool Register(IConfigurableItem pConfigurable)
        {
            if ( this.mConfigurableItems.ContainsKey( pConfigurable.Id ) )
            {
                return false;
            }

            this.mConfigurableItems.Add( pConfigurable.Id, pConfigurable );

            // Register the configurable item's configuration item(s).
            Type lConfigurableType = pConfigurable.GetType();
            PropertyInfo[] lProperties = lConfigurableType.GetProperties();
            for ( int lCurr = 0; lCurr < lProperties.Length; lCurr++ )
            {
                PropertyInfo lCurrent = lProperties[ lCurr ];
                object[] lAttributes = lCurrent.GetCustomAttributes( typeof(IConfigurationAttribute), true );
                if ( lAttributes == null ||
                     lAttributes.Length == 0 )
                {
                    continue;
                }

                IConfigurationAttribute lAttribute = lAttributes[ 0 ] as IConfigurationAttribute;
                ConfigurationItem lNewItem = new ConfigurationItem( lAttribute.Description, lAttribute.Type, lAttribute.Parser, lAttribute.Validator, pConfigurable, lCurrent );
                pConfigurable.Register( lNewItem );

                this.RegisterConfigurationItem( lNewItem );
            }

            return true;
        }

        /// <summary>
        /// Registers a new configuration item.
        /// </summary>
        /// <param name="pItem"></param>
        private void RegisterConfigurationItem(IConfigurationItem pItem)
        {
            if ( this.mConfigurationItems.ContainsKey( pItem.Id ) == false )
            {
                pItem.Updated += this.OnConfigurationItemUpdated;

                this.mConfigurationItems[ pItem.Id ] = pItem;
            }
        }

        /// <summary>
        /// Delegate called on configuration item changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pItem"></param>
        private void OnConfigurationItemUpdated(object pSender, IConfigurationItem pItem)
        {
            this.mIsDirty = true;

            this.NotifyConfigurationUpdated( pItem );
        }

        /// <summary>
        /// Saves a configuration file given its full path.
        /// </summary>
        /// <param name="pPath"></param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool Save(string pPath)
        {
            if ( string.IsNullOrEmpty( pPath ) )
            {
                return false;
            }

            this.mIsSaving = true;

            this.mSaveWorker.RunWorkerAsync( pPath );
            
            return true;
        }

        /// <summary>
        /// Loads a configuration file given its full path.
        /// </summary>
        /// <param name="pPath"></param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool Load(string pPath)
        {
            if ( string.IsNullOrEmpty( pPath ) )
            {
                return false;
            }

            this.mIsLoading = true;

            this.mLoadWorker.RunWorkerAsync( pPath );
            
            return true;
        }
        
        /// <summary>
        /// Delegate called on load worker started.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnLoadWorkerStarted(object pSender, DoWorkEventArgs pEventArgs)
        {
            BackgroundWorker lWorker = pSender as BackgroundWorker;
            if ( lWorker.CancellationPending )
            {
                pEventArgs.Cancel = true;
                return;
            }

            string lPath = pEventArgs.Argument as string;

            Dictionary<string, object> lResult = new Dictionary<string, object>();
            XElement lXRoot = XElement.Load( lPath );
            foreach ( XElement lXConfigurable in lXRoot.Descendants( cConfigurableTag ) )
            {
                foreach ( XElement lXItem in lXConfigurable.Descendants( cConfigurationTag ) )
                {
                    XAttribute lXId = lXItem.Attribute( cIdTag );
                    if ( lXId != null )
                    {
                        string lItemId = lXId.Value;
                        IConfigurationItem lItem = this[ new ConfigurationName( lItemId ) ];
                        if ( lItem != null )
                        {
                            object lValue;
                            if ( lItem.Parser.Read( lXItem, out lValue ) )
                            {
                                lResult[ lItemId ] = lValue;
                            }
                            else
                            {
                                LogManager.Instance.Log( string.Format( "Item {0} parser read failed. Please check the corresponding configuration parser.", lItemId ), LogType.ERROR );
                            }
                        }
                        else
                        {
                            LogManager.Instance.Log( string.Format( "The item {0} has not been found during loading.", lItemId ) );
                        }
                    }
                }
            }

            pEventArgs.Result = lResult;
        }

        /// <summary>
        /// Delegate called on save worker started.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSaveWorkerStarted(object pSender, DoWorkEventArgs pEventArgs)
        {
            BackgroundWorker lWorker = pSender as BackgroundWorker;
            if ( lWorker.CancellationPending )
            {
                pEventArgs.Cancel = true;
                return;
            }

            string lPath = pEventArgs.Argument as string;
            XElement lXResult = new XElement( "Configuration" );
            foreach ( IConfigurableItem lConfigurable in this.ConfigurableItems )
            {
                XElement lXConfigurable = new XElement( cConfigurableTag );
                lXConfigurable.SetAttributeValue( cIdTag, lConfigurable.Id );
                foreach ( IConfigurationItem lItem in lConfigurable.Items )
                {
                    XElement lXItem = new XElement( cConfigurationTag );
                    lXItem.SetAttributeValue( cIdTag, lItem.Id );

                    // Parses the item.
                    if ( lItem.Parser.Write( lItem.Value, lXItem ) == false )
                    {
                        LogManager.Instance.Log( string.Format( "Item {0} parser save failed. Please check the corresponding configuration parser.", lItem.Id ), LogType.ERROR );
                    }

                    lXConfigurable.Add( lXItem );
                }

                lXResult.Add( lXConfigurable );
            }

            pEventArgs.Result = new Tuple<string, XElement>( lPath, lXResult );
        }

        /// <summary>
        /// Delegate called on load worker completed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnLoadWorkerCompleted(object pSender, RunWorkerCompletedEventArgs pEventArgs)
        {
            Dictionary<string, object> lResult = pEventArgs.Result as Dictionary<string, object>;

            if ( lResult != null )
            {
                // Fill the result in the configuration item(s).
                foreach ( IConfigurationItem lItem in this.ConfigurationItems )
                {
                    object lValue;
                    if ( lResult.TryGetValue( lItem.Id, out lValue ) )
                    {
                        lItem.Value = lValue;
                    }
                }
            }

            this.mIsLoading = false;

            this.NotifyLoaded();
        }

        /// <summary>
        /// Delegate called on save worker completed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSaveWorkerCompleted(object pSender, RunWorkerCompletedEventArgs pEventArgs)
        {
            Tuple<string, XElement> lResult = pEventArgs.Result as Tuple<string, XElement>;
            if ( lResult != null )
            {
                lResult.Item2.Save( lResult.Item1 );
            }

            this.mIsSaving = false;

            this.NotifySaved();
        }

        /// <summary>
        /// Notifies a configutaion item(s) saves.
        /// </summary>
        private void NotifySaved()
        {
            if ( this.Saved != null )
            {
                this.Saved();
            }
        }

        /// <summary>
        /// Notifies a configutaion item(s) loads.
        /// </summary>
        private void NotifyLoaded()
        {
            if ( this.Loaded != null )
            {
                this.Loaded();
            }
        }
        
        /// <summary>
        /// Notifies a configuration changed.
        /// </summary>
        private void NotifyConfigurationUpdated(IConfigurationItem pItem)
        {
            if ( this.ConfigurationUpdated != null )
            {
                this.ConfigurationUpdated( this, pItem );
            }
        }
        
        #endregion Methods
    }
}
