﻿using OrcNet.Core.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OrcNet.Core.Service
{
    /// <summary>
    /// Service manager singleton class definition
    /// Thread-safe
    /// </summary>
    public sealed class ServiceManager
    {
        #region Fields

        /// <summary>
        /// Stores the base service interface type.
        /// </summary>
        private static Type sServiceBaseType = typeof(IService);

        /// <summary>
        /// Stores the service manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile ServiceManager sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();
        
        /// <summary>
        /// Stores the registered services
        /// </summary>
        private Dictionary<Type, IService> mRegisteredServices;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the service manager handle.
        /// </summary>
        public static ServiceManager Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if ( sInstance == null )
                {
                    // Lock on
                    lock ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if ( sInstance == null )
                        {
                            sInstance = new ServiceManager();
                        }
                    }
                }

                return sInstance;
            }
        }

        #endregion Properties

        #region Constructor
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceManager"/> class.
        /// </summary>
        private ServiceManager()
        {
            this.mRegisteredServices = new Dictionary<Type, IService>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Retrieves the requested service.
        /// </summary>
        /// <param name="pType">The service type to look for.</param>
        /// <returns>The found service, NULL otherwise.</returns>
        public IService GetService(Type pType)
        {
            if ( pType == sServiceBaseType )
            {
                // Avoid crap call.
                return null;
            }

            IService lService = null;
            if ( this.mRegisteredServices.TryGetValue( pType, out lService ) )
            {
                return lService;
            }

            // If not having in cache the service by matching type
            // try to look for inheritance and cache the service 
            // with that other type.
            var lEnumerator = this.mRegisteredServices.Keys.GetEnumerator();
            while ( lEnumerator.MoveNext() &&
                    lService == null )
            {
                if ( pType.IsAssignableFrom( lEnumerator.Current ) )
                {
                    lService = this.mRegisteredServices[ lEnumerator.Current ];
                }
            }

            if ( lService != null )
            {
                // Cache for the next time a user attempt to look 
                // for that service using that other type.
                this.mRegisteredServices[ pType ] = lService;
            }

            return lService;
        }

        /// <summary>
        /// Retrieves the requested service.
        /// </summary>
        /// <typeparam name="T">The service type</typeparam>
        /// <returns>The found service, NULL otherwise.</returns>
        public T GetService<T>() where T : class, IService
        {
            Type lServiceType = typeof(T);
            return this.GetService( lServiceType ) as T;
        }

        /// <summary>
        /// Registers a new service.
        /// </summary>
        /// <param name="pNewService">The new service to register.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public bool RegisterService(IService pNewService)
        {
            if ( pNewService == null )
            {
                return false;
            }

            Type lServiceType = pNewService.GetType();
            if ( this.mRegisteredServices.ContainsKey( lServiceType ) == false )
            {
                this.mRegisteredServices.Add( lServiceType, pNewService );
                LogManager.Instance.Log( string.Format( "Registered the service {0}", pNewService.Name != null ? pNewService.Name : lServiceType.Name ), LogType.INFO );
                return true;
            }

            return false;
        }

        /// <summary>
        /// Unregisters the given service.
        /// </summary>
        /// <param name="pOldService">The service to remove.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public bool UnregisterService(IService pOldService)
        {
            if ( pOldService == null )
            {
                return false;
            }

            Type lServiceType = pOldService.GetType();
            IService lService = null;
            if ( this.mRegisteredServices.TryGetValue( lServiceType, out lService ) &&
                 lService.IsCore == false ) // Protect users from removing core services.
            {
                if ( this.mRegisteredServices.Remove( lServiceType ) )
                {
                    // See if registering has been done regarding to inherited types as well to remove all
                    // assuring to not having the service no more whatever the Type ley.
                    IEnumerable<Type> lTypesToRemove = this.mRegisteredServices.Keys.Where( pElt => pElt.IsAssignableFrom( lServiceType ) );
                    foreach ( Type lToRemove in lTypesToRemove )
                    {
                        this.mRegisteredServices.Remove( lToRemove );
                    }

                    LogManager.Instance.Log( string.Format( "Unregistered the service {0}", pOldService.Name != null ? pOldService.Name : lServiceType.Name ), LogType.INFO );
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Helper method creating the service corresponding to the given type.
        /// </summary>
        /// <param name="pServiceType">The service type to create.</param>
        /// <param name="pParams">The constructor parameters if needed.</param>
        /// <returns>The created service, null if any failure or already exist in the ServiceManager.</returns>
        public static IService CreateService(Type pServiceType, params object[] pParams)
        {
            if ( pServiceType == null || // Don t allow creation if Null
                 pServiceType == sServiceBaseType || // Don t allow creation if crap type
                 pServiceType.IsAbstract || // Don t allow creation if abstract
                 ServiceManager.Instance.mRegisteredServices.ContainsKey( pServiceType ) ) // Don t allow creation if already existing.
            {
                return null;
            }

            try
            {
                // Gets the default contructor straight. Faster than Activator.Create.
                ConstructorInfo lConstructor = pServiceType.GetConstructor( pParams.Select( pElt => pElt.GetType() ).ToArray() );
                IService lNewService = lConstructor.Invoke( pParams ) as IService;
                return lNewService;
            }
            catch
            {
                LogManager.Instance.Log( string.Format( "Failed to create the service {0}", pServiceType ), LogType.ERROR );
            }

            return null;
        }

        #endregion Methods
    }
}
