﻿using OrcNet.Core.Configuration;
using System.Collections.Generic;

namespace OrcNet.Core.Service
{
    /// <summary>
    /// Configuration service delegate prototype definition.
    /// </summary>
    public delegate void ConfigurationServiceDelegate();

    /// <summary>
    /// Configuration service delegate prototype definition.
    /// </summary>
    /// <param name="pSender"></param>
    /// <param name="pItem"></param>
    public delegate void ConfigurationServiceItemDelegate(IConfigurationService pSender, IConfigurationItem pItem);

    /// <summary>
    /// Definition of the <see cref="IConfigurationService"/> interface.
    /// </summary>
    public interface IConfigurationService : IService
    {
        #region Events

        /// <summary>
        /// Event fired on configutaion item(s) saves.
        /// </summary>
        event ConfigurationServiceDelegate Saved;

        /// <summary>
        /// Event fired on configutaion item(s) loads.
        /// </summary>
        event ConfigurationServiceDelegate Loaded;
        
        /// <summary>
        /// Event fired on configuration changed.
        /// </summary>
        event ConfigurationServiceItemDelegate ConfigurationUpdated;
        
        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the current configuration path.
        /// </summary>
        string ConfigurationPath
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the configuration is dirty or not.
        /// </summary>
        bool IsDirty
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item from its identifier
        /// </summary>
        /// <param name="pId">The unique configuration item identifier.</param>
        /// <returns>The configuration item, null otherwise.</returns>
        IConfigurationItem this[ConfigurationName pId]
        {
            get;
        }

        /// <summary>
        /// Gets the configurable item from its identifier
        /// </summary>
        /// <param name="pId">The unique configurable item identifier.</param>
        /// <returns>The configurable item, null otherwise.</returns>
        IConfigurableItem this[ConfigurableName pId]
        {
            get;
        }

        /// <summary>
        /// Gets the overall application configuration items
        /// </summary>
        IEnumerable<IConfigurationItem> ConfigurationItems
        {
            get;
        }

        /// <summary>
        /// Gets the overall application configurable items
        /// </summary>
        IEnumerable<IConfigurableItem> ConfigurableItems
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Registers a configurable items.
        /// </summary>
        /// <param name="pConfigurable">The configurable item.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        bool Register(IConfigurableItem pConfigurable);

        /// <summary>
        /// Saves a configuration file given its full path.
        /// </summary>
        /// <param name="pPath"></param>
        /// <returns>True if succeeded, false otherwise.</returns>
        bool Save(string pPath);

        /// <summary>
        /// Loads a configuration file given its full path.
        /// </summary>
        /// <param name="pPath"></param>
        /// <returns>True if succeeded, false otherwise.</returns>
        bool Load(string pPath);

        #endregion Methods
    }
}
