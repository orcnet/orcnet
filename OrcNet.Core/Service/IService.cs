﻿using System;

namespace OrcNet.Core
{
    /// <summary>
    /// Base service interface definition.
    /// </summary>
    public interface IService : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        string Name
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        bool IsCore
        {
            get;
        }

        #endregion Properties
    }
}
