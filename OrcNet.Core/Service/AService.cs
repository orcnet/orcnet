﻿namespace OrcNet.Core.Service
{
    /// <summary>
    /// Definition of the <see cref="AService"/> class.
    /// </summary>
    public abstract class AService : AMemoryProfilable, IService
    {
        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;

                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #region Properties IService

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public abstract bool IsCore
        {
            get;
        }
        
        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public virtual string Name
        {
            get
            {
                return this.TypeName;
            }
        }

        #endregion Properties IService

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AService"/> class.
        /// </summary>
        protected AService()
        {

        }

        #endregion Constructor

        #region Methods

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {

        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
