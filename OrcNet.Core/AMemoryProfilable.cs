﻿using System;

namespace OrcNet.Core
{
    /// <summary>
    /// Base abstract class offering basic implementation for all
    /// Memory profilable object.
    /// </summary>
    public abstract class AMemoryProfilable : IMemoryProfilable
    {
        #region Fields

        /// <summary>
        /// Stores the object's type name
        /// </summary>
        private static string sTypeName = null;

        /// <summary>
        /// Stores the object's namespace.
        /// </summary>
        private static string sTypeNamespace = null;

        /// <summary>
        /// Stores the object's assembly name.
        /// </summary>
        private static string sAssemblyName  = null;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public abstract uint Size
        {
            get;
        }

        /// <summary>
        /// Gets the object namespace.
        /// </summary>
        public string TypeName
        {
            get
            {
                if ( sTypeName == null )
                {
                    this.CacheTypeInfo();
                }

                return sTypeName;
            }
        }

        /// <summary>
        /// Gets the assembly name this object belongs to.
        /// </summary>
        public string TypeNamespace
        {
            get
            {
                if ( sTypeNamespace == null )
                {
                    this.CacheTypeInfo();
                }

                return sTypeNamespace;
            }
        }

        /// <summary>
        /// Gets the size of the object.
        /// </summary>
        public string AssemblyName
        {
            get
            {
                if ( sAssemblyName == null )
                {
                    this.CacheTypeInfo();
                }

                return sAssemblyName;
            }
        }

        #endregion Properties

        #region Constructor
        
        /// <summary>
        /// Initializes a new instance of the <see cref="AMemoryProfilable"/> class.
        /// </summary>
        protected AMemoryProfilable()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Get type info into cache.
        /// </summary>
        private void CacheTypeInfo()
        {
            Type lThisType = this.GetType();
            sTypeName      = lThisType.Name;
            sAssemblyName  = lThisType.Assembly.GetName().Name;
            sTypeNamespace = lThisType.Namespace;
        }

        #endregion Methods
    }
}
