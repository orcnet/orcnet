﻿namespace OrcNet.Core
{
    /// <summary>
    /// Property changed event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void PropertyChangedEventDelegate(object pSender, PropertyChangedEventArgs pEventArgs);

    /// <summary>
    /// Property changed event arguments class definition.
    /// </summary>
    public struct PropertyChangedEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the old value.
        /// </summary>
        private object mOldValue;

        /// <summary>
        /// Stores the old value.
        /// </summary>
        private object mNewValue;

        /// <summary>
        /// Stores the property whose value has changed.
        /// </summary>
        private string mPropertyName;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the old value.
        /// </summary>
        public object OldValue
        {
            get
            {
                return this.mOldValue;
            }
        }

        /// <summary>
        /// Gets the old value.
        /// </summary>
        public object NewValue
        {
            get
            {
                return this.mNewValue;
            }
        }

        /// <summary>
        /// Gets the property whose value has changed.
        /// </summary>
        public string PropertyName
        {
            get
            {
                return this.mPropertyName;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyChangedEventArgs"/> class.
        /// </summary>
        /// <param name="pOldValue">The old value.</param>
        /// <param name="pNewValue">The new value.</param>
        /// <param name="pPropertyName">The property whose value has changed</param>
        public PropertyChangedEventArgs(object pOldValue, object pNewValue, string pPropertyName)
        {
            this.mOldValue = pOldValue;
            this.mNewValue = pNewValue;
            this.mPropertyName = pPropertyName;
        }

        #endregion Constructor
    }
}
