﻿using System.Collections.Generic;

namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="IConfigurableItem"/> interface.
    /// </summary>
    public interface IConfigurableItem
    {
        #region Properties

        /// <summary>
        /// Gets the configurable item unique identifier.
        /// </summary>
        string Id
        {
            get;
        }

        /// <summary>
        /// Gets the configurable item(s).
        /// </summary>
        IEnumerable<IConfigurationItem> Items
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item given its id.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns>The item, null otherwise.</returns>
        IConfigurationItem this[ConfigurationName pId]
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Registers a new configuration item.
        /// </summary>
        /// <param name="pItem">The new configuration item.</param>
        void Register(IConfigurationItem pItem);

        #endregion Methods
    }
}
