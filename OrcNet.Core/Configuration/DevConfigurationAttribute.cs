﻿namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="DevConfigurationAttribute"/> class.
    /// </summary>
    public class DevConfigurationAttribute : AConfigurationAttribute
    {
        #region Properties
        
        /// <summary>
        /// Gets the configuration item type.
        /// </summary>
        public override ConfigurationType Type
        {
            get
            {
                return ConfigurationType.Dev;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DevConfigurationAttribute"/> class.
        /// </summary>
        /// <param name="pDescription">The configuration item description.</param>
        /// <param name="pParser">The configuration item parser providing default value, reader and writer processes.</param>
        /// <param name="pValidator">The configuration item value validator.</param>
        public DevConfigurationAttribute(string pDescription, IConfigurationParser pParser, IConfigurationValidator pValidator) :
        base( pDescription, pParser, pValidator )
        {

        }

        #endregion Constructor
    }
}
