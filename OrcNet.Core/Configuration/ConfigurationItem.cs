﻿using OrcNet.Core.Logger;
using System;
using System.Reflection;

namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="ConfigurationItem"/> class.
    /// </summary>
    internal class ConfigurationItem : AMemoryProfilable, IConfigurationItem
    {
        #region Fields

        /// <summary>
        /// Stores the item Id.
        /// </summary>
        private string mId;

        /// <summary>
        /// Stores the flag indicating whether the configuration item is dirty or not.
        /// </summary>
        private bool mIsDirty;

        /// <summary>
        /// Stores the configuration item description.
        /// </summary>
        private string mDescription;

        /// <summary>
        /// Stores the item type.
        /// </summary>
        private ConfigurationType mType;

        /// <summary>
        /// Stores the configuration item parser.
        /// </summary>
        private IConfigurationParser mParser;

        /// <summary>
        /// Stores the configuration validator.
        /// </summary>
        private IConfigurationValidator mValidator;

        /// <summary>
        /// Stores the configuration item owner.
        /// </summary>
        private IConfigurableItem mOwner;

        /// <summary>
        /// Stores the item value.
        /// </summary>
        private object mValue;

        /// <summary>
        /// Stores the configuration property info owning the value.
        /// </summary>
        private PropertyInfo mPropertyInfo;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on configuration item updates.
        /// </summary>
        public event ConfigurationDelegate Updated;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the item Id.
        /// </summary>
        public string Id
        {
            get
            {
                return this.mId;
            }
        }

        /// <summary>
        /// Gets the configuration item description.
        /// </summary>
        public string Description
        {
            get
            {
                return this.mDescription;
            }
        }

        /// <summary>
        /// Gets the item type.
        /// </summary>
        public ConfigurationType Type
        {
            get
            {
                return this.mType;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the configuration item is dirty or not.
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return this.mIsDirty;
            }
        }

        /// <summary>
        /// Gets the configuration item parser.
        /// </summary>
        public IConfigurationParser Parser
        {
            get
            {
                return this.mParser;
            }
        }

        /// <summary>
        /// Gets the configuration validator.
        /// </summary>
        public IConfigurationValidator Validator
        {
            get
            {
                return this.mValidator;
            }
        }

        /// <summary>
        /// Gets the configuration item owner.
        /// </summary>
        public IConfigurableItem Owner
        {
            get
            {
                return this.mOwner;
            }
        }

        /// <summary>
        /// Gets or sets the item value.
        /// </summary>
        public object Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                this.UpdateValue( value );
            }
        }

        /// <summary>
        /// Gets the item memory footprint.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += (uint)this.mId.Length * sizeof(char);
                lSize += (uint)this.mDescription.Length * sizeof(char);
                lSize += sizeof(ConfigurationType);
                lSize += sizeof(int) * 3; // Three objects refs.

                return lSize;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationItem"/> class.
        /// </summary>
        /// <param name="pDescription">The item description.</param>
        /// <param name="pType">The item type.</param>
        /// <param name="pParser">The item default value and reader/writer processes.</param>
        /// <param name="pValidator">The configuration item value validator.</param>
        /// <param name="pOwner">The item configurable owner.</param>
        /// <param name="pProperty">The owner property owning the configuration item.</param>
        public ConfigurationItem(string pDescription, ConfigurationType pType, IConfigurationParser pParser, IConfigurationValidator pValidator, IConfigurableItem pOwner, PropertyInfo pProperty)
        {
            this.mDescription = pDescription;
            this.mType = pType;
            this.mParser = pParser;
            this.mValidator = pValidator;
            this.mOwner = pOwner;
            this.mPropertyInfo = pProperty;

            this.mId = GetItemId( pOwner, pProperty );

            if ( this.mParser != null )
            {
                this.Value = this.mParser.Initialize();
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the unique item identifier.
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pProperty"></param>
        /// <returns></returns>
        internal static string GetItemId(IConfigurableItem pOwner, PropertyInfo pProperty)
        {
            return string.Format( "{0}.{1}", pOwner.Id, pProperty.Name );
        }

        /// <summary>
        /// Updates the configuration item value.
        /// </summary>
        /// <param name="pNewValue">The new value.</param>
        private void UpdateValue(object pNewValue)
        {
            if ( pNewValue == null )
            {
                return;
            }

            try
            {
                bool lHasChanged = false;
                if ( this.mValidator != null &&
                     this.mValidator.Validate( pNewValue ) )
                {
                    this.mValue = this.mValidator.Coerce( pNewValue );
                    lHasChanged = true;
                }
                else if ( this.mValue != pNewValue )
                {
                    this.mValue = pNewValue;
                    lHasChanged = true;
                }

                if ( lHasChanged )
                {
                    this.mIsDirty = true;

                    // Update the owner property.
                    this.mPropertyInfo.SetValue( this.mOwner, this.mValue, null );

                    this.NotifyUpdated();
                }
            }
            catch (Exception pEx)
            {
                LogManager.Instance.Log( pEx );
            }
        }

        /// <summary>
        /// Notifies the item has been updated.
        /// </summary>
        private void NotifyUpdated()
        {
            if ( this.Updated != null )
            {
                this.Updated( this, this );
            }
        }

        #endregion Methods
    }
}
