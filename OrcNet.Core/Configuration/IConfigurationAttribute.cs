﻿namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="IConfigurationAttribute"/> interface.
    /// </summary>
    public interface IConfigurationAttribute
    {
        #region Properties

        /// <summary>
        /// Gets the configuration item description.
        /// </summary>
        string Description
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item type.
        /// </summary>
        ConfigurationType Type
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item parser.
        /// </summary>
        IConfigurationParser Parser
        {
            get;
        }

        /// <summary>
        /// Gets the configuration validator.
        /// </summary>
        IConfigurationValidator Validator
        {
            get;
        }

        #endregion Properties
    }
}
