﻿using System.Diagnostics;

namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="ConfigurableName"/> class.
    /// </summary>
    [DebuggerDisplay("Name = {mName}")]
    public class ConfigurableName
    {
        #region Fields

        /// <summary>
        /// Stores the name.
        /// </summary>
        private string mName;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurableName"/> class.
        /// </summary>
        /// <param name="pName">The name</param>
        public ConfigurableName(string pName)
        {
            this.mName = pName;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from ConfigurableName to string.
        /// </summary>
        /// <param name="pName">The name</param>
        public static implicit operator string(ConfigurableName pName)
        {
            return pName.mName;
        }

        #endregion Methods
    }
}
