﻿namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="ConfigurationType"/> enumeration.
    /// </summary>
    public enum ConfigurationType
    {
        /// <summary>
        /// Invalid type.
        /// </summary>
        Invalid = 0,

        /// <summary>
        /// For dev purpose.
        /// </summary>
        Dev,

        /// <summary>
        /// For client purpose.
        /// </summary>
        Client,
    }
}
