﻿namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="IConfigurationValidator"/> interface.
    /// </summary>
    public interface IConfigurationValidator
    {
        #region Methods

        /// <summary>
        /// Validates the given value.
        /// </summary>
        /// <param name="pValue">The value to check.</param>
        /// <returns>True if valid value, false otherwise.</returns>
        bool Validate(object pValue);

        /// <summary>
        /// Coerces the given value.
        /// </summary>
        /// <param name="pValue">The value to coerce.</param>
        /// <returns>The constrained new value.</returns>
        object Coerce(object pValue);

        #endregion Methods
    }
}
