﻿using System;

namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="AConfigurationAttribute"/> class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public abstract class AConfigurationAttribute : Attribute, IConfigurationAttribute
    {
        #region Fields

        /// <summary>
        /// Stores the configuration item description.
        /// </summary>
        private string mDescription;

        /// <summary>
        /// Stores the configuration item parser.
        /// </summary>
        private IConfigurationParser mParser;

        /// <summary>
        /// Stores the configutaion validator.
        /// </summary>
        private IConfigurationValidator mValidator;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the configuration item description.
        /// </summary>
        public string Description
        {
            get
            {
                return this.mDescription;
            }
        }

        /// <summary>
        /// Gets the configuration item type.
        /// </summary>
        public abstract ConfigurationType Type
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item parser.
        /// </summary>
        public IConfigurationParser Parser
        {
            get
            {
                return this.mParser;
            }
        }

        /// <summary>
        /// Gets the configuration validator.
        /// </summary>
        public IConfigurationValidator Validator
        {
            get
            {
                return this.mValidator;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AConfigurationAttribute"/> class.
        /// </summary>
        /// <param name="pDescription">The configuration item description.</param>
        /// <param name="pParser">The configuration item parser providing default value, reader and writer processes.</param>
        /// <param name="pValidator">The configuration item value validator.</param>
        protected AConfigurationAttribute(string pDescription, IConfigurationParser pParser, IConfigurationValidator pValidator)
        {
            this.mDescription = pDescription;
            this.mParser = pParser;
            this.mValidator = pValidator;
        }

        #endregion Constructor
    }
}
