﻿using System.Xml.Linq;

namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="IConfigurationParser"/> interface.
    /// </summary>
    public interface IConfigurationParser
    {
        #region Methods

        /// <summary>
        /// Initializes the configuration element and provides a default value.
        /// </summary>
        /// <returns></returns>
        object Initialize();

        /// <summary>
        /// Reads the configuration value from Xml.
        /// </summary>
        /// <param name="pElement">The XElement to parse the value from.</param>
        /// <param name="pValue">The resulting parsed value.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        bool Read(XElement pElement, out object pValue);

        /// <summary>
        /// Writes the configuration value to Xml
        /// </summary>
        /// <param name="pValue">The value to write.</param>
        /// <param name="pElement">The XElement to fill with the value.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        bool Write(object pValue, XElement pElement);

        #endregion Methods
    }
}
