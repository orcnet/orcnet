﻿namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Delegate prototype for item configuration events
    /// </summary>
    /// <param name="pSender"></param>
    /// <param name="pItem"></param>
    public delegate void ConfigurationDelegate(object pSender, IConfigurationItem pItem);
    
    /// <summary>
    /// Definition of the <see cref="IConfigurationItem"/> interface.
    /// </summary>
    public interface IConfigurationItem
    {
        #region Events

        /// <summary>
        /// Event fired on configuration item updates.
        /// </summary>
        event ConfigurationDelegate Updated;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the item Id.
        /// </summary>
        string Id
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item description.
        /// </summary>
        string Description
        {
            get;
        }

        /// <summary>
        /// Gets the item type.
        /// </summary>
        ConfigurationType Type
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the configuration item is dirty or not.
        /// </summary>
        bool IsDirty
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item parser.
        /// </summary>
        IConfigurationParser Parser
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item validator.
        /// </summary>
        IConfigurationValidator Validator
        {
            get;
        }

        /// <summary>
        /// Gets the configuration item owner.
        /// </summary>
        IConfigurableItem Owner
        {
            get;
        }

        /// <summary>
        /// Gets or sets the item value.
        /// </summary>
        object Value
        {
            get;
            set;
        }
        
        #endregion Properties
    }
}
