﻿using System.Diagnostics;

namespace OrcNet.Core.Configuration
{
    /// <summary>
    /// Definition of the <see cref="ConfigurationName"/> class.
    /// </summary>
    [DebuggerDisplay("Name = {mName}")]
    public class ConfigurationName
    {
        #region Fields

        /// <summary>
        /// Stores the name.
        /// </summary>
        private string mName;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationName"/> class.
        /// </summary>
        /// <param name="pName">The name</param>
        public ConfigurationName(string pName)
        {
            this.mName = pName;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from ConfigurationName to string.
        /// </summary>
        /// <param name="pName">The name</param>
        public static implicit operator string(ConfigurationName pName)
        {
            return pName.mName;
        }

        #endregion Methods
    }
}
