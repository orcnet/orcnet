﻿using System;
using System.Collections.Generic;

namespace OrcNet.Core.Helpers
{
    /// <summary>
    /// Offers a set of core utility methods
    /// </summary>
    public static class Utilities
    {
        #region Methods

        /// <summary>
        /// Swaps two objects with eachother
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="pFirst">The first object to swap.</param>
        /// <param name="pSecond">The second object to swap.</param>
        public static void Swap<T>(ref T pFirst, ref T pSecond)
        {
            T lTemp = pFirst;
            pFirst  = pSecond;
            pSecond = lTemp;
        }

        /// <summary>
        /// Swaps two objects of two lists by index.
        /// NOTE: Asumes the indices is in bounds and lists are not empty
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="pThis">The first list</param>
        /// <param name="pOther">The second list</param>
        /// <param name="pThisIndex">The first list object index to swap with the second list object</param>
        /// <param name="pOtherIndex">The second list object index to swap with the first list object.</param>
        public static void Swap<T>(this IList<T> pThis, IList<T> pOther, int pThisIndex, int pOtherIndex)
        {
            // Asumes the index is in bounds and list are not empty
            T lThisObject = pThis[ pThisIndex ];
            pThis[ pThisIndex ]   = pOther[ pOtherIndex ];
            pOther[ pOtherIndex ] = lThisObject;
        }

        /// <summary>
        /// Swaps two lists with each other.
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="pThis">The first list</param>
        /// <param name="pOther">The second list</param>
        public static void Swap<T>(this IList<T> pThis, IList<T> pOther)
        {
            if
                ( pThis.Count != pOther.Count )
            {
                return;
            }

            for
                ( int lCurrent = 0; lCurrent < pThis.Count; lCurrent++ )
            {
                pThis.Swap( pOther, lCurrent, lCurrent );
            }
        }

        /// <summary>
        /// Swpas two dictionaries with each other.
        /// </summary>
        /// <typeparam name="K">The dictionary key type</typeparam>
        /// <typeparam name="V">The dictionary value type</typeparam>
        /// <param name="pThis">The first dictionary</param>
        /// <param name="pOther">The second dictionary</param>
        public static void Swap<K, V>(this IDictionary<K, V> pThis, IDictionary<K, V> pOther)
        {
            if
                ( pThis.Count != pOther.Count )
            {
                return;
            }

            Dictionary<K, V> lTemp = new Dictionary<K, V>( pThis );
            pThis.Clear();
            foreach
                ( KeyValuePair<K, V> lOtherPair in pOther )
            {
                pThis.Add( lOtherPair.Key, lOtherPair.Value );
            }
            pOther.Clear();
            foreach
                ( KeyValuePair<K, V> lTempPair in lTemp )
            {
                pOther.Add( lTempPair.Key, lTempPair.Value );
            }
        }

        /// <summary>
        /// Swpas two dictionaries values with each other.
        /// Assumes both dictionaries have the supplied keys.
        /// </summary>
        /// <typeparam name="K">The dictionary key type</typeparam>
        /// <typeparam name="V">The dictionary value type</typeparam>
        /// <param name="pThis">The first dictionary</param>
        /// <param name="pOther">The second dictionary</param>
        /// <param name="pKey">The key of the value of the first dictionary that must be swapped with the other.</param>
        /// <param name="pOtherKey">The key of the value of the second dictionary that must be swapped with the other.</param>
        public static void Swap<K, V>(this IDictionary<K, V> pThis, IDictionary<K, V> pOther, K pKey, K pOtherKey)
        {
            V lTemp = pThis[ pKey ];
            pThis[ pKey ] = pOther[ pOtherKey ];
            pOther[ pOtherKey ] = lTemp;
        }

        /// <summary>
        /// Clamps a value between a min and a max value.
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="pMin">The min value</param>
        /// <param name="pToClamp">The value to clamp.</param>
        /// <param name="pMax">The max value</param>
        /// <returns>The clamped value.</returns>
        public static T Clamp<T>(T pMin, T pToClamp, T pMax) where T : System.IComparable<T>
        {
            T lResult = pToClamp;
            if 
                ( pToClamp.CompareTo( pMax ) > 0 )
            {
                lResult = pMax;
            }

            if 
                ( pToClamp.CompareTo( pMin ) < 0 )
            {
                lResult = pMin;
            }
            
            return lResult;
        }

        /// <summary>
        /// Parses an enum string as a given enum value.
        /// </summary>
        /// <typeparam name="T">The enum type.</typeparam>
        /// <param name="pEnumStr">The enum value as string to parse.</param>
        /// <param name="pDefaultValue">The default value if troubles.</param>
        /// <returns>The converted or default value.</returns>
        public static T ParseEnum<T>(string pEnumStr, T pDefaultValue) where T : struct
        {
            if
                ( string.IsNullOrEmpty( pEnumStr ) )
            {
                return pDefaultValue;
            }

            T lResult;
            return Enum.TryParse<T>( pEnumStr, true, out lResult ) ? lResult : pDefaultValue;
        }

        /// <summary>
        /// Checks whether the supplied value is Odd or Even.
        /// </summary>
        /// <param name="pValue">The value to look for oddness.</param>
        /// <returns>True if Odd, false otherwise.</returns>
        public static bool IsOdd(int pValue)
        {
            return pValue % 2 != 0;
        }

        #endregion Methods
    }
}
