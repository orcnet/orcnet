﻿using System.IO;
using System.Runtime.InteropServices;

namespace OrcNet.Core.Helpers
{
    /// <summary>
    /// Definition of the <see cref="CLR_Utilities"/> class.
    /// </summary>
    public static class CLR_Utilities
    {
        #region Inner classes

        /// <summary>
        /// Definition of the <see cref="MachineType"/> enumeration.
        /// </summary>
        public enum MachineType
        {
            /// <summary>
            /// Native.
            /// </summary>
            Native = 0,

            /// <summary>
            /// I386
            /// </summary>
            I386 = 0x014c,

            /// <summary>
            /// Itanium
            /// </summary>
            Itanium = 0x0200,

            /// <summary>
            /// x64
            /// </summary>
            x64 = 0x8664
        }

        /// <summary>
        /// Definition of the <see cref="IMAGE_DOS_HEADER"/> structure.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        public struct IMAGE_DOS_HEADER
        {
            /// <summary>
            /// The File address of new exe header.
            /// </summary>
            [FieldOffset(60)]
            public int e_lfanew;
        }

        /// <summary>
        /// Definition of the <see cref="IMAGE_FILE_HEADER"/> structure.
        /// </summary>
        public struct IMAGE_FILE_HEADER
        {
            /// <summary>
            /// The machine type.
            /// </summary>
            public ushort Machine;

            /// <summary>
            /// The number of sections.
            /// </summary>
            public ushort NumberOfSections;

            /// <summary>
            /// The data time stamp.
            /// </summary>
            public ulong TimeDateStamp;

            /// <summary>
            /// The pointer to the symbol table.
            /// </summary>
            public ulong PointerToSymbolTable;

            /// <summary>
            /// The number of symbol.
            /// </summary>
            public ulong NumberOfSymbols;

            /// <summary>
            /// The size of the optional header.
            /// </summary>
            public ushort SizeOfOptionalHeader;

            /// <summary>
            /// The characteristics
            /// </summary>
            public ushort Characteristics;
        }

        /// <summary>
        /// Definition of the <see cref="IMAGE_NT_HEADERS32"/> structure.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        public struct IMAGE_NT_HEADERS32
        {
            /// <summary>
            /// The signature.
            /// </summary>
            [FieldOffset(0)]
            public uint Signature;

            /// <summary>
            /// The file header.
            /// </summary>
            [FieldOffset(4)]
            public IMAGE_FILE_HEADER FileHeader;

            /// <summary>
            /// The optional header part.
            /// </summary>
            [FieldOffset(24)]
            public IMAGE_OPTIONAL_HEADER32 OptionalHeader;
        }

        /// <summary>
        /// Definition of the <see cref="IMAGE_NT_HEADERS64"/> structure.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        public struct IMAGE_NT_HEADERS64
        {
            /// <summary>
            /// The signature.
            /// </summary>
            [FieldOffset(0)]
            public uint Signature;

            /// <summary>
            /// The file header.
            /// </summary>
            [FieldOffset(4)]
            public IMAGE_FILE_HEADER FileHeader;

            /// <summary>
            /// The optional header part.
            /// </summary>
            [FieldOffset(24)]
            public IMAGE_OPTIONAL_HEADER64 OptionalHeader;
        }

        /// <summary>
        /// Definition of the <see cref="IMAGE_DATA_DIRECTORY"/> structure.
        /// </summary>
        public struct IMAGE_DATA_DIRECTORY
        {
            /// <summary>
            /// The virtual address.
            /// </summary>
            public uint VirtualAddress;

            /// <summary>
            /// The size.
            /// </summary>
            public uint Size;
        }

        /// <summary>
        /// Definition of the <see cref="IMAGE_OPTIONAL_HEADER32"/> structure.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        public struct IMAGE_OPTIONAL_HEADER32
        {
            /// <summary>
            /// The Magic number
            /// </summary>
            [FieldOffset(0)]
            public ushort Magic;

            /// <summary>
            /// The data directory
            /// </summary>
            [FieldOffset(208)]
            public IMAGE_DATA_DIRECTORY DataDirectory;
        }

        /// <summary>
        /// Definition of the <see cref="IMAGE_OPTIONAL_HEADER64"/> structure.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        public struct IMAGE_OPTIONAL_HEADER64
        {
            /// <summary>
            /// The Magic number
            /// </summary>
            [FieldOffset(0)]
            public ushort Magic;

            /// <summary>
            /// The data directory
            /// </summary>
            [FieldOffset(224)]
            public IMAGE_DATA_DIRECTORY DataDirectory;
        }

        #endregion Inner classes

        #region Methods

        /// <summary>
        /// Checks whether a file assembly is managed or unmanaged.
        /// </summary>
        /// <param name="pFilepath">The assembly filepath</param>
        /// <returns>True if managed, false otherwise.</returns>
        public static bool IsManaged(string pFilepath)
        {
            // Create a byte array to hold the information.
            byte[] lData = new byte[4096];

            // Open the file
            FileInfo lFileInfo = new FileInfo( pFilepath );
            using ( FileStream lStream = lFileInfo.Open( FileMode.Open, FileAccess.Read, FileShare.Read ) )
            {
                // Put in the first 4k of the file into our byte array
                int lReadCount = lStream.Read( lData, 0, 4096 );

                // Flush any buffered data and close (we don’t need to read anymore)
                lStream.Flush();
                lStream.Close();
            }

            unsafe
            {
                // The fixed statement prevents relocation of a variable 
                // by the garbage collector. It pins the location of the Data object 
                // in memory so that they will not be moved. The objects will 
                // be unpinned when the fixed block completes. In other words,
                // The gosh darn efficient garbage collector is always working 
                // and always moving stuff around. We need to tell him to keep 
                // off our property while we’re working with it. 
                fixed ( byte* pData = lData )
                {
                    // Get the first 64 bytes and turn it into a IMAGE_DOS_HEADER
                    IMAGE_DOS_HEADER* idh = (IMAGE_DOS_HEADER*)pData;

                    // Now that we have the DOS header, we can get the offset
                    // (e_lfanew) add it to the original address (p_Data) and 
                    // squeeze those bytes into a IMAGE_NT_HEADERS32 structure
                    // (I’ll talk about the 64 bit stuff in a bit
                    IMAGE_NT_HEADERS32* inhs = (IMAGE_NT_HEADERS32*)(idh->e_lfanew + pData);

                    // Here, I used the OptionalHeader.Magic. It tells you whether
                    // the assembly is PE32 (0x10b) or PE32+ (0x20b). 
                    // PE32+ just means 64-bit. So, instead of checking if it is 
                    // an X64 or Itanium, I just check if it’s a PE32+.
                    if (inhs->OptionalHeader.Magic == 0x20b)
                    {
                        // If it is a PE32+, I want to be sure I get the correct Optional
                        // Header. I cast it as an IMAGE_NT_HEADERS64 pointer.
                        // All you have to do is check the size!
                        if (((IMAGE_NT_HEADERS64*)inhs)->OptionalHeader.DataDirectory.Size > 0)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (inhs->OptionalHeader.DataDirectory.Size > 0)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        #endregion Methods
    }
}
