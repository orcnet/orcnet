﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OrcNet.Core.Extensions
{
    /// <summary>
    /// Type extensions class definition offering
    /// reflection helper method(s) on a Type object.
    /// </summary>
    public static class TypeExtensions
    {
        #region Fields

        /// <summary>
        /// Stores the static instance of an empty type array.
        /// </summary>
        private static readonly Type[] sEmptyTypes = new Type[0];

        /// <summary>
        /// Stores the set of inherited types by base type in cache to speed up 
        /// next call(s) of GetInheritedTypes().
        /// </summary>
        private static Dictionary<Type, List<Type>> sInheritedsByBaseType = new Dictionary<Type, List<Type>>();

        #endregion Fields

        #region Methods

        /// <summary>
        /// Retrieves the inherited types from a supplied type whatever it is an
        /// interface, abstract or concret base class type ONLY in the very same assembly.
        /// </summary>
        /// <param name="pBaseType">The base object type</param>
        /// <returns>The set of inherited type(s) from the base type. At least an empty list.</returns>
        public static IEnumerable<Type> GetInheritedTypes(this Type pBaseType)
        {
            List<Type> lInheriteds;
            if ( sInheritedsByBaseType.TryGetValue( pBaseType, out lInheriteds ) )
            {
                return lInheriteds;
            }
            else
            {
                // Else look in the assembly of the supplied type ONLY.
                lInheriteds = new List<Type>( pBaseType.Assembly.GetTypes().Where( pElt => pBaseType.IsAssignableFrom( pElt ) && pElt.IsAbstract == false ) );
                sInheritedsByBaseType.Add( pBaseType, lInheriteds );
                return lInheriteds;
            }
        }

        /// <summary>
        /// Retrieves the inherited types from a supplied type whatever it is an
        /// interface, abstract or concret base class type IN ALL assemblies.
        /// </summary>
        /// <param name="pBaseType">The base object type</param>
        /// <returns>The set of inherited type(s) from the base type. At least an empty list.</returns>
        public static IEnumerable<Type> GetAllInheritedTypes(this Type pBaseType)
        {
            return AppDomain.CurrentDomain.GetAssemblies().SelectMany( pAssembly => pAssembly.GetTypes() ).Where( pType => pBaseType.IsAssignableFrom( pType ) && pType.IsAbstract == false );
        }

        /// <summary>
        /// Creates an instance of a type.
        /// </summary>
        /// <param name="pType">The type to create.</param>
        /// <param name="pParams">The parameters to create an instance.</param>
        /// <returns></returns>
        public static object CreateInstance(this Type pType, params object[] pParams)
        {
            if ( pType.IsAbstract )
            {
                return null;
            }

            ConstructorInfo lConstructor;
            // Default constructor.
            if ( pParams == null || 
                 pParams.Length == 0 )
            {
                lConstructor = pType.GetConstructor( sEmptyTypes );
            }
            else
            {
                Type[] lParamTypes = pParams.Select( pElt => pElt.GetType() ).ToArray();
                lConstructor = pType.GetConstructor( lParamTypes );
            }

            if ( lConstructor != null )
            {
                return lConstructor.Invoke( pParams );
            }

            return null;
        }

        #endregion Methods
    }
}
