﻿using System.IO;
using System.Text;

namespace OrcNet.Core.Extensions
{
    /// <summary>
    /// File extensions and file helper methods definition.
    /// </summary>
    public static class FileExtensions
    {
        #region Methods

        /// <summary>
        /// Determines a text file's encoding by analyzing its byte order mark (BOM).
        /// Defaults to ASCII when detection of the text file's endianness fails.
        /// </summary>
        /// <param name="pFileName">The text file to analyze.</param>
        /// <returns>The detected encoding.</returns>
        public static Encoding GetEncoding(string pFileName)
        {
            // Read the BOM
            byte[] lBom = new byte[4];
            using
                ( FileStream lStream = new FileStream( pFileName, FileMode.Open, FileAccess.Read))
            {
                lStream.Read( lBom, 0, 4 );
            }

            // Analyze the BOM
            if (lBom[0] == 0x2b && lBom[1] == 0x2f && lBom[2] == 0x76) return Encoding.UTF7;
            if (lBom[0] == 0xef && lBom[1] == 0xbb && lBom[2] == 0xbf) return Encoding.UTF8;
            if (lBom[0] == 0xff && lBom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (lBom[0] == 0xfe && lBom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (lBom[0] == 0 && lBom[1] == 0 && lBom[2] == 0xfe && lBom[3] == 0xff) return Encoding.UTF32;
            return Encoding.ASCII;
        }

        #endregion Methods
    }
}
