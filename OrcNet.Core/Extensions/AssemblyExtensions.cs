﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OrcNet.Core.Extensions
{
    /// <summary>
    /// Definition of the <see cref="AssemblyExtensions"/> class.
    /// </summary>
    public static class AssemblyExtensions
    {
        #region Fields

        /// <summary>
        /// Stores the set of inherited types by base type in cache to speed up 
        /// next call(s) of GetInheritedTypes().
        /// </summary>
        private static Dictionary<Assembly, Dictionary<Type, List<Type>>> sInheritedsByBaseType = new Dictionary<Assembly, Dictionary<Type, List<Type>>>();

        #endregion Fields

        #region Methods

        /// <summary>
        /// Retrieves the inherited types from a supplied type whatever it is an
        /// interface or a concret class type ONLY in the given assembly.
        /// </summary>
        /// <param name="pAssembly">The assembly in which look for inherited type.</param>
        /// <returns>The set of inherited type(s) from the base type in the assembly. At least an empty list.</returns>
        public static List<Type> GetInheritedTypes<T>(this Assembly pAssembly)
        {
            Type lBaseType = typeof(T);
            Assembly lAssembly = pAssembly;
            Dictionary<Type, List<Type>> lInheritedsByBaseType;
            if( sInheritedsByBaseType.TryGetValue( lAssembly, out lInheritedsByBaseType ) == false )
            {
                lInheritedsByBaseType = new Dictionary<Type, List<Type>>();
                sInheritedsByBaseType.Add( lAssembly, lInheritedsByBaseType );
            }

            List<Type> lInheriteds;
            if ( lInheritedsByBaseType.TryGetValue( lBaseType, out lInheriteds ) )
            {
                return lInheriteds;
            }
            else
            {
                // Else look in the assembly of the supplied type ONLY.
                lInheriteds = new List<Type>( pAssembly.GetTypes().Where( pElt => lBaseType.IsAssignableFrom( pElt ) && pElt.IsAbstract == false ) );
                lInheritedsByBaseType.Add( lBaseType, lInheriteds );
                return lInheriteds;
            }
        }

        #endregion Methods
    }
}
