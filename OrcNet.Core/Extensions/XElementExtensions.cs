﻿using System.Xml.Linq;

namespace OrcNet.Core.Extensions
{
    /// <summary>
    /// XElement extensions class definition.
    /// </summary>
    public static class XElementExtensions
    {
        #region Methods

        /// <summary>
        /// Gets the element name.
        /// </summary>
        /// <param name="pElement">The xml element.</param>
        /// <returns>The element name.</returns>
        public static string Name(this XElement pElement)
        {
            if
                ( pElement == null ||
                  pElement.Name == null )
            {
                return string.Empty;
            }

            return pElement.Name.LocalName;
        }

        #endregion Methods
    }
}
