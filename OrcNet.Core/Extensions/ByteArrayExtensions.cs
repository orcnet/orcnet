﻿namespace OrcNet.Core.Extensions
{
    /// <summary>
    /// Definition of the <see cref="ByteArrayExtensions"/> class.
    /// </summary>
    public static class ByteArrayExtensions
    {
        #region Methods

        /// <summary>
        /// Swaps two byte whithin the given byte array.
        /// </summary>
        /// <param name="pThis">The byte array containing the tewo bytes to swap with each other.</param>
        /// <param name="pFirstIndex">The first byte index to swap.</param>
        /// <param name="pSecondIndex">The second byte index to swap.</param>
        public static void SwapBytes(this byte[] pThis, int pFirstIndex, int pSecondIndex)
        {
            byte lTemp = pThis[ pFirstIndex ];
            pThis[ pFirstIndex ]  = pThis[ pSecondIndex ];
            pThis[ pSecondIndex ] = lTemp;
        }

        #endregion Methods
    }
}
