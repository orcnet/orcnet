﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace OrcNet.Core.Extensions
{
    /// <summary>
    /// String extensions containing helper methods definition.
    /// </summary>
    public static class StringExtensions
    {
        #region Methods

        /// <summary>
        /// Extracts the words in the given input string.
        /// </summary>
        /// <param name="pInput">The input string to get words from.</param>
        /// <returns>The set of words in the given string.</returns>
        public static string[] GetWords(this string pInput)
        {
            string[] lWords = pInput.Split( new string[] { " ", "\r\n", "\n", "\t" }, StringSplitOptions.RemoveEmptyEntries );

            return lWords;
        }

        /// <summary>
        /// Remove word that could postfix an apostrophe (e.g: "You're" counts only for "You" )
        /// </summary>
        /// <param name="pWord">The word to trim.</param>
        /// <returns>The trimmed word.</returns>
        private static string TrimSuffix(string pWord)
        {
            int lApostropheLocation = pWord.IndexOf( '\'' );
            if ( lApostropheLocation != -1 )
            {
                pWord = pWord.Substring( 0, lApostropheLocation );
            }

            return pWord;
        }

        #endregion Methods
    }
}
