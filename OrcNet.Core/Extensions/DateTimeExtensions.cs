﻿using System;

namespace OrcNet.Core.Extensions
{
    /// <summary>
    /// Date time extensions definition.
    /// </summary>
    public static class DateTimeExtensions
    {
        #region Methods

        /// <summary>
        /// Returns the current Date as string.
        /// </summary>
        /// <returns>The current date as string</returns>
        public static string GetDateString(this DateTime pDateTime)
        {
            return string.Format( "{0:d}", pDateTime.Date );
        }

        /// <summary>
        /// Returns the current time of the day as string
        /// </summary>
        /// <param name="pDateTime">The DateTime</param>
        /// <returns>The current time of the day as string</returns>
        public static string GetTimeOfDayString(this DateTime pDateTime)
        {
            return string.Format( "{1:g}", pDateTime.TimeOfDay );
        }

        /// <summary>
        /// Returns the current date and time formated as string
        /// </summary>
        /// <param name="pDateTime">The DateTime</param>
        /// <returns>The current date and time formated</returns>
        public static string GetDateTimeString(this DateTime pDateTime)
        {
            return string.Format( "{0} {1}", pDateTime.GetDateString(), pDateTime.GetTimeOfDayString() );
        }

        #endregion Methods
    }
}
