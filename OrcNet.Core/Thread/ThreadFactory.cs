﻿using System.Globalization;

namespace OrcNet.Core.Thread
{
    /// <summary>
    /// Static helper class allowing to create new threads.
    /// </summary>
    public static class ThreadFactory
    {
        #region Methods

        /// <summary>
        /// Create a new thread.
        /// </summary>
        /// <param name="pName">The new thread's name.</param>*<param name="pWork">The thread work delegate</param>
        /// <returns>The new thread object</returns>
        public static System.Threading.Thread CreateThread(string pName, System.Threading.ThreadStart pWork)
        {
            System.Threading.Thread lNewThread = new System.Threading.Thread( pWork );
            lNewThread.Name = pName;
            lNewThread.CurrentCulture   = CultureInfo.CurrentCulture;
            lNewThread.CurrentUICulture = CultureInfo.CurrentUICulture;
            return lNewThread;
        }

        /// <summary>
        /// Create a new thread.
        /// </summary>
        /// <param name="pName">The new thread's name.</param>*<param name="pWork">The thread work delegate</param>
        /// <returns>The new thread object</returns>
        public static System.Threading.Thread CreateThread(string pName, System.Threading.ParameterizedThreadStart pWork)
        {
            System.Threading.Thread lNewThread = new System.Threading.Thread( pWork );
            lNewThread.Name = pName;
            lNewThread.CurrentCulture   = CultureInfo.CurrentCulture;
            lNewThread.CurrentUICulture = CultureInfo.CurrentUICulture;
            return lNewThread;
        }

        #endregion Methods
    }
}
