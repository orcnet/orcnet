﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Service;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace OrcNet.Core
{
    /// <summary>
    /// Definition of the <see cref="AModule"/> class.
    /// </summary>
    public abstract class AModule : IModule
    {
        #region Fields

        /// <summary>
        /// Stores the module's corresponding assembly.
        /// </summary>
        private Assembly mAssembly;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the module name.
        /// </summary>
        public virtual string Name
        {
            get
            {
                if ( this.mAssembly != null )
                {
                    return this.mAssembly.GetName().Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the module version.
        /// </summary>
        public string Version
        {
            get
            {
                if ( this.mAssembly != null )
                {
                    return this.mAssembly.GetName().Version.ToString();
                }

                return string.Empty;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AModule"/> class.
        /// </summary>
        protected AModule()
        {
            this.mAssembly = this.GetType().Assembly;

            // Load all service being in the module assembly as Core services.
            List<Type> lServiceTypes = this.mAssembly.GetInheritedTypes<IService>();
            int lServiceCount = lServiceTypes.Count;
            if ( lServiceCount > 0 )
            {
                // Creates and register them.
                for ( int lCurr = 0; lCurr < lServiceCount; lCurr++ )
                {
                    Type lServiceType = lServiceTypes[ lCurr ];
                    IService lCoreService = ServiceManager.CreateService( lServiceType );
                    if ( lCoreService != null )
                    {
                        // Those one will never be removed.
                        ServiceManager.Instance.RegisterService( lCoreService );
                    }
                }
            }
        }

        #endregion Constructor
    }
}
