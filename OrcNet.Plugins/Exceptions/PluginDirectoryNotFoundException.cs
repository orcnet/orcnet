﻿using System;
using System.Runtime.Serialization;

namespace Application.Plugins.Exceptions
{
    /// <summary>
    /// Definition of the <see cref="PluginDirectoryNotFoundException"/> class.
    /// </summary>
    [Serializable]
    public class PluginDirectoryNotFoundException : System.IO.IOException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginDirectoryNotFoundException"/> class.
        /// </summary>
        public PluginDirectoryNotFoundException() : 
        base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginDirectoryNotFoundException"/> class.
        /// </summary>
        /// <param name="pMessage">The exception message.</param>
        public PluginDirectoryNotFoundException(string pMessage) : 
        base( pMessage )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginDirectoryNotFoundException"/> class.
        /// </summary>
        /// <param name="pFormat">The message format.</param>
        /// <param name="pArgs">The message format arguments.</param>
        public PluginDirectoryNotFoundException(string pFormat, params object[] pArgs) : 
        base( string.Format( pFormat, pArgs ) )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginDirectoryNotFoundException"/> class.
        /// </summary>
        /// <param name="pMessage">The exception message.</param>
        /// <param name="pInnerException">The inner exception.</param>
        public PluginDirectoryNotFoundException(string pMessage, Exception pInnerException) : 
        base( pMessage, pInnerException )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginDirectoryNotFoundException"/> class.
        /// </summary>
        /// <param name="pFormat">The message format.</param>
        /// <param name="pInnerException">The inner exception.</param>
        /// <param name="pArgs">The message format arguments.</param>
        public PluginDirectoryNotFoundException(string pFormat, Exception pInnerException, params object[] pArgs) : 
        base( string.Format( pFormat, pArgs ), pInnerException )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginDirectoryNotFoundException"/> class.
        /// </summary>
        /// <param name="pInfo">The serialization info</param>
        /// <param name="pContext">The serialization context.</param>
        protected PluginDirectoryNotFoundException(SerializationInfo pInfo, StreamingContext pContext) : 
        base( pInfo, pContext )
        {

        }

        #endregion Constructors
    }
}
