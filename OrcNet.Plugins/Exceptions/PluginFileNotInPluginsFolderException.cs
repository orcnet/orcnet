﻿using System;
using System.Runtime.Serialization;

namespace Application.Plugins.Exceptions
{
    /// <summary>
    /// Definition of the <see cref="PluginFileNotInPluginsFolderException"/> class.
    /// </summary>
    [Serializable]
    public class PluginFileNotInPluginsFolderException : System.IO.IOException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotInPluginsFolderException"/> class.
        /// </summary>
        public PluginFileNotInPluginsFolderException() : 
        base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotInPluginsFolderException"/> class.
        /// </summary>
        /// <param name="pMessage">The exception message.</param>
        public PluginFileNotInPluginsFolderException(string pMessage) : 
        base( pMessage )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotInPluginsFolderException"/> class.
        /// </summary>
        /// <param name="pFormat">The message format.</param>
        /// <param name="pArgs">The message format arguments.</param>
        public PluginFileNotInPluginsFolderException(string pFormat, params object[] pArgs) : 
        base( string.Format( pFormat, pArgs ) )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotInPluginsFolderException"/> class.
        /// </summary>
        /// <param name="pMessage">The exception message.</param>
        /// <param name="pInnerException">The inner exception.</param>
        public PluginFileNotInPluginsFolderException(string pMessage, Exception pInnerException) : 
        base( pMessage, pInnerException )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotInPluginsFolderException"/> class.
        /// </summary>
        /// <param name="pFormat">The message format.</param>
        /// <param name="pInnerException">The inner exception.</param>
        /// <param name="pArgs">The message format arguments.</param>
        public PluginFileNotInPluginsFolderException(string pFormat, Exception pInnerException, params object[] pArgs) : 
        base( string.Format( pFormat, pArgs ), pInnerException )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotInPluginsFolderException"/> class.
        /// </summary>
        /// <param name="pInfo">The serialization info</param>
        /// <param name="pContext">The serialization context.</param>
        protected PluginFileNotInPluginsFolderException(SerializationInfo pInfo, StreamingContext pContext) : 
        base( pInfo, pContext )
        {

        }

        #endregion Constructors
    }
}
