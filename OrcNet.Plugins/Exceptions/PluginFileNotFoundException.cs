﻿using System;
using System.Runtime.Serialization;

namespace Application.Plugins.Exceptions
{
    /// <summary>
    /// Definition of the <see cref="PluginFileNotFoundException"/> class.
    /// </summary>
    [Serializable]
    public class PluginFileNotFoundException : System.IO.IOException
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotFoundException"/> class.
        /// </summary>
        public PluginFileNotFoundException() : 
        base()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotFoundException"/> class.
        /// </summary>
        /// <param name="pMessage">The exception message.</param>
        public PluginFileNotFoundException(string pMessage) : 
        base( pMessage )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotFoundException"/> class.
        /// </summary>
        /// <param name="pFormat">The message format.</param>
        /// <param name="pArgs">The message format arguments.</param>
        public PluginFileNotFoundException(string pFormat, params object[] pArgs) : 
        base( string.Format( pFormat, pArgs ) )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotFoundException"/> class.
        /// </summary>
        /// <param name="pMessage">The exception message.</param>
        /// <param name="pInnerException">The inner exception.</param>
        public PluginFileNotFoundException(string pMessage, Exception pInnerException) : 
        base( pMessage, pInnerException )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotFoundException"/> class.
        /// </summary>
        /// <param name="pFormat">The message format.</param>
        /// <param name="pInnerException">The inner exception.</param>
        /// <param name="pArgs">The message format arguments.</param>
        public PluginFileNotFoundException(string pFormat, Exception pInnerException, params object[] pArgs) : 
        base( string.Format( pFormat, pArgs ), pInnerException )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginFileNotFoundException"/> class.
        /// </summary>
        /// <param name="pInfo">The serialization info</param>
        /// <param name="pContext">The serialization context.</param>
        protected PluginFileNotFoundException(SerializationInfo pInfo, StreamingContext pContext) : 
        base( pInfo, pContext )
        {

        }

        #endregion Constructors
    }
}
