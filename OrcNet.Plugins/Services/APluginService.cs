﻿using OrcNet.Core.Plugins;
using OrcNet.Core.Service;

namespace OrcNet.Plugins.Services
{
    /// <summary>
    /// Definition of the <see cref="APluginService"/> class.
    /// </summary>
    public abstract class APluginService : AService, IPluginService
    {
        #region Fields

        /// <summary>
        /// Stores the service's owner.
        /// </summary>
        private IPlugin mOwner;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether it is a core service or not.
        /// </summary>
        public sealed override bool IsCore
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the service's owner.
        /// </summary>
        public IPlugin Owner
        {
            get
            {
                return this.mOwner;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APluginService"/> class.
        /// </summary>
        /// <param name="pOwner">The service's owner.</param>
        protected APluginService(IPlugin pOwner)
        {
            this.mOwner = pOwner;
        }

        #endregion Constructor
    }
}
