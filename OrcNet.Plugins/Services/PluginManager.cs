﻿using Application.Plugins.Exceptions;
using OrcNet.Core.Logger;
using OrcNet.Core.Plugins;
using OrcNet.Core.Service;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Web;
using System.Xml.Linq;

namespace OrcNet.Plugins
{
    /// <summary>
    /// Definition of the <see cref="PluginManager"/> class.
    /// </summary>
    internal sealed class PluginManager : AService, IPluginManager
    {
        #region Fields

        /// <summary>
        /// Stores the constant cache configuration Xml Tag.
        /// </summary>
        private const string cCacheTag = "Cache";

        /// <summary>
        /// Stores the constant plugin cache type Xml Attribute.
        /// </summary>
        private const string cTypeAttribute = "type";

        /// <summary>
        /// Stores the constant plugin cache file watcher delay Xml Attribute.
        /// </summary>
        private const string cFileWatcherDelayAttribute = "filesystemWatcherDelay";

        /// <summary>
        /// Stores the constant plugin cache Allow Sliding Expiration Xml Attribute.
        /// </summary>
        private const string cAllowSlidingAttribute = "allowSlidingExpiration";

        /// <summary>
        /// Stores the constant plugin cache Expiration Interval Xml Attribute.
        /// </summary>
        private const string cExpirationIntervalAttribute = "cacheExpirationInterval";

        /// <summary>
        /// Stores the constant plugin cache Auto Reload On Expiration Xml Attribute.
        /// </summary>
        private const string cAutoReloadOnExpirationAttribute = "autoReloadOnCacheExpiration";

        /// <summary>
        /// Stores the constant default configuration Xml Tag.
        /// </summary>
        private const string cDefaultConfigurationTag = "DefaultPluginConfiguration";

        /// <summary>
        /// Stores the constant plugin Folder Xml Attribute.
        /// </summary>
        private const string cFolderAttribute = "pluginsFolder";

        /// <summary>
        /// Stores the constant createPluginFolder Xml Attribute.
        /// </summary>
        private const string cCreatePluginFolderAttribute = "createPluginFolder";

        /// <summary>
        /// Stores the constant keepFileHandle Xml Attribute.
        /// </summary>
        private const string cKeepFileHandleAttribute = "keepFileHandle";

        /// <summary>
        /// Stores the plugin assemblies container.
        /// </summary>
        private Lazy<ConcurrentDictionary<string, PluginAssembly>> mAssemblies = new Lazy<ConcurrentDictionary<string, PluginAssembly>>(() => new ConcurrentDictionary<string, PluginAssembly>());

        /// <summary>
        /// Stores the plugins manager default configuration.
        /// </summary>
        private PluginConfiguration mDefaultConfiguration;

        /// <summary>
        /// Stores the file watcher
        /// </summary>
        private FileSystemWatcher mWatcher = null;

        /// <summary>
        /// Stores the cache expiration timer.
        /// </summary>
        private Timer mCacheExpirationTimer = null;

        /// <summary>
        /// Stores the plugin folder path.
        /// </summary>
        private string mPluginsFolder;

        /// <summary>
        /// Stores the flag indicating whether the file handle must be kept or not.
        /// </summary>
        private bool mKeepFileHandle;

        /// <summary>
        /// Stores the plugin manager cache policy.
        /// </summary>
        private CachePolicy mCachePolicy;

        /// <summary>
        /// Stores the flag indicating whether the plugins folder must be created or not.
        /// </summary>
        private bool mCreatePluginsFolder;
        
        #endregion Fields

        #region Properties

        #region Properties IService

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "PluginManager";
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties IService

        /// <summary>
        /// Gets the plugins default configuration.
        /// </summary>
        public PluginConfiguration DefaultConfiguration
        {
            get
            {
                return this.mDefaultConfiguration;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the plugins folder must be created or not.
        /// </summary>
        public bool CreatePluginsFolder
        {
            get
            {
                return this.mCreatePluginsFolder;
            }
        }

        /// <summary>
        /// Gets the path of the folder where plugin assemblies are stored
        /// </summary>
        public string PluginsFolder
        {
            get
            {
                if ( string.IsNullOrWhiteSpace( this.mPluginsFolder ) == false )
                {
                    if ( Directory.Exists( this.mPluginsFolder ) )
                    {
                        return new DirectoryInfo( this.mPluginsFolder ).FullName;
                    }
                    else if ( HttpContext.Current != null && 
                              Directory.Exists( HttpContext.Current.Server.MapPath( this.mPluginsFolder ) ) )
                    {
                        return HttpContext.Current.Server.MapPath( this.mPluginsFolder );
                    }
                    else if ( Directory.Exists( Path.Combine( Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ), this.mPluginsFolder ) ) )
                    {
                        return Path.Combine( Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ), this.mPluginsFolder );
                    }
                    else
                    {
                        return this.mPluginsFolder;
                    }
                }
                else
                {
                    return Path.Combine( Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ), "Plugins" );
                }
            }
        }

        /// <summary>
        /// Gets the plugin manager cache policy.
        /// </summary>
        public CachePolicy CachePolicy
        {
            get
            {
                return mCachePolicy;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the file handle must be kept or not.
        /// </summary>
        public bool KeepFileHandle
        {
            get
            {
                return this.mKeepFileHandle;
            }
        }

        #endregion Properties

        #region Events

        /// <summary>
        /// Event triggerd on plugin loaded. (depending on the policy, can occur multiple times)
        /// </summary>
        public event PluginEventDelegate PluginLoaded;

        /// <summary>
        /// Event triggered on plugin unloaded. (depending on the policy, can occur multiple times)
        /// </summary>
        public event PluginEventDelegate PluginUnloaded;

        #endregion Events

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginManager"/> class.
        /// </summary>
        public PluginManager()
        {
            
        }

        #endregion Constructors

        #region Methods

        #region Methods Statics

        /// <summary>
        /// Loads a plugin configuration given the file path.
        /// </summary>
        /// <param name="pFilePath">The file path of the configuration.</param>
        /// <returns>The configuration object.</returns>
        public static PluginConfiguration LoadConfiguration(string pFilePath)
        {
            if ( string.IsNullOrEmpty( pFilePath ) )
            {
                return null;
            }

            if ( File.Exists( pFilePath ) )
            {
                XElement lXRoot = XElement.Load( pFilePath );
                if ( lXRoot != null )
                {
                    XElement lXDefaultConfiguration = lXRoot.Element( cDefaultConfigurationTag );
                    if ( lXDefaultConfiguration != null )
                    {
                        string lPluginFolder = "Plugins";
                        XAttribute lXPluginFolder = lXDefaultConfiguration.Attribute( cFolderAttribute );
                        if ( lXPluginFolder != null )
                        {
                            lPluginFolder = lXPluginFolder.Value;
                        }

                        bool lCreatePluginFolder = true;
                        XAttribute lXCreateFolder = lXDefaultConfiguration.Attribute( cCreatePluginFolderAttribute );
                        if ( lXCreateFolder != null )
                        {
                            bool lResult;
                            if ( bool.TryParse( lXCreateFolder.Value, out lResult ) )
                            {
                                lCreatePluginFolder = lResult;
                            }
                        }

                        bool lKeepFileHandle = false;
                        XAttribute lXKeepFileHandle = lXDefaultConfiguration.Attribute( cKeepFileHandleAttribute );
                        if ( lXKeepFileHandle != null )
                        {
                            bool lResult;
                            if ( bool.TryParse( lXKeepFileHandle.Value, out lResult ) )
                            {
                                lKeepFileHandle = lResult;
                            }
                        }

                        ConfigurationCache lCache = null;
                        XElement lXCache = lXDefaultConfiguration.Element( cCacheTag );
                        if ( lXCache != null )
                        {
                            CachePolicyType lType = CachePolicyType.FileWatch;
                            XAttribute lXType = lXCache.Attribute( cTypeAttribute );
                            if ( lXType != null )
                            {
                                CachePolicyType lResult;
                                if ( Enum.TryParse( lXType.Value, out lResult ) )
                                {
                                    lType = lResult;
                                }
                            }

                            double lExpirationInterval = 5000;
                            XAttribute lXExpirationInterval = lXCache.Attribute( cExpirationIntervalAttribute );
                            if ( lXExpirationInterval != null )
                            {
                                double lResult;
                                if ( double.TryParse( lXExpirationInterval.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lResult ) )
                                {
                                    lExpirationInterval = lResult;
                                }
                            }

                            bool lAllowSliding = true;
                            XAttribute lXAllowSliding = lXCache.Attribute( cAllowSlidingAttribute );
                            if ( lXAllowSliding != null )
                            {
                                bool lResult;
                                if ( bool.TryParse( lXAllowSliding.Value, out lResult ) )
                                {
                                    lAllowSliding = lResult;
                                }
                            }

                            bool lAutoReload = true;
                            XAttribute lXAutoReload = lXCache.Attribute( cAutoReloadOnExpirationAttribute );
                            if ( lXAutoReload != null )
                            {
                                bool lResult;
                                if ( bool.TryParse( lXAutoReload.Value, out lResult ) )
                                {
                                    lAutoReload = lResult;
                                }
                            }

                            double lWatcherDelay = 1000;
                            XAttribute lXWatcherDelay = lXCache.Attribute( cFileWatcherDelayAttribute );
                            if ( lXWatcherDelay != null )
                            {
                                double lResult;
                                if ( double.TryParse( lXWatcherDelay.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lResult ) )
                                {
                                    lWatcherDelay = lResult;
                                }
                            }

                            lCache = new ConfigurationCache( lType, lExpirationInterval, lAllowSliding, lAutoReload, lWatcherDelay );
                        }

                        PluginConfiguration lConfiguration = new PluginConfiguration( lPluginFolder, lCreatePluginFolder, lKeepFileHandle, lCache );
                        return lConfiguration;
                    }
                }
            }

            return null;
        }

        #endregion Methods Statics

        #region Methods Internal

        /// <summary>
        /// Loads manager properties from configuration file
        /// </summary>
        private void LoadConfiguration()
        {
            string lConfigFilePath = Path.Combine( Environment.CurrentDirectory, @"..\Resources\PluginsConfiguration\PluginManager.xml" );
            this.mDefaultConfiguration = PluginManager.LoadConfiguration( lConfigFilePath );
            if ( this.mDefaultConfiguration != null )
            {
                this.mPluginsFolder       = this.mDefaultConfiguration.PluginsFolder;
                this.mKeepFileHandle      = this.mDefaultConfiguration.KeepFileHandle;
                this.mCreatePluginsFolder = this.mDefaultConfiguration.CreatePluginsFolder;
                if ( this.mDefaultConfiguration.Cache != null )
                {
                    this.mCachePolicy = new CachePolicy()
                    {
                        PolicyType                  = this.mDefaultConfiguration.Cache.Type,
                        FilesystemWatcherDelay      = this.mDefaultConfiguration.Cache.FilesystemWatcherDelay,
                        AutoReloadOnCacheExpiration = this.mDefaultConfiguration.Cache.AutoReloadOnCacheExpiration,
                        CacheExpirationInterval     = this.mDefaultConfiguration.Cache.CacheExpirationInterval,
                        AllowSlidingExpiration      = this.mDefaultConfiguration.Cache.AllowSlidingExpiration
                    };
                }
                else
                {
                    // Default policy.
                    this.mCachePolicy = new CachePolicy();
                }
            }
        }

        /// <summary>
        /// Initializes components of the manager class based on the properties values
        /// </summary>
        private void InitializePluginManager()
        {
            if ( Directory.Exists( this.mPluginsFolder ) == false && 
                 this.mCreatePluginsFolder )
            {
                Directory.CreateDirectory( this.mPluginsFolder );
            }

            if ( this.mCachePolicy != null && 
                 this.mCachePolicy.PolicyType.HasFlag( CachePolicyType.TimeInterval ) )
            {
                this.mCacheExpirationTimer = new Timer()
                {
                    Interval  = this.mCachePolicy.CacheExpirationInterval,
                    Enabled   = true,
                    AutoReset = true
                };

                this.mCacheExpirationTimer.Elapsed += this.OnCacheExpirationElapsed;
            }

            if ( this.mCachePolicy != null && 
                 this.mCachePolicy.PolicyType.HasFlag( CachePolicyType.FileWatch ) )
            {
                if ( Directory.Exists( this.mPluginsFolder ) )
                {
                    this.mWatcher = new FileSystemWatcher( this.mPluginsFolder, "*.dll" )
                    {
                        EnableRaisingEvents   = true,
                        IncludeSubdirectories = true
                    };

                    this.mWatcher.Changed += this.OnPluginChanged;
                    this.mWatcher.Deleted += this.OnPluginDeleted;
                }
                else
                {
                    throw new PluginDirectoryNotFoundException( string.Format( "Plugins directory \"{0}\" not found...", this.mPluginsFolder ) );
                }
            }
        }
        
        /// <summary>
        /// Loads assembly instance from FileSystem
        /// </summary>
        /// <param name="pFilePath">The Assembly file path to load.</param>
        /// <returns>The loaded assembly.</returns>
        private Assembly LoadAssemblyFromFilesystem(string pFilePath)
        {
            if ( File.Exists( pFilePath ) )
            {
                if ( this.mKeepFileHandle == false )
                {
                    return Assembly.Load( File.ReadAllBytes( pFilePath ) );
                }
                else
                {
                    return Assembly.LoadFile( pFilePath );
                }
            }
            else
            {
                throw new PluginFileNotFoundException( string.Format( "The plugin file \"{0}\" is missing...", pFilePath ) );
            }
        }

        #endregion Methods Internal

        #region Methods IPluginManager

        /// <summary>
        /// Initializes the plugin manager.
        /// NOTE: If parameters remain null, by default, a configuration file will be read in "Resources".
        /// </summary>
        /// <param name="pPluginsFolder">The plugin folder path.</param>
        /// <param name="pCachePolicy">The plugin manager cache policy.</param>
        /// <param name="pCreatePluginsFolder">The flag indicating whether the plugins folder must be created or not.</param>
        /// <param name="pKeepFileHandle">The flag indicating whether the file handle must be kept or not.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public bool Initialize(string pPluginsFolder = null, CachePolicy pCachePolicy = null, bool? pCreatePluginsFolder = null, bool? pKeepFileHandle = null)
        {
            this.LoadConfiguration(); // Default.

            // Overwrite with non-Null supplied parameters.
            if ( pPluginsFolder != null )
            {
                this.mPluginsFolder = pPluginsFolder;
            }

            if ( pCachePolicy != null )
            {
                this.mCachePolicy = pCachePolicy;
            }

            if ( pCreatePluginsFolder != null )
            {
                this.mCreatePluginsFolder = (bool)pCreatePluginsFolder;
            }

            if ( pKeepFileHandle != null )
            {
                this.mKeepFileHandle = (bool)pKeepFileHandle;
            }

            // Check for relevant member validity.
            if ( string.IsNullOrEmpty( this.mPluginsFolder ) ||
                 ( Directory.Exists( this.mPluginsFolder ) == false && this.mCreatePluginsFolder == false ) )
            {
                return false;
            }

            this.InitializePluginManager();

            return true;
        }

        /// <summary>
        /// Retrieves all plugins of the given base type corresponding to the supplied name.
        /// </summary>
        /// <typeparam name="T">Type of plugin base implementing IPlugin interface.</typeparam>
        /// <param name="pPluginName">The plugin assembly filename or full path to plugin assembly</param>
        /// <param name="pSubfolder">An optional subfolder name where plugin assembly is located inside the plugins folder</param>
        /// <returns>The set of plugins.</returns>
        public IEnumerable<T> GetPlugin<T>(string pPluginName, string pSubfolder = null) where T : class, IPlugin
        {
            string lPluginPath = string.Empty;
            
            // Prepare the assembly path.
            if ( File.Exists( pPluginName ) )
            {
                lPluginPath = new DirectoryInfo( pPluginName ).FullName.ToLower();
                if ( lPluginPath.Contains( this.mPluginsFolder ) )
                {
                    throw new PluginFileNotInPluginsFolderException( string.Format( "The plugin \"{0}\" is not in the plugins folder \"{1}\".", lPluginPath, this.mPluginsFolder ) );
                }
            }
            else
            {
                if ( string.IsNullOrWhiteSpace( pSubfolder ) )
                {
                    lPluginPath = Path.Combine( this.mPluginsFolder, string.Format( "{0}{1}", pPluginName, pPluginName.EndsWith( ".dll", StringComparison.InvariantCultureIgnoreCase ) == false ? ".dll" : string.Empty ) ).Trim().ToLower().ToString();
                }
                else
                {
                    lPluginPath = Path.Combine( this.mPluginsFolder, pSubfolder, string.Format( "{0}{1}", pPluginName, pPluginName.EndsWith( ".dll", StringComparison.InvariantCultureIgnoreCase ) == false ? ".dll" : string.Empty ) ).Trim().ToLower().ToString();
                }
            }

            PluginAssembly lAssemblyInfo = null;
            if ( this.mAssemblies.Value.TryGetValue( lPluginPath, out lAssemblyInfo ) )
            {
                // Update load time if sliding expiration
                if ( this.mCachePolicy.AllowSlidingExpiration )
                {
                    PluginAssembly lNewAssemblyInfo = new PluginAssembly( DateTime.Now, lPluginPath, lAssemblyInfo.Assembly, lAssemblyInfo.Plugins );
                    this.mAssemblies.Value.AddOrUpdate( lPluginPath, 
                                                        lNewAssemblyInfo, 
                                                        (pKey, pOldValue) => lNewAssemblyInfo );
                }
            }
            else
            {
                lAssemblyInfo = new PluginAssembly( DateTime.Now, lPluginPath, this.LoadAssemblyFromFilesystem( lPluginPath ) );
                //Do not add to cache if cache policy not defined or assembly does not have IPlugin classes
                if ( this.CachePolicy != null &&
                     lAssemblyInfo.Plugins.Any() )
                {
                    this.mAssemblies.Value.AddOrUpdate( lPluginPath, 
                                                        lAssemblyInfo, 
                                                        (pKey, pOldValue) => lAssemblyInfo );
                    if ( this.PluginLoaded != null )
                    {
                        this.PluginLoaded( this, new PluginEventArgs( lPluginPath ) );
                    }
                }
            }

            return lAssemblyInfo.Plugins.Cast<T>();
        }

        /// <summary>
        /// Retrieves all plugins of the given base type corresponding to the supplied name.
        /// </summary>
        /// <param name="pPluginName">The plugin assembly filename or full path to plugin assembly</param>
        /// <param name="pSubfolder">An optional subfolder name where plugin assembly is located inside the plugins folder</param>
        /// <returns>The set of plugins.</returns>
        public IEnumerable<IPlugin> GetPlugin(string pPluginName, string pSubfolder = null)
        {
            return this.GetPlugin<APlugin>( pPluginName, pSubfolder );
        }

        /// <summary>
        /// Retrieves all plugins corresponding to the supplied assembly file information.
        /// </summary>
        /// <param name="pPluginAssemblyFileInfo">The plugin assembly file info.</param>
        /// <returns>The set of plugins.</returns>
        public IEnumerable<IPlugin> GetPlugin(FileInfo pPluginAssemblyFileInfo)
        {
            return this.GetPlugin<APlugin>( pPluginAssemblyFileInfo.FullName.ToLower() );
        }

        /// <summary>
        /// Retrieves all plugins of the given base type corresponding to the supplied assembly file information.
        /// </summary>
        /// <typeparam name="T">The type of the plugin base implementing IPlugin interface</typeparam>
        /// <param name="pPluginAssemblyFileInfo">The plugin assembly file info.</param>
        /// <returns>The set of plugins.</returns>
        public IEnumerable<T> GetPlugin<T>(FileInfo pPluginAssemblyFileInfo) where T : class, IPlugin
        {
            return this.GetPlugin<T>( pPluginAssemblyFileInfo.FullName.ToLower() );
        }

        /// <summary>
        /// Loads every single plugins in the target plugin folder given either through the config file or as initial parameter.
        /// </summary>
        /// <returns>The amount of loaded plugins. 0 meaning no plugins loaded.</returns>
        public int LoadAllPlugins()
        {
            int lCount = 0;
            if ( string.IsNullOrEmpty( this.mPluginsFolder ) ||
                 Directory.Exists( this.mPluginsFolder ) == false ) // At this level, the folder must exist and contains plugin files.
            {
                return lCount;
            }

            string[] lPlugins = Directory.GetFiles( this.mPluginsFolder, "*.dll" );
            lCount = lPlugins.Length;
            for ( int lCurr = 0; lCurr < lCount; lCurr++ )
            {
                string lPluginPath = lPlugins[ lCurr ];
                try
                {
                    this.GetPlugin<IPlugin>( lPluginPath );
                }
                catch ( Exception pEx )
                {
                    LogManager.Instance.Log( string.Format( "Failed to load the plugin \"{0}\" due to : {1}.", lPluginPath, pEx.Message ) );
                }
            }

            return lCount;
        }

        #endregion Methods IPluginManager

        #region Methods Events

        /// <summary>
        /// Delegate called on plugin cache expiration.
        /// </summary>
        /// <param name="pSender">The timer.</param>
        /// <param name="pEventArg">The event arguments.</param>
        private void OnCacheExpirationElapsed(object pSender, ElapsedEventArgs pEventArg)
        {
            if ( this.mAssemblies != null && 
                 this.mAssemblies.Value != null )
            {
                IEnumerable<KeyValuePair<string, PluginAssembly>> lExpiredAssemblies = this.mAssemblies.Value.Where( lAssembly => lAssembly.Value.LoadTime.AddMilliseconds( this.mCachePolicy.CacheExpirationInterval ) <= DateTime.Now );
                if ( lExpiredAssemblies.Any() )
                {
                    foreach ( KeyValuePair<string, PluginAssembly> lExpiredAssembly in lExpiredAssemblies )
                    {
                        string lAssemblyPath = lExpiredAssembly.Key;

                        // Releases the old one.
                        lExpiredAssembly.Value.Dispose();

                        if ( this.mCachePolicy.AutoReloadOnCacheExpiration )
                        {
                            PluginAssembly lNewAssemblyInfo = new PluginAssembly( DateTime.Now, lAssemblyPath, this.LoadAssemblyFromFilesystem( lAssemblyPath ) );
                            this.mAssemblies.Value.AddOrUpdate( lAssemblyPath,
                                                                lNewAssemblyInfo,
                                                                (pKey, pOldValue) => lNewAssemblyInfo );
                            
                            if ( this.PluginLoaded != null )
                            {
                                this.PluginLoaded( this, new PluginEventArgs( lAssemblyPath ) );
                            }
                        }
                        else
                        {
                            PluginAssembly lRemovedAssembly;
                            if ( this.mAssemblies.Value.TryRemove( lAssemblyPath, out lRemovedAssembly ) )
                            {
                                if ( this.PluginUnloaded != null )
                                {
                                    this.PluginUnloaded( this, new PluginEventArgs( lAssemblyPath ) );
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called on plugin file changed.
        /// </summary>
        /// <param name="pSender">The file watcher.</param>
        /// <param name="pEventArg">The event arguments.</param>
        private void OnPluginChanged(object pSender, FileSystemEventArgs pEventArg)
        {
            PluginAssembly lLoadedAssemblyInfo = null;
            PluginAssembly lCachedAssemblyInfo = null;
            if ( pEventArg.ChangeType == WatcherChangeTypes.Changed )
            {
                string lSafePath = pEventArg.FullPath.Trim().ToLower();

                // Update in cache
                if ( this.mAssemblies.Value.TryGetValue( lSafePath, out lCachedAssemblyInfo ) )
                {
                    // Realod assembly if file is older than CachePolicy.FilesystemWatcherDelay miliseconds 
                    // to avoid issue with FileSystemWatcher multiple eventson file cache
                    if ( lCachedAssemblyInfo.LoadTime.AddMilliseconds( this.mCachePolicy.FilesystemWatcherDelay ) < DateTime.Now )
                    {
                        // Releases the old one.
                        lCachedAssemblyInfo.Dispose();

                        Assembly lLoadedAssembly = this.LoadAssemblyFromFilesystem( lSafePath );
                        if ( lLoadedAssembly != null )
                        {
                            lLoadedAssemblyInfo = new PluginAssembly( DateTime.Now, lSafePath, lLoadedAssembly );
                            this.mAssemblies.Value.AddOrUpdate( lSafePath,
                                                                lLoadedAssemblyInfo,
                                                                (pKey, pOldValue) => lLoadedAssemblyInfo );

                            if ( this.PluginLoaded != null )
                            {
                                this.PluginLoaded( this, new PluginEventArgs( lSafePath ) );
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called on plugin file deleted.
        /// </summary>
        /// <param name="pSender">The file watcher.</param>
        /// <param name="pEventArg">The event arguments.</param>
        private void OnPluginDeleted(object pSender, FileSystemEventArgs pEventArg)
        {
            PluginAssembly lCachedAssemblyInfo = null;
            if ( pEventArg.ChangeType == WatcherChangeTypes.Deleted )
            {
                string lSafePath = pEventArg.FullPath.Trim().ToLower();

                // Remove from cache
                if ( this.mAssemblies.Value.TryRemove( lSafePath, out lCachedAssemblyInfo ) )
                {
                    // Clean it.
                    lCachedAssemblyInfo.Dispose();

                    if ( this.PluginUnloaded != null )
                    {
                        this.PluginUnloaded( this, new PluginEventArgs( lSafePath ) );
                    }
                }
            }
        }

        #endregion Methods Events

        #region Methods IDisposable
        
        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDispose()
        {
            if ( this.mCacheExpirationTimer != null )
            {
                this.mCacheExpirationTimer.Stop();
                this.mCacheExpirationTimer.Enabled = false;
                this.mCacheExpirationTimer.Dispose();
                this.mCacheExpirationTimer = null;
            }

            if ( this.mWatcher != null )
            {
                this.mWatcher.EnableRaisingEvents = false;
                this.mWatcher.Dispose();
                this.mWatcher = null;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}