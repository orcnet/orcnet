﻿using OrcNet.Core;

namespace OrcNet.Plugins
{
    /// <summary>
    /// Definition of the <see cref="PluginModule"/> class.
    /// </summary>
    internal sealed class PluginModule : AModule
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginModule"/> class.
        /// </summary>
        public PluginModule()
        {

        }

        #endregion Constructor
    }
}
