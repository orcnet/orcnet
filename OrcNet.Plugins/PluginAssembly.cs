﻿using OrcNet.Core.Extensions;
using OrcNet.Core.Logger;
using OrcNet.Core.Plugins;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace OrcNet.Plugins
{
    /// <summary>
    /// Definition of the <see cref="PluginAssembly"/> class.
    /// </summary>
    internal class PluginAssembly : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the Date and time which the plugin has been loaded at.
        /// </summary>
        private DateTime mLoadTime;

        /// <summary>
        /// Stores the assembly's fullpath.
        /// </summary>
        private string   mFullPath;

        /// <summary>
        /// Stores the plugin's corresponding assembly object.
        /// </summary>
        private Assembly mAssembly;

        /// <summary>
        /// Stores the set of assembly's plugins.
        /// </summary>
        private List<IPlugin> mPlugins;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the Date and time which the plugin has been loaded at.
        /// </summary>
        public DateTime LoadTime
        {
            get
            {
                return this.mLoadTime;
            }
        }

        /// <summary>
        /// Gets the assembly's fullpath.
        /// </summary>
        public string FullPath
        {
            get
            {
                return this.mFullPath;
            }
        }

        /// <summary>
        /// Gets the plugin's corresponding assembly object.
        /// </summary>
        public Assembly Assembly
        {
            get
            {
                return this.mAssembly;
            }
        }

        /// <summary>
        /// Gets the set of assembly's plugins.
        /// </summary>
        public IEnumerable<IPlugin> Plugins
        {
            get
            {
                return this.mPlugins;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginAssembly"/> class.
        /// </summary>
        /// <param name="pLoadTime">The Date and time which the plugin has been loaded at.</param>
        /// <param name="pFullPath">The assembly's full path.</param>
        /// <param name="pAssembly">The plugin's corresponding assembly object.</param>
        /// <param name="pPlugins">The set of assembly's plugins.</param>
        public PluginAssembly(DateTime pLoadTime, string pFullPath, Assembly pAssembly, IEnumerable<IPlugin> pPlugins = null)
        {
            this.mFullPath = pFullPath;
            this.mAssembly = pAssembly;
            this.mLoadTime = pLoadTime;
            this.mPlugins  = pPlugins as List<IPlugin>;
            
            // If no plugins provided, create them.
            if ( this.mPlugins == null )
            {
                this.mPlugins = new List<IPlugin>();
                List<Type> lPlugins = this.mAssembly.GetInheritedTypes<IPlugin>();
                int lPluginCount = lPlugins.Count;
                for ( int lCurr = 0; lCurr < lPluginCount; lCurr++ )
                {
                    IPlugin lPlugin = PluginAssembly.CreatePlugin( lPlugins[ lCurr ], this.mFullPath );
                    if ( lPlugin != null )
                    {
                        this.mPlugins.Add( lPlugin );
                    }
                }
            }
        }

        #endregion Constructor

        #region Methods

        #region Methods Statics

        /// <summary>
        /// Create the plugin given its type.
        /// </summary>
        /// <param name="pPluginType">The plugin type.</param>
        /// <param name="pPluginPath">The plugin path.</param>
        /// <returns>The new plugin, Null if any failure.</returns>
        public static IPlugin CreatePlugin(Type pPluginType, string pPluginPath)
        {
            if ( pPluginType == null )
            {
                return null;
            }

            try
            {
                ConstructorInfo lConstructor = pPluginType.GetConstructor( new Type[] { typeof(string) } );
                IPlugin lNewPlugin = lConstructor.Invoke( new object[] { pPluginPath } ) as IPlugin;
                lNewPlugin.Initialize();

                return lNewPlugin;
            }
            catch
            {
                LogManager.Instance.Log( string.Format( "Failed to create the plugin \"{0}\"", pPluginType.FullName ) );
            }

            return null;
        }

        #endregion Methods Statics

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.mAssembly = null;
            int lPluginCount = this.mPlugins.Count;
            for ( int lCurr = 0; lCurr < lPluginCount; lCurr++ )
            {
                IPlugin lPlugin = this.mPlugins[ lCurr ];
                lPlugin.Dispose();
            }
            this.mPlugins.Clear();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
