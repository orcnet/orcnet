﻿using OrcNet.Core;
using OrcNet.Core.Extensions;
using OrcNet.Core.Plugins;
using OrcNet.Core.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace OrcNet.Plugins
{
    /// <summary>
    /// Definition of the <see cref="APlugin"/> class.
    /// </summary>
    public abstract class APlugin : IPlugin
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the plugin has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the file path of the plugin assembly.
        /// </summary>
        private string mAssemblyLocation;

        /// <summary>
        /// Stores the plugin's configuration.
        /// </summary>
        private PluginConfiguration mPluginConfiguration;

        /// <summary>
        /// Stores the services constrainer Type in the case of multiple modules per assembly.
        /// </summary>
        protected HashSet<Type> mServicesContrainers;
        
        /// <summary>
        /// Stores the specific plugin's services.
        /// </summary>
        private List<IPluginService> mPluginServices;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the plugin's services.
        /// </summary>
        public IPluginService[] Services
        {
            get
            {
                return this.mPluginServices.ToArray();
            }
        }

        /// <summary>
        /// Gets the file path of the plugin assembly.
        /// </summary>
        public string AssemblyLocation
        {
            get
            {
                return this.mAssemblyLocation;
            }
        }

        /// <summary>
        /// Gets the plugin's configuration.
        /// </summary>
        public PluginConfiguration PluginConfiguration
        {
            get
            {
                return this.mPluginConfiguration;
            }
        }
        
        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="APlugin"/> class.
        /// </summary>
        protected APlugin() :
        this( null )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="APlugin"/> class.
        /// </summary>
        /// <param name="pAssemblyPath">The assembly file path of the loaded plugin to load it's own configuration.</param>
        protected APlugin(string pAssemblyPath = null)
        {
            this.mIsDisposed          = false;
            this.mServicesContrainers = new HashSet<Type>();
            this.mPluginServices      = new List<IPluginService>();
            this.mAssemblyLocation    = pAssemblyPath;
        }

        #endregion Constructors

        #region Methods

        #region Methods IPlugin

        /// <summary>
        /// Initializes the plugin and its services.
        /// </summary>
        /// <returns></returns>
        public bool Initialize()
        {
            if ( string.IsNullOrWhiteSpace( this.mAssemblyLocation ) == false && 
                 File.Exists( string.Format( "{0}.xml", this.mAssemblyLocation ) ) )
            {
                this.mPluginConfiguration = PluginManager.LoadConfiguration( this.mAssemblyLocation + ".xml" );
            }
            else
            {
                this.mPluginConfiguration = PluginManager.LoadConfiguration( Assembly.GetExecutingAssembly().Location + ".xml" );
            }

            if ( this.mPluginConfiguration == null )
            {
                this.mPluginConfiguration = ServiceManager.Instance.GetService<IPluginManager>().DefaultConfiguration;
            }

            // Retrieves the plugin service linked to this assembly.
            Assembly lAssembly = this.GetType().Assembly;

            // Load all service being in the module assembly as Core services.
            List<Type> lServiceTypes = lAssembly.GetInheritedTypes<IPluginService>();
            int lServiceCount = lServiceTypes.Count;
            if ( lServiceCount > 0 )
            {
                // Creates and register them.
                for ( int lCurr = 0; lCurr < lServiceCount; lCurr++ )
                {
                    Type lServiceType = lServiceTypes[ lCurr ];
                    IService lPluginService = ServiceManager.CreateService( lServiceType, this );
                    if ( lPluginService != null )
                    {
                        // Those services can be unregistered.
                        this.mPluginServices.Add( lPluginService as IPluginService );
                        ServiceManager.Instance.RegisterService( lPluginService );
                    }
                }
            }

            return this.OnInitialize();
        }

        /// <summary>
        /// Initializes extra plugin elements.
        /// </summary>
        /// <returns>True if successful, false otherwise.</returns>
        protected virtual bool OnInitialize()
        {
            // To override.
            return true;
        }

        /// <summary>
        /// Gets a plugin's service corresponding to the given generic type. Can be from whatever plugin loaded.
        /// </summary>
        /// <typeparam name="T">The service type.</typeparam>
        /// <returns>The plugin service, null otherwise.</returns>
        public T GetService<T>() where T : class, IPluginService
        {
            // Wrapp the Service manager T being constrained by the calling method.
            // Furthermore, the Service manager will only contain the service if loaded as service(s) is(are) added/removed on plugin load/unload.
            return ServiceManager.Instance.GetService<T>();
        }

        #endregion Methods IPlugin

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                // Allow user extra clean.
                this.OnDisposed();

                // Make sure the services be cleaned.
                int lPluginServiceCount = this.mPluginServices.Count;
                for ( int lCurr = 0; lCurr < lPluginServiceCount; lCurr++ )
                {
                    IPluginService lService = this.mPluginServices[ lCurr ];
                    ServiceManager.Instance.UnregisterService( lService );
                    lService.Dispose();
                }
                this.mPluginServices.Clear();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDisposed()
        {
            // Allow extra clean.
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
