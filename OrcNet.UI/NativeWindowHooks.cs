﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="NativeWindowHooks"/> class.
    /// </summary>
    public class NativeWindowHooks
    {
        #region Properties

        /// <summary>
        /// Gets or sets the size changed handler.
        /// </summary>
        public SizeChangedDelegate SizeChangedHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the close handler.
        /// </summary>
        public CloseDelegate CloseHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the visibility handler.
        /// </summary>
        public VisibilityDelegate VisibilityHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the activate handler.
        /// </summary>
        public ActivateDelegate ActivateHandler
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowHooks"/> class.
        /// </summary>
        public NativeWindowHooks() :
        this( null, null, null, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowHooks"/> class.
        /// </summary>
        /// <param name="pSizeChangedHandler">The size changed handler.</param>
        public NativeWindowHooks(SizeChangedDelegate pSizeChangedHandler) :
        this( pSizeChangedHandler, null, null, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowHooks"/> class.
        /// </summary>
        /// <param name="pCloseHandler">The close handler.</param>
        public NativeWindowHooks(CloseDelegate pCloseHandler) :
        this( null, pCloseHandler, null, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowHooks"/> class.
        /// </summary>
        /// <param name="pVisibilityHandler">The visibility handler.</param>
        public NativeWindowHooks(VisibilityDelegate pVisibilityHandler) :
        this( null, null, pVisibilityHandler, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowHooks"/> class.
        /// </summary>
        /// <param name="pActivateHandler">The activate handler.</param>
        public NativeWindowHooks(ActivateDelegate pActivateHandler) :
        this( null, null, null, pActivateHandler )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowHooks"/> class.
        /// </summary>
        /// <param name="pSizeChangedHandler">The size changed handler.</param>
        /// <param name="pCloseHandler">The close handler.</param>
        /// <param name="pVisibilityHandler">The visibility handler.</param>
        /// <param name="pActivateHandler">The activate handler.</param>
        public NativeWindowHooks(SizeChangedDelegate pSizeChangedHandler, CloseDelegate pCloseHandler, VisibilityDelegate pVisibilityHandler, ActivateDelegate pActivateHandler)
        {
            this.SizeChangedHandler = pSizeChangedHandler;
            this.CloseHandler = pCloseHandler;
            this.VisibilityHandler = pVisibilityHandler;
            this.ActivateHandler = pActivateHandler;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Merges the hooks with that one.
        /// </summary>
        /// <param name="pHooks">The other hooks.</param>
        public NativeWindowHooks Merge(NativeWindowHooks pHooks)
        {
            if ( this.SizeChangedHandler == null )
            {
                this.SizeChangedHandler = pHooks.SizeChangedHandler;
            }

            if ( this.CloseHandler == null )
            {
                this.CloseHandler = pHooks.CloseHandler;
            }

            if ( this.ActivateHandler == null )
            {
                this.ActivateHandler = pHooks.ActivateHandler;
            }

            if ( this.VisibilityHandler == null )
            {
                this.VisibilityHandler = pHooks.VisibilityHandler;
            }

            return this;
        }

        #endregion Methods
    }
}
