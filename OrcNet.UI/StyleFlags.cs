﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="StyleFlags"/> enumeration.
    /// </summary>
    [Flags]
    public enum StyleFlags : int
    {
        /// <summary>
        /// The window has a title.
        /// </summary>
        HasTitle = 0x00000001,

        /// <summary>
        /// The window has a border.
        /// </summary>
        HasBorder = 0x00000002,

        /// <summary>
        /// The window has a menu.
        /// </summary>
        HasMenu = 0x00000004,
        
        /// <summary>
        /// The window is initially minimized.
        /// </summary>
        Minimized = 0x0000008,

        /// <summary>
        /// The window is initially maximized.
        /// </summary>
        Maximized = 0x00000010,

        /// <summary>
        /// The window can be minimize.
        /// </summary>
        CanMinimize = 0x00000020,

        /// <summary>
        /// The window can be maximize.
        /// </summary>
        CanMaximize = 0x00000040,

        /// <summary>
        /// The window is initially disabled.
        /// </summary>
        Disbaled = 0x00000080,

        /// <summary>
        /// The window is initially visible.
        /// </summary>
        Visible = 0x00000100,

        /// <summary>
        /// The window is a popup.
        /// </summary>
        Popup = 0x10000000,
    }
}
