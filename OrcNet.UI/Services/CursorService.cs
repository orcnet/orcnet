﻿using OrcNet.Core.Service;

namespace OrcNet.UI.Services
{
    /// <summary>
    /// Definition of the <see cref="CursorService"/> class.
    /// </summary>
    public class CursorService : AService, ICursorService
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CursorService"/> class.
        /// </summary>
        public CursorService()
        {

        }
        
        #endregion Constructor

        #region Methods

        /// <summary>
        /// Loads the cursor(s) in the given file path.
        /// </summary>
        /// <param name="pFilePath">The file path.</param>
        /// <returns>The cursor.</returns>
        public XCursor LoadCursor(string pFilePath)
        {
            return XCursorFile.Load( pFilePath ).Cursors[ 0 ];
        }

        #endregion Methods
    }
}
