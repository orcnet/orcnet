﻿using OrcNet.Core;

namespace OrcNet.UI.Services
{
    /// <summary>
    /// Definition of the <see cref="ICursorService"/> interface.
    /// </summary>
    public interface ICursorService : IService
    {
        #region Properties



        #endregion Properties
        
        #region Methods

        /// <summary>
        /// Loads the cursor(s) in the given file path.
        /// </summary>
        /// <param name="pFilePath">The file path.</param>
        /// <returns>The cursor.</returns>
        XCursor LoadCursor(string pFilePath);

        #endregion Methods
    }
}
