﻿using System.Collections.Generic;

namespace OrcNet.UI
{
	/// <summary>
	/// Element class of the LayoutingQueue
	/// </summary>
	public struct LayoutItem
	{
        #region Properties

        /// <summary>
        /// The target element.
        /// </summary>
        public UIElement Target { get; set; }

        /// <summary>
        /// The layout update purpose.
        /// </summary>
        public LayoutingType LayoutType { get; set; }

        /// <summary>
        /// The layout update tries.
        /// </summary>
        public int LayoutingTries { get; set; }

        /// <summary>
        /// The layout update discard count.
        /// </summary>
        public int DiscardCount { get; set; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LayoutItem"/> class.
        /// </summary>
        /// <param name="pLayoutType"></param>
        /// <param name="pTarget"></param>
        public LayoutItem(LayoutingType pLayoutType, UIElement pTarget)
		{			
			this.LayoutType = pLayoutType;
			this.Target = pTarget;
			this.Target.State |= LayoutType;
			this.LayoutingTries = 0;
			this.DiscardCount = 0;
		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Processes layouting.
        /// </summary>
        public void ProcessLayouting()
        {
            if ( this.Target.VisualParent == null )
            {
                return;
            }

            this.LayoutingTries++;
            this.Target.ArrangeConstraint = this.LayoutType;

            if ( this.Target.UpdateLayout() == false )
            {
                if ( this.LayoutingTries < ContextLayoutManager.MaxLayoutingTries )
                {
                    this.Target.State |= this.LayoutType;
                    ContextLayoutManager.From( this.Target.Dispatcher ).ArrangeQueue.Enqueue( this );
                }
                else if ( this.DiscardCount < ContextLayoutManager.MaxDiscardCount )
                {
                    this.LayoutingTries = 0;
                    this.DiscardCount++;
                    this.Target.State |= this.LayoutType;
                    ContextLayoutManager.From( this.Target.Dispatcher ).DiscardQueue.Enqueue( this );
                }
            }
        }
        
        /// <summary>
        /// Turns this object into a string.
        /// </summary>
        /// <returns></returns>
		public override string ToString()
		{
			return string.Format("{2};{3} {1}->{0}", this.LayoutType, this.Target.ToString(), this.LayoutingTries, this.DiscardCount);
		}

        #endregion Methods
    }

    /// <summary>
    /// Definition of the <see cref="LQIList"/> class.
    /// </summary>
	public class LQIList : List<LayoutItem>
    {

	}
}

