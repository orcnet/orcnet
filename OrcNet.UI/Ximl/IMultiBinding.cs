﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IMultiBinding"/> interface.
    /// </summary>
    public interface IMultiBinding : IBinding
    {
        #region Methods

        /// <summary>
        /// Bind the To-bind-instances regarding to the binding information.
        /// </summary>
        /// <param name="pSource">The source instance.</param>
        /// <param name="pTargets">The target instances involved in the multi binding.</param>
        /// <returns>True if successfully bound, false otherwise.</returns>
        bool Bind(IBindable pSource, IBindable[] pTargets);

        #endregion Methods
    }
}
