﻿using System;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="EventBindingExpression"/> class.
    /// </summary>
    public class EventBindingExpression : IEventBinding
    {
        #region Fields

        /// <summary>
        /// Stores the binding status.
        /// </summary>
        private BindingStatus mStatus;

        /// <summary>
        /// Stores the flag indicating whether the bidning must do nothing on value changes or not.
        /// </summary>
        private bool mDoNothing;

        /// <summary>
        /// Stores the binding event handler.
        /// </summary>
        private Delegate mHandler;

        /// <summary>
        /// Stores the ref to the source instance.
        /// </summary>
        protected object mSource;

        /// <summary>
        /// Stores the ref to the target instance.
        /// </summary>
        protected object mTarget;

        /// <summary>
        /// Stores the source event to register to.
        /// </summary>
        private EventInfo mSourceEvent;

        /// <summary>
        /// Stores the target delegate to run.
        /// </summary>
        private MethodInfo mTargetDelegate;

        /// <summary>
        /// Stores the flag indicating whether the binding is disposed or not
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the event name to bind with.
        /// </summary>
        private string mEventName;

        /// <summary>
        /// Stores the delegate name to bind with the event.
        /// </summary>
        private string mTargetDelegateName;

        /// <summary>
        /// Stores the target element name in the case the delegate in not in the current DataContext object.
        /// </summary>
        private string mTargetElementName;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the binding status.
        /// </summary>
        public BindingStatus Status
        {
            get
            {
                return this.mStatus;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the binding must do nothing on value changes or not.
        /// </summary>
        public bool DoNothing
        {
            get
            {
                return this.mDoNothing;
            }
            set
            {
                this.mDoNothing = value;
            }
        }

        /// <summary>
        /// Gets or sets the format in the case the value is displayed as a string.
        /// NOTE : Not used for event binding.
        /// </summary>
        public string Format
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value to set to the target in the case the source is Null.
        /// NOTE : Not used for event binding.
        /// </summary>
        public object TargetNullValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value to use if the binding fails.
        /// NOTE : Not used for event binding.
        /// </summary>
        public object FallbackValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the bound member name.
        /// </summary>
        string IEventBinding.MemberName
        {
            get
            {
                return this.EventName;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the binding is for a template definition or not.
        /// </summary>
        public bool IsTemplate
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the event name to bind with.
        /// </summary>
        public string EventName
        {
            get
            {
                return this.mEventName;
            }
        }

        /// <summary>
        /// Gets the delegate name to bind with the event.
        /// </summary>
        public string TargetDelegateName
        {
            get
            {
                return this.mTargetDelegateName;
            }
        }

        /// <summary>
        /// Gets the target element name in the case the delegate in not in the current DataContext object.
        /// </summary>
        public string TargetElementName
        {
            get
            {
                return this.mTargetElementName;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EventBindingExpression"/> class.
        /// </summary>
        /// <param name="pEventName">The event name to bind to.</param>
        /// <param name="pTargetDelegateName">The delegate name to register.</param>
        /// <param name="pTargetElementName">The target element name in the case the delegate in not in the current DataContext object.</param>
        public EventBindingExpression(string pEventName, string pTargetDelegateName, string pTargetElementName = null)
        {
            this.mEventName = pEventName;
            this.mTargetDelegateName = pTargetDelegateName;
            this.mTargetElementName  = pTargetElementName;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Bind the To-bind-instances regarding to the binding information.
        /// </summary>
        /// <param name="pSource">The source instance.</param>
        /// <param name="pTarget">The target instance.</param>
        /// <returns>True if successfully bound, false otherwise.</returns>
        public bool Bind(IBindable pSource, IBindable pTarget)
        {
            if ( pSource == null ||
                 pTarget == null )
            {
                this.mStatus = BindingStatus.Inactive;
                return false;
            }

            Type lSourceType  = pSource.GetType();
            this.mSourceEvent = lSourceType.GetEvent( this.mEventName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public );

            if ( this.mSourceEvent == null )
            {
                this.mStatus = BindingStatus.SourceError;
                return false;
            }

            Type lTargetType  = pTarget.GetType();
            this.mTargetDelegate = lTargetType.GetMethod( this.mTargetDelegateName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public );

            if ( this.mTargetDelegate == null )
            {
                this.mStatus = BindingStatus.TargetError;
                return false;
            }

            try
            {
                this.mHandler = Delegate.CreateDelegate( this.mSourceEvent.EventHandlerType, this.mTargetDelegate );
                this.mSourceEvent.AddEventHandler( this.mTarget, this.mHandler );
            }
            catch
            {
                this.mStatus = BindingStatus.PathError;
                return false;
            }

            this.mStatus = BindingStatus.Binded;
            return true;
        }
        
        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            if ( this.mSource != null )
            {
                this.mSourceEvent.RemoveEventHandler( this.mTarget, this.mHandler );
                this.mSourceEvent = null;
                this.mHandler = null;
                this.mSource = null;
            }

            if ( this.mTarget != null )
            {
                this.mTargetDelegate = null;
                this.mTarget = null;
            }
        }

        #endregion Methods
    }
}
