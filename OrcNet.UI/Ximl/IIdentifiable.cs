﻿using System;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IIdentifiable"/> interface.
    /// </summary>
    public interface IIdentifiable : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the id.
        /// </summary>
        string Id
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Attempt to find the identifiable corresponding to the given Id.
        /// </summary>
        /// <param name="pId">The Id to look for.</param>
        /// <returns>The found identifiable, null otherwise.</returns>
        IIdentifiable Find(string pId);

        #endregion Methods
    }
}
