﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="XimlConstants"/> class.
    /// 
    /// Especially Ximl keyword definition as constants created once for all.
    /// </summary>
    public static class XimlConstants
    {
        #region Methods

        /// <summary>
        /// The constant class Xml tag.
        /// </summary>
        public const string cClassTag = "Class";

        /// <summary>
        /// The constant StaticResource keyword Xml tag.
        /// </summary>
        public const string cStaticResourceTag = "StaticResource";

        /// <summary>
        /// The constant style Xml tag.
        /// </summary>
        public const string cStyleTag = "Style";

        /// <summary>
        /// The constant set of resources Xml tag.
        /// </summary>
        public const string cResourcesTag = "Resources";

        /// <summary>
        /// The constant window Xml tag.
        /// </summary>
        public const string cWindowTag = "Window";

        /// <summary>
        /// The constant Solid color brush Xml tag.
        /// </summary>
        public const string cSolidBrush = "SolidColorBrush";

        /// <summary>
        /// The constant default style suffix.
        /// </summary>
        public const string cDefault = "Default";

        /// <summary>
        /// The constant default style suffix.
        /// </summary>
        public const string cDefaultStyle = "DefaultStyle";

        /// <summary>
        /// The constant resource key Xml tag.
        /// </summary>
        public const string cResourceKeyTag = "Key";

        /// <summary>
        /// The constant Name Xml tag.
        /// </summary>
        public const string cNameTag = "Name";

        /// <summary>
        /// The constants template Xml tag.
        /// </summary>
        public const string cTemplateTag = "Template";

        /// <summary>
        /// The constants control template Xml tag.
        /// </summary>
        public const string cControlTemplateTag = "ControlTemplate";

        /// <summary>
        /// The constants property Xml tag.
        /// </summary>
        public const string cPropertyTag = "Property";

        /// <summary>
        /// The constants value Xml tag.
        /// </summary>
        public const string cValueTag = "Value";

        /// <summary>
        /// The constants target type Xml tag.
        /// </summary>
        public const string cTargetTypeTag = "TargetType";

        /// <summary>
        /// The constants target name Xml tag.
        /// </summary>
        public const string cTargetNameTag = "TargetName";

        /// <summary>
        /// The constant setter Xml tag.
        /// </summary>
        public const string cSetterTag = "Setter";

        /// <summary>
        /// The constants trigger Xml tag.
        /// </summary>
        public const string cTriggerTag = "Trigger";

        /// <summary>
        /// The constants set of triggers Xml tag.
        /// </summary>
        public const string cTriggersTag = "Triggers";

        /// <summary>
        /// The constant binding Xml tag.
        /// </summary>
        public const string cBindingTag = "Binding";

        /// <summary>
        /// The constant template binding Xml tag.
        /// </summary>
        public const string cTemplateBindingTag = "TemplateBinding";

        /// <summary>
        /// The constant element name binding Xml tag.
        /// </summary>
        public const string cElementNameBindingTag = "ElementName";

        /// <summary>
        /// The constant element path binding Xml tag.
        /// </summary>
        public const string cElementPathBindingTag = "Path";

        /// <summary>
        /// The constant binding mode Xml tag.
        /// </summary>
        public const string cBindingModeTag = "Mode";

        /// <summary>
        /// The constant Color Xml tag.
        /// </summary>
        public const string cColorTag = "Color";

        /// <summary>
        /// The constant Grid Xml tag.
        /// </summary>
        public const string cGridTag = "Grid";

        /// <summary>
        /// The constant Grid column definition Xml tag.
        /// </summary>
        public const string cColumnDefinitionTag = "ColumnDefinition";

        /// <summary>
        /// The constant width Xml tag.
        /// </summary>
        public const string cWidthTag = "Width";

        /// <summary>
        /// The constant minimum width Xml tag.
        /// </summary>
        public const string cMinWidthTag = "MinWidth";

        /// <summary>
        /// The constant maximum width Xml tag.
        /// </summary>
        public const string cMaxWidthTag = "MaxWidth";

        /// <summary>
        /// The constant Grid row definition Xml tag.
        /// </summary>
        public const string cRowDefinitionTag = "RowDefinition";

        /// <summary>
        /// The constant height Xml tag.
        /// </summary>
        public const string cHeightTag = "Height";

        /// <summary>
        /// The constant minimum height Xml tag.
        /// </summary>
        public const string cMinHeightTag = "MinHeight";

        /// <summary>
        /// The constant maximum height Xml tag.
        /// </summary>
        public const string cMaxHeightTag = "MaxHeight";

        #endregion Methods
    }
}
