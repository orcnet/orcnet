﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="BindingStatus"/> enumeration.
    /// </summary>
    public enum BindingStatus
    {
        /// <summary>
        /// The binding is not ready yet.
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// The source and target are linked.
        /// </summary>
        Binded = 0x01,
        
        /// <summary>
        /// The path to the target property to bind is wrong.
        /// </summary>
        PathError = 0x02,

        /// <summary>
        /// Invalid source.
        /// </summary>
        SourceError = 0x04,

        /// <summary>
        /// Invalid target.
        /// </summary>
        TargetError = 0x08
    }
}
