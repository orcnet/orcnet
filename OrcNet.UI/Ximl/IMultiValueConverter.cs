﻿using System;
using System.Globalization;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IMultiValueConverter"/> interface.
    /// </summary>
    public interface IMultiValueConverter
    {
        #region Methods

        /// <summary>
        /// Converts a set of values into a single object of the given target type.
        /// </summary>
        /// <param name="pSources">The values to convert.</param>
        /// <param name="pTargetType">The target type to convert values to.</param>
        /// <param name="pParameter">An optional parameter for the conversion.</param>
        /// <param name="pCulture">The culture to use if needed.</param>
        /// <returns>The converted object.</returns>
        object Convert(object[] pSources, Type pTargetType, object pParameter, CultureInfo pCulture);

        /// <summary>
        /// Converts a single value into those corresponding to the given target types.
        /// </summary>
        /// <param name="pSource">The value to convert.</param>
        /// <param name="pTargetTypes">The target types to convert value (for each) to.</param>
        /// <param name="pParameter">An optional parameter for the conversion.</param>
        /// <param name="pCulture">The culture to use if needed.</param>
        /// <returns>The converted set of objects.</returns>
        object[] ConvertBack(object pSource, Type[] pTargetTypes, object pParameter, CultureInfo pCulture);

        #endregion Methods
    }
}
