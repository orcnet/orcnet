﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IStylable"/> interface.
    /// </summary>
    public interface IStylable
    {
        #region Properties

        /// <summary>
        /// Gets the style.
        /// </summary>
        Style Style
        {
            get;
        }

        #endregion Properties
    }
}
