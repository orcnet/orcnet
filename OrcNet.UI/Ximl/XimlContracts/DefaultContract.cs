﻿using System;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="DefaultContract"/> class.
    /// </summary>
    public class DefaultContract : AXimlParserContract
    {
        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return XimlConstants.cDefault;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Check whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public override int CanManage(Type pType)
        {
            return int.MaxValue;
        }

        #endregion Methods
    }
}
