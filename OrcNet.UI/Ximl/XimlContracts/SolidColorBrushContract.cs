﻿using OrcNet.UI.Ximl.Helpers;
using System;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="SolidColorBrushContract"/> class.
    /// </summary>
    public class SolidColorBrushContract : AXimlParserContract
    {
        #region Fields

        /// <summary>
        /// Stores the solid color brush type.
        /// </summary>
        private static readonly Type sSolidBrushType = typeof(SolidColorBrush);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return XimlConstants.cSolidBrush;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Check whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public override int CanManage(Type pType)
        {
            if ( sSolidBrushType.IsAssignableFrom( pType ) )
            {
                return 0;
            }

            return -1;
        }

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The resulting Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            XAttribute lXColor = pElement.Attribute( XimlConstants.cColorTag );
            if ( lXColor == null )
            {
                return false;
            }

            string lColorAsString = lXColor.Value;
            Color lColor = (Color)XimlHelpers.GetValue( lColorAsString, typeof(Color), pContext.Resources );

            string lKey = null;
            XAttribute lXKey = pElement.Attribute( XimlConstants.cResourceKeyTag );
            if ( lXKey != null )
            {
                lKey = lXKey.Value;
            }

            SolidColorBrush lNewBrush = new SolidColorBrush( lColor, lKey );
            pContext.Current = lNewBrush;

            // Set it to the global resource cache.
            pContext.Resources.Add( lNewBrush );

            // Add it to the current Ximl node resource as well.
            pParent.Resources.Add( lNewBrush );

            return base.Read( pElement, pContext, pParent );
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            bool lResult = base.Write( pToWrite, pContext, pElement );


            return true;
        }

        #endregion Methods
    }
}
