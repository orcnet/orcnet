﻿namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="ControlTemplateTriggersContract"/> class.
    /// </summary>
    public class ControlTemplateTriggersContract : DefaultContract
    {
        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return string.Format( "{0}.{1}", XimlConstants.cControlTemplateTag, XimlConstants.cTriggersTag );
            }
        }

        #endregion Properties
    }
}
