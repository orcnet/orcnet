﻿using OrcNet.UI.Extensions;
using OrcNet.UI.Ximl.Helpers;
using System;
using System.Reflection;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="SetterContract"/> class.
    /// </summary>
    public class SetterContract : AXimlParserContract
    {
        #region Fields

        /// <summary>
        /// Stores the base setter type.
        /// </summary>
        private static readonly Type sSetterType = typeof(StyleSetter);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return XimlConstants.cSetterTag;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Check whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public override int CanManage(Type pType)
        {
            if ( sSetterType == pType ||
                 sSetterType.IsAssignableFrom( pType ) )
            {
                return 0;
            }

            return -1;
        }

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The parent Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            if ( pContext.TargetType == null )
            {
                return false;
            }

            XAttribute lXPropertyName = pElement.Attribute( XimlConstants.cPropertyTag );
            if ( lXPropertyName == null )
            {
                return false;
            }

            string lPropertyName = lXPropertyName.Value;
            PropertyInfo lProperty = PropertyInfoExtensions.GetProperty( pContext.TargetType, lPropertyName );

            string lTargetName = null;
            XAttribute lXTargetName = pElement.Attribute( XimlConstants.cTargetNameTag );
            if ( lXTargetName != null )
            {
                lTargetName = lXTargetName.Value;
            }

            object lValue = null;
            XAttribute lXValue = pElement.Attribute( XimlConstants.cValueTag );
            if ( lXValue != null )
            {
                // Either direct assignment or Static resource.
                string lValueInfo = lXValue.Value;
                lValue = XimlHelpers.GetValue( lValueInfo, lProperty.PropertyType, pContext.Resources );
            }

            // If no value, no big deal, it could be described into child elements
            
            StyleSetter lSetter = new StyleSetter( lProperty, lValue, lTargetName );

            ISetterContainer lSetterContainer = pContext.Current as ISetterContainer;
            if ( lSetterContainer != null )
            {
                lSetterContainer.AddSetter( lSetter );
            }

            // Set the setter as currently edited object so children set the value in it then.
            pContext.Current = lSetter;

            return base.Read( pElement, pContext, pParent );
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            bool lResult = base.Write( pToWrite, pContext, pElement );


            return true;
        }

        #endregion Methods
    }
}
