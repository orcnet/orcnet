﻿namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="StyleTriggersContract"/> class.
    /// </summary>
    public class StyleTriggersContract : DefaultContract
    {
        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return string.Format( "{0}.{1}", XimlConstants.cStyleTag, XimlConstants.cTriggersTag );
            }
        }

        #endregion Properties
    }
}
