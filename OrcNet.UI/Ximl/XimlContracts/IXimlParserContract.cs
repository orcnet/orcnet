﻿using System;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="IXimlParserContract"/> interface.
    /// </summary>
    public interface IXimlParserContract
    {
        #region Properties

        /// <summary>
        /// Gets the contract by Tag differenciating the different Ximl contract
        /// </summary>
        string Tag
        {
            get;
        }

        /// <summary>
        /// Gets the contract version.
        /// </summary>
        Version Version
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Checkw whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        int CanManage(Type pType);

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The parent Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent);

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement);

        #endregion Methods
    }
}
