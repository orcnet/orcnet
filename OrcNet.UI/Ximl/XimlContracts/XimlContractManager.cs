﻿using OrcNet.UI.Extensions;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="XimlContractManager"/> class.
    /// </summary>
    public class XimlContractManager
    {
        #region Fields

        /// <summary>
        /// Stores the static default contract to assure a contract is always returned by default.
        /// </summary>
        private static DefaultContract sDefault = new DefaultContract();

        /// <summary>
        /// Stores the Ximl contract manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile XimlContractManager sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the parser contracts.
        /// </summary>
        private Dictionary<string, IXimlParserContract> mContracts;

        /// <summary>
        /// Stores the best contract by type cache.
        /// </summary>
        private Dictionary<Type, IXimlParserContract> mContractByTypeCache;

        #endregion Fields

        #region Methods

        /// <summary>
        /// Gets the Ximl contract manager handle.
        /// </summary>
        public static XimlContractManager Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if ( sInstance == null )
                {
                    // Lock on
                    lock ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if ( sInstance == null )
                        {
                            sInstance = new XimlContractManager();
                        }
                    }
                }

                return sInstance;
            }
        }

        #endregion Methods

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="XimlContractManager"/> class.
        /// </summary>
        private XimlContractManager()
        {
            this.mContractByTypeCache = new Dictionary<Type, IXimlParserContract>();

            this.mContracts = new Dictionary<string, IXimlParserContract>();
            // Load all service being in the module assembly as Core services.
            IEnumerable<Type> lContractTypes = typeof(IXimlParserContract).GetInheritedTypes();
            foreach ( Type lType in lContractTypes )
            {
                ConstructorInfo lConstructor = lType.GetConstructor( new Type[] { } );
                IXimlParserContract lContract = lConstructor.Invoke( new object[] { } ) as IXimlParserContract;
                this.mContracts.Add( lContract.Tag, lContract );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the contract corresponding to the given tag.
        /// </summary>
        /// <param name="pTag">The Xml Tag to get the contract of.</param>
        /// <returns>The Ximl contract, Null otherwise.</returns>
        public IXimlParserContract GetContract(string pTag)
        {
            IXimlParserContract lContract = null;
            if ( this.mContracts.TryGetValue( pTag, out lContract ) )
            {
                return lContract;
            }

            return null;
        }

        /// <summary>
        /// Gets the contract that would have any support for the given type.
        /// </summary>
        /// <param name="pType">The type to look for a parser contract support.</param>
        /// <returns>The Ximl parser contract, Null otherwise.</returns>
        public IXimlParserContract GetContract(Type pType)
        {
            if ( pType == null )
            {
                return sDefault;
            }

            IXimlParserContract lContract = null;
            if ( this.mContractByTypeCache.TryGetValue( pType, out lContract ) )
            {
                return lContract;
            }

            int lCurrentBestResult = int.MaxValue; // Default contract result.
            // Else if not in cache already, look in all contract for management.
            foreach ( IXimlParserContract lCandidate in this.mContractByTypeCache.Values )
            {
                int lResult;
                if ( (lResult = lCandidate.CanManage( pType )) >= 0 && // At least support?
                     lResult < lCurrentBestResult ) // Better contract support than a previous one??
                {
                    lContract = lCandidate;
                }
            }

            if ( lContract != null )
            {
                // Keep it in cache to save futur requests for a same type.
                this.mContractByTypeCache[ pType ] = lContract;
                return lContract;
            }

            return sDefault;
        }

        #endregion Methods
    }
}
