﻿using OrcNet.UI.GraphicObjects;
using OrcNet.UI.GraphicObjects.Collections;
using System;
using System.Globalization;
using System.Reflection;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="GridColumnDefinitionContract"/> class.
    /// </summary>
    public class GridColumnDefinitionContract : AXimlParserContract
    {
        #region Fields

        /// <summary>
        /// Stores the Column definition type.
        /// </summary>
        private static readonly Type sColumnDefinitionType = typeof(ColumnDefinition);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return XimlConstants.cColumnDefinitionTag;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Check whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public override int CanManage(Type pType)
        {
            if ( sColumnDefinitionType == pType ||
                 sColumnDefinitionType.IsAssignableFrom( pType ) )
            {
                return 0;
            }

            return -1;
        }

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The resulting Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            Type lGridType = pParent.Type;
            if ( lGridType != typeof(Grid) )
            {
                return false;
            }

            object lValue = null;
            ColumnDefinitionCollection lColumns = null;
            PropertyInfo lColumnsProperty = lGridType.GetProperty( "ColumnDefinitions" );
            if ( pParent.HasValue( lColumnsProperty, out lValue ) )
            {
                lColumns = lValue as ColumnDefinitionCollection;
            }
            else
            {
                lColumns = new ColumnDefinitionCollection();
                pParent.AddValue( lColumnsProperty, lColumns );
            }

            GridLength lWidth = new GridLength( 1.0 );
            XAttribute lXWidth = pElement.Attribute( XimlConstants.cWidthTag );
            if ( lXWidth != null )
            {
                string lWidthValue = lXWidth.Value;
                if ( lWidthValue.Contains( "Auto" ) )
                {
                    lWidth = new GridLength( 1.0, GridUnitType.Auto );
                }
                else if( lWidthValue.Contains( "*" ) )
                {
                    double lLengthValue;
                    if ( double.TryParse( lWidthValue.Replace( "*", "" ), NumberStyles.Float, CultureInfo.InvariantCulture, out lLengthValue ) == false )
                    {
                        lLengthValue = 1.0;
                    }

                    lWidth = new GridLength( lLengthValue, GridUnitType.Star );
                }
                else
                {
                    double lLengthValue;
                    if ( double.TryParse( lWidthValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lLengthValue ) == false )
                    {
                        lLengthValue = 1.0;
                    }

                    lWidth = new GridLength( lLengthValue );
                }
            }

            double lMinWidth = lWidth.Value;
            XAttribute lXMin = pElement.Attribute( XimlConstants.cMinWidthTag );
            if ( lXMin != null )
            {
                double lMinValue;
                if ( double.TryParse( lXMin.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lMinValue ) )
                {
                    lMinWidth = lMinValue;
                }
            }

            double lMaxWidth = lWidth.Value;
            XAttribute lXMax = pElement.Attribute( XimlConstants.cMaxWidthTag );
            if ( lXMax != null )
            {
                double lMaxValue;
                if ( double.TryParse( lXMax.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lMaxValue ) )
                {
                    lMaxWidth = lMaxValue;
                }
            }

            lColumns.Add( new ColumnDefinition()
            {
                Width    = lWidth,
                MinWidth = lMinWidth,
                MaxWidth = lMaxWidth
            } );

            return base.Read( pElement, pContext, pParent );
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            bool lResult = base.Write( pToWrite, pContext, pElement );


            return true;
        }

        #endregion Methods
    }
}
