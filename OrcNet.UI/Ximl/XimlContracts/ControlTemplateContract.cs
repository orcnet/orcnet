﻿using System;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="ControlTemplateContract"/> class.
    /// </summary>
    public class ControlTemplateContract : AXimlParserContract
    {
        #region Fields

        /// <summary>
        /// Stores the control template type.
        /// </summary>
        private static readonly Type sControlTemplateType = typeof(ControlTemplate);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return XimlConstants.cControlTemplateTag;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Check whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public override int CanManage(Type pType)
        {
            if ( sControlTemplateType == pType ||
                 sControlTemplateType.IsAssignableFrom( pType ) )
            {
                return 0;
            }

            return -1;
        }

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The resulting Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            XAttribute lXTargetType = pElement.Attribute( XimlConstants.cTargetTypeTag );
            if ( lXTargetType == null )
            {
                return false;
            }

            string lTargetTypeValue = lXTargetType.Value;
            Type lTargetType = Type.GetType( lTargetTypeValue );
            pContext.TargetType = lTargetType;

            ControlTemplate lTemplate = new ControlTemplate( lTargetType );

            // Assign it as value if the parent was a setter.
            StyleSetter lCurrent = pContext.Current as StyleSetter;
            if ( lCurrent != null )
            {
                lCurrent.Value = lTemplate;
            }

            // Set teh template as current for children.
            pContext.Current = lTemplate;

            return base.Read( pElement, pContext, pParent );
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            bool lResult = base.Write( pToWrite, pContext, pElement );


            return true;
        }

        #endregion Methods
    }
}
