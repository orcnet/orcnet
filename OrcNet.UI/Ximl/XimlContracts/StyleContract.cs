﻿using System;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="StyleContract"/> class.
    /// </summary>
    public class StyleContract : AXimlParserContract
    {
        #region Fields

        /// <summary>
        /// Stores the style type.
        /// </summary>
        private static readonly Type sStyleType = typeof(Style);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return XimlConstants.cStyleTag;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Check whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public override int CanManage(Type pType)
        {
            if ( sStyleType.IsAssignableFrom( pType ) )
            {
                return 0;
            }

            return -1;
        }

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The parent Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            XAttribute lXTargetType = pElement.Attribute( XimlConstants.cTargetTypeTag );
            if ( lXTargetType == null )
            {
                return false;
            }

            string lTargetTypeValue = lXTargetType.Value;
            Type lTargetType = Type.GetType( lTargetTypeValue );
            pContext.TargetType = lTargetType;

            string lDefaultKey = string.Format( "{0}{1}", lTargetTypeValue, XimlConstants.cDefaultStyle );
            XAttribute lXKey = pElement.Attribute( XimlConstants.cResourceKeyTag );
            if ( lXKey != null )
            {
                lDefaultKey = lXKey.Value;
            }

            Style lNewStyle = new Style( lTargetType, lDefaultKey );
            pContext.Current = lNewStyle;

            // Set it to the global resource cache.
            pContext.Resources.Add( lNewStyle );

            // Add it to the current Ximl node resource as well.
            pParent.Resources.Add( lNewStyle );

            return base.Read( pElement, pContext, pParent );
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            bool lResult = base.Write( pToWrite, pContext, pElement );
             // TO DO: 

            return true;
        }

        #endregion Methods
    }
}
