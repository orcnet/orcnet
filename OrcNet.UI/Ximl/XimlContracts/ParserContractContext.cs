﻿using OrcNet.UI.Ximl.Collections;
using System;
using System.Collections.Generic;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="ParserContractContext"/> class.
    /// </summary>
    public class ParserContractContext
    {
        #region Fields

        /// <summary>
        /// Stores the overall previously created nodes flattened.
        /// </summary>
        private Dictionary<string, XimlNode> mPredecessors;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets a contextual target type.
        /// </summary>
        public Type TargetType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the currently parsed object. 
        /// </summary>
        public object Current
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the resources loaded so far.
        /// </summary>
        public ResourceDictionary Resources
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the overall previously created nodes flattened
        /// </summary>
        public IEnumerable<XimlNode> Predecessors
        {
            get
            {
                return this.mPredecessors.Values;
            }
        }

        /// <summary>
        /// Gets one of the previously created node by Id.
        /// </summary>
        /// <param name="pId">The ID of the node to look for.</param>
        /// <returns>The node if found, Null otherwise.</returns>
        public XimlNode this[NodeId pId]
        {
            get
            {
                if ( string.IsNullOrEmpty( pId ) )
                {
                    return null;
                }

                XimlNode lResult = null;
                if ( this.mPredecessors.TryGetValue( pId, out lResult ) )
                {
                    return lResult;
                }

                return null;
            }
        }
        
        /// <summary>
        /// Gets or sets an untyped extra contextual object.
        /// </summary>
        public object Extra
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ParserContractContext"/> class.
        /// </summary>
        public ParserContractContext()
        {
            this.mPredecessors = new Dictionary<string, XimlNode>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new predessecor.
        /// </summary>
        /// <param name="pPredessecor"></param>
        public void Add(XimlNode pPredessecor)
        {
            if ( pPredessecor == null )
            {
                return;
            }

            this.mPredecessors.Add( pPredessecor.Id, pPredessecor );
        }

        #endregion Methods
    }
}
