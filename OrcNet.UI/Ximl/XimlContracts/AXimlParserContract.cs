﻿using OrcNet.UI.Ximl.Helpers;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the abstract base <see cref="AXimlParserContract"/> class.
    /// </summary>
    public abstract class AXimlParserContract : IXimlParserContract
    {
        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public abstract string Tag
        {
            get;
        }

        /// <summary>
        /// Gets the contract version.
        /// </summary>
        public virtual Version Version
        {
            get
            {
                return new Version( 1, 0 );
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Checkw whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public abstract int CanManage(Type pType);

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The parent Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public virtual bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            object lCurrent  = null;
            XimlNode lParent = null;

            // If template, switch to another parent that will be the root node of the template.
            if ( lCurrent is ControlTemplate )
            {
                ControlTemplate lTemplate = lCurrent as ControlTemplate;
                lTemplate.Template = new TemplateContent();
                lParent  = lTemplate.Template;
            }
            else
            {
                lCurrent = pContext.Current;
                lParent  = pParent;
            }

            foreach ( XElement lXChild in pElement.Elements() )
            {
                // Reset the current object as every child can modify it the for their own children.
                pContext.Current = lCurrent;

                IXimlParserContract lContract = XimlContractManager.Instance.GetContract( lXChild.GetElementType() );
                if ( lContract != null )
                {
                    lContract.Read( lXChild, pContext, lParent );
                }
            }

            return true;
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public virtual bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            List<XElement> lChildren = new List<XElement>();
            foreach ( XimlNode lChild in pToWrite.Children )
            {
                IXimlParserContract lContract = XimlContractManager.Instance.GetContract( lChild.Type );
                if ( lContract != null )
                {
                    XElement lXChild = new XElement( lChild.Type.Name );
                    lChildren.Add( lXChild );
                    lContract.Write( lChild, pContext, lXChild );
                }
            }
            pElement.Add( lChildren );

            return true;
        }

        #endregion Methods
    }
}
