﻿using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="SetterValueContract"/> class.
    /// </summary>
    public class SetterValueContract : AXimlParserContract
    {
        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return string.Format( "{0}.{1}", XimlConstants.cSetterTag, XimlConstants.cValueTag );
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The resulting Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            StyleSetter lSetter = pContext.Current as StyleSetter;
            if ( lSetter == null )
            {
                return false;
            }

            // Process the sub children containing the value.
            bool lResult = base.Read( pElement, pContext, pParent );

            // Set the value to the setter that must be the newly current edited object.
            lSetter.Value = pContext.Current != lSetter ? pContext.Current : null;

            return lResult;
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            bool lResult = base.Write( pToWrite, pContext, pElement );


            return true;
        }

        #endregion Methods
    }
}
