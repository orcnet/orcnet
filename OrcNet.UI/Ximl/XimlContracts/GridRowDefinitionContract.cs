﻿using OrcNet.UI.GraphicObjects;
using OrcNet.UI.GraphicObjects.Collections;
using System;
using System.Globalization;
using System.Reflection;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="GridRowDefinitionContract"/> class.
    /// </summary>
    public class GridRowDefinitionContract : AXimlParserContract
    {
        #region Fields

        /// <summary>
        /// Stores the row definition type.
        /// </summary>
        private static readonly Type sRowDefinitionType = typeof(RowDefinition);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return XimlConstants.cRowDefinitionTag;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Check whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public override int CanManage(Type pType)
        {
            if ( sRowDefinitionType == pType ||
                 sRowDefinitionType.IsAssignableFrom( pType ) )
            {
                return 0;
            }

            return -1;
        }

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The resulting Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            Type lGridType = pParent.Type;
            if ( lGridType != typeof(Grid) )
            {
                return false;
            }

            object lValue = null;
            RowDefinitionCollection lRows = null;
            PropertyInfo lRowsProperty = lGridType.GetProperty( "RowDefinitions" );
            if ( pParent.HasValue( lRowsProperty, out lValue ) )
            {
                lRows = lValue as RowDefinitionCollection;
            }
            else
            {
                lRows = new RowDefinitionCollection();
                pParent.AddValue( lRowsProperty, lRows );
            }

            GridLength lHeight = new GridLength( 1.0 );
            XAttribute lXHeight = pElement.Attribute( XimlConstants.cHeightTag );
            if ( lXHeight != null )
            {
                string lHeightValue = lXHeight.Value;
                if ( lHeightValue.Contains( "Auto" ) )
                {
                    lHeight = new GridLength( 1.0, GridUnitType.Auto );
                }
                else if( lHeightValue.Contains( "*" ) )
                {
                    double lLengthValue;
                    if ( double.TryParse( lHeightValue.Replace( "*", "" ), NumberStyles.Float, CultureInfo.InvariantCulture, out lLengthValue ) == false )
                    {
                        lLengthValue = 1.0;
                    }

                    lHeight = new GridLength( lLengthValue, GridUnitType.Star );
                }
                else
                {
                    double lLengthValue;
                    if ( double.TryParse( lHeightValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lLengthValue ) == false )
                    {
                        lLengthValue = 1.0;
                    }

                    lHeight = new GridLength( lLengthValue );
                }
            }

            double lMinHeight = lHeight.Value;
            XAttribute lXMin = pElement.Attribute( XimlConstants.cMinHeightTag );
            if ( lXMin != null )
            {
                double lMinValue;
                if ( double.TryParse( lXMin.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lMinValue ) )
                {
                    lMinHeight = lMinValue;
                }
            }

            double lMaxHeight = lHeight.Value;
            XAttribute lXMax = pElement.Attribute( XimlConstants.cMaxHeightTag );
            if ( lXMax != null )
            {
                double lMaxValue;
                if ( double.TryParse( lXMax.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out lMaxValue ) )
                {
                    lMaxHeight = lMaxValue;
                }
            }

            lRows.Add( new RowDefinition()
            {
                Height    = lHeight,
                MinHeight = lMinHeight,
                MaxHeight = lMaxHeight
            } );

            return base.Read( pElement, pContext, pParent );
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            bool lResult = base.Write( pToWrite, pContext, pElement );


            return true;
        }

        #endregion Methods
    }
}
