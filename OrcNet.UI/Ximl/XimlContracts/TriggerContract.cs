﻿using OrcNet.UI.Extensions;
using OrcNet.UI.Ximl.Helpers;
using System;
using System.Reflection;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.XimlContracts
{
    /// <summary>
    /// Definition of the <see cref="TriggerContract"/> class.
    /// </summary>
    public class TriggerContract : AXimlParserContract
    {
        #region Fields

        /// <summary>
        /// Stores the base trigger type.
        /// </summary>
        private static readonly Type sTriggerType = typeof(AStyleTrigger);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the contract Tag differenciating the different Ximl contract
        /// </summary>
        public override string Tag
        {
            get
            {
                return XimlConstants.cTriggerTag;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Checkw whether the contract can manage the given type or not.
        /// </summary>
        /// <param name="pType">The type to check for contract support.</param>
        /// <returns>[0, N] if manages the weight value 0 being full support, -1 otherwise.</returns>
        public override int CanManage(Type pType)
        {
            if ( sTriggerType.IsAssignableFrom( pType ) )
            {
                return 0;
            }

            return -1;
        }

        /// <summary>
        /// Reads the Xml element to create the corresponding object.
        /// </summary>
        /// <param name="pElement">The Xml element.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pParent">The parent Ximl node.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Read(XElement pElement, ParserContractContext pContext, XimlNode pParent)
        {
            XAttribute lXPropertyName = pElement.Attribute( XimlConstants.cPropertyTag );
            if ( lXPropertyName == null )
            {
                return false;
            }

            string lPropertyName = lXPropertyName.Value;
            
            string[] lSplitted = lPropertyName.Split( '.' );
            if ( lSplitted.Length > 1 ) // Meaning TargetType + TargetProperty
            {
                pContext.TargetType = Type.GetType( lSplitted[ 0 ] );
                lPropertyName = lSplitted[ 1 ];
            }

            PropertyInfo lProperty = PropertyInfoExtensions.GetProperty( pContext.TargetType, lPropertyName );

            object lValue = null;
            XAttribute lXValue = pElement.Attribute( XimlConstants.cValueTag );
            if ( lXValue != null )
            {
                // Either direct assignment or Static resource.
                string lValueInfo = lXValue.Value;
                lValue = XimlHelpers.GetValue( lValueInfo, lProperty.PropertyType, pContext.Resources );
            }

            StyleTrigger lNewTrigger = new StyleTrigger( lProperty, lValue );

            ITriggerContainer lTriggerContainer = pContext.Current as ITriggerContainer;
            if ( lTriggerContainer != null )
            {
                lTriggerContainer.AddTrigger( lNewTrigger );
            }

            // Set the trigger as currently edited object so children set the value in it then.
            pContext.Current = lNewTrigger;

            return base.Read( pElement, pContext, pParent );
        }

        /// <summary>
        /// Writes the given object into a Xml element.
        /// </summary>
        /// <param name="pToWrite">The Ximl node to write.</param>
        /// <param name="pContext">The parser context</param>
        /// <param name="pElement">The resulting Xml element.</param>
        /// <returns>True if successful, false otherwise.</returns>
        public override bool Write(XimlNode pToWrite, ParserContractContext pContext, XElement pElement)
        {
            bool lResult = base.Write( pToWrite, pContext, pElement );


            return true;
        }

        #endregion Methods
    }
}
