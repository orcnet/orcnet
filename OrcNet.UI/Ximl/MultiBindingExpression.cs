﻿using OrcNet.UI.Ximl.Collections;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="MultiBindingExpression"/> class.
    /// </summary>
    public class MultiBindingExpression : IMultiBinding
    {
        #region Fields

        /// <summary>
        /// Stores the source property.
        /// </summary>
        private PropertyInfo mSourceProperty;

        /// <summary>
        /// Stores the targets properties.
        /// </summary>
        private PropertyInfo[] mTargetProperties;

        /// <summary>
        /// Stores the source ref of the binding.
        /// </summary>
        private INotifyPropertyChanged mSource;

        /// <summary>
        /// Stores the target refs of the binding.
        /// </summary>
        private INotifyPropertyChanged[] mTargets;

        /// <summary>
        /// Stores the binding status.
        /// </summary>
        private BindingStatus mStatus;

        /// <summary>
        /// Stores the flag indicating whether the bidning must do nothing on value changes or not.
        /// </summary>
        private bool mDoNothing;

        /// <summary>
        /// Stores the set of bindings.
        /// </summary>
        private BindingCollection mBindings;

        /// <summary>
        /// Stores the optional converter to use for the binding.
        /// </summary>
        private IMultiValueConverter mConverter;

        /// <summary>
        /// Stores the converter parameter value if needed for conversion.
        /// </summary>
        private object mParameter;

        /// <summary>
        /// Stores the culture info to use by the converter if needed.
        /// </summary>
        private CultureInfo mCultureInfo;

        /// <summary>
        /// Stores the binding mode.
        /// </summary>
        private BindingMode mMode;

        /// <summary>
        /// Stores the format in the case the value is displayed as a string.
        /// </summary>
        private string mFormat;

        /// <summary>
        /// Stores the value to set to the target in the case the source is Null.
        /// </summary>
        private object mTargetNullValue;

        /// <summary>
        /// Stores the value to use if the binding fails.
        /// </summary>
        private object mFallbackValue;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the binding status.
        /// </summary>
        public BindingStatus Status
        {
            get
            {
                return this.mStatus;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the bidning must do nothing on value changes or not.
        /// </summary>
        public bool DoNothing
        {
            get
            {
                return this.mDoNothing;
            }
            set
            {
                this.mDoNothing = value;
            }
        }

        /// <summary>
        /// Gets the set of bindings.
        /// </summary>
        public BindingCollection Bindings
        {
            get
            {
                return this.mBindings;
            }
        }
        
        /// <summary>
        /// Gets or sets the flag indicating whether the binding is for a template definition or not.
        /// </summary>
        public bool IsTemplate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the optional converter to use for the binding.
        /// </summary>
        public IMultiValueConverter Converter
        {
            get
            {
                return this.mConverter;
            }
            set
            {
                this.mConverter = value;
            }
        }

        /// <summary>
        /// Gets or sets the converter parameter value if needed for conversion.
        /// </summary>
        public object Parameter
        {
            get
            {
                return this.mParameter;
            }
            set
            {
                this.mParameter = value;
            }
        }

        /// <summary>
        /// Gets or sets the culture info to use by the converter if needed.
        /// </summary>
        public CultureInfo CultureInfo
        {
            get
            {
                return this.mCultureInfo;
            }
            set
            {
                this.mCultureInfo = value;
            }
        }

        /// <summary>
        /// Gets the binding mode.
        /// </summary>
        public BindingMode Mode
        {
            get
            {
                return this.mMode;
            }
        }

        /// <summary>
        /// Gets or sets the format in the case the value is displayed as a string.
        /// </summary>
        public string Format
        {
            get
            {
                return this.mFormat;
            }
            set
            {
                this.mFormat = value;
            }
        }

        /// <summary>
        /// Gets or sets the value to set to the target in the case the source is Null.
        /// </summary>
        public object TargetNullValue
        {
            get
            {
                return this.mTargetNullValue;
            }
            set
            {
                this.mTargetNullValue = value;
            }
        }

        /// <summary>
        /// Gets or sets the value to use if the binding fails.
        /// </summary>
        public object FallbackValue
        {
            get
            {
                return this.mFallbackValue;
            }
            set
            {
                this.mFallbackValue = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiBindingExpression"/> class.
        /// </summary>
        /// <param name="pMode">The binding mode. (Targets to source by default)</param>
        public MultiBindingExpression(BindingMode pMode = BindingMode.OneWayToSource)
        {
            this.mMode = pMode;
            this.mBindings = new BindingCollection();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Bind the To-bind-instances regarding to the binding information.
        /// </summary>
        /// <param name="pSource">The source instance.</param>
        /// <param name="pTargets">The target instances involved in the multi binding.</param>
        /// <returns>True if successfully bound, false otherwise.</returns>
        public bool Bind(IBindable pSource, IBindable[] pTargets)
        {
            // Cannot have dirty parameters.
            if ( pSource == null ||
                 pTargets == null )
            {
                this.mStatus = BindingStatus.Inactive;
                return false;
            }

            // Cannot be empty.
            if ( this.mBindings == null ||
                 this.mBindings.Count == 0 ||
                 this.mBindings.Count != pTargets.Length )
            {
                this.mStatus = BindingStatus.Inactive;
                return false;
            }

            // Cannot deal with multi values without a converter.
            if ( this.mConverter == null )
            {
                this.mStatus = BindingStatus.PathError;
                return false;
            }

            this.mTargets = new INotifyPropertyChanged[ this.mBindings.Count ];
            this.mTargetProperties = new PropertyInfo[ this.mBindings.Count ];

            int lCurrent = 0;
            ISingleBinding lFirst = null;
            // Foreach bindings, Bind them with the supplied instances.
            foreach ( ISingleBinding lBinding in this.mBindings )
            {
                // Get the first for later.
                if ( lFirst == null )
                {
                    lFirst = lBinding;
                }

                // Assumes Targets and Bindings are in the right order.
                if ( lBinding.Bind( pSource, pTargets[ lCurrent ] ) == false )
                {
                    // Stop all as there is an issue with bindings.
                    this.mStatus = lBinding.Status;
                    return false;
                }

                this.mTargets[ lCurrent ] = lBinding.Target;
                this.mTargetProperties[ lCurrent ] = lBinding.TargetProperty;

                // Prevent each binding to process there own job on data changes.
                lBinding.DoNothing = true;
                lBinding.TargetChanging += this.OnTargetChanging;

                lCurrent++;
            }

            // Listen just one of the bindings source as it should be the same for all target in case of multibindings.
            this.mSource = lFirst.Source;
            this.mSourceProperty = lFirst.SourceProperty;
            lFirst.SourceChanging += this.OnSourceChanging;

            this.mStatus = BindingStatus.Binded;
            return true;
        }

        /// <summary>
        /// Delegate called on source changes.
        /// </summary>
        /// <param name="pSender">The binding expression that triggered the changes.</param>
        /// <param name="pValue">The current value.</param>
        private void OnSourceChanging(object pSender, object pValue)
        {
            if ( this.mDoNothing )
            {
                return;
            }

            int lTargetCount = this.mTargetProperties.Length;

            try
            {
                // If the source changes, it must be set to targets.
                object lSourceValue = this.mSourceProperty.GetValue( this.mSource );
                if ( lSourceValue == null )
                {
                    // Set Null to all targets.
                    for ( int lCurr = 0; lCurr < lTargetCount; lCurr++ )
                    {
                        this.mTargetProperties[ lCurr ].SetValue( this.mTargets, null );
                    }
                    return;
                }

                // Else convert it.
                Type[] lTypes = new Type[ lTargetCount ];
                for ( int lCurr = 0; lCurr < lTargetCount; lCurr++ )
                {
                    lTypes[ lCurr ] = this.mTargetProperties[ lCurr ].PropertyType;
                }

                object[] lResults = this.mConverter.ConvertBack( lSourceValue, lTypes, this.mParameter, this.mCultureInfo );
                if ( lResults == null ||
                     lResults.Length != lTargetCount )
                {
                    return;
                }

                for ( int lCurr = 0; lCurr < lTargetCount; lCurr++ )
                {
                    this.mTargetProperties[ lCurr ].SetValue( this.mTargets[ lCurr ], lResults[ lCurr ] );
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Delegate called on one of the target changes.
        /// </summary>
        /// <param name="pSender">The binding expression that triggered the changes.</param>
        /// <param name="pValue">The current value.</param>
        private void OnTargetChanging(object pSender, object pValue)
        {
            if ( this.mDoNothing )
            {
                return;
            }

            int lTargetCount = this.mTargetProperties.Length;

            try
            {
                // If one of the target changes, get all target values and convert to the source type.
                object[] lValues = new object[ lTargetCount ];
                for ( int lCurr = 0; lCurr < lTargetCount; lCurr++ )
                {
                    lValues[ lCurr ] = this.mTargetProperties[ lCurr ].GetValue( this.mTargets[ lCurr ] );
                }

                object lResult = this.mConverter.Convert( lValues, this.mSourceProperty.PropertyType, this.mParameter, this.mCultureInfo );
                this.mSourceProperty.SetValue( this.mSource, lResult );
            }
            catch
            {

            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.mSource = null;
            this.mTargets = null;
            this.mSourceProperty = null;
            this.mTargetProperties = null;
            this.mFallbackValue = null;
            this.mTargetNullValue = null;
            this.mCultureInfo = null;
            this.mParameter = null;
            this.mConverter = null;
            this.mBindings.Dispose();
        }

        #endregion Methods
    }
}
