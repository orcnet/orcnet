﻿using System.Collections.Generic;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="ITriggerContainer"/> interface.
    /// </summary>
    public interface ITriggerContainer
    {
        #region Properties

        /// <summary>
        /// Gets the set of triggers.
        /// </summary>
        IEnumerable<StyleTrigger> Triggers
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a trigger.
        /// </summary>
        /// <param name="pTrigger">The trigger to add.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        bool AddTrigger(StyleTrigger pTrigger);

        /// <summary>
        /// Adds a trigger.
        /// </summary>
        /// <param name="pProperty">The property the trigger is for.</param>
        /// <param name="pValue">The property value to set.</param>
        bool AddTrigger(PropertyInfo pProperty, object pValue);

        /// <summary>
        /// Removes a trigger
        /// </summary>
        /// <param name="pTrigger">The trigger to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemoveTrigger(StyleTrigger pTrigger);

        /// <summary>
        /// Removes a trigger
        /// </summary>
        /// <param name="pPropertyName">The property the trigger is for.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemoveTrigger(string pPropertyName);

        #endregion Methods
    }
}
