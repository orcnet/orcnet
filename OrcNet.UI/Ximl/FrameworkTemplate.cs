﻿using OrcNet.UI.Ximl.Collections;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the abstract base <see cref="FrameworkTemplate"/> class.
    /// </summary>
    public abstract class FrameworkTemplate
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the template has any Ximl node or not.
        /// </summary>
        private bool mHasXimlNodeContent;

        /// <summary>
        /// Stores the template content Ximl description.
        /// </summary>
        private TemplateContent mTemplateContent;

        /// <summary>
        /// Stores the resources associated with this specific template.
        /// </summary>
        private ResourceDictionary mResources;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the template has any content.
        /// </summary>
        public bool HasContent
        {
            get
            {
                return this.mHasXimlNodeContent;
            }
        }

        /// <summary>
        /// Gets or sets the template content Ximl description.
        /// </summary>
        public TemplateContent Template
        {
            get
            {
                return this.mTemplateContent;
            }
            set
            {
                if ( this.mHasXimlNodeContent == false )
                {
                    value.Owner = this;
                    value.ParseXiml();
                    this.mTemplateContent = value;
                    this.mHasXimlNodeContent = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the resources associated with this specific template.
        /// </summary>
        public ResourceDictionary Resources
        {
            get
            {
                if ( this.mResources == null )
                {
                    this.mResources = new ResourceDictionary();
                }
                
                return this.mResources;
            }
            set
            {
                this.mResources = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameworkTemplate"/> class.
        /// </summary>
        protected FrameworkTemplate()
        {
            this.mHasXimlNodeContent = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Loads the content of the template as an new instance of an UI object.
        /// </summary>
        /// <returns>The newly created instance.</returns>
        public object LoadContent()
        {
            return null;
        }

        /// <summary>
        /// Subclasses must override this method if they need to
        /// impose additional rules for the TemplatedParent
        /// </summary>
        /// <param name="pTemplatedParent">The templated parent.</param>
        protected virtual void ValidateTemplatedParent(FrameworkElement pTemplatedParent)
        {
            // Nothing to do.
        }

        /// <summary>
        /// Applies the template content on a visual tree.
        /// </summary>
        /// <param name="pContainer">The content container.</param>
        /// <param name="pContext">The content apply context.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        internal bool ApplyTemplateContent(FrameworkElement pContainer, object pContext)
        {
            this.ValidateTemplatedParent( pContainer );

            bool lResult = this.ApplyTemplateContentCore( pContainer, pContext );

            return lResult;
        }

        /// <summary>
        /// Inner apply template content method (overridable)
        /// </summary>
        /// <param name="pContainer">The content container.</param>
        /// <param name="pContext">The content apply context.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        protected virtual bool ApplyTemplateContentCore(FrameworkElement pContainer, object pContext)
        {
            return true;
        }

        /// <summary>
        /// Find the requested resource corresponding to the given key.
        /// </summary>
        /// <param name="pKey">The key to look for.</param>
        /// <returns>The resource corresponding to the key, Null otherwise.</returns>
        internal object FindResource(string pKey)
        {
            if ( this.mResources != null &&
                 this.mResources.ContainsKey( pKey ) )
            {
                return this.mResources[ pKey ];
            }

            return null;
        }

        #endregion Methods
    }
}
