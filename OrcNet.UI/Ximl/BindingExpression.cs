﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Threading;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="BindingExpression"/> class.
    /// </summary>
    public class BindingExpression : ISingleBinding
    {
        #region Fields

        /// <summary>
        /// Stores the binding status.
        /// </summary>
        private BindingStatus mStatus;

        /// <summary>
        /// Stores the flag indicating whether the bidning must do nothing on value changes or not.
        /// </summary>
        private bool mDoNothing;

        /// <summary>
        /// Stores the optional converter to use for the binding.
        /// </summary>
        private IValueConverter mConverter;

        /// <summary>
        /// Stores the converter parameter value if needed for conversion.
        /// </summary>
        private object mParameter;

        /// <summary>
        /// Stores the culture info to use by the converter if needed.
        /// </summary>
        private CultureInfo mCultureInfo;

        /// <summary>
        /// Stores the ref to the source instance.
        /// </summary>
        private INotifyPropertyChanged mSource;

        /// <summary>
        /// Stores the ref to the target instance.
        /// </summary>
        private INotifyPropertyChanged mTarget;

        /// <summary>
        /// Stores the source property info.
        /// </summary>
        private PropertyInfo mSourceProperty;

        /// <summary>
        /// Stores the target property info.
        /// </summary>
        private PropertyInfo mTargetProperty;

        /// <summary>
        /// Stores the flag indicating whether the binding is disposed or not
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the binding mode.
        /// </summary>
        private BindingMode mMode;

        /// <summary>
        /// Stores the source bound property name.
        /// </summary>
        private string mSourcePropertyName;

        /// <summary>
        /// Stores the target bound property name.
        /// </summary>
        private string mTargetPropertyName;

        /// <summary>
        /// Stores the target element name
        /// </summary>
        private string mTargetElementName;

        /// <summary>
        /// Stores the target element path to the bound property.
        /// </summary>
        private string mTargetPathToProperty;

        /// <summary>
        /// Stores the format in the case the value is displayed as a string.
        /// </summary>
        private string mFormat;

        /// <summary>
        /// Stores the value to set to the target in the case the source is Null.
        /// </summary>
        private object mTargetNullValue;

        /// <summary>
        /// Stores the value to use if the binding fails.
        /// </summary>
        private object mFallbackValue;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event triggered before the source value be updated, therefore, has changed.
        /// </summary>
        public event BindingChangesDelegate SourceChanging;

        /// <summary>
        /// Event triggered after the source value be updated.
        /// </summary>
        public event BindingChangesDelegate SourceChanged;

        /// <summary>
        /// Event triggered before the target value be updated, therefore, has changed.
        /// </summary>
        public event BindingChangesDelegate TargetChanging;

        /// <summary>
        /// Event triggered after the target value be updated.
        /// </summary>
        public event BindingChangesDelegate TargetChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the binding status.
        /// </summary>
        public BindingStatus Status
        {
            get
            {
                return this.mStatus;
            }
        }

        /// <summary>
        /// Gets the source property.
        /// </summary>
        public PropertyInfo SourceProperty
        {
            get
            {
                return this.mSourceProperty;
            }
        }

        /// <summary>
        /// Gets the target property.
        /// </summary>
        public PropertyInfo TargetProperty
        {
            get
            {
                return this.mTargetProperty;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the bidning must do nothing on value changes or not.
        /// </summary>
        public bool DoNothing
        {
            get
            {
                return this.mDoNothing;
            }
            set
            {
                this.mDoNothing = value;
            }
        }

        /// <summary>
        /// Gets or sets the optional converter to use for the binding.
        /// </summary>
        public IValueConverter Converter
        {
            get
            {
                return this.mConverter;
            }
            set
            {
                this.mConverter = value;
            }
        }

        /// <summary>
        /// Gets or sets the converter parameter value if needed for conversion.
        /// </summary>
        public object Parameter
        {
            get
            {
                return this.mParameter;
            }
            set
            {
                this.mParameter = value;
            }
        }

        /// <summary>
        /// Gets or sets the culture info to use by the converter if needed.
        /// </summary>
        public CultureInfo CultureInfo
        {
            get
            {
                return this.mCultureInfo;
            }
            set
            {
                this.mCultureInfo = value;
            }
        }

        /// <summary>
        /// Gets or sets the format in the case the value is displayed as a string.
        /// </summary>
        public string Format
        {
            get
            {
                return this.mFormat;
            }
            set
            {
                this.mFormat = value;
            }
        }

        /// <summary>
        /// Gets or sets the value to set to the target in the case the source is Null.
        /// </summary>
        public object TargetNullValue
        {
            get
            {
                return this.mTargetNullValue;
            }
            set
            {
                this.mTargetNullValue = value;
            }
        }

        /// <summary>
        /// Gets or sets the value to use if the binding fails.
        /// </summary>
        public object FallbackValue
        {
            get
            {
                return this.mFallbackValue;
            }
            set
            {
                this.mFallbackValue = value;
            }
        }

        /// <summary>
        /// Gets the bound member name.
        /// </summary>
        string ISingleBinding.MemberName
        {
            get
            {
                return this.SourcePropertyName;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the binding is for a template definition or not.
        /// </summary>
        public virtual bool IsTemplate
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the binding mode.
        /// </summary>
        public BindingMode Mode
        {
            get
            {
                return this.mMode;
            }
        }

        /// <summary>
        /// Gets the source bound property name.
        /// </summary>
        public string SourcePropertyName
        {
            get
            {
                return this.mSourcePropertyName;
            }
        }

        /// <summary>
        /// Gets the target bound property name.
        /// </summary>
        public string TargetPropertyName
        {
            get
            {
                return this.mTargetPropertyName;
            }
        }

        /// <summary>
        /// Gets the target element name
        /// </summary>
        public string TargetElementName
        {
            get
            {
                return this.mTargetElementName;
            }
        }

        /// <summary>
        /// Gets the target element path to the bound property.
        /// </summary>
        public string TargetPathToProperty
        {
            get
            {
                return this.mTargetPathToProperty;
            }
        }

        /// <summary>
        /// Gets the ref to the source instance.
        /// </summary>
        public INotifyPropertyChanged Source
        {
            get
            {
                return this.mSource;
            }
        }

        /// <summary>
        /// Gets the ref to the target instance.
        /// </summary>
        public INotifyPropertyChanged Target
        {
            get
            {
                return this.mTarget;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingExpression"/> class.
        /// </summary>
        /// <param name="pSourcePropertyName">The source property name.</param>
        /// <param name="pTargetPropertyName">The target property name.</param>
        /// <param name="pMode">The binding mode.</param>
        /// <param name="pTargetElementName">The target element name to bound to (different from the current DataContext)</param>
        /// <param name="pTargetPathToProperty">The target element path to property to bound.</param>
        public BindingExpression(string pSourcePropertyName, string pTargetPropertyName, BindingMode pMode = BindingMode.OneWay, string pTargetElementName = null, string pTargetPathToProperty = null)
        {
            this.mStatus = BindingStatus.Inactive;
            this.mIsDisposed = false;
            this.mSourcePropertyName = pSourcePropertyName;
            this.mTargetPropertyName = pTargetPropertyName;
            this.mMode = pMode;
            this.mTargetElementName    = pTargetElementName;
            this.mTargetPathToProperty = pTargetPathToProperty;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Bind the To-bind-instances regarding to the binding information.
        /// </summary>
        /// <param name="pSource">The source instance.</param>
        /// <param name="pTarget">The target instance.</param>
        /// <returns>True if successfully bound, false otherwise.</returns>
        public virtual bool Bind(IBindable pSource, IBindable pTarget)
        {
            if ( pSource == null || 
                 pTarget == null )
            {
                this.mStatus = BindingStatus.Inactive;
                return false;
            }

            this.mSource = pSource.Instance;
            if ( pSource.Instance.DataContext != null )
            {
                this.mSource = pSource.Instance.DataContext;
            }

            this.mTarget = pTarget.Instance;
            if ( pTarget.Instance.DataContext != null )
            {
                this.mTarget = pTarget.Instance.DataContext;
            }

            // Get the bound property info.
            Type lSourceType = this.mSource.GetType();
            this.mSourceProperty = lSourceType.GetProperty( this.mSourcePropertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public );
            
            if ( this.mSourceProperty == null )
            {
                this.mStatus = BindingStatus.SourceError;
                return false;
            }

            Type lTargetType = this.mTarget.GetType();
            if ( string.IsNullOrEmpty( this.mTargetPathToProperty ) )
            {
                this.mTargetProperty = lTargetType.GetProperty( this.mTargetPropertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public );
            }
            else
            {
                string[] lSplitted = this.mTargetPathToProperty.Split( '.' );
                object lTargetInstance = this.mTarget;
                try
                {
                    int lCount = lSplitted.Length;
                    
                    // Get the instance up to the property that must be bound.
                    for ( int lCurr = 0; lCurr < lCount - 1; lCurr++ )
                    {
                        lTargetInstance = lTargetType.GetProperty( lSplitted[ lCurr ], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public ).GetValue( lTargetInstance );
                        lTargetType = lTargetInstance.GetType();
                    }
                }
                catch
                {
                    this.mStatus = BindingStatus.PathError;
                    return false;
                }

                // Refresh the target as it is the given one no more.
                this.mTarget = lTargetInstance as INotifyPropertyChanged;
                if ( this.mTarget != null )
                {
                    this.mTargetProperty = lTargetType.GetProperty( lSplitted[ lSplitted.Length - 1 ], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public );
                }
            }

            if ( this.mTargetProperty == null )
            {
                this.mStatus = BindingStatus.TargetError;
                return false;
            }
            
            switch ( this.mMode )
            {
                case BindingMode.OneWay:
                    {
                        this.mSource.PropertyChanged += this.OnSourcePropertyChanged;
                    }
                    break;
                case BindingMode.TwoWay:
                    {
                        this.mSource.PropertyChanged += this.OnSourcePropertyChanged;
                        this.mTarget.PropertyChanged += this.OnTargetPropertyChanged;
                    }
                    break;
                case BindingMode.OneWayToSource:
                    {
                        this.mTarget.PropertyChanged += this.OnTargetPropertyChanged;
                    }
                    break;
            }

            this.mStatus = BindingStatus.Binded;
            return true;
        }

        /// <summary>
        /// Delegate called on source property changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSourcePropertyChanged(object pSender, PropertyChangedEventArgs pEventArgs)
        {
            if ( pEventArgs.PropertyName == this.mSourceProperty.Name )
            {
                try
                {
                    object lSourceValue = this.mSourceProperty.GetValue( this.mSource );

                    if ( this.SourceChanging != null )
                    {
                        this.SourceChanging( this, lSourceValue );
                    }

                    // If requested to do nothing breaks after it at least notified the source was changing.
                    if ( this.mDoNothing )
                    {
                        return;
                    }

                    if ( lSourceValue == null )
                    {
                        lSourceValue = this.mTargetNullValue;
                    }
                    else if ( this.mConverter != null ) // Don t convert if assigning the target Null value as it must be of the proper type.
                    {
                        lSourceValue = this.mConverter.Convert( lSourceValue, this.mTargetProperty.PropertyType, this.mParameter, this.mCultureInfo == null ? Thread.CurrentThread.CurrentCulture : this.mCultureInfo );
                    }

                    this.mTargetProperty.SetValue( this.mTarget, lSourceValue );

                    if ( this.SourceChanged != null )
                    {
                        this.SourceChanged( this, lSourceValue );
                    }
                }
                catch
                {

                }
            }
        }

        /// <summary>
        /// Delegate called on target property changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnTargetPropertyChanged(object pSender, PropertyChangedEventArgs pEventArgs)
        {
            if ( pEventArgs.PropertyName == this.mTargetProperty.Name )
            {
                try
                {
                    object lSourceValue = this.mTargetProperty.GetValue( this.mTarget );

                    if ( this.TargetChanging != null )
                    {
                        this.TargetChanging( this, lSourceValue );
                    }

                    // If requested to do nothing breaks after it at least notified the source was changing.
                    if ( this.mDoNothing )
                    {
                        return;
                    }

                    if ( lSourceValue == null )
                    {
                        lSourceValue = this.mTargetNullValue;
                    }
                    else if ( this.mConverter != null ) // Don t convert if assigning the target Null value as it must be of the proper type.
                    {
                        lSourceValue = this.mConverter.ConvertBack( lSourceValue, this.mSourceProperty.PropertyType, this.mParameter, this.mCultureInfo == null ? Thread.CurrentThread.CurrentCulture : this.mCultureInfo );
                    }

                    this.mSourceProperty.SetValue( this.mSource, lSourceValue );

                    if ( this.TargetChanged != null )
                    {
                        this.TargetChanged( this, lSourceValue );
                    }
                }
                catch
                {

                }
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            this.mTargetNullValue = null;
            this.mFallbackValue = null;
            this.mConverter = null;
            this.mCultureInfo = null;
            this.mParameter = null;

            if ( this.mSource != null )
            {
                this.mSourceProperty = null;
                this.mSource.PropertyChanged -= this.OnSourcePropertyChanged;
                this.mSource = null;
            }

            if ( this.mTarget != null )
            {
                this.mTargetProperty = null;
                this.mTarget.PropertyChanged -= this.OnTargetPropertyChanged;
                this.mTarget = null;
            }
        }

        #endregion Methods
    }
}
