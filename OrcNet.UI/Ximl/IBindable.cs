﻿using OrcNet.UI.Core;
using System.ComponentModel;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IBindable"/> interface.
    /// </summary>
    public interface IBindable : IIdentifiable, INotifyPropertyChanged
    {
        #region Properties

        /// <summary>
        /// Gets the bindable instance.
        /// </summary>
        IUIResource Instance
        {
            get;
            set;
        }

        #endregion Properties
    }
}
