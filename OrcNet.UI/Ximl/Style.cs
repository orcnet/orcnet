﻿using OrcNet.UI.Ximl.Collections;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="Style"/> class.
    /// </summary>
    public class Style : ITriggerContainer, ISetterContainer, IIdentifiable, IXimlResource, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the default style target type.
        /// </summary>
        internal readonly Type DefaultTargetType = typeof(IFrameworkInputElement);

        /// <summary>
        /// Stores the flag indicating whether the style can be changed or not.
        /// </summary>
        private bool mIsReadOnly;

        /// <summary>
        /// Stores the style this one is based on.
        /// </summary>
        private Style mBasedOnStyle;

        /// <summary>
        /// Stores the style's target type.
        /// </summary>
        private Type mTargetType;

        /// <summary>
        /// Stores the style's key id.
        /// </summary>
        private string mId;

        /// <summary>
        /// Stores the set of resources.
        /// </summary>
        private ResourceDictionary mResources;

        /// <summary>
        /// Stores the set of setters.
        /// </summary>
        private StyleSetterCollection mSetters;

        /// <summary>
        /// Stores the set of triggers.
        /// </summary>
        private StyleTriggerCollection mTriggers;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the style can be changed or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.mIsReadOnly;
            }
            internal set
            {
                this.mIsReadOnly = value;
            }
        }

        /// <summary>
        /// Gets the style this one is based on.
        /// </summary>
        public Style BasedOn
        {
            get
            {
                return this.mBasedOnStyle;
            }
            set
            {
                this.mBasedOnStyle = value;
            }
        }

        /// <summary>
        /// Gets the style's target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return this.mTargetType;
            }
        }

        /// <summary>
        /// Gets the style's key id.
        /// </summary>
        public string Id
        {
            get
            {
                return this.mId;
            }
        }

        /// <summary>
        /// Gets the set of style resources (scoped to this style).
        /// </summary>
        public ResourceDictionary Resources
        {
            get
            {
                return this.mResources;
            }
        }

        /// <summary>
        /// Gets the set of setters.
        /// </summary>
        public IEnumerable<StyleSetter> Setters
        {
            get
            {
                return this.mSetters;
            }
        }

        /// <summary>
        /// Gets the set of triggers.
        /// </summary>
        public IEnumerable<StyleTrigger> Triggers
        {
            get
            {
                return this.mTriggers;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Style"/> class.
        /// </summary>
        /// <param name="pType">The type the style is for.</param>
        /// <param name="pKey">The specific key that will restrict the style to those specifying it then.</param>
        public Style(Type pType, string pKey)
        {
            this.mId = pKey;
            this.mTargetType = pType;
            this.mSetters = new StyleSetterCollection();
            this.mTriggers = new StyleTriggerCollection();
            this.mResources = new ResourceDictionary();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Prepares the style.
        /// </summary>
        public void Prepare()
        {
            // TO DO.
        }

        /// <summary>
        /// Checks the if the style target type matches the given element.
        /// </summary>
        /// <param name="pElement"></param>
        internal void CheckTargetType(object pElement)
        {
            // In the most common case TargetType is Default
            // and we can avoid a call to IsAssignableFrom() who's performance is unknown.
            if ( this.DefaultTargetType == this.TargetType )
            {
                return;
            }

            Type lElementType = pElement.GetType();
            if ( TargetType.IsAssignableFrom( lElementType )  == false )
            {
                throw new InvalidOperationException(string.Format( "Cannot use the {0} style for the {1} element type.",
                                                    this.TargetType.Name,
                                                    lElementType.Name));
            }
        }

        /// <summary>
        /// Finds the resource corresponding to the given key in the style's resources.
        /// </summary>
        /// <param name="pResourceKey"></param>
        /// <returns>The resource, null otherwise.</returns>
        internal object FindResource(string pResourceKey)
        {
            IXimlResource lResource = null;
            if ( this.mResources != null && 
                 this.mResources.TryGetValue( pResourceKey, out lResource ) )
            {
                return lResource;
            }

            if ( this.mBasedOnStyle != null )
            {
                return this.mBasedOnStyle.FindResource( pResourceKey );
            }

            return null;
        }

        /// <summary>
        /// Finds the resource dictionary containing the given resource by key.
        /// </summary>
        /// <param name="pResourceKey"></param>
        /// <returns>The dictionary, null otherwise.</returns>
        internal ResourceDictionary FindResourceDictionary(string pResourceKey)
        {
            Debug.Assert( string.IsNullOrEmpty( pResourceKey ) == false, "Argument cannot be null");
            if ( this.mResources != null && 
                 this.mResources.ContainsKey( pResourceKey ) )
            {
                return this.mResources;
            }

            if ( this.mBasedOnStyle != null )
            {
                return this.mBasedOnStyle.FindResourceDictionary( pResourceKey );
            }

            return null;
        }

        /// <summary>
        /// Adds a setter.
        /// </summary>
        /// <param name="pSetter">The setter to add.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool AddSetter(StyleSetter pSetter)
        {
            if ( pSetter == null ||
                 pSetter.Property == null )
            {
                return false;
            }

            // Overwrite if already in.
            this.mSetters[ pSetter.Id ] = pSetter;
            return true;
        }

        /// <summary>
        /// Adds a setter.
        /// </summary>
        /// <param name="pProperty">The property the setter is for.</param>
        /// <param name="pValue">The property value to set.</param>
        /// <param name="pTargetName">The target name if any.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool AddSetter(PropertyInfo pProperty, object pValue, string pTargetName = null)
        {
            return this.AddSetter( new StyleSetter( pProperty, pValue, pTargetName ) );
        }

        /// <summary>
        /// Removes a setter
        /// </summary>
        /// <param name="pSetter">The setter to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveSetter(StyleSetter pSetter)
        {
            return this.RemoveSetter( pSetter.Id );
        }

        /// <summary>
        /// Removes a setter
        /// </summary>
        /// <param name="pPropertyName">The property the setter is for.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveSetter(string pPropertyName)
        {
            return this.mSetters.Remove( pPropertyName );
        }

        /// <summary>
        /// Adds a trigger.
        /// </summary>
        /// <param name="pTrigger">The trigger to add.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool AddTrigger(StyleTrigger pTrigger)
        {
            if ( pTrigger == null ||
                 pTrigger.Property == null )
            {
                return false;
            }

            // Overwrite if already in.
            this.mTriggers[ pTrigger.Id ] = pTrigger;
            return true;
        }

        /// <summary>
        /// Adds a trigger.
        /// </summary>
        /// <param name="pProperty">The property the trigger is for.</param>
        /// <param name="pValue">The property value to set.</param>
        public bool AddTrigger(PropertyInfo pProperty, object pValue)
        {
            return this.AddTrigger( new StyleTrigger( pProperty, pValue ) );
        }

        /// <summary>
        /// Removes a trigger
        /// </summary>
        /// <param name="pTrigger">The trigger to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveTrigger(StyleTrigger pTrigger)
        {
            return this.RemoveTrigger( pTrigger.Id );
        }

        /// <summary>
        /// Removes a trigger
        /// </summary>
        /// <param name="pPropertyName">The property the trigger is for.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveTrigger(string pPropertyName)
        {
            return this.mTriggers.Remove( pPropertyName );
        }
        
        #region Methods IIdentifiable

        /// <summary>
        /// Attempt to find the identifiable corresponding to the given Id.
        /// </summary>
        /// <param name="pId">The Id to look for.</param>
        /// <returns>The found identifiable, null otherwise.</returns>
        public IIdentifiable Find(string pId)
        {
            // In the case one day, styles contain styles.
            return this;
        }

        #endregion Methods IIdentifiable

        #region Methods ICloneable

        /// <summary>
        /// Clone the style.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            Style lClone = new Style( this.mTargetType, this.mId );
            lClone.mIsReadOnly   = this.mIsReadOnly;
            lClone.mBasedOnStyle = this.mBasedOnStyle;
            foreach ( StyleSetter lSetter in this.mSetters )
            {
                lClone.mSetters.Add( lSetter.Clone() as StyleSetter );
            }
            foreach ( StyleTrigger lTrigger in this.mTriggers )
            {
                lClone.mTriggers.Add( lTrigger.Clone() as StyleTrigger );
            }
            //foreach ( KeyValuePair<string, IXimlResource> lResource in this.mResources )
            //{
            //    lClone.mResources.Add( lResource.Key, lResource.Value.Clone() as IXimlResource );
            //}
            lClone.mResources = this.mResources;

            return lClone;
        }

        #endregion Methods ICloneable

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.mSetters.Clear();
        }
        
        #endregion Methods IDisposable

        #endregion Methods
    }
}
