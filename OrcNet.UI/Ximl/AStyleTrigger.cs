﻿using System;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="AStyleTrigger"/> class.
    /// </summary>
    public abstract class AStyleTrigger : IIdentifiable, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the trigger has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the id.
        /// </summary>
        public abstract string Id
        {
            get;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AStyleTrigger"/> class.
        /// </summary>
        protected AStyleTrigger()
        {
            this.mIsDisposed = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Attempt to find the identifiable corresponding to the given Id.
        /// </summary>
        /// <param name="pId">The Id to look for.</param>
        /// <returns>The found identifiable, null otherwise.</returns>
        public IIdentifiable Find(string pId)
        {
            return this;
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {

        }

        /// <summary>
        /// Clone the trigger
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            AStyleTrigger lTrigger = this.InternalClone();

            return lTrigger;
        }

        /// <summary>
        /// Internal clone.
        /// </summary>
        /// <returns></returns>
        protected abstract AStyleTrigger InternalClone();

        #endregion Methods
    }
}
