﻿using OrcNet.UI.Ximl.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="XimlParser"/> class.
    /// 
    /// Ximl for eXtened Interface Markup Language.
    /// </summary>
    public class XimlParser
    {
        #region Fields

        /// <summary>
        /// Stores the set of loaded classes.
        /// </summary>
        private Dictionary<string, XimlClass> mClasses;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the set of loaded classes.
        /// </summary>
        public IEnumerable<XimlClass> Classes
        {
            get
            {
                return this.mClasses.Values;
            }
        }

        /// <summary>
        /// Gets the Ximl class corresponding to the given Identifier.
        /// </summary>
        /// <param name="pId">The class identifier to look for.</param>
        /// <returns>The Ximl class, Null otherwise.</returns>
        public XimlClass this[string pId]
        {
            get
            {
                if ( string.IsNullOrEmpty( pId ) )
                {
                    return null;
                }

                XimlClass lResult;
                if ( this.mClasses.TryGetValue( pId, out lResult ) )
                {
                    return lResult;
                }

                return null;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="XimlParser"/> class.
        /// </summary>
        public XimlParser()
        {
            this.mClasses = new Dictionary<string, XimlClass>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Parses all the Ximl resource files.
        /// </summary>
        /// <param name="pBaseFolder">The base folder name.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool ParseXimlFiles(string pBaseFolder)
        {
            string[] lXimlFiles = Directory.GetFiles( pBaseFolder, "*.ximl", SearchOption.AllDirectories );
            int lFileCount = lXimlFiles.Length;
            for ( int lCurr = 0; lCurr < lFileCount; lCurr++ )
            {
                string lXimlFile = lXimlFiles[ lCurr ];
                try
                {
                    XElement lRoot = XElement.Load( lXimlFile );
                    Type lClassType = lRoot.GetElementType();
                    XAttribute lXClass = lRoot.Attribute( XimlConstants.cClassTag );
                    if ( lXClass != null )
                    {
                        lClassType = Type.GetType( lXClass.Name.LocalName );
                    }

                    if ( lClassType == null )
                    {
                        throw new NullReferenceException( "Class type cannot be Null !!!" );
                    }

                    XimlClass lNewClass = new XimlClass( lClassType );
                    if ( lNewClass.Parse( lRoot ) == false )
                    {
                        Console.WriteLine( string.Format( "Failed to parse the {0} Ximl !!!", lXimlFile ) );
                    }

                    this.mClasses.Add( lNewClass.Id, lNewClass );
                }
                catch
                {
                    Console.WriteLine( string.Format( "Failed to load the {0} Ximl !!!", lXimlFile ) );
                }
            }

            return false;
        }

        #endregion Methods
    }
}
