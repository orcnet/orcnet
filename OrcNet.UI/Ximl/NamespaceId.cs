﻿using System.Diagnostics;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Namespace id structure definition.
    /// </summary>
    [DebuggerDisplay("Id = {mId}")]
    public struct NamespaceId
    {
        #region Fields

        /// <summary>
        /// Stores the id.
        /// </summary>
        private string mId;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NamespaceId"/> class.
        /// </summary>
        /// <param name="pId">The Id</param>
        public NamespaceId(string pId)
        {
            this.mId = pId;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// implicit cast from NodeId to string.
        /// </summary>
        /// <param name="pId">The node Id</param>
        public static implicit operator string(NamespaceId pId)
        {
            return pId.mId;
        }

        #endregion Methods
    }
}
