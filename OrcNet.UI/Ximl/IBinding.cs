﻿using System;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IBinding"/> interface.
    /// </summary>
    public interface IBinding : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the bidning must do nothing on value changes or not.
        /// </summary>
        bool DoNothing
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the bidning status.
        /// </summary>
        BindingStatus Status
        {
            get;
        }

        /// <summary>
        /// Gets or sets the format in the case the value is displayed as a string.
        /// </summary>
        string Format
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value to set to the target in the case the source is Null.
        /// </summary>
        object TargetNullValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value to use if the binding fails.
        /// </summary>
        object FallbackValue
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets the flag indicating whether the binding is for a template definition or not.
        /// </summary>
        bool IsTemplate
        {
            get;
        }

        #endregion Properties
    }
}
