﻿using OrcNet.UI.Ximl.Collections;
using OrcNet.UI.Ximl.XimlConverters;
using System;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl.Helpers
{
    /// <summary>
    /// Definition of the <see cref="XimlHelpers"/> helper class.
    /// </summary>
    public static class XimlHelpers
    {
        #region Methods

        /// <summary>
        /// Gets the object type related to the Xml element.
        /// </summary>
        /// <param name="pElement">The Xeml element to extract the object Type from.</param>
        /// <returns>The object type.</returns>
        public static Type GetElementType(this XElement pElement)
        {
            if ( pElement == null )
            {
                return null;
            }

            string lName = pElement.Name.LocalName;
            return Type.GetType( lName );
        }

        /// <summary>
        /// Gets the requested value from string
        /// </summary>
        /// <param name="pValueAsString"></param>
        /// <param name="pPropertyType"></param>
        /// <param name="pResources">The set of static resources</param>
        /// <returns>The value if succeeded, Null otherwise.</returns>
        public static object GetValue(string pValueAsString, Type pPropertyType, ResourceDictionary pResources)
        {
            object lValue = null;
            if ( pValueAsString.Contains( XimlConstants.cStaticResourceTag ) )
            {
                string lResourceKey = pValueAsString.Replace( XimlConstants.cStaticResourceTag, "" ).Replace( "{", "" ).Replace( "}", "" );
                lValue = pResources[ lResourceKey ];
            }
            else
            {
                IXimlValueConverter lConverter = XimlConverterManager.Instance.GetConverter( pPropertyType );
                if ( lConverter != null )
                {
                    lValue = lConverter.Convert( pValueAsString );
                }
            }

            return lValue;
        }

        #endregion Methods
    }
}
