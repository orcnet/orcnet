﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Ximl.Collections
{
    /// <summary>
    /// Definition of the <see cref="StyleTriggerCollection"/> class.
    /// </summary>
    public class StyleTriggerCollection : ICollection<StyleTrigger>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the trigger list by property name.
        /// </summary>
        private Dictionary<string, StyleTrigger> mTriggers;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the trigger count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mTriggers.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is readonly or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the trigger having the given property name.
        /// </summary>
        /// <param name="pPropertyName">The element property name to look for.</param>
        /// <returns>The found trigger, null otherwise.</returns>
        public StyleTrigger this[string pPropertyName]
        {
            get
            {
                StyleTrigger lTrigger;
                if ( this.mTriggers.TryGetValue( pPropertyName, out lTrigger ) )
                {
                    return lTrigger;
                }

                return null;
            }
            set
            {
                if ( value != null )
                {
                    this.Add( value );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StyleTriggerCollection"/> class.
        /// </summary>
        public StyleTriggerCollection()
        {
            this.mTriggers = new Dictionary<string, StyleTrigger>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new trigger.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(StyleTrigger pItem)
        {
            this.mTriggers[ pItem.Id ] = pItem;
        }

        /// <summary>
        /// Removes a trigger.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(string pItem)
        {
            return this.mTriggers.Remove( pItem );
        }

        /// <summary>
        /// Removes a trigger.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(StyleTrigger pItem)
        {
            return this.Remove( pItem.Id );
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            this.mTriggers.Clear();
        }

        /// <summary>
        /// Checks whether the collection contains the given trigger or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(StyleTrigger pItem)
        {
            return this.mTriggers.ContainsKey( pItem.Id );
        }

        /// <summary>
        /// Copies the setters into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(StyleTrigger[] pArray, int pArrayIndex)
        {
            this.mTriggers.Values.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Gets the setters enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<StyleTrigger> GetEnumerator()
        {
            return this.mTriggers.Values.GetEnumerator();
        }
        
        /// <summary>
        /// Gets the setters enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            foreach ( StyleTrigger lTrigger in this.mTriggers.Values )
            {
                lTrigger.Dispose();
            }
            this.Clear();
        }

        #endregion Methods
    }
}
