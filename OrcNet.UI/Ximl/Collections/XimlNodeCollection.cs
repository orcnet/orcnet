﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Ximl.Collections
{
    /// <summary>
    /// Definition of the <see cref="XimlNodeCollection"/> class.
    /// </summary>
    public class XimlNodeCollection : ICollection<XimlNode>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the ximl node list by type name.
        /// </summary>
        private Dictionary<string, XimlNode> mNodes;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the node count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mNodes.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is readonly or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the Ximl node having teh given Id.
        /// </summary>
        /// <param name="pId">The element Id to look for.</param>
        /// <returns>The found node, null otherwise.</returns>
        public XimlNode this[string pId]
        {
            get
            {
                XimlNode lNode;
                if ( this.mNodes.TryGetValue( pId, out lNode ) )
                {
                    return lNode;
                }

                return null;
            }
            set
            {
                if ( value != null )
                {
                    this.Add( value );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="XimlNodeCollection"/> class.
        /// </summary>
        public XimlNodeCollection()
        {
            this.mNodes = new Dictionary<string, XimlNode>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new node.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(XimlNode pItem)
        {
            this.mNodes[ pItem.Id ] = pItem;
        }

        /// <summary>
        /// Removes a node.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(XimlNode pItem)
        {
            return this.mNodes.Remove( pItem.Id );
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            this.mNodes.Clear();
        }

        /// <summary>
        /// Checks whether the collection contains the given node or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(XimlNode pItem)
        {
            return this.mNodes.ContainsKey( pItem.Id );
        }

        /// <summary>
        /// Copies the node into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(XimlNode[] pArray, int pArrayIndex)
        {
            this.mNodes.Values.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Gets the xaml node enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<XimlNode> GetEnumerator()
        {
            return this.mNodes.Values.GetEnumerator();
        }
        
        /// <summary>
        /// Gets the xaml node enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            foreach ( XimlNode lNode in this.mNodes.Values )
            {
                lNode.Dispose();
            }
            this.Clear();
        }

        #endregion Methods
    }
}
