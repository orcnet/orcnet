﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Ximl.Collections
{
    /// <summary>
    /// Definition of the <see cref="BindingCollection"/> class.
    /// </summary>
    public class BindingCollection : ICollection<ISingleBinding>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the binding list by property name.
        /// </summary>
        private Dictionary<string, ISingleBinding> mBinding;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the node count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mBinding.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is readonly or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets or sets the binding having the given member name.
        /// </summary>
        /// <param name="pMemberName">The element member name to look for.</param>
        /// <returns>The found binding, null otherwise.</returns>
        public ISingleBinding this[string pMemberName]
        {
            get
            {
                ISingleBinding lBinding;
                if ( this.mBinding.TryGetValue( pMemberName, out lBinding ) )
                {
                    return lBinding;
                }

                return null;
            }
            set
            {
                if ( value != null )
                {
                    this.Add( value );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingCollection"/> class.
        /// </summary>
        public BindingCollection()
        {
            this.mBinding = new Dictionary<string, ISingleBinding>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new binding.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(ISingleBinding pItem)
        {
            this.mBinding[ pItem.MemberName ] = pItem;
        }

        /// <summary>
        /// Removes a binding.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(ISingleBinding pItem)
        {
            return this.mBinding.Remove( pItem.MemberName );
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            this.mBinding.Clear();
        }

        /// <summary>
        /// Checks whether the collection contains the given node or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(ISingleBinding pItem)
        {
            return this.mBinding.ContainsKey( pItem.MemberName );
        }

        /// <summary>
        /// Copies the binding into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(ISingleBinding[] pArray, int pArrayIndex)
        {
            this.mBinding.Values.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Gets the ximl node enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ISingleBinding> GetEnumerator()
        {
            return this.mBinding.Values.GetEnumerator();
        }
        
        /// <summary>
        /// Gets the ximl node enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            foreach ( ISingleBinding lBinding in this.mBinding.Values )
            {
                lBinding.Dispose();
            }
            this.Clear();
        }

        #endregion Methods
    }
}
