﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Ximl.Collections
{
    /// <summary>
    /// Definition of the <see cref="StyleSetterCollection"/> class.
    /// </summary>
    public class StyleSetterCollection : ICollection<StyleSetter>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the setter list by property name.
        /// </summary>
        private Dictionary<string, StyleSetter> mSetters;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the node count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mSetters.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is readonly or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the setter having the given property name.
        /// </summary>
        /// <param name="pPropertyName">The element property name to look for.</param>
        /// <returns>The found setter, null otherwise.</returns>
        public StyleSetter this[string pPropertyName]
        {
            get
            {
                StyleSetter lSetter;
                if ( this.mSetters.TryGetValue( pPropertyName, out lSetter ) )
                {
                    return lSetter;
                }

                return null;
            }
            set
            {
                if ( value != null )
                {
                    this.Add( value );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StyleSetterCollection"/> class.
        /// </summary>
        public StyleSetterCollection()
        {
            this.mSetters = new Dictionary<string, StyleSetter>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new setter.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(StyleSetter pItem)
        {
            this.mSetters[ pItem.Id ] = pItem;
        }

        /// <summary>
        /// Removes a setter.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(string pItem)
        {
            return this.mSetters.Remove( pItem );
        }

        /// <summary>
        /// Removes a setter.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(StyleSetter pItem)
        {
            return this.mSetters.Remove( pItem.Id );
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            this.mSetters.Clear();
        }

        /// <summary>
        /// Checks whether the collection contains the given setter or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(StyleSetter pItem)
        {
            return this.mSetters.ContainsKey( pItem.Id );
        }

        /// <summary>
        /// Copies the setters into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(StyleSetter[] pArray, int pArrayIndex)
        {
            this.mSetters.Values.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Gets the setters enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<StyleSetter> GetEnumerator()
        {
            return this.mSetters.Values.GetEnumerator();
        }
        
        /// <summary>
        /// Gets the setters enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            foreach ( StyleSetter lSetter in this.mSetters.Values )
            {
                lSetter.Dispose();
            }
            this.Clear();
        }

        #endregion Methods
    }
}
