﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Ximl.Collections
{
    /// <summary>
    /// Definition of the <see cref="MergedResources"/> class.
    /// </summary>
    public class MergedResources : ICollection<ResourceDictionary>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the shared resources dictionaries.
        /// </summary>
        private Dictionary<Uri, ResourceDictionary> mResourceDictionaries;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the resource dictionary count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mResourceDictionaries.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the container is read only or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the resources dictionary having the given Uri.
        /// </summary>
        /// <param name="pSource">The Uri to look for.</param>
        /// <returns>The resources dictionaries if found, null otherwise.</returns>
        public ResourceDictionary this[Uri pSource]
        {
            get
            {
                ResourceDictionary lResources = null;
                if ( this.mResourceDictionaries.TryGetValue( pSource, out lResources ) )
                {
                    return lResources;
                }

                return null;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MergedResources"/> class.
        /// </summary>
        public MergedResources()
        {
            this.mResourceDictionaries = new Dictionary<Uri, ResourceDictionary>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new resources dictionary.
        /// </summary>
        /// <param name="pItem">The new resources dictionary.</param>
        public void Add(ResourceDictionary pItem)
        {
            this.mResourceDictionaries[ pItem.Source ] = pItem;
        }

        /// <summary>
        /// Removes a resources dictionary.
        /// </summary>
        /// <param name="pItem">The resources dictionary to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool Remove(ResourceDictionary pItem)
        {
            return this.mResourceDictionaries.Remove( pItem.Source );
        }

        /// <summary>
        /// Clear the set of resources dictionaries.
        /// </summary>
        public void Clear()
        {
            this.mResourceDictionaries.Clear();
        }

        /// <summary>
        /// Checks whether the given resources dictionary is contained in this merged set or not.
        /// </summary>
        /// <param name="pItem">The resources dictionary to look for.</param>
        /// <returns>True if contained, false otherwise.</returns>
        public bool Contains(ResourceDictionary pItem)
        {
            return this.mResourceDictionaries.ContainsKey( pItem.Source );
        }

        /// <summary>
        /// Copies the set of resources dictionaries into the given array.
        /// </summary>
        /// <param name="pArray">The array to fill.</param>
        /// <param name="pArrayIndex">The array index to start filling at.</param>
        public void CopyTo(ResourceDictionary[] pArray, int pArrayIndex)
        {
            this.mResourceDictionaries.Values.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Gets the resources dictionaries enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public IEnumerator<ResourceDictionary> GetEnumerator()
        {
            return this.mResourceDictionaries.Values.GetEnumerator();
        }

        /// <summary>
        /// Gets the resources dictionaries enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            foreach ( KeyValuePair<Uri, ResourceDictionary> lDictionary in this.mResourceDictionaries )
            {
                lDictionary.Value.Dispose();
            }
            this.Clear();
        }
        
        #endregion Methods
    }
}
