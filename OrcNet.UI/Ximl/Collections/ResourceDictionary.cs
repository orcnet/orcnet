﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Ximl.Collections
{
    /// <summary>
    /// Definition of the <see cref="ResourceDictionary"/> class.
    /// </summary>
    public class ResourceDictionary : IDictionary<string, IXimlResource>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the merged resources.
        /// </summary>
        private MergedResources mMergedResources;

        /// <summary>
        /// Stores the unifrorm resource identifier to load resources from.
        /// </summary>
        private Uri mSource;

        /// <summary>
        /// Stores the resources container.
        /// </summary>
        private Dictionary<string, IXimlResource> mResourceMap;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the merged resources.
        /// </summary>
        public MergedResources MergedResources
        {
            get
            {
                return this.mMergedResources;
            }
        }

        /// <summary>
        /// Gets or sets the unifrorm resource identifier to load resources from.
        /// </summary>
        public Uri Source
        {
            get
            {
                return this.mSource;
            }
            set
            {
                if ( value != null )
                {
                    this.mSource = value;
                    this.mMergedResources.Add( this );
                }
            }
        }

        /// <summary>
        /// Gets the value by its key.
        /// </summary>
        /// <param name="pKey">The key to look for.</param>
        /// <returns>The value fi found, null otherwise.</returns>
        public IXimlResource this[string pKey]
        {
            get
            {
                if ( string.IsNullOrEmpty( pKey ) )
                {
                    return null;
                }

                IXimlResource lResource = null;
                if ( this.TryGetValue( pKey, out lResource ) )
                {
                    return lResource;
                }

                return null;
            }
            set
            {
                if ( string.IsNullOrEmpty( pKey ) )
                {
                    return;
                }

                this.mResourceMap[ pKey ] = value;
            }
        }

        /// <summary>
        /// Gets the resources count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mResourceMap.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the resources are read only or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the resources keys.
        /// </summary>
        public ICollection<string> Keys
        {
            get
            {
                return this.mResourceMap.Keys;
            }
        }

        /// <summary>
        /// Gets the resources values.
        /// </summary>
        public ICollection<IXimlResource> Values
        {
            get
            {
                return this.mResourceMap.Values;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceDictionary"/> class.
        /// </summary>
        public ResourceDictionary()
        {
            this.mMergedResources = new MergedResources();
            this.mResourceMap     = new Dictionary<string, IXimlResource>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new resource.
        /// </summary>
        /// <param name="pResource">The resource to add.</param>
        public void Add(IXimlResource pResource)
        {
            this.Add( pResource.Id, pResource );
        }

        /// <summary>
        /// Adds a new resource.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(KeyValuePair<string, IXimlResource> pItem)
        {
            this.Add( pItem.Key, pItem.Value );
        }

        /// <summary>
        /// Adds a new resource.
        /// </summary>
        /// <param name="pKey"></param>
        /// <param name="pValue"></param>
        public void Add(string pKey, IXimlResource pValue)
        {
            this.mResourceMap[ pKey ] = pValue;
        }

        /// <summary>
        /// Removes the given item.
        /// </summary>
        /// <param name="pItem">The item to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool Remove(KeyValuePair<string, IXimlResource> pItem)
        {
            return this.Remove( pItem.Key );
        }

        /// <summary>
        /// Removes the given item by key.
        /// </summary>
        /// <param name="pKey">The item key to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool Remove(string pKey)
        {
            return this.mResourceMap.Remove( pKey );
        }

        /// <summary>
        /// Attempts to get a value given its key.
        /// </summary>
        /// <param name="pKey">The key to look for.</param>
        /// <param name="pValue">The mapped value.</param>
        /// <returns>True if found, false otherwise.</returns>
        public bool TryGetValue(string pKey, out IXimlResource pValue)
        {
            return this.mResourceMap.TryGetValue( pKey, out pValue );
        }

        /// <summary>
        /// Clears the resources.
        /// </summary>
        public void Clear()
        {
            this.mResourceMap.Clear();
        }

        /// <summary>
        /// Checks whether the container owns the given item
        /// </summary>
        /// <param name="pItem">The item to look for.</param>
        /// <returns>True if found, false otherwise.</returns>
        public bool Contains(KeyValuePair<string, IXimlResource> pItem)
        {
            // We don t care about the value, a dictionary differenciate element by key making each unique
            // so why caring about values.
            return this.ContainsKey( pItem.Key );
        }

        /// <summary>
        /// Checks whether the container owns the given key
        /// </summary>
        /// <param name="pKey">The key to look for.</param>
        /// <returns>True if found, false otherwise.</returns>
        public bool ContainsKey(string pKey)
        {
            return this.mResourceMap.ContainsKey( pKey );
        }

        /// <summary>
        /// Copies the resources into an array.
        /// </summary>
        /// <param name="pArray">The array to fill.</param>
        /// <param name="pArrayIndex">The array index to start filling at.</param>
        public void CopyTo(KeyValuePair<string, IXimlResource>[] pArray, int pArrayIndex)
        {
            int lCurr  = 0;
            int lCount = pArray.Length - pArrayIndex;
            foreach ( KeyValuePair<string, IXimlResource> lPair in this.mResourceMap )
            {
                pArray[ pArrayIndex + lCurr++ ] = lPair;

                if ( lCurr >= lCount )
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Gets the resources enumerator.
        /// </summary>
        /// <returns>The resources enumerator.</returns>
        public IEnumerator<KeyValuePair<string, IXimlResource>> GetEnumerator()
        {
            return this.mResourceMap.GetEnumerator();
        }
        
        /// <summary>
        /// Gets the resources enumerator.
        /// </summary>
        /// <returns>The resources enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            foreach ( KeyValuePair<string, IXimlResource> lPair in this.mResourceMap )
            {
                lPair.Value.Dispose();
            }
            this.Clear();
        }

        #endregion Methods
    }
}
