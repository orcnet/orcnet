﻿using System;
using System.Globalization;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IValueConverter"/> interface.
    /// </summary>
    public interface IValueConverter
    {
        #region Methods

        /// <summary>
        /// Converts a value from the source type to the given target type and return it.
        /// </summary>
        /// <param name="pSource">The source to convert</param>
        /// <param name="pTargeType">The target type to convert to.</param>
        /// <param name="pParameter">An optional parameter for the conversion.</param>
        /// <param name="pCulture">The culture if needed.</param>
        /// <returns>The converted object.</returns>
        object Convert(object pSource, Type pTargeType, object pParameter, CultureInfo pCulture);

        /// <summary>
        /// Converts a value back to its original type.
        /// </summary>
        /// <param name="pSource">The object to convert back.</param>
        /// <param name="pTargetType">The target type</param>
        /// <param name="pParameter">An optional parameter for the conversion.</param>
        /// <param name="pCulture">The culture if needed.</param>
        /// <returns>The converted object.</returns>
        object ConvertBack(object pSource, Type pTargetType, object pParameter, CultureInfo pCulture);

        #endregion Methods
    }
}
