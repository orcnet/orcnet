﻿using System.ComponentModel;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Binding changes delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pValue">The current value.</param>
    public delegate void BindingChangesDelegate(object pSender, object pValue);

    /// <summary>
    /// Definition of the <see cref="ISingleBinding"/> interface.
    /// </summary>
    public interface ISingleBinding : IBinding
    {
        #region Events

        /// <summary>
        /// Event triggered before the source value be updated, therefore, has changed.
        /// </summary>
        event BindingChangesDelegate SourceChanging;

        /// <summary>
        /// Event triggered after the source value be updated.
        /// </summary>
        event BindingChangesDelegate SourceChanged;

        /// <summary>
        /// Event triggered before the target value be updated, therefore, has changed.
        /// </summary>
        event BindingChangesDelegate TargetChanging;

        /// <summary>
        /// Event triggered after the target value be updated.
        /// </summary>
        event BindingChangesDelegate TargetChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the source property.
        /// </summary>
        PropertyInfo SourceProperty
        {
            get;
        }

        /// <summary>
        /// Gets the target property.
        /// </summary>
        PropertyInfo TargetProperty
        {
            get;
        }

        /// <summary>
        /// Gets the ref to the source instance.
        /// </summary>
        INotifyPropertyChanged Source
        {
            get;
        }

        /// <summary>
        /// Gets the ref to the target instance.
        /// </summary>
        INotifyPropertyChanged Target
        {
            get;
        }

        /// <summary>
        /// Gets the bound member name.
        /// </summary>
        string MemberName
        {
            get;
        }

        #endregion Properties

        #region Methods
        
        /// <summary>
        /// Bind the To-bind-instances regarding to the binding information.
        /// </summary>
        /// <param name="pSource">The source instance.</param>
        /// <param name="pTarget">The target instance.</param>
        /// <returns>True if successfully bound, false otherwise.</returns>
        bool Bind(IBindable pSource, IBindable pTarget);

        #endregion Methods
    }
}
