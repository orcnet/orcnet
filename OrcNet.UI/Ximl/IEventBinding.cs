﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IEventBinding"/> interface.
    /// </summary>
    public interface IEventBinding : IBinding
    {
        #region Properties

        /// <summary>
        /// Gets the bound member name.
        /// </summary>
        string MemberName
        {
            get;
        }

        #endregion Properties

        #region Methods
        
        /// <summary>
        /// Bind the To-bind-instances regarding to the binding information.
        /// </summary>
        /// <param name="pSource">The source instance.</param>
        /// <param name="pTarget">The target instance.</param>
        /// <returns>True if successfully bound, false otherwise.</returns>
        bool Bind(IBindable pSource, IBindable pTarget);

        #endregion Methods
    }
}
