﻿using System;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="ItemsPanelTemplate"/> class.
    /// </summary>
    public class ItemsPanelTemplate : FrameworkTemplate
    {
        #region Fields

        /// <summary>
        /// Stores the UI target type this template is for.
        /// </summary>
        private Type mTargetType;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the UI target type this template is for.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return this.mTargetType;
            }
            set
            {
                this.mTargetType = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemsPanelTemplate"/> class.
        /// </summary>
        public ItemsPanelTemplate()
        {

        }
        
        #endregion Constructor

        #region Methods

        
        #endregion Methods
    }
}
