﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="TemplateContent"/> class.
    /// </summary>
    public class TemplateContent
    {
        #region Fields

        /// <summary>
        /// Stores the content Ximl root node.
        /// </summary>
        private XimlNode mRoot;

        /// <summary>
        /// Stores the template content owner.
        /// </summary>
        private FrameworkTemplate mOwner;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the template content owner.
        /// </summary>
        internal FrameworkTemplate Owner
        {
            get
            {
                return this.mOwner;
            }
            set
            {
                this.mOwner = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateContent"/> class.
        /// </summary>
        public TemplateContent()
        {
            // Fake node type for the root node.
            this.mRoot = new XimlNode( typeof(XimlNode) );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Parses Ximl content to form the template this content is for.
        /// </summary>
        internal void ParseXiml()
        {

        }

        /// <summary>
        /// Implicitely turns the content into the wrapped Ximl node defining the template decoration.
        /// </summary>
        /// <param name="pTemplate"></param>
        public static implicit operator XimlNode(TemplateContent pTemplate)
        {
            return pTemplate.mRoot;
        }

        #endregion Methods
    }
}
