﻿using System.Collections.Generic;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="ISetterContainer"/> interface.
    /// </summary>
    public interface ISetterContainer
    {
        #region Properties

        /// <summary>
        /// Gets the set of setters.
        /// </summary>
        IEnumerable<StyleSetter> Setters
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a setter.
        /// </summary>
        /// <param name="pSetter">The setter to add.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        bool AddSetter(StyleSetter pSetter);

        /// <summary>
        /// Adds a setter.
        /// </summary>
        /// <param name="pProperty">The property the setter is for.</param>
        /// <param name="pValue">The property value to set.</param>
        /// <param name="pTargetName">The target name if any.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        bool AddSetter(PropertyInfo pProperty, object pValue, string pTargetName = null);

        /// <summary>
        /// Removes a setter
        /// </summary>
        /// <param name="pSetter">The setter to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemoveSetter(StyleSetter pSetter);

        /// <summary>
        /// Removes a setter
        /// </summary>
        /// <param name="pPropertyName">The property the setter is for.</param>
        /// <returns>True if removed, false otherwise.</returns>
        bool RemoveSetter(string pPropertyName);

        #endregion Methods
    }
}
