﻿using OrcNet.UI.Ximl.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="StyleTrigger"/> class.
    /// </summary>
    public class StyleTrigger : AStyleTrigger, ISetterContainer
    {
        #region Fields

        /// <summary>
        /// Stores the property.
        /// </summary>
        private PropertyInfo mProperty;

        /// <summary>
        /// Stores the value to set as string (cannot be a binding).
        /// </summary>
        private object mValue;

        /// <summary>
        /// Stores the set of setters.
        /// </summary>
        private StyleSetterCollection mSetters;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the id.
        /// </summary>
        public override string Id
        {
            get
            {
                return this.Property.Name;
            }
        }

        /// <summary>
        /// Gets the setters.
        /// </summary>
        public IEnumerable<StyleSetter> Setters
        {
            get
            {
                return this.mSetters;
            }
        }

        /// <summary>
        /// Gets the property.
        /// </summary>
        public PropertyInfo Property
        {
            get
            {
                return this.mProperty;
            }
        }

        /// <summary>
        /// Gets the value to set as string (cannot be a binding).
        /// </summary>
        public object Value
        {
            get
            {
                return this.mValue;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StyleTrigger"/> class.
        /// </summary>
        /// <param name="pProperty">The setter property to fill with the value.</param>
        /// <param name="pValue">The setter value to set.</param>
        public StyleTrigger(PropertyInfo pProperty, object pValue)
        {
            this.mValue = pValue;
            this.mProperty = pProperty;
            this.mSetters = new StyleSetterCollection();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a setter.
        /// </summary>
        /// <param name="pSetter">The setter to add.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool AddSetter(StyleSetter pSetter)
        {
            if ( pSetter == null ||
                 pSetter.Property == null )
            {
                return false;
            }

            // Overwrite if already in.
            this.mSetters[ pSetter.Id ] = pSetter;
            return true;
        }

        /// <summary>
        /// Adds a setter.
        /// </summary>
        /// <param name="pProperty">The property the setter is for.</param>
        /// <param name="pValue">The property value to set.</param>
        /// <param name="pTargetName">The target name if any.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool AddSetter(PropertyInfo pProperty, object pValue, string pTargetName = null)
        {
            return this.AddSetter( new StyleSetter( pProperty, pValue, pTargetName ) );
        }
        
        /// <summary>
        /// Removes a setter.
        /// </summary>
        /// <param name="pPropertyName">The setter identifier to remove.</param>
        /// <returns></returns>
        public bool RemoveSetter(string pPropertyName)
        {
            return this.mSetters.Remove( pPropertyName );
        }

        /// <summary>
        /// Removes a setter.
        /// </summary>
        /// <param name="pSetter">The setter to remove.</param>
        /// <returns></returns>
        public bool RemoveSetter(StyleSetter pSetter)
        {
            return this.RemoveSetter( pSetter.Id );
        }

        /// <summary>
        /// Internal clone.
        /// </summary>
        /// <returns></returns>
        protected override AStyleTrigger InternalClone()
        {
            StyleTrigger lTrigger = new StyleTrigger( this.mProperty, this.mValue );
            foreach ( StyleSetter lSetter in this.mSetters )
            {
                lTrigger.mSetters.Add( lSetter.Clone() as StyleSetter );
            }
            return lTrigger;
        }
        
        #endregion Methods
    }
}
