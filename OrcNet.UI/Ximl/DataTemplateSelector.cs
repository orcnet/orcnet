﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="DataTemplateSelector"/> class.
    /// </summary>
    public class DataTemplateSelector
    {
        #region Methods

        /// <summary>
        /// Override this method to return an app specific <seealso cref="DataTemplate"/>.
        /// </summary>
        /// <param name="pItem">The data content</param>
        /// <param name="pContainer">The element to which the template will be applied</param>
        /// <returns>An app-specific template to apply, or null.</returns>
        public virtual DataTemplate SelectTemplate(object pItem, UIElement pContainer)
        {
            return null;
        }

        #endregion Methods
    }
}
