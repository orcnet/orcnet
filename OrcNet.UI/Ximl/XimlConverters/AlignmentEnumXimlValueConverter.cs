﻿namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="AlignmentEnumXimlValueConverter"/> class.
    /// </summary>
    public class AlignmentEnumXimlValueConverter : AEnumXimlValueConverter<Alignment>
    {

    }
}
