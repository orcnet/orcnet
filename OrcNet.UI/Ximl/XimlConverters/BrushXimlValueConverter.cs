﻿using System;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="BrushXimlValueConverter"/> class.
    /// </summary>
    public class BrushXimlValueConverter : IXimlValueConverter
    {
        #region Fields

        /// <summary>
        /// Stores the brush default value.
        /// </summary>
        private static readonly ABrush cDefaultValue = new SolidColorBrush( Color.White );

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return typeof(ABrush);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        public object Convert(string pValue)
        {
            if ( string.IsNullOrEmpty( pValue ) )
            {
                return cDefaultValue;
            }

            try
            {
                return ABrush.Parse( pValue );
            }
            catch
            {

            }

            return cDefaultValue;
        }

        #endregion Methods
    }
}
