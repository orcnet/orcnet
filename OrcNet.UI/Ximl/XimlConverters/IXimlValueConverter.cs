﻿using System;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="IXimlValueConverter"/> interface.
    /// 
    /// NOTE: Those converters are simply in charge of turning 'Value' string
    /// into the concret instance object to set afterward.
    /// </summary>
    public interface IXimlValueConverter
    {
        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        Type TargetType
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        object Convert(string pValue);

        #endregion Methods
    }
}
