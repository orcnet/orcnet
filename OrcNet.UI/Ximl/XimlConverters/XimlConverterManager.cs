﻿using OrcNet.UI.Extensions;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="XimlConverterManager"/> class.
    /// </summary>
    public class XimlConverterManager
    {
        #region Fields

        /// <summary>
        /// Stores the Ximl contract manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile XimlConverterManager sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the parser contracts.
        /// </summary>
        private Dictionary<Type, IXimlValueConverter> mConverters;

        #endregion Fields

        #region Methods

        /// <summary>
        /// Gets the Ximl converter manager handle.
        /// </summary>
        public static XimlConverterManager Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if ( sInstance == null )
                {
                    // Lock on
                    lock ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if ( sInstance == null )
                        {
                            sInstance = new XimlConverterManager();
                        }
                    }
                }

                return sInstance;
            }
        }

        #endregion Methods

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="XimlConverterManager"/> class.
        /// </summary>
        private XimlConverterManager()
        {
            this.mConverters = new Dictionary<Type, IXimlValueConverter>();
            // Load all service being in the module assembly as Core services.
            IEnumerable<Type> lConverterTypes = typeof(IXimlValueConverter).GetInheritedTypes();
            foreach ( Type lType in lConverterTypes )
            {
                ConstructorInfo lConstructor = lType.GetConstructor( new Type[] { } );
                IXimlValueConverter lConverter = lConstructor.Invoke( new object[] { } ) as IXimlValueConverter;
                this.mConverters.Add( lConverter.TargetType, lConverter );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the contract corresponding to the given tag.
        /// </summary>
        /// <param name="pType">The type to get the converter from string to this type.</param>
        /// <returns>The Ximl contract, Null otherwise.</returns>
        public IXimlValueConverter GetConverter(Type pType)
        {
            IXimlValueConverter lConverter = null;
            if ( this.mConverters.TryGetValue( pType, out lConverter ) )
            {
                return lConverter;
            }

            return null;
        }

        #endregion Methods
    }
}
