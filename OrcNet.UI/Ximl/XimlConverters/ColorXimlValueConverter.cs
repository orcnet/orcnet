﻿using System;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="DoubleXimlValueConverter"/> class.
    /// </summary>
    public class ColorXimlValueConverter : IXimlValueConverter
    {
        #region Fields

        /// <summary>
        /// Stores the color default value.
        /// </summary>
        private static readonly Color cDefaultValue = Color.White;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return typeof(Color);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        public object Convert(string pValue)
        {
            if ( string.IsNullOrEmpty( pValue ) )
            {
                return cDefaultValue;
            }

            try
            {
                return Color.Parse( pValue );
            }
            catch
            {

            }

            return cDefaultValue;
        }

        #endregion Methods
    }
}
