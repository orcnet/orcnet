﻿using System;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="UInt32XimlValueConverter"/> class.
    /// </summary>
    public class UInt32XimlValueConverter : IXimlValueConverter
    {
        #region Fields

        /// <summary>
        /// Stores the unsigned Int32 default value.
        /// </summary>
        private const uint cDefaultValue = 0;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return typeof(uint);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        public object Convert(string pValue)
        {
            if ( string.IsNullOrEmpty( pValue ) )
            {
                return cDefaultValue;
            }

            uint lResult;
            if ( uint.TryParse( pValue, out lResult ) )
            {
                return lResult;
            }

            return cDefaultValue;
        }

        #endregion Methods
    }
}
