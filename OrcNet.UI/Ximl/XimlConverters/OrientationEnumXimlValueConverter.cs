﻿namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="OrientationEnumXimlValueConverter"/> class.
    /// </summary>
    public class OrientationEnumXimlValueConverter : AEnumXimlValueConverter<Orientation>
    {

    }
}
