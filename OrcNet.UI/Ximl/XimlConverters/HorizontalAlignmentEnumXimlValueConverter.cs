﻿namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="HorizontalAlignmentEnumXimlValueConverter"/> class.
    /// </summary>
    public class HorizontalAlignmentEnumXimlValueConverter : AEnumXimlValueConverter<HorizontalAlignment>
    {

    }
}
