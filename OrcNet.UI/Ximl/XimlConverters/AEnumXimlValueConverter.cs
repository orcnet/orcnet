﻿using System;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the abstract base <see cref="AEnumXimlValueConverter{T}"/> class.
    /// </summary>
    public abstract class AEnumXimlValueConverter<T> : IXimlValueConverter where T : struct
    {
        #region Fields

        /// <summary>
        /// Stores the enum default value.
        /// </summary>
        private static readonly T cDefaultValue = default(T);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return typeof(T);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        public object Convert(string pValue)
        {
            if ( string.IsNullOrEmpty( pValue ) )
            {
                return cDefaultValue;
            }

            T lResult;
            if ( Enum.TryParse( pValue, out lResult ) )
            {
                return lResult;
            }

            return cDefaultValue;
        }

        #endregion Methods
    }
}
