﻿using System;
using System.Globalization;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="FloatXimlValueConverter"/> class.
    /// </summary>
    public class FloatXimlValueConverter : IXimlValueConverter
    {
        #region Fields

        /// <summary>
        /// Stores the float default value.
        /// </summary>
        private const float cDefaultValue = 0.0f;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return typeof(float);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        public object Convert(string pValue)
        {
            if ( string.IsNullOrEmpty( pValue ) )
            {
                return cDefaultValue;
            }

            float lResult;
            if ( float.TryParse( pValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lResult ) )
            {
                return lResult;
            }

            return cDefaultValue;
        }

        #endregion Methods
    }
}
