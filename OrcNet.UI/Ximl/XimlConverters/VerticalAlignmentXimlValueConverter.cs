﻿namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="VerticalAlignmentXimlValueConverter"/> class.
    /// </summary>
    public class VerticalAlignmentXimlValueConverter : AEnumXimlValueConverter<VerticalAlignment>
    {

    }
}
