﻿using System;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="Int16XimlValueConverter"/> class.
    /// </summary>
    public class Int16XimlValueConverter : IXimlValueConverter
    {
        #region Fields

        /// <summary>
        /// Stores the short default value.
        /// </summary>
        private const short cDefaultValue = 0;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return typeof(short);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        public object Convert(string pValue)
        {
            if ( string.IsNullOrEmpty( pValue ) )
            {
                return cDefaultValue;
            }

            short lResult;
            if ( short.TryParse( pValue, out lResult ) )
            {
                return lResult;
            }

            return cDefaultValue;
        }

        #endregion Methods
    }
}
