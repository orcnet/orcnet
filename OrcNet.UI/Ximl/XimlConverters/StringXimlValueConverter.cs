﻿using System;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="StringXimlValueConverter"/> class.
    /// </summary>
    public class StringXimlValueConverter : IXimlValueConverter
    {
        #region Fields

        /// <summary>
        /// Stores the string default value.
        /// </summary>
        private const string cDefaultValue = "Error";

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return typeof(string);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        public object Convert(string pValue)
        {
            if ( string.IsNullOrEmpty( pValue ) )
            {
                return cDefaultValue;
            }

            return pValue;
        }

        #endregion Methods
    }
}
