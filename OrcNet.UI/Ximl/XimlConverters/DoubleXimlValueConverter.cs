﻿using System;
using System.Globalization;

namespace OrcNet.UI.Ximl.XimlConverters
{
    /// <summary>
    /// Definition of the <see cref="DoubleXimlValueConverter"/> class.
    /// </summary>
    public class DoubleXimlValueConverter : IXimlValueConverter
    {
        #region Fields

        /// <summary>
        /// Stores the double default value.
        /// </summary>
        private const double cDefaultValue = 0.0;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the converter target type.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return typeof(double);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Turns the given string into the corresponding value.
        /// </summary>
        /// <param name="pValue">The value as string to parse.</param>
        /// <returns>The concret value.</returns>
        public object Convert(string pValue)
        {
            if ( string.IsNullOrEmpty( pValue ) )
            {
                return cDefaultValue;
            }

            double lResult;
            if ( double.TryParse( pValue, NumberStyles.Float, CultureInfo.InvariantCulture, out lResult ) )
            {
                return lResult;
            }

            return cDefaultValue;
        }

        #endregion Methods
    }
}
