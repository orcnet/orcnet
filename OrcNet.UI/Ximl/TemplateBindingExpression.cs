﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="TemplateBindingExpression"/> class.
    /// </summary>
    public class TemplateBindingExpression : BindingExpression
    {
        #region Fields


        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the binding is for a template definition or not.
        /// </summary>
        public override bool IsTemplate
        {
            get
            {
                return true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateBindingExpression"/> class.
        /// </summary>
        /// <param name="pSourcePropertyName">The source property name.</param>
        /// <param name="pTargetPropertyName">The target property name.</param>
        /// <param name="pMode">The binding mode.</param>
        /// <param name="pTargetElementName">The target element name to bound to (different from the current DataContext)</param>
        /// <param name="pTargetPathToProperty">The target element path to property to bound.</param>
        public TemplateBindingExpression(string pSourcePropertyName, string pTargetPropertyName, BindingMode pMode = BindingMode.OneWay, string pTargetElementName = null, string pTargetPathToProperty = null) :
        base( pSourcePropertyName, pTargetPropertyName, pMode, pTargetElementName, pTargetPathToProperty )
        {
            
        }

        #endregion Constructor
    }
}
