﻿namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="BindingMode"/> enumeration.
    /// </summary>
    public enum BindingMode
    {
        /// <summary>
        /// Source toward target.
        /// </summary>
        OneWay =0,

        /// <summary>
        /// Both directions.
        /// </summary>
        TwoWay,

        /// <summary>
        /// Target toward source.
        /// </summary>
        OneWayToSource
    }
}
