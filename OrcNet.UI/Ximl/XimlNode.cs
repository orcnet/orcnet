﻿using OrcNet.UI.Core;
using OrcNet.UI.Ximl.Collections;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="XimlNode"/> class.
    /// </summary>
    [DebuggerDisplay("Id = {Id}")]
    public class XimlNode : IBindable, IXimlResource
    {
        #region Fields

        /// <summary>
        /// Stores the element to build name (Can be empty)
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the instance.
        /// </summary>
        private IUIResource mInstance;

        /// <summary>
        /// Stores the element index if child of a parent.
        /// </summary>
        private int mIndex;

        /// <summary>
        /// Stores the type to build.
        /// </summary>
        private Type mType;

        /// <summary>
        /// Stores the specific type resource tag.
        /// </summary>
        private string mTypeResourceTag;

        /// <summary>
        /// Stores the parent node.
        /// </summary>
        private XimlNode mParent;

        /// <summary>
        /// Stores the property values by property info.
        /// </summary>
        protected Dictionary<PropertyInfo, object> mPropertyValues;

        /// <summary>
        /// Stores the 
        /// </summary>
        protected XimlNodeCollection mChildren;

        /// <summary>
        /// Stores the set of resources.
        /// </summary>
        protected ResourceDictionary mResources;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event triggered on property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the element to build name (Can be empty)
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
            set
            {
                this.mName = value;
            }
        }

        /// <summary>
        /// Gets the children count.
        /// </summary>
        public int ChildCount
        {
            get
            {
                return this.mChildren.Count;
            }
        }

        /// <summary>
        /// Gets the id.
        /// </summary>
        public string Id
        {
            get
            {
                return string.Format( "{0},{1},{2}", this.mType.FullName, this.mType.Assembly.GetName().Name, this.mIndex );
            }
        }

        /// <summary>
        /// Gets the element index if child of a parent.
        /// </summary>
        public int Index
        {
            get
            {
                return this.mIndex;
            }
        }

        /// <summary>
        /// Gets the type to build.
        /// </summary>
        public Type Type
        {
            get
            {
                return this.mType;
            }
        }

        /// <summary>
        /// Gets the children if any.
        /// </summary>
        public IEnumerable<XimlNode> Children
        {
            get
            {
                return this.mChildren;
            }
        }

        /// <summary>
        /// Gets the set of local node resources.
        /// </summary>
        public ResourceDictionary Resources
        {
            get
            {
                return this.mResources;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the node represent a template or not.
        /// </summary>
        public bool HasTemplate
        {
            get
            {
                return typeof(TemplatedControl).IsAssignableFrom( this.mType );
            }
        }

        /// <summary>
        /// Gets the bindable instance.
        /// </summary>
        public IUIResource Instance
        {
            get
            {
                return this.mInstance;
            }
            set
            {
                this.mInstance = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="XimlNode"/> class.
        /// </summary>
        /// <param name="pType">The type to build.</param>
        public XimlNode(Type pType)
        {
            this.mType  = pType;
            this.mIndex = 0;
            this.mChildren = new XimlNodeCollection();
            this.mResources = new ResourceDictionary();
            this.mPropertyValues = new Dictionary<PropertyInfo, object>();

            // Build the type resource tag depending on the type.
            this.mTypeResourceTag = string.Format( "{0}.Resources", this.mType.Name );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the node contains already the property value to set.
        /// </summary>
        /// <param name="pInfo">The property info as Key for the value.</param>
        /// <param name="pValue">The value if found, null otherwise.</param>
        /// <returns>True if has the property value, false otherwise.</returns>
        public bool HasValue(PropertyInfo pInfo, out object pValue)
        {
            if ( this.mPropertyValues.TryGetValue( pInfo, out pValue ) )
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Adds a new value to set to the object this node defines.
        /// </summary>
        /// <param name="pProperty">The property to set.</param>
        /// <param name="pValue">Teh value to set.</param>
        public void AddValue(PropertyInfo pProperty, object pValue)
        {
            if ( pProperty == null ||
                 pValue == null )
            {
                return;
            }

            this.mPropertyValues[ pProperty ] = pValue;
        }

        /// <summary>
        /// Adds a new child node.
        /// </summary>
        /// <param name="pChild">The child node.</param>
        public void AddChild(XimlNode pChild)
        {
            if ( this.mChildren.Contains( pChild ) )
            {
                // If child index already existing, get the next available one.
                pChild.mIndex = this.mChildren.Max( pElt => pElt.mIndex ) + 1;
            }

            this.mChildren.Add( pChild );
        }

        /// <summary>
        /// Attempt to find the identifiable corresponding to the given Id.
        /// </summary>
        /// <param name="pId">The Id to look for.</param>
        /// <returns>The found identifiable, null otherwise.</returns>
        public virtual IIdentifiable Find(string pId)
        {
            if ( pId == this.Id )
            {
                return this;
            }

            // Look upward.
            return this.mParent.Find( pId );
        }

        /// <summary>
        /// Checks whether this node is equal to another or not.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns></returns>
        public override bool Equals(object pOther)
        {
            if (pOther == null)
            {
                return false;
            }

            XimlNode lNode = pOther as XimlNode;
            return this.mType  == lNode.mType && 
                   this.mIndex == lNode.mIndex;
        }

        /// <summary>
        /// Gets the hashcode.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.mType.GetHashCode() ^ this.mIndex.GetHashCode();
        }
        
        /// <summary>
        /// Implicit node to string convertor.
        /// </summary>
        /// <param name="pNode"></param>
        public static implicit operator string(XimlNode pNode)
        {
            return pNode.ToString();
        }

        /// <summary>
        /// Turns the element into a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("[{0}].{1}", this.Id, this.mIndex );
        }

        #region Methods ICloneable

        /// <summary>
        /// Clone the Ximl node.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            XimlNode lClone = this.CreateClone();
            lClone.mResources = this.mResources;
            foreach ( XimlNode lChild in this.mChildren )
            {
                lClone.mChildren.Add( lChild.Clone() as XimlNode );
            }

            foreach ( KeyValuePair<PropertyInfo, object> lValues in this.mPropertyValues )
            {
                lClone.mPropertyValues.Add( lValues.Key, 
                                            lValues.Value is ICloneable ? (lValues.Value as ICloneable).Clone() : lValues.Value );
            }
            return lClone;
        }

        /// <summary>
        /// Internal clone method.
        /// </summary>
        /// <returns>The cloned instance.</returns>
        protected virtual XimlNode CreateClone()
        {
            return new XimlNode( this.mType );
        }

        #endregion Methods ICloneable

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.mParent = null;

            this.mChildren.Dispose();
            this.mPropertyValues.Clear();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
