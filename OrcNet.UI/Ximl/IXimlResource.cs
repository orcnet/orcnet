﻿using System;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IXimlResource"/> interface.
    /// </summary>
    public interface IXimlResource : IIdentifiable, IDisposable, ICloneable
    {

    }
}
