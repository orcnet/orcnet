﻿using System;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="StyleSetter"/> class.
    /// </summary>
    public class StyleSetter : IIdentifiable, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the setter target name.
        /// </summary>
        private string mTargetName;

        /// <summary>
        /// Stores the property to set.
        /// </summary>
        private PropertyInfo mProperty;

        /// <summary>
        /// Stores the value to set as string (cannot be a binding).
        /// </summary>
        private object mValue;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the setter identifier.
        /// </summary>
        public string Id
        {
            get
            {
                return this.Property.Name;
            }
        }

        /// <summary>
        /// Gets the setter target name.
        /// </summary>
        public string TargetName
        {
            get
            {
                return this.mTargetName;
            }
        }

        /// <summary>
        /// Gets the property to set.
        /// </summary>
        public PropertyInfo Property
        {
            get
            {
                return this.mProperty;
            }
        }

        /// <summary>
        /// Gets or sets the value to set as string (cannot be a binding).
        /// </summary>
        public object Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                this.mValue = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StyleSetter"/> class.
        /// </summary>
        /// <param name="pProperty">The setter property to fill with the value.</param>
        /// <param name="pValue">The setter value to set.</param>
        /// <param name="pTargetName">The target name (optional)</param>
        public StyleSetter(PropertyInfo pProperty, object pValue, string pTargetName = null)
        {
            this.mValue = pValue;
            this.mTargetName = pTargetName;
            this.mProperty = pProperty;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Attempt to find the identifiable corresponding to the given Id.
        /// </summary>
        /// <param name="pId">The Id to look for.</param>
        /// <returns>The found identifiable, null otherwise.</returns>
        public IIdentifiable Find(string pId)
        {
            return this;
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.mValue = null;
        }

        /// <summary>
        /// Clone the setter.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            StyleSetter lClone = new StyleSetter( this.mProperty, 
                                                  this.mValue is ICloneable ? (this.mValue as ICloneable).Clone() : this.mValue, 
                                                  this.mTargetName );

            return lClone;
        }
        
        #endregion Methods
    }
}
