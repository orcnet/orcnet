﻿using System.Xml.Linq;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="IXimlParsable"/> interface.
    /// </summary>
    public interface IXimlParsable
    {
        #region Methods

        /// <summary>
        /// Parse from a Xml fragment.
        /// </summary>
        /// <param name="pFragment">The xml fragment.</param>
        /// <returns>True if succeeded, false otheriwse.</returns>
        bool Parse(XElement pFragment);

        #endregion Methods
    }
}
