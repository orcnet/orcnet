﻿using OrcNet.UI.Ximl.Collections;
using OrcNet.UI.Ximl.Helpers;
using OrcNet.UI.Ximl.XimlContracts;
using System;
using System.Xml.Linq;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="XimlClass"/> class.
    /// 
    /// Containing a set of Ximl nodes that defines a Ximl 
    /// interface class being a control.
    /// 
    /// NOTE: Equivalent to a single Ximl file.
    /// </summary>
    public class XimlClass : XimlNode, IXimlParsable
    {
        #region Fields
        
        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="XimlClass"/> class.
        /// </summary>
        /// <param name="pType">The Ximl class type.</param>
        public XimlClass(Type pType) :
        base( pType )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods IXmlParsable

        /// <summary>
        /// Parse from a Xml.
        /// </summary>
        /// <param name="pRoot">The xml root.</param>
        /// <returns>True if succeeded, false otheriwse.</returns>
        public bool Parse(XElement pRoot)
        {
            IXimlParserContract lContract = XimlContractManager.Instance.GetContract( pRoot.GetElementType() );
            if ( lContract == null )
            {
                return false;
            }
            
            // Start parsing.
            ParserContractContext lContext = new ParserContractContext();
            lContext.TargetType = this.Type;
            lContext.Resources  = new ResourceDictionary();
            lContext.Add( this );

            if ( lContract.Read( pRoot, lContext, this ) )
            {
                return true;
            }

            return false;
        }

        #endregion Methods IXmlParsable

        #region Methods IIdentifiable

        /// <summary>
        /// Find the Ximl element corresponding to the given Id.
        /// </summary>
        /// <param name="pId">The Id to look for.</param>
        /// <returns>The Ximl element if found, Null otheriwse.</returns>
        public override IIdentifiable Find(string pId)
        {
            if ( this.Id == pId )
            {
                return this;
            }

            // Look downward.
            foreach ( XimlNode lChild in this.Children )
            {
                IIdentifiable lResult = lChild.Find( pId );
                if ( lResult != null )
                {
                    return lResult;
                }
            }

            return null;
        }

        #endregion Methods IIdentifiable

        #region Methods ICloneable

        /// <summary>
        /// Create this Ximl class clone.
        /// </summary>
        /// <returns></returns>
        protected override XimlNode CreateClone()
        {
            return new XimlClass( this.Type );
        }

        #endregion Methods ICloneable

        #endregion Methods
    }
}
