﻿using OrcNet.UI.Core;
using OrcNet.UI.Ximl.Collections;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="ATemplate"/> class.
    /// </summary>
    public abstract class ATemplate
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the template is read only or not.
        /// </summary>
        private bool mIsReadOnly;

        /// <summary>
        /// Stores the template content.
        /// </summary>
        private TemplateContent mTemplate;

        /// <summary>
        /// Stores the resources dictionary.
        /// </summary>
        private ResourceDictionary mResources;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the template is read only or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return this.mIsReadOnly;
            }
        }

        /// <summary>
        /// Gets or sets the template content, that is, the Ximl Nodes describing the UI resource to instanciate.
        /// </summary>
        public TemplateContent Template
        {
            get
            {
                return this.mTemplate;
            }
            set
            {
                this.mTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the resources dictionary.
        /// </summary>
        public ResourceDictionary Resources
        {
            get
            {
                return this.mResources;
            }
            set
            {
                this.mResources = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Makes the template Read Only.
        /// </summary>
        public void Seal()
        {
            this.mIsReadOnly = true;
        }

        /// <summary>
        /// Loads a new instance of the templated content.
        /// </summary>
        /// <returns>The new content instance root element.</returns>
        public virtual IUIResource LoadContent()
        {
            return null;
        }

        #endregion Methods
    }
}
