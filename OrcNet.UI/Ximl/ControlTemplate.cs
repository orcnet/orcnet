﻿using OrcNet.UI.Ximl.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace OrcNet.UI.Ximl
{
    /// <summary>
    /// Definition of the <see cref="ControlTemplate"/> class.
    /// </summary>
    public class ControlTemplate : FrameworkTemplate, ITriggerContainer
    {
        #region Fields

        /// <summary>
        /// Stores the UI target type this template is for.
        /// </summary>
        private Type mTargetType;

        /// <summary>
        /// Stores the set of template triggers.
        /// </summary>
        private StyleTriggerCollection mTriggers;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the UI target type this template is for.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return this.mTargetType;
            }
            set
            {
                this.mTargetType = value;
            }
        }

        /// <summary>
        /// Gets the set of template triggers.
        /// </summary>
        public IEnumerable<StyleTrigger> Triggers
        {
            get
            {
                return this.mTriggers;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlTemplate"/> class.
        /// </summary>
        public ControlTemplate() :
        this( null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlTemplate"/> class.
        /// </summary>
        /// <param name="pTargetType">The target type the template is for.</param>
        public ControlTemplate(Type pTargetType)
        {
            this.mTargetType = pTargetType;
            this.mTriggers = new StyleTriggerCollection();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a trigger.
        /// </summary>
        /// <param name="pTrigger">The trigger to add.</param>
        /// <returns>True if succeeded, false otherwise.</returns>
        public bool AddTrigger(StyleTrigger pTrigger)
        {
            if ( pTrigger == null ||
                 pTrigger.Property == null )
            {
                return false;
            }

            // Overwrite if already in.
            this.mTriggers[ pTrigger.Id ] = pTrigger;
            return true;
        }

        /// <summary>
        /// Adds a trigger.
        /// </summary>
        /// <param name="pProperty">The property the trigger is for.</param>
        /// <param name="pValue">The property value to set.</param>
        public bool AddTrigger(PropertyInfo pProperty, object pValue)
        {
            return this.AddTrigger( new StyleTrigger( pProperty, pValue ) );
        }

        /// <summary>
        /// Removes a trigger
        /// </summary>
        /// <param name="pTrigger">The trigger to remove.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveTrigger(StyleTrigger pTrigger)
        {
            return this.RemoveTrigger( pTrigger.Id );
        }

        /// <summary>
        /// Removes a trigger
        /// </summary>
        /// <param name="pPropertyName">The property the trigger is for.</param>
        /// <returns>True if removed, false otherwise.</returns>
        public bool RemoveTrigger(string pPropertyName)
        {
            return this.mTriggers.Remove( pPropertyName );
        }

        #endregion Methods
    }
}
