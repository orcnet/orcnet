﻿using System;
using System.Diagnostics;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Rectangle"/> class.
    /// </summary>
    public struct Rectangle : IEquatable<Rectangle>
    {
        #region Fields

        /// <summary>
        /// Stores the rectangle type.
        /// </summary>
        internal static Type TRectangle = typeof(Rectangle);

        /// <summary>
        /// Stores the constant Empty rectangle.
        /// </summary>
        private readonly static Rectangle sEmpty = CreateEmptyRect();

        /// <summary>
        /// Stores the X position component
        /// </summary>
        private int mX;

        /// <summary>
        /// Stores the Y position component
        /// </summary>
        private int mY;

        /// <summary>
        /// Stores the width.
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the height.
        /// </summary>
        private int mHeight;

        #endregion Fields

        #region Properties

        /// <summary> 
        /// Gets an Empty rectangle.
        /// X and Y are positive-infinity and Width and Height are negative infinity.
        /// This is the only situation where Width or Height can be negative.
        /// </summary> 
        public static Rectangle Empty
        {
            get
            {
                return sEmpty;
            }
        }

        /// <summary>
        /// Returns true if this Rectangle is the Empty rectangle. 
        /// Note: If pWidth or pHeight are 0 this Rectangle still contains a 0 or 1 dimensional set
        /// of points, so this method should not be used to check for 0 area.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                // The funny pWidth and pHeight tests are to handle NaNs
                Debug.Assert((!(this.mWidth < 0) && !(this.mHeight < 0)) || (this == Empty));

                return this.mWidth < 0;
            }
        }

        /// <summary>
        /// Gets or sets the X position component
        /// </summary>
        public int X
        {
            get
            {
                return this.mX;
            }
            set
            {
                this.mX = value;
            }
        }

        /// <summary>
        /// Gets or sets the Y position component
        /// </summary>
        public int Y
        {
            get
            {
                return this.mY;
            }
            set
            {
                this.mY = value;
            }
        }

        /// <summary>
        /// Gets or sets rectangle left component.
        /// </summary>
        public int Left
        {
            get
            {
                return this.X;
            }
            set
            {
                this.X = value;
            }
        }

        /// <summary>
        /// Gets or sets rectangle top component.
        /// </summary>
        public int Top
        {
            get
            {
                return this.Y;
            }
            set
            {
                this.Y = value;
            }
        }
        
        /// <summary>
        /// Gets or sets width.
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
            set
            {
                this.mWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets height.
        /// </summary>
        public int Height
        {
            get
            {
                return this.mHeight;
            }
            set
            {
                this.mHeight = value;
            }
        }

        /// <summary>
        /// Gets or sets the rectangle right component.
        /// </summary>
        public int Right
        {
            get
            {
                return this.mX + this.mWidth;
            }
        }

        /// <summary>
        /// Gets or sets the rectangle bottom component.
        /// </summary>
        public int Bottom
        {
            get
            {
                return this.mY + this.mHeight;
            }
        }

        /// <summary>
        /// Gets or sets the rectangle size.
        /// </summary>
        public Size Size
        {
            get
            {
                return new Size( this.Width, this.Height );
            }
            set
            {
                this.Width  = value.Width;
                this.Height = value.Height;
            }
        }

        /// <summary>
        /// Gets or sets the rectangle position.
        /// </summary>
        public Point Position
        {
            get
            {
                return new Point( this.X, this.Y );
            }
            set
            {
                this.X = value.X;
                this.Y = value.Y;
            }
        }

        /// <summary>
        /// Gets or sets the rectangle top left point.
        /// </summary>
        public Point TopLeft
        {
            get
            {
                return new Point( this.X, this.Y );
            }
            set
            {
                this.X = value.X;
                this.Y = value.Y;
            }
        }

        /// <summary>
        /// Gets or sets the rectangle top right point.
        /// </summary>
        public Point TopRight
        {
            get
            {
                return new Point( this.Right, this.Y );
            }
        }

        /// <summary>
        /// Gets or sets the rectangle bottom left point.
        /// </summary>
        public Point BottomLeft
        {
            get
            {
                return new Point( this.X, this.Bottom );
            }
        }

        /// <summary>
        /// Gets or sets the rectangle bottom right point.
        /// </summary>
        public Point BottomRight
        {
            get
            {
                return new Point( this.Right, this.Bottom );
            }
        }

        /// <summary>
        /// Gets or sets the rectangle center point.
        /// </summary>
        public Point Center
        {
            get
            {
                return new Point( this.Left + this.Width / 2, this.Top + this.Height / 2 );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle"/> class.
        /// </summary>
        /// <param name="pPoint">The position</param>
        /// <param name="pSize">The size.</param>
        public Rectangle(Point pPoint, Size pSize) :
        this( pPoint.X, pPoint.Y, pSize.Width, pSize.Height )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle"/> class.
        /// </summary>
        /// <param name="pSize">The size</param>
        public Rectangle(Size pSize) :
        this( 0, 0, pSize.Width, pSize.Height )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle"/> class.
        /// </summary>
        /// <param name="pX">The X position component</param>
        /// <param name="pY">The Y position component</param>
        /// <param name="pWidth">The width</param>
        /// <param name="pHeight">The height</param>
        public Rectangle(int pX, int pY, int pWidth, int pHeight)
        {
            this.mX = pX;
            this.mY = pY;
            this.mWidth  = pWidth;
            this.mHeight = pHeight;
        }

        #endregion Constructor

        #region Methods

        #region Methods Operators

        /// <summary>
        /// Turns a system Rectangle into an OrcNet Rectangle.
        /// </summary>
        /// <param name="pRectangle"></param>
        public static implicit operator Rectangle(System.Drawing.Rectangle pRectangle)
        {
            return new Rectangle( pRectangle.X, pRectangle.Y, pRectangle.Width, pRectangle.Height );
        }

        /// <summary>
        /// Turns an OrcNet Rectangle into a system Rectangle.
        /// </summary>
        /// <param name="pRectangle"></param>
        public static implicit operator System.Drawing.Rectangle(Rectangle pRectangle)
        {
            return new System.Drawing.Rectangle( pRectangle.X, pRectangle.Y, pRectangle.Width, pRectangle.Height );
        }

        /// <summary>
        /// Adds the two rectangles together.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns></returns>
        public static Rectangle operator +(Rectangle pFirst, Rectangle pSecond)
        {
            int lX  = Math.Min(pFirst.X, pSecond.X);
            int lY  = Math.Min(pFirst.Y, pSecond.Y);
            int lRight  = Math.Max(pFirst.Right, pSecond.Right);
            int lBottom = Math.Max(pFirst.Bottom, pSecond.Bottom);
            return new Rectangle( lX, lY, lRight - lX, lBottom - lY );
        }

        /// <summary>
        /// Adds the given point into the rectangle.
        /// </summary>
        /// <param name="pRectangle"></param>
        /// <param name="pPoint"></param>
        /// <returns></returns>
        public static Rectangle operator +(Rectangle pRectangle, Point pPoint)
        {
            return new Rectangle( pRectangle.X + pPoint.X, pRectangle.Y + pPoint.Y, pRectangle.Width, pRectangle.Height );
        }

        /// <summary>
        /// Subtracts the given point from the rectangle.
        /// </summary>
        /// <param name="pRectangle"></param>
        /// <param name="pPoint"></param>
        /// <returns></returns>
        public static Rectangle operator -(Rectangle pRectangle, Point pPoint)
        {
            return new Rectangle( pRectangle.X - pPoint.X, pRectangle.Y - pPoint.Y, pRectangle.Width, pRectangle.Height );
        }

        /// <summary>
        /// Checks whether the two rectangles are equal or not.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns></returns>
        public static bool operator ==(Rectangle pFirst, Rectangle pSecond)
        {
            return pFirst.TopLeft == pSecond.TopLeft && pFirst.Size == pSecond.Size ? true : false;
        }

        /// <summary>
        /// Checks whether the two rectangles are different or not.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns></returns>
        public static bool operator !=(Rectangle pFirst, Rectangle pSecond)
        {
            return pFirst.TopLeft == pSecond.TopLeft && pFirst.Size == pSecond.Size ? false : true;
        }

        #endregion Methods Operators

        /// <summary>
        /// Inflates the rectangle.
        /// </summary>
        /// <param name="pXDelta"></param>
        /// <param name="pYDelta"></param>
        public void Inflate(int pXDelta, int pYDelta)
        {
            this.X -= pXDelta;
            this.Width += 2 * pXDelta;
            this.Y -= pYDelta;
            this.Height += 2 * pYDelta;
        }

        /// <summary>
        /// Homogeneously inflates the rectangle.
        /// </summary>
        /// <param name="pDelta"></param>
        public void Inflate(int pDelta)
        {
            this.Inflate( pDelta, pDelta );
        }

        /// <summary>
        /// Checks whether the given point is in the rectangle or not.
        /// </summary>
        /// <param name="pPoint"></param>
        /// <returns></returns>
        public bool ContainsOrIsEqual(Point pPoint)
        {
            return (pPoint.X >= X && pPoint.X <= X + Width && pPoint.Y >= Y && pPoint.Y <= Y + Height) ?
                true : false;
        }

        /// <summary>
        /// Checks whether the given rectangle is contained or equal
        /// </summary>
        /// <param name="pRectangle"></param>
        /// <returns></returns>
        public bool ContainsOrIsEqual(Rectangle pRectangle)
        {
            return pRectangle.TopLeft >= this.TopLeft &&
                   pRectangle.BottomRight <= this.BottomRight ? true : false;
        }

        /// <summary>
        /// Checks whether the given rectangle intersects this one or not.
        /// </summary>
        /// <param name="pRectangle"></param>
        /// <returns></returns>
        public bool Intersect(Rectangle pRectangle)
        {
            int lMaxLeft   = Math.Max( this.Left, pRectangle.Left );
            int lMinRight  = Math.Min( this.Right, pRectangle.Right );
            int lMaxTop    = Math.Max( this.Top, pRectangle.Top );
            int lMinBottom = Math.Min( this.Bottom, pRectangle.Bottom );

            return (lMaxLeft < lMinRight) && (lMaxTop < lMinBottom) ?
                true : false;
        }

        /// <summary>
        /// Produces the resulting rectangle of this one and the given one.
        /// </summary>
        /// <param name="pRectangle"></param>
        /// <returns></returns>
        public Rectangle Intersection(Rectangle pRectangle)
        {
            Rectangle lResult = new Rectangle();

            if (pRectangle.Left >= this.Left)
            {
                lResult.Left = pRectangle.Left;
            }
            else
            {
                lResult.TopLeft = this.TopLeft;
            }

            if (pRectangle.Right >= this.Right)
            {
                lResult.Width = this.Right - lResult.Left;
            }
            else
            {
                lResult.Width = pRectangle.Right - lResult.Left;
            }

            if (pRectangle.Top >= this.Top)
            {
                lResult.Top = pRectangle.Top;
            }
            else
            {
                lResult.Top = this.Top;
            }

            if (pRectangle.Bottom >= this.Bottom)
            {
                lResult.Height = this.Bottom - lResult.Top;
            }
            else
            {
                lResult.Height = pRectangle.Bottom - lResult.Top;
            }

            return lResult;
        }

        /// <summary>
        /// Turns the rectangle into a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", X, Y, Width, Height);
        }

        /// <summary>
        /// Parses the given string to build a rectangle.
        /// </summary>
        /// <param name="pString"></param>
        /// <returns></returns>
        public static Rectangle Parse(string pString)
        {
            string[] lValues = pString.Split(new char[] { ',' });
            return new Rectangle( int.Parse(lValues[0]),
                                  int.Parse(lValues[1]),
                                  int.Parse(lValues[2]),
                                  int.Parse(lValues[3]));
        }

        /// <summary>
        /// Gets the hash code.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int lHash = 17;
                // Suitable nullity checks etc, of course :)
                lHash = lHash * 23 + this.mX.GetHashCode();
                lHash = lHash * 23 + this.mY.GetHashCode();
                lHash = lHash * 23 + this.mWidth.GetHashCode();
                lHash = lHash * 23 + this.mHeight.GetHashCode();

                return lHash;
            }
        }

        /// <summary>
        /// Checks whether the rectangle is equal to another or not.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns></returns>
        public override bool Equals(object pOther)
        {
            if ( pOther is Rectangle )
            {
                return this.Equals( (Rectangle)pOther );
            }

            return false;
        }

        /// <summary>
        /// Checks whether the rectangle is equal to another or not.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns></returns>
        public bool Equals(Rectangle pOther)
        {
            return this == pOther;
        }

        /// <summary>
        /// Creates an empty rectangle.
        /// </summary>
        /// <returns>The new rectangle.</returns>
        static private Rectangle CreateEmptyRect()
        {
            Rectangle lRectangle = new Rectangle();
            // We can't set these via the property setters because negatives widths 
            // are rejected in those APIs.
            lRectangle.X = 0;
            lRectangle.Y = 0;
            lRectangle.Width = 0;
            lRectangle.Height = 0;
            return lRectangle;
        }

        #endregion Methods
    }
}
