﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Xml.Serialization;
using Cairo;
using System.Globalization;
using System.Xml.Linq;
using System.Diagnostics;
using OrcNet.UI.Input;

namespace OrcNet.UI
{
    /// <summary>
    /// The PresentationSource Class is the top container of the application.
    /// It provides the Dirty bitmap and zone of the interface to be drawn on screen
    ///
    /// The Interface contains :
    /// 	- rendering and layouting queues and logic.
    /// 	- helpers to load XML interfaces files
    /// 	- global constants and variables of CROW
    /// 	- Keyboard and Mouse logic
    /// 	- the resulting bitmap of the interface
    /// </summary>
    public class PresentationSource : ADispatcherObject
    {
        #region Statics

        /// <summary>
        /// Global font rendering settings for Cairo
        /// </summary>
        public static FontOptions FontRenderingOptions;

        /// <summary>
        /// Global font rendering settings for Cairo
        /// </summary>
        public static Antialias Antialias = Antialias.Subpixel;

        /// <summary>
        /// Each control need a ref to the root interface containing it, if not set in GraphicObject.currentInterface,
        /// the ref of this one will be stored in GraphicObject.currentInterface
        /// </summary>
        internal static PresentationSource CurrentInterface;

        #endregion Statics

        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the source is resizing itself or not.
        /// </summary>
        private bool mIsInternalUpdate = false;

        /// <summary>
        /// Stores the 
        /// </summary>
        private SizeToContent mSizeToContent;

        /// <summary>
        /// Stores the flag indicating whether the window is minimized or not.
        /// </summary>
        private bool mIsWindowInMinimizeState = false;

        /// <summary>
        /// Stores the native window handle.
        /// </summary>
        private NativeWindowWrapper mNative;

        /// <summary>
        /// Stores the presentation target.
        /// </summary>
        private PresentationTarget mPresentationTarget;

        /// <summary>
        /// Stores the root visual.
        /// </summary>
        private AVisual mRootVisual;

        /// <summary>
        /// Stores the previous root size.
        /// </summary>
        private Size? mPreviousSize;

        #endregion Fields

        #region Events
        
        /// <summary>
        /// Event fired on root changes.
        /// </summary>
        public event EventHandler<PropertyValueChangedEventArgs> RootChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the window must be auto sized to content or not.
        /// </summary>
        public SizeToContent SizeToContent
        {
            get
            {
                return this.mSizeToContent;
            }
            set
            {
                if ( this.IsDisposed )
                {
                    return;
                }
                
                if ( this.mSizeToContent == value )
                {
                    return;
                }

                this.mSizeToContent = value;
                
                if ( this.IsLayoutActive() )
                {
                    this.SetLayoutSize();
                }
            }
        }

        /// <summary>
        /// Gets the window wrapper.
        /// </summary>
        internal NativeWindowWrapper WindowWrapper
        {
            get
            {
                return this.mNative;
            }
        }

        /// <summary>
        /// Gets the presentation target.
        /// </summary>
        public PresentationTarget CompositionTarget
        {
            get
            {
                if ( this.IsDisposed )
                {
                    return null;
                }

                if ( this.mPresentationTarget != null &&
                     this.mPresentationTarget.IsDisposed )
                {
                    return null;
                }

                return this.mPresentationTarget;
            }
        }

        /// <summary>
        /// Gets the source style flags.
        /// </summary>
        internal int SourceStyle
        {
            get
            {
                return this.mNative.StyleFlags;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the presentation source has currently the focus or not.
        /// </summary>
        private bool HasFocus
        {
            get
            {
                return this.mNative.HasFocus;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the source window is active or not.
        /// </summary>
        internal bool IsActive
        {
            get
            {
                return this.mNative.IsActive;
            }
        }

        /// <summary>
        /// Checks whether this source is usable or not.
        /// </summary>
        private bool IsUsable
        {
            get
            {
                return this.IsDisposed == false &&
                       this.mPresentationTarget != null &&
                       this.mPresentationTarget.IsDisposed == false;
            }
        }

        /// <summary>
        /// Gets or sets the root visual.
        /// </summary>
        public AVisual RootVisual
        {
            get
            {
                if ( this.IsDisposed )
                {
                    return null;
                }

                return this.mRootVisual;
            }
            set
            {
                if ( this.IsDisposed )
                {
                    return;
                }

                this.RootVisualInternal = value;
            }
        }

        /// <summary>
        /// Assigns the root visual.
        /// </summary>
        private AVisual RootVisualInternal
        {
            set
            {
                if ( this.mRootVisual == value )
                {
                    return;
                }

                AVisual lOldVisual = this.mRootVisual;
                if ( lOldVisual != null )
                {
                    if ( lOldVisual is UIElement )
                    {
                        (lOldVisual as UIElement).LayoutUpdated -= this.OnLayoutUpdated;
                    }
                }

                this.mRootVisual = value;

                if ( this.mRootVisual != null )
                {
                    if ( this.mRootVisual is UIElement )
                    {
                        (this.mRootVisual as UIElement).LayoutUpdated += this.OnLayoutUpdated;
                    }
                }

                if ( this.mPresentationTarget != null &&
                     this.mPresentationTarget.IsDisposed == false )
                {
                    this.mPresentationTarget.RootVisual = this.mRootVisual;
                }

                this.NotifyRootChanged( lOldVisual, this.mRootVisual );

                if ( this.IsLayoutActive() )
                {
                    this.SetLayoutSize();

                    // Trigger a rendering pass??
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="PresentationSource"/> class.
        /// </summary>
        static PresentationSource()
        {
            loadStyling();
            findAvailableTemplates();

            FontRenderingOptions = new FontOptions();
            FontRenderingOptions.Antialias = Antialias.Subpixel;
            FontRenderingOptions.HintMetrics = HintMetrics.On;
            FontRenderingOptions.HintStyle = HintStyle.Medium;
            FontRenderingOptions.SubpixelOrder = SubpixelOrder.Rgb;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PresentationSource"/> class.
        /// </summary>
        /// <param name="pParameters">The parameters.</param>
		public PresentationSource(NativeWindowParameters pParameters)
        {
            CurrentInterface = this;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;

            this.Initialize( pParameters );
        }

        #endregion Constructor
        
        #region Public Fields
                              
        /// <summary>
        /// each IML and fragments (such as inline Templates) are compiled as a Dynamic Method stored here
        /// on the first instance creation of a IML item.
        /// </summary>
        public static Dictionary<String, Instantiator> Instantiators = new Dictionary<string, Instantiator>();

        #endregion
        
        #region Default values and Style loading
        /// Default values of properties from GraphicObjects are retrieve from XML Attributes.
        /// The reflexion process used to retrieve those values being very slow, it is compiled in MSIL
        /// and injected as a dynamic method referenced in the DefaultValuesLoader Dictionnary.
        /// The compilation is done on the first object instancing, and is also done for custom widgets
        public delegate void LoaderInvoker(object instance);
        /// <summary>Store one loader per StyleKey</summary>
        public static Dictionary<String, LoaderInvoker> DefaultValuesLoader = new Dictionary<string, LoaderInvoker>();
        /// <summary>Store dictionnary of member/value per StyleKey</summary>
        public static Dictionary<string, Style> Styling;
        /// <summary> parse all styling data's during application startup and build global Styling Dictionary </summary>
        static void loadStyling()
        {
            Styling = new Dictionary<string, Style>();

            //fetch styling info in this order, if member styling is alreadey referenced in previous
            //assembly, it's ignored.
            loadStylingFromAssembly(Assembly.GetEntryAssembly());
            loadStylingFromAssembly(Assembly.GetExecutingAssembly());
        }
        /// <summary> Search for .style resources in assembly </summary>
        static void loadStylingFromAssembly(Assembly assembly)
        {
            foreach (string s in assembly
                .GetManifestResourceNames()
                .Where(r => r.EndsWith(".style", StringComparison.OrdinalIgnoreCase)))
            {
                new StyleReader(assembly, s)
                    .Dispose();
            }
        }
        
        #endregion

        #region Templates
        /// <summary>Store one default templates resource ID per class.
        /// Resource ID must be 'fullClassName.template' (not case sensitive)
        /// Those found in application assembly have priority to the default Crow's one
        /// </summary>
        public static Dictionary<string, string> DefaultTemplates = new Dictionary<string, string>();
        /// <summary>Finds available default templates at startup</summary>
        static void findAvailableTemplates()
        {
            searchTemplatesIn(Assembly.GetEntryAssembly());
            searchTemplatesIn(Assembly.GetExecutingAssembly());
        }
        static void searchTemplatesIn(Assembly assembly)
        {
            foreach (string resId in assembly
                .GetManifestResourceNames()
                .Where(r => r.EndsWith(".template", StringComparison.OrdinalIgnoreCase)))
            {
                string clsName = resId.Substring(0, resId.Length - 9);
                if (DefaultTemplates.ContainsKey(clsName))
                    continue;
                DefaultTemplates[clsName] = "#" + resId;
            }
        }
        #endregion

        #region Load/Save
        
        /// <summary>Create an instance of a GraphicObject and add it to the GraphicTree
        /// of this Interface</summary>
        public GraphicObject LoadInterface(string path)
        {
            lock (UpdateMutex)
            {
                GraphicObject tmp = Load(path);
                AddWidget(tmp);

                return tmp;
            }
        }
        /// <summary>Create an instance of a GraphicObject linked to this interface but
        /// not added to the GraphicTree</summary>
        public GraphicObject Load(string path)
        {
            try
            {
                return GetInstantiator(path).CreateInstance(this);
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading <" + path + ">:", ex);
            }
        }
        /// <summary>Fetch it from cache or create it</summary>
        public static Instantiator GetInstantiator(string path)
        {
            if (!Instantiators.ContainsKey(path))
                Instantiators[path] = new Instantiator(path);
            return Instantiators[path];
        }
        /// <summary>Item templates have additional properties for recursivity and
        /// custom display per item type</summary>
        public static ItemTemplate GetItemTemplate(string path)
        {
            if (!Instantiators.ContainsKey(path))
                Instantiators[path] = new ItemTemplate(path);
            return Instantiators[path] as ItemTemplate;
        }
        //TODO: .Net xml serialisation is no longer used, it has been replaced with instantiators
        public static void Save<T>(string file, T graphicObject)
        {
            XmlSerializerNamespaces xn = new XmlSerializerNamespaces();
            xn.Add("", "");
            XmlSerializer xs = new XmlSerializer(typeof(T));

            xs = new XmlSerializer(typeof(T));
            using (Stream s = new FileStream(file, FileMode.Create))
            {
                xs.Serialize(s, graphicObject, xn);
            }
        }
        #endregion
        
        #region Methods

        /// <summary>
        /// Main Update loop, executed in this interface thread, lock the UpdateMutex
        /// Steps:
        /// 	- Execute Repeat events
        /// 	- Layouting
        /// 	- Clipping
        /// 	- Drawing
        /// Result: the Interface bitmap is drawn in memory (byte[] bmp) and a dirtyRect and bitmap are available
        /// </summary>
        public void Update()
        {
            InputManager.Instance.ProcessRepeatEvents();

            ContextLayoutManager.From( this.Dispatcher ).UpdateLayout();

            // MediaContext.From( this.Dispatcher ).Render();
        }

        /// <summary>
        /// Processes interface resizing.
        /// </summary>
        /// <param name="pBounds"></param>
        public void Resize(Rectangle pBounds)
        {
            try
            {
                this.mIsInternalUpdate = true;
                
                Size lSize = pBounds.Size;
                this.WindowWrapper.SetWindowPosition( 0, 0, 
                                                      lSize.Width, lSize.Height, 
                                                      false, 
                                                      true );
            }
            finally
            {
                this.mIsInternalUpdate = false;
            }
        }

        /// <summary>
        /// Gets the presentation source from the given visual element.
        /// </summary>
        /// <param name="pVisual"></param>
        /// <returns></returns>
        public static PresentationSource FromVisual(AVisual pVisual)
        {
            if ( pVisual == null )
            {
                throw new ArgumentNullException("pVisual");
            }

            PresentationSource lSource = FindSource( pVisual);
            
            if ( lSource != null && 
                 lSource.IsDisposed )
            {
                lSource = null;
            }

            return lSource;
        }

        /// <summary>
        /// Finds the presentation source of the given element.
        /// </summary>
        /// <param name="pVisual"></param>
        /// <returns></returns>
        private static PresentationSource FindSource(AVisual pVisual)
        {
            PresentationSource lSource = null;

            AVisual lRoot = AVisual.GetRootVisual( pVisual );
            if ( lRoot != null )
            {
                lSource = lRoot.PresentationSource;
            }

            return lSource;
        }

        /// <summary>
        /// Checks whether the presentation layout is active or not.
        /// </summary>
        /// <returns>True if so, false otherwise.</returns>
        private bool IsLayoutActive()
        {

            if ( this.mRootVisual is UIElement && 
                 this.mPresentationTarget != null &&
                 this.mPresentationTarget.IsDisposed == false )
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets the layout size.
        /// </summary>
        private void SetLayoutSize()
        {
            Debug.Assert( this.mPresentationTarget != null, "PresentationTarget is null" );
            Debug.Assert( this.mPresentationTarget.IsDisposed == false, "PresentationTarget is disposed" );

            UIElement lRootUIElement = this.mRootVisual as UIElement;
            if ( lRootUIElement == null )
            {
                return;
            }

            lRootUIElement.InvalidateMeasure();

            lRootUIElement.Measure();

            lRootUIElement.RegisterForLayouting( LayoutingType.All );

            lRootUIElement.UpdateLayout();
        }

        /// <summary>
        /// Initializes the 
        /// </summary>
        /// <param name="pParameters"></param>
        private void Initialize(NativeWindowParameters pParameters)
        {
            // Create the window.
            this.mNative = new NativeWindowWrapper( pParameters );
            this.mNative.NativeSizeChanged += this.OnNativeWindowSizeChanged;

            // TO DO: Pass the window to the target or the context if needed.
            this.mPresentationTarget = new PresentationTarget( this.mNative.Window );


            if ( pParameters.HasAssignedSize == false )
            {
                this.mSizeToContent = SizeToContent.Both;
            }

            // Manages drag and drop between source.
        }

        /// <summary>
        /// Delegate called on window size changes.
        /// </summary>
        /// <param name="pWindow"></param>
        /// <param name="pNewSize"></param>
        private void OnNativeWindowSizeChanged(INativeWindow pWindow, Size pNewSize)
        {
            UIElement lRootUIElement = this.mRootVisual as UIElement;
            if ( lRootUIElement == null )
            {
                return;
            }

            this.mIsWindowInMinimizeState = pWindow.IsMinimized;

            if ( this.mIsInternalUpdate == false &&
                 this.mSizeToContent != SizeToContent.Both &&
                 this.mIsWindowInMinimizeState == false )
            {
                lRootUIElement.Measure();

                lRootUIElement.RegisterForLayouting( LayoutingType.All );

                lRootUIElement.UpdateLayout();
            }

            if ( this.CompositionTarget != null )
            {
                this.CompositionTarget.OnResize();
            }
        }

        /// <summary>
        /// Delegate called on root layout updates.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnLayoutUpdated(object pSender, EventArgs pEventArgs)
        {
            UIElement lRoot = this.mRootVisual as UIElement;
            if ( lRoot != null )
            {
                Size lNewSize = lRoot.Slot.Size;
                if ( this.mPreviousSize == null || 
                     DoubleUtil.AreClose( this.mPreviousSize.Value.Width, lNewSize.Width ) == false || 
                     DoubleUtil.AreClose( this.mPreviousSize.Value.Height, lNewSize.Height ) == false )
                {
                    this.mPreviousSize = lNewSize;
                    
                    // Auto resize if no manual resizing.
                    if ( this.mSizeToContent != SizeToContent.Manual &&
                         this.mIsWindowInMinimizeState == false )
                    {
                        this.Resize( lNewSize );
                    }
                }
            }
        }
        
        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDispose()
        {
            this.RootVisualInternal = null;

            if ( this.mPresentationTarget != null )
            {
                this.mPresentationTarget.Dispose();
                this.mPresentationTarget = null;
            }

            if ( this.mNative != null )
            {
                // Remove any drag and drop stuff.

                this.mNative.NativeSizeChanged -= this.OnNativeWindowSizeChanged;

                this.mNative.Dispose();
                this.mNative = null;
            }

            base.OnDispose();
        }

        /// <summary>
        /// Notifies the root has changed.
        /// </summary>
        /// <param name="pOldRoot"></param>
        /// <param name="pNewRoot"></param>
        private void NotifyRootChanged(AVisual pOldRoot, AVisual pNewRoot)
        {
            PresentationSource lOldSource = null;

            if ( pOldRoot == pNewRoot )
            {
                return;
            }

            if ( pOldRoot != null )
            {
                lOldSource = pOldRoot.PresentationSource;
                pOldRoot.PresentationSource = null;
            }

            if ( pNewRoot != null )
            {
                pNewRoot.PresentationSource = this;
            }

            UIElement lOldRootUIElement = pOldRoot as UIElement;
            UIElement lNewRootUIElement = pNewRoot as UIElement;
            
            if ( lOldRootUIElement != null )
            {
                lOldRootUIElement.OnPresentationSourceChanged( false );
            }

            if ( lNewRootUIElement != null )
            {
                lNewRootUIElement.OnPresentationSourceChanged( true );
            }

            if ( this.RootChanged != null )
            {
                this.RootChanged( this, new PropertyValueChangedEventArgs( pOldRoot, pNewRoot ) );
            }
        }

        #endregion Methods
    }
}

