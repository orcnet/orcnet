﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="UIConstants"/> class.
    /// </summary>
    public static class UIConstants
    {
        #region Methods

        /// <summary>
        /// The constant UI unamed element value.
        /// </summary>
        public const string cNoName = "Unamed";

        #endregion Methods
    }
}
