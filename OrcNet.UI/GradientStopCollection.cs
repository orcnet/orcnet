﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="GradientStopCollection"/> class.
    /// </summary>
    public class GradientStopCollection : ICollection<GradientStop>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the set of gradient stops.
        /// </summary>
        private List<GradientStop> mStops;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the node count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mStops.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is readonly or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the setter having the given property name.
        /// </summary>
        /// <param name="pIndex">The element property name to look for.</param>
        /// <returns>The found setter, null otherwise.</returns>
        public GradientStop this[int pIndex]
        {
            get
            {
                if ( pIndex >= 0 &&
                     pIndex < this.mStops.Count )
                {
                    return this.mStops[ pIndex ];
                }
                
                return null;
            }
            set
            {
                if ( value != null )
                {
                    this.Add( value );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GradientStopCollection"/> class.
        /// </summary>
        public GradientStopCollection()
        {
            this.mStops = new List<GradientStop>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new setter.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(GradientStop pItem)
        {
            this.mStops.Add( pItem );
        }
        
        /// <summary>
        /// Removes a setter.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(GradientStop pItem)
        {
            return this.mStops.Remove( pItem );
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            this.mStops.Clear();
        }

        /// <summary>
        /// Checks whether the collection contains the given setter or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(GradientStop pItem)
        {
            return this.mStops.Contains( pItem );
        }

        /// <summary>
        /// Copies the setters into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(GradientStop[] pArray, int pArrayIndex)
        {
            this.mStops.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Gets the setters enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<GradientStop> GetEnumerator()
        {
            return this.mStops.GetEnumerator();
        }
        
        /// <summary>
        /// Gets the setters enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.Clear();
        }

        #endregion Methods
    }
}
