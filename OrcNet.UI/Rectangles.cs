﻿using System.Collections.Generic;
using System.Text;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Rectangles"/> class.
    /// </summary>
    public class Rectangles
    {
        #region Fields

        /// <summary>
        /// Stores the bounds encapsulating all rectangles.
        /// </summary>
        private Rectangle mBounds;

        /// <summary>
        /// Stores the flag indicating whether the bounds are dirty or not.
        /// </summary>
        private bool mIsDirty = true;

        /// <summary>
        /// Stores the set of rectangles.
        /// </summary>
        private List<Rectangle> mRectangles = new List<Rectangle>();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the rectangle count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mRectangles.Count;
            }
        }

        /// <summary>
        /// Gets the set of rectangles.
        /// </summary>
        public IEnumerable<Rectangle> RectangleList
        {
            get
            {
                return this.mRectangles;
            }
        }

        /// <summary>
        /// Gets the bounds.
        /// </summary>
        public Rectangle Bounds
        {
            get
            {
                if ( this.mIsDirty == false )
                {
                    if ( this.mRectangles.Count > 0 )
                    {
                        this.mBounds = this.mRectangles[ 0 ];
                        for ( int lCurr = 1; lCurr < mRectangles.Count; lCurr++ )
                        {
                            this.mBounds += this.mRectangles[ lCurr ];
                        }
                    }
                    else
                    {
                        this.mBounds = Rectangle.Empty;
                    }

                    this.mIsDirty = true;
                }

                return this.mBounds;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new rectangle.
        /// </summary>
        /// <param name="pNewRectangle"></param>
        public void AddRectangle(Rectangle pNewRectangle)
        {
			if ( this.DoesNotContain( pNewRectangle ) )
            {
				this.mRectangles.Add( pNewRectangle );
				this.mIsDirty = false;
			}
        }

        /// <summary>
        /// Reset the set of rectangles.
        /// </summary>
        public void Reset()
        {
            this.mRectangles = new List<Rectangle>();
			this.mBounds  = Rectangle.Empty;
			this.mIsDirty = true;
        }

        /// <summary>
        /// Checks whether the given rectangle is not in this set already or not.
        /// </summary>
        /// <param name="pToCheck">The rectangle to check.</param>
        /// <returns>True if not in yet, false otherwise.</returns>
        private bool DoesNotContain(Rectangle pToCheck)
        {
            foreach ( Rectangle rInList in this.mRectangles)
            {
                if ( rInList.ContainsOrIsEqual( pToCheck ) )
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks whether the given rectangle intersect this set or not.
        /// </summary>
        /// <param name="pToCheck"></param>
        /// <returns></returns>
        public bool Intersect(Rectangle pToCheck)
        {
            foreach ( Rectangle rInList in this.mRectangles )
            {
                if ( rInList.Intersect( pToCheck ) )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Stokes the region determined by this set of rectangles.
        /// </summary>
        /// <param name="pContext">The drawing context.</param>
        /// <param name="pColor">The stroke color</param>
        public void Stroke(Context pContext, Color pColor)
		{
            foreach (Rectangle lRectangle in mRectangles)
            {
                pContext.Rectangle( lRectangle );
            }

            pContext.SetSourceColor( pColor );

			pContext.LineWidth = 2;
            pContext.Stroke();
		}

        /// <summary>
        /// Clear and clip the region determined by this set of rectangles.
        /// </summary>
        /// <param name="pContext">The drawing context.</param>
        public void ClearAndClip(Context pContext)
        {
			if ( this.mRectangles.Count == 0 )
            {
                return;
            }

            foreach ( Rectangle lRectangle in this.mRectangles )
            {
                pContext.Rectangle( lRectangle );
            }

			pContext.ClipPreserve();
			pContext.Operator = Operator.Clear;
            pContext.Fill();
            pContext.Operator = Operator.Over;
        }

        /// <summary>
        /// Clips the region determined by this set of rectangles.
        /// </summary>
        /// <param name="pContext">The drawing context.</param>
        public void Clip(Context pContext)
        {
            foreach ( Rectangle lRectangle in this.mRectangles )
            {
                pContext.Rectangle( lRectangle );
            }

            pContext.Clip();
        }

        /// <summary>
        /// Clears the region determined by this set of rectangles.
        /// </summary>
        /// <param name="pContext">The drawing context.</param>
        public void Clear(Context pContext)
        {
            foreach ( Rectangle lRectangle in this.mRectangles )
            {
                pContext.Rectangle( lRectangle );
            }

            pContext.Operator = Operator.Clear;
            pContext.Fill();
            pContext.Operator = Operator.Over;
        }

        /// <summary>
        /// Turns this rectangle into a string.
        /// </summary>
        /// <returns>The rectangle as string.</returns>
		public override string ToString()
		{
            StringBuilder lResult = new StringBuilder();
			foreach ( Rectangle lRectangle in mRectangles )
            {
                lResult.Append( lRectangle.ToString() );
			}

			return lResult.ToString();
		}

        #endregion Methods
    }
}
