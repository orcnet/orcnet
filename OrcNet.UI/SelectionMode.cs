﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="SelectionMode"/> enumeration.
    /// </summary>
    public enum SelectionMode
    {
        /// <summary>
        /// Single selection.
        /// </summary>
        Single = 0,

        /// <summary>
        /// Multiple selection.
        /// </summary>
        Multiple = 1
    }
}
