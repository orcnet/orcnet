﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="SelectionChangeEventArgs{T}"/> class.
    /// </summary>
	public class SelectionChangeEventArgs<T>: EventArgs
	{
        #region Properties

        /// <summary>
        /// Gets the old selection value.
        /// </summary>
        public T OldValue
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the new selection value.
        /// </summary>
        public T NewValue
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectionChangeEventArgs{T}"/> class.
        /// </summary>
        /// <param name="pNewValue"></param>
        /// <param name="pOldValue"></param>
        public SelectionChangeEventArgs(T pNewValue, T pOldValue) : 
        base()
		{
			this.NewValue = pNewValue;
            this.OldValue = pOldValue;
		}

        #endregion Constructor
    }
}

