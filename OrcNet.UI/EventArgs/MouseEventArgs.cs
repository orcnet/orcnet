﻿using System;
#if !MINIMAL
using System.Drawing;
#endif

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="MouseEventArgs"/> class.
    /// </summary>
    public class MouseEventArgs : EventArgs
    {
	    #region Fields

        /// <summary>
        /// Stores the mouse mState.
        /// </summary>
	    private MouseState mState;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the X position of the mouse for the event.
        /// </summary>
        public int X
        {
            get
            {
                return this.mState.X;
            }
            internal set
            {
                this.mState.X = value;
            }
        }

        /// <summary>
        /// Gets the Y position of the mouse for the event.
        /// </summary>
        public int Y
        {
            get
            {
                return this.mState.Y;
            }
            internal set
            {
                this.mState.Y = value;
            }
        }

        /// <summary>
        /// Gets a <see cref="System.Drawing.Point"/> representing the location of the mouse for the event.
        /// </summary>
        public Point Position
        {
            get
            {
                return new Point( this.mState.X, this.mState.Y );
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        /// <summary>
        /// Gets the current <see cref="OpenTK.Input.MouseState"/>.
        /// </summary>
        public MouseState Mouse
        {
            get
            {
                return this.mState;
            }
            set
            {
                this.mState = value;
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        public MouseEventArgs()
	    {
	        this.mState.SetIsConnected( true );
	    }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        /// <param name="pX">The X position.</param>
        /// <param name="pY">The Y position.</param>
        public MouseEventArgs(int pX, int pY) : 
        this()
	    {
	        this.mState.X = pX;
	        this.mState.Y = pY;
	    }

	    /// <summary>
	    /// Constructs a new instance.
	    /// </summary>
	    /// <param name="pEventArgs">The <see cref="MouseEventArgs"/> instance to clone.</param>
	    public MouseEventArgs(MouseEventArgs pEventArgs) : 
        this( pEventArgs.X, pEventArgs.Y )
	    {

	    }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Set the given button state.
        /// </summary>
        /// <param name="pButton">The button to set the state for.</param>
        /// <param name="pState">The new button state.</param>
        internal void SetButton(MouseButton pButton, ButtonState pState)
	    {
	        if ( pButton < 0 || 
                 pButton > MouseButton.LastButton )
            {
                throw new ArgumentOutOfRangeException();
            }

	        switch ( pState )
	        {
		    case ButtonState.Pressed:
		        this.mState.EnableBit( (int)pButton );
		        break;

		    case ButtonState.Released:
		        this.mState.DisableBit( (int)pButton );
		        break;
	        }
	    }

        /// <summary>
        /// Gets the given button state.
        /// </summary>
        /// <param name="pButton">The button to get the state of.</param>
        /// <returns>The button state.</returns>
	    internal ButtonState GetButton(MouseButton pButton)
	    {
	        if ( pButton < 0 || 
                 pButton > MouseButton.LastButton )
            {
                throw new ArgumentOutOfRangeException();
            }

	        return this.mState.ReadBit( (int)pButton ) ? ButtonState.Pressed : 
                                                         ButtonState.Released;
	    }

        #endregion Methods
    }

    /// <summary>
    /// Definition of the <see cref="MouseMoveEventArgs"/> class.
    /// </summary>
    public class MouseMoveEventArgs : MouseEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the mouse move horizontal delta 
        /// </summary>
        private int mDeltaX;

        /// <summary>
        /// Stores the mouse move vertical delta 
        /// </summary>
        private int mDeltaY;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the change in X position produced by this event.
        /// </summary>
        public int XDelta
        {
            get
            {
                return this.mDeltaX;
            }
            internal set
            {
                this.mDeltaX = value;
            }
        }

        /// <summary>
        /// Gets the change in Y position produced by this event.
        /// </summary>
        public int YDelta
        {
            get
            {
                return this.mDeltaY;
            }
            internal set
            {
                this.mDeltaY = value;
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseMoveEventArgs"/> class.
        /// </summary>
        public MouseMoveEventArgs()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseMoveEventArgs"/> class.
        /// </summary>
        /// <param name="pX">The X position.</param>
        /// <param name="pY">The Y position.</param>
        /// <param name="pDeltaX">The change in X position produced by this event.</param>
        /// <param name="pDeltaY">The change in Y position produced by this event.</param>
        public MouseMoveEventArgs(int pX, int pY, int pDeltaX, int pDeltaY) : 
        base( pX, pY )
	    {
	        this.mDeltaX = pDeltaX;
	        this.mDeltaY = pDeltaY;
	    }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseMoveEventArgs"/> class.
        /// </summary>
        /// <param name="pEventArgs">The <see cref="MouseMoveEventArgs"/> instance to clone.</param>
        public MouseMoveEventArgs(MouseMoveEventArgs pEventArgs) : 
        this( pEventArgs.X, pEventArgs.Y, pEventArgs.mDeltaX, pEventArgs.mDeltaY )
	    {

	    }

        #endregion Constructors
    }

    /// <summary>
    /// Definition of the <see cref="MouseButtonEventArgs"/> class.
    /// </summary>
    public class MouseButtonEventArgs : MouseEventArgs
    {
	    #region Fields

        /// <summary>
        /// Stores the mouse Button involved.
        /// </summary>
	    private MouseButton mButton;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the <see cref="MouseButton"/> that triggered this event.
        /// </summary>
        public MouseButton Button
        {
            get
            {
                return this.mButton;
            }
            internal set
            {
                this.mButton = value;
            }
        }

        /// <summary>
        /// Gets a System.Boolean representing the state of the mouse mButton for the event.
        /// </summary>
        public bool IsPressed
        {
            get
            {
                return GetButton( Button ) == ButtonState.Pressed;
            }
            internal set
            {
                SetButton( Button, value ? ButtonState.Pressed : ButtonState.Released );
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseButtonEventArgs"/> class.
        /// </summary>
        public MouseButtonEventArgs()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseButtonEventArgs"/> class.
        /// </summary>
        /// <param name="pX">The X position.</param>
        /// <param name="pY">The Y position.</param>
        /// <param name="pButton">The mouse mButton for the event.</param>
        /// <param name="pIsPressed">The current state of the mButton.</param>
        public MouseButtonEventArgs(int pX, int pY, MouseButton pButton, bool pIsPressed) : 
        base( pX, pY )
	    {
	        this.mButton = pButton;
	        this.IsPressed = pIsPressed;
	    }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseButtonEventArgs"/> class.
        /// </summary>
        /// <param name="pEventArgs">The <see cref="MouseButtonEventArgs"/> instance to clone.</param>
        public MouseButtonEventArgs(MouseButtonEventArgs pEventArgs) : 
        this( pEventArgs.X, pEventArgs.Y, pEventArgs.Button, pEventArgs.IsPressed )
	    {

	    }

        #endregion Constructors
    }

    /// <summary>
    /// Definition of the <see cref="MouseWheelEventArgs"/> class.
    /// </summary>
    public class MouseWheelEventArgs : MouseEventArgs
    {
	    #region Fields

        /// <summary>
        /// Stores the mouse wheel delta.
        /// </summary>
	    private float mDelta;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the value of the wheel in integer units.
        /// To support high-precision mice, it is recommended to use <see cref="ValuePrecise"/> instead.
        /// </summary>
        public int Value
        {
            get
            {
                return (int)Math.Round( Mouse.Scroll.Y, MidpointRounding.AwayFromZero);
            }
        }

        /// <summary>
        /// Gets the change in value of the wheel for this event in integer units.
        /// To support high-precision mice, it is recommended to use <see cref="DeltaPrecise"/> instead.
        /// </summary>
        public int Delta
        {
            get
            {
                return (int)Math.Round( this.mDelta, MidpointRounding.AwayFromZero );
            }
        }

        /// <summary>
        /// Gets the precise value of the wheel in floating-point units.
        /// </summary>
        public float ValuePrecise
        {
            get
            {
                return Mouse.Scroll.Y;
            }
        }

        /// <summary>
        /// Gets the precise change in value of the wheel for this event in floating-point units.
        /// </summary>
        public float DeltaPrecise
        {
            get
            {
                return this.mDelta;
            }
            internal set
            {
                this.mDelta = value;
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseWheelEventArgs"/> class.
        /// </summary>
        public MouseWheelEventArgs()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseWheelEventArgs"/> class.
        /// </summary>
        /// <param name="pX">The X position.</param>
        /// <param name="pY">The Y position.</param>
        /// <param name="pValue">The value of the wheel.</param>
        /// <param name="pDelta">The change in value of the wheel for this event.</param>
        public MouseWheelEventArgs(int pX, int pY, int pValue, int pDelta) : 
        base( pX, pY )
	    {
	        Mouse.SetScrollAbsolute( Mouse.Scroll.X, pValue );
	        this.mDelta = pDelta;
	    }

	    /// <summary>
	    /// Constructs a new <see cref="MouseWheelEventArgs"/> instance.
	    /// </summary>
	    /// <param name="pEventArgs">The <see cref="MouseWheelEventArgs"/> instance to clone.</param>
	    public MouseWheelEventArgs(MouseWheelEventArgs pEventArgs) : 
        this( pEventArgs.X, pEventArgs.Y, pEventArgs.Value, pEventArgs.Delta )
	    {

	    }

        #endregion Constructors
    }
}
