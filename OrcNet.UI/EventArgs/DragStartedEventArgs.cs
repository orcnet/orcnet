﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Delegate prototype of a drag started event.
    /// </summary>
    /// <param name="pSender"></param>
    /// <param name="pEventArg"></param>
    public delegate void DragStartedDelegate(object pSender, DragStartedEventArgs pEventArg);

    /// <summary>
    /// Definition of the <see cref="DragStartedEventArgs"/> class.
    /// </summary>
    public class DragStartedEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the horizontal offset
        /// </summary>
        private double mHorizontalOffset;

        /// <summary>
        /// Stores the vertical offset
        /// </summary>
        private double mVerticalOffset;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the horizontal offset
        /// </summary>
        public double HorizontalOffset
        {
            get
            {
                return this.mHorizontalOffset;
            }
        }

        /// <summary>
        /// Gets the vertical offset
        /// </summary>
        public double VerticalOffset
        {
            get
            {
                return this.mVerticalOffset;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DragStartedEventArgs"/> class.
        /// </summary>
        /// <param name="pHorizontalOffset"></param>
        /// <param name="pVerticalOffset"></param>
        public DragStartedEventArgs(double pHorizontalOffset, double pVerticalOffset)
        {
            this.mHorizontalOffset = pHorizontalOffset;
            this.mVerticalOffset = pVerticalOffset;
        }

        #endregion Constructor
    }
}
