﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="MouseCursorChangedEventArgs"/> class.
    /// </summary>
	public class MouseCursorChangedEventArgs : EventArgs
	{
        #region Fields

        /// <summary>
        /// Stores the new cursor value.
        /// </summary>
        private XCursor mNewCursor;

        /// <summary>
        /// Stores the old cursor value.
        /// </summary>
        private XCursor mOldCursor;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the new cursor value.
        /// </summary>
        public XCursor NewCursor
        {
            get
            {
                return this.mNewCursor;
            }
        }

        /// <summary>
        /// Gets the old cursor value.
        /// </summary>
        public XCursor OldCursor
        {
            get
            {
                return this.mOldCursor;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseCursorChangedEventArgs"/> class.
        /// </summary>
        /// <param name="pOldCursor">The old cursor.</param>
        /// <param name="pNewCursor">The new cursor.</param>
        public MouseCursorChangedEventArgs(XCursor pOldCursor, XCursor pNewCursor) : 
        base()
        {
            this.mNewCursor = pNewCursor;
            this.mOldCursor = pOldCursor;
        }

        #endregion Constructor
    }
}
