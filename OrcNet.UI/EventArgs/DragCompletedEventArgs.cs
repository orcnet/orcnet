﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Delegate prototype of a Drag completed event.
    /// </summary>
    /// <param name="pSender"></param>
    /// <param name="pEventArgs"></param>
    public delegate void DragCompletedDelegate(object pSender, DragCompletedEventArgs pEventArgs);

    /// <summary>
    /// Definition of the <see cref="DragCompletedEventArgs"/> class.
    /// </summary>
    public class DragCompletedEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the drag event has been canceled or not.
        /// </summary>
        private bool mIsCanceled;

        /// <summary>
        /// Stores the horizontal change
        /// </summary>
        private double mHorizontalChange;

        /// <summary>
        /// Stores the vertical change
        /// </summary>
        private double mVerticalChange;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the drag event has been canceled or not.
        /// </summary>
        public bool IsCanceled
        {
            get
            {
                return this.mIsCanceled;
            }
        }

        /// <summary>
        /// Gets the horizontal change
        /// </summary>
        public double HorizontalChange
        {
            get
            {
                return this.mHorizontalChange;
            }
        }

        /// <summary>
        /// Gets the vertical change
        /// </summary>
        public double VerticalChange
        {
            get
            {
                return this.mVerticalChange;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DragCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="pHorizontalChange"></param>
        /// <param name="pVerticalChange"></param>
        /// <param name="pIsCanceled"></param>
        public DragCompletedEventArgs(double pHorizontalChange, double pVerticalChange, bool pIsCanceled)
        {
            this.mHorizontalChange = pHorizontalChange;
            this.mVerticalChange = pVerticalChange;
            this.mIsCanceled = pIsCanceled;
        }

        #endregion Constructor
    }
}
