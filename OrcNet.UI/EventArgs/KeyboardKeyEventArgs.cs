﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="KeyboardKeyEventArgs"/> class.
    /// </summary>
    public class KeyboardKeyEventArgs : EventArgs
    {
	    #region Fields

        /// <summary>
        /// Stores the Key pressed.
        /// </summary>
	    private Key mKey;

        /// <summary>
        /// Stores the flag indicating whether the Key has been repeated.
        /// </summary>
	    private bool mRepeat;

        /// <summary>
        /// Stores the keyboard state.
        /// </summary>
	    private KeyboardState mState;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the <see cref="Key"/> that generated this event.
        /// </summary>
        public Key Key
        {
            get
            {
                return this.mKey;
            }
            internal set
            {
                this.mKey = value;
            }
        }

        /// <summary>
        /// Gets the scancode which generated this event.
        /// </summary>
        public uint ScanCode
        {
            get
            {
                return (uint)Key;
            }
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="OpenTK.Input.KeyModifiers.Alt"/> is pressed.
        /// </summary>
        /// <value><c>true</c> if pressed; otherwise, <c>false</c>.</value>
        public bool Alt
        {
            get
            {
                return this.mState[Key.AltLeft] || 
                       this.mState[Key.AltRight];
            }
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="OpenTK.Input.KeyModifiers.Control"/> is pressed.
        /// </summary>
        /// <value><c>true</c> if pressed; otherwise, <c>false</c>.</value>
        public bool Control
        {
            get
            {
                return this.mState[Key.ControlLeft] || 
                       this.mState[Key.ControlRight];
            }
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="OpenTK.Input.KeyModifiers.Shift"/> is pressed.
        /// </summary>
        /// <value><c>true</c> if pressed; otherwise, <c>false</c>.</value>
        public bool Shift
        {
            get
            {
                return this.mState[Key.ShiftLeft] || 
                       this.mState[Key.ShiftRight];
            }
        }

        /// <summary>
        /// Gets a bitwise combination representing the <see cref="OpenTK.Input.KeyModifiers"/>
        /// that are currently pressed.
        /// </summary>
        /// <value>The modifiers.</value>
        public KeyModifiers Modifiers
        {
            get
            {
                KeyModifiers lModifiers = 0;
                lModifiers |= Alt ? KeyModifiers.Alt : 0;
                lModifiers |= Control ? KeyModifiers.Control : 0;
                lModifiers |= Shift ? KeyModifiers.Shift : 0;
                return lModifiers;
            }
        }

        /// <summary>
        /// Gets the current <see cref="OpenTK.Input.KeyboardState"/>.
        /// </summary>
        /// <value>The keyboard.</value>
        public KeyboardState Keyboard
        {
            get
            {
                return this.mState;
            }
            internal set
            {
                this.mState = value;
            }
        }

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether
        /// this mKey event is a mRepeat.
        /// </summary>
        /// <value>
        /// true, if this event was caused by the user holding down
        /// a mKey; false, if this was caused by the user pressing a
        /// mKey for the first time.
        /// </value>
        public bool IsRepeat
        {
            get
            {
                return this.mRepeat;
            }
            internal set
            {
                this.mRepeat = value;
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyboardKeyEventArgs"/> class.
        /// </summary>
        /// <param name="pKey"></param>
        /// <param name="pRepeat"></param>
        /// <param name="pState"></param>
        public KeyboardKeyEventArgs(Key pKey, bool pRepeat, KeyboardState pState)
		{
			this.mKey    = pKey;
			this.mRepeat = pRepeat;
			this.mState  = pState;
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyboardKeyEventArgs"/> class.
        /// </summary>
        /// <param name="pEventArgs">The event arguments to clone.</param>
        public KeyboardKeyEventArgs(KeyboardKeyEventArgs pEventArgs)
	    {
	        this.Key = pEventArgs.Key;
	    }

	    #endregion Constructors
    }
}
