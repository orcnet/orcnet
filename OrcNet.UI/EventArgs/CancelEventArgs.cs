﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Delegate prototype of a cancel event.
    /// </summary>
    /// <param name="pSender"></param>
    /// <param name="pEventArgs"></param>
    public delegate void CancelEventDelegate(object pSender, CancelEventArgs pEventArgs);

    /// <summary>
    /// Definition of the <see cref="CancelEventArgs"/> class.
    /// </summary>
    public class CancelEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the operation must be canceled or not.
        /// </summary>
        private bool mCancel;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the operation must be canceled or not.
        /// </summary>
        public bool Cancel
        {
            get
            {
                return this.mCancel;
            }
            set
            {
                this.mCancel = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CancelEventArgs"/> class.
        /// </summary>
        public CancelEventArgs() :
        this( false )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CancelEventArgs"/> class.
        /// </summary>
        public CancelEventArgs(bool pCancel)
        {
            this.mCancel = pCancel;
        }

        #endregion Constructor
    }
}
