﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="PropertyValueChangedEventArgs"/> class.
    /// </summary>
	public class PropertyValueChangedEventArgs : EventArgs
	{
        #region Fields

        /// <summary>
        /// Stores the old element.
        /// </summary>
        private object mOldDataSource;

        /// <summary>
        /// Stores the new element.
        /// </summary>
		private object mNewDataSource;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the old element.
        /// </summary>
        public object OldDataSource
        {
            get
            {
                return this.mOldDataSource;
            }
        }

        /// <summary>
        /// Gets the new element.
        /// </summary>
		public object NewDataSource
        {
            get
            {
                return this.mNewDataSource;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyValueChangedEventArgs"/> class.
        /// </summary>
        /// <param name="pOldDataSource"></param>
        /// <param name="pNewDataSource"></param>
        public PropertyValueChangedEventArgs(object pOldDataSource, object pNewDataSource) :
        base()
		{
			this.mOldDataSource = pOldDataSource;
			this.mNewDataSource = pNewDataSource;
		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Turns this event arguments object into a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
		{
			return string.Format( "Old: {0} => New: {1}", this.mOldDataSource, this.mNewDataSource );
		}

        #endregion Methods
    }
}

