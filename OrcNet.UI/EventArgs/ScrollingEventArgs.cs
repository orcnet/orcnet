﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ScrollingEventArgs"/> class.
    /// </summary>
	public class ScrollingEventArgs: EventArgs
	{
        #region Properties

        /// <summary>
        /// Gets the scroll event type
        /// </summary>
        public ScrollEventType Type
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the new scroll value corresponding to the scroll type.
        /// </summary>
        public double Value
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollingEventArgs"/> class.
        /// </summary>
        /// <param name="pType">The scroll event type.</param>
        /// <param name="pNewValue">The new scroll value corresponding to the scroll type.</param>
        public ScrollingEventArgs(ScrollEventType pType, double pNewValue) : 
        base()
		{
			this.Type = pType;
            this.Value = pNewValue;
		}

        #endregion Constructor
    }
}

