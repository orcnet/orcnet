﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ScrollChangedEventArgs"/> class.
    /// </summary>
    public class ScrollChangedEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the new horizontal scroll offset.
        /// </summary>
        private double mOffsetX;

        /// <summary>
        /// Stores the new vertical scroll offset.
        /// </summary>
        private double mOffsetY;

        /// <summary>
        /// Stores the new horizontal scroll offset delta since last offset.
        /// </summary>
        private double mOffsetDeltaX;

        /// <summary>
        /// Stores the new vertical scroll offset delta since last offset.
        /// </summary>
        private double mOffsetDeltaY;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the new horizontal scroll offset.
        /// </summary>
        public double OffsetX
        {
            get
            {
                return this.mOffsetX;
            }
        }

        /// <summary>
        /// Gets the new vertical scroll offset.
        /// </summary>
        public double OffsetY
        {
            get
            {
                return this.mOffsetY;
            }
        }

        /// <summary>
        /// Gets the new horizontal scroll offset delta since last offset.
        /// </summary>
        public double OffsetDeltaX
        {
            get
            {
                return this.mOffsetDeltaX;
            }
        }

        /// <summary>
        /// Gets the new vertical scroll offset delta since last offset.
        /// </summary>
        public double OffsetDeltaY
        {
            get
            {
                return this.mOffsetDeltaY;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Definition of the <see cref="ScrollChangedEventArgs"/> class.
        /// </summary>
        /// <param name="pOffsetX"></param>
        /// <param name="pOffsetY"></param>
        /// <param name="pOffsetDeltaX"></param>
        /// <param name="pOffsetDeltaY"></param>
        public ScrollChangedEventArgs(double pOffsetX, double pOffsetY, double pOffsetDeltaX, double pOffsetDeltaY)
        {
            this.mOffsetX = pOffsetX;
            this.mOffsetY = pOffsetY;
            this.mOffsetDeltaX = pOffsetDeltaX;
            this.mOffsetDeltaY = pOffsetDeltaY;
        }

        #endregion Constructor
    }
}
