using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Defines the event arguments for KeyPress events. 
    /// Instances of this class are cached:
    /// KeyPressEventArgs should only be used inside the relevant event, unless manually cloned.
    /// </summary>
    public class KeyPressEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the key pressed value.
        /// </summary>
        private char mKeyChar;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets a <see cref="System.Char"/> that defines the ASCII character that was typed.
        /// </summary>
        public char KeyChar
        {
            get
            {
                return this.mKeyChar;
            }
            internal set
            {
                this.mKeyChar = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyPressEventArgs"/> class.
        /// </summary>
        /// <param name="pKeyChar">The ASCII character that was typed.</param>
        public KeyPressEventArgs(char pKeyChar)
        {
            this.mKeyChar = pKeyChar;
        }

        #endregion Constructor
    }
}
