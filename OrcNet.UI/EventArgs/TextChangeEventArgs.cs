﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Initializes a new instance of the <see cref="TextChangeEventArgs"/> class.
    /// </summary>
	public class TextChangeEventArgs: EventArgs
	{
        #region Fields

        /// <summary>
        /// Stores the changed text.
        /// </summary>
        private string mText;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the changed text.
        /// </summary>
        public string Text
        {
            get
            {
                return this.mText;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextChangeEventArgs"/> class.
        /// </summary>
        /// <param name="pText"></param>
        public TextChangeEventArgs(string pText) : 
        base()
        {
            this.mText = pText;
        }

        #endregion Constructor
    }
}

