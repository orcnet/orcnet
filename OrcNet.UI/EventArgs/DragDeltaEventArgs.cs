﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Delegate prototype of a Drag delta event.
    /// </summary>
    /// <param name="pSender"></param>
    /// <param name="pEventArgs"></param>
    public delegate void DragDeltaDelegate(object pSender, DragDeltaEventArgs pEventArgs);

    /// <summary>
    /// Definition of the <see cref="DragDeltaEventArgs"/> class.
    /// </summary>
    public class DragDeltaEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the horizontal delta
        /// </summary>
        private double mHorizontalDelta;

        /// <summary>
        /// Stores the vertical delta
        /// </summary>
        private double mVerticalDelta;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the horizontal delta
        /// </summary>
        public double HorizontalDelta
        {
            get
            {
                return this.mHorizontalDelta;
            }
        }

        /// <summary>
        /// Gets the vertical delta
        /// </summary>
        public double VerticalDelta
        {
            get
            {
                return this.mVerticalDelta;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DragStartedEventArgs"/> class.
        /// </summary>
        /// <param name="pHorizontalDelta"></param>
        /// <param name="pVerticalDelta"></param>
        public DragDeltaEventArgs(double pHorizontalDelta, double pVerticalDelta)
        {
            this.mHorizontalDelta = pHorizontalDelta;
            this.mVerticalDelta = pVerticalDelta;
        }

        #endregion Constructor
    }
}
