﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="BubblingMouseButtonEventArg"/> class.
    /// </summary>
	public class BubblingMouseButtonEventArg: MouseButtonEventArgs
	{
        #region Fields

        /// <summary>
        /// Stores the focused element.
        /// </summary>
        private UIElement mFocused;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the focused element.
        /// </summary>
        public UIElement Focused
        {
            get
            {
                return this.mFocused;
            }
            set
            {
                this.mFocused = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BubblingMouseButtonEventArg"/> class.
        /// </summary>
        /// <param name="pEventArgs">The event arguments to copy.</param>
        public BubblingMouseButtonEventArg(MouseButtonEventArgs pEventArgs) : 
        base( pEventArgs )
        {
			
		}

        #endregion Constructor
    }
}

