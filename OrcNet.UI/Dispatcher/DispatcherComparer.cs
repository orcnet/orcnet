﻿using System.Collections.Generic;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DispatcherComparer"/> class.
    /// </summary>
    public class DispatcherComparer : IEqualityComparer<Dispatcher>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatcherComparer"/> class.
        /// </summary>
        public DispatcherComparer()
        {

        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Checks whether the two dispatcher are equal or not.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public bool Equals(Dispatcher pFirst, Dispatcher pSecond)
        {
            return pFirst != null ? pFirst.Equals( pSecond ) : pSecond == null;
        }

        /// <summary>
        /// Gets the hashcode.
        /// </summary>
        /// <param name="pObject"></param>
        /// <returns></returns>
        public int GetHashCode(Dispatcher pObject)
        {
            return pObject.GetHashCode();
        }

        #endregion Methods
    }
}
