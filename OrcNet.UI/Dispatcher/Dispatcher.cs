﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Dispatcher"/> class.
    /// </summary>
    public sealed class Dispatcher : IEquatable<Dispatcher>, IEquatable<UIThread>
    {
        #region Fields

        /// <summary>
        /// Stores the dispatcher thread.
        /// </summary>
        private UIThread mThread;

        /// <summary>
        /// Stores the dispatcher queue.
        /// </summary>
        private DispatcherQueue mQueue;

        /// <summary>
        /// Stores the layout manager linked to this dispatcher.
        /// </summary>
        private ContextLayoutManager mLayoutManager;

        /// <summary>
        /// Stores the drawing context linked to this dispatcher.
        /// </summary>
        private MediaContext mDrawingContext;

        /// <summary>
        /// Stores the sync root.
        /// </summary>
        private object mSyncRoot = new object();

        /// <summary>
        /// Stores the last dispatcher returned.
        /// </summary>
        private static WeakReference sLastDispatcher = new WeakReference( null );

        /// <summary>
        /// Stores the global dispatcher sync root.
        /// </summary>
        private readonly static object sGlobalSyncRoot = new object();

        /// <summary>
        /// Stores the global set of dispatchers
        /// </summary>
        private readonly static Dictionary<UIThread, WeakReference> sDispatchers = new Dictionary<UIThread, WeakReference>();

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on operation posted.
        /// </summary>
        public Action<Dispatcher, DispatcherOperation> OperationPosted;

        /// <summary>
        /// Event fired on priority changes.
        /// </summary>
        public Action<Dispatcher, DispatcherOperation> PriorityChanged;

        /// <summary>
        /// Event fired on operation started.
        /// </summary>
        public Action<Dispatcher, DispatcherOperation> OperationStarted;

        /// <summary>
        /// Event fired on operation completed.
        /// </summary>
        public Action<Dispatcher, DispatcherOperation> OperationCompleted;

        /// <summary>
        /// Event fired on operation aborted.
        /// </summary>
        public Action<Dispatcher, DispatcherOperation> OperationAborted;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the sync root.
        /// </summary>
        internal object SyncRoot
        {
            get
            {
                return this.mSyncRoot;
            }
        }

        /// <summary>
        /// Gets the layout manager linked to this dispatcher.
        /// </summary>
        internal ContextLayoutManager LayoutManager
        {
            get
            {
                return this.mLayoutManager;
            }
            set
            {
                this.mLayoutManager = value;
            }
        }

        /// <summary>
        /// Gets the drawing context linked to this dispatcher.
        /// </summary>
        internal MediaContext DrawingContext
        {
            get
            {
                return this.mDrawingContext;
            }
            set
            {
                this.mDrawingContext = value;
            }
        }

        /// <summary>
        /// Gets the dispatcher thread.
        /// </summary>
        public UIThread Thread
        {
            get
            {
                return this.mThread;
            }
        }

        /// <summary>
        /// Gets the current dispatcher.
        /// </summary>
        public static Dispatcher CurrentDispatcher
        {
            get
            {
                // Gets the dispatcher corresponding to this thread.
                Dispatcher lCurrentDispatcher = Dispatcher.FromThread( UIThread.CurrentThread );

                // If none, create the dispatcher automatically.
                if ( lCurrentDispatcher == null )
                {
                    lCurrentDispatcher = new Dispatcher();
                }

                return lCurrentDispatcher;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Dispatcher"/> class.
        /// </summary>
        private Dispatcher()
        {
            this.mThread = UIThread.CurrentThread;
            this.mQueue = new DispatcherQueue();

            lock ( sGlobalSyncRoot )
            {
                sDispatchers.Add( this.mThread, new WeakReference( this ) );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the dispatcher associated with the given thread.
        /// </summary>
        /// <param name="pThread"></param>
        /// <returns></returns>
        public static Dispatcher FromThread(UIThread pThread)
        {
            Dispatcher lDispatcher = null;

            lock ( sGlobalSyncRoot )
            {
                if ( pThread != null )
                {
                    lDispatcher = sLastDispatcher.Target as Dispatcher;
                    if ( lDispatcher == null || 
                         lDispatcher.Thread != pThread )
                    {
                        lDispatcher = null;

                        if ( sDispatchers.Count > 0 )
                        {
                            WeakReference lResult;
                            if ( sDispatchers.TryGetValue( pThread, out lResult ) )
                            {
                                lDispatcher = lResult.Target as Dispatcher;
                                if ( lDispatcher == null )
                                {
                                    sDispatchers.Remove( pThread );
                                }
                            }
                        }

                        if ( lDispatcher != null )
                        {
                            if ( sLastDispatcher.IsAlive )
                            {
                                sLastDispatcher.Target = lDispatcher;
                            }
                            else
                            {
                                sLastDispatcher = new WeakReference( lDispatcher );
                            }
                        }
                    }
                }
            }

            return lDispatcher;
        }

        /// <summary>
        /// Checks whether the calling thread has access to the element this dispatcher is for.
        /// </summary>
        /// <returns></returns>
        public bool CheckAccess()
        {
            return this.mThread == System.Threading.Thread.CurrentThread;
        }

        /// <summary>
        /// Invokes asynchroneously the method on the dispatcher thread.
        /// </summary>
        /// <param name="pMethod"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        public DispatcherOperation BeginInvoke(Delegate pMethod, params object[] pParams)
        {
            return this.InternalBeginInvoke( DispatcherPriority.Normal, pMethod, pParams );
        }

        /// <summary>
        /// Invokes asynchroneously the method on the dispatcher thread.
        /// </summary>
        /// <param name="pMethod"></param>
        /// <param name="pPriority"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        public DispatcherOperation BeginInvoke(Delegate pMethod, DispatcherPriority pPriority, params object[] pParams)
        {
            return this.InternalBeginInvoke( pPriority, pMethod, pParams );
        }

        /// <summary>
        /// Invokes asynchroneously the method on the dispatcher thread.
        /// </summary>
        /// <param name="pPriority"></param>
        /// <param name="pMethod"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        private DispatcherOperation InternalBeginInvoke(DispatcherPriority pPriority, Delegate pMethod, object[] pParams)
        {
            if ( pMethod == null )
            {
                throw new ArgumentNullException( "pMethod" );
            }

            DispatcherOperation lOperation = new DispatcherOperation( this, pMethod, pPriority, pParams );
            this.InternalInvokeAsync( lOperation );

            return lOperation;
        }

        /// <summary>
        /// Invokes asynchroneously the method on the dispatcher thread.
        /// </summary>
        /// <param name="pCallback"></param>
        /// <returns></returns>
        public DispatcherOperation InvokeAsync(Action pCallback)
        {
            return this.InvokeAsync( pCallback, DispatcherPriority.Normal );
        }

        /// <summary>
        /// Invokes asynchroneously the method on the dispatcher thread.
        /// </summary>
        /// <param name="pCallback"></param>
        /// <param name="pPriority"></param>
        /// <returns></returns>
        public DispatcherOperation InvokeAsync(Action pCallback, DispatcherPriority pPriority)
        {
            if ( pCallback == null )
            {
                throw new ArgumentNullException("pCallback");
            }
            
            DispatcherOperation lOperation = new DispatcherOperation( this, pCallback, pPriority );
            this.InternalInvokeAsync( lOperation );

            return lOperation;
        }

        /// <summary>
        /// Invokes immediately the method on the dispatcher thread.
        /// </summary>
        /// <param name="pMethod"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        public object Invoke(Delegate pMethod, params object[] pParams)
        {
            return this.InternalInvoke( DispatcherPriority.Normal, TimeSpan.FromMilliseconds(-1), pMethod, pParams );
        }

        /// <summary>
        /// Invokes immediately the method on the dispatcher thread.
        /// </summary>
        /// <param name="pMethod"></param>
        /// <param name="pPriority"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        public object Invoke(Delegate pMethod, DispatcherPriority pPriority, params object[] pParams)
        {
            return this.InternalInvoke( pPriority, TimeSpan.FromMilliseconds(-1), pMethod, pParams );
        }

        /// <summary>
        /// Invokes immediately the method on the dispatcher thread.
        /// </summary>
        /// <param name="pMethod"></param>
        /// <param name="pTimeout"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        public object Invoke(Delegate pMethod, TimeSpan pTimeout, params object[] pParams)
        {
            return this.InternalInvoke( DispatcherPriority.Normal, pTimeout, pMethod, pParams );
        }

        /// <summary>
        /// Invokes immediately the method on the dispatcher thread.
        /// </summary>
        /// <param name="pMethod"></param>
        /// <param name="pTimeout"></param>
        /// <param name="pPriority"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        public object Invoke(Delegate pMethod, TimeSpan pTimeout, DispatcherPriority pPriority, params object[] pParams)
        {
            return this.InternalInvoke( pPriority, pTimeout, pMethod, pParams );
        }

        /// <summary>
        /// Checks whether the first dispatcher is equal to the second.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public static bool operator == (Dispatcher pFirst, Dispatcher pSecond)
        {
            if ( object.ReferenceEquals( pFirst, null ) )
            {
                return object.ReferenceEquals( pSecond, null );
            }

            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Checks whether the first dispatcher is different from the second.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public static bool operator !=(Dispatcher pFirst, Dispatcher pSecond)
        {
            return (pFirst == pSecond) == false;
        }
        
        /// <summary>
        /// Checks whether this dispatcher is equal to another or not.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            if ( pOther is Dispatcher )
            {
                return this.Equals( pOther as Dispatcher );
            }
            else if ( pOther is UIThread )
            {
                return this.Equals( pOther as UIThread );
            }

            return false;
        }

        /// <summary>
        /// Checks whether this dispatcher is equal to a thread or not.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public bool Equals(UIThread pOther)
        {
            return this.mThread.Equals( pOther );
        }

        /// <summary>
        /// Checks whether this dispatcher is equal to another or not.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public bool Equals(Dispatcher pOther)
        {
            if ( object.ReferenceEquals( pOther, null ) )
            {
                return false;
            }

            return this.Equals( pOther.mThread );
        }

        /// <summary>
        /// Gets the hashcode.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.mThread.GetHashCode() ^ this.mQueue.GetHashCode();
        }

        /// <summary>
        /// Invokes immediately the method on the dispatcher thread.
        /// </summary>
        /// <param name="pPriority"></param>
        /// <param name="pTimeout"></param>
        /// <param name="pMethod"></param>
        /// <param name="pParams"></param>
        /// <returns></returns>
        internal object InternalInvoke(DispatcherPriority pPriority, TimeSpan pTimeout, Delegate pMethod, object[] pParams)
        {
            if ( pMethod == null )
            {
                throw new ArgumentNullException("pMethod");
            }

            if ( pTimeout.TotalMilliseconds < 0 &&
                 pTimeout != TimeSpan.FromMilliseconds(-1) )
            {
                if ( this.CheckAccess() )
                {
                    pTimeout = TimeSpan.FromMilliseconds(-1);
                }
                else
                {
                    throw new ArgumentOutOfRangeException("pTimeout");
                }
            }

            DispatcherOperation lOperation = new DispatcherOperation( this, pMethod, pPriority, pParams );
            
            // Invoke straight.
            if ( pPriority == DispatcherPriority.Immediate && 
                 this.CheckAccess() )
            {
                lOperation.Invoke();
                return lOperation.Result;
            }

            // Otherwise, queue it and wait for the operation to be done.
            return this.InternalInvoke( lOperation, pTimeout );
        }

        /// <summary>
        /// Invokes immediately the method on the dispatcher thread.
        /// </summary>
        /// <param name="pOperation"></param>
        /// <param name="pTimeout"></param>
        /// <returns></returns>
        private object InternalInvoke(DispatcherOperation pOperation, TimeSpan pTimeout)
        {
            object lResult = null;

            Debug.Assert( pTimeout.TotalMilliseconds >= 0 || pTimeout == TimeSpan.FromMilliseconds(-1) );
            Debug.Assert( pOperation.Priority != DispatcherPriority.Immediate || this.CheckAccess() == false );

            // This operation must be queued since it was invoked either to
            // another thread, or at a priority other than Send.
            this.InternalInvokeAsync( pOperation );

            try
            {
                pOperation.Wait();

                Debug.Assert( pOperation.Status == OperationStatus.Completed ||
                              pOperation.Status == OperationStatus.Aborted );
                
                lResult = pOperation.Result;
            }
            catch
            {

            }

            return lResult;
        }

        /// <summary>
        /// Invokes asynchroneously the operation.
        /// </summary>
        /// <param name="pOperation"></param>
        private void InternalInvokeAsync(DispatcherOperation pOperation)
        {
            bool lIsSucceeded = false;

            lock ( this.SyncRoot )
            {
                if ( Environment.HasShutdownStarted == false )
                {
                    try
                    {
                        this.mQueue.Enqueue( pOperation.Priority, pOperation );

                        lIsSucceeded = true;
                    }
                    catch
                    {
                        lIsSucceeded = false;
                    }

                    if ( lIsSucceeded == false )
                    {
                        this.mQueue.Remove( pOperation );
                    }
                }
            }

            if ( lIsSucceeded )
            {
                this.NotifyOperationPosted( pOperation );
            }
        }

        /// <summary>
        /// Aborts the given operation.
        /// </summary>
        /// <param name="pOperation"></param>
        /// <returns></returns>
        internal bool Abort(DispatcherOperation pOperation)
        {
            bool lNotify = false;
            lock ( this.mSyncRoot )
            {
                if ( this.mQueue != null &&
                     pOperation.IsQueued )
                {
                    this.mQueue.Remove( pOperation );
                    pOperation.Status = OperationStatus.Aborted;
                    lNotify = true;
                }
            }

            if ( lNotify )
            {
                this.NotifyOperationAborted( pOperation );
            }

            return lNotify;
        }

        /// <summary>
        /// Tries to modify the operation priority
        /// </summary>
        /// <param name="pOperation"></param>
        /// <param name="pNewPriority"></param>
        /// <returns>The new priority if successful, the old one otherwise.</returns>
        internal DispatcherPriority SetPriority(DispatcherOperation pOperation, DispatcherPriority pNewPriority)
        {
            bool lNotify = false;
            lock ( this.mSyncRoot )
            {
                if ( this.mQueue != null &&
                     pOperation.IsQueued )
                {
                    this.mQueue.ChangeOperationPriority( pOperation, pNewPriority );
                    lNotify = true;
                }
            }

            if ( lNotify )
            {
                this.NotifyPriorityChanged( pOperation );

                return pNewPriority;
            }

            return pOperation.Priority;
        }

        /// <summary>
        /// Processes the operation queue.
        /// </summary>
        private void ProcessQueue()
        {
            DispatcherPriority lMaxPriority = DispatcherPriority.Invalid;
            DispatcherOperation lOperation = null;

            lock ( this.mSyncRoot )
            {
                lMaxPriority = this.mQueue.Max;

                if ( lMaxPriority != DispatcherPriority.Invalid )
                {
                    lOperation = this.mQueue.Dequeue();
                }
            }

            if ( lOperation != null )
            {
                this.NotifyOperationStarted( lOperation );

                lOperation.Invoke();

                this.NotifyOperationCompleted( lOperation );
            }
        }

        /// <summary>
        /// Notifies an operation posted.
        /// </summary>
        /// <param name="pOperation"></param>
        private void NotifyOperationPosted(DispatcherOperation pOperation)
        {
            if ( this.OperationPosted != null )
            {
                this.OperationPosted( this, pOperation );
            }
        }

        /// <summary>
        /// Notifies a priority changed.
        /// </summary>
        /// <param name="pOperation"></param>
        private void NotifyPriorityChanged(DispatcherOperation pOperation)
        {
            if ( this.PriorityChanged != null )
            {
                this.PriorityChanged( this, pOperation );
            }
        }

        /// <summary>
        /// Notifies an operation started.
        /// </summary>
        /// <param name="pOperation"></param>
        private void NotifyOperationStarted(DispatcherOperation pOperation)
        {
            if ( this.OperationStarted != null )
            {
                this.OperationStarted( this, pOperation );
            }
        }

        /// <summary>
        /// Notifies an operation completed.
        /// </summary>
        /// <param name="pOperation"></param>
        private void NotifyOperationCompleted(DispatcherOperation pOperation)
        {
            if ( this.OperationCompleted != null )
            {
                this.OperationCompleted( this, pOperation );
            }
        }

        /// <summary>
        /// Notifies an operation aborted.
        /// </summary>
        /// <param name="pOperation"></param>
        private void NotifyOperationAborted(DispatcherOperation pOperation)
        {
            if ( this.OperationAborted != null )
            {
                this.OperationAborted( this, pOperation );
            }
        }
        
        #endregion Methods
    }
}
