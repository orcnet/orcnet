﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ADispatcherObject"/> class.
    /// </summary>
    public abstract class ADispatcherObject : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the object is disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the dispatcher.
        /// </summary>
        private Dispatcher mDispatcher;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on disposale.
        /// </summary>
        public event EventHandler Disposed;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the object is disposed or not.
        /// </summary>
        public bool IsDisposed
        {
            get
            {
                return this.mIsDisposed;
            }
        }

        /// <summary>
        /// Gets the dispatcher.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                return this.mDispatcher;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ADispatcherObject"/> class.
        /// </summary>
        protected ADispatcherObject()
        {
            this.mIsDisposed = false;
            this.mDispatcher = Dispatcher.CurrentDispatcher;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the calling thread has acess to this object or not.
        /// </summary>
        /// <returns>True if so, false otherwise.</returns>
        public bool CheckAccess()
        {
            bool lIsAccessAllowed = true;
            Dispatcher lDispatcher = this.mDispatcher;
            
            if ( lDispatcher != null )
            {
                lIsAccessAllowed = lDispatcher.CheckAccess();
            }

            return lIsAccessAllowed;
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.OnDispose();
                this.mIsDisposed = true;

                this.NotifyDisposed();
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {

        }

        /// <summary>
        /// Notifies the disposale.
        /// </summary>
        private void NotifyDisposed()
        {
            if ( this.Disposed != null )
            {
                this.Disposed( this, EventArgs.Empty );
            }
        }

        #endregion Methods
    }
}
