﻿using System;
using System.Threading;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DispatcherOperation"/> class.
    /// </summary>
    public class DispatcherOperation
    {
        #region Fields

        /// <summary>
        /// Stores the dispatcher operation owner.
        /// </summary>
        private Dispatcher mOwner;

        /// <summary>
        /// Stores the operation priority.
        /// </summary>
        private DispatcherPriority mPriority;

        /// <summary>
        /// Stores the operation status.
        /// </summary>
        private OperationStatus mStatus;

        /// <summary>
        /// Stores the operation to process.
        /// </summary>
        private Delegate mMethod;

        /// <summary>
        /// Stores the method result if any.
        /// </summary>
        private object mResult;

        /// <summary>
        /// Stores the method parameters.
        /// </summary>
        private object[] mParameters;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on operation completed.
        /// </summary>
        public EventHandler Completed;

        /// <summary>
        /// Event fired on operation aborted.
        /// </summary>
        public EventHandler Aborted;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the sync root.
        /// </summary>
        internal object SyncRoot
        {
            get
            {
                return this.mOwner.SyncRoot;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the operation has been registered or not.
        /// </summary>
        internal bool IsQueued
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the method result if any.
        /// </summary>
        public object Result
        {
            get
            {
                return this.mResult;
            }
        }

        /// <summary>
        /// Gets the dispatcher operation owner.
        /// </summary>
        public Dispatcher Owner
        {
            get
            {
                return this.mOwner;
            }
        }

        /// <summary>
        /// Gets or sets the operation priority.
        /// </summary>
        public DispatcherPriority Priority
        {
            get
            {
                return this.mPriority;
            }
            set
            {
                if ( this.mPriority == value )
                {
                    return;
                }

                this.mPriority = this.mOwner.SetPriority( this, value );
            }
        }

        /// <summary>
        /// Gets the operation status.
        /// </summary>
        public OperationStatus Status
        {
            get
            {
                return this.mStatus;
            }
            internal set
            {
                this.mStatus = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatcherOperation"/> class.
        /// </summary>
        /// <param name="pOwner">The operation owner.</param>
        /// <param name="pMethod">The operation job.</param>
        /// <param name="pPriority">The operation priority.</param>
        /// <param name="pParams">THe method parameter(s).</param>
        public DispatcherOperation(Dispatcher pOwner, Delegate pMethod, DispatcherPriority pPriority, params object[] pParams)
        {
            this.mOwner = pOwner;
            this.mMethod = pMethod;
            this.mPriority = pPriority;
            this.mParameters = pParams;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Waits for this operation to complete.
        /// </summary>
        /// <returns></returns>
        public OperationStatus Wait()
        {
            return this.Wait( TimeSpan.FromMilliseconds(-1) );
        }

        /// <summary>
        /// Waits for this operation to complete.
        /// </summary>
        /// <param name="pTimeout">The maximum timeout to wait.</param>
        /// <returns></returns>
        public OperationStatus Wait(TimeSpan pTimeout)
        {
            if ( (this.mStatus == OperationStatus.Pending || this.mStatus == OperationStatus.Executing) &&
                  pTimeout.TotalMilliseconds != 0 )
            {
                if ( this.mOwner.Thread == Thread.CurrentThread )
                {
                    if ( this.mStatus == OperationStatus.Executing )
                    {
                        // Throw instead of deadlocking.
                        throw new InvalidOperationException( "Already executing the operation." );
                    }
                    
                    // Let the dispatcher process its queue.
                }
                else
                {
                    DispatcherOperationEvent lWait = new DispatcherOperationEvent( this, pTimeout );
                    lWait.WaitOne();
                }
            }

            return this.mStatus;
        }

        /// <summary>
        /// Invokes the operation.
        /// </summary>
        internal void Invoke()
        {
            this.mStatus = OperationStatus.Executing;

            bool lHasException = false;
            try
            {
                this.mResult = this.mMethod.Method.Invoke( this.mMethod.Target, this.mParameters );
            }
            catch( Exception )
            {
                lHasException = true;
            }

            Action lHandler = null;
            lock ( this.mOwner.SyncRoot )
            {
                if ( lHasException )
                {
                    lHandler = this.NotifyAborted;
                    this.mStatus = OperationStatus.Aborted;
                }
                else
                {
                    lHandler = this.NotifyCompleted;
                    this.mStatus = OperationStatus.Completed;
                }
            }

            if ( lHandler != null )
            {
                lHandler();
            }
        }

        /// <summary>
        /// Aborts the operation.
        /// </summary>
        /// <returns></returns>
        public bool Abort()
        {
            bool lIsRemoved = false;
            if ( this.mOwner != null )
            {
                lIsRemoved = this.mOwner.Abort( this );

                if ( lIsRemoved )
                {
                    this.NotifyAborted();
                }
            }

            return lIsRemoved;
        }

        /// <summary>
        /// Notifies the operation complited.
        /// </summary>
        private void NotifyCompleted()
        {
            if ( this.Completed != null )
            {
                this.Completed( this, EventArgs.Empty );
            }
        }

        /// <summary>
        /// Notifies the operation aborted.
        /// </summary>
        private void NotifyAborted()
        {
            if ( this.Aborted != null )
            {
                this.Aborted( this, EventArgs.Empty );
            }
        }

        #endregion Methods
    }
}
