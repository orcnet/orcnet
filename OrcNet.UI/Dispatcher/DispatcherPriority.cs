﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DispatcherPriority"/> enumeration.
    /// </summary>
    public enum DispatcherPriority
    {
        /// <summary>
        /// Invalid.
        /// </summary>
        Invalid = 0,

        /// <summary>
        /// Wait for application iddle times
        /// </summary>
        Idle,

        /// <summary>
        /// Wait for all non-idle operation(s) to process.
        /// </summary>
        Background,

        /// <summary>
        /// Wait for layout operation(s) to process.
        /// </summary>
        Layout,

        /// <summary>
        /// Wait for render operation(s) to process.
        /// </summary>
        Render,

        /// <summary>
        /// Wait for binding operation(s) to process.
        /// </summary>
        Binding,

        /// <summary>
        /// Process operation at the first occasion without switching context.
        /// </summary>
        Normal,

        /// <summary>
        /// Process operation immediately forcing to switch context.
        /// </summary>
        Immediate,
    }
}
