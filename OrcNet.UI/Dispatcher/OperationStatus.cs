﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="OperationStatus"/> enumeration.
    /// </summary>
    public enum OperationStatus
    {
        /// <summary>
        /// Waiting for being processed.
        /// </summary>
        Pending = 0,

        /// <summary>
        /// Aborted.
        /// </summary>
        Aborted,

        /// <summary>
        /// Completed.
        /// </summary>
        Completed,

        /// <summary>
        /// Being executed.
        /// </summary>
        Executing,
    }
}
