﻿using System;
using System.Threading;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DispatcherOperationEvent"/> class.
    /// </summary>
    internal sealed class DispatcherOperationEvent
    {
        #region Fields

        /// <summary>
        /// Stores the operation.
        /// </summary>
        private DispatcherOperation mOperation;

        /// <summary>
        /// Stores the time out.
        /// </summary>
        private TimeSpan mTimeout;

        /// <summary>
        /// Stores the manual reset event.
        /// </summary>
        private ManualResetEvent mEvent;

        /// <summary>
        /// Stores the flag indicating whether the event is closed or not.
        /// </summary>
        private bool mIsEventClosed;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the sync root.
        /// </summary>
        private object SyncRoot
        {
            get
            {
                return this.mOperation.SyncRoot;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatcherOperationEvent"/> class.
        /// </summary>
        /// <param name="pOperation"></param>
        /// <param name="pTimeout"></param>
        public DispatcherOperationEvent(DispatcherOperation pOperation, TimeSpan pTimeout)
        {
            this.mOperation = pOperation;
            this.mTimeout   = pTimeout;
            this.mEvent = new ManualResetEvent( false );
            this.mIsEventClosed = false;

            lock ( this.SyncRoot )
            {
                // We will set our event once the operation is completed or aborted.
                this.mOperation.Aborted   += new EventHandler( this.OnCompletedOrAborted );
                this.mOperation.Completed += new EventHandler( this.OnCompletedOrAborted );

                // Since some other thread is dispatching this operation, it could
                // have been dispatched while we were setting up the handlers.
                // We check the state again and set the event ourselves if this
                // happened.
                if ( this.mOperation.Status != OperationStatus.Pending &&
                     this.mOperation.Status != OperationStatus.Executing )
                {
                    this.mEvent.Set();
                }
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Wait for the operation to be done.
        /// </summary>
        public void WaitOne()
        {
            this.mEvent.WaitOne( this.mTimeout, false );

            lock ( this.SyncRoot )
            {
                if ( this.mIsEventClosed == false )
                {
                    // Cleanup the events.
                    this.mOperation.Aborted   -= new EventHandler( this.OnCompletedOrAborted );
                    this.mOperation.Completed -= new EventHandler( this.OnCompletedOrAborted );

                    // Close the event immediately instead of waiting for a GC
                    // because the Dispatcher is a a high-activity component and
                    // we could run out of events.
                    this.mEvent.Close();

                    this.mIsEventClosed = true;
                }
            }
        }

        /// <summary>
        /// Delegate called on operation completed or aborted.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnCompletedOrAborted(object pSender, EventArgs pEventArgs)
        {
            lock ( this.SyncRoot )
            {
                if ( this.mIsEventClosed == false )
                {
                    this.mEvent.Set();
                }
            }
        }
        
        #endregion Methods
    }
}
