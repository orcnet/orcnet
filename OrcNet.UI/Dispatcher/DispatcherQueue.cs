﻿using System.Collections.Generic;
using System.Linq;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DispatcherQueue"/> class.
    /// </summary>
    public class DispatcherQueue
    {
        #region Fields

        /// <summary>
        /// Internal cache.
        /// </summary>
        private Dictionary<DispatcherPriority, Queue<DispatcherOperation>> mCache;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the maximal priority.
        /// </summary>
        public DispatcherPriority Max
        {
            get
            {
                if ( this.mCache.Count > 0 )
                {
                    return this.mCache.Keys.Max();
                }

                return DispatcherPriority.Invalid;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DispatcherQueue"/> class.
        /// </summary>
        public DispatcherQueue()
        {
            this.mCache = new Dictionary<DispatcherPriority, Queue<DispatcherOperation>>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Enqueue the given operation.
        /// </summary>
        /// <param name="pPriority"></param>
        /// <param name="pData"></param>
        /// <returns></returns>
        public DispatcherOperation Enqueue(DispatcherPriority pPriority, DispatcherOperation pData)
        {
            if ( pPriority == DispatcherPriority.Invalid )
            {
                return pData;
            }

            Queue<DispatcherOperation> lQueue;
            if ( this.mCache.TryGetValue( pPriority, out lQueue ) == false )
            {
                this.mCache[ pPriority ] = lQueue = new Queue<DispatcherOperation>();
            }

            lQueue.Enqueue( pData );

            pData.IsQueued = true;

            return pData;
        }

        /// <summary>
        /// Dequeue the next operation.
        /// </summary>
        /// <returns></returns>
        public DispatcherOperation Dequeue()
        {
            DispatcherOperation lResult = null;
            Queue<DispatcherOperation> lQueue;
            if ( this.mCache.TryGetValue( this.Max, out lQueue ) ) // Get the highest priority.
            {
                lResult = lQueue.Dequeue();
                lResult.IsQueued = false;
            }

            return lResult;
        }

        /// <summary>
        /// Peeks the new operation.
        /// </summary>
        /// <returns></returns>
        public DispatcherOperation Peek()
        {
            DispatcherOperation lResult = null;
            Queue<DispatcherOperation> lQueue;
            if ( this.mCache.TryGetValue( this.Max, out lQueue ) )  // Get the highest priority.
            {
                lResult = lQueue.Peek();
            }

            return lResult;
        }

        /// <summary>
        /// Removes the given operation.
        /// </summary>
        /// <param name="pOperation"></param>
        public void Remove(DispatcherOperation pOperation)
        {
            Queue<DispatcherOperation> lQueue;
            if ( this.mCache.TryGetValue( pOperation.Priority, out lQueue ) )
            {
                IEnumerable<DispatcherOperation> lRest = lQueue.Except( new List<DispatcherOperation>() { pOperation } );

                if ( lRest.Count() > 0 )
                {
                    lQueue = new Queue<DispatcherOperation>( lRest );
                }
                else
                {
                    this.mCache.Remove( pOperation.Priority );
                }
            }
        }

        /// <summary>
        /// Changes the operation priority.
        /// </summary>
        /// <param name="pOperation"></param>
        /// <param name="pPriority"></param>
        public void ChangeOperationPriority(DispatcherOperation pOperation, DispatcherPriority pPriority)
        {
            this.Remove( pOperation );
            
            Queue<DispatcherOperation> lQueue;
            if ( this.mCache.TryGetValue( pPriority, out lQueue ) == false )
            {
                this.mCache[ pPriority ] = lQueue = new Queue<DispatcherOperation>();
            }

            lQueue.Enqueue( pOperation );
        }

        #endregion Methods
    }
}
