﻿using System;
using System.Threading;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ContextLayoutManager"/> class.
    /// </summary>
    internal sealed class ContextLayoutManager : ADispatcherObject
    {
        #region Fields

        /// <summary>
        /// Above this count, the layout pass is discard from the current
		/// update cycle and requeued for the next
        /// </summary>
		public const int MaxLayoutingTries = 3;

        /// <summary>
        /// Above this count, the layout pass is discard for the element and it
        /// will not be rendered on screen
        /// </summary>
        public const int MaxDiscardCount = 5;

        /// <summary>
        /// Stores the arrange queue.
        /// </summary>
        private ArrangeQueue mArrangeQueue;

        /// <summary>
        /// Stores the discard element queue between two updates.
        /// </summary>
        private DiscardQueue mDiscardQueue;

        /// <summary>
        /// Stores the layouted element(s) ready to be drawn.
        /// </summary>
        private LayoutedQueue mLayoutedQueue;

        /// <summary>
        /// Stores the layout manager sync root.
        /// </summary>
        private object mLayoutMutex = new object();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the layout manager sync root.
        /// </summary>
        internal object LayoutMutex
        {
            get
            {
                return this.mLayoutMutex;
            }
        }

        /// <summary>
        /// Gets the arrange queue.
        /// </summary>
        internal ArrangeQueue ArrangeQueue
        {
            get
            {
                if ( this.mArrangeQueue == null )
                {
                    this.mArrangeQueue = new ArrangeQueue();
                }

                return this.mArrangeQueue;
            }
        }

        /// <summary>
        /// Gets the discard queue.
        /// </summary>
        internal DiscardQueue DiscardQueue
        {
            get
            {
                if ( this.mDiscardQueue == null )
                {
                    this.mDiscardQueue = new DiscardQueue();
                }

                return this.mDiscardQueue;
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextLayoutManager"/> class.
        /// </summary>
        public ContextLayoutManager()
        {
            this.mLayoutedQueue = new LayoutedQueue();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the context layout manager from the dispatcher.
        /// </summary>
        /// <param name="pDispatcher"></param>
        /// <returns></returns>
        internal static ContextLayoutManager From(Dispatcher pDispatcher)
        {
            ContextLayoutManager lLayoutManager = pDispatcher.LayoutManager;
            if ( lLayoutManager == null )
            {
                if ( Dispatcher.CurrentDispatcher != pDispatcher )
                {
                    throw new InvalidOperationException();
                }

                lLayoutManager = new ContextLayoutManager();
                pDispatcher.LayoutManager = lLayoutManager;
            }

            return lLayoutManager;
        }

        /// <summary>
        /// Updates the layout.
        /// </summary>
        internal void UpdateLayout()
        {
            if ( Monitor.TryEnter( this.mLayoutMutex ) )
            {
                this.mDiscardQueue = new DiscardQueue();
                LayoutItem lItem;
                while ( this.mArrangeQueue.Any )
                {
                    lItem = this.mArrangeQueue.Dequeue();

                    lItem.ProcessLayouting();
                }

                this.mArrangeQueue = new ArrangeQueue( this.mDiscardQueue );

                Monitor.Exit( this.mLayoutMutex );

                this.mDiscardQueue = null;
            }

            // Update layouted element(s) clipping.
            this.UpdateClipping();
        }

        /// <summary>
        /// Registers a layouted visual.
        /// </summary>
        /// <param name="pVisual"></param>
        internal void RegisterLayouted(AVisual pVisual)
        {
            lock ( this.mLayoutedQueue )
            {
                this.mLayoutedQueue.Enqueue( pVisual );
            }
        }

        /// <summary>
        /// Updates the clipping measure.
        /// </summary>
        private void UpdateClipping()
        {
            AVisual lVisual = null;
            while ( this.mLayoutedQueue.Count > 0 )
            {
                lock ( this.mLayoutedQueue )
                {
                    lVisual = this.mLayoutedQueue.Dequeue();
                }

                if ( lVisual.VisualParent == null )
                {
                    continue;
                }

                lock ( lVisual )
                {
                    lVisual.VisualParent.RegisterClip( lVisual.LastSlots );
                    lVisual.VisualParent.RegisterClip( lVisual.Slot );
                }
            }
        }

        #endregion Methods
    }
}
