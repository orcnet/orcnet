﻿using Cairo;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="MediaContext"/> class.
    /// </summary>
    internal sealed class MediaContext : ADispatcherObject
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the target has any part that must be redrawn or not.
        /// </summary>
        private bool mIsDirty = false;

        /// <summary>
        /// Stores the resulting bitmap limited to last redrawn part
        /// </summary>
        public byte[] mRawDirtyBitmap;

        /// <summary>
        /// Stores the dirty region of the original one that must be redrawn.
        /// </summary>
        private Rectangle mDirtyRegion;

        /// <summary>
        /// Stores the root clipping region
        /// </summary>
        private Rectangles mClipping = new Rectangles();

        /// <summary>
        /// Stores the raw resulting drawing data.
        /// </summary>
        public byte[] mRawDrawingData;
        
        /// <summary>
        /// Stores the client drawing region.
        /// </summary>
        private Rectangle mClientRectangle;
        
        /// <summary>
        /// Stores the composition targets registered.
        /// </summary>
        private HashSet<ICompositionTarget> mRegisteredTargets;

        /// <summary>
        /// Sync mutex for rendering operations (bmp, dirtyBmp,...)
        /// </summary>
        private object mRenderMutex = new object();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the sync root.
        /// </summary>
        internal object SyncRoot
        {
            get
            {
                return this.mRenderMutex;
            }
        }

        /// <summary>
        /// Gets the current media context.
        /// </summary>
        internal static MediaContext CurrentMediaContext
        {
            get
            {
                return From( Dispatcher.CurrentDispatcher );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaContext"/> class.
        /// </summary>
        /// <param name="pDispatcher">The dispatcher this media context is for.</param>
        internal MediaContext(Dispatcher pDispatcher)
        {
            Debug.Assert( pDispatcher.DrawingContext == null );

            this.mRegisteredTargets = new HashSet<ICompositionTarget>();

            pDispatcher.DrawingContext = this;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the given visual can be rendered or not.
        /// </summary>
        /// <param name="pVisual"></param>
        /// <returns></returns>
        internal bool CanRender(AVisual pVisual)
        {
            return this.mClipping.Intersect( pVisual.Slot );
        }

        /// <summary>
        /// Resizes the media context for the given bounds.
        /// </summary>
        internal void Resize(Rectangle pBounds)
        {
            this.mClientRectangle = pBounds;

            int lStride = 4 * this.mClientRectangle.Width;
            int lRawSize = Math.Abs( lStride ) * this.mClientRectangle.Height;
            this.mRawDrawingData = new byte[ lRawSize ];

            // Resize target(s).
            foreach ( ICompositionTarget lTarget in this.mRegisteredTargets )
            {
                lTarget.OnResize();
            }
            
            this.mClipping.AddRectangle( this.mClientRectangle );

            // Trigger manually a Render??
        }

        /// <summary>
        /// Gets the media context from the dispatcher.
        /// </summary>
        /// <param name="pDispatcher"></param>
        /// <returns></returns>
        internal static MediaContext From(Dispatcher pDispatcher)
        {
            Debug.Assert( pDispatcher != null, "Dispatcher cannot be null.");

            MediaContext lDrawingContext = pDispatcher.DrawingContext;
            if ( lDrawingContext == null )
            {
                lDrawingContext = new MediaContext( pDispatcher );
            }

            return lDrawingContext;
        }

        /// <summary>
        /// Registers the given composition target.
        /// </summary>
        /// <param name="pDispatcher"></param>
        /// <param name="pTarget"></param>
        internal static void RegisterCompositionTarget(Dispatcher pDispatcher, ICompositionTarget pTarget)
        {
            Debug.Assert( pDispatcher != null );
            Debug.Assert( pTarget != null );

            MediaContext lCurrent = From( pDispatcher );
            lCurrent.RegisterCompositionTargetInternal( pTarget );
        }

        /// <summary>
        /// Unregisters the given composition target.
        /// </summary>
        /// <param name="pDispatcher"></param>
        /// <param name="pTarget"></param>
        internal static void UnregisterCompositionTarget(Dispatcher pDispatcher, ICompositionTarget pTarget)
        {
            Debug.Assert( pDispatcher != null );
            Debug.Assert( pTarget != null );

            MediaContext lCurrent = From( pDispatcher );
            lCurrent.UnregisterCompositionTargetInternal( pTarget );
        }

        /// <summary>
        /// Registers the given composition target.
        /// </summary>
        /// <param name="pTarget"></param>
        private void RegisterCompositionTargetInternal(ICompositionTarget pTarget)
        {
            Debug.Assert( this.IsDisposed == false );
            Debug.Assert( pTarget != null );

            this.mRegisteredTargets.Add( pTarget );
        }

        /// <summary>
        /// Unregisters the given composition target.
        /// </summary>
        /// <param name="pTarget"></param>
        private void UnregisterCompositionTargetInternal(ICompositionTarget pTarget)
        {
            Debug.Assert( pTarget != null );

            if ( this.IsDisposed )
            {
                return;
            }

            this.mRegisteredTargets.Remove( pTarget );
        }

        /// <summary>
        /// Renders all the composition targets registered on the context.
        /// </summary>
        internal void Render()
        {
            Debug.Assert( this.IsDisposed == false );
            Debug.Assert( this.mRegisteredTargets != null );

#if MEASURE_TIME
			drawingMeasure.StartCycle();
#endif
            using ( ImageSurface lSurface = new ImageSurface( this.mRawDrawingData, Format.Argb32, this.mClientRectangle.Width, this.mClientRectangle.Height, this.mClientRectangle.Width * 4 ) )
            {
                using ( Context lContext = new Context( lSurface ) )
                {
                    if ( this.mClipping.Count > 0 )
                    {
                        this.mClipping.ClearAndClip( lContext );

                        foreach ( ICompositionTarget lTarget in this.mRegisteredTargets )
                        {
                            lTarget.Render( lContext );
                        }

#if DEBUG_CLIP_RECTANGLE
						this.mClipping.stroke (pContext, Color.Red.AdjustAlpha(0.5));
#endif
                        lock ( this.mRenderMutex )
                        {
                            if ( this.mIsDirty )
                            {
                                this.mDirtyRegion += this.mClipping.Bounds;
                            }
                            else
                            {
                                this.mDirtyRegion = this.mClipping.Bounds;
                            }

                            this.mDirtyRegion.Left   = Math.Max( 0, this.mDirtyRegion.Left );
                            this.mDirtyRegion.Top    = Math.Max( 0, this.mDirtyRegion.Top );
                            this.mDirtyRegion.Width  = Math.Min( this.mClientRectangle.Width - this.mDirtyRegion.Left, this.mDirtyRegion.Width );
                            this.mDirtyRegion.Height = Math.Min( this.mClientRectangle.Height - this.mDirtyRegion.Top, this.mDirtyRegion.Height );
                            this.mDirtyRegion.Width  = Math.Max( 0, this.mDirtyRegion.Width );
                            this.mDirtyRegion.Height = Math.Max( 0, this.mDirtyRegion.Height );

                            if ( this.mDirtyRegion.Width > 0 && 
                                 this.mDirtyRegion.Height > 0 )
                            {
                                this.mRawDirtyBitmap = new byte[ 4 * this.mDirtyRegion.Width * this.mDirtyRegion.Height ];
                                for ( int y = 0; y < this.mDirtyRegion.Height; y++ )
                                {
                                    Array.Copy( this.mRawDrawingData, 
                                                ((this.mDirtyRegion.Top + y) * this.mClientRectangle.Width * 4) + this.mDirtyRegion.Left * 4,
                                                this.mRawDirtyBitmap, y * this.mDirtyRegion.Width * 4, 
                                                this.mDirtyRegion.Width * 4 );
                                }

                                this.mIsDirty = true;
                            }
                            else
                            {
                                this.mIsDirty = false;
                            }
                        }

                        this.mClipping.Reset();
                    }
                }
            }
#if MEASURE_TIME
			drawingMeasure.StopCycle();
#endif
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDispose()
        {
            foreach ( ICompositionTarget lTarget in this.mRegisteredTargets.ToArray() )
            {
                lTarget.Dispose();
            }
            this.mRegisteredTargets.Clear();
            this.mRegisteredTargets = null;

            base.OnDispose();
        }

        #endregion Methods
    }
}
