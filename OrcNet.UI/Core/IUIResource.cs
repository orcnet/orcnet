﻿using System;
using System.ComponentModel;

namespace OrcNet.UI.Core
{
    /// <summary>
    /// Definition of the <see cref="IUIResource"/> interface.
    /// </summary>
    public interface IUIResource : INotifyPropertyChanged, ICloneable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the data context involved in bindings.
        /// 
        /// NOTE: The UI Resource itself by default.
        /// </summary>
        INotifyPropertyChanged DataContext
        {
            get;
            set;
        }

        #endregion Properties
    }
}
