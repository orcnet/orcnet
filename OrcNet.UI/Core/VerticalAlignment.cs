﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="VerticalAlignment"/> enumeration.
    /// </summary>
    public enum VerticalAlignment
    {
        /// <summary>
        /// Top
        /// </summary>
        Top,

        /// <summary>
        /// Bottom
        /// </summary>
        Bottom,

        /// <summary>
        /// Center
        /// </summary>
        Center,

        /// <summary>
        /// Stretch the content.
        /// </summary>
        Stretch
    }
}
