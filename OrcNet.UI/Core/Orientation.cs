﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Orientation"/> enumeration.
    /// </summary>
    public enum Orientation
    {
        /// <summary>
        /// Horizontal
        /// </summary>
        Horizontal,

        /// <summary>
        /// Vertical
        /// </summary>
        Vertical
    }
}
