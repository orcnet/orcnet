﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="HorizontalAlignment"/> enumeration.
    /// </summary>
    public enum HorizontalAlignment
    {
        /// <summary>
        /// Left
        /// </summary>
        Left,

        /// <summary>
        /// Right
        /// </summary>
        Right,

        /// <summary>
        /// Center
        /// </summary>
        Center,

        /// <summary>
        /// Stretch the content.
        /// </summary>
        Stretch
    }
}
