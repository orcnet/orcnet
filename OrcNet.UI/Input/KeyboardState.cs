﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Encapsulates the state of a Keyboard device.
    /// </summary>
    public struct KeyboardState : IEquatable<KeyboardState>
    {
	    #region Fields

	    /// <summary>
        /// Stores the constant integer 32bits size.
        /// </summary>
	    private const int IntSize = sizeof(int);

        /// <summary>
        /// Stores the constant amount of integers for each keys.
        /// </summary>
	    private const int NumInts = ((int)Key.LastKey + IntSize - 1) / IntSize;

        /// <summary>
        /// The following line triggers bogus CS0214 in gmcs 2.0.1, sigh...
        /// </summary>
        private unsafe fixed int Keys[NumInts];

        /// <summary>
        /// Stores the flag indicating whether a keyboard is connected.
        /// </summary>
	    private bool mIsConnected;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether the specified
        /// <see cref="OpenTK.Input.Key"/> is pressed.
        /// </summary>
        /// <param name="pKey">The <see cref="OpenTK.Input.Key"/> to check.</param>
        /// <returns>True if key is pressed; false otherwise.</returns>
        public bool this[Key pKey]
	    {
	        get
            {
                return this.IsKeyDown( pKey );
            }
	        internal set
            {
                this.SetKeyState( pKey, value );
            }
	    }

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether the specified
        /// <see cref="OpenTK.Input.Key"/> is pressed.
        /// </summary>
        /// <param name="pCode">The scancode to check.</param>
        /// <returns>True if code is pressed; false otherwise.</returns>
        public bool this[short pCode]
	    {
	        get
            {
                return this.IsKeyDown( (Key)pCode );
            }
	    }

        /// <summary>
	    /// Gets a <see cref="System.Boolean"/> indicating whether this keyboard
	    /// is connected.
	    /// </summary>
	    public bool IsConnected
        {
            get
            {
                return this.mIsConnected;
            }
            internal set
            {
                this.mIsConnected = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether this key is down.
        /// </summary>
        /// <param name="pKey">The <see cref="OpenTK.Input.Key"/> to check.</param>
        public bool IsKeyDown(Key pKey)
	    {
	        return this.ReadBit( (int)pKey );
	    }

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether this scan code is down.
        /// </summary>
        /// <param name="pCode">The scan code to check.</param>
        public bool IsKeyDown(short pCode)
	    {
	        return pCode >= 0 &&
                   pCode < (short)Key.LastKey && 
                   this.ReadBit( pCode );
	    }

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether this key is up.
        /// </summary>
        /// <param name="pKey">The <see cref="OpenTK.Input.Key"/> to check.</param>
        public bool IsKeyUp(Key pKey)
	    {
	        return this.ReadBit( (int)pKey ) == false;
	    }

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether this scan code is down.
        /// </summary>
        /// <param name="pCode">The scan code to check.</param>
        public bool IsKeyUp(short pCode)
	    {
	        return this.IsKeyDown( pCode ) == false;
	    }
        
	    /// <summary>
	    /// Checks whether two <see cref="KeyboardState" /> instances are equal.
	    /// </summary>
	    /// <param name="pLeft">
	    /// A <see cref="KeyboardState"/> instance.
	    /// </param>
	    /// <param name="pRight">
	    /// A <see cref="KeyboardState"/> instance.
	    /// </param>
	    /// <returns>
	    /// True if both pLeft is equal to pRight; false otherwise.
	    /// </returns>
	    public static bool operator ==(KeyboardState pLeft, KeyboardState pRight)
	    {
	        return pLeft.Equals(pRight);
	    }

	    /// <summary>
	    /// Checks whether two <see cref="KeyboardState" /> instances are not equal.
	    /// </summary>
	    /// <param name="pLeft">
	    /// A <see cref="KeyboardState"/> instance.
	    /// </param>
	    /// <param name="pRight">
	    /// A <see cref="KeyboardState"/> instance.
	    /// </param>
	    /// <returns>
	    /// True if both pLeft is not equal to pRight; false otherwise.
	    /// </returns>
	    public static bool operator !=(KeyboardState pLeft, KeyboardState pRight)
	    {
	        return pLeft.Equals(pRight) == false;
	    }

        /// <summary>
        /// Compares to an object instance for equality.
        /// </summary>
        /// <param name="pOther">
        /// The <see cref="System.Object"/> to compare to.
        /// </param>
        /// <returns>
        /// True if this instance is equal to obj; false otherwise.
        /// </returns>
        public override bool Equals(object pOther)
	    {
	        if ( pOther is KeyboardState)
	        {
		        return this == (KeyboardState)pOther;
	        }
	        else
	        {
		        return false;
	        }
	    }

	    /// <summary>
	    /// Generates a hashcode for the current instance.
	    /// </summary>
	    /// <returns>
	    /// A <see cref="System.Int32"/> represting the hashcode for this instance.
	    /// </returns>
	    public override int GetHashCode()
	    {
	        unsafe
	        {
		        fixed (int* k = Keys)
		        {
		            int hashcode = 0;
                    for (int i = 0; i < NumInts; i++)
                    {
                        hashcode ^= (k + i)->GetHashCode();
                    }

		            return hashcode;
		        }
	        }
	    }
        
        /// <summary>
        /// Set a new key state.
        /// </summary>
        /// <param name="pKey">The key to set the state for.</param>
        /// <param name="pIsDown">The key state.</param>
	    public void SetKeyState(Key pKey, bool pIsDown)
	    {
	        if ( pIsDown )
	        {
		        this.EnableBit( (int)pKey );
	        }
	        else
	        {
		        this.DisableBit( (int)pKey );
	        }
	    }

        /// <summary>
        /// Read a keyboard bit state.
        /// </summary>
        /// <param name="pOffset">The keyboard key offset.</param>
        /// <returns></returns>
	    internal bool ReadBit(int pOffset)
	    {
	        ValidateOffset(pOffset);

	        int int_offset = pOffset / 32;
	        int bit_offset = pOffset % 32;
	        unsafe
	        {
		        fixed (int* k = Keys)
                {
                    return (*(k + int_offset) & (1 << bit_offset)) != 0u;
                }
	        }
	    }

        /// <summary>
        /// Set a key as pressed.
        /// </summary>
        /// <param name="pOffset">The keyboard key offset.</param>
	    internal void EnableBit(int pOffset)
	    {
	        int int_offset = pOffset / 32;
	        int bit_offset = pOffset % 32;
	        unsafe
	        {
		        fixed (int* k = Keys)
                {
                    *(k + int_offset) |= 1 << bit_offset;
                }
	        }
	    }

        /// <summary>
        /// Set a key as unpressed.
        /// </summary>
        /// <param name="pOffset">The keyboard key offset.</param>
	    internal void DisableBit(int pOffset)
	    {
	        ValidateOffset(pOffset);

	        int int_offset = pOffset / 32;
	        int bit_offset = pOffset % 32;
	        unsafe
	        {
		        fixed (int* k = Keys)
                {
                    *(k + int_offset) &= ~(1 << bit_offset);
                }
	        }
	    }

        /// <summary>
        /// Merges the keyboard states with another.
        /// </summary>
        /// <param name="pOther">The other keyboard states to merge with.</param>
	    internal void MergeBits(KeyboardState pOther)
	    {
	        unsafe
	        {
		        int* k2 = pOther.Keys;
		        fixed (int* k1 = Keys)
		        {
		            for (int i = 0; i < NumInts; i++)
			        *(k1 + i) |= *(k2 + i);
		        }
	        }

	        this.IsConnected |= pOther.IsConnected;
	    }

        /// <summary>
        /// Set the keyboard as connected or disconnected.
        /// </summary>
        /// <param name="pValue"></param>
	    internal void SetIsConnected(bool pValue)
	    {
	        this.IsConnected = pValue;
	    }
        
        /// <summary>
        /// Validates a key offset as being in the set of key state count.
        /// </summary>
        /// <param name="pOffset"></param>
	    private static void ValidateOffset(int pOffset)
	    {
	        if ( pOffset < 0 || 
                 pOffset >= NumInts * IntSize )
            {
                throw new ArgumentOutOfRangeException();
            }
	    }
        
        /// <summary>
        /// Compares two KeyboardState instances.
        /// </summary>
        /// <param name="pOther">The instance to compare two.</param>
        /// <returns>True, if both instances are equal; false otherwise.</returns>
        public bool Equals(KeyboardState pOther)
	    {
	        bool lIsEqual = true;
	        unsafe
	        {
		        int* k2 = pOther.Keys;
		        fixed (int* k1 = Keys)
		        {
                    for (int i = 0; lIsEqual && i < NumInts; i++)
                    {
                        lIsEqual &= *(k1 + i) == *(k2 + i);
                    }
		        }
	        }

	        return lIsEqual;
	    }
        
        #endregion Methods
    }
}
