using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Encapsulates the state of a mouse device.
    /// </summary>
    public struct MouseState : IEquatable<MouseState>
    {
	    #region Fields

        /// <summary>
        /// Stores the constant maximum mouse button count.
        /// </summary>
	    public const int MaxButtons = 16;

        /// <summary>
        /// Stores the current mouse screen mPosition.
        /// </summary>
	    private Point mPosition;

        /// <summary>
        /// Stores the current mouse wheel state.
        /// </summary>
	    private MouseScroll mWheelState;

        /// <summary>
        /// Stores the set of button states.
        /// </summary>
	    private ushort mButtons;

        /// <summary>
        /// Stores the flag indicating whether a mouse is connected or not.
        /// </summary>
	    private bool mIsConnected;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the mouse screen position.
        /// </summary>
        internal Point Position
        {
            get
            {
                return this.mPosition;
            }
            set
            {
                this.mPosition = value;
            }
        }

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether the specified
        /// <see cref="OpenTK.Input.MouseButton"/> is pressed.
        /// </summary>
        /// <param name="pButton">The <see cref="OpenTK.Input.MouseButton"/> to check.</param>
        /// <returns>True if key is pressed; false otherwise.</returns>
        public bool this[MouseButton pButton]
	    {
	        get
            {
                return IsButtonDown( pButton );
            }
	        internal set
	        {
                if ( value )
                {
                    this.EnableBit( (int)pButton );
                }
                else
                {
                    this.DisableBit( (int)pButton );
                }
	        }
	    }
        
	    /// <summary>
	    /// Gets the absolute wheel mPosition in integer units.
	    /// To support high-precision mice, it is recommended to use <see cref="WheelPrecise"/> instead.
	    /// </summary>
	    public int Wheel
	    {
	        get
            {
                return (int)Math.Round( this.mWheelState.Y, MidpointRounding.AwayFromZero );
            }
	    }

	    /// <summary>
	    /// Gets the absolute wheel mPosition in floating-point units.
	    /// </summary>
	    public float WheelPrecise
	    {
	        get
            {
                return this.mWheelState.Y;
            }
	    }

	    /// <summary>
	    /// Gets a <see cref="OpenTK.Input.MouseScroll"/> instance,
	    /// representing the current state of the mouse mWheelState wheel.
	    /// </summary>
	    public MouseScroll Scroll
	    {
	        get
            {
                return this.mWheelState;
            }
	    }

	    /// <summary>
	    /// Gets or sets an integer representing the absolute x mPosition of the pointer, in window pixel coordinates.
	    /// </summary>
	    public int X
	    {
		    get
            {
                return this.mPosition.X;
            }
	        set
            {
                this.mPosition.X = value;
            }
	    }

	    /// <summary>
	    /// Gets or sets an integer representing the absolute y mPosition of the pointer, in window pixel coordinates.
	    /// </summary>
	    public int Y
	    {
		    get
            {
                return this.mPosition.Y;
            }
	        set
            {
                this.mPosition.Y = value;
            }
	    }

	    /// <summary>
	    /// Gets a <see cref="System.Boolean"/> indicating whether the pLeft mouse button is pressed.
	    /// This property is intended for XNA compatibility.
	    /// </summary>
	    public ButtonState LeftButton
	    {
	        get
            {
                return this.IsButtonDown( MouseButton.Left ) ? ButtonState.Pressed : 
                                                               ButtonState.Released;
            }
	    }

	    /// <summary>
	    /// Gets a <see cref="System.Boolean"/> indicating whether the middle mouse button is pressed.
	    /// This property is intended for XNA compatibility.
	    /// </summary>
	    public ButtonState MiddleButton
	    {
	        get
            {
                return this.IsButtonDown( MouseButton.Middle ) ? ButtonState.Pressed : 
                                                                 ButtonState.Released;
            }
	    }

	    /// <summary>
	    /// Gets a <see cref="System.Boolean"/> indicating whether the pRight mouse button is pressed.
	    /// This property is intended for XNA compatibility.
	    /// </summary>
	    public ButtonState RightButton
	    {
	        get
            {
                return this.IsButtonDown( MouseButton.Right ) ? ButtonState.Pressed : 
                                                                ButtonState.Released;
            }
	    }

	    /// <summary>
	    /// Gets a <see cref="System.Boolean"/> indicating whether the first extra mouse button is pressed.
	    /// This property is intended for XNA compatibility.
	    /// </summary>
	    public ButtonState XButton1
	    {
	        get
            {
                return this.IsButtonDown( MouseButton.Button1 ) ? ButtonState.Pressed : 
                                                                  ButtonState.Released;
            }
	    }

	    /// <summary>
	    /// Gets a <see cref="System.Boolean"/> indicating whether the second extra mouse button is pressed.
	    /// This property is intended for XNA compatibility.
	    /// </summary>
	    public ButtonState XButton2
	    {
	        get
            {
                return this.IsButtonDown( MouseButton.Button2 ) ? ButtonState.Pressed : 
                                                                  ButtonState.Released;
            }
	    }

	    /// <summary>
	    /// Gets the absolute wheel mPosition in integer units. This property is intended for XNA compatibility.
	    /// To support high-precision mice, it is recommended to use <see cref="WheelPrecise"/> instead.
	    /// </summary>
	    public int ScrollWheelValue
	    {
	        get
            {
                return this.Wheel;
            }
	    }

	    /// <summary>
	    /// Gets a value indicating whether this instance is connected.
	    /// </summary>
	    /// <value><c>true</c> if this instance is connected; otherwise, <c>false</c>.</value>
	    public bool IsConnected
	    {
	        get
            {
                return this.mIsConnected;
            }
	        internal set
            {
                this.mIsConnected = value;
            }
	    }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether this button is down.
        /// </summary>
        /// <param name="pButton">The <see cref="OpenTK.Input.MouseButton"/> to check.</param>
        public bool IsButtonDown(MouseButton pButton)
	    {
	        return this.ReadBit( (int)pButton );
	    }

        /// <summary>
        /// Gets a <see cref="System.Boolean"/> indicating whether this button is up.
        /// </summary>
        /// <param name="pButton">The <see cref="OpenTK.Input.MouseButton"/> to check.</param>
        public bool IsButtonUp(MouseButton pButton)
	    {
	        return this.ReadBit( (int)pButton ) == false;
	    }

        /// <summary>
        /// Reads one of the mouse button state.
        /// </summary>
        /// <param name="pOffset">The button offset</param>
        /// <returns></returns>
        internal bool ReadBit(int pOffset)
        {
            ValidateOffset( pOffset );
            return (this.mButtons & (1 << pOffset)) != 0;
        }

        /// <summary>
        /// Set the bit at the given offset.
        /// </summary>
        /// <param name="pOffset"></param>
        public void EnableBit(int pOffset)
        {
            ValidateOffset( pOffset );
            this.mButtons |= unchecked((ushort)(1 << pOffset));
        }

        /// <summary>
        /// Unset the bit at the given offset.
        /// </summary>
        /// <param name="pOffset"></param>
        public void DisableBit(int pOffset)
        {
            ValidateOffset( pOffset );
            this.mButtons &= unchecked((ushort)(~(1 << pOffset)));
        }

        /// <summary>
        /// Merges mouse states together.
        /// </summary>
        /// <param name="pOther">The other state to merge with.</param>
        internal void MergeBits(MouseState pOther)
        {
            this.mButtons |= pOther.mButtons;
            SetScrollRelative( pOther.mWheelState.X, pOther.mWheelState.Y );
            this.X += pOther.X;
            this.Y += pOther.Y;
            this.IsConnected |= pOther.IsConnected;
        }

        /// <summary>
        /// Set the mouse as connected or not.
        /// </summary>
        /// <param name="pValue"></param>
        internal void SetIsConnected(bool pValue)
        {
            this.IsConnected = pValue;
        }

        /// <summary>
        /// Set the absolute horizontal and vertical wheel state.
        /// </summary>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        internal void SetScrollAbsolute(float pX, float pY)
        {
            this.mWheelState.X = pX;
            this.mWheelState.Y = pY;
        }

        /// <summary>
        /// Set the relative horizontal and vertical wheel state.
        /// </summary>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        internal void SetScrollRelative(float pX, float pY)
        {
            this.mWheelState.X += pX;
            this.mWheelState.Y += pY;
        }

        /// <summary>
        /// Checks whether two <see cref="MouseState" /> instances are equal.
        /// </summary>
        /// <param name="pLeft">
        /// A <see cref="MouseState"/> instance.
        /// </param>
        /// <param name="pRight">
        /// A <see cref="MouseState"/> instance.
        /// </param>
        /// <returns>
        /// True if both pLeft is equal to pRight; false otherwise.
        /// </returns>
        public static bool operator ==(MouseState pLeft, MouseState pRight)
	    {
	        return pLeft.Equals(pRight);
	    }

	    /// <summary>
	    /// Checks whether two <see cref="MouseState" /> instances are not equal.
	    /// </summary>
	    /// <param name="pLeft">
	    /// A <see cref="MouseState"/> instance.
	    /// </param>
	    /// <param name="pRight">
	    /// A <see cref="MouseState"/> instance.
	    /// </param>
	    /// <returns>
	    /// True if both pLeft is not equal to pRight; false otherwise.
	    /// </returns>
	    public static bool operator !=(MouseState pLeft, MouseState pRight)
	    {
	        return pLeft.Equals(pRight) == false;
	    }

        /// <summary>
        /// Compares to an object instance for equality.
        /// </summary>
        /// <param name="pOther">
        /// The <see cref="System.Object"/> to compare to.
        /// </param>
        /// <returns>
        /// True if this instance is equal to obj; false otherwise.
        /// </returns>
        public override bool Equals(object pOther)
	    {
	        if ( pOther is MouseState )
	        {
		        return this == (MouseState)pOther;
	        }
	        else
	        {
		        return false;
	        }
	    }

	    /// <summary>
	    /// Generates a hashcode for the current instance.
	    /// </summary>
	    /// <returns>
	    /// A <see cref="System.Int32"/> represting the hashcode for this instance.
	    /// </returns>
	    public override int GetHashCode()
	    {
	        return this.mButtons.GetHashCode() ^ X.GetHashCode() ^ Y.GetHashCode() ^ mWheelState.GetHashCode();
	    }

	    /// <summary>
	    /// Returns a <see cref="System.String"/> that represents the current <see cref="OpenTK.Input.MouseState"/>.
	    /// </summary>
	    /// <returns>A <see cref="System.String"/> that represents the current <see cref="OpenTK.Input.MouseState"/>.</returns>
	    public override string ToString()
	    {
	        string b = Convert.ToString(mButtons, 2).PadLeft(10, '0');
	        return String.Format("[X={0}, Y={1}, Scroll={2}, Buttons={3}, IsConnected={4}]",
		                           X, Y, Scroll, b, IsConnected);
	    }

        /// <summary>
        /// Compares two MouseState instances.
        /// </summary>
        /// <param name="pOther">The instance to compare two.</param>
        /// <returns>True, if both instances are equal; false otherwise.</returns>
        public bool Equals(MouseState pOther)
        {
            return this.mButtons == pOther.mButtons &&
                   this.X == pOther.X &&
                   this.Y == pOther.Y &&
                   this.Scroll == pOther.Scroll;
        }

        /// <summary>
        /// Validates the given offset as being in an UShort bounds.
        /// </summary>
        /// <param name="pOffset"></param>
        private static void ValidateOffset(int pOffset)
        {
            if ( pOffset < 0 ||
                 pOffset >= 16 )
            {
                throw new ArgumentOutOfRangeException("offset");
            }
        }

        #endregion Methods
    }
}
