﻿using OrcNet.Core.Thread;
using System;
using System.Diagnostics;
using System.Threading;

namespace OrcNet.UI.Input
{
    /// <summary>
    /// Input manager singleton class definition
    /// Thread-safe
    /// </summary>
    public sealed class InputManager
    {
        #region Fields

        /// <summary>
        /// Stores the curent mouse cursor.
        /// </summary>
        private XCursor mCursor = XCursor.Default;

        /// <summary>
        /// The flag indicating whether the mouse focus is given when mouse is over control or not.
        /// </summary>
		public static bool FocusOnHover = false;

        /// <summary>
        /// Double click threshold in milisecond, that is,
        /// the max duration between two mouse down event for a double click in milisec.
        /// </summary>
		public static int DoubleClick = 200;

        /// <summary>
        /// Time to wait in millisecond before starting repeat loop
        /// </summary>
        public static int DeviceRepeatDelay = 700;

        /// <summary>
        /// Time interval in millisecond between device event repeat
        /// </summary>
        public static int DeviceRepeatInterval = 40;

        /// <summary>
        /// Stores the input manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile InputManager sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the current active UIElement the mouse has been pressed on.
        /// </summary>
        private FrameworkElement mActiveElement;

        /// <summary>
        /// Stores the UIElement having the mouse is hover.
        /// </summary>
        private FrameworkElement mHoverElement;

        /// <summary>
        /// Stores the UIElement having the focus.
        /// </summary>
        private FrameworkElement mFocusedElement;

        /// <summary>
        /// Stores the current interface
        /// </summary>
        private PresentationSource mCurrentInterface;

        /// <summary>
        /// Stores the mouse state.
        /// </summary>
        private MouseState mMouse;

        /// <summary>
        /// Stores the keyboard state.
        /// </summary>
        private KeyboardState mKeyboard;

        /// <summary>
        /// Stores the flag indicating whether the mouse repeat is on or not.
        /// </summary>
        private volatile bool mIsMouseRepeatOn;

        /// <summary>
        /// Stores the flag indicating whether the keyboard repeat is on or not.
        /// </summary>
        private volatile bool mIsKeyboardRepeatOn;

        /// <summary>
        /// Stores the mouse repeat count.
        /// </summary>
        private volatile int mMouseRepeatCount;

        /// <summary>
        /// Stores the keyboard repeat count.
        /// </summary>
        private volatile int mKeyboardRepeatCount;

        /// <summary>
        /// Stores the mouse repeat thread.
        /// </summary>
        private Thread mMouseRepeatThread;

        /// <summary>
        /// Stores the keyboard repeat thread.
        /// </summary>
        private Thread mKeyboardRepeatThread;

        /// <summary>
        /// Stores the last keyboard key down event arguments.
        /// </summary>
        private KeyboardKeyEventArgs mLastKeyDownArgs;

        /// <summary>
        /// Stores the double click timer.
        /// </summary>
        private Stopwatch mClickTimer;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on cursor changes.
        /// </summary>
        public event EventHandler<MouseCursorChangedEventArgs> CursorChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the current mouse cursor.
        /// </summary>
        public XCursor MouseCursor
        {
            get
            {
                return this.mCursor;
            }
            set
            {
                if ( value == this.mCursor )
                {
                    return;
                }

                XCursor lOldCursor = this.mCursor;

                this.mCursor = value;

                this.NotifyCursorChanged( lOldCursor, this.mCursor );
            }
        }

        /// <summary>
        /// Gets the input manager handle.
        /// </summary>
        public static InputManager Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if
                    ( sInstance == null )
                {
                    // Lock on
                    lock
                        ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if
                            ( sInstance == null )
                        {
                            sInstance = new InputManager();
                        }
                    }
                }

                return sInstance;
            }
        }

        /// <summary>
        /// Gets or sets the current interface
        /// </summary>
        internal PresentationSource CurrentInterface
        {
            get
            {
                return this.mCurrentInterface;
            }
            set
            {
                this.mCurrentInterface = value;
            }
        }

        /// <summary>
        /// Gets the mouse state.
        /// </summary>
        public MouseState Mouse
        {
            get
            {
                return this.mMouse;
            }
        }

        /// <summary>
        /// Gets the keyboard state.
        /// </summary>
        public KeyboardState Keyboard
        {
            get
            {
                return this.mKeyboard;
            }
        }

        /// <summary>
        /// Gets the current active UIElement the mouse has been pressed on.
        /// </summary>
        public UIElement ActiveElement
		{
			get
            {
                return this.mActiveElement;
            }
			internal set
			{
                if ( this.mActiveElement != value )
                {
                    if ( this.mActiveElement != null )
                    {
                        this.mActiveElement.IsActive = false;
                    }

                    this.mActiveElement = value as FrameworkElement;

                    if ( this.mActiveElement != null )
                    {
                        this.mActiveElement.IsActive = true;
                    }
                }
			}
		}

        /// <summary>
        /// Gets the UIElement having the mouse is hover.
        /// </summary>
        public UIElement HoverElement
		{
			get
            {
                return this.mHoverElement;
            }
			internal set
            {
				if ( this.mHoverElement != value )
                {
                    this.mHoverElement = value as FrameworkElement;

                    // Modify the current cursor regarding to the element the
                    // mouse is over.
                    if ( this.mHoverElement != null )
                    {
                        this.MouseCursor = this.mHoverElement.Cursor;
                    }
                }
			}
		}

        /// <summary>
        /// Gets the UIElement having the focus.
        /// </summary>
        public UIElement FocusElement
        {
			get
            {
                return this.mFocusedElement;
            }
			internal set
            {
                if ( this.mFocusedElement != value )
                {
                    if ( this.mFocusedElement != null )
                    {
                        this.mFocusedElement.IsFocused = false;
                    }

                    this.mFocusedElement = value as FrameworkElement;

                    if ( this.mFocusedElement != null )
                    {
                        this.mFocusedElement.IsFocused = true;
                    }
                }
			}
		}

        /// <summary>
        /// Gets or sets the current UIElement eligible for double click event.
        /// </summary>
        internal UIElement DoubleClickCandidate
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="InputManager"/> class.
        /// </summary>
        static InputManager()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InputManager"/> class.
        /// </summary>
        private InputManager()
        {
            this.DoubleClickCandidate = null;
            this.mClickTimer = new Stopwatch();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether in the double click temporal region or not.
        /// </summary>
        /// <returns>True if can be a double click candidate, false otherwise.</returns>
        internal bool CanBeDoubleClickCandidate()
        {
            if ( this.mClickTimer.ElapsedMilliseconds < DoubleClick )
            {
                return true;
            }
            
            return false;
        }

        /// <summary>
        /// Checks whether the user has double clicked on the given element or not.
        /// </summary>
        /// <param name="pCandidate">The candidate element.</param>
        /// <returns>True if double clicked on the candidate, false otherwise.</returns>
        internal bool IsDoubleClicking(UIElement pCandidate)
        {
            if ( this.DoubleClickCandidate == pCandidate && 
                 this.mClickTimer.ElapsedMilliseconds < DoubleClick )
            {
                return true;
            }

            this.mClickTimer.Restart();
            return false;
        }

        /// <summary>
        /// Processes repeat event(s) if any.
        /// </summary>
        internal void ProcessRepeatEvents()
        {
            if ( this.mMouseRepeatCount > 0 )
            {
                int lCount = this.mMouseRepeatCount;
                this.mMouseRepeatCount -= lCount;
                for ( int lCurr = 0; lCurr < lCount; lCurr++ )
                {
                    this.FocusElement.OnMouseClick( this, new MouseButtonEventArgs( this.Mouse.X, 
                                                                                    this.Mouse.Y, 
                                                                                    MouseButton.Left, 
                                                                                    true ) );
                }
            }

            if ( this.mKeyboardRepeatCount > 0 )
            {
                int lCount = this.mKeyboardRepeatCount;
                this.mKeyboardRepeatCount -= lCount;
                for ( int lCurr = 0; lCurr < lCount; lCurr++ )
                {
                    this.FocusElement.OnKeyDown( this, this.mLastKeyDownArgs );
                }
            }
        }

        /// <summary>
        /// Processes mouse moves.
        /// </summary>
        /// <param name="pX">The screen X component.</param>
        /// <param name="pY">The screen Y component.</param>
        /// <returns>True if inside the interface, false otherwise.</returns>
		public bool ProcessMouseMove(int pX, int pY)
        {
            int lDeltaX = pX - this.mMouse.X;
            int lDeltaY = pY - this.mMouse.Y;
            this.mMouse.X = pX;
            this.mMouse.Y = pY;

            MouseMoveEventArgs lEventArgs = new MouseMoveEventArgs( pX, pY, lDeltaX, lDeltaY );
            lEventArgs.Mouse = this.mMouse;

            if ( this.mActiveElement != null )
            {
                //TODO, ensure object is still in the graphic tree
                //send move evt even if mouse move outside bounds
                this.mActiveElement.OnMouseMove( this, lEventArgs);
                return true;
            }

            if ( this.mHoverElement != null )
            {
                ////TODO, ensure object is still in the graphic tree
                ////check topmost graphicobject first
                //FrameworkElement lTemp = this.mHoverElement as FrameworkElement;
                //FrameworkElement lTopMost = null;
                //while ( lTemp is FrameworkElement )
                //{
                //    lTopMost = lTemp;
                //    lTemp = lTemp.LogicalParent as FrameworkElement;
                //}

                //int idxhw = this.mCurrentInterface.GraphicTree.IndexOf( lTopMost );
                //if (idxhw != 0)
                //{
                //    int lCurr = 0;
                //    while (lCurr < idxhw)
                //    {
                //        if ( this.mCurrentInterface.GraphicTree[ lCurr ].localLogicalParentIsNull )
                //        {
                //            if ( this.mCurrentInterface.GraphicTree[ lCurr ].IsMouseIn( lEventArgs.Position ) )
                //            {
                //                while ( this.mHoverElement != null )
                //                {
                //                    this.mHoverElement.OnMouseLeave( this.mHoverElement, lEventArgs );
                //                    this.HoverElement = this.mHoverElement.LogicalParent as FrameworkElement;
                //                }

                //                this.mCurrentInterface.GraphicTree[ lCurr ].OnMouseHover( lEventArgs );
                //                return true;
                //            }
                //        }
                //        lCurr++;
                //    }
                //}


                if ( this.mHoverElement.IsMouseIn( lEventArgs.Position ) )
                {
                    this.mHoverElement.OnMouseHover( lEventArgs );
                    return true;
                }
                else
                {
                    this.mHoverElement.OnMouseLeave( this.mHoverElement, lEventArgs );

                    // Seek upward from last focused graph obj's
                    while ( this.mHoverElement.LogicalParent is FrameworkElement )
                    {
                        this.HoverElement = this.mHoverElement.LogicalParent as FrameworkElement;
                        if ( this.mHoverElement.IsMouseIn( lEventArgs.Position ) )
                        {
                            this.mHoverElement.OnMouseHover( lEventArgs );
                            return true;
                        }
                        else
                        {
                            this.mHoverElement.OnMouseLeave( this.mHoverElement, lEventArgs );
                        }
                    }
                }
            }

            // Top level graphic element.
            UIElement lRoot = this.mCurrentInterface.RootVisual as UIElement;
            lock ( lRoot )
            {
                if ( lRoot.IsMouseIn( lEventArgs.Position ) )
                {
                    lRoot.OnMouseHover( lEventArgs );

                    return true;
                }
            }

            this.HoverElement = null;
            return false;
        }

        /// <summary>
        /// Processes mouse button up interactions.
        /// </summary>
        /// <param name="pButton">The released button.</param>
        /// <returns>True if inside the interface, false otherwise.</returns>
        public bool ProcessMouseButtonUp(int pButton)
        {
            this.mMouse.DisableBit( pButton );
            MouseButtonEventArgs lEventArgs = new MouseButtonEventArgs() { Mouse = this.mMouse };

            if ( this.mActiveElement == null )
            {
                return false;
            }

            if ( this.mMouseRepeatThread != null )
            {
                this.mIsMouseRepeatOn = false;
                this.mMouseRepeatThread.Abort();
                this.mMouseRepeatThread.Join();
            }

            this.mActiveElement.OnMouseUp( this.mActiveElement, lEventArgs );
            this.ActiveElement = null;
            return true;
        }

        /// <summary>
        /// Processes mouse button down interactions.
        /// </summary>
        /// <param name="pButton">The pressed button.</param>
        /// <returns>True if inside the interface, false otherwise.</returns>
        public bool ProcessMouseButtonDown(int pButton)
        {
            this.mMouse.EnableBit( pButton );
            MouseButtonEventArgs lEventArgs = new MouseButtonEventArgs() { Mouse = this.mMouse };

            if ( this.mHoverElement == null )
            {
                return false;
            }

            this.mHoverElement.OnMouseDown( this.mHoverElement, new BubblingMouseButtonEventArg( lEventArgs ) );

            if ( this.mFocusedElement == null )
            {
                return true;
            }

            if ( this.mFocusedElement.AllowMouseRepeat == false )
            {
                return true;
            }
            
            this.mMouseRepeatThread = ThreadFactory.CreateThread( "Mouse repeat worker", this.MouseRepeatThreadJob );
            this.mMouseRepeatThread.IsBackground = true;
            this.mMouseRepeatThread.Start();

            return true;
        }

        /// <summary>
        /// Processes mouse wheel changes.
        /// </summary>
        /// <param name="pDelta">The wheel delta value.</param>
        /// <returns>True if inside the interface, false otherwise.</returns>
        public bool ProcessMouseWheelChanged(float pDelta)
        {
            this.mMouse.SetScrollRelative( 0, pDelta );
            MouseWheelEventArgs lEventArgs = new MouseWheelEventArgs() { Mouse = this.mMouse, DeltaPrecise = pDelta };

            if ( this.mHoverElement == null )
            {
                return false;
            }

            this.mHoverElement.OnMouseWheel( this, lEventArgs );

            return true;
        }

        /// <summary>
        /// Processes keyboard key down interactions.
        /// </summary>
        /// <param name="pKey">The pressed key.</param>
        /// <returns>True if inside the interface, false otherwise.</returns>
        public bool ProcessKeyDown(int pKey)
        {
            Key lKey = (Key)pKey;

            this.mKeyboard.SetKeyState( lKey, true );

            if ( this.mFocusedElement == null )
            {
                return false;
            }

            KeyboardKeyEventArgs lEventArgs = this.mLastKeyDownArgs = new KeyboardKeyEventArgs( lKey, false, this.mKeyboard );
            this.mLastKeyDownArgs.IsRepeat = true;

            this.mFocusedElement.OnKeyDown( this, lEventArgs );

            this.mKeyboardRepeatThread = ThreadFactory.CreateThread( "Keyboard repeat worker",  this.KeyboardRepeatThreadJob );
            this.mKeyboardRepeatThread.IsBackground = true;
            this.mKeyboardRepeatThread.Start();

            return true;
        }

        /// <summary>
        /// Processes keyboard key up interactions.
        /// </summary>
        /// <param name="pKey">The released key.</param>
        /// <returns>True if inside the interface, false otherwise.</returns>
        public bool ProcessKeyUp(int pKey)
        {
            Key lKey = (Key)pKey;

            this.mKeyboard.SetKeyState( lKey, false );

            if ( this.mFocusedElement == null )
            {
                return false;
            }

            KeyboardKeyEventArgs lEventArgs = new KeyboardKeyEventArgs( lKey, false, this.mKeyboard );

            this.mFocusedElement.OnKeyUp( this, lEventArgs );

            if ( this.mKeyboardRepeatThread != null )
            {
                this.mIsKeyboardRepeatOn = false;
                this.mKeyboardRepeatThread.Abort();
                this.mKeyboardRepeatThread.Join();
            }

            return true;
        }

        /// <summary>
        /// Processes keyboard any key pressed interactions.
        /// </summary>
        /// <param name="pKey">The pressed key.</param>
        /// <returns>True if inside the interface, false otherwise.</returns>
        public bool ProcessKeyPress(char pKey)
        {
            if ( this.mFocusedElement == null )
            {
                return false;
            }

            KeyPressEventArgs lEventArgs = new KeyPressEventArgs( pKey );

            this.mFocusedElement.OnKeyPress( this, lEventArgs );

            return true;
        }

        /// <summary>
        /// Notifies the cursor has changed.
        /// </summary>
        /// <param name="pOldCursor"></param>
        /// <param name="pNewCursor"></param>
        private void NotifyCursorChanged(XCursor pOldCursor, XCursor pNewCursor)
        {
            if ( this.CursorChanged != null )
            {
                this.CursorChanged( this, new MouseCursorChangedEventArgs( pOldCursor, pNewCursor ) );
            }
        }

        /// <summary>
        /// Mouse repeat thread job.
        /// </summary>
        private void MouseRepeatThreadJob()
        {
            this.mIsMouseRepeatOn = true;
            Thread.Sleep( InputManager.DeviceRepeatDelay );
            while ( this.mIsMouseRepeatOn )
            {
                this.mMouseRepeatCount++;
                Thread.Sleep( InputManager.DeviceRepeatInterval );
            }

            this.mMouseRepeatCount = 0;
        }

        /// <summary>
        /// Keyboard repeat thread job.
        /// </summary>
        private void KeyboardRepeatThreadJob()
        {
            this.mIsKeyboardRepeatOn = true;
            Thread.Sleep( InputManager.DeviceRepeatDelay );
            while ( this.mIsKeyboardRepeatOn )
            {
                this.mKeyboardRepeatCount++;
                Thread.Sleep( InputManager.DeviceRepeatInterval );
            }

            this.mKeyboardRepeatCount = 0;
        }

        #endregion Methods
    }
}
