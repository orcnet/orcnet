﻿using OpenTK;
using System;
using System.Runtime.InteropServices;

namespace OrcNet.UI
{
    /// <summary>
    /// Double utilities class definition.
    /// </summary>
    internal static class DoubleUtil
    {
        #region Constants

        /// <summary>
        /// Stores the smallest pValue such that 1.0+DBL_EPSILON != 1.0
        /// </summary>
        internal const double DBL_EPSILON = 2.2204460492503131e-016;

        /// <summary>
        /// Stores the float number close to zero, where float.MinValue is -float.MaxValue
        /// </summary>
        internal const float FLT_MIN = 1.175494351e-38F;

        #endregion Constants

        #region Methods

        /// <summary>
        /// Checks whether the double is finite or not.
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        public static bool IsFinite(double pValue)
        {
            return (double.IsInfinity( pValue ) ||
                    double.IsNaN( pValue )) == false;
        }

        /// <summary> 
        /// AreClose - Returns whether or not two doubles are "close".  That is, whether or 
        /// not they are within epsilon of each other.  Note that this epsilon is proportional
        /// to the numbers themselves to that AreClose survives scalar multiplication. 
        /// There are plenty of ways for this to return false even for numbers which
        /// are theoretically identical, so no code calling this should fail to work if this
        /// returns false.  This is important enough to repeat:
        /// NB: NO CODE CALLING THIS FUNCTION SHOULD DEPEND ON ACCURATE RESULTS - this should be 
        /// used for optimizations *only*.
        /// </summary>
        /// <param name="pValue1">The first double to compare.</param>
        /// <param name="pValue2">The second double to compare.</param>
        /// <returns>True if close, false otherwise</returns>
        public static bool AreClose(double pValue1, double pValue2)
        {
            //in case they are Infinities (then epsilon check does not work)
            if (pValue1 == pValue2)
                return true;

            // This computes (|pValue1-pValue2| / (|pValue1| + |pValue2| + 10.0)) < DBL_EPSILON 
            double lEspilon = (System.Math.Abs(pValue1) + System.Math.Abs(pValue2) + 10.0) * DBL_EPSILON;
            double lDelta = pValue1 - pValue2;
            return (-lEspilon < lDelta) && (lEspilon > lDelta);
        }

        /// <summary> 
        /// LessThan - Returns whether or not the first double is less than the second double.
        /// That is, whether or not the first is strictly less than *and* not within epsilon of 
        /// the other number.  Note that this epsilon is proportional to the numbers themselves 
        /// to that AreClose survives scalar multiplication.  Note,
        /// There are plenty of ways for this to return false even for numbers which 
        /// are theoretically identical, so no code calling this should fail to work if this
        /// returns false.  This is important enough to repeat:
        /// NB: NO CODE CALLING THIS FUNCTION SHOULD DEPEND ON ACCURATE RESULTS - this should be
        /// used for optimizations *only*. 
        /// </summary>
        /// <param name="pValue1">The first double to compare.</param>
        /// <param name="pValue2">The second double to compare.</param>
        /// <returns>True if less than, false otherwise</returns>
        public static bool LessThan(double pValue1, double pValue2)
        {
            return (pValue1 < pValue2) && !AreClose(pValue1, pValue2);
        }


        /// <summary>
        /// GreaterThan - Returns whether or not the first double is greater than the second double. 
        /// That is, whether or not the first is strictly greater than *and* not within epsilon of
        /// the other number.  Note that this epsilon is proportional to the numbers themselves
        /// to that AreClose survives scalar multiplication.  Note,
        /// There are plenty of ways for this to return false even for numbers which 
        /// are theoretically identical, so no code calling this should fail to work if this
        /// returns false.  This is important enough to repeat: 
        /// NB: NO CODE CALLING THIS FUNCTION SHOULD DEPEND ON ACCURATE RESULTS - this should be 
        /// used for optimizations *only*.
        /// </summary>
        /// <param name="pValue1">The first double to compare.</param>
        /// <param name="pValue2">The second double to compare.</param>
        /// <returns>True if greater than, false otherwise.</returns>
        public static bool GreaterThan(double pValue1, double pValue2)
        {
            return (pValue1 > pValue2) && !AreClose(pValue1, pValue2);
        }

        /// <summary>
        /// LessThanOrClose - Returns whether or not the first double is less than or close to
        /// the second double.  That is, whether or not the first is strictly less than or within 
        /// epsilon of the other number.  Note that this epsilon is proportional to the numbers
        /// themselves to that AreClose survives scalar multiplication.  Note, 
        /// There are plenty of ways for this to return false even for numbers which 
        /// are theoretically identical, so no code calling this should fail to work if this
        /// returns false.  This is important enough to repeat: 
        /// NB: NO CODE CALLING THIS FUNCTION SHOULD DEPEND ON ACCURATE RESULTS - this should be
        /// used for optimizations *only*.
        /// </summary>
        /// <param name="pValue1">The first double to compare.</param>
        /// <param name="pValue2">The second double to compare.</param>
        /// <returns>True if less than or close, false otherwise</returns>
        public static bool LessThanOrClose(double pValue1, double pValue2)
        {
            return (pValue1 < pValue2) || AreClose(pValue1, pValue2);
        }

        /// <summary>
        /// GreaterThanOrClose - Returns whether or not the first double is greater than or close to 
        /// the second double.  That is, whether or not the first is strictly greater than or within 
        /// epsilon of the other number.  Note that this epsilon is proportional to the numbers
        /// themselves to that AreClose survives scalar multiplication.  Note, 
        /// There are plenty of ways for this to return false even for numbers which
        /// are theoretically identical, so no code calling this should fail to work if this
        /// returns false.  This is important enough to repeat:
        /// NB: NO CODE CALLING THIS FUNCTION SHOULD DEPEND ON ACCURATE RESULTS - this should be 
        /// used for optimizations *only*.
        /// </summary>
        /// <param name="pValue1">The first double to compare.</param>
        /// <param name="pValue2">The second double to compare.</param>
        /// <returns>True if greater than or close, false otherwise</returns>
        public static bool GreaterThanOrClose(double pValue1, double pValue2)
        {
            return (pValue1 > pValue2) || AreClose(pValue1, pValue2);
        }

        /// <summary>
        /// IsOne - Returns whether or not the double is "close" to 1.  Same as AreClose(double, 1), 
        /// but this is faster.
        /// </summary>
        /// <param name="pValue">The double to compare to 1.</param>
        /// <returns>True if equals to one, false otherwise</returns>
        public static bool IsOne(double pValue)
        {
            return System.Math.Abs(pValue - 1.0) < 10.0 * DBL_EPSILON;
        }

        /// <summary>
        /// IsZero - Returns whether or not the double is "close" to 0.  Same as AreClose(double, 0), 
        /// but this is faster.
        /// </summary>
        /// <param name="pValue">The double to compare to 0.</param>
        /// <returns>True if equals to zero, false otherwise</returns>
        public static bool IsZero(double pValue)
        {
            return System.Math.Abs(pValue) < 10.0 * DBL_EPSILON;
        }
        
        /// <summary>
        /// Compares two Vector instances for fuzzy equality.  This function
        /// helps compensate for the fact that double values can 
        /// acquire error when operated upon
        /// </summary> 
        /// <param name="pVector1">The first Vector to compare.</param>
        /// <param name="pVector2">The second Vector to compare.</param>
        /// <returns>Whether or not the two Vector instances are equal</returns> 
        public static bool AreClose(Vector2d pVector1, Vector2d pVector2)
        {
            return DoubleUtil.AreClose(pVector1.X, pVector2.X) &&
                   DoubleUtil.AreClose(pVector1.Y, pVector2.Y);
        }

        /// <summary> 
        /// Compares two rectangles for fuzzy equality.  This function
        /// helps compensate for the fact that double values can 
        /// acquire error when operated upon
        /// </summary>
        /// <param name="pRectangle1">The first rectangle to compare.</param>
        /// <param name="pRectangle2">The second rectangle to compare.</param>
        /// <returns>Whether or not the two rectangles are equal</returns>
        public static bool AreClose(Rectangle pRectangle1, Rectangle pRectangle2)
        {
            // If they're both empty, don't bother with the double logic.
            if (pRectangle1.IsEmpty)
            {
                return pRectangle2.IsEmpty;
            }

            // At this point, pRectangle1 isn't empty, so the first thing we can test is
            // pRectangle2.IsEmpty, followed by property-wise compares. 

            return (pRectangle2.IsEmpty == false) &&
                    DoubleUtil.AreClose(pRectangle1.X, pRectangle2.X) &&
                    DoubleUtil.AreClose(pRectangle1.Y, pRectangle2.Y) &&
                    DoubleUtil.AreClose(pRectangle1.Height, pRectangle2.Height) &&
                    DoubleUtil.AreClose(pRectangle1.Width, pRectangle2.Width);
        }

        /// <summary> 
        /// Checks whether the value is between zero and one.
        /// </summary>
        /// <param name="pValue">The value to test.</param>
        /// <returns>True if between, false otherwise.</returns>
        public static bool IsBetweenZeroAndOne(double pValue)
        {
            return (GreaterThanOrClose(pValue, 0) && LessThanOrClose(pValue, 1));
        }

        /// <summary> 
        /// Converts a double value into an integer.
        /// </summary> 
        /// <param name="pValue">The double to convert.</param>
        /// <returns>The integer version.</returns>
        public static int DoubleToInt(double pValue)
        {
            return (0 < pValue) ? (int)(pValue + 0.5) : (int)(pValue - 0.5);
        }


        /// <summary> 
        /// rectHasNaN - this returns true if this rect has X, Y , Height or Width as NaN.
        /// </summary>
        /// <param name="pRectangle">The rectangle to test</param>
        /// <returns>returns whether the Rect has NaN</returns> 
        public static bool RectHasNaN(Rectangle pRectangle)
        {
            if ( DoubleUtil.IsNaN(pRectangle.X) || 
                 DoubleUtil.IsNaN(pRectangle.Y) || 
                 DoubleUtil.IsNaN(pRectangle.Height) || 
                 DoubleUtil.IsNaN(pRectangle.Width))
            {
                return true;
            }
            return false;
        }


#if !PBTCOMPILER

        /// <summary>
        /// Nan union structure definition.
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        private struct NanUnion
        {
            /// <summary>
            /// Double value.
            /// </summary>
            [FieldOffset(0)]
            internal double DoubleValue;

            /// <summary>
            /// Unsigned int value.
            /// </summary>
            [FieldOffset(0)]
            internal UInt64 UintValue;
        }

        /// <summary>
        /// The standard CLR double.IsNaN() function is approximately 100 times slower than our own wrapper, 
        /// so please make sure to use DoubleUtil.IsNaN() in performance sensitive code.
        /// PS item that tracks the CLR improvement is DevDiv Schedule : 26916.
        /// IEEE 754 : If the argument is any pValue in the range 0x7ff0000000000001L through 0x7fffffffffffffffL
        /// or in the range 0xfff0000000000001L through 0xffffffffffffffffL, the result will be NaN. 
        /// </summary>
        /// <param name="pValue">The value to test.</param>
        /// <returns>True if NaN, false otherwise.</returns>
        public static bool IsNaN(double pValue)
        {
            NanUnion t = new NanUnion();
            t.DoubleValue = pValue;

            UInt64 exp = t.UintValue & 0xfff0000000000000;
            UInt64 man = t.UintValue & 0x000fffffffffffff;

            return (exp == 0x7ff0000000000000 || exp == 0xfff0000000000000) && (man != 0);
        }
#endif 
        #endregion Methods
    }
}
