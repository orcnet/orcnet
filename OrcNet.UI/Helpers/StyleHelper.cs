﻿using OrcNet.UI.Ximl;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="StyleHelper"/> class.
    /// 
    /// TO DO: Implement it so it manages Style and Template from Ximl to UI elements.
    /// </summary>
    public static class StyleHelper
    {
        #region Fields

        /// <summary>
        /// Stores the styles.
        /// </summary>
        internal static readonly Dictionary<string, Ximl.Style> Styles = new Dictionary<string, Ximl.Style>();

        /// <summary>
        /// Stores the templates.
        /// </summary>
        internal static readonly Dictionary<string, FrameworkTemplate> Templates = new Dictionary<string, FrameworkTemplate>();

        /// <summary>
        /// Stores the set of all children by name created from Style/Template
        /// </summary>
        internal static readonly Dictionary<string, UIElement> TemplatedChildren = new Dictionary<string, UIElement>();

        #endregion Fields

        #region Methods
        
        /// <summary>
        /// Looks for the element in the template content having the given name.
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pTemplate"></param>
        /// <returns>The element, null otherwise.</returns>
        internal static object FindNameInTemplateContent(string pName, FrameworkTemplate pTemplate)
        {
            UIElement lElement;
            if( TemplatedChildren.TryGetValue( pName, out lElement ) )
            {
                return lElement;
            }

            return null;
        }

        /// <summary>
        /// Gets the theme style corresponding to the given element.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        internal static Ximl.Style GetThemeStyle(FrameworkElement pElement)
        {
            Debug.Assert( pElement != null );

            // Fetch the DefaultStyleKey and the self Style for
            // the given Framework[Content]Element
            string lThemeStyleKey = null;
            Ximl.Style lSelfStyle = null;
            Ximl.Style lOldThemeStyle = null;
            Ximl.Style lNewThemeStyle = null;
            if ( pElement != null )
            {
                lThemeStyleKey = pElement.DefaultStyleKey;
                lSelfStyle = pElement.Style;

                lOldThemeStyle = pElement.ThemeStyle;
            }
            
            if ( lThemeStyleKey != null )
            {
                // Fetch the DependencyObjectType for the ThemeStyleKey
                Type lTypeKey = null;
                if ( pElement != null)
                {
                    lTypeKey = pElement.GetType();
                }

                // First look for an applicable style in system resources
                Ximl.Style lStyleLookup = null;
                if ( lTypeKey != null )
                {
                    if ( Styles.TryGetValue( lTypeKey.FullName, out lStyleLookup ) )
                    {
                        lNewThemeStyle = lStyleLookup;
                    }
                }
                
            }

            // Propagate change notification
            if ( lOldThemeStyle != lNewThemeStyle )
            {
                if ( pElement != null )
                {
                    pElement.OnThemeStyleChanged( lOldThemeStyle, lNewThemeStyle );
                }
            }

            return lNewThemeStyle;
        }

        /// <summary>
        /// Update the theme style cache.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pOldThemeStyle"></param>
        /// <param name="pNewThemeStyle"></param>
        /// <param name="pThemeStyle"></param>
        internal static void UpdateThemeStyleCache(FrameworkElement pElement, Ximl.Style pOldThemeStyle, Ximl.Style pNewThemeStyle, ref Ximl.Style pThemeStyle)
        {
            Debug.Assert( pElement != null );

            if ( pNewThemeStyle != null )
            {
                pNewThemeStyle.CheckTargetType( pElement );
                pNewThemeStyle.Prepare();
            }

            pThemeStyle = pNewThemeStyle;
        }

        #endregion Methods
    }
}
