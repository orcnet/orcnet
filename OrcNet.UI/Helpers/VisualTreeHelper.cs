﻿namespace OrcNet.UI.Helpers
{
    /// <summary>
    /// Definition of the <see cref="VisualTreeHelper"/> helper class.
    /// </summary>
    public static class VisualTreeHelper
    {
        #region Methods

        /// <summary>
        /// Gets the visual parent of the given object.
        /// </summary>
        /// <param name="pReference"></param>
        /// <returns></returns>
        public static AVisual GetParent(AVisual pReference)
        {
            if ( pReference == null )
            {
                return null;
            }
            
            return pReference.VisualParent;
        }

        #endregion Methods
    }
}
