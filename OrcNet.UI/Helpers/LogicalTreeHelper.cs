﻿using OrcNet.Core.Datastructures;
using System;
using System.Collections;

namespace OrcNet.UI.Helpers
{
    /// <summary>
    /// Definition of the <see cref="LogicalTreeHelper"/> helper class.
    /// </summary>
    public static class LogicalTreeHelper
    {
        #region Fields

        /// <summary>
        /// Empty enumerator.
        /// </summary>
        private static readonly EmptyEnumerator<object> sEmptyEnumerator = new EmptyEnumerator<object>();

        #endregion Fields

        #region Methods

        /// <summary>
        /// Get the logical parent of the given object.
        /// </summary>
        public static AVisual GetParent(AVisual pCurrent)
        {
            if ( pCurrent == null )
            {
                throw new ArgumentNullException("pCurrent");
            }

            FrameworkElement lElement = pCurrent as FrameworkElement;
            if ( lElement != null )
            {
                return lElement.LogicalParent;
            }

            //FrameworkContentElement fce = current as FrameworkContentElement;
            //if (fce != null)
            //{
            //    return fce.Parent;
            //}

            return null;
        }

        /// <summary>
        /// Adds a logical child.
        /// </summary>
        /// <param name="pParent"></param>
        /// <param name="pChild"></param>
        internal static void AddLogicalChild(AVisual pParent, object pChild)
        {
            if ( pChild != null && 
                 pParent != null )
            {
                FrameworkElement lParentElement = pParent as FrameworkElement;
                if ( lParentElement != null )
                {
                    lParentElement.AddLogicalChild( pChild );
                }
                //else
                //{
                //    FrameworkContentElement parentFCE = parent as FrameworkContentElement;
                //    if (parentFCE != null)
                //    {
                //        parentFCE.AddLogicalChild(child);
                //    }
                //}
            }
        }

        /// <summary>
        /// Adds a logical child.
        /// </summary>
        /// <param name="pParent"></param>
        /// <param name="pChild"></param>
        internal static void AddLogicalChild(FrameworkElement pParent/*, FrameworkContentElement parentFCE*/, object pChild)
        {
            if ( pChild != null )
            {
                if ( pParent != null )
                {
                    pParent.AddLogicalChild( pChild );
                }
                //else if (parentFCE != null)
                //{
                //    parentFCE.AddLogicalChild(child);
                //}
            }
        }

        /// <summary>
        /// Removes the logicla child
        /// </summary>
        /// <param name="pParent"></param>
        /// <param name="pChild"></param>
        internal static void RemoveLogicalChild(AVisual pParent, object pChild)
        {
            if ( pChild != null && 
                 pParent != null )
            {
                FrameworkElement lParentElement = pParent as FrameworkElement;
                if ( lParentElement != null )
                {
                    lParentElement.RemoveLogicalChild( pChild );
                }
                //else
                //{
                //    FrameworkContentElement parentFCE = parent as FrameworkContentElement;
                //    if (parentFCE != null)
                //    {
                //        parentFCE.RemoveLogicalChild(child);
                //    }
                //}
            }
        }

        /// <summary>
        /// Removes the logicla child
        /// </summary>
        /// <param name="pParent"></param>
        /// <param name="pChild"></param>
        internal static void RemoveLogicalChild(FrameworkElement pParent/*, FrameworkContentElement parentFCE*/, object pChild)
        {
            if ( pChild != null )
            {
                if ( pParent != null )
                {
                    pParent.RemoveLogicalChild( pChild );
                }
                //else
                //{
                //    parentFCE.RemoveLogicalChild(child);
                //}
            }
        }

        /// <summary>
        /// Gets the logical children of the given element.
        /// </summary>
        /// <param name="pCurrent"></param>
        /// <returns></returns>
        internal static IEnumerator GetLogicalChildren(AVisual pCurrent)
        {
            FrameworkElement lElement = pCurrent as FrameworkElement;
            if ( lElement != null )
            {
                return lElement.LogicalChildren;
            }

            //FrameworkContentElement fce = current as FrameworkContentElement;
            //if (fce != null)
            //{
            //    return fce.LogicalChildren;
            //}

            return sEmptyEnumerator;
        }

        /// <summary>
        /// Gets the logical children of the given element.
        /// </summary>
        /// <param name="pCurrent"></param>
        /// <returns></returns>
        public static IEnumerable GetChildren(AVisual pCurrent)
        {
            if ( pCurrent == null )
            {
                throw new ArgumentNullException("pCurrent");
            }

            FrameworkElement lElement = pCurrent as FrameworkElement;
            if ( lElement != null )
            {
                return new EnumeratorWrapper( lElement.LogicalChildren );
            }

            //FrameworkContentElement fce = pCurrent as FrameworkContentElement;
            //if (fce != null)
            //{
            //    return new EnumeratorWrapper(fce.LogicalChildren);
            //}

            return EnumeratorWrapper.Empty;
        }

        #endregion Methods

        #region Inner Classes

        /// <summary>
        /// Definition of the <see cref="EnumeratorWrapper"/> class.
        /// </summary>
        private class EnumeratorWrapper : IEnumerable
        {
            #region Fields

            /// <summary>
            /// Stores the empty enumerator wrapper.
            /// </summary>
            private static EnumeratorWrapper sEmptyInstance;

            /// <summary>
            /// Stores the wrapped enumerator.
            /// </summary>
            private IEnumerator mEnumerator;

            #endregion Fields

            #region Properties

            /// <summary>
            /// Gets the empty enumerator wrapper.
            /// </summary>
            internal static EnumeratorWrapper Empty
            {
                get
                {
                    if ( sEmptyInstance == null )
                    {
                        sEmptyInstance = new EnumeratorWrapper( null );
                    }

                    return sEmptyInstance;
                }
            }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="EnumeratorWrapper"/> class.
            /// </summary>
            /// <param name="pEnumerator"></param>
            public EnumeratorWrapper(IEnumerator pEnumerator)
            {
                if ( pEnumerator != null )
                {
                    this.mEnumerator = pEnumerator;
                }
                else
                {
                    this.mEnumerator = sEmptyEnumerator;
                }
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// Gets the enumerator.
            /// </summary>
            /// <returns></returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.mEnumerator;
            }

            #endregion Methods
        }

        #endregion Inner Classes
    }
}
