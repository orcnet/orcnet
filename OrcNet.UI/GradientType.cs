﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="GradientType"/> enumeration.
    /// </summary>
    public enum GradientType
    {
        /// <summary>
        /// Vertical gradient.
        /// </summary>
        Vertical,

        /// <summary>
        /// Horizontal gradient.
        /// </summary>
        Horizontal,

        /// <summary>
        /// Oblic gradient.
        /// </summary>
        Oblic,

        /// <summary>
        /// Radial gradient.
        /// </summary>
        Radial
    }
}
