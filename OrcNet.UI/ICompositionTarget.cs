﻿using Cairo;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ICompositionTarget"/> interface.
    /// </summary>
    public interface ICompositionTarget : IDisposable
    {
        #region Methods

        /// <summary>
        /// Renders the visual tree.
        /// </summary>
        void Render(Context pContext);

        /// <summary>
        /// Updates the target content on resize.
        /// </summary>
        void OnResize();
        
        #endregion Methods
    }
}
