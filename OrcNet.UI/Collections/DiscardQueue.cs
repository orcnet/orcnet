﻿using System.Collections.Generic;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DiscardQueue"/> class.
    /// </summary>
    internal sealed class DiscardQueue : Queue<LayoutItem>
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the queue has any element or not.
        /// </summary>
        public bool Any
        {
            get
            {
                return this.Count > 0;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the queue is empty or not.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.Any == false;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DiscardQueue"/> class.
        /// </summary>
        public DiscardQueue()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DiscardQueue"/> class.
        /// </summary>
        /// <param name="pItems"></param>
        public DiscardQueue(IEnumerable<LayoutItem> pItems) :
        base( pItems )
        {

        }

        #endregion Constructor
    }
}
