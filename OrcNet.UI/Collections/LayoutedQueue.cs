﻿using OrcNet.Core.Datastructures;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="LayoutedQueue"/> class.
    /// </summary>
    internal sealed class LayoutedQueue : UniqueQueue<AVisual>
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the queue has any element or not.
        /// </summary>
        public bool Any
        {
            get
            {
                return this.Count > 0;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the queue is empty or not.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.Any == false;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LayoutedQueue"/> class.
        /// </summary>
        public LayoutedQueue()
        {

        }
        
        #endregion Constructor
    }
}
