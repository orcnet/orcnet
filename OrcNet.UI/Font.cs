﻿
namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Font"/> class.
    /// </summary>
	public class Font
	{
        #region Fields

        /// <summary>
        /// Stores the font name.
        /// </summary>
        private string mName = "droid";

        /// <summary>
        /// Stores the font size.
        /// </summary>
        private int mSize = 12;

        /// <summary>
        /// Stores the font style.
        /// </summary>
        private FontStyle mStyle   = FontStyle.Normal;

        /// <summary>
        /// Stores the font weight.
        /// </summary>
        private FontWeight mWeight = FontWeight.Normal;

        #endregion Fields
        
        #region Properties

        /// <summary>
        /// Gets or sets the font name.
        /// </summary>
        public string Name
        {
			get
            {
                return this.mName;
            }
			set
            {
                this.mName = value;
            }
		}

        /// <summary>
        /// Gets or sets the font size.
        /// </summary>
		public int Size
        {
			get
            {
                return this.mSize;
            }
			set
            {
                this.mSize = value;
            }
		}

        /// <summary>
        /// Gets or sets the font style.
        /// </summary>
		public FontStyle Style
        {
			get
            {
                return this.mStyle;
            }
			set
            {
                this.mStyle = value;
            }
		}

        /// <summary>
        /// Gets or sets the font weight.
        /// </summary>
		public FontWeight Weight
        {
			get
            {
                return this.mWeight;
            }
			set
            {
                this.mWeight = value;
            }
		}

        /// <summary>
        /// Gets the cairo corresponding font style value.
        /// </summary>
		internal Cairo.FontSlant Slant
        {
			get
            { 
				switch ( this.mStyle )
                {
                    case FontStyle.Normal:
                    default:
                        return Cairo.FontSlant.Normal;
                    case FontStyle.Oblique:
                        return Cairo.FontSlant.Oblique;
                    case FontStyle.Italic:
                        return Cairo.FontSlant.Italic;
				}			
			}
		}

        /// <summary>
        /// Gets the cairo corresponding font weight value.
        /// </summary>
		internal Cairo.FontWeight Wheight
        {
			get
            { 
				switch ( this.mWeight )
                {
				case FontWeight.Bold:
					return Cairo.FontWeight.Bold;
				case FontWeight.Normal:
				default:
					return Cairo.FontWeight.Normal;
				}			
			}
		}

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Font"/> class.
        /// </summary>
        public Font()
        {

        }

        #endregion Constructor

        #region Methods

  //      /// <summary>
  //      /// Inplicit cast from font to string.
  //      /// </summary>
  //      /// <param name="pFont"></param>
  //      public static implicit operator string(Font pFont)
		//{
		//	return pFont.ToString();
		//}

  //      /// <summary>
  //      /// Implicit cast from string to Font.
  //      /// </summary>
  //      /// <param name="s"></param>
		//public static implicit operator Font(string s)
		//{
		//	Font lFont = new Font();

		//	if ( string.IsNullOrEmpty(s) == false )
  //          {
		//		string[] c = s.TrimStart().TrimEnd().Split (new char[] { ',' });

  //              if (c.Length == 2)
  //              {
  //                  lFont.Size = int.Parse(c[1].TrimStart());
  //              }

		//		string[] n = c [0].TrimEnd().Split (new char[] { ' ' });

  //              lFont.Name = n [0];

  //              if (n.Length > 1)
  //              {
  //                  lFont.Style = (FontStyle)Enum.Parse(typeof(FontStyle), n[n.Length - 1], true);
  //              }
		//	}

		//	return lFont;
		//}
        
  //      /// <summary>
  //      /// Turns this font into a string.
  //      /// </summary>
  //      /// <returns></returns>
		//public override string ToString()
		//{
  //          return string.Format( "{0}|{1}|{2}|{3}", this.mName, this.mSize, this.mStyle, this.mWeight );
		//}

  //      /// <summary>
  //      /// Parses the given string into a font.
  //      /// </summary>
  //      /// <param name="pFont"></param>
  //      /// <returns></returns>
		//public static object Parse(string pFont)
		//{
		//	return (Font)pFont;
		//}

        #endregion Methods
    }
}

