﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Size"/> structure.
    /// </summary>
    public struct Size
    {
        #region Statics

        /// <summary>
        /// The Size type.
        /// </summary>
        internal static Type TSize = typeof(Size);

        #endregion Statics

        #region Fields

        /// <summary>
        /// Stores the width.
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the height.
        /// </summary>
        private int mHeight;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the Zero size.
        /// </summary>
        public static Size Zero
        {
            get
            {
                return new Size(0, 0);
            }
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public int Width
        {
            get
            {
                return mWidth;
            }
            set
            {
                mWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public int Height
        {
            get
            {
                return mHeight;
            }
            set
            {
                mHeight = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Size"/> class.
        /// </summary>
        /// <param name="pWidth">The width</param>
        /// <param name="pHeight">The height</param>
        public Size(int pWidth, int pHeight)
        {
            mWidth  = pWidth;
            mHeight = pHeight;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Size"/> class.
        /// </summary>
        /// <param name="pUniformSize">The uniform size.</param>
		public Size(int pUniformSize)
		{
			mWidth  = pUniformSize;
			mHeight = pUniformSize;
		}

        #endregion Constructor

        #region Methods

        #region operators

        /// <summary>
        /// Turns a Size into a rectangle.
        /// </summary>
        /// <param name="s"></param>
        public static implicit operator Rectangle(Size s)
		{
			return new Rectangle(s);
		}

        /// <summary>
        /// Creates a Size from a scalar.
        /// </summary>
        /// <param name="i"></param>
        public static implicit operator Size(int i)
        {
            return new Size(i, i);
        }

        /// <summary>
        /// Turns a Size into a string.
        /// </summary>
        /// <param name="s"></param>
		public static implicit operator string(Size s)
		{
			return s.ToString ();
		}

        /// <summary>
        /// Parses a string to create a Size.
        /// </summary>
        /// <param name="s"></param>
		public static implicit operator Size(string s)
		{
			return string.IsNullOrEmpty (s) ? Size.Zero : Parse (s);
		}

        /// <summary>
        /// Checks whether a Size is equal from another or not.
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
		public static bool operator ==(Size s1, Size s2)
        {
            if (s1.Width == s2.Width && s1.Height == s2.Height)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Checks whether a Size is different from another or not.
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool operator !=(Size s1, Size s2)
        {
            if (s1.Width == s2.Width && s1.Height == s2.Height)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Checks whether a Size is greater to another or not.
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool operator >(Size s1, Size s2)
        {
            if (s1.Width > s2.Width && s1.Height > s2.Height)
                return true;
            else
                return false;
        }
        /// <summary>
        /// Checks whether a Size is greater or equal to another or not.
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool operator >=(Size s1, Size s2)
        {
            if (s1.Width >= s2.Width && s1.Height >= s2.Height)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Checks whether a Size is smaller to another or not.
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool operator <(Size s1, Size s2)
        {
            if (s1.Width < s2.Width)
                if (s1.Height <= s2.Height)
                    return true;
                else
                    return false;
            else if (s1.Width == s2.Width && s1.Height < s2.Height)
                return true;

            return false;
        }

        /// <summary>
        /// Checks whether Size components are smaller to a scalar or not.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="i"></param>
        /// <returns></returns>
		public static bool operator <(Size s, int i)
		{
			return s.Width < i && s.Height < i ? true : false;
		}

        /// <summary>
        /// Checks whether Size components are greater to a scalar or not.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static bool operator >(Size s, int i)
        {
            return s.Width > i && s.Height > i ? true : false;
        }

        /// <summary>
        /// Checks whether Size components are smaller or equal to a scalar or not.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static bool operator <=(Size s, int i)
		{
			return s.Width <= i && s.Height <= i ? true : false;
		}

        /// <summary>
        /// Checks whether Size components are greater or equal to a scalar or not.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static bool operator >=(Size s, int i)
        {
            return s.Width >= i && s.Height >= i ? true : false;
        }

        /// <summary>
        /// Checks whether a Size is smaller or equal to another or not.
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool operator <=(Size s1, Size s2)
        {
            if (s1.Width <= s2.Width && s1.Height <= s2.Height)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Checks whether Size components are equal to a scalar
        /// </summary>
        /// <param name="s"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static bool operator ==(Size s, int i)
        {
            if (s.Width == i && s.Height == i)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Checks whether Size components are different to a scalar
        /// </summary>
        /// <param name="s"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static bool operator !=(Size s, int i)
        {
            if (s.Width == i && s.Height == i)
                return false;
            else
                return true;
		}

        /// <summary>
        /// Adds two Size together.
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
		public static Size operator +(Size s1, Size s2)
		{
			return new Size(s1.Width + s2.Width, s1.Height + s2.Height);
		}

        /// <summary>
        /// Adds a scalar to a size.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static Size operator +(Size s, int i)
        {
            return new Size(s.Width + i, s.Height + i);
        }

		#endregion

        /// <summary>
        /// Gets the instance hashcode.
        /// </summary>
        /// <returns></returns>
		public override int GetHashCode()
		{
			unchecked // Overflow is fine, just wrap
			{
				int hash = 17;
				// Suitable nullity checks etc, of course :)
				hash = hash * 23 + mWidth.GetHashCode();
				hash = hash * 23 + mHeight.GetHashCode();
				return hash;
			}
		}

        /// <summary>
        /// Checks whether the given instance is equal to this one.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
		public override bool Equals(object obj)
		{
			return (obj == null || obj.GetType() != TSize) ?
				false :
				this == (Size)obj;
		}

        /// <summary>
        /// Turns this instance into a string.
        /// </summary>
        /// <returns>The string.</returns>
		public override string ToString()
		{
			return string.Format("{0},{1}", Width, Height);
		}

        /// <summary>
        /// Parses the given string into a Size.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
		public static Size Parse(string s)
		{
			string[] d = s.Split(new char[] { ',' });
			return d.Length == 1 ? new Size(int.Parse(d[0])) : new Size(
				int.Parse(d[0]),
				int.Parse(d[1]));
		}

        #endregion Methods
    }
}
