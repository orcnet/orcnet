﻿using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="XCursorFile"/> class.
    /// </summary>
	internal sealed class XCursorFile
	{
        #region Constant

        /// <summary>
        /// XC_TYPE_IMG constant.
        /// </summary>
        private const uint XC_TYPE_IMG = 0xfffd0002;

        #endregion Constant

        #region Properties

        /// <summary>
        /// Gets or sets the set of cursors.
        /// </summary>
        public List<XCursor> Cursors { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Loads the cursor(s).
        /// </summary>
        /// <param name="pFilePath"></param>
        /// <returns></returns>
        public static XCursorFile Load(string pFilePath)
		{
            if ( File.Exists( pFilePath ) == false )
            {
                return null;
            }

            XCursorFile lFile = null;
            using ( Stream lStream = new FileStream( pFilePath, FileMode.Open, FileAccess.Read, FileShare.Read ) )
            {
                lFile = LoadFromStream( lStream );
            }

            return lFile;
		}

        /// <summary>
        /// Loads cursors from stream.
        /// </summary>
        /// <param name="pStream"></param>
        /// <returns></returns>
        private static XCursorFile LoadFromStream(Stream pStream)
        {
            List<toc> lTocList = new List<toc>();
            XCursorFile lFile = new XCursorFile();

            using ( BinaryReader lReader = new BinaryReader( pStream ) )
            {
                //magic: CARD32 ’Xcur’ (0x58, 0x63, 0x75, 0x72)
                if (new string(lReader.ReadChars(4)) != "Xcur")
                {
                    Debug.WriteLine("XCursor Load error: Wrong magic");
                    return null;
                }
                //header: CARD32 bytes in this header
                uint headerLength = lReader.ReadUInt32();
                //version: CARD32 file version number
                uint version = lReader.ReadUInt32();
                //ntoc: CARD32 number of toc entries
                uint nbToc = lReader.ReadUInt32();
                //toc: LISTofTOC table of contents
                for (uint i = 0; i < nbToc; i++)
                {
                    lTocList.Add(new toc(lReader));
                }

                foreach (toc t in lTocList)
                {
                    if (t.type != XC_TYPE_IMG)
                    {
                        continue;
                    }

                    lReader.BaseStream.Seek(t.pos, SeekOrigin.Begin);
                    lFile.Cursors.Add( LoadImage( lReader ) );
                }
            }

            return lFile;
        }

        /// <summary>
        /// Loads a cursor image from reader.
        /// </summary>
        /// <param name="pReader"></param>
        /// <returns></returns>
        private static XCursor LoadImage(BinaryReader pReader)
		{
			XCursor lCursor = new XCursor();
			//			header: 36 Image headers are 36 bytes
			uint header = pReader.ReadUInt32();
			//			type: 0xfffd0002 Image type is 0xfffd0002
			uint type = pReader.ReadUInt32();
			//			subtype: CARD32 Image subtype is the nominal size
			uint subtype = pReader.ReadUInt32();
			//			version: 1
			uint version = pReader.ReadUInt32();
			//			width: CARD32 Must be less than or equal to 0x7fff
			lCursor.Width = pReader.ReadUInt32();
			//			height: CARD32 Must be less than or equal to 0x7fff
			lCursor.Height = pReader.ReadUInt32();
			//			xhot: CARD32 Must be less than or equal to width
			lCursor.Xhot = pReader.ReadUInt32();
			//			yhot: CARD32 Must be less than or equal to height
			lCursor.Yhot = pReader.ReadUInt32();
			//			delay: CARD32 Delay between animation frames in milliseconds
			lCursor.Delay = pReader.ReadUInt32();
			//			pixels: LISTofCARD32 Packed ARGB format pixels
			lCursor.data = pReader.ReadBytes((int)(lCursor.Width * lCursor.Height * 4));

			return lCursor;
		}

        #endregion Methods
        
        #region Inner Classes

        /// <summary>
        /// Definition of the <see cref="toc"/> class.
        /// </summary>
        private class toc
        {
            #region Properties

            /// <summary>
            /// Gets or sets the type.
            /// </summary>
            public uint type { get; set; }

            /// <summary>
            /// Gets or sets the sub type.
            /// </summary>
            public uint subtype { get; set; }

            /// <summary>
            /// Gets or sets the position index.
            /// </summary>
            public uint pos { get; set; }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="toc"/> class.
            /// </summary>
            /// <param name="sr"></param>
            public toc(BinaryReader sr)
            {
                type = sr.ReadUInt32();
                subtype = sr.ReadUInt32();
                pos = sr.ReadUInt32();
            }

            #endregion Constructor
        }

        #endregion Inner Classes
    }

    /// <summary>
    /// Definition of the <see cref="XCursor"/> class.
    /// </summary>
	public sealed class XCursor
	{
        #region Constants

        /// <summary>
        /// The default cursor.
        /// </summary>
        public static XCursor Default;

        /// <summary>
        /// The cross cursor.
        /// </summary>
		public static XCursor Cross;

        /// <summary>
        /// The arrow cursor.
        /// </summary>
		public static XCursor Arrow;

        /// <summary>
        /// The text cursor.
        /// </summary>
		public static XCursor Text;

        /// <summary>
        /// The SW cursor.
        /// </summary>
		public static XCursor SW;

        /// <summary>
        /// The SE cursor.
        /// </summary>
		public static XCursor SE;

        /// <summary>
        /// The NW cursor.
        /// </summary>
		public static XCursor NW;

        /// <summary>
        /// The NE cursor.
        /// </summary>
		public static XCursor NE;

        /// <summary>
        /// The N cursor.
        /// </summary>
		public static XCursor N;

        /// <summary>
        /// The S cursor.
        /// </summary>
		public static XCursor S;

        /// <summary>
        /// The V cursor.
        /// </summary>
		public static XCursor V;

        /// <summary>
        /// The H cursor.
        /// </summary>
		public static XCursor H;

        #endregion Constants

        #region Properties

        /// <summary>
        /// The cursor width.
        /// </summary>
		public uint Width;

        /// <summary>
        /// The cursor height.
        /// </summary>
		public uint Height;

        /// <summary>
        /// The cursor X hit position.
        /// </summary>
		public uint Xhot;

        /// <summary>
        /// The cursor Y hit position.
        /// </summary>
		public uint Yhot;

        /// <summary>
        /// The cursor delay.
        /// </summary>
		public uint Delay;

        /// <summary>
        /// The cursor data.
        /// </summary>
		public byte[] data;

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="XCursor"/> class.
        /// </summary>
        static XCursor()
        {
            XCursor.Cross = XCursorFile.Load("#Crow.Images.Icons.Cursors.cross").Cursors[0];
            XCursor.Default = XCursorFile.Load("#Crow.Images.Icons.Cursors.arrow").Cursors[0];
            XCursor.NW = XCursorFile.Load("#Crow.Images.Icons.Cursors.top_left_corner").Cursors[0];
            XCursor.NE = XCursorFile.Load("#Crow.Images.Icons.Cursors.top_right_corner").Cursors[0];
            XCursor.SW = XCursorFile.Load("#Crow.Images.Icons.Cursors.bottom_left_corner").Cursors[0];
            XCursor.SE = XCursorFile.Load("#Crow.Images.Icons.Cursors.bottom_right_corner").Cursors[0];
            XCursor.H = XCursorFile.Load("#Crow.Images.Icons.Cursors.sb_h_double_arrow").Cursors[0];
            XCursor.V = XCursorFile.Load("#Crow.Images.Icons.Cursors.sb_v_double_arrow").Cursors[0];
            XCursor.Text = XCursorFile.Load("#Crow.Images.Icons.Cursors.ibeam").Cursors[0];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XCursor"/> class.
        /// </summary>
        public XCursor()
		{

		}

        #endregion Constructor
    }
}

