﻿using OpenTK;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="PresentationTarget"/> class.
    /// </summary>
    public class PresentationTarget : ACompositionTarget
    {
        #region Fields

        /// <summary>
        /// Stores the native window handle.
        /// </summary>
        private INativeWindow mHandle;

        /// <summary>
        /// Stores the background color drawn before anything else. 
        /// Clear color of rendering APIs.
        /// </summary>
        private Color mBackgroundColor;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the background color drawn before anything else. 
        /// Clear color of rendering APIs.
        /// </summary>
        public Color BackgroundColor
        {
            get
            {
                return this.mBackgroundColor;
            }
            set
            {
                if ( this.mBackgroundColor == value )
                {
                    return;
                }

                this.mBackgroundColor = value;
            }
        }

        /// <summary>
        /// Gets the matrix that can be used to transform coordinates from
        /// the rendering destination device to this target.
        /// </summary>
        public override Matrix4d TransformFromDevice
        {
            get
            {
                return Matrix4d.Identity;
            }
        }

        /// <summary>
        /// Gets the matrix that can be used to transform coordinates from this
        /// target to the rendering destination device.
        /// </summary>
        public override Matrix4d TransformToDevice
        {
            get
            {
                return Matrix4d.Identity;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PresentationTarget"/> class.
        /// </summary>
        /// <param name="pHandle">The native window handle.</param>
        public PresentationTarget(INativeWindow pHandle)
        {
            this.mHandle = pHandle;

            this.GetWindowBounds();

            MediaContext.RegisterCompositionTarget( this.Dispatcher, this );
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Gets the window bounds.
        /// </summary>
        private void GetWindowBounds()
        {
            this.WorldClipBounds = new Rectangle( this.mHandle.Left, 
                                                  this.mHandle.Top, 
                                                  this.mHandle.Width, 
                                                  this.mHandle.Height );
        }

        /// <summary>
        /// Delegate called on target resizing.
        /// </summary>
        internal void OnResize()
        {
            MediaContext lDrawingContext = MediaContext.From( this.Dispatcher );

            this.GetWindowBounds();

            lDrawingContext.Resize( this.WorldClipBounds );
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDispose()
        {
            this.RootVisual = null;

            MediaContext.UnregisterCompositionTarget( this.Dispatcher, this );

            base.OnDispose();
        }

        #endregion Methods
    }
}
