﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="SizeToContent"/> enumeration.
    /// </summary>
    public enum SizeToContent
    {
        /// <summary>
        /// Does not size to content
        /// </summary>
        Manual = 0,

        /// <summary>
        /// Sizes Width to content's Width
        /// </summary>        
        Width = 1,

        /// <summary>
        /// Sizes Height to content's Height
        /// </summary>
        Height = 2,

        /// <summary>
        /// Sizes both Width and Height to content's size
        /// </summary>
        Both = 3,
    }
}
