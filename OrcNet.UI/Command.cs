﻿using System;
using OrcNet.Core;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Command"/> class.
    /// </summary>
	public class Command : AObservable, ICommand
	{
        #region Fields

        /// <summary>
        /// Stores the command action to execute.
        /// </summary>
        private Action<object> mExecuteAction;

        /// <summary>
        /// Stores the action determining whether the command can be executed or not.
        /// </summary>
        private Func<object, bool> mCanExecuteAction;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on CanExecute changes.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the command action to execute.
        /// </summary>
        internal Action<object> ExecuteAction
        {
            get
            {
                return this.mExecuteAction;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mExecuteAction, value, "ExecuteAction" );
            }
        }

        /// <summary>
        /// Gets or sets the action determining whether the command can be executed or not.
        /// </summary>
        internal Func<object, bool> CanExecuteAction
        {
            get
            {
                return this.mCanExecuteAction;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCanExecuteAction, value, "CanExecuteAction" );

                this.OnCanExecuteChanged();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Command"/> class.
        /// </summary>
        public Command() :
        this( null, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Command"/> class.
        /// </summary>
        /// <param name="pExecuteAction">The action to execute.</param>
        /// <param name="pCanExecuteAction">The can execute action.</param>
        public Command(Action<object> pExecuteAction, Func<object, bool> pCanExecuteAction)
		{
			this.mExecuteAction = pExecuteAction;
            this.mCanExecuteAction = pCanExecuteAction;
		}

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Checks whether the command can be executed or not.
        /// </summary>
        /// <param name="pParameter">The optional parameter.</param>
        /// <returns>True if can execute, false otherwise.</returns>
        public bool CanExecute(object pParameter)
        {
            if ( this.mCanExecuteAction != null )
            {
                return this.mCanExecuteAction( pParameter );
            }

            return true;
        }

        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="pParameter">The optional parameter.</param>
        public void Execute(object pParameter)
        {
            if ( this.mExecuteAction != null &&
                 this.CanExecuteAction( pParameter ) )
            {
                this.mExecuteAction( pParameter );
            }
        }

        /// <summary>
        /// Delegate called on can execute state changes.
        /// </summary>
        protected internal void OnCanExecuteChanged()
        {
            if ( this.CanExecuteChanged != null )
            {
                this.CanExecuteChanged( this, new EventArgs() );
            }
        }

        #endregion Methods
    }
}
