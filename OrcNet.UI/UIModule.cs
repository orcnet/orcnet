﻿using OrcNet.Core;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="UIModule"/> class.
    /// </summary>
    internal sealed class UIModule : AModule
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UIModule"/> class.
        /// </summary>
        public UIModule()
        {

        }

        #endregion Constructor
    }
}
