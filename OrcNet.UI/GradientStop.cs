﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="GradientStop"/> class.
    /// </summary>
    public sealed class GradientStop : ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the gradient stop within the gradient vector.
        /// </summary>
        private double mOffset;

        /// <summary>
        /// Stores the color of the gradient stop.
        /// </summary>
        private Color mColor;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the gradient stop within the gradient vector.
        /// </summary>
        public double Offset
        {
            get
            {
                return this.mOffset;
            }
            set
            {
                this.mOffset = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the gradient stop.
        /// </summary>
        public Color Color
        {
            get
            {
                return this.mColor;
            }
            set
            {
                this.mColor = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GradientStop"/> class.
        /// </summary>
        /// <param name="pOffset">The gradient stop within the gradient vector.</param>
        /// <param name="pColor">The color of the gradient stop.</param>
        public GradientStop(double pOffset, Color pColor)
        {
            this.mOffset = pOffset;
            this.mColor = pColor;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Parses the gradient stop from string.
        /// </summary>
        /// <param name="pGradientAsString"></param>
        /// <returns></returns>
        public static object Parse(string pGradientAsString)
        {
            if (string.IsNullOrEmpty(pGradientAsString))
            {
                return null;
            }

            string[] lParts = pGradientAsString.Trim().Split(':');

            if ( lParts.Length > 2 )
            {
                throw new Exception("too many parameters in color stop: " + pGradientAsString);
            }

            if ( lParts.Length == 2 )
            {
                return new GradientStop( double.Parse( lParts[0] ), (Color)lParts[1] );
            }

            return new GradientStop( -1, (Color)lParts[0] );
        }

        /// <summary>
        /// Clones the instance.
        /// </summary>
        /// <returns>The cloned instance.</returns>
        public object Clone()
        {
            return new GradientStop( this.mOffset, this.mColor );
        }

        #endregion Methods
    }
}
