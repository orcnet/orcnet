﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="CairoHelpers"/> class.
    /// </summary>
    public static class CairoHelpers
    {
        #region Methods

        /// <summary>
        /// Turns the OrcNet Font style into a Cairo FontSlant
        /// </summary>
        /// <param name="pStyle"></param>
        /// <returns></returns>
        internal static Cairo.FontSlant ToCairo(this FontStyle pStyle)
        {
            switch ( pStyle )
            {
                case FontStyle.Italic:
                    return Cairo.FontSlant.Italic;
                case FontStyle.Oblique:
                    return Cairo.FontSlant.Oblique;
                default:
                    return Cairo.FontSlant.Normal;
            }
        }

        /// <summary>
        /// Turns the OrcNet Font weight into a Cairo FontWeight.
        /// </summary>
        /// <param name="pWeight"></param>
        /// <returns></returns>
        internal static Cairo.FontWeight ToCairo(this FontWeight pWeight)
        {
            switch (pWeight)
            {
                case FontWeight.Bold:
                    return Cairo.FontWeight.Bold;
                default:
                    return Cairo.FontWeight.Normal;
            }
        }

        /// <summary>
        /// Clamps the given value between the two supplied bounding ones.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pValue"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static T Clamp<T>(this T pValue, T min, T max) where T : IComparable<T>
		{
		    if (pValue.CompareTo(min) < 0) return min;
		    else if(pValue.CompareTo(max) > 0) return max;
		    else return pValue;
		}

		/// <summary>
		/// Convert string to utf8 (extension method)
		/// </summary>
		/// <returns>byte array with utf8 encoding</returns>
		public static byte[] ToUtf8(this String str)
		{
			return System.Text.UTF8Encoding.UTF8.GetBytes(str);
		}

        /// <summary>
        /// Gets the minimum value among the set of given values.
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static double min(params double[] arr)
        {
            int minp = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] < arr[minp])
                {
                    minp = i;
                }
            }

            return arr[minp];
        }

        /// <summary>
        /// Draws a rectangle using Cairo 2D.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pRegion"></param>
        /// <param name="pRadius"></param>
        /// <param name="pStroke"></param>
		public static void CairoRectangle(Cairo.Context pContext, Rectangle pRegion, double pRadius, double pStroke = 0.0)
		{
            if (pRadius > 0)
            {
                CairoHelpers.DrawRoundedRectangle(pContext, pRegion, pRadius, pStroke);
            }
            else
            {
                pContext.Rectangle(pRegion, pStroke);
            }
		}

        /// <summary>
        /// Draws a circle using Cairo 2D.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pRegion"></param>
        public static void CairoCircle(Cairo.Context pContext, Rectangle pRegion)
		{
			pContext.Arc(pRegion.X + pRegion.Width/2, pRegion.Y + pRegion.Height/2, Math.Min(pRegion.Width,pRegion.Height)/2, 0, 2*Math.PI);
		}

        /// <summary>
        /// Draws a rounded rectangle using Cairo 2D.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pRegion"></param>
        /// <param name="pRadius"></param>
        /// <param name="pStroke"></param>
		public static void DrawRoundedRectangle(Cairo.Context pContext, Rectangle pRegion, double pRadius, double pStroke = 0.0)
        {
            if (pStroke > 0.0)
            {
                pContext.LineWidth = pStroke;
                double hsw = pStroke / 2.0;
                DrawRoundedRectangle(pContext, pRegion.X + hsw, pRegion.Y + hsw, pRegion.Width - pStroke, pRegion.Height - pStroke, pRadius);
                pContext.Stroke();
            }
            else
            {
                DrawRoundedRectangle(pContext, pRegion.X, pRegion.Y, pRegion.Width, pRegion.Height, pRadius);
            }
        }

        /// <summary>
        /// Draws a rounded rectangle using Cairo 2D.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        /// <param name="pWidth"></param>
        /// <param name="height"></param>
        /// <param name="pRadius"></param>
        public static void DrawRoundedRectangle(Cairo.Context pContext, double pX, double pY, double pWidth, double height, double pRadius)
        {
            pContext.Save();

            if ((pRadius > height / 2) || (pRadius > pWidth / 2))
            {
                pRadius = min(height / 2, pWidth / 2);
            }

            pContext.MoveTo(pX, pY + pRadius);
            pContext.Arc(pX + pRadius, pY + pRadius, pRadius, Math.PI, -Math.PI / 2);
            pContext.LineTo(pX + pWidth - pRadius, pY);
            pContext.Arc(pX + pWidth - pRadius, pY + pRadius, pRadius, -Math.PI / 2, 0);
            pContext.LineTo(pX + pWidth, pY + height - pRadius);
            pContext.Arc(pX + pWidth - pRadius, pY + height - pRadius, pRadius, 0, Math.PI / 2);
            pContext.LineTo(pX + pRadius, pY + height);
            pContext.Arc(pX + pRadius, pY + height - pRadius, pRadius, Math.PI / 2, Math.PI);
            pContext.ClosePath();
            pContext.Restore();
        }

        /// <summary>
        /// Strokes a raised rectangle.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pRegion"></param>
        /// <param name="pWidth"></param>
        public static void StrokeRaisedRectangle(Cairo.Context pContext, Rectangle pRegion, double pWidth = 1)
        {
            pContext.Save();
            pRegion.Inflate((int)-pWidth / 2, (int)-pWidth / 2);
            pContext.LineWidth = pWidth;
			pContext.SetSourceColor(Color.White);
            pContext.MoveTo(pRegion.BottomLeft);
            pContext.LineTo(pRegion.TopLeft);
            pContext.LineTo(pRegion.TopRight);
            pContext.Stroke();

			pContext.SetSourceColor(Color.DarkGray);
            pContext.MoveTo(pRegion.TopRight);
            pContext.LineTo(pRegion.BottomRight);
            pContext.LineTo(pRegion.BottomLeft);
            pContext.Stroke();

            pContext.Restore();
        }

        /// <summary>
        /// Strokes a lowered rectangle.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pRegion"></param>
        /// <param name="pWidth"></param>
        public static void StrokeLoweredRectangle(Cairo.Context pContext, Rectangle pRegion, double pWidth = 1)
        {
            pContext.Save();
            pRegion.Inflate((int)-pWidth / 2, (int)-pWidth / 2);
            pContext.LineWidth = pWidth;
			pContext.SetSourceColor(Color.DarkGray);
            pContext.MoveTo(pRegion.BottomLeft);
            pContext.LineTo(pRegion.TopLeft);
            pContext.LineTo(pRegion.TopRight);
            pContext.Stroke();
			pContext.SetSourceColor(Color.White);
            pContext.MoveTo(pRegion.TopRight);
            pContext.LineTo(pRegion.BottomRight);
            pContext.LineTo(pRegion.BottomLeft);
            pContext.Stroke();

            pContext.Restore();
        }

        #endregion Methods
    }
}
