﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Size changed delegate prototype.
    /// </summary>
    /// <param name="pWindow"></param>
    /// <param name="pNewSize"></param>
    public delegate void SizeChangedDelegate(INativeWindow pWindow, Size pNewSize);

    /// <summary>
    /// Visibility changed delegate prototype.
    /// </summary>
    /// <param name="pWindow"></param>
    /// <param name="pIsVisible"></param>
    public delegate void VisibilityDelegate(INativeWindow pWindow, bool pIsVisible);

    /// <summary>
    /// Close delegate prototype.
    /// </summary>
    /// <param name="pWindow"></param>
    public delegate void CloseDelegate(INativeWindow pWindow);

    /// <summary>
    /// Activate delegate prototype.
    /// </summary>
    /// <param name="pWindow"></param>
    public delegate void ActivateDelegate(INativeWindow pWindow);

    /// <summary>
    /// Definition of the <see cref="NativeWindowWrapper"/> class.
    /// </summary>
    internal sealed class NativeWindowWrapper : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the wrapper has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the parameters.
        /// </summary>
        private NativeWindowParameters mParameters;

        /// <summary>
        /// Stores the native window instance.
        /// </summary>
        private INativeWindow mWindow;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on native window size changes.
        /// </summary>
        public event SizeChangedDelegate NativeSizeChanged;

        /// <summary>
        /// Event fired on native window closing.
        /// </summary>
        public event CloseDelegate NativeClose;

        /// <summary>
        /// Event fired on native window activated.
        /// </summary>
        public event ActivateDelegate NativeActivate;

        /// <summary>
        /// Event fired on native window visibility changes.
        /// </summary>
        public event VisibilityDelegate NativeVisibility;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the native window instance.
        /// </summary>
        internal INativeWindow Window
        {
            get
            {
                return this.mWindow;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the native window is minimized or not.
        /// </summary>
        internal bool IsMinimized
        {
            get
            {
                return this.mWindow.IsMinimized;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the native window is maximized or not.
        /// </summary>
        internal bool IsMaximized
        {
            get
            {
                return this.mWindow.IsMaximized;
            }
        }

        /// <summary>
        /// Gets the window size.
        /// </summary>
        internal Size WindowSize
        {
            get
            {
                return new Size( this.mWindow.Width, this.mWindow.Height );
            }
        }

        /// <summary>
        /// Gets the styles flags.
        /// </summary>
        internal int StyleFlags
        {
            get
            {
                return this.mWindow.StyleFlags;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the source window is active or not.
        /// </summary>
        internal bool IsActive
        {
            get
            {
                return this.mWindow.IsActive;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the window has currently the focus or not.
        /// </summary>
        public bool HasFocus
        {
            get
            {
                return this.mWindow.HasFocus;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowWrapper"/> class.
        /// </summary>
        /// <param name="pParameters">The source window parameters.</param>
        public NativeWindowWrapper(NativeWindowParameters pParameters)
        {
            this.mIsDisposed = false;
            this.mParameters = pParameters;
            this.CreateWindow( this.mParameters );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Shows the window.
        /// </summary>
        public void Show()
        {

        }

        /// <summary>
        /// Hides the window.
        /// </summary>
        public void Hide()
        {

        }

        /// <summary>
        /// Sets the new window position and/or size.
        /// </summary>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        /// <param name="pWidth"></param>
        /// <param name="pHeight"></param>
        /// <param name="pMove">The flag indicating whether the window must be moved to the new X/Y position.</param>
        /// <param name="pResize">The flag indicating whether the window must be resized to the new Width/Height.</param>
        public void SetWindowPosition(int pX, int pY, int pWidth, int pHeight, bool pMove, bool pResize)
        {
            if ( pMove )
            {

            }

            if ( pResize )
            {

            }
        }

        /// <summary>
        /// Updates the window title.
        /// </summary>
        /// <param name="pTitle"></param>
        public void UpdateTitle(string pTitle)
        {

        }

        /// <summary>
        /// Restores the window.
        /// </summary>
        public void Restore()
        {
            
        }

        /// <summary>
        /// Minimizes the window.
        /// </summary>
        public void Minimize()
        {
            
        }

        /// <summary>
        /// Maximizes the window.
        /// </summary>
        public void Maximize()
        {
            
        }

        /// <summary>
        /// Creates a window.
        /// </summary>
        /// <param name="pParameters"></param>
        /// <returns></returns>
        private INativeWindow CreateWindow(NativeWindowParameters pParameters)
        {
            return null;
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mIsDisposed == false )
            {


                this.mIsDisposed = true;
            }
        }

        #endregion Methods
    }
}
