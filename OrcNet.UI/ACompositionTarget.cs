﻿using Cairo;
using OpenTK;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ACompositionTarget"/> class.
    /// </summary>
    public abstract class ACompositionTarget : ADispatcherObject, ICompositionTarget
    {
        #region Fields
        
        /// <summary>
        /// Stores the root visual.
        /// </summary>
        private AVisual mRootVisual;

        /// <summary>
        /// Stores the world clip bounds in screen space.
        /// </summary>
        private Rectangle mWorldClipBounds;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the root visual.
        /// </summary>
        public virtual AVisual RootVisual
        {
            get
            {
                return this.mRootVisual;
            }
            set
            {
                if ( this.mRootVisual == value )
                {
                    return;
                }

                this.SetRootVisual( value );

                // Post Render???
            }
        }

        /// <summary>
        /// Gets the world clip bounds in screen space.
        /// </summary>
        internal Rectangle WorldClipBounds
        {
            get
            {
                return this.mWorldClipBounds;
            }
            set
            {
                this.mWorldClipBounds = value;
            }
        }

        /// <summary>
        /// Gets the matrix that can be used to transform coordinates from this
        /// target to the rendering destination device.
        /// </summary>
        public abstract Matrix4d TransformToDevice
        {
            get;
        }

        /// <summary>
        /// Gets the matrix that can be used to transform coordinates from
        /// the rendering destination device to this target.
        /// </summary>
        public abstract Matrix4d TransformFromDevice
        {
            get;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ACompositionTarget"/> class.
        /// </summary>
        protected ACompositionTarget()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Renders the visual tree.
        /// </summary>
        /// <param name="pContext">The drawing context.</param>
        void ICompositionTarget.Render(Context pContext)
        {
            if ( this.mRootVisual != null )
            {
                MediaContext lDrawingContext = MediaContext.From( this.Dispatcher );

                if ( this.mRootVisual is UIElement &&
                     (this.mRootVisual as UIElement).IsVisible == false )
                {
                    return;
                }

                if ( lDrawingContext.CanRender( this.mRootVisual ) == false )
                {
                    return;
                }

                pContext.Save();

                this.mRootVisual.Render( pContext );

                pContext.Restore();
            }
        }

        /// <summary>
        /// Sets the root visual.
        /// </summary>
        /// <param name="pVisual"></param>
        private void SetRootVisual(AVisual pVisual)
        {
            if ( pVisual != null &&
                 (pVisual.VisualParent != null || pVisual.IsRootElement) )
            {
                throw new ArgumentException( "Already has or cannot have a parent." );
            }

            if ( this.mRootVisual != null )
            {
                this.mRootVisual.IsRootElement = false;
            }

            this.mRootVisual = pVisual;

            if ( this.mRootVisual != null )
            {
                this.mRootVisual.IsRootElement = true;
            }
        }

        /// <summary>
        /// Updates the target content on resize.
        /// </summary>
        void ICompositionTarget.OnResize()
        {
            if ( this.mRootVisual != null )
            {
                UIElement lElement = this.mRootVisual as UIElement;
                if ( lElement == null ||
                     lElement.IsVisible == false )
                {
                    return;
                }

                lElement.RegisterForLayouting( LayoutingType.All );
            }
        }

        #endregion Methods
    }
}
