﻿using System.IO;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="SvgImageSource"/> class.
    /// </summary>
	public class SvgImageSource : ImageSource
	{
        #region Fields

        /// <summary>
        /// Stores the Svg image handle.
        /// </summary>
        private Rsvg.Handle mSvg;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SvgImageSource"/> class.
        /// </summary>
        public SvgImageSource()
		{

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvgImageSource"/> class.
        /// </summary>
        /// <param name="pPath">The image path.</param>
		public SvgImageSource(string pPath) : 
        base( pPath )
		{

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Loads an image from stream.
        /// </summary>
        /// <param name="pStream"></param>
        protected override void LoadFromStream(Stream pStream)
		{
            using ( MemoryStream lMemoryStream = new MemoryStream() )
            {
                pStream.CopyTo( lMemoryStream );

                this.mSvg = new Rsvg.Handle( lMemoryStream.ToArray() );

                this.Width  = this.mSvg.Dimensions.Width;
                this.Height = this.mSvg.Dimensions.Height;
            }
		}
        
        /// <summary>
        /// Set the image as drawing source.
        /// </summary>
        /// <param name="pContext">The graphic context</param>
        /// <param name="pBounds">The region in which to draw.</param>
		public override void SetAsSource(Context pContext, Rectangle pBounds = default(Rectangle))
		{
			double lWidthRatio  = 1.0;
			double lHeightRatio = 1.0;

			if ( (this.mStretch & GraphicObjects.Stretch.Fill) == GraphicObjects.Stretch.Fill )
            {
				lWidthRatio  = pBounds.Width / this.Width;
				lHeightRatio = pBounds.Height / this.Height;
			}

			if ( (this.mStretch & GraphicObjects.Stretch.Uniform) == GraphicObjects.Stretch.Uniform )
            {
				if ( lWidthRatio < lHeightRatio )
                {
                    lHeightRatio = lWidthRatio;
                }
				else
                {
                    lWidthRatio = lHeightRatio;
                }
			}

            using ( ImageSurface lSurface = new ImageSurface( Format.Argb32, 
                                                              pBounds.Width, 
                                                              pBounds.Height ) )
            {
                using ( Cairo.Context lContext = new Context( lSurface ) )
                {
                    lContext.Translate(pBounds.Left, pBounds.Top);
                    lContext.Scale(lWidthRatio, lHeightRatio);
                    lContext.Translate( (pBounds.Width / lWidthRatio - this.Width) / 2, 
                                        (pBounds.Height / lHeightRatio - this.Height) / 2 );

                    this.mSvg.Render( lContext );
                }

                pContext.SetSource( lSurface );
            }	
		}
        
        /// <summary>
        /// Renders the image.
        /// </summary>
        /// <param name="pContext">The graphic context.</param>
        /// <param name="pRegion">The region in which to draw.</param>
        /// <param name="pSubPart">The Svg subpart if any.</param>
        public override void Render(Context pContext, Rectangle pRegion, string pSubPart = "")
        {
			double lWidthRatio  = 1.0;
			double lHeightRatio = 1.0;

			if ( (this.mStretch & GraphicObjects.Stretch.Fill) == GraphicObjects.Stretch.Fill )
            {
				lWidthRatio  = pRegion.Width / this.Width;
				lHeightRatio = pRegion.Height / this.Height;
			}

			if ( (this.mStretch & GraphicObjects.Stretch.Uniform) == GraphicObjects.Stretch.Uniform )
            {
                if ( lWidthRatio < lHeightRatio )
                {
                    lHeightRatio = lWidthRatio;
                }
                else
                {
                    lWidthRatio = lHeightRatio;
                }
			}

            pContext.Save();

			pContext.Translate( pRegion.Left, pRegion.Top );
            pContext.Scale( lWidthRatio, lHeightRatio );
            pContext.Translate( (pRegion.Width / lWidthRatio - this.Width) / 2.0, 
                                (pRegion.Height / lHeightRatio - this.Height) / 2.0 );

            if ( string.IsNullOrEmpty( pSubPart ) )
            {
                this.mSvg.Render( pContext );
            }
            else
            {
                this.mSvg.RenderSub( pContext, "#" + pSubPart );
            }

            pContext.Restore();		
		}

        /// <summary>
        /// Clones the Svg image.
        /// </summary>
        /// <returns>The cloned Svg.</returns>
        protected override ABrush InternalClone()
        {
            return new SvgImageSource( this.Path );
        }

        #endregion Methods
    }
}

