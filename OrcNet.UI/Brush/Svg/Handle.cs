namespace Rsvg
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Definition of the <see cref="Handle"/> class.
    /// </summary>
	public class Handle
    {
        #region Extern

        [DllImport("rsvg-2")]
		static extern IntPtr rsvg_handle_new();

		[DllImport("rsvg-2")]
		static extern unsafe IntPtr rsvg_handle_new_from_data(byte[] data, UIntPtr n_data, out IntPtr error);

        [DllImport("rsvg-2")]
        static extern unsafe IntPtr rsvg_handle_new_from_file(string file_name, out IntPtr error);

        [DllImport("rsvg-2")]
        static extern IntPtr rsvg_handle_get_base_uri(IntPtr raw);

        [DllImport("rsvg-2")]
        static extern void rsvg_handle_set_dpi(IntPtr raw, double dpi);

        [DllImport("rsvg-2")]
        static extern void rsvg_handle_render_cairo(IntPtr raw, IntPtr cr);

        [DllImport("rsvg-2")]
        static extern void rsvg_handle_set_dpi_x_y(IntPtr raw, double dpi_x, double dpi_y);

        [DllImport("rsvg-2")]
        static extern void rsvg_handle_get_dimensions(IntPtr raw, IntPtr dimension_data);

        [DllImport("rsvg-2")]
        static extern unsafe bool rsvg_handle_close(IntPtr raw, out IntPtr error);

        [DllImport("rsvg-2")]
        static extern IntPtr rsvg_handle_get_title(IntPtr raw);

        [DllImport("rsvg-2")]
        static extern void rsvg_handle_render_cairo_sub(IntPtr raw, IntPtr cr, string id);

        [DllImport("rsvg-2")]
        static extern IntPtr rsvg_handle_get_metadata(IntPtr raw);

        #endregion Extern

        #region Fields

        /// <summary>
        /// Stores the raw data pointer.
        /// </summary>
        public IntPtr Raw;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the Dpi information.
        /// </summary>
        public double Dpi
        {
            set
            {
                rsvg_handle_set_dpi(Raw, value);
            }
        }

        /// <summary>
        /// Gets the Dimensions informations.
        /// </summary>
        public Rsvg.DimensionData Dimensions
        {
            get
            {
                Rsvg.DimensionData dimension_data;
                IntPtr native_dimension_data = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(Rsvg.DimensionData)));
                rsvg_handle_get_dimensions(Raw, native_dimension_data);
                dimension_data = Rsvg.DimensionData.New(native_dimension_data);
                Marshal.FreeHGlobal(native_dimension_data);
                return dimension_data;
            }
        }

        /// <summary>
        /// Gets the title information.
        /// </summary>
        public string Title
        {
            get
            {
                IntPtr raw_ret = rsvg_handle_get_title(Raw);
                return "not supported";
            }
        }

        /// <summary>
        /// Gets the metadata informations.
        /// </summary>
        public string Metadata
        {
            get
            {
                IntPtr raw_ret = rsvg_handle_get_metadata(Raw);
                return "not supported";
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Handle"/> class.
        /// </summary>
        public Handle()
		{
			Raw = rsvg_handle_new();
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Handle"/> class.
        /// </summary>
        /// <param name="pData"></param>
        public unsafe Handle(byte[] pData)
		{
			if (GetType() != typeof (Handle))
            {
				throw new InvalidOperationException ("Can't override this constructor.");
			}

			IntPtr error = IntPtr.Zero;
			Raw = rsvg_handle_new_from_data( pData, new UIntPtr ((ulong) (pData == null ? 0 : pData.Length)), out error);
            if (error != IntPtr.Zero)
            {
                throw new Exception(error.ToString());
            }
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Handle"/> class.
        /// </summary>
        /// <param name="pFileName"></param>
        public unsafe Handle(string pFileName)
		{
			IntPtr error = IntPtr.Zero;
			Raw = rsvg_handle_new_from_file( pFileName, out error);

            if (error != IntPtr.Zero)
            {
                throw new Exception(error.ToString());
            }
		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Renders the Svg.
        /// </summary>
        /// <param name="pContext"></param>
        public void Render(Cairo.Context pContext)
        {
			unsafe
            {
				rsvg_handle_render_cairo (Raw, pContext == null ? IntPtr.Zero : pContext.Handle);
			}
		}

		/// <summary>
        /// Sets the Svg Dpi info
        /// </summary>
        /// <param name="pDpiX"></param>
        /// <param name="pDpiY"></param>
		public void SetDpiXY(double pDpiX, double pDpiY)
        {
			rsvg_handle_set_dpi_x_y(Raw, pDpiX, pDpiY);
		}

		/// <summary>
        /// Closes the Svg.
        /// </summary>
        /// <returns></returns>
		public unsafe bool Close()
        {
			IntPtr error = IntPtr.Zero;
			bool raw_ret = rsvg_handle_close(Raw, out error);
            if (error != IntPtr.Zero)
            {
                throw new Exception(error.ToString());
            }

			return raw_ret;
		}

        /// <summary>
        /// Renders an Svg sub layer.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pId"></param>
        public void RenderSub(Cairo.Context pContext, string pId)
        {
			rsvg_handle_render_cairo_sub( Raw, pContext == null ? IntPtr.Zero : pContext.Handle, pId );
		}

        #endregion Methods
    }
}
