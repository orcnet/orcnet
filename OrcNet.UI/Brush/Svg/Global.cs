namespace Rsvg
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Definition of the <see cref="Global"/> class.
    /// </summary>
    public class Global
    {
        #region Extern

        [DllImport("rsvg-2")]
		static extern void rsvg_set_default_dpi_x_y(double dpi_x, double dpi_y);

        [DllImport("rsvg-2")]
        static extern int rsvg_error_quark();

        [DllImport("rsvg-2")]
        static extern void rsvg_set_default_dpi(double dpi);

        #endregion Extern

        #region Properties

        /// <summary>
        /// Gets the Error quark.
        /// </summary>
        public static int ErrorQuark
        {
            get
            {
                int raw_ret = rsvg_error_quark();
                int ret = raw_ret;
                return ret;
            }
        }

        /// <summary>
        /// Sets the Svg default Dpi info.
        /// </summary>
        public static double DefaultDpi
        {
            set
            {
                rsvg_set_default_dpi(value);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Sets the default Dpi X and Y info.
        /// </summary>
        /// <param name="dpi_x"></param>
        /// <param name="dpi_y"></param>
        public static void SetDefaultDpiXY(double dpi_x, double dpi_y)
        {
			rsvg_set_default_dpi_x_y(dpi_x, dpi_y);
		}

        #endregion Methods
    }
}
