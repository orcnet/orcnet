namespace Rsvg
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Definition of the <see cref="DimensionData"/> structure.
    /// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public struct DimensionData
    {
        #region Fields

        /// <summary>
        /// Stores the dimensions data width.
        /// </summary>
        public int Width;

        /// <summary>
        /// Stores the dimensions data height.
        /// </summary>
		public int Height;

        /// <summary>
        /// Stores the dimensions data Em.
        /// </summary>
		public double Em;

        /// <summary>
        /// Stores the dimensions data Ex.
        /// </summary>
		public double Ex;

        /// <summary>
        /// Zero dimensions data.
        /// </summary>
		public readonly static Rsvg.DimensionData sZero = new Rsvg.DimensionData();

        #endregion Fields
        
        #region Methods

        /// <summary>
        /// Creates a new dimension data structure from raw pointer.
        /// </summary>
        /// <param name="pRaw">The raw data.</param>
        /// <returns>The dimensions.</returns>
        public static Rsvg.DimensionData New(IntPtr pRaw)
        {
            if ( pRaw == IntPtr.Zero )
            {
                return Rsvg.DimensionData.sZero;
            }

			return (Rsvg.DimensionData)Marshal.PtrToStructure( pRaw, typeof(Rsvg.DimensionData) );
		}

        #endregion Methods
    }
}
