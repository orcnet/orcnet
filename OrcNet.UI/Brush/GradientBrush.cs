﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="GradientBrush"/> class.
    /// </summary>
	public class GradientBrush : ABrush
	{
        #region Fields

        /// <summary>
        /// Stores the gradient type.
        /// </summary>
        private GradientType mGradientType;

        /// <summary>
        /// Stores the set of gradient stop colors.
        /// </summary>
		private GradientStopCollection mGradientStops;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the gradient type.
        /// </summary>
        public GradientType GradientType
        {
            get
            {
                return this.mGradientType;
            }
            set
            {
                this.mGradientType = value;
            }
        }

        /// <summary>
        /// Gets or sets the set of gradient stop colors.
        /// </summary>
		public GradientStopCollection GradientStops
        {
            get
            {
                return this.mGradientStops;
            }
            set
            {
                this.mGradientStops = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GradientBrush"/> class.
        /// </summary>
        public GradientBrush() :
        this( GradientType.Vertical )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GradientBrush"/> class.
        /// </summary>
        /// <param name="pType"></param>
        public GradientBrush(GradientType pType)
		{
			this.mGradientType = pType;
            this.mGradientStops = new GradientStopCollection();
		}

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Set the gradient as current drawing source.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pBounds"></param>
        public override void SetAsSource (Cairo.Context pContext, Rectangle pBounds = default(Rectangle))
		{
			Cairo.Gradient lGradient = null;
			switch ( this.mGradientType )
            {
			case GradientType.Vertical:
				lGradient = new Cairo.LinearGradient( pBounds.Left, pBounds.Top, pBounds.Left, pBounds.Bottom );
				break;
			case GradientType.Horizontal:
				lGradient = new Cairo.LinearGradient( pBounds.Left, pBounds.Top, pBounds.Right, pBounds.Top );
				break;
			case GradientType.Oblic:
				lGradient = new Cairo.LinearGradient( pBounds.Left, pBounds.Top, pBounds.Right, pBounds.Bottom );
				break;
			case GradientType.Radial:
				throw new NotImplementedException ();
			}

            foreach ( GradientStop lStop in this.mGradientStops )
            {
                lGradient.AddColorStop( lStop.Offset, lStop.Color );
            }
			
			pContext.SetSource( lGradient );
			lGradient.Dispose();
		}

        /// <summary>
        /// Parses a gradient from string.
        /// </summary>
        /// <param name="pGradientAsString"></param>
        /// <returns></returns>
        public new static object Parse(string pGradientAsString)
		{
            if ( string.IsNullOrEmpty( pGradientAsString ) )
            {
                return Color.White;
            }

			GradientBrush lGradient;

            string[] lStops = pGradientAsString.Trim().Split('|');
			switch ( lStops[0].Trim() )
            {
			case "vgradient":
                    lGradient = new GradientBrush( GradientType.Vertical );
				break;
			case "hgradient":
                    lGradient = new GradientBrush( GradientType.Horizontal );
				break;
			case "ogradient":
                    lGradient = new GradientBrush( GradientType.Oblic );
				break;
			default:
                    throw new Exception("Unknown gradient type: " + lStops[0]);
			}

            for ( int i = 1; i < lStops.Length; ++i )
            {
                lGradient.mGradientStops.Add( (GradientStop)GradientStop.Parse( lStops[i] ) );
            }

			return lGradient;
		}

        /// <summary>
        /// Clones the gradient.
        /// </summary>
        /// <returns></returns>
        protected override ABrush InternalClone()
        {
            GradientBrush lClone = new GradientBrush( this.GradientType );
            foreach ( GradientStop lStop in this.mGradientStops )
            {
                lClone.mGradientStops.Add( lStop.Clone() as GradientStop );
            }
            return lClone;
        }

        #endregion Methods
    }
}

