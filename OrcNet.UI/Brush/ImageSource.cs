﻿using System.IO;
using Cairo;
using OrcNet.UI.GraphicObjects;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ImageSource"/> class.
    /// </summary>
	public abstract class ImageSource : ABrush
	{
        #region Fields

        /// <summary>
        /// Stores the image path.
        /// </summary>
        private string mPath;

        /// <summary>
        /// Stores the image dimensions.
        /// </summary>
		private Size mDimensions;

        /// <summary>
        /// Stores how the image should be stretched to fill the destination rectangle.
        /// </summary>
        public Stretch mStretch;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the image path.
        /// </summary>
        public string Path
        {
            get
            {
                return this.mPath;
            }
            set
            {
                this.mPath = value;
            }
        }

        /// <summary>
        /// Gets or sets the image width.
        /// </summary>
		public double Width
        {
            get
            {
                return this.mDimensions.Width;
            }
            set
            {
                this.mDimensions.Width = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets the image height.
        /// </summary>
		public double Height
        {
            get
            {
                return this.mDimensions.Height;
            }
            set
            {
                this.mDimensions.Height = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets how the image should be stretched to fill the destination rectangle.
        /// </summary>
        public Stretch Stretch
        {
            get
            {
                return this.mStretch;
            }
            set
            {
                this.mStretch = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageSource"/> class.
        /// </summary>
        public ImageSource()
		{

		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageSource"/> class.
        /// </summary>
        /// <param name="pPath">The image path.</param>
		public ImageSource(string pPath)
		{
			this.LoadImage( pPath );
		}

        #endregion Constructor
        
        #region Methods

        /// <summary>
        /// Loads an image from the given path.
        /// </summary>
        /// <param name="pPath">The image path.</param>
        public void LoadImage(string pPath)
		{
            using ( Stream lStream = new FileStream( pPath, FileMode.Open, FileAccess.Read, FileShare.Read ) )
            {
                this.LoadFromStream( lStream );
            }

			this.mPath = pPath;
		}
			
        /// <summary>
        /// Loads the image from a stream.
        /// </summary>
        /// <param name="pStream">The stream to load from.</param>
		protected abstract void LoadFromStream(Stream pStream);

        /// <summary>
        /// Renders the image.
        /// </summary>
        /// <param name="pContext">The graphic context.</param>
        /// <param name="pRegion">The region in which to draw.</param>
        /// <param name="pSubPart">The Svg subpart if any.</param>
        public abstract void Render(Context pContext, Rectangle pRegion, string pSubPart = "");

        /// <summary>
        /// Implicitely loads an image from path.
        /// </summary>
        /// <param name="pPath">The image path.</param>
		public static implicit operator ImageSource(string pPath)
		{
			if ( string.IsNullOrEmpty( pPath ) )
            {
                return null;
            }
			
			ImageSource lSource = null;

            if ( pPath.EndsWith(".svg", true, System.Globalization.CultureInfo.InvariantCulture) )
            {
                lSource = new SvgImageSource();
            }
            else
            {
                lSource = new BmpImageSource();
            }

            lSource.LoadImage( pPath );			

			return lSource;
		}

        /// <summary>
        /// Implicitely turns an image into a string description.
        /// </summary>
        /// <param name="pSource">The image source.</param>
		public static implicit operator string(ImageSource pSource)
		{
			return pSource == null ? null : pSource.Path;
		}

        /// <summary>
        /// Parses a image from a path.
        /// </summary>
        /// <param name="pPath">The image path.</param>
        /// <returns>The image source.</returns>
		public new static object Parse(string pPath)
		{
			if ( string.IsNullOrEmpty( pPath ) )
            {
                return null;
            }
			
			ImageSource lSource = null;

            if ( pPath.EndsWith(".svg", true, System.Globalization.CultureInfo.InvariantCulture) )
            {
                lSource = new SvgImageSource();
            }
            else
            {
                lSource = new BmpImageSource();
            }

            lSource.LoadImage( pPath );			

			return lSource;
		}

        /// <summary>
        /// Turns this image into a string description.
        /// </summary>
        /// <returns></returns>
		public override string ToString()
		{
			return Path;
		}

        #endregion Methods
    }
}

