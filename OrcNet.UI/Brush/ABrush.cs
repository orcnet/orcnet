﻿using OrcNet.UI.Ximl;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ABrush"/> class.
    /// </summary>
	public abstract class ABrush : IXimlResource
	{
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the brush is the clear color or not.
        /// </summary>
        public virtual bool IsClearColor
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the brush Id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return string.Empty;
            }
        }

        #endregion Properties
        
        #region Methods

        /// <summary>
        /// Set this brush as source brush.
        /// </summary>
        /// <param name="pContext">The drawing context.</param>
        /// <param name="pBounds">The bounding region.</param>
        public abstract void SetAsSource(Cairo.Context pContext, Rectangle pBounds = default(Rectangle));

        /// <summary>
        /// Parse a string into a brush.
        /// </summary>
        /// <param name="pBrushAsString"></param>
        /// <returns></returns>
        public static object Parse(string pBrushAsString)
        {
			if (string.IsNullOrEmpty( pBrushAsString ) )
            {
                return null;
            }
			if ( pBrushAsString.Substring(1).StartsWith ("gradient"))
            {
                return (GradientBrush)GradientBrush.Parse( pBrushAsString );
            }
			if ( pBrushAsString.EndsWith (".svg", true, System.Globalization.CultureInfo.InvariantCulture) )
            {
                return SvgImageSource.Parse( pBrushAsString );
            }
			if ( pBrushAsString.EndsWith (".png", true, System.Globalization.CultureInfo.InvariantCulture)  ||
			     pBrushAsString.EndsWith (".jpg", true, System.Globalization.CultureInfo.InvariantCulture)  ||
			     pBrushAsString.EndsWith (".jpeg", true, System.Globalization.CultureInfo.InvariantCulture) ||
			     pBrushAsString.EndsWith (".bmp", true, System.Globalization.CultureInfo.InvariantCulture)  ||
                 pBrushAsString.EndsWith (".gif", true, System.Globalization.CultureInfo.InvariantCulture) )
            {
                return BmpImageSource.Parse( pBrushAsString );
            }
			
			return (SolidColorBrush)SolidColorBrush.Parse( pBrushAsString );
		}

        /// <summary>
        /// Finds the identifiable having the given Id.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public IIdentifiable Find(string pId)
        {
            return this;
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.OnDisposed();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDisposed()
        {

        }

        /// <summary>
        /// Clone the brush.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return this.InternalClone();
        }

        /// <summary>
        /// Clones the brush.
        /// </summary>
        /// <returns></returns>
        protected abstract ABrush InternalClone();

        /// <summary>
        /// Turns a brush into a color.
        /// </summary>
        /// <param name="pBrush"></param>
        public static implicit operator Color(ABrush pBrush)
        {
			SolidColorBrush sc = pBrush as SolidColorBrush;
			return sc == null ? default(Color) : sc.Color;
		}

        /// <summary>
        /// Turns a color into a brush.
        /// </summary>
        /// <param name="pColor"></param>
		public static implicit operator ABrush(Color pColor)
        {
			return new SolidColorBrush( pColor );
		}

        #endregion Methods
    }
}

