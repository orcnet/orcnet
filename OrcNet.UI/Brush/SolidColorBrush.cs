﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="SolidColorBrush"/> class.
    /// </summary>
	public class SolidColorBrush : ABrush
    {
        #region Fields

        /// <summary>
        /// Stores the brush Identifier.
        /// </summary>
        private string mId;

        /// <summary>
        /// Stores the solid brush Color.
        /// </summary>
        public Color Color = Color.Transparent;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the brush is the clear color or not.
        /// </summary>
        public override bool IsClearColor
        {
            get
            {
                return this.Color == Color.Clear;
            }
        }

        /// <summary>
        /// Gets the brush Id.
        /// </summary>
        public override string Id
        {
            get
            {
                return this.mId;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SolidColorBrush"/> class.
        /// </summary>
        /// <param name="pColor">The Color.</param>
        /// <param name="pBrushKey">The bursh key if needed.</param>
        public SolidColorBrush(Color pColor, string pBrushKey = null)
		{
            Color = pColor;
            
            if ( string.IsNullOrEmpty( pBrushKey ) )
            {
                this.mId = pColor.ToString();
            }
            else
            {
                this.mId = pBrushKey;
            }
		}

        #endregion Constructor

        #region Methods

        #region Methods ABrush

        /// <summary>
        /// Set this brush as source brush.
        /// </summary>
        /// <param name="pContext">The drawing context.</param>
        /// <param name="pBounds">The bounding region.</param>
        public override void SetAsSource(Cairo.Context pContext, Rectangle pBounds = default(Rectangle))
		{
			pContext.SetSourceRGBA( Color.R, Color.G, Color.B, Color.A );
		}

        /// <summary>
        /// Parses a color into a brush.
        /// </summary>
        /// <param name="pColorAsString"></param>
        /// <returns></returns>
		public new static object Parse(string pColorAsString)
		{
			return new SolidColorBrush( (Color)pColorAsString );
		}

        #endregion Methods ABrush

        #region Operators

        /// <summary>
        /// Turns a brush into a Color.
        /// </summary>
        /// <param name="pBrush"></param>
        public static implicit operator Color(SolidColorBrush pBrush)
        {
			return pBrush.Color;
        }

        /// <summary>
        /// Turns a color into a brush.
        /// </summary>
        /// <param name="pColor"></param>
		public static implicit operator SolidColorBrush(Color pColor)
		{
			return new SolidColorBrush( pColor );
		}

        /// <summary>
        /// Checks whether two brushes are equal or not.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
		public static bool operator ==(SolidColorBrush left, SolidColorBrush right)
		{
			return left is SolidColorBrush ? right is SolidColorBrush ? true : false :
				left.Color == right.Color ? true : false;
		}

        /// <summary>
        /// Checks whether two brushes are different or not.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
		public static bool operator !=(SolidColorBrush left, SolidColorBrush right)
		{
			return left is SolidColorBrush ? right is SolidColorBrush ? false : true :
				left.Color == right.Color ? false : true;

		}

        /// <summary>
        /// Gets the brush hash code.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
		{
			return Color.GetHashCode();
		}

        /// <summary>
        /// Checks whether this brush is equal to another.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns></returns>
        public override bool Equals(object pOther)
		{
            if (pOther is Color)
            {
                return this.Color == (Color)pOther;
            }
            else if (pOther is SolidColorBrush)
            {
                return this.Color == (pOther as SolidColorBrush).Color;
            }

			return false;			
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="f"></param>
        /// <returns></returns>
		public static SolidColorBrush operator *(SolidColorBrush c, Double f)
		{
			return new SolidColorBrush( new Color( c.Color.R, c.Color.G, c.Color.B, c.Color.A * f ) );
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
		public static SolidColorBrush operator +(SolidColorBrush c1, SolidColorBrush c2)
		{
			return new SolidColorBrush( new Color( c1.Color.R + c2.Color.R, c1.Color.G + c2.Color.G, c1.Color.B + c2.Color.B, c1.Color.A + c2.Color.A ) );
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
		public static SolidColorBrush operator -(SolidColorBrush c1, SolidColorBrush c2)
		{
			return new SolidColorBrush( new Color( c1.Color.R - c2.Color.R, c1.Color.G - c2.Color.G, c1.Color.B - c2.Color.B, c1.Color.A - c2.Color.A ) );
		}

        #endregion

        /// <summary>
        /// Clones the brush.
        /// </summary>
        /// <returns></returns>
        protected override ABrush InternalClone()
        {
            return new SolidColorBrush( this.Color, this.mId );
        }

        /// <summary>
        /// Turns the brush into a string.
        /// </summary>
        /// <returns></returns>
		public override string ToString()
		{
			return Color.ToString();
		}

        #endregion Methods
    }
}
