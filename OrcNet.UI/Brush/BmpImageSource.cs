﻿using System;
using System.IO;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="BmpImageSource"/> class.
    /// </summary>
	public class BmpImageSource : ImageSource
	{
        #region Fields

        /// <summary>
        /// Stores the image bytes.
        /// </summary>
        private byte[] mImage;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new <see cref="BmpImageSource"/> class.
        /// </summary>
        public BmpImageSource()
		{

        }

        /// <summary>
        /// Initializes a new <see cref="BmpImageSource"/> class.
        /// </summary>
        /// <param name="pPath">The image path.</param>
		public BmpImageSource(string pPath) : 
        base( pPath )
		{

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Loads the image from a stream.
        /// </summary>
        /// <param name="pStream">The stream to load from.</param>
        protected override void LoadFromStream(Stream pStream)
		{
            using ( MemoryStream lMemoryStream = new MemoryStream() )
            {
                pStream.CopyTo( lMemoryStream );
                this.LoadBitmap( new System.Drawing.Bitmap( lMemoryStream ) );
            }
		}

        /// <summary>
        /// Loads the image via System.Drawing.Bitmap as cairo loads png only
        /// </summary>
        /// <param name="pBitmap"></param>
        private void LoadBitmap(System.Drawing.Bitmap pBitmap)
		{
            if ( pBitmap == null )
            {
                return;
            }

			System.Drawing.Imaging.BitmapData lData = pBitmap.LockBits( new System.Drawing.Rectangle( 0, 0, pBitmap.Width, pBitmap.Height ),
					                                                    System.Drawing.Imaging.ImageLockMode.ReadOnly, 
                                                                        System.Drawing.Imaging.PixelFormat.Format32bppArgb );

            this.Width  = pBitmap.Width;
            this.Height = pBitmap.Height;

			int lStride = lData.Stride;
            int lBitmapSize = Math.Abs( lData.Stride ) * pBitmap.Height;

			this.mImage = new byte[ lBitmapSize ];
            System.Runtime.InteropServices.Marshal.Copy( lData.Scan0, this.mImage, 0, lBitmapSize );

            pBitmap.UnlockBits( lData );          
		}
        
        /// <summary>
        /// Set the image as the drawing source.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pBounds"></param>
        public override void SetAsSource(Context pContext, Rectangle pBounds = default(Rectangle))
		{
			double lWidthRatio  = 1.0;
			double lHeightRatio = 1.0;

			if ( (this.mStretch & GraphicObjects.Stretch.Fill) == GraphicObjects.Stretch.Fill )
            {
				lWidthRatio  = pBounds.Width / this.Width;
				lHeightRatio = pBounds.Height / this.Height;
			}

			if ( (this.mStretch & GraphicObjects.Stretch.Uniform) == GraphicObjects.Stretch.Uniform )
            {
                if ( lWidthRatio < lHeightRatio )
                {
                    lHeightRatio = lWidthRatio;
                }
                else
                {
                    lWidthRatio = lHeightRatio;
                }
			}

            using ( ImageSurface lSurface = new ImageSurface( Format.Argb32, 
                                                              pBounds.Width, 
                                                              pBounds.Height ) )
            {
                using ( Cairo.Context lContext = new Context( lSurface ) )
                {
                    lContext.Translate( pBounds.Left, pBounds.Top );
                    lContext.Scale( lWidthRatio, lHeightRatio );
                    lContext.Translate( (pBounds.Width / lWidthRatio - this.Width) / 2.0, 
                                        (pBounds.Height / lHeightRatio - this.Height) / 2.0 );

                    using ( ImageSurface lImgSurface = new ImageSurface( this.mImage, 
                                                                         Format.Argb32,
                                                                         (int)this.Width, 
                                                                         (int)this.Height, 
                                                                         (int)(4 * this.Width) ) )
                    {
                        lContext.SetSourceSurface( lImgSurface, 0, 0 );
                        lContext.Paint();
                    }
                }

                pContext.SetSource( lSurface );
            }				
		}
        
        /// <summary>
        /// Renders the image.
        /// </summary>
        /// <param name="pContext">The graphic context.</param>
        /// <param name="pRegion">The region in which to draw.</param>
        /// <param name="pSubPart">The bmp subpart if any.</param>
        public override void Render(Context pContext, Rectangle pRegion, string pSubPart = "")
        {
			double lWidthRatio  = 1.0;
			double lHeightRatio = 1.0;

			if ( (this.mStretch & GraphicObjects.Stretch.Fill) == GraphicObjects.Stretch.Fill )
            {
				lWidthRatio  = pRegion.Width / this.Width;
				lHeightRatio = pRegion.Height / this.Height;
			}

			if ( (this.mStretch & GraphicObjects.Stretch.Uniform) == GraphicObjects.Stretch.Uniform )
            {
                if ( lWidthRatio < lHeightRatio )
                {
                    lHeightRatio = lWidthRatio;
                }
                else
                {
                    lWidthRatio = lHeightRatio;
                }
			}

            pContext.Save();

            pContext.Translate( pRegion.Left, pRegion.Top );
            pContext.Scale( lWidthRatio, lHeightRatio );
            pContext.Translate( (pRegion.Width / lWidthRatio - this.Width) / 2.0, 
                                (pRegion.Height / lHeightRatio - this.Height) / 2.0 );

            using ( ImageSurface lImgSurface = new ImageSurface( this.mImage, 
                                                                 Format.Argb32,
                                                                 (int)this.Width, 
                                                                 (int)this.Height, 
                                                                 (int)(4 * this.Width) ) )
            {
                pContext.SetSourceSurface( lImgSurface, 0, 0 );
                pContext.Paint();
            }

            pContext.Restore();
		}

        /// <summary>
        /// Clones the Bitmap.
        /// </summary>
        /// <returns>The cloned bitmap.</returns>
        protected override ABrush InternalClone()
        {
            return new BmpImageSource( this.Path );
        }

        #endregion Methods
    }
}

