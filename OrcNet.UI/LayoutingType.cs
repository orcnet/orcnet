﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="LayoutingType"/> enumeration.
    /// </summary>
	[Flags]
	public enum LayoutingType : byte
	{
        /// <summary>
        /// None
        /// </summary>
		None = 0x00,

        /// <summary>
        /// X
        /// </summary>
		X = 0x01,

        /// <summary>
        /// Y
        /// </summary>
		Y = 0x02,

        /// <summary>
        /// Positioning
        /// </summary>
		Positioning = 0x03,

        /// <summary>
        /// Width
        /// </summary>
		Width = 0x04,

        /// <summary>
        /// Height
        /// </summary>
		Height = 0x08,

        /// <summary>
        /// Sizing.
        /// </summary>
		Sizing = 0x0C,

        /// <summary>
        /// Arrange children
        /// </summary>
		ArrangeChildren = 0x10,

        /// <summary>
        /// All.
        /// </summary>
		All = 0xFF
	}
}
