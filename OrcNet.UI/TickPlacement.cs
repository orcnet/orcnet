﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TickPlacement"/> enumeration.
    /// </summary>
    public enum TickPlacement
    {
        /// <summary>
        /// No tick marks
        /// </summary>
        None = 0,

        /// <summary>
        /// Tick marks on the top if Horizontal, on the left if vertical.
        /// </summary>
        TopLeft,

        /// <summary>
        /// Tick marks on the bottom if Horizontal, on the right if vertical.
        /// </summary>
        BottomRight,

        /// <summary>
        /// Tick marks on both side of the slider.
        /// </summary>
        BothSides
    }
}
