﻿using System;
using System.Collections.Generic;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Color"/> structure.
    /// </summary>
    public struct Color
    {
        #region Constants

        /// <summary>
        /// The constants color type.
        /// </summary>
        internal readonly static Type TColor = typeof(Color);

        #endregion Constants

        #region Predefined colors

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Transparent = new Color(0, 0, 0, 0, "Transparent");

        /// <summary>
        /// Clear color.
        /// </summary>
        public static readonly Color Clear = new Color(-1, -1, -1, -1, "Clear");

        /// <summary>
        /// Green color.
        /// </summary>
        public static readonly Color Green = new Color(0, 1.0, 0, 1.0, "Green");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AirForceBlueRaf = new Color(0.364705882352941, 0.541176470588235, 0.658823529411765, 1.0, "AirForceBlueRaf");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AirForceBlueUsaf = new Color(0, 0.188235294117647, 0.56078431372549, 1.0, "AirForceBlueUsaf");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AirSuperiorityBlue = new Color(0.447058823529412, 0.627450980392157, 0.756862745098039, 1.0, "AirSuperiorityBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AlabamaCrimson = new Color(0.63921568627451, 0.149019607843137, 0.219607843137255, 1.0, "AlabamaCrimson");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AliceBlue = new Color(0.941176470588235, 0.972549019607843, 1, 1.0, "AliceBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AlizarinCrimson = new Color(0.890196078431373, 0.149019607843137, 0.211764705882353, 1.0, "AlizarinCrimson");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AlloyOrange = new Color(0.768627450980392, 0.384313725490196, 0.0627450980392157, 1.0, "AlloyOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Almond = new Color(0.937254901960784, 0.870588235294118, 0.803921568627451, 1.0, "Almond");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Amaranth = new Color(0.898039215686275, 0.168627450980392, 0.313725490196078, 1.0, "Amaranth");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Amber = new Color(1, 0.749019607843137, 0, 1.0, "Amber");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AmberSaeEce = new Color(1, 0.494117647058824, 0, 1.0, "AmberSaeEce");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AmericanRose = new Color(1, 0.0117647058823529, 0.243137254901961, 1.0, "AmericanRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Amethyst = new Color(0.6, 0.4, 0.8, 1.0, "Amethyst");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AndroidGreen = new Color(0.643137254901961, 0.776470588235294, 0.223529411764706, 1.0, "AndroidGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AntiFlashWhite = new Color(0.949019607843137, 0.952941176470588, 0.956862745098039, 1.0, "AntiFlashWhite");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AntiqueBrass = new Color(0.803921568627451, 0.584313725490196, 0.458823529411765, 1.0, "AntiqueBrass");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AntiqueFuchsia = new Color(0.568627450980392, 0.36078431372549, 0.513725490196078, 1.0, "AntiqueFuchsia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AntiqueRuby = new Color(0.517647058823529, 0.105882352941176, 0.176470588235294, 1.0, "AntiqueRuby");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AntiqueWhite = new Color(0.980392156862745, 0.92156862745098, 0.843137254901961, 1.0, "AntiqueWhite");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AoEnglish = new Color(0, 0.501960784313725, 0, 1.0, "AoEnglish");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AppleGreen = new Color(0.552941176470588, 0.713725490196078, 0, 1.0, "AppleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Apricot = new Color(0.984313725490196, 0.807843137254902, 0.694117647058824, 1.0, "Apricot");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Aqua = new Color(0, 1, 1, 1.0, "Aqua");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Aquamarine = new Color(0.498039215686275, 1, 0.831372549019608, 1.0, "Aquamarine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ArmyGreen = new Color(0.294117647058824, 0.325490196078431, 0.125490196078431, 1.0, "ArmyGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Arsenic = new Color(0.231372549019608, 0.266666666666667, 0.294117647058824, 1.0, "Arsenic");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ArylideYellow = new Color(0.913725490196078, 0.83921568627451, 0.419607843137255, 1.0, "ArylideYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AshGrey = new Color(0.698039215686274, 0.745098039215686, 0.709803921568627, 1.0, "AshGrey");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Asparagus = new Color(0.529411764705882, 0.662745098039216, 0.419607843137255, 1.0, "Asparagus");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AtomicTangerine = new Color(1, 0.6, 0.4, 1.0, "AtomicTangerine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Auburn = new Color(0.647058823529412, 0.164705882352941, 0.164705882352941, 1.0, "Auburn");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Aureolin = new Color(0.992156862745098, 0.933333333333333, 0, 1.0, "Aureolin");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Aurometalsaurus = new Color(0.431372549019608, 0.498039215686275, 0.501960784313725, 1.0, "Aurometalsaurus");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Avocado = new Color(0.337254901960784, 0.509803921568627, 0.0117647058823529, 1.0, "Avocado");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Azure = new Color(0, 0.498039215686275, 1, 1.0, "Azure");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color AzureMistWeb = new Color(0.941176470588235, 1, 1, 1.0, "AzureMistWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BabyBlue = new Color(0.537254901960784, 0.811764705882353, 0.941176470588235, 1.0, "BabyBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BabyBlueEyes = new Color(0.631372549019608, 0.792156862745098, 0.945098039215686, 1.0, "BabyBlueEyes");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BabyPink = new Color(0.956862745098039, 0.76078431372549, 0.76078431372549, 1.0, "BabyPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BallBlue = new Color(0.129411764705882, 0.670588235294118, 0.803921568627451, 1.0, "BallBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BananaMania = new Color(0.980392156862745, 0.905882352941176, 0.709803921568627, 1.0, "BananaMania");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BananaYellow = new Color(1, 0.882352941176471, 0.207843137254902, 1.0, "BananaYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BarnRed = new Color(0.486274509803922, 0.0392156862745098, 0.00784313725490196, 1.0, "BarnRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BattleshipGrey = new Color(0.517647058823529, 0.517647058823529, 0.509803921568627, 1.0, "BattleshipGrey");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Bazaar = new Color(0.596078431372549, 0.466666666666667, 0.482352941176471, 1.0, "Bazaar");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BeauBlue = new Color(0.737254901960784, 0.831372549019608, 0.901960784313726, 1.0, "BeauBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Beaver = new Color(0.623529411764706, 0.505882352941176, 0.43921568627451, 1.0, "Beaver");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Beige = new Color(0.96078431372549, 0.96078431372549, 0.862745098039216, 1.0, "Beige");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BigDipORuby = new Color(0.611764705882353, 0.145098039215686, 0.258823529411765, 1.0, "BigDipORuby");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Bisque = new Color(1, 0.894117647058824, 0.768627450980392, 1.0, "Bisque");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Bistre = new Color(0.23921568627451, 0.168627450980392, 0.12156862745098, 1.0, "Bistre");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Bittersweet = new Color(0.996078431372549, 0.435294117647059, 0.368627450980392, 1.0, "Bittersweet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BittersweetShimmer = new Color(0.749019607843137, 0.309803921568627, 0.317647058823529, 1.0, "BittersweetShimmer");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Black = new Color(0, 0, 0, 1.0, "Black");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlackBean = new Color(0.23921568627451, 0.0470588235294118, 0.00784313725490196, 1.0, "BlackBean");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlackLeatherJacket = new Color(0.145098039215686, 0.207843137254902, 0.16078431372549, 1.0, "BlackLeatherJacket");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlackOlive = new Color(0.231372549019608, 0.235294117647059, 0.211764705882353, 1.0, "BlackOlive");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlanchedAlmond = new Color(1, 0.92156862745098, 0.803921568627451, 1.0, "BlanchedAlmond");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlastOffBronze = new Color(0.647058823529412, 0.443137254901961, 0.392156862745098, 1.0, "BlastOffBronze");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BleuDeFrance = new Color(0.192156862745098, 0.549019607843137, 0.905882352941176, 1.0, "BleuDeFrance");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlizzardBlue = new Color(0.674509803921569, 0.898039215686275, 0.933333333333333, 1.0, "BlizzardBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Blond = new Color(0.980392156862745, 0.941176470588235, 0.745098039215686, 1.0, "Blond");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Blue = new Color(0, 0, 1, 1.0, "Blue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueBell = new Color(0.635294117647059, 0.635294117647059, 0.815686274509804, 1.0, "BlueBell");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueCrayola = new Color(0.12156862745098, 0.458823529411765, 0.996078431372549, 1.0, "BlueCrayola");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueGray = new Color(0.4, 0.6, 0.8, 1.0, "BlueGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueGreen = new Color(0.0509803921568627, 0.596078431372549, 0.729411764705882, 1.0, "BlueGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueMunsell = new Color(0, 0.576470588235294, 0.686274509803922, 1.0, "BlueMunsell");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueNcs = new Color(0, 0.529411764705882, 0.741176470588235, 1.0, "BlueNcs");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BluePigment = new Color(0.2, 0.2, 0.6, 1.0, "BluePigment");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueRyb = new Color(0.00784313725490196, 0.27843137254902, 0.996078431372549, 1.0, "BlueRyb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueSapphire = new Color(0.0705882352941176, 0.380392156862745, 0.501960784313725, 1.0, "BlueSapphire");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BlueViolet = new Color(0.541176470588235, 0.168627450980392, 0.886274509803922, 1.0, "BlueViolet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Blush = new Color(0.870588235294118, 0.364705882352941, 0.513725490196078, 1.0, "Blush");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Bole = new Color(0.474509803921569, 0.266666666666667, 0.231372549019608, 1.0, "Bole");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BondiBlue = new Color(0, 0.584313725490196, 0.713725490196078, 1.0, "BondiBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Bone = new Color(0.890196078431373, 0.854901960784314, 0.788235294117647, 1.0, "Bone");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BostonUniversityRed = new Color(0.8, 0, 0, 1.0, "BostonUniversityRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BottleGreen = new Color(0, 0.415686274509804, 0.305882352941176, 1.0, "BottleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Boysenberry = new Color(0.529411764705882, 0.196078431372549, 0.376470588235294, 1.0, "Boysenberry");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrandeisBlue = new Color(0, 0.43921568627451, 1, 1.0, "BrandeisBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Brass = new Color(0.709803921568627, 0.650980392156863, 0.258823529411765, 1.0, "Brass");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrickRed = new Color(0.796078431372549, 0.254901960784314, 0.329411764705882, 1.0, "BrickRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrightCerulean = new Color(0.113725490196078, 0.674509803921569, 0.83921568627451, 1.0, "BrightCerulean");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrightGreen = new Color(0.4, 1, 0, 1.0, "BrightGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrightLavender = new Color(0.749019607843137, 0.580392156862745, 0.894117647058824, 1.0, "BrightLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrightMaroon = new Color(0.764705882352941, 0.129411764705882, 0.282352941176471, 1.0, "BrightMaroon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrightPink = new Color(1, 0, 0.498039215686275, 1.0, "BrightPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrightTurquoise = new Color(0.0313725490196078, 0.909803921568627, 0.870588235294118, 1.0, "BrightTurquoise");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrightUbe = new Color(0.819607843137255, 0.623529411764706, 0.909803921568627, 1.0, "BrightUbe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrilliantLavender = new Color(0.956862745098039, 0.733333333333333, 1, 1.0, "BrilliantLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrilliantRose = new Color(1, 0.333333333333333, 0.63921568627451, 1.0, "BrilliantRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrinkPink = new Color(0.984313725490196, 0.376470588235294, 0.498039215686275, 1.0, "BrinkPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BritishRacingGreen = new Color(0, 0.258823529411765, 0.145098039215686, 1.0, "BritishRacingGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Bronze = new Color(0.803921568627451, 0.498039215686275, 0.196078431372549, 1.0, "Bronze");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrownTraditional = new Color(0.588235294117647, 0.294117647058824, 0, 1.0, "BrownTraditional");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BrownWeb = new Color(0.647058823529412, 0.164705882352941, 0.164705882352941, 1.0, "BrownWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BubbleGum = new Color(1, 0.756862745098039, 0.8, 1.0, "BubbleGum");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Bubbles = new Color(0.905882352941176, 0.996078431372549, 1, 1.0, "Bubbles");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Buff = new Color(0.941176470588235, 0.862745098039216, 0.509803921568627, 1.0, "Buff");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BulgarianRose = new Color(0.282352941176471, 0.0235294117647059, 0.0274509803921569, 1.0, "BulgarianRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Burgundy = new Color(0.501960784313725, 0, 0.125490196078431, 1.0, "Burgundy");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Burlywood = new Color(0.870588235294118, 0.72156862745098, 0.529411764705882, 1.0, "Burlywood");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BurntOrange = new Color(0.8, 0.333333333333333, 0, 1.0, "BurntOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BurntSienna = new Color(0.913725490196078, 0.454901960784314, 0.317647058823529, 1.0, "BurntSienna");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color BurntUmber = new Color(0.541176470588235, 0.2, 0.141176470588235, 1.0, "BurntUmber");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Byzantine = new Color(0.741176470588235, 0.2, 0.643137254901961, 1.0, "Byzantine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Byzantium = new Color(0.43921568627451, 0.16078431372549, 0.388235294117647, 1.0, "Byzantium");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cadet = new Color(0.325490196078431, 0.407843137254902, 0.447058823529412, 1.0, "Cadet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CadetBlue = new Color(0.372549019607843, 0.619607843137255, 0.627450980392157, 1.0, "CadetBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CadetGrey = new Color(0.568627450980392, 0.63921568627451, 0.690196078431373, 1.0, "CadetGrey");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CadmiumGreen = new Color(0, 0.419607843137255, 0.235294117647059, 1.0, "CadmiumGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CadmiumOrange = new Color(0.929411764705882, 0.529411764705882, 0.176470588235294, 1.0, "CadmiumOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CadmiumRed = new Color(0.890196078431373, 0, 0.133333333333333, 1.0, "CadmiumRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CadmiumYellow = new Color(1, 0.964705882352941, 0, 1.0, "CadmiumYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CafAuLait = new Color(0.650980392156863, 0.482352941176471, 0.356862745098039, 1.0, "CafAuLait");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CafNoir = new Color(0.294117647058824, 0.211764705882353, 0.129411764705882, 1.0, "CafNoir");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CalPolyGreen = new Color(0.117647058823529, 0.301960784313725, 0.168627450980392, 1.0, "CalPolyGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CambridgeBlue = new Color(0.63921568627451, 0.756862745098039, 0.67843137254902, 1.0, "CambridgeBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Camel = new Color(0.756862745098039, 0.603921568627451, 0.419607843137255, 1.0, "Camel");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CameoPink = new Color(0.937254901960784, 0.733333333333333, 0.8, 1.0, "CameoPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CamouflageGreen = new Color(0.470588235294118, 0.525490196078431, 0.419607843137255, 1.0, "CamouflageGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CanaryYellow = new Color(1, 0.937254901960784, 0, 1.0, "CanaryYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CandyAppleRed = new Color(1, 0.0313725490196078, 0, 1.0, "CandyAppleRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CandyPink = new Color(0.894117647058824, 0.443137254901961, 0.47843137254902, 1.0, "CandyPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Capri = new Color(0, 0.749019607843137, 1, 1.0, "Capri");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CaputMortuum = new Color(0.349019607843137, 0.152941176470588, 0.125490196078431, 1.0, "CaputMortuum");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cardinal = new Color(0.768627450980392, 0.117647058823529, 0.227450980392157, 1.0, "Cardinal");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CaribbeanGreen = new Color(0, 0.8, 0.6, 1.0, "CaribbeanGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Carmine = new Color(0.588235294117647, 0, 0.0941176470588235, 1.0, "Carmine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CarmineMP = new Color(0.843137254901961, 0, 0.250980392156863, 1.0, "CarmineMP");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CarminePink = new Color(0.92156862745098, 0.298039215686275, 0.258823529411765, 1.0, "CarminePink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CarmineRed = new Color(1, 0, 0.219607843137255, 1.0, "CarmineRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CarnationPink = new Color(1, 0.650980392156863, 0.788235294117647, 1.0, "CarnationPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Carnelian = new Color(0.701960784313725, 0.105882352941176, 0.105882352941176, 1.0, "Carnelian");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CarolinaBlue = new Color(0.6, 0.729411764705882, 0.866666666666667, 1.0, "CarolinaBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CarrotOrange = new Color(0.929411764705882, 0.568627450980392, 0.129411764705882, 1.0, "CarrotOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CatalinaBlue = new Color(0.0235294117647059, 0.164705882352941, 0.470588235294118, 1.0, "CatalinaBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ceil = new Color(0.572549019607843, 0.631372549019608, 0.811764705882353, 1.0, "Ceil");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Celadon = new Color(0.674509803921569, 0.882352941176471, 0.686274509803922, 1.0, "Celadon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CeladonBlue = new Color(0, 0.482352941176471, 0.654901960784314, 1.0, "CeladonBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CeladonGreen = new Color(0.184313725490196, 0.517647058823529, 0.486274509803922, 1.0, "CeladonGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CelesteColour = new Color(0.698039215686274, 1, 1, 1.0, "CelesteColour");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CelestialBlue = new Color(0.286274509803922, 0.592156862745098, 0.815686274509804, 1.0, "CelestialBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cerise = new Color(0.870588235294118, 0.192156862745098, 0.388235294117647, 1.0, "Cerise");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CerisePink = new Color(0.925490196078431, 0.231372549019608, 0.513725490196078, 1.0, "CerisePink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cerulean = new Color(0, 0.482352941176471, 0.654901960784314, 1.0, "Cerulean");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CeruleanBlue = new Color(0.164705882352941, 0.32156862745098, 0.745098039215686, 1.0, "CeruleanBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CeruleanFrost = new Color(0.427450980392157, 0.607843137254902, 0.764705882352941, 1.0, "CeruleanFrost");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CgBlue = new Color(0, 0.47843137254902, 0.647058823529412, 1.0, "CgBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CgRed = new Color(0.87843137254902, 0.235294117647059, 0.192156862745098, 1.0, "CgRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Chamoisee = new Color(0.627450980392157, 0.470588235294118, 0.352941176470588, 1.0, "Chamoisee");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Champagne = new Color(0.980392156862745, 0.83921568627451, 0.647058823529412, 1.0, "Champagne");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Charcoal = new Color(0.211764705882353, 0.270588235294118, 0.309803921568627, 1.0, "Charcoal");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CharmPink = new Color(0.901960784313726, 0.56078431372549, 0.674509803921569, 1.0, "CharmPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ChartreuseTraditional = new Color(0.874509803921569, 1, 0, 1.0, "ChartreuseTraditional");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ChartreuseWeb = new Color(0.498039215686275, 1, 0, 1.0, "ChartreuseWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cherry = new Color(0.870588235294118, 0.192156862745098, 0.388235294117647, 1.0, "Cherry");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CherryBlossomPink = new Color(1, 0.717647058823529, 0.772549019607843, 1.0, "CherryBlossomPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Chestnut = new Color(0.803921568627451, 0.36078431372549, 0.36078431372549, 1.0, "Chestnut");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ChinaPink = new Color(0.870588235294118, 0.435294117647059, 0.631372549019608, 1.0, "ChinaPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ChinaRose = new Color(0.658823529411765, 0.317647058823529, 0.431372549019608, 1.0, "ChinaRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ChineseRed = new Color(0.666666666666667, 0.219607843137255, 0.117647058823529, 1.0, "ChineseRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ChocolateTraditional = new Color(0.482352941176471, 0.247058823529412, 0, 1.0, "ChocolateTraditional");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ChocolateWeb = new Color(0.823529411764706, 0.411764705882353, 0.117647058823529, 1.0, "ChocolateWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ChromeYellow = new Color(1, 0.654901960784314, 0, 1.0, "ChromeYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cinereous = new Color(0.596078431372549, 0.505882352941176, 0.482352941176471, 1.0, "Cinereous");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cinnabar = new Color(0.890196078431373, 0.258823529411765, 0.203921568627451, 1.0, "Cinnabar");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cinnamon = new Color(0.823529411764706, 0.411764705882353, 0.117647058823529, 1.0, "Cinnamon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Citrine = new Color(0.894117647058824, 0.815686274509804, 0.0392156862745098, 1.0, "Citrine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ClassicRose = new Color(0.984313725490196, 0.8, 0.905882352941176, 1.0, "ClassicRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cobalt = new Color(0, 0.27843137254902, 0.670588235294118, 1.0, "Cobalt");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CocoaBrown = new Color(0.823529411764706, 0.411764705882353, 0.117647058823529, 1.0, "CocoaBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Coffee = new Color(0.435294117647059, 0.305882352941176, 0.215686274509804, 1.0, "Coffee");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ColumbiaBlue = new Color(0.607843137254902, 0.866666666666667, 1, 1.0, "ColumbiaBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CongoPink = new Color(0.972549019607843, 0.513725490196078, 0.474509803921569, 1.0, "CongoPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CoolBlack = new Color(0, 0.180392156862745, 0.388235294117647, 1.0, "CoolBlack");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CoolGrey = new Color(0.549019607843137, 0.572549019607843, 0.674509803921569, 1.0, "CoolGrey");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Copper = new Color(0.72156862745098, 0.450980392156863, 0.2, 1.0, "Copper");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CopperCrayola = new Color(0.854901960784314, 0.541176470588235, 0.403921568627451, 1.0, "CopperCrayola");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CopperPenny = new Color(0.67843137254902, 0.435294117647059, 0.411764705882353, 1.0, "CopperPenny");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CopperRed = new Color(0.796078431372549, 0.427450980392157, 0.317647058823529, 1.0, "CopperRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CopperRose = new Color(0.6, 0.4, 0.4, 1.0, "CopperRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Coquelicot = new Color(1, 0.219607843137255, 0, 1.0, "Coquelicot");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Coral = new Color(1, 0.498039215686275, 0.313725490196078, 1.0, "Coral");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CoralPink = new Color(0.972549019607843, 0.513725490196078, 0.474509803921569, 1.0, "CoralPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CoralRed = new Color(1, 0.250980392156863, 0.250980392156863, 1.0, "CoralRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cordovan = new Color(0.537254901960784, 0.247058823529412, 0.270588235294118, 1.0, "Cordovan");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Corn = new Color(0.984313725490196, 0.925490196078431, 0.364705882352941, 1.0, "Corn");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CornellRed = new Color(0.701960784313725, 0.105882352941176, 0.105882352941176, 1.0, "CornellRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CornflowerBlue = new Color(0.392156862745098, 0.584313725490196, 0.929411764705882, 1.0, "CornflowerBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cornsilk = new Color(1, 0.972549019607843, 0.862745098039216, 1.0, "Cornsilk");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CosmicLatte = new Color(1, 0.972549019607843, 0.905882352941176, 1.0, "CosmicLatte");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CottonCandy = new Color(1, 0.737254901960784, 0.850980392156863, 1.0, "CottonCandy");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cream = new Color(1, 0.992156862745098, 0.815686274509804, 1.0, "Cream");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Crimson = new Color(0.862745098039216, 0.0784313725490196, 0.235294117647059, 1.0, "Crimson");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CrimsonGlory = new Color(0.745098039215686, 0, 0.196078431372549, 1.0, "CrimsonGlory");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Cyan = new Color(0, 1, 1, 1.0, "Cyan");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color CyanProcess = new Color(0, 0.717647058823529, 0.92156862745098, 1.0, "CyanProcess");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Daffodil = new Color(1, 1, 0.192156862745098, 1.0, "Daffodil");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Dandelion = new Color(0.941176470588235, 0.882352941176471, 0.188235294117647, 1.0, "Dandelion");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkBlue = new Color(0, 0, 0.545098039215686, 1.0, "DarkBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkBrown = new Color(0.396078431372549, 0.262745098039216, 0.129411764705882, 1.0, "DarkBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkByzantium = new Color(0.364705882352941, 0.223529411764706, 0.329411764705882, 1.0, "DarkByzantium");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkCandyAppleRed = new Color(0.643137254901961, 0, 0, 1.0, "DarkCandyAppleRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkCerulean = new Color(0.0313725490196078, 0.270588235294118, 0.494117647058824, 1.0, "DarkCerulean");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkChestnut = new Color(0.596078431372549, 0.411764705882353, 0.376470588235294, 1.0, "DarkChestnut");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkCoral = new Color(0.803921568627451, 0.356862745098039, 0.270588235294118, 1.0, "DarkCoral");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkCyan = new Color(0, 0.545098039215686, 0.545098039215686, 1.0, "DarkCyan");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkElectricBlue = new Color(0.325490196078431, 0.407843137254902, 0.470588235294118, 1.0, "DarkElectricBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkGoldenrod = new Color(0.72156862745098, 0.525490196078431, 0.0431372549019608, 1.0, "DarkGoldenrod");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkGray = new Color(0.662745098039216, 0.662745098039216, 0.662745098039216, 1.0, "DarkGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkGreen = new Color(0.00392156862745098, 0.196078431372549, 0.125490196078431, 1.0, "DarkGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkImperialBlue = new Color(0, 0.254901960784314, 0.415686274509804, 1.0, "DarkImperialBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkJungleGreen = new Color(0.101960784313725, 0.141176470588235, 0.129411764705882, 1.0, "DarkJungleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkKhaki = new Color(0.741176470588235, 0.717647058823529, 0.419607843137255, 1.0, "DarkKhaki");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkLava = new Color(0.282352941176471, 0.235294117647059, 0.196078431372549, 1.0, "DarkLava");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkLavender = new Color(0.450980392156863, 0.309803921568627, 0.588235294117647, 1.0, "DarkLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkMagenta = new Color(0.545098039215686, 0, 0.545098039215686, 1.0, "DarkMagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkMidnightBlue = new Color(0, 0.2, 0.4, 1.0, "DarkMidnightBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkOliveGreen = new Color(0.333333333333333, 0.419607843137255, 0.184313725490196, 1.0, "DarkOliveGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkOrange = new Color(1, 0.549019607843137, 0, 1.0, "DarkOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkOrchid = new Color(0.6, 0.196078431372549, 0.8, 1.0, "DarkOrchid");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkPastelBlue = new Color(0.466666666666667, 0.619607843137255, 0.796078431372549, 1.0, "DarkPastelBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkPastelGreen = new Color(0.0117647058823529, 0.752941176470588, 0.235294117647059, 1.0, "DarkPastelGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkPastelPurple = new Color(0.588235294117647, 0.435294117647059, 0.83921568627451, 1.0, "DarkPastelPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkPastelRed = new Color(0.76078431372549, 0.231372549019608, 0.133333333333333, 1.0, "DarkPastelRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkPink = new Color(0.905882352941176, 0.329411764705882, 0.501960784313725, 1.0, "DarkPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkPowderBlue = new Color(0, 0.2, 0.6, 1.0, "DarkPowderBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkRaspberry = new Color(0.529411764705882, 0.149019607843137, 0.341176470588235, 1.0, "DarkRaspberry");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkRed = new Color(0.545098039215686, 0, 0, 1.0, "DarkRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkSalmon = new Color(0.913725490196078, 0.588235294117647, 0.47843137254902, 1.0, "DarkSalmon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkScarlet = new Color(0.337254901960784, 0.0117647058823529, 0.0980392156862745, 1.0, "DarkScarlet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkSeaGreen = new Color(0.56078431372549, 0.737254901960784, 0.56078431372549, 1.0, "DarkSeaGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkSienna = new Color(0.235294117647059, 0.0784313725490196, 0.0784313725490196, 1.0, "DarkSienna");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkSlateBlue = new Color(0.282352941176471, 0.23921568627451, 0.545098039215686, 1.0, "DarkSlateBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkSlateGray = new Color(0.184313725490196, 0.309803921568627, 0.309803921568627, 1.0, "DarkSlateGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkSpringGreen = new Color(0.0901960784313725, 0.447058823529412, 0.270588235294118, 1.0, "DarkSpringGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkTan = new Color(0.568627450980392, 0.505882352941176, 0.317647058823529, 1.0, "DarkTan");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkTangerine = new Color(1, 0.658823529411765, 0.0705882352941176, 1.0, "DarkTangerine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkTaupe = new Color(0.282352941176471, 0.235294117647059, 0.196078431372549, 1.0, "DarkTaupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkTerraCotta = new Color(0.8, 0.305882352941176, 0.36078431372549, 1.0, "DarkTerraCotta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkTurquoise = new Color(0, 0.807843137254902, 0.819607843137255, 1.0, "DarkTurquoise");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkViolet = new Color(0.580392156862745, 0, 0.827450980392157, 1.0, "DarkViolet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DarkYellow = new Color(0.607843137254902, 0.529411764705882, 0.0470588235294118, 1.0, "DarkYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DartmouthGreen = new Color(0, 0.43921568627451, 0.235294117647059, 1.0, "DartmouthGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DavySGrey = new Color(0.333333333333333, 0.333333333333333, 0.333333333333333, 1.0, "DavySGrey");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DebianRed = new Color(0.843137254901961, 0.0392156862745098, 0.325490196078431, 1.0, "DebianRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepCarmine = new Color(0.662745098039216, 0.125490196078431, 0.243137254901961, 1.0, "DeepCarmine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepCarminePink = new Color(0.937254901960784, 0.188235294117647, 0.219607843137255, 1.0, "DeepCarminePink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepCarrotOrange = new Color(0.913725490196078, 0.411764705882353, 0.172549019607843, 1.0, "DeepCarrotOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepCerise = new Color(0.854901960784314, 0.196078431372549, 0.529411764705882, 1.0, "DeepCerise");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepChampagne = new Color(0.980392156862745, 0.83921568627451, 0.647058823529412, 1.0, "DeepChampagne");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepChestnut = new Color(0.725490196078431, 0.305882352941176, 0.282352941176471, 1.0, "DeepChestnut");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepCoffee = new Color(0.43921568627451, 0.258823529411765, 0.254901960784314, 1.0, "DeepCoffee");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepFuchsia = new Color(0.756862745098039, 0.329411764705882, 0.756862745098039, 1.0, "DeepFuchsia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepJungleGreen = new Color(0, 0.294117647058824, 0.286274509803922, 1.0, "DeepJungleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepLilac = new Color(0.6, 0.333333333333333, 0.733333333333333, 1.0, "DeepLilac");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepMagenta = new Color(0.8, 0, 0.8, 1.0, "DeepMagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepPeach = new Color(1, 0.796078431372549, 0.643137254901961, 1.0, "DeepPeach");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepPink = new Color(1, 0.0784313725490196, 0.576470588235294, 1.0, "DeepPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepRuby = new Color(0.517647058823529, 0.247058823529412, 0.356862745098039, 1.0, "DeepRuby");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepSaffron = new Color(1, 0.6, 0.2, 1.0, "DeepSaffron");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepSkyBlue = new Color(0, 0.749019607843137, 1, 1.0, "DeepSkyBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DeepTuscanRed = new Color(0.4, 0.258823529411765, 0.301960784313725, 1.0, "DeepTuscanRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Denim = new Color(0.0823529411764706, 0.376470588235294, 0.741176470588235, 1.0, "Denim");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Desert = new Color(0.756862745098039, 0.603921568627451, 0.419607843137255, 1.0, "Desert");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DesertSand = new Color(0.929411764705882, 0.788235294117647, 0.686274509803922, 1.0, "DesertSand");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DimGray = new Color(0.411764705882353, 0.411764705882353, 0.411764705882353, 1.0, "DimGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DodgerBlue = new Color(0.117647058823529, 0.564705882352941, 1, 1.0, "DodgerBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DogwoodRose = new Color(0.843137254901961, 0.0941176470588235, 0.407843137254902, 1.0, "DogwoodRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DollarBill = new Color(0.52156862745098, 0.733333333333333, 0.396078431372549, 1.0, "DollarBill");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Drab = new Color(0.588235294117647, 0.443137254901961, 0.0901960784313725, 1.0, "Drab");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color DukeBlue = new Color(0, 0, 0.611764705882353, 1.0, "DukeBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color EarthYellow = new Color(0.882352941176471, 0.662745098039216, 0.372549019607843, 1.0, "EarthYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ebony = new Color(0.333333333333333, 0.364705882352941, 0.313725490196078, 1.0, "Ebony");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ecru = new Color(0.76078431372549, 0.698039215686274, 0.501960784313725, 1.0, "Ecru");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Eggplant = new Color(0.380392156862745, 0.250980392156863, 0.317647058823529, 1.0, "Eggplant");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Eggshell = new Color(0.941176470588235, 0.917647058823529, 0.83921568627451, 1.0, "Eggshell");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color EgyptianBlue = new Color(0.0627450980392157, 0.203921568627451, 0.650980392156863, 1.0, "EgyptianBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricBlue = new Color(0.490196078431373, 0.976470588235294, 1, 1.0, "ElectricBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricCrimson = new Color(1, 0, 0.247058823529412, 1.0, "ElectricCrimson");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricCyan = new Color(0, 1, 1, 1.0, "ElectricCyan");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricGreen = new Color(0, 1, 0, 1.0, "ElectricGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricIndigo = new Color(0.435294117647059, 0, 1, 1.0, "ElectricIndigo");
        
        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricLavender = new Color(0.956862745098039, 0.733333333333333, 1, 1.0, "ElectricLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricLime = new Color(0.8, 1, 0, 1.0, "ElectricLime");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricPurple = new Color(0.749019607843137, 0, 1, 1.0, "ElectricPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricUltramarine = new Color(0.247058823529412, 0, 1, 1.0, "ElectricUltramarine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricViolet = new Color(0.56078431372549, 0, 1, 1.0, "ElectricViolet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ElectricYellow = new Color(1, 1, 0, 1.0, "ElectricYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Emerald = new Color(0.313725490196078, 0.784313725490196, 0.470588235294118, 1.0, "Emerald");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color EnglishLavender = new Color(0.705882352941177, 0.513725490196078, 0.584313725490196, 1.0, "EnglishLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color EtonBlue = new Color(0.588235294117647, 0.784313725490196, 0.635294117647059, 1.0, "EtonBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Fallow = new Color(0.756862745098039, 0.603921568627451, 0.419607843137255, 1.0, "Fallow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FaluRed = new Color(0.501960784313725, 0.0941176470588235, 0.0941176470588235, 1.0, "FaluRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Fandango = new Color(0.709803921568627, 0.2, 0.537254901960784, 1.0, "Fandango");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FashionFuchsia = new Color(0.956862745098039, 0, 0.631372549019608, 1.0, "FashionFuchsia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Fawn = new Color(0.898039215686275, 0.666666666666667, 0.43921568627451, 1.0, "Fawn");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Feldgrau = new Color(0.301960784313725, 0.364705882352941, 0.325490196078431, 1.0, "Feldgrau");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FernGreen = new Color(0.309803921568627, 0.474509803921569, 0.258823529411765, 1.0, "FernGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FerrariRed = new Color(1, 0.156862745098039, 0, 1.0, "FerrariRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FieldDrab = new Color(0.423529411764706, 0.329411764705882, 0.117647058823529, 1.0, "FieldDrab");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FireEngineRed = new Color(0.807843137254902, 0.125490196078431, 0.16078431372549, 1.0, "FireEngineRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Firebrick = new Color(0.698039215686274, 0.133333333333333, 0.133333333333333, 1.0, "Firebrick");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Flame = new Color(0.886274509803922, 0.345098039215686, 0.133333333333333, 1.0, "Flame");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FlamingoPink = new Color(0.988235294117647, 0.556862745098039, 0.674509803921569, 1.0, "FlamingoPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Flavescent = new Color(0.968627450980392, 0.913725490196078, 0.556862745098039, 1.0, "Flavescent");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Flax = new Color(0.933333333333333, 0.862745098039216, 0.509803921568627, 1.0, "Flax");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FloralWhite = new Color(1, 0.980392156862745, 0.941176470588235, 1.0, "FloralWhite");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FluorescentOrange = new Color(1, 0.749019607843137, 0, 1.0, "FluorescentOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FluorescentPink = new Color(1, 0.0784313725490196, 0.576470588235294, 1.0, "FluorescentPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FluorescentYellow = new Color(0.8, 1, 0, 1.0, "FluorescentYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Folly = new Color(1, 0, 0.309803921568627, 1.0, "Folly");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ForestGreenTraditional = new Color(0.00392156862745098, 0.266666666666667, 0.129411764705882, 1.0, "ForestGreenTraditional");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ForestGreenWeb = new Color(0.133333333333333, 0.545098039215686, 0.133333333333333, 1.0, "ForestGreenWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FrenchBeige = new Color(0.650980392156863, 0.482352941176471, 0.356862745098039, 1.0, "FrenchBeige");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FrenchBlue = new Color(0, 0.447058823529412, 0.733333333333333, 1.0, "FrenchBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FrenchLilac = new Color(0.525490196078431, 0.376470588235294, 0.556862745098039, 1.0, "FrenchLilac");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FrenchLime = new Color(0.8, 1, 0, 1.0, "FrenchLime");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FrenchRaspberry = new Color(0.780392156862745, 0.172549019607843, 0.282352941176471, 1.0, "FrenchRaspberry");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FrenchRose = new Color(0.964705882352941, 0.290196078431373, 0.541176470588235, 1.0, "FrenchRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Fuchsia = new Color(1, 0, 1, 1.0, "Fuchsia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FuchsiaCrayola = new Color(0.756862745098039, 0.329411764705882, 0.756862745098039, 1.0, "FuchsiaCrayola");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FuchsiaPink = new Color(1, 0.466666666666667, 1, 1.0, "FuchsiaPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FuchsiaRose = new Color(0.780392156862745, 0.262745098039216, 0.458823529411765, 1.0, "FuchsiaRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Fulvous = new Color(0.894117647058824, 0.517647058823529, 0, 1.0, "Fulvous");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color FuzzyWuzzy = new Color(0.8, 0.4, 0.4, 1.0, "FuzzyWuzzy");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Gainsboro = new Color(0.862745098039216, 0.862745098039216, 0.862745098039216, 1.0, "Gainsboro");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Gamboge = new Color(0.894117647058824, 0.607843137254902, 0.0588235294117647, 1.0, "Gamboge");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GhostWhite = new Color(0.972549019607843, 0.972549019607843, 1, 1.0, "GhostWhite");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ginger = new Color(0.690196078431373, 0.396078431372549, 0, 1.0, "Ginger");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Glaucous = new Color(0.376470588235294, 0.509803921568627, 0.713725490196078, 1.0, "Glaucous");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Glitter = new Color(0.901960784313726, 0.909803921568627, 0.980392156862745, 1.0, "Glitter");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GoldMetallic = new Color(0.831372549019608, 0.686274509803922, 0.215686274509804, 1.0, "GoldMetallic");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GoldWebGolden = new Color(1, 0.843137254901961, 0, 1.0, "GoldWebGolden");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GoldenBrown = new Color(0.6, 0.396078431372549, 0.0823529411764706, 1.0, "GoldenBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GoldenPoppy = new Color(0.988235294117647, 0.76078431372549, 0, 1.0, "GoldenPoppy");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GoldenYellow = new Color(1, 0.874509803921569, 0, 1.0, "GoldenYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Goldenrod = new Color(0.854901960784314, 0.647058823529412, 0.125490196078431, 1.0, "Goldenrod");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GrannySmithApple = new Color(0.658823529411765, 0.894117647058824, 0.627450980392157, 1.0, "GrannySmithApple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Gray = new Color(0.501960784313725, 0.501960784313725, 0.501960784313725, 1.0, "Gray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GrayAsparagus = new Color(0.274509803921569, 0.349019607843137, 0.270588235294118, 1.0, "GrayAsparagus");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GrayHtmlCssGray = new Color(0.501960784313725, 0.501960784313725, 0.501960784313725, 1.0, "GrayHtmlCssGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GrayX11Gray = new Color(0.745098039215686, 0.745098039215686, 0.745098039215686, 1.0, "GrayX11Gray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GreenColorWheelX11Green = new Color(0, 1, 0, 1.0, "GreenColorWheelX11Green");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GreenCrayola = new Color(0.109803921568627, 0.674509803921569, 0.470588235294118, 1.0, "GreenCrayola");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GreenHtmlCssGreen = new Color(0, 0.501960784313725, 0, 1.0, "GreenHtmlCssGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GreenMunsell = new Color(0, 0.658823529411765, 0.466666666666667, 1.0, "GreenMunsell");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GreenNcs = new Color(0, 0.623529411764706, 0.419607843137255, 1.0, "GreenNcs");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GreenPigment = new Color(0, 0.647058823529412, 0.313725490196078, 1.0, "GreenPigment");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GreenRyb = new Color(0.4, 0.690196078431373, 0.196078431372549, 1.0, "GreenRyb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GreenYellow = new Color(0.67843137254902, 1, 0.184313725490196, 1.0, "GreenYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Grullo = new Color(0.662745098039216, 0.603921568627451, 0.525490196078431, 1.0, "Grullo");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color GuppieGreen = new Color(0, 1, 0.498039215686275, 1.0, "GuppieGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HalayBe = new Color(0.4, 0.219607843137255, 0.329411764705882, 1.0, "HalayBe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HanBlue = new Color(0.266666666666667, 0.423529411764706, 0.811764705882353, 1.0, "HanBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HanPurple = new Color(0.32156862745098, 0.0941176470588235, 0.980392156862745, 1.0, "HanPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HansaYellow = new Color(0.913725490196078, 0.83921568627451, 0.419607843137255, 1.0, "HansaYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Harlequin = new Color(0.247058823529412, 1, 0, 1.0, "Harlequin");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HarvardCrimson = new Color(0.788235294117647, 0, 0.0862745098039216, 1.0, "HarvardCrimson");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HarvestGold = new Color(0.854901960784314, 0.568627450980392, 0, 1.0, "HarvestGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HeartGold = new Color(0.501960784313725, 0.501960784313725, 0, 1.0, "HeartGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Heliotrope = new Color(0.874509803921569, 0.450980392156863, 1, 1.0, "Heliotrope");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HollywoodCerise = new Color(0.956862745098039, 0, 0.631372549019608, 1.0, "HollywoodCerise");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Honeydew = new Color(0.941176470588235, 1, 0.941176470588235, 1.0, "Honeydew");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HonoluluBlue = new Color(0, 0.498039215686275, 0.749019607843137, 1.0, "HonoluluBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HookerSGreen = new Color(0.286274509803922, 0.474509803921569, 0.419607843137255, 1.0, "HookerSGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HotMagenta = new Color(1, 0.113725490196078, 0.807843137254902, 1.0, "HotMagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HotPink = new Color(1, 0.411764705882353, 0.705882352941177, 1.0, "HotPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color HunterGreen = new Color(0.207843137254902, 0.368627450980392, 0.231372549019608, 1.0, "HunterGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Iceberg = new Color(0.443137254901961, 0.650980392156863, 0.823529411764706, 1.0, "Iceberg");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Icterine = new Color(0.988235294117647, 0.968627450980392, 0.368627450980392, 1.0, "Icterine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ImperialBlue = new Color(0, 0.137254901960784, 0.584313725490196, 1.0, "ImperialBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Inchworm = new Color(0.698039215686274, 0.925490196078431, 0.364705882352941, 1.0, "Inchworm");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color IndiaGreen = new Color(0.0745098039215686, 0.533333333333333, 0.0313725490196078, 1.0, "IndiaGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color IndianRed = new Color(0.803921568627451, 0.36078431372549, 0.36078431372549, 1.0, "IndianRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color IndianYellow = new Color(0.890196078431373, 0.658823529411765, 0.341176470588235, 1.0, "IndianYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Indigo = new Color(0.435294117647059, 0, 1, 1.0, "Indigo");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color IndigoDye = new Color(0, 0.254901960784314, 0.415686274509804, 1.0, "IndigoDye");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color IndigoWeb = new Color(0.294117647058824, 0, 0.509803921568627, 1.0, "IndigoWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color InternationalKleinBlue = new Color(0, 0.184313725490196, 0.654901960784314, 1.0, "InternationalKleinBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color InternationalOrangeAerospace = new Color(1, 0.309803921568627, 0, 1.0, "InternationalOrangeAerospace");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color InternationalOrangeEngineering = new Color(0.729411764705882, 0.0862745098039216, 0.0470588235294118, 1.0, "InternationalOrangeEngineering");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color InternationalOrangeGoldenGateBridge = new Color(0.752941176470588, 0.211764705882353, 0.172549019607843, 1.0, "InternationalOrangeGoldenGateBridge");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Iris = new Color(0.352941176470588, 0.309803921568627, 0.811764705882353, 1.0, "Iris");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Isabelline = new Color(0.956862745098039, 0.941176470588235, 0.925490196078431, 1.0, "Isabelline");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color IslamicGreen = new Color(0, 0.564705882352941, 0, 1.0, "IslamicGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ivory = new Color(1, 1, 0.941176470588235, 1.0, "Ivory");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Jade = new Color(0, 0.658823529411765, 0.419607843137255, 1.0, "Jade");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Jasmine = new Color(0.972549019607843, 0.870588235294118, 0.494117647058824, 1.0, "Jasmine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Jasper = new Color(0.843137254901961, 0.231372549019608, 0.243137254901961, 1.0, "Jasper");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color JazzberryJam = new Color(0.647058823529412, 0.0431372549019608, 0.368627450980392, 1.0, "JazzberryJam");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Jet = new Color(0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0, "Jet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Jonquil = new Color(0.980392156862745, 0.854901960784314, 0.368627450980392, 1.0, "Jonquil");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color JuneBud = new Color(0.741176470588235, 0.854901960784314, 0.341176470588235, 1.0, "JuneBud");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color JungleGreen = new Color(0.16078431372549, 0.670588235294118, 0.529411764705882, 1.0, "JungleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color KellyGreen = new Color(0.298039215686275, 0.733333333333333, 0.0901960784313725, 1.0, "KellyGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color KenyanCopper = new Color(0.486274509803922, 0.109803921568627, 0.0196078431372549, 1.0, "KenyanCopper");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color KhakiHtmlCssKhaki = new Color(0.764705882352941, 0.690196078431373, 0.568627450980392, 1.0, "KhakiHtmlCssKhaki");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color KhakiX11LightKhaki = new Color(0.941176470588235, 0.901960784313726, 0.549019607843137, 1.0, "KhakiX11LightKhaki");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color KuCrimson = new Color(0.909803921568627, 0, 0.0509803921568627, 1.0, "KuCrimson");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LaSalleGreen = new Color(0.0313725490196078, 0.470588235294118, 0.188235294117647, 1.0, "LaSalleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LanguidLavender = new Color(0.83921568627451, 0.792156862745098, 0.866666666666667, 1.0, "LanguidLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LapisLazuli = new Color(0.149019607843137, 0.380392156862745, 0.611764705882353, 1.0, "LapisLazuli");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LaserLemon = new Color(0.996078431372549, 0.996078431372549, 0.133333333333333, 1.0, "LaserLemon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LaurelGreen = new Color(0.662745098039216, 0.729411764705882, 0.615686274509804, 1.0, "LaurelGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Lava = new Color(0.811764705882353, 0.0627450980392157, 0.125490196078431, 1.0, "Lava");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderBlue = new Color(0.8, 0.8, 1, 1.0, "LavenderBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderBlush = new Color(1, 0.941176470588235, 0.96078431372549, 1.0, "LavenderBlush");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderFloral = new Color(0.709803921568627, 0.494117647058824, 0.862745098039216, 1.0, "LavenderFloral");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderGray = new Color(0.768627450980392, 0.764705882352941, 0.815686274509804, 1.0, "LavenderGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderIndigo = new Color(0.580392156862745, 0.341176470588235, 0.92156862745098, 1.0, "LavenderIndigo");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderMagenta = new Color(0.933333333333333, 0.509803921568627, 0.933333333333333, 1.0, "LavenderMagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderMist = new Color(0.901960784313726, 0.901960784313726, 0.980392156862745, 1.0, "LavenderMist");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderPink = new Color(0.984313725490196, 0.682352941176471, 0.823529411764706, 1.0, "LavenderPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderPurple = new Color(0.588235294117647, 0.482352941176471, 0.713725490196078, 1.0, "LavenderPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderRose = new Color(0.984313725490196, 0.627450980392157, 0.890196078431373, 1.0, "LavenderRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LavenderWeb = new Color(0.901960784313726, 0.901960784313726, 0.980392156862745, 1.0, "LavenderWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LawnGreen = new Color(0.486274509803922, 0.988235294117647, 0, 1.0, "LawnGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Lemon = new Color(1, 0.968627450980392, 0, 1.0, "Lemon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LemonChiffon = new Color(1, 0.980392156862745, 0.803921568627451, 1.0, "LemonChiffon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LemonLime = new Color(0.890196078431373, 1, 0, 1.0, "LemonLime");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Licorice = new Color(0.101960784313725, 0.0666666666666667, 0.0627450980392157, 1.0, "Licorice");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightApricot = new Color(0.992156862745098, 0.835294117647059, 0.694117647058824, 1.0, "LightApricot");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightBlue = new Color(0.67843137254902, 0.847058823529412, 0.901960784313726, 1.0, "LightBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightBrown = new Color(0.709803921568627, 0.396078431372549, 0.113725490196078, 1.0, "LightBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightCarminePink = new Color(0.901960784313726, 0.403921568627451, 0.443137254901961, 1.0, "LightCarminePink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightCoral = new Color(0.941176470588235, 0.501960784313725, 0.501960784313725, 1.0, "LightCoral");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightCornflowerBlue = new Color(0.576470588235294, 0.8, 0.917647058823529, 1.0, "LightCornflowerBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>s
        public static readonly Color LightCrimson = new Color(0.96078431372549, 0.411764705882353, 0.568627450980392, 1.0, "LightCrimson");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightCyan = new Color(0.87843137254902, 1, 1, 1.0, "LightCyan");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightFuchsiaPink = new Color(0.976470588235294, 0.517647058823529, 0.937254901960784, 1.0, "LightFuchsiaPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightGoldenrodYellow = new Color(0.980392156862745, 0.980392156862745, 0.823529411764706, 1.0, "LightGoldenrodYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightGray = new Color(0.827450980392157, 0.827450980392157, 0.827450980392157, 1.0, "LightGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightGreen = new Color(0.564705882352941, 0.933333333333333, 0.564705882352941, 1.0, "LightGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightKhaki = new Color(0.941176470588235, 0.901960784313726, 0.549019607843137, 1.0, "LightKhaki");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightPastelPurple = new Color(0.694117647058824, 0.611764705882353, 0.850980392156863, 1.0, "LightPastelPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightPink = new Color(1, 0.713725490196078, 0.756862745098039, 1.0, "LightPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightRedOchre = new Color(0.913725490196078, 0.454901960784314, 0.317647058823529, 1.0, "LightRedOchre");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightSalmon = new Color(1, 0.627450980392157, 0.47843137254902, 1.0, "LightSalmon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightSalmonPink = new Color(1, 0.6, 0.6, 1.0, "LightSalmonPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightSeaGreen = new Color(0.125490196078431, 0.698039215686274, 0.666666666666667, 1.0, "LightSeaGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightSkyBlue = new Color(0.529411764705882, 0.807843137254902, 0.980392156862745, 1.0, "LightSkyBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightSlateGray = new Color(0.466666666666667, 0.533333333333333, 0.6, 1.0, "LightSlateGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightTaupe = new Color(0.701960784313725, 0.545098039215686, 0.427450980392157, 1.0, "LightTaupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightThulianPink = new Color(0.901960784313726, 0.56078431372549, 0.674509803921569, 1.0, "LightThulianPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LightYellow = new Color(1, 1, 0.87843137254902, 1.0, "LightYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Lilac = new Color(0.784313725490196, 0.635294117647059, 0.784313725490196, 1.0, "Lilac");
        
        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LimeColorWheel = new Color(0.749019607843137, 1, 0, 1.0, "LimeColorWheel");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LimeGreen = new Color(0.196078431372549, 0.803921568627451, 0.196078431372549, 1.0, "LimeGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LimeWebX11Green = new Color(0, 1, 0, 1.0, "LimeWebX11Green");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Limerick = new Color(0.615686274509804, 0.76078431372549, 0.0352941176470588, 1.0, "Limerick");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LincolnGreen = new Color(0.0980392156862745, 0.349019607843137, 0.0196078431372549, 1.0, "LincolnGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Linen = new Color(0.980392156862745, 0.941176470588235, 0.901960784313726, 1.0, "Linen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Lion = new Color(0.756862745098039, 0.603921568627451, 0.419607843137255, 1.0, "Lion");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color LittleBoyBlue = new Color(0.423529411764706, 0.627450980392157, 0.862745098039216, 1.0, "LittleBoyBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Liver = new Color(0.325490196078431, 0.294117647058824, 0.309803921568627, 1.0, "Liver");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Lust = new Color(0.901960784313726, 0.125490196078431, 0.125490196078431, 1.0, "Lust");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Magenta = new Color(1, 0, 1, 1.0, "Magenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MagentaDye = new Color(0.792156862745098, 0.12156862745098, 0.482352941176471, 1.0, "MagentaDye");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MagentaProcess = new Color(1, 0, 0.564705882352941, 1.0, "MagentaProcess");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MagicMint = new Color(0.666666666666667, 0.941176470588235, 0.819607843137255, 1.0, "MagicMint");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Magnolia = new Color(0.972549019607843, 0.956862745098039, 1, 1.0, "Magnolia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Mahogany = new Color(0.752941176470588, 0.250980392156863, 0, 1.0, "Mahogany");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Maize = new Color(0.984313725490196, 0.925490196078431, 0.364705882352941, 1.0, "Maize");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MajorelleBlue = new Color(0.376470588235294, 0.313725490196078, 0.862745098039216, 1.0, "MajorelleBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Malachite = new Color(0.0431372549019608, 0.854901960784314, 0.317647058823529, 1.0, "Malachite");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Manatee = new Color(0.592156862745098, 0.603921568627451, 0.666666666666667, 1.0, "Manatee");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MangoTango = new Color(1, 0.509803921568627, 0.262745098039216, 1.0, "MangoTango");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Mantis = new Color(0.454901960784314, 0.764705882352941, 0.396078431372549, 1.0, "Mantis");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MardiGras = new Color(0.533333333333333, 0, 0.52156862745098, 1.0, "MardiGras");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MaroonCrayola = new Color(0.764705882352941, 0.129411764705882, 0.282352941176471, 1.0, "MaroonCrayola");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MaroonHtmlCss = new Color(0.501960784313725, 0, 0, 1.0, "MaroonHtmlCss");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MaroonX11 = new Color(0.690196078431373, 0.188235294117647, 0.376470588235294, 1.0, "MaroonX11");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Mauve = new Color(0.87843137254902, 0.690196078431373, 1, 1.0, "Mauve");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MauveTaupe = new Color(0.568627450980392, 0.372549019607843, 0.427450980392157, 1.0, "MauveTaupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Mauvelous = new Color(0.937254901960784, 0.596078431372549, 0.666666666666667, 1.0, "Mauvelous");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MayaBlue = new Color(0.450980392156863, 0.76078431372549, 0.984313725490196, 1.0, "MayaBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MeatBrown = new Color(0.898039215686275, 0.717647058823529, 0.231372549019608, 1.0, "MeatBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumAquamarine = new Color(0.4, 0.866666666666667, 0.666666666666667, 1.0, "MediumAquamarine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumBlue = new Color(0, 0, 0.803921568627451, 1.0, "MediumBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumCandyAppleRed = new Color(0.886274509803922, 0.0235294117647059, 0.172549019607843, 1.0, "MediumCandyAppleRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumCarmine = new Color(0.686274509803922, 0.250980392156863, 0.207843137254902, 1.0, "MediumCarmine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumChampagne = new Color(0.952941176470588, 0.898039215686275, 0.670588235294118, 1.0, "MediumChampagne");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumElectricBlue = new Color(0.0117647058823529, 0.313725490196078, 0.588235294117647, 1.0, "MediumElectricBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumJungleGreen = new Color(0.109803921568627, 0.207843137254902, 0.176470588235294, 1.0, "MediumJungleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumLavenderMagenta = new Color(0.866666666666667, 0.627450980392157, 0.866666666666667, 1.0, "MediumLavenderMagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumOrchid = new Color(0.729411764705882, 0.333333333333333, 0.827450980392157, 1.0, "MediumOrchid");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumPersianBlue = new Color(0, 0.403921568627451, 0.647058823529412, 1.0, "MediumPersianBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumPurple = new Color(0.576470588235294, 0.43921568627451, 0.858823529411765, 1.0, "MediumPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumRedViolet = new Color(0.733333333333333, 0.2, 0.52156862745098, 1.0, "MediumRedViolet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumRuby = new Color(0.666666666666667, 0.250980392156863, 0.411764705882353, 1.0, "MediumRuby");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumSeaGreen = new Color(0.235294117647059, 0.701960784313725, 0.443137254901961, 1.0, "MediumSeaGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumSlateBlue = new Color(0.482352941176471, 0.407843137254902, 0.933333333333333, 1.0, "MediumSlateBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumSpringBud = new Color(0.788235294117647, 0.862745098039216, 0.529411764705882, 1.0, "MediumSpringBud");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumSpringGreen = new Color(0, 0.980392156862745, 0.603921568627451, 1.0, "MediumSpringGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumTaupe = new Color(0.403921568627451, 0.298039215686275, 0.27843137254902, 1.0, "MediumTaupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumTurquoise = new Color(0.282352941176471, 0.819607843137255, 0.8, 1.0, "MediumTurquoise");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumTuscanRed = new Color(0.474509803921569, 0.266666666666667, 0.231372549019608, 1.0, "MediumTuscanRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumVermilion = new Color(0.850980392156863, 0.376470588235294, 0.231372549019608, 1.0, "MediumVermilion");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MediumVioletRed = new Color(0.780392156862745, 0.0823529411764706, 0.52156862745098, 1.0, "MediumVioletRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MellowApricot = new Color(0.972549019607843, 0.72156862745098, 0.470588235294118, 1.0, "MellowApricot");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MellowYellow = new Color(0.972549019607843, 0.870588235294118, 0.494117647058824, 1.0, "MellowYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Melon = new Color(0.992156862745098, 0.737254901960784, 0.705882352941177, 1.0, "Melon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MidnightBlue = new Color(0.0980392156862745, 0.0980392156862745, 0.43921568627451, 1.0, "MidnightBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MidnightGreenEagleGreen = new Color(0, 0.286274509803922, 0.325490196078431, 1.0, "MidnightGreenEagleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MikadoYellow = new Color(1, 0.768627450980392, 0.0470588235294118, 1.0, "MikadoYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Mint = new Color(0.243137254901961, 0.705882352941177, 0.537254901960784, 1.0, "Mint");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MintCream = new Color(0.96078431372549, 1, 0.980392156862745, 1.0, "MintCream");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MintGreen = new Color(0.596078431372549, 1, 0.596078431372549, 1.0, "MintGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MistyRose = new Color(1, 0.894117647058824, 0.882352941176471, 1.0, "MistyRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Moccasin = new Color(0.980392156862745, 0.92156862745098, 0.843137254901961, 1.0, "Moccasin");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ModeBeige = new Color(0.588235294117647, 0.443137254901961, 0.0901960784313725, 1.0, "ModeBeige");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MoonstoneBlue = new Color(0.450980392156863, 0.662745098039216, 0.76078431372549, 1.0, "MoonstoneBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MordantRed19 = new Color(0.682352941176471, 0.0470588235294118, 0, 1.0, "MordantRed19");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MossGreen = new Color(0.67843137254902, 0.874509803921569, 0.67843137254902, 1.0, "MossGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MountainMeadow = new Color(0.188235294117647, 0.729411764705882, 0.56078431372549, 1.0, "MountainMeadow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MountbattenPink = new Color(0.6, 0.47843137254902, 0.552941176470588, 1.0, "MountbattenPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color MsuGreen = new Color(0.0941176470588235, 0.270588235294118, 0.231372549019608, 1.0, "MsuGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Mulberry = new Color(0.772549019607843, 0.294117647058824, 0.549019607843137, 1.0, "Mulberry");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Mustard = new Color(1, 0.858823529411765, 0.345098039215686, 1.0, "Mustard");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Myrtle = new Color(0.129411764705882, 0.258823529411765, 0.117647058823529, 1.0, "Myrtle");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NadeshikoPink = new Color(0.964705882352941, 0.67843137254902, 0.776470588235294, 1.0, "NadeshikoPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NapierGreen = new Color(0.164705882352941, 0.501960784313725, 0, 1.0, "NapierGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NaplesYellow = new Color(0.980392156862745, 0.854901960784314, 0.368627450980392, 1.0, "NaplesYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NavajoWhite = new Color(1, 0.870588235294118, 0.67843137254902, 1.0, "NavajoWhite");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NavyBlue = new Color(0, 0, 0.501960784313725, 1.0, "NavyBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NeonCarrot = new Color(1, 0.63921568627451, 0.262745098039216, 1.0, "NeonCarrot");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NeonFuchsia = new Color(0.996078431372549, 0.254901960784314, 0.392156862745098, 1.0, "NeonFuchsia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NeonGreen = new Color(0.223529411764706, 1, 0.0784313725490196, 1.0, "NeonGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NewYorkPink = new Color(0.843137254901961, 0.513725490196078, 0.498039215686275, 1.0, "NewYorkPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NonPhotoBlue = new Color(0.643137254901961, 0.866666666666667, 0.929411764705882, 1.0, "NonPhotoBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color NorthTexasGreen = new Color(0.0196078431372549, 0.564705882352941, 0.2, 1.0, "NorthTexasGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OceanBoatBlue = new Color(0, 0.466666666666667, 0.745098039215686, 1.0, "OceanBoatBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ochre = new Color(0.8, 0.466666666666667, 0.133333333333333, 1.0, "Ochre");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OfficeGreen = new Color(0, 0.501960784313725, 0, 1.0, "OfficeGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OldGold = new Color(0.811764705882353, 0.709803921568627, 0.231372549019608, 1.0, "OldGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OldLace = new Color(0.992156862745098, 0.96078431372549, 0.901960784313726, 1.0, "OldLace");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OldLavender = new Color(0.474509803921569, 0.407843137254902, 0.470588235294118, 1.0, "OldLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OldMauve = new Color(0.403921568627451, 0.192156862745098, 0.27843137254902, 1.0, "OldMauve");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OldRose = new Color(0.752941176470588, 0.501960784313725, 0.505882352941176, 1.0, "OldRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Olive = new Color(0.501960784313725, 0.501960784313725, 0, 1.0, "Olive");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OliveDrab7 = new Color(0.235294117647059, 0.203921568627451, 0.12156862745098, 1.0, "OliveDrab7");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OliveDrabWebOliveDrab3 = new Color(0.419607843137255, 0.556862745098039, 0.137254901960784, 1.0, "OliveDrabWebOliveDrab3");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Olivine = new Color(0.603921568627451, 0.725490196078431, 0.450980392156863, 1.0, "Olivine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Onyx = new Color(0.207843137254902, 0.219607843137255, 0.223529411764706, 1.0, "Onyx");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OperaMauve = new Color(0.717647058823529, 0.517647058823529, 0.654901960784314, 1.0, "OperaMauve");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OrangeColorWheel = new Color(1, 0.498039215686275, 0, 1.0, "OrangeColorWheel");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OrangePeel = new Color(1, 0.623529411764706, 0, 1.0, "OrangePeel");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OrangeRed = new Color(1, 0.270588235294118, 0, 1.0, "OrangeRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OrangeRyb = new Color(0.984313725490196, 0.6, 0.00784313725490196, 1.0, "OrangeRyb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OrangeWebColor = new Color(1, 0.647058823529412, 0, 1.0, "OrangeWebColor");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Orchid = new Color(0.854901960784314, 0.43921568627451, 0.83921568627451, 1.0, "Orchid");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OtterBrown = new Color(0.396078431372549, 0.262745098039216, 0.129411764705882, 1.0, "OtterBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OuCrimsonRed = new Color(0.6, 0, 0, 1.0, "OuCrimsonRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OuterSpace = new Color(0.254901960784314, 0.290196078431373, 0.298039215686275, 1.0, "OuterSpace");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OutrageousOrange = new Color(1, 0.431372549019608, 0.290196078431373, 1.0, "OutrageousOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color OxfordBlue = new Color(0, 0.129411764705882, 0.27843137254902, 1.0, "OxfordBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PakistanGreen = new Color(0, 0.4, 0, 1.0, "PakistanGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PalatinateBlue = new Color(0.152941176470588, 0.231372549019608, 0.886274509803922, 1.0, "PalatinateBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PalatinatePurple = new Color(0.407843137254902, 0.156862745098039, 0.376470588235294, 1.0, "PalatinatePurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleAqua = new Color(0.737254901960784, 0.831372549019608, 0.901960784313726, 1.0, "PaleAqua");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleBlue = new Color(0.686274509803922, 0.933333333333333, 0.933333333333333, 1.0, "PaleBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleBrown = new Color(0.596078431372549, 0.462745098039216, 0.329411764705882, 1.0, "PaleBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleCarmine = new Color(0.686274509803922, 0.250980392156863, 0.207843137254902, 1.0, "PaleCarmine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleCerulean = new Color(0.607843137254902, 0.768627450980392, 0.886274509803922, 1.0, "PaleCerulean");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleChestnut = new Color(0.866666666666667, 0.67843137254902, 0.686274509803922, 1.0, "PaleChestnut");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleCopper = new Color(0.854901960784314, 0.541176470588235, 0.403921568627451, 1.0, "PaleCopper");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleCornflowerBlue = new Color(0.670588235294118, 0.803921568627451, 0.937254901960784, 1.0, "PaleCornflowerBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleGold = new Color(0.901960784313726, 0.745098039215686, 0.541176470588235, 1.0, "PaleGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleGoldenrod = new Color(0.933333333333333, 0.909803921568627, 0.666666666666667, 1.0, "PaleGoldenrod");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleGreen = new Color(0.596078431372549, 0.984313725490196, 0.596078431372549, 1.0, "PaleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleLavender = new Color(0.862745098039216, 0.815686274509804, 1, 1.0, "PaleLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleMagenta = new Color(0.976470588235294, 0.517647058823529, 0.898039215686275, 1.0, "PaleMagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PalePink = new Color(0.980392156862745, 0.854901960784314, 0.866666666666667, 1.0, "PalePink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PalePlum = new Color(0.866666666666667, 0.627450980392157, 0.866666666666667, 1.0, "PalePlum");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleRedViolet = new Color(0.858823529411765, 0.43921568627451, 0.576470588235294, 1.0, "PaleRedViolet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleRobinEggBlue = new Color(0.588235294117647, 0.870588235294118, 0.819607843137255, 1.0, "PaleRobinEggBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleSilver = new Color(0.788235294117647, 0.752941176470588, 0.733333333333333, 1.0, "PaleSilver");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleSpringBud = new Color(0.925490196078431, 0.92156862745098, 0.741176470588235, 1.0, "PaleSpringBud");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleTaupe = new Color(0.737254901960784, 0.596078431372549, 0.494117647058824, 1.0, "PaleTaupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PaleVioletRed = new Color(0.858823529411765, 0.43921568627451, 0.576470588235294, 1.0, "PaleVioletRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PansyPurple = new Color(0.470588235294118, 0.0941176470588235, 0.290196078431373, 1.0, "PansyPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PapayaWhip = new Color(1, 0.937254901960784, 0.835294117647059, 1.0, "PapayaWhip");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ParisGreen = new Color(0.313725490196078, 0.784313725490196, 0.470588235294118, 1.0, "ParisGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelBlue = new Color(0.682352941176471, 0.776470588235294, 0.811764705882353, 1.0, "PastelBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelBrown = new Color(0.513725490196078, 0.411764705882353, 0.325490196078431, 1.0, "PastelBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelGray = new Color(0.811764705882353, 0.811764705882353, 0.768627450980392, 1.0, "PastelGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelGreen = new Color(0.466666666666667, 0.866666666666667, 0.466666666666667, 1.0, "PastelGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelMagenta = new Color(0.956862745098039, 0.603921568627451, 0.76078431372549, 1.0, "PastelMagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelOrange = new Color(1, 0.701960784313725, 0.27843137254902, 1.0, "PastelOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelPink = new Color(0.870588235294118, 0.647058823529412, 0.643137254901961, 1.0, "PastelPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelPurple = new Color(0.701960784313725, 0.619607843137255, 0.709803921568627, 1.0, "PastelPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelRed = new Color(1, 0.411764705882353, 0.380392156862745, 1.0, "PastelRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelViolet = new Color(0.796078431372549, 0.6, 0.788235294117647, 1.0, "PastelViolet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PastelYellow = new Color(0.992156862745098, 0.992156862745098, 0.588235294117647, 1.0, "PastelYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Patriarch = new Color(0.501960784313725, 0, 0.501960784313725, 1.0, "Patriarch");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PayneSGrey = new Color(0.325490196078431, 0.407843137254902, 0.470588235294118, 1.0, "PayneSGrey");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Peach = new Color(1, 0.898039215686275, 0.705882352941177, 1.0, "Peach");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PeachCrayola = new Color(1, 0.796078431372549, 0.643137254901961, 1.0, "PeachCrayola");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PeachOrange = new Color(1, 0.8, 0.6, 1.0, "PeachOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PeachPuff = new Color(1, 0.854901960784314, 0.725490196078431, 1.0, "PeachPuff");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PeachYellow = new Color(0.980392156862745, 0.874509803921569, 0.67843137254902, 1.0, "PeachYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Pear = new Color(0.819607843137255, 0.886274509803922, 0.192156862745098, 1.0, "Pear");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Pearl = new Color(0.917647058823529, 0.87843137254902, 0.784313725490196, 1.0, "Pearl");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PearlAqua = new Color(0.533333333333333, 0.847058823529412, 0.752941176470588, 1.0, "PearlAqua");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PearlyPurple = new Color(0.717647058823529, 0.407843137254902, 0.635294117647059, 1.0, "PearlyPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Peridot = new Color(0.901960784313726, 0.886274509803922, 0, 1.0, "Peridot");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Periwinkle = new Color(0.8, 0.8, 1, 1.0, "Periwinkle");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PersianBlue = new Color(0.109803921568627, 0.223529411764706, 0.733333333333333, 1.0, "PersianBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PersianGreen = new Color(0, 0.650980392156863, 0.576470588235294, 1.0, "PersianGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PersianIndigo = new Color(0.196078431372549, 0.0705882352941176, 0.47843137254902, 1.0, "PersianIndigo");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PersianOrange = new Color(0.850980392156863, 0.564705882352941, 0.345098039215686, 1.0, "PersianOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PersianPink = new Color(0.968627450980392, 0.498039215686275, 0.745098039215686, 1.0, "PersianPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PersianPlum = new Color(0.43921568627451, 0.109803921568627, 0.109803921568627, 1.0, "PersianPlum");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PersianRed = new Color(0.8, 0.2, 0.2, 1.0, "PersianRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PersianRose = new Color(0.996078431372549, 0.156862745098039, 0.635294117647059, 1.0, "PersianRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Persimmon = new Color(0.925490196078431, 0.345098039215686, 0, 1.0, "Persimmon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Peru = new Color(0.803921568627451, 0.52156862745098, 0.247058823529412, 1.0, "Peru");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Phlox = new Color(0.874509803921569, 0, 1, 1.0, "Phlox");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PhthaloBlue = new Color(0, 0.0588235294117647, 0.537254901960784, 1.0, "PhthaloBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PhthaloGreen = new Color(0.0705882352941176, 0.207843137254902, 0.141176470588235, 1.0, "PhthaloGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PiggyPink = new Color(0.992156862745098, 0.866666666666667, 0.901960784313726, 1.0, "PiggyPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PineGreen = new Color(0.00392156862745098, 0.474509803921569, 0.435294117647059, 1.0, "PineGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Pink = new Color(1, 0.752941176470588, 0.796078431372549, 1.0, "Pink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PinkLace = new Color(1, 0.866666666666667, 0.956862745098039, 1.0, "PinkLace");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PinkOrange = new Color(1, 0.6, 0.4, 1.0, "PinkOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PinkPearl = new Color(0.905882352941176, 0.674509803921569, 0.811764705882353, 1.0, "PinkPearl");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PinkSherbet = new Color(0.968627450980392, 0.56078431372549, 0.654901960784314, 1.0, "PinkSherbet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Pistachio = new Color(0.576470588235294, 0.772549019607843, 0.447058823529412, 1.0, "Pistachio");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Platinum = new Color(0.898039215686275, 0.894117647058824, 0.886274509803922, 1.0, "Platinum");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PlumTraditional = new Color(0.556862745098039, 0.270588235294118, 0.52156862745098, 1.0, "PlumTraditional");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PlumWeb = new Color(0.866666666666667, 0.627450980392157, 0.866666666666667, 1.0, "PlumWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PortlandOrange = new Color(1, 0.352941176470588, 0.211764705882353, 1.0, "PortlandOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PowderBlueWeb = new Color(0.690196078431373, 0.87843137254902, 0.901960784313726, 1.0, "PowderBlueWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PrincetonOrange = new Color(1, 0.56078431372549, 0, 1.0, "PrincetonOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Prune = new Color(0.43921568627451, 0.109803921568627, 0.109803921568627, 1.0, "Prune");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PrussianBlue = new Color(0, 0.192156862745098, 0.325490196078431, 1.0, "PrussianBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PsychedelicPurple = new Color(0.874509803921569, 0, 1, 1.0, "PsychedelicPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Puce = new Color(0.8, 0.533333333333333, 0.6, 1.0, "Puce");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Pumpkin = new Color(1, 0.458823529411765, 0.0941176470588235, 1.0, "Pumpkin");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PurpleHeart = new Color(0.411764705882353, 0.207843137254902, 0.611764705882353, 1.0, "PurpleHeart");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PurpleHtmlCss = new Color(0.501960784313725, 0, 0.501960784313725, 1.0, "PurpleHtmlCss");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PurpleMountainMajesty = new Color(0.588235294117647, 0.470588235294118, 0.713725490196078, 1.0, "PurpleMountainMajesty");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PurpleMunsell = new Color(0.623529411764706, 0, 0.772549019607843, 1.0, "PurpleMunsell");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PurplePizzazz = new Color(0.996078431372549, 0.305882352941176, 0.854901960784314, 1.0, "PurplePizzazz");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PurpleTaupe = new Color(0.313725490196078, 0.250980392156863, 0.301960784313725, 1.0, "PurpleTaupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color PurpleX11 = new Color(0.627450980392157, 0.125490196078431, 0.941176470588235, 1.0, "PurpleX11");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Quartz = new Color(0.317647058823529, 0.282352941176471, 0.309803921568627, 1.0, "Quartz");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Rackley = new Color(0.364705882352941, 0.541176470588235, 0.658823529411765, 1.0, "Rackley");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RadicalRed = new Color(1, 0.207843137254902, 0.368627450980392, 1.0, "RadicalRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Rajah = new Color(0.984313725490196, 0.670588235294118, 0.376470588235294, 1.0, "Rajah");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Raspberry = new Color(0.890196078431373, 0.0431372549019608, 0.364705882352941, 1.0, "Raspberry");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RaspberryGlace = new Color(0.568627450980392, 0.372549019607843, 0.427450980392157, 1.0, "RaspberryGlace");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RaspberryPink = new Color(0.886274509803922, 0.313725490196078, 0.596078431372549, 1.0, "RaspberryPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RaspberryRose = new Color(0.701960784313725, 0.266666666666667, 0.423529411764706, 1.0, "RaspberryRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RawUmber = new Color(0.509803921568627, 0.4, 0.266666666666667, 1.0, "RawUmber");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RazzleDazzleRose = new Color(1, 0.2, 0.8, 1.0, "RazzleDazzleRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Razzmatazz = new Color(0.890196078431373, 0.145098039215686, 0.419607843137255, 1.0, "Razzmatazz");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Red = new Color(1, 0, 0, 1.0, "Red");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RedBrown = new Color(0.647058823529412, 0.164705882352941, 0.164705882352941, 1.0, "RedBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RedDevil = new Color(0.525490196078431, 0.00392156862745098, 0.0666666666666667, 1.0, "RedDevil");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RedMunsell = new Color(0.949019607843137, 0, 0.235294117647059, 1.0, "RedMunsell");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RedNcs = new Color(0.768627450980392, 0.00784313725490196, 0.2, 1.0, "RedNcs");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RedOrange = new Color(1, 0.325490196078431, 0.286274509803922, 1.0, "RedOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RedPigment = new Color(0.929411764705882, 0.109803921568627, 0.141176470588235, 1.0, "RedPigment");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RedRyb = new Color(0.996078431372549, 0.152941176470588, 0.0705882352941176, 1.0, "RedRyb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RedViolet = new Color(0.780392156862745, 0.0823529411764706, 0.52156862745098, 1.0, "RedViolet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Redwood = new Color(0.670588235294118, 0.305882352941176, 0.32156862745098, 1.0, "Redwood");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Regalia = new Color(0.32156862745098, 0.176470588235294, 0.501960784313725, 1.0, "Regalia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ResolutionBlue = new Color(0, 0.137254901960784, 0.529411764705882, 1.0, "ResolutionBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RichBlack = new Color(0, 0.250980392156863, 0.250980392156863, 1.0, "RichBlack");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RichBrilliantLavender = new Color(0.945098039215686, 0.654901960784314, 0.996078431372549, 1.0, "RichBrilliantLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RichCarmine = new Color(0.843137254901961, 0, 0.250980392156863, 1.0, "RichCarmine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RichElectricBlue = new Color(0.0313725490196078, 0.572549019607843, 0.815686274509804, 1.0, "RichElectricBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RichLavender = new Color(0.654901960784314, 0.419607843137255, 0.811764705882353, 1.0, "RichLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RichLilac = new Color(0.713725490196078, 0.4, 0.823529411764706, 1.0, "RichLilac");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RichMaroon = new Color(0.690196078431373, 0.188235294117647, 0.376470588235294, 1.0, "RichMaroon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RifleGreen = new Color(0.254901960784314, 0.282352941176471, 0.2, 1.0, "RifleGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RobinEggBlue = new Color(0, 0.8, 0.8, 1.0, "RobinEggBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Rose = new Color(1, 0, 0.498039215686275, 1.0, "Rose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoseBonbon = new Color(0.976470588235294, 0.258823529411765, 0.619607843137255, 1.0, "RoseBonbon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoseEbony = new Color(0.403921568627451, 0.282352941176471, 0.274509803921569, 1.0, "RoseEbony");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoseGold = new Color(0.717647058823529, 0.431372549019608, 0.474509803921569, 1.0, "RoseGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoseMadder = new Color(0.890196078431373, 0.149019607843137, 0.211764705882353, 1.0, "RoseMadder");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RosePink = new Color(1, 0.4, 0.8, 1.0, "RosePink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoseQuartz = new Color(0.666666666666667, 0.596078431372549, 0.662745098039216, 1.0, "RoseQuartz");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoseTaupe = new Color(0.564705882352941, 0.364705882352941, 0.364705882352941, 1.0, "RoseTaupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoseVale = new Color(0.670588235294118, 0.305882352941176, 0.32156862745098, 1.0, "RoseVale");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Rosewood = new Color(0.396078431372549, 0, 0.0431372549019608, 1.0, "Rosewood");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RossoCorsa = new Color(0.831372549019608, 0, 0, 1.0, "RossoCorsa");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RosyBrown = new Color(0.737254901960784, 0.56078431372549, 0.56078431372549, 1.0, "RosyBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoyalAzure = new Color(0, 0.219607843137255, 0.658823529411765, 1.0, "RoyalAzure");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoyalBlueTraditional = new Color(0, 0.137254901960784, 0.4, 1.0, "RoyalBlueTraditional");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoyalBlueWeb = new Color(0.254901960784314, 0.411764705882353, 0.882352941176471, 1.0, "RoyalBlueWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoyalFuchsia = new Color(0.792156862745098, 0.172549019607843, 0.572549019607843, 1.0, "RoyalFuchsia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoyalPurple = new Color(0.470588235294118, 0.317647058823529, 0.662745098039216, 1.0, "RoyalPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RoyalYellow = new Color(0.980392156862745, 0.854901960784314, 0.368627450980392, 1.0, "RoyalYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RubineRed = new Color(0.819607843137255, 0, 0.337254901960784, 1.0, "RubineRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ruby = new Color(0.87843137254902, 0.0666666666666667, 0.372549019607843, 1.0, "Ruby");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RubyRed = new Color(0.607843137254902, 0.0666666666666667, 0.117647058823529, 1.0, "RubyRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ruddy = new Color(1, 0, 0.156862745098039, 1.0, "Ruddy");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RuddyBrown = new Color(0.733333333333333, 0.396078431372549, 0.156862745098039, 1.0, "RuddyBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RuddyPink = new Color(0.882352941176471, 0.556862745098039, 0.588235294117647, 1.0, "RuddyPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Rufous = new Color(0.658823529411765, 0.109803921568627, 0.0274509803921569, 1.0, "Rufous");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Russet = new Color(0.501960784313725, 0.274509803921569, 0.105882352941176, 1.0, "Russet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Rust = new Color(0.717647058823529, 0.254901960784314, 0.0549019607843137, 1.0, "Rust");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color RustyRed = new Color(0.854901960784314, 0.172549019607843, 0.262745098039216, 1.0, "RustyRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SacramentoStateGreen = new Color(0, 0.337254901960784, 0.247058823529412, 1.0, "SacramentoStateGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SaddleBrown = new Color(0.545098039215686, 0.270588235294118, 0.0745098039215686, 1.0, "SaddleBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SafetyOrangeBlazeOrange = new Color(1, 0.403921568627451, 0, 1.0, "SafetyOrangeBlazeOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Saffron = new Color(0.956862745098039, 0.768627450980392, 0.188235294117647, 1.0, "Saffron");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Salmon = new Color(1, 0.549019607843137, 0.411764705882353, 1.0, "Salmon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SalmonPink = new Color(1, 0.568627450980392, 0.643137254901961, 1.0, "SalmonPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sand = new Color(0.76078431372549, 0.698039215686274, 0.501960784313725, 1.0, "Sand");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SandDune = new Color(0.588235294117647, 0.443137254901961, 0.0901960784313725, 1.0, "SandDune");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sandstorm = new Color(0.925490196078431, 0.835294117647059, 0.250980392156863, 1.0, "Sandstorm");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SandyBrown = new Color(0.956862745098039, 0.643137254901961, 0.376470588235294, 1.0, "SandyBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SandyTaupe = new Color(0.588235294117647, 0.443137254901961, 0.0901960784313725, 1.0, "SandyTaupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sangria = new Color(0.572549019607843, 0, 0.0392156862745098, 1.0, "Sangria");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SapGreen = new Color(0.313725490196078, 0.490196078431373, 0.164705882352941, 1.0, "SapGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sapphire = new Color(0.0588235294117647, 0.32156862745098, 0.729411764705882, 1.0, "Sapphire");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SapphireBlue = new Color(0, 0.403921568627451, 0.647058823529412, 1.0, "SapphireBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SatinSheenGold = new Color(0.796078431372549, 0.631372549019608, 0.207843137254902, 1.0, "SatinSheenGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Scarlet = new Color(1, 0.141176470588235, 0, 1.0, "Scarlet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ScarletCrayola = new Color(0.992156862745098, 0.0549019607843137, 0.207843137254902, 1.0, "ScarletCrayola");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SchoolBusYellow = new Color(1, 0.847058823529412, 0, 1.0, "SchoolBusYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ScreaminGreen = new Color(0.462745098039216, 1, 0.47843137254902, 1.0, "ScreaminGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SeaBlue = new Color(0, 0.411764705882353, 0.580392156862745, 1.0, "SeaBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SeaGreen = new Color(0.180392156862745, 0.545098039215686, 0.341176470588235, 1.0, "SeaGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SealBrown = new Color(0.196078431372549, 0.0784313725490196, 0.0784313725490196, 1.0, "SealBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Seashell = new Color(1, 0.96078431372549, 0.933333333333333, 1.0, "Seashell");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SelectiveYellow = new Color(1, 0.729411764705882, 0, 1.0, "SelectiveYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sepia = new Color(0.43921568627451, 0.258823529411765, 0.0784313725490196, 1.0, "Sepia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Shadow = new Color(0.541176470588235, 0.474509803921569, 0.364705882352941, 1.0, "Shadow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ShamrockGreen = new Color(0, 0.619607843137255, 0.376470588235294, 1.0, "ShamrockGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ShockingPink = new Color(0.988235294117647, 0.0588235294117647, 0.752941176470588, 1.0, "ShockingPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ShockingPinkCrayola = new Color(1, 0.435294117647059, 1, 1.0, "ShockingPinkCrayola");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sienna = new Color(0.533333333333333, 0.176470588235294, 0.0901960784313725, 1.0, "Sienna");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Silver = new Color(0.752941176470588, 0.752941176470588, 0.752941176470588, 1.0, "Silver");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sinopia = new Color(0.796078431372549, 0.254901960784314, 0.0431372549019608, 1.0, "Sinopia");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Skobeloff = new Color(0, 0.454901960784314, 0.454901960784314, 1.0, "Skobeloff");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SkyBlue = new Color(0.529411764705882, 0.807843137254902, 0.92156862745098, 1.0, "SkyBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SkyMagenta = new Color(0.811764705882353, 0.443137254901961, 0.686274509803922, 1.0, "SkyMagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SlateBlue = new Color(0.415686274509804, 0.352941176470588, 0.803921568627451, 1.0, "SlateBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SlateGray = new Color(0.43921568627451, 0.501960784313725, 0.564705882352941, 1.0, "SlateGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SmaltDarkPowderBlue = new Color(0, 0.2, 0.6, 1.0, "SmaltDarkPowderBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SmokeyTopaz = new Color(0.576470588235294, 0.23921568627451, 0.254901960784314, 1.0, "SmokeyTopaz");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SmokyBlack = new Color(0.0627450980392157, 0.0470588235294118, 0.0313725490196078, 1.0, "SmokyBlack");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Snow = new Color(1, 0.980392156862745, 0.980392156862745, 1.0, "Snow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SpiroDiscoBall = new Color(0.0588235294117647, 0.752941176470588, 0.988235294117647, 1.0, "SpiroDiscoBall");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SpringBud = new Color(0.654901960784314, 0.988235294117647, 0, 1.0, "SpringBud");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SpringGreen = new Color(0, 1, 0.498039215686275, 1.0, "SpringGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color StPatrickSBlue = new Color(0.137254901960784, 0.16078431372549, 0.47843137254902, 1.0, "StPatrickSBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color SteelBlue = new Color(0.274509803921569, 0.509803921568627, 0.705882352941177, 1.0, "SteelBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color StilDeGrainYellow = new Color(0.980392156862745, 0.854901960784314, 0.368627450980392, 1.0, "StilDeGrainYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Stizza = new Color(0.6, 0, 0, 1.0, "Stizza");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Stormcloud = new Color(0.309803921568627, 0.4, 0.415686274509804, 1.0, "Stormcloud");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Straw = new Color(0.894117647058824, 0.850980392156863, 0.435294117647059, 1.0, "Straw");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sunglow = new Color(1, 0.8, 0.2, 1.0, "Sunglow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Sunset = new Color(0.980392156862745, 0.83921568627451, 0.647058823529412, 1.0, "Sunset");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Tan = new Color(0.823529411764706, 0.705882352941177, 0.549019607843137, 1.0, "Tan");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Tangelo = new Color(0.976470588235294, 0.301960784313725, 0, 1.0, "Tangelo");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Tangerine = new Color(0.949019607843137, 0.52156862745098, 0, 1.0, "Tangerine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TangerineYellow = new Color(1, 0.8, 0, 1.0, "TangerineYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TangoPink = new Color(0.894117647058824, 0.443137254901961, 0.47843137254902, 1.0, "TangoPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Taupe = new Color(0.282352941176471, 0.235294117647059, 0.196078431372549, 1.0, "Taupe");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TaupeGray = new Color(0.545098039215686, 0.52156862745098, 0.537254901960784, 1.0, "TaupeGray");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TeaGreen = new Color(0.815686274509804, 0.941176470588235, 0.752941176470588, 1.0, "TeaGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TeaRoseOrange = new Color(0.972549019607843, 0.513725490196078, 0.474509803921569, 1.0, "TeaRoseOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TeaRoseRose = new Color(0.956862745098039, 0.76078431372549, 0.76078431372549, 1.0, "TeaRoseRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Teal = new Color(0, 0.501960784313725, 0.501960784313725, 1.0, "Teal");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TealBlue = new Color(0.211764705882353, 0.458823529411765, 0.533333333333333, 1.0, "TealBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TealGreen = new Color(0, 0.509803921568627, 0.498039215686275, 1.0, "TealGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Telemagenta = new Color(0.811764705882353, 0.203921568627451, 0.462745098039216, 1.0, "Telemagenta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TennTawny = new Color(0.803921568627451, 0.341176470588235, 0, 1.0, "TennTawny");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TerraCotta = new Color(0.886274509803922, 0.447058823529412, 0.356862745098039, 1.0, "TerraCotta");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Thistle = new Color(0.847058823529412, 0.749019607843137, 0.847058823529412, 1.0, "Thistle");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color ThulianPink = new Color(0.870588235294118, 0.435294117647059, 0.631372549019608, 1.0, "ThulianPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TickleMePink = new Color(0.988235294117647, 0.537254901960784, 0.674509803921569, 1.0, "TickleMePink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TiffanyBlue = new Color(0.0392156862745098, 0.729411764705882, 0.709803921568627, 1.0, "TiffanyBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TigerSEye = new Color(0.87843137254902, 0.552941176470588, 0.235294117647059, 1.0, "TigerSEye");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Timberwolf = new Color(0.858823529411765, 0.843137254901961, 0.823529411764706, 1.0, "Timberwolf");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TitaniumYellow = new Color(0.933333333333333, 0.901960784313726, 0, 1.0, "TitaniumYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Tomato = new Color(1, 0.388235294117647, 0.27843137254902, 1.0, "Tomato");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Toolbox = new Color(0.454901960784314, 0.423529411764706, 0.752941176470588, 1.0, "Toolbox");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Topaz = new Color(1, 0.784313725490196, 0.486274509803922, 1.0, "Topaz");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TractorRed = new Color(0.992156862745098, 0.0549019607843137, 0.207843137254902, 1.0, "TractorRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TrolleyGrey = new Color(0.501960784313725, 0.501960784313725, 0.501960784313725, 1.0, "TrolleyGrey");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TropicalRainForest = new Color(0, 0.458823529411765, 0.368627450980392, 1.0, "TropicalRainForest");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TrueBlue = new Color(0, 0.450980392156863, 0.811764705882353, 1.0, "TrueBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TuftsBlue = new Color(0.254901960784314, 0.490196078431373, 0.756862745098039, 1.0, "TuftsBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Tumbleweed = new Color(0.870588235294118, 0.666666666666667, 0.533333333333333, 1.0, "Tumbleweed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TurkishRose = new Color(0.709803921568627, 0.447058823529412, 0.505882352941176, 1.0, "TurkishRose");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Turquoise = new Color(0.188235294117647, 0.835294117647059, 0.784313725490196, 1.0, "Turquoise");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TurquoiseBlue = new Color(0, 1, 0.937254901960784, 1.0, "TurquoiseBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TurquoiseGreen = new Color(0.627450980392157, 0.83921568627451, 0.705882352941177, 1.0, "TurquoiseGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TuscanRed = new Color(0.486274509803922, 0.282352941176471, 0.282352941176471, 1.0, "TuscanRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TwilightLavender = new Color(0.541176470588235, 0.286274509803922, 0.419607843137255, 1.0, "TwilightLavender");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color TyrianPurple = new Color(0.4, 0.00784313725490196, 0.235294117647059, 1.0, "TyrianPurple");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UaBlue = new Color(0, 0.2, 0.666666666666667, 1.0, "UaBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UaRed = new Color(0.850980392156863, 0, 0.298039215686275, 1.0, "UaRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ube = new Color(0.533333333333333, 0.470588235294118, 0.764705882352941, 1.0, "Ube");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UclaBlue = new Color(0.325490196078431, 0.407843137254902, 0.584313725490196, 1.0, "UclaBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UclaGold = new Color(1, 0.701960784313725, 0, 1.0, "UclaGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UfoGreen = new Color(0.235294117647059, 0.815686274509804, 0.43921568627451, 1.0, "UfoGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UltraPink = new Color(1, 0.435294117647059, 1, 1.0, "UltraPink");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Ultramarine = new Color(0.0705882352941176, 0.0392156862745098, 0.56078431372549, 1.0, "Ultramarine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UltramarineBlue = new Color(0.254901960784314, 0.4, 0.96078431372549, 1.0, "UltramarineBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Umber = new Color(0.388235294117647, 0.317647058823529, 0.27843137254902, 1.0, "Umber");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UnbleachedSilk = new Color(1, 0.866666666666667, 0.792156862745098, 1.0, "UnbleachedSilk");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UnitedNationsBlue = new Color(0.356862745098039, 0.572549019607843, 0.898039215686275, 1.0, "UnitedNationsBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UniversityOfCaliforniaGold = new Color(0.717647058823529, 0.529411764705882, 0.152941176470588, 1.0, "UniversityOfCaliforniaGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UnmellowYellow = new Color(1, 1, 0.4, 1.0, "UnmellowYellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UpForestGreen = new Color(0.00392156862745098, 0.266666666666667, 0.129411764705882, 1.0, "UpForestGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UpMaroon = new Color(0.482352941176471, 0.0666666666666667, 0.0745098039215686, 1.0, "UpMaroon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UpsdellRed = new Color(0.682352941176471, 0.125490196078431, 0.16078431372549, 1.0, "UpsdellRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Urobilin = new Color(0.882352941176471, 0.67843137254902, 0.129411764705882, 1.0, "Urobilin");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UsafaBlue = new Color(0, 0.309803921568627, 0.596078431372549, 1.0, "UsafaBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UscCardinal = new Color(0.6, 0, 0, 1.0, "UscCardinal");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UscGold = new Color(1, 0.8, 0, 1.0, "UscGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color UtahCrimson = new Color(0.827450980392157, 0, 0.247058823529412, 1.0, "UtahCrimson");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Vanilla = new Color(0.952941176470588, 0.898039215686275, 0.670588235294118, 1.0, "Vanilla");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VegasGold = new Color(0.772549019607843, 0.701960784313725, 0.345098039215686, 1.0, "VegasGold");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VenetianRed = new Color(0.784313725490196, 0.0313725490196078, 0.0823529411764706, 1.0, "VenetianRed");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Verdigris = new Color(0.262745098039216, 0.701960784313725, 0.682352941176471, 1.0, "Verdigris");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VermilionCinnabar = new Color(0.890196078431373, 0.258823529411765, 0.203921568627451, 1.0, "VermilionCinnabar");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VermilionPlochere = new Color(0.850980392156863, 0.376470588235294, 0.231372549019608, 1.0, "VermilionPlochere");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Veronica = new Color(0.627450980392157, 0.125490196078431, 0.941176470588235, 1.0, "Veronica");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Violet = new Color(0.56078431372549, 0, 1, 1.0, "Violet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VioletBlue = new Color(0.196078431372549, 0.290196078431373, 0.698039215686274, 1.0, "VioletBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VioletColorWheel = new Color(0.498039215686275, 0, 1, 1.0, "VioletColorWheel");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VioletRyb = new Color(0.525490196078431, 0.00392156862745098, 0.686274509803922, 1.0, "VioletRyb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VioletWeb = new Color(0.933333333333333, 0.509803921568627, 0.933333333333333, 1.0, "VioletWeb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Viridian = new Color(0.250980392156863, 0.509803921568627, 0.427450980392157, 1.0, "Viridian");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VividAuburn = new Color(0.572549019607843, 0.152941176470588, 0.141176470588235, 1.0, "VividAuburn");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VividBurgundy = new Color(0.623529411764706, 0.113725490196078, 0.207843137254902, 1.0, "VividBurgundy");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VividCerise = new Color(0.854901960784314, 0.113725490196078, 0.505882352941176, 1.0, "VividCerise");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VividTangerine = new Color(1, 0.627450980392157, 0.537254901960784, 1.0, "VividTangerine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color VividViolet = new Color(0.623529411764706, 0, 1, 1.0, "VividViolet");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color WarmBlack = new Color(0, 0.258823529411765, 0.258823529411765, 1.0, "WarmBlack");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Waterspout = new Color(0.643137254901961, 0.956862745098039, 0.976470588235294, 1.0, "Waterspout");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Wenge = new Color(0.392156862745098, 0.329411764705882, 0.32156862745098, 1.0, "Wenge");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Wheat = new Color(0.96078431372549, 0.870588235294118, 0.701960784313725, 1.0, "Wheat");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color White = new Color(1, 1, 1, 1.0, "White");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color WhiteSmoke = new Color(0.96078431372549, 0.96078431372549, 0.96078431372549, 1.0, "WhiteSmoke");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color WildBlueYonder = new Color(0.635294117647059, 0.67843137254902, 0.815686274509804, 1.0, "WildBlueYonder");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color WildStrawberry = new Color(1, 0.262745098039216, 0.643137254901961, 1.0, "WildStrawberry");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color WildWatermelon = new Color(0.988235294117647, 0.423529411764706, 0.52156862745098, 1.0, "WildWatermelon");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Wine = new Color(0.447058823529412, 0.184313725490196, 0.215686274509804, 1.0, "Wine");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color WineDregs = new Color(0.403921568627451, 0.192156862745098, 0.27843137254902, 1.0, "WineDregs");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Wisteria = new Color(0.788235294117647, 0.627450980392157, 0.862745098039216, 1.0, "Wisteria");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color WoodBrown = new Color(0.756862745098039, 0.603921568627451, 0.419607843137255, 1.0, "WoodBrown");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Xanadu = new Color(0.450980392156863, 0.525490196078431, 0.470588235294118, 1.0, "Xanadu");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color YaleBlue = new Color(0.0588235294117647, 0.301960784313725, 0.572549019607843, 1.0, "YaleBlue");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Yellow = new Color(1, 1, 0, 1.0, "Yellow");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color YellowGreen = new Color(0.603921568627451, 0.803921568627451, 0.196078431372549, 1.0, "YellowGreen");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color YellowMunsell = new Color(0.937254901960784, 0.8, 0, 1.0, "YellowMunsell");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color YellowNcs = new Color(1, 0.827450980392157, 0, 1.0, "YellowNcs");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color YellowOrange = new Color(1, 0.682352941176471, 0.258823529411765, 1.0, "YellowOrange");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color YellowProcess = new Color(1, 0.937254901960784, 0, 1.0, "YellowProcess");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color YellowRyb = new Color(0.996078431372549, 0.996078431372549, 0.2, 1.0, "YellowRyb");

        /// <summary>
        /// Transparent color.
        /// </summary>
        public static readonly Color Zaffre = new Color(0, 0.0784313725490196, 0.658823529411765, 1.0, "Zaffre");

        /// <summary>
        /// ZinnwalditeBrown color
        /// </summary>
        public static readonly Color ZinnwalditeBrown = new Color(0.172549019607843, 0.0862745098039216, 0.0313725490196078, 1.0, "ZinnwalditeBrown");

        #endregion Predefined colors

        #region Fields

        /// <summary>
        /// Stores the set of built in colors.
        /// </summary>
        private static Dictionary<string, Color> sColors;

        /// <summary>
        /// Stores the color name if any.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the red component value.
        /// </summary>
        private double mR;

        /// <summary>
        /// Stores the green component value.
        /// </summary>
        private double mG;

        /// <summary>
        /// Stores the blue component value.
        /// </summary>
        private double mB;

        /// <summary>
        /// Stores the alpha component value.
        /// </summary>
        private double mA;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Turns this color into an array.
        /// </summary>
        /// <returns></returns>
		public double[] ToArray
        {
            get
            {
                return new double[]
                {
                    this.mR,
                    this.mG,
                    this.mB,
                    this.mA
                };
            }
        }

        /// <summary>
        /// Gets the Hue of this RGBA color.
        /// </summary>
        public double Hue
        {
            get
            {
                double lMin = Math.Min(R, Math.Min(G, B));   // Min. value of RGB
                double lMax = Math.Max(R, Math.Max(G, B));   // Max. value of RGB
                double lDiff = lMax - lMin;                  // Delta RGB value

                if ( lDiff == 0 ) //This is a gray, no chroma...
                {
                    return 0;
                }

                double lHue = 0.0;

                double lDiffR = (((lMax - R) / 6.0) + (lDiff / 2.0)) / lDiff;
                double lDiffG = (((lMax - G) / 6.0) + (lDiff / 2.0)) / lDiff;
                double lDiffB = (((lMax - B) / 6.0) + (lDiff / 2.0)) / lDiff;

                if (R == lMax)
                {
                    lHue = lDiffB - lDiffG;
                }
                else if (G == lMax)
                {
                    lHue = (1.0 / 3.0) + lDiffR - lDiffB;
                }
                else if (B == lMax)
                {
                    lHue = (2.0 / 3.0) + lDiffG - lDiffR;
                }

                if (lHue < 0)
                {
                    lHue += 1;
                }

                if (lHue > 1)
                {
                    lHue -= 1;
                }

                return lHue;
            }
        }

        /// <summary>
        /// Gets the Saturation of this RGBA color.
        /// </summary>
        public double Saturation
        {
            get
            {
                return Math.Max(R, Math.Max(G, B)); //Max. value of RGB
            }
        }

        /// <summary>
        /// Gets the Value/Luminance of this RGBA color.
        /// </summary>
        public double Value
        {
            get
            {
                double lMin = Math.Min(R, Math.Min(G, B));   // Min. value of RGB
                double lMax = Math.Max(R, Math.Max(G, B));   // Max. value of RGB
                double lDiff = lMax - lMin;                  // Delta RGB value
                return lDiff == 0 ? 0 : lDiff / lMax;
            }
        }

        /// <summary>
        /// Gets the color name if any.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
            internal set
            {
                this.mName = value;
            }
        }

        /// <summary>
        /// Gets or sets the red component value.
        /// </summary>
        public double R
        {
            get
            {
                return this.mR;
            }
            set
            {
                this.mR = value;
            }
        }

        /// <summary>
        /// Gets or sets the green component value.
        /// </summary>
        public double G
        {
            get
            {
                return this.mG;
            }
            set
            {
                this.mG = value;
            }
        }

        /// <summary>
        /// Gets or sets the blue component value.
        /// </summary>
        public double B
        {
            get
            {
                return this.mB;
            }
            set
            {
                this.mB = value;
            }
        }

        /// <summary>
        /// Gets or sets the alpha component value.
        /// </summary>
        public double A
        {
            get
            {
                return this.mA;
            }
            set
            {
                this.mA = value;
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes static member(s) of the <see cref="Color"/> class.
        /// </summary>
        static Color()
        {
            sColors = new Dictionary<string, Color>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Color"/> class.
        /// </summary>
        /// <param name="pRed">The red component value.</param>
        /// <param name="pGreen">The green component value.</param>
        /// <param name="pBlue">The blue component value.</param>
        /// <param name="pAlpha">The alpha component value.</param>
        public Color(double pRed, double pGreen, double pBlue, double pAlpha)
		{
            this.mA = pAlpha.Clamp(0, 1);
            this.mR = pRed.Clamp(0, 1);
            this.mG = pGreen.Clamp(0, 1);
            this.mB = pBlue.Clamp(0, 1);
			this.mName = "";
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Color"/> class.
        /// </summary>
        /// <param name="pRed">The red component value.</param>
        /// <param name="pGreen">The green component value.</param>
        /// <param name="pBlue">The blue component value.</param>
        /// <param name="pAlpha">The alpha component value.</param>
        /// <param name="pName">The color name.</param>
		internal Color(double pRed, double pGreen, double pBlue, double pAlpha, string pName)
		{
			this.mA = pAlpha;
			this.mR = pRed;
			this.mG = pGreen;
			this.mB = pBlue;
			this.mName = pName;
			sColors[ this.mName ] = this;
		}

        #endregion Constructors
        
		#region Methods

        /// <summary>
        /// Turns the color into a strinf.
        /// </summary>
        /// <param name="pColor"></param>
        public static implicit operator string(Color pColor)
        {
            return pColor.ToString();
        }

        /// <summary>
        /// Turns a string into a color.
        /// </summary>
        /// <param name="pColorAsString"></param>
		public static implicit operator Color(string pColorAsString)
		{
            if ( string.IsNullOrEmpty( pColorAsString ) )
            {
                return White;
            }

			string[] lColor = pColorAsString.Split(new char[] { ',' });
			if ( lColor.Length == 1 )
			{
                Color lResult;
                if ( sColors.TryGetValue( pColorAsString, out lResult ) )
                {
                    return lResult;
                }
			}
			return new Color( double.Parse(lColor[0]),
				              double.Parse(lColor[1]),
				              double.Parse(lColor[2]),
				              double.Parse(lColor[3]));
		}

        /// <summary>
        /// Checks whether the two colors are equal or not.
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <returns></returns>
		public static bool operator ==(Color pLeft, Color pRight)
		{
			return pLeft.A != pRight.A ? false :
				    pLeft.R != pRight.R ? false :
				    pLeft.G != pRight.G ? false :
				    pLeft.B != pRight.B ? false : true;
		}

        /// <summary>
        /// Checks whether the two colors are different or not.
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <returns></returns>
		public static bool operator !=(Color pLeft, Color pRight)
		{
			return pLeft.A == pRight.A ? false :
				    pLeft.R == pRight.R ? false :
				    pLeft.G == pRight.G ? false :
				    pLeft.B == pRight.B ? false : true;

		}

        /// <summary>
        /// Checks whether the color is equal of the color name or not.
        /// </summary>
        /// <param name="pColor"></param>
        /// <param name="pColorName"></param>
        /// <returns></returns>
        public static bool operator ==(Color pColor, string pColorName)
		{
			return string.Equals(pColor.Name, pColorName, StringComparison.Ordinal);
		}

        /// <summary>
        /// Checks whether the color is different of the color name or not.
        /// </summary>
        /// <param name="pColor"></param>
        /// <param name="pColorName"></param>
        /// <returns></returns>
		public static bool operator !=(Color pColor, string pColorName)
		{
			return !string.Equals(pColor.Name, pColorName, StringComparison.Ordinal);
		}

        /// <summary>
        /// Checks whether the color is equal of the color name or not.
        /// </summary>
        /// <param name="pColor"></param>
        /// <param name="pColorName"></param>
        /// <returns></returns>
		public static bool operator ==(string pColorName, Color pColor)
		{
			return string.Equals (pColor.Name, pColorName, StringComparison.Ordinal);
		}

        /// <summary>
        /// Checks whether the color is different of the color name or not.
        /// </summary>
        /// <param name="pColor"></param>
        /// <param name="pColorName"></param>
        /// <returns></returns>
		public static bool operator !=(string pColorName, Color pColor)
		{
			return !string.Equals (pColor.Name, pColorName, StringComparison.Ordinal);
		}

        /// <summary>
        /// Applies a factor to the alpha component to the color.
        /// </summary>
        /// <param name="pColor"></param>
        /// <param name="pFactor"></param>
        /// <returns></returns>
		public static Color operator *(Color pColor, double pFactor)
		{
            return new Color( pColor.R, pColor.G, pColor.B, pColor.A * pFactor );
		}

        /// <summary>
        /// Sums two colors together.
        /// </summary>
        /// <param name="pColor1"></param>
        /// <param name="pColor2"></param>
        /// <returns></returns>
		public static Color operator +(Color pColor1, Color pColor2)
		{
            return new Color(pColor1.R + pColor2.R, pColor1.G + pColor2.G, pColor1.B + pColor2.B, pColor1.A + pColor2.A);
		}

        /// <summary>
        /// Subtracts two colors together.
        /// </summary>
        /// <param name="pColor1"></param>
        /// <param name="pColor2"></param>
        /// <returns></returns>
		public static Color operator -(Color pColor1, Color pColor2)
		{
            return new Color(pColor1.R - pColor2.R, pColor1.G - pColor2.G, pColor1.B - pColor2.B, pColor1.A - pColor2.A);
		}
		
        /// <summary>
        /// Resets the color's name.
        /// </summary>
		public void ResetName()
        {
			this.mName = string.Empty;
		}
        
        /// <summary>
        /// Gets the color hashcode.
        /// </summary>
        /// <returns></returns>
		public override int GetHashCode()
		{
			unchecked // Overflow is fine, just wrap
			{
				int lHashCode = 17;
				// Suitable nullity checks etc, of course :)
				lHashCode = lHashCode * 23 + A.GetHashCode();
				lHashCode = lHashCode * 23 + R.GetHashCode();
				lHashCode = lHashCode * 23 + G.GetHashCode();
				lHashCode = lHashCode * 23 + B.GetHashCode();
				return lHashCode;
			}
		}

        /// <summary>
        /// Checks whether this object is equal to another.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns></returns>
		public override bool Equals(object pOther)
		{
			return (pOther == null || pOther.GetType() != TColor) ?
				    false :
				    this == (Color)pOther;
		}

        /// <summary>
        /// Turns this color into a string.
        /// </summary>
        /// <returns></returns>
		public override string ToString()
		{
            if ( string.IsNullOrEmpty( this.mName ) == false )
            {
                return this.mName;
            }
            
			return string.Format("{0},{1},{2},{3}", R, G, B, A);
		}

        /// <summary>
        /// Turns a string into a color.
        /// </summary>
        /// <param name="pColorAsString"></param>
        /// <returns></returns>
        public static object Parse(string pColorAsString)
        {
            return (Color)pColorAsString;
        }

        /// <summary>
        /// Turns a HSV color into a RGBA color.
        /// </summary>
        /// <param name="pHue"></param>
        /// <param name="pValue"></param>
        /// <param name="pSaturation"></param>
        /// <param name="pAlpha"></param>
        /// <returns></returns>
		public static Color FromHSV(double pHue, double pValue = 1.0, double pSaturation = 1.0, double pAlpha = 1.0)
        {
			Color lColor = Color.Black;
			lColor.ResetName ();
			lColor.A = pAlpha;
			if (pSaturation == 0) //HSV from 0 to 1
            {
				lColor.R = pValue;
				lColor.G = pValue;
				lColor.B = pValue;
			}
            else
            {
				double var_h = pHue * 6.0;

                if (var_h == 6.0)
                {
                    var_h = 0;  //H must be < 1
                }

				double var_i = Math.Floor( var_h );	//Or ... var_i = floor( var_h )
				double var_1 = pValue * ( 1.0 - pSaturation );
				double var_2 = pValue * (1.0 - pSaturation * (var_h - var_i));
				double var_3 = pValue * (1.0 - pSaturation * (1.0 - (var_h - var_i)));

				if (var_i == 0.0)
                {
					lColor.R = pValue;
					lColor.G = var_3;
					lColor.B = var_1;
				}
                else if ( var_i == 1.0 ) { lColor.R = var_2 ; lColor.G = pValue     ; lColor.B = var_1; }
				else if ( var_i == 2 ) { lColor.R = var_1 ; lColor.G = pValue     ; lColor.B = var_3; }
				else if ( var_i == 3 ) { lColor.R = var_1 ; lColor.G = var_2 ; lColor.B = pValue;     }
				else if ( var_i == 4 ) { lColor.R = var_3 ; lColor.G = var_1 ; lColor.B = pValue;    }
				else                   { lColor.R = pValue     ; lColor.G = var_1 ; lColor.B = var_2; }
			}

			return lColor;
		}

        #endregion Methods
    }
}
