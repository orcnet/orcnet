﻿using System;
using System.Reflection;

namespace OrcNet.UI.Extensions
{
    /// <summary>
    /// Definition of the <see cref="PropertyInfoExtensions"/> class.
    /// </summary>
    public static class PropertyInfoExtensions
    {
        #region Methods

        /// <summary>
        /// Gets the property having the given name.
        /// </summary>
        /// <param name="pType">The type the property must be get on.</param>
        /// <param name="pPropertyName">The property name.</param>
        /// <returns>The property info.</returns>
        public static PropertyInfo GetProperty(this Type pType, string pPropertyName)
        {
            return pType.GetProperty( pPropertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public );
        }

        #endregion Methods
    }
}
