﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="UIThread"/> class.
    /// 
    /// Thread monitored by the current interface triggering 
    /// the Finished event when the state is equal to Stopped
    /// </summary>
    public class UIThread : IEquatable<UIThread>, IEquatable<Thread>
    {
        #region Fields

        /// <summary>
        /// Stores the system thread.
        /// </summary>
        private Thread mThread;

        /// <summary>
        /// Stores the global threads cache.
        /// </summary>
        private static readonly Dictionary<Thread, UIThread> sCache = new Dictionary<Thread, UIThread>();

        /// <summary>
        /// Stores the sync root.
        /// </summary>
        private static readonly object sSyncRoot = new object();
		
        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on thread finished.
        /// </summary>
        public event EventHandler Finished;

        #endregion Events

        #region Properties
        
        /// <summary>
        /// Gets the current thread.
        /// </summary>
        public static UIThread CurrentThread
        {
            get
            {
                return sCache[ Thread.CurrentThread ];
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UIThread"/> class.
        /// </summary>
        /// <param name="pStart">The thread start callback.</param>
        public UIThread(ParameterizedThreadStart pStart)
        {
			this.mThread = new Thread( pStart );
			this.mThread.IsBackground = true;

            sCache[ this.mThread ] = this;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks the thread's state.
        /// </summary>
        public void CheckState()
        {
			if ( this.mThread.ThreadState != ThreadState.Stopped )
            {
                return;
            }

            this.Finished.Raise( this, EventArgs.Empty );
		}

        /// <summary>
        /// Starts the thread.
        /// </summary>
        /// <param name="pParameter">The parameter if needed.</param>
		public void Start(object pParameter = null)
        {
            this.mThread.Start( pParameter );
        }

        /// <summary>
        /// Cancels the thread.
        /// </summary>
		public void Cancel()
        {
			if ( this.mThread.IsAlive )
            {
                this.mThread.Join();
			}

            sCache.Remove( this.mThread );
		}

        /// <summary>
        /// Checks whether the first thread is equal to the second.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public static bool operator == (UIThread pFirst, UIThread pSecond)
        {
            if ( object.ReferenceEquals( pFirst, null ) )
            {
                return object.ReferenceEquals( pSecond, null );
            }

            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Checks whether the first thread is different from the second.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public static bool operator !=(UIThread pFirst, UIThread pSecond)
        {
            return (pFirst == pSecond) == false;
        }

        /// <summary>
        /// Checks whether the first thread is equal to the second.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public static bool operator == (UIThread pFirst, Thread pSecond)
        {
            if ( object.ReferenceEquals( pFirst, null ) )
            {
                return object.ReferenceEquals( pSecond, null );
            }

            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Checks whether the first thread is different from the second.
        /// </summary>
        /// <param name="pFirst"></param>
        /// <param name="pSecond"></param>
        /// <returns>True if equals, false otherwise.</returns>
        public static bool operator !=(UIThread pFirst, Thread pSecond)
        {
            return (pFirst == pSecond) == false;
        }

        /// <summary>
        /// Checks whether the given thread is equal to another.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns></returns>
        public override bool Equals(object pOther)
        {
            if ( pOther is Thread )
            {
                return this.Equals( pOther as Thread );
            }
            else if ( pOther is UIThread )
            {
                return this.Equals( pOther as UIThread );
            }

            return false;
        }

        /// <summary>
        /// Checks whether the given thread is equal to another.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns></returns>
        public bool Equals(UIThread pOther)
        {
            if ( object.ReferenceEquals( pOther, null ) )
            {
                return false;
            }

            return this.Equals( pOther.mThread );
        }

        /// <summary>
        /// Checks whether the given thread is equal to another.
        /// </summary>
        /// <param name="pOther"></param>
        /// <returns></returns>
        public bool Equals(Thread pOther)
        {
            if ( object.ReferenceEquals( pOther, null ) )
            {
                return false;
            }

            return this.mThread == pOther;
        }

        /// <summary>
        /// Gets the hashcode.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.mThread.GetHashCode();
        }

        /// <summary>
        /// Implicit cast to System Thread
        /// </summary>
        /// <param name="pUIThread"></param>
        public static implicit operator Thread(UIThread pUIThread)
        {
            return pUIThread.mThread;
        }

        #endregion Methods
    }
}

