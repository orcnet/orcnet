﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="NativeWindowParameters"/> class.
    /// </summary>
    public struct NativeWindowParameters
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the parameters containes a window size or not.
        /// </summary>
        private bool mHasAssignedSize;

        /// <summary>
        /// Stores the native window hooks.
        /// </summary>
        private NativeWindowHooks mHooks;

        /// <summary>
        /// Stores the window name.
        /// </summary>
        private string mWindowName;

        /// <summary>
        /// Stores the window style flags.
        /// </summary>
        private int mWindowStyleFlags;

        /// <summary>
        /// Stores the window X position.
        /// </summary>
        private int mX;

        /// <summary>
        /// Stores the window Y position.
        /// </summary>
        private int mY;

        /// <summary>
        /// Stores the window width.
        /// </summary>
        private int mWidth;

        /// <summary>
        /// Stores the window height.
        /// </summary>
        private int mHeight;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the parent window if any.
        /// </summary>
        public INativeWindow Parent
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the flag indicating whether the parameters containes a window size or not.
        /// </summary>
        public bool HasAssignedSize
        {
            get
            {
                return this.mHasAssignedSize;
            }
        }

        /// <summary>
        /// Gets the native window hooks.
        /// </summary>
        public NativeWindowHooks Hooks
        {
            get
            {
                return this.mHooks;
            }
            set
            {
                this.mHooks = value;
            }
        }

        /// <summary>
        /// Gets or sets the window name.
        /// </summary>
        public string WindowName
        {
            get
            {
                return this.mWindowName;
            }
            set
            {
                this.mWindowName = value;
            }
        }

        /// <summary>
        /// Gets or sets the window style flags.
        /// </summary>
        public int WindowStyleFlags
        {
            get
            {
                return this.mWindowStyleFlags;
            }
            set
            {
                this.mWindowStyleFlags = value;
            }
        }

        /// <summary>
        /// Gets or sets the window X position.
        /// </summary>
        public int X
        {
            get
            {
                return this.mX;
            }
            set
            {
                this.mX = value;
            }
        }

        /// <summary>
        /// Gets or sets the window Y position.
        /// </summary>
        public int Y
        {
            get
            {
                return this.mY;
            }
            set
            {
                this.mY = value;
            }
        }

        /// <summary>
        /// Gets or sets the window width.
        /// </summary>
        public int Width
        {
            get
            {
                return this.mWidth;
            }
            set
            {
                this.mWidth = value;
                this.mHasAssignedSize = true;
            }
        }

        /// <summary>
        /// Gets or sets the window height.
        /// </summary>
        public int Height
        {
            get
            {
                return this.mHeight;
            }
            set
            {
                this.mHeight = value;
                this.mHasAssignedSize = true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowParameters"/> class.
        /// </summary>
        /// <param name="pName">The window name.</param>
        /// <param name="pHooks">The window hooks</param>
        public NativeWindowParameters(string pName, NativeWindowHooks pHooks)
        {
            this.Parent = null;
            this.mHasAssignedSize = false;
            this.mHooks = pHooks;

            this.mWindowStyleFlags = (int)StyleFlags.Visible;
            this.mWindowStyleFlags |= (int)StyleFlags.HasMenu;
            this.mWindowStyleFlags |= (int)StyleFlags.HasTitle;
            this.mWindowStyleFlags |= (int)StyleFlags.HasBorder;

            this.mX = 0;
            this.mY = 0;

            this.mWidth  = 1;
            this.mHeight = 1;

            this.mWindowName = pName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NativeWindowParameters"/> class.
        /// </summary>
        /// <param name="pName">The window name.</param>
        /// <param name="pWidth">The window width.</param>
        /// <param name="pHeight">The window height.</param>
        /// <param name="pHooks">The window hooks</param>
        public NativeWindowParameters(string pName, int pWidth, int pHeight, NativeWindowHooks pHooks) :
        this( pName, pHooks )
        {
            this.Width  = pWidth;
            this.Height = pHeight;
        }

        #endregion Constructor

        #region Methods



        #endregion Methods
    }
}
