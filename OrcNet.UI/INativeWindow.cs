﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="INativeWindow"/> interface.
    /// </summary>
    public interface INativeWindow
    {
        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the window is minimized or not.
        /// </summary>
        bool IsMinimized
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the window is maximized or not.
        /// </summary>
        bool IsMaximized
        {
            get;
        }

        /// <summary>
        /// Gets the left window position.
        /// </summary>
        int Left
        {
            get;
        }

        /// <summary>
        /// Gets the top window position.
        /// </summary>
        int Top
        {
            get;
        }

        /// <summary>
        /// Gets the window width.
        /// </summary>
        int Width
        {
            get;
        }

        /// <summary>
        /// Gets the window height.
        /// </summary>
        int Height
        {
            get;
        }

        /// <summary>
        /// Gets the styles flags.
        /// </summary>
        int StyleFlags
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the source window is active or not.
        /// </summary>
        bool IsActive
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the window has currently the focus or not.
        /// </summary>
        bool HasFocus
        {
            get;
        }

        #endregion Properties
    }
}
