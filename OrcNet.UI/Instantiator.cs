﻿//
//  Instantiator.cs
//
//  Author:
//       Jean-Philippe Bruyère <jp.bruyere@hotmail.com>
//
//  Copyright (c) 2016 jp
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Threading;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.Reflection.Emit;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using OrcNet.UI.IML;
using System.Xml.Linq;

namespace OrcNet.UI
{
    /// <summary>
    /// Instanciator delegate prototype definition.
    /// </summary>
    /// <param name="iface"></param>
    /// <returns></returns>
	public delegate object InstanciatorInvoker(PresentationSource iface);

	/// <summary>
	/// Instantiator
	/// </summary>
	public class Instantiator
	{
		#region Dynamic Method ID generation
		static long curId = 0;
		internal static long NewId {
			get { return curId++; }
		}
		#endregion

        /// <summary>
        /// Root type.
        /// </summary>
		public Type RootType;

        /// <summary>
        /// Loader.
        /// </summary>
		private InstanciatorInvoker loader;

        /// <summary>
        /// The source path.
        /// </summary>
		internal string sourcePath;

		#region CTOR

        /// <summary>
        /// Initializes a new instance of the <see cref="Instantiator"/> class.
        /// </summary>
        /// <param name="path"></param>
		public Instantiator (string path) : 
        this (PresentationSource.GetStreamFromPath(path))
        {
			sourcePath = path;
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Instantiator"/> class.
        /// </summary>
        /// <param name="pRoot"></param>
        public Instantiator(XElement pRoot)
        {
#if DEBUG_LOAD
			Stopwatch loadingTime = new Stopwatch ();
			loadingTime.Start ();
#endif
            this.ParseIML2( pRoot );
#if DEBUG_LOAD
			loadingTime.Stop ();
			Debug.WriteLine ("IML Instantiator creation '{2}' : {0} ticks, {1} ms",
				loadingTime.ElapsedTicks, loadingTime.ElapsedMilliseconds, imlPath);
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Instantiator"/> class.
        /// </summary>
        /// <param name="stream"></param>
		public Instantiator (Stream stream) :
        this( XElement.Load(stream) )
		{
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Instantiator"/> class.
        /// </summary>
        /// <param name="_root"></param>
        /// <param name="_loader"></param>
		public Instantiator (Type _root, InstanciatorInvoker _loader)
		{
			RootType = _root;
			loader = _loader;
		}

        /// <summary>
        /// Create from a Iml fragment.
        /// </summary>
        /// <param name="fragment"></param>
        /// <returns></returns>
		public static Instantiator CreateFromImlFragment (string fragment)
		{
			try {
				using (Stream s = new MemoryStream (Encoding.UTF8.GetBytes (fragment))) {
					return new Instantiator (s);
				}
			} catch (Exception ex) {
				throw new Exception ("Error loading fragment:\n" + fragment + "\n", ex);
			}
		}
		#endregion

        /// <summary>
        /// Create an instance of the given interface.
        /// </summary>
        /// <param name="iface"></param>
        /// <returns></returns>
		public GraphicObject CreateInstance(PresentationSource iface)
        {
			return loader (iface) as GraphicObject;
		}

		List<DynamicMethod> dsValueChangedDynMeths = new List<DynamicMethod>();
		List<Delegate> cachedDelegates = new List<Delegate>();
		List<int> templateCachedDelegateIndices = new List<int>();//store indices of template delegate to be handled by root parentChanged event
		Delegate templateBinding;

		#region IML parsing

		/// <summary>
		/// Parses IML and build a dynamic method that will be used to instanciate one or multiple occurence of the IML file or fragment
		/// </summary>
		private void ParseIML(XElement pRoot)
        {
			Context lContext = new Context( this.FindRootType( pRoot ));

            lContext.nodesStack.Push( new Node( lContext.RootType ) );
			this.EmitLoader( pRoot, lContext );
            lContext.nodesStack.Pop();

            foreach ( int idx in templateCachedDelegateIndices )
            {
                lContext.emitCachedDelegateHandlerAddition(idx, CompilerServices.eiLogicalParentChanged);
            }

            lContext.ResolveNamedTargets ();

			emitBindingDelegates( lContext );

			lContext.il.Emit(OpCodes.Ldloc_0);//load root obj to return
            lContext.il.Emit(OpCodes.Ret);
            
			RootType = lContext.RootType;
			loader = (InstanciatorInvoker)lContext.dm.CreateDelegate (typeof (InstanciatorInvoker), this);
		}

        /// <summary>
		/// Parses IML and build a dynamic method that will be used to instanciate one or multiple occurence of the IML file or fragment
		/// </summary>
		private void ParseIML2(XElement pRoot)
        {
			this.RootType = this.FindRootType( pRoot );

   //         lContext.nodesStack.Push( new Node( lContext.RootType ) );
			//this.EmitLoader( pRoot, lContext );
   //         lContext.nodesStack.Pop();

   //         foreach ( int idx in templateCachedDelegateIndices )
   //         {
   //             lContext.emitCachedDelegateHandlerAddition(idx, CompilerServices.eiLogicalParentChanged);
   //         }

   //         lContext.ResolveNamedTargets ();

			//emitBindingDelegates( lContext );

			//lContext.il.Emit(OpCodes.Ldloc_0);//load root obj to return
   //         lContext.il.Emit(OpCodes.Ret);
            
			//loader = (InstanciatorInvoker)lContext.dm.CreateDelegate (typeof (InstanciatorInvoker), this);
		}

		/// <summary>
		/// read first node to set GraphicObject class for loading
		/// and let reader position on that node
		/// </summary>
        /// <param name="pElement"></param>
		private Type FindRootType(XElement pElement)
		{
			string root = "Object";
			if( pElement != null )
            {
                root = pElement.Name.LocalName;
            }

			Type t = Type.GetType ("Crow." + root);
			if (t == null)
            {
				Assembly a = Assembly.GetEntryAssembly ();
				foreach (Type expT in a.GetExportedTypes ())
                {
					if (expT.Name == root)
                    {
						t = expT;
						break;
					}
				}
			}

			if (t == null)
            { 
				throw new Exception ("IML parsing error: undefined root type (" + root + ")");
            }

			return t;
		}

        /// <summary>
        /// Loads a UI element.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pContext"></param>
		private void EmitLoader(XElement pElement, Context pContext)
		{
			if ( pContext.nodesStack.Peek().HasTemplate )
            {
                this.EmitTemplateLoad( pContext, pElement );
            }

			this.EmitGOLoad( pContext, pElement );
		}

        /// <summary>
        /// Emit a template load.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pFragment"></param>
		private void EmitTemplateLoad(Context pContext, XElement pFragment)
        {
            //if its a template, first read template elements
            List<string[]> lItemTemplateIds = new List<string[]>();
            bool inlineTemplate = false;

            XAttribute lXTemplatePath = pFragment.Attribute( "Template" );
                
            //reader.Read();
            //int depth = reader.Depth + 1;
            foreach( XElement lChild in pFragment.Elements() )
            {
                //if ( reader.IsStartElement() == false || 
                //     reader.Depth > depth )
                //{
                //    continue;
                //}

                string lChildName = lChild.Name.LocalName;
                if ( lChildName == "Template" )
                {
                    inlineTemplate = true;
                    //reader.Read();
                    this.ReadChildren( lChild, pContext, -1 );
                }
                else if ( lChildName == "ItemTemplate" )
                {
                    string lDataType = "default";
                    string lDatas = "";
                    string lPath = "";
                    XAttribute lXDataType = lChild.Attribute( "DataType" );
                    if ( lXDataType != null )
                    {
                        lDataType = lXDataType.Value;
                    }

                    XAttribute lXData = lChild.Attribute( "Data" );
                    if ( lXData != null )
                    {
                        lDatas = lXData.Value;
                    }

                    XAttribute lXPath = lChild.Attribute( "Path" );
                    if ( lXPath != null )
                    {
                        lPath = lXPath.Value;
                    }
                    
                    string itemTmpID;

                    if (string.IsNullOrEmpty(lPath))
                    {
                        itemTmpID = Guid.NewGuid().ToString();
                        PresentationSource.Instantiators[itemTmpID] = new ItemTemplate(lChild, lDataType, lDatas);//new ItemTemplate(new MemoryStream(Encoding.UTF8.GetBytes( lChild.ReadInnerXml())), lDataType, lDatas);
                    }
                    else
                    {
                        if ( lChild.IsEmpty == false )
                        {
                            throw new Exception("ItemTemplate with Path attribute may not include sub nodes");
                        }

                        itemTmpID = lPath;
                        PresentationSource.Instantiators[itemTmpID] = new ItemTemplate( PresentationSource.GetStreamFromPath(itemTmpID), lDataType, lDatas );
                    }
                    lItemTemplateIds.Add(new string[] { lDataType, itemTmpID, lDatas });
                }
            }
            
            if ( inlineTemplate == false )
            {
                //load from lPath or default template
                pContext.il.Emit(OpCodes.Ldloc_0);//Load  current templatedControl ref
                if ( lXTemplatePath == null )
                {
                    pContext.il.Emit(OpCodes.Ldnull);//default template loading
                }
                else
                {
                    pContext.il.Emit(OpCodes.Ldarg_1);//load currentInterface
                    pContext.il.Emit(OpCodes.Ldstr, lXTemplatePath.Value ); //Load template lPath string
                    pContext.il.Emit(OpCodes.Callvirt,//call Interface.Load(lPath)
                        CompilerServices.miIFaceLoad);
                }

                pContext.il.Emit(OpCodes.Callvirt, CompilerServices.miLoadTmp);//load template
            }

            //copy item templates (review this)
            foreach (string[] iTempId in lItemTemplateIds)
            {
                pContext.il.Emit(OpCodes.Ldloc_0);//load TempControl ref
                pContext.il.Emit(OpCodes.Ldfld, CompilerServices.fldItemTemplates);//load ItemTemplates dic field
                pContext.il.Emit(OpCodes.Ldstr, iTempId[0]);//load key
                pContext.il.Emit(OpCodes.Ldstr, iTempId[1]);//load value
                pContext.il.Emit(OpCodes.Callvirt, CompilerServices.miGetITemp);
                pContext.il.Emit(OpCodes.Callvirt, CompilerServices.miAddITemp);

                if ( string.IsNullOrEmpty(iTempId[2]) == false )
                {
                    //expand delegate creation
                    pContext.il.Emit(OpCodes.Ldloc_0);//load TempControl ref
                    pContext.il.Emit(OpCodes.Ldfld, CompilerServices.fldItemTemplates);
                    pContext.il.Emit(OpCodes.Ldstr, iTempId[0]);//load key
                    pContext.il.Emit(OpCodes.Callvirt, CompilerServices.miGetITempFromDic);
                    pContext.il.Emit(OpCodes.Ldloc_0);//load root of treeView
                    pContext.il.Emit(OpCodes.Callvirt, CompilerServices.miCreateExpDel);
                }
            }
		}

        /// <summary>
        /// Emit 
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="pFragment"></param>
		private void EmitGOLoad(Context pContext, XElement pFragment)
        {
            bool lAnyAttributes = pFragment.HasAttributes;

            #region Styling and default values loading

            if ( lAnyAttributes )
            {
                XAttribute lXStyle = pFragment.Attribute("Style");
                if ( lXStyle != null )
                {
                    CompilerServices.EmitSetValue(pContext.il, CompilerServices.piStyle, lXStyle.Value );
                }
            }

            pContext.il.Emit(OpCodes.Ldloc_0);
            pContext.il.Emit(OpCodes.Callvirt, CompilerServices.miLoadDefaultVals);

            #endregion Styling and default values loading

            #region Attributes reading

            if ( lAnyAttributes )
            {
                foreach( XAttribute lXAttribute in pFragment.Attributes())
                {
                    string lAttrName  = lXAttribute.Name.LocalName;

                    if ( lAttrName == "Style" )
                    {
                        continue;
                    }

                    string lAttrValue = lXAttribute.Value;
                    MemberInfo mi = pContext.CurrentNodeType.GetMember( lAttrName ).FirstOrDefault();
                    if (mi == null)
                    {
                        throw new Exception("Member '" + lXAttribute.Name + "' not found in " + pContext.CurrentNodeType.Name);
                    }

                    if (mi.MemberType == MemberTypes.Event)
                    {
                        foreach ( string exp in CompilerServices.splitOnSemiColumnOutsideAccolades( lAttrValue ) )
                        {
                            string trimed = exp.Trim();
                            if (trimed.StartsWith("{", StringComparison.OrdinalIgnoreCase))
                            {
                                compileAndStoreDynHandler(pContext, mi as EventInfo, trimed.Substring(1, trimed.Length - 2));
                            }
                            else
                            {
                                emitHandlerBinding(pContext, mi as EventInfo, trimed);
                            }
                        }

                        continue;
                    }

                    PropertyInfo pi = mi as PropertyInfo;
                    if (pi == null)
                    {
                        throw new Exception("Member '" + lXAttribute.Name + "' is not a property in " + pContext.CurrentNodeType.Name);
                    }

                    if (pi.Name == "Name")
                    {
                        pContext.StoreCurrentName( lAttrValue );
                    }

                    if ( lAttrValue.StartsWith("{", StringComparison.OrdinalIgnoreCase) )
                    {
                        readPropertyBinding( pContext, lAttrName, lAttrValue.Substring(1, lAttrValue.Length - 2) );
                    }
                    else
                    {
                        CompilerServices.EmitSetValue( pContext.il, pi, lAttrValue );
                    }
                }
            }

            #endregion

            this.ReadChildren( pFragment, pContext );

            pContext.nodesStack.ResetCurrentNodeIndex();
		}

		/// <summary>
		/// Parse child node an generate corresponding msil
		/// </summary>
		private void ReadChildren (XElement pContainer, Context pContext, int startingIdx = 0)
		{
			int nodeIdx = startingIdx;
            foreach( XElement lChild in pContainer.Elements() )
            {
                string lChildName = lChild.Name.LocalName;

                // Skip Templates
                if ( lChildName == "Template" ||
                     lChildName == "ItemTemplate")
                {
                    continue;
                }

                //push 2x current instance on stack for parenting and reseting loc0 to parent
                //loc_0 will be used for child
                pContext.il.Emit(OpCodes.Ldloc_0);
                pContext.il.Emit(OpCodes.Ldloc_0);

                Type t = Type.GetType("Crow." + lChildName);
                if (t == null)
                {
                    Assembly a = Assembly.GetEntryAssembly();
                    foreach (Type expT in a.GetExportedTypes())
                    {
                        if (expT.Name == lChildName)
                        {
                            t = expT;
                            break;
                        }
                    }
                }

                if (t == null)
                {
                    throw new Exception(lChildName + " type not found");
                }

                pContext.il.Emit(OpCodes.Newobj, t.GetConstructors()[0]);//TODO:search parameterless ctor
                pContext.il.Emit(OpCodes.Stloc_0);//child is now loc_0
                CompilerServices.emitSetCurInterface(pContext.il);

                pContext.nodesStack.Push(new Node(t, nodeIdx));
                this.EmitLoader( lChild, pContext );
                pContext.nodesStack.Pop();

                pContext.il.Emit(OpCodes.Ldloc_0);//load child on stack for parenting
                pContext.il.Emit(OpCodes.Callvirt, pContext.nodesStack.Peek().GetAddMethod(nodeIdx));
                pContext.il.Emit(OpCodes.Stloc_0); //reset local to current go

                nodeIdx++;
            }
		}

		#endregion

        /// <summary>
        /// Read a property binding.
        /// </summary>
        /// <param name="pContext"></param>
        /// <param name="sourceMember"></param>
        /// <param name="expression"></param>
		void readPropertyBinding (Context pContext, string sourceMember, string expression)
		{
			NodeAddress sourceNA = pContext.CurrentNodeAddress;
			BindingDefinition bindingDef = sourceNA.GetBindingDef (sourceMember, expression);

#if DEBUG_BINDING
			Debug.WriteLine("Property Binding: " + bindingDef.ToString());
#endif

            if (bindingDef.IsDataSourceBinding)//bind on data source
            {
                emitDataSourceBindings(pContext, bindingDef);
            }
            else
            {
                pContext.StorePropertyBinding(bindingDef);
            }
		}

		#region Emit Helper
		void dataSourceChangedEmitHelper(object dscSource, object dataSource, int dynMethIdx){
			if (dataSource is IValueChange)
				(dataSource as IValueChange).ValueChanged +=
					(EventHandler<ValueChangeEventArgs>)dsValueChangedDynMeths [dynMethIdx].CreateDelegate (typeof(EventHandler<ValueChangeEventArgs>), dscSource);
		}
		#endregion

		#region Event Bindings
		/// <summary>
		/// Compile events expression in IML attributes, and store the result in the instanciator
		/// Those handlers will be bound when instatiing
		/// </summary>
		void compileAndStoreDynHandler (Context pContext, EventInfo sourceEvent, string expression)
		{
			//store event handler dynamic method in instanciator
			int dmIdx = cachedDelegates.Count;
			cachedDelegates.Add (CompilerServices.compileDynEventHandler (sourceEvent, expression, pContext.CurrentNodeAddress));
			pContext.emitCachedDelegateHandlerAddition(dmIdx, sourceEvent);
		}
		/// <summary> Emits handler method bindings </summary>
		void emitHandlerBinding (Context pContext, EventInfo sourceEvent, string expression){
			NodeAddress currentNode = pContext.CurrentNodeAddress;
			BindingDefinition bindingDef = currentNode.GetBindingDef (sourceEvent.Name, expression);

			#if DEBUG_BINDING
			Debug.WriteLine("Event Binding: " + bindingDef.ToString());
			#endif

			if (bindingDef.IsTemplateBinding | bindingDef.IsDataSourceBinding) {
				//we need to bind datasource method to source event
				DynamicMethod dm = new DynamicMethod ("dyn_dsORtmpChangedForHandler" + NewId,
					                   typeof(void),
					                   CompilerServices.argsBoundDSChange, true);

				ILGenerator il = dm.GetILGenerator (256);
				System.Reflection.Emit.Label cancel = il.DefineLabel ();

				il.DeclareLocal (typeof(MethodInfo));//used to cancel binding if method doesn't exist

				il.Emit (OpCodes.Nop);

				emitRemoveOldDataSourceHandler (il, sourceEvent.Name, bindingDef.TargetMember, false);

				//fetch method in datasource and test if it exist
				il.Emit (OpCodes.Ldarg_2);//load new datasource
				il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
				il.Emit (OpCodes.Brfalse, cancel);//cancel if new datasource is null
				il.Emit (OpCodes.Ldarg_2);//load new datasource
				il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
				il.Emit (OpCodes.Ldstr, bindingDef.TargetMember);//load handler method name
				il.Emit (OpCodes.Call, CompilerServices.miGetMethInfoWithRefx);
				il.Emit (OpCodes.Stloc_0);//save MethodInfo
				il.Emit (OpCodes.Ldloc_0);//push mi for test if null

				il.Emit (OpCodes.Brfalse, cancel);

				il.Emit (OpCodes.Ldarg_1);//load datasource change source where the event is as 1st arg of handler.add
				if (bindingDef.IsTemplateBinding)//fetch source instance with address
					CompilerServices.emitGetInstance (il, bindingDef.SourceNA);

				//load handlerType of sourceEvent to create delegate (1st arg)
				il.Emit (OpCodes.Ldtoken, sourceEvent.EventHandlerType);
				il.Emit (OpCodes.Call, CompilerServices.miGetTypeFromHandle);
				il.Emit (OpCodes.Ldarg_2);//load new datasource where the method is defined
				il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
				il.Emit (OpCodes.Ldloc_0);//load methodInfo (3rd arg)

				il.Emit (OpCodes.Callvirt, CompilerServices.miCreateBoundDel);
				il.Emit (OpCodes.Callvirt, sourceEvent.AddMethod);//call add event

				System.Reflection.Emit.Label finish = il.DefineLabel ();
				il.Emit (OpCodes.Br, finish);
				il.MarkLabel (cancel);
				il.EmitWriteLine (string.Format ("Handler method '{0}' for '{1}' not found in new dataSource ", bindingDef.TargetMember, sourceEvent.Name));
				il.MarkLabel (finish);
				il.Emit (OpCodes.Ret);

				//store dschange delegate in instatiator instance for access while instancing graphic object
				int delDSIndex = cachedDelegates.Count;
				cachedDelegates.Add (dm.CreateDelegate (CompilerServices.ehTypeDSChange, this));

				if (bindingDef.IsDataSourceBinding)
					pContext.emitCachedDelegateHandlerAddition (delDSIndex, CompilerServices.eiDSChange);
				else //template handler binding, will be added to root parentChanged
					templateCachedDelegateIndices.Add (delDSIndex);
			} else {//normal in tree handler binding, store until tree is complete (end of parse)
				pContext.UnresolvedTargets.Add (new EventBinding (
					bindingDef.SourceNA, sourceEvent,
					bindingDef.TargetNA, bindingDef.TargetMember, bindingDef.TargetName));
			}
		}
		#endregion

		#region Property Bindings
		/// <summary>
		/// Create and store in the instanciator the ValueChanged delegates
		/// those delegates uses grtree functions to set destination value so they don't
		/// need to be bound to destination instance as in the ancient system.
		/// </summary>
		void emitBindingDelegates(Context pContext){
			foreach (KeyValuePair<NodeAddress,Dictionary<string, List<MemberAddress>>> bindings in pContext.Bindings ) {
				if (bindings.Key.Count == 0)//template binding
					emitTemplateBindings (pContext, bindings.Value);
				else
					emitPropertyBindings (pContext,  bindings.Key, bindings.Value);
			}
		}
		void emitPropertyBindings(Context pContext, NodeAddress origine, Dictionary<string, List<MemberAddress>> bindings){
			Type origineNodeType = origine.NodeType;

			//value changed dyn method
			DynamicMethod dm = new DynamicMethod ("dyn_valueChanged" + NewId,
				typeof (void), CompilerServices.argsValueChange, true);
			ILGenerator il = dm.GetILGenerator (256);

			System.Reflection.Emit.Label endMethod = il.DefineLabel ();

			il.DeclareLocal (CompilerServices.TObject);

			il.Emit (OpCodes.Nop);

			int i = 0;
			foreach (KeyValuePair<string, List<MemberAddress>> bindingCase in bindings ) {

				System.Reflection.Emit.Label nextTest = il.DefineLabel ();

				#region member name test
				//load source member name
				il.Emit (OpCodes.Ldarg_1);
				il.Emit (OpCodes.Ldfld, CompilerServices.fiVCMbName);

				il.Emit (OpCodes.Ldstr, bindingCase.Key);//load name to test
				il.Emit (OpCodes.Ldc_I4_4);//StringComparison.Ordinal
				il.Emit (OpCodes.Callvirt, CompilerServices.stringEquals);
				il.Emit (OpCodes.Brfalse, nextTest);//if not equal, jump to next case
				#endregion

				#region destination member affectations
				PropertyInfo piOrig = origineNodeType.GetProperty (bindingCase.Key);
				Type origineType = null;
				if (piOrig != null)
					origineType = piOrig.PropertyType;
				foreach (MemberAddress ma in bindingCase.Value) {
					//first we have to load destination instance onto the stack, it is access
					//with graphic tree functions deducted from nodes topology
					il.Emit (OpCodes.Ldarg_0);//load source instance of ValueChanged event

					NodeAddress destination = ma.Address;

					if (destination.Count == 0){//template reverse binding
						//fetch destination instance (which is the template root)
						for (int j = 0; j < origine.Count ; j++)
							il.Emit(OpCodes.Callvirt, CompilerServices.miGetLogicalParent);
					}else
						CompilerServices.emitGetInstance (il, origine, destination);

					if (origineType != null && destination.Count > 0){//else, prop less binding or reverse template bind, no init requiered
						//for initialisation dynmeth, push destination instance loc_0 is root node in pContext
						pContext.il.Emit(OpCodes.Ldloc_0);
						CompilerServices.emitGetInstance (pContext.il, destination);

						//init dynmeth: load actual value from origine
						pContext.il.Emit (OpCodes.Ldloc_0);
						CompilerServices.emitGetInstance (pContext.il, origine);
						pContext.il.Emit (OpCodes.Callvirt, origineNodeType.GetProperty (bindingCase.Key).GetGetMethod());
					}
					//load new value
					il.Emit (OpCodes.Ldarg_1);
					il.Emit (OpCodes.Ldfld, CompilerServices.fiVCNewValue);

					if (origineType == null)//property less binding, no init
						CompilerServices.emitConvert (il, ma.Property.PropertyType);
					else if (destination.Count > 0) {
						if (origineType.IsValueType)
							pContext.il.Emit(OpCodes.Box, origineType);

						CompilerServices.emitConvert (pContext.il, origineType, ma.Property.PropertyType);
						CompilerServices.emitConvert (il, origineType, ma.Property.PropertyType);

						pContext.il.Emit (OpCodes.Callvirt, ma.Property.GetSetMethod());//set init value
					} else {// reverse templateBinding
						il.Emit (OpCodes.Ldstr, ma.memberName);//arg 3 of setValueWithReflexion
						il.Emit (OpCodes.Call, CompilerServices.miSetValWithRefx);
						continue;
					}
					il.Emit (OpCodes.Callvirt, ma.Property.GetSetMethod());//set value on value changes
				}
				#endregion
				il.Emit (OpCodes.Br, endMethod);
				il.MarkLabel (nextTest);

				i++;
			}

			il.MarkLabel (endMethod);
			il.Emit (OpCodes.Ret);

			//store and emit Add in pContext
			int dmIdx = cachedDelegates.Count;
			cachedDelegates.Add (dm.CreateDelegate (typeof(EventHandler<ValueChangeEventArgs>)));
			pContext.emitCachedDelegateHandlerAddition (dmIdx, CompilerServices.eiValueChange, origine);
		}
		void emitTemplateBindings(Context pContext, Dictionary<string, List<MemberAddress>> bindings){
			//value changed dyn method
			DynamicMethod dm = new DynamicMethod ("dyn_tmpValueChanged",
				typeof (void), CompilerServices.argsValueChange, true);
			ILGenerator il = dm.GetILGenerator (256);

			//create parentchanged dyn meth in parallel to have only one loop over bindings
			DynamicMethod dmPC = new DynamicMethod ("dyn_InitAndLogicalParentChanged",
				typeof (void),
				CompilerServices.argsBoundDSChange, true);
			ILGenerator ilPC = dmPC.GetILGenerator (256);

			il.Emit (OpCodes.Nop);
			ilPC.Emit (OpCodes.Nop);

			System.Reflection.Emit.Label endMethod = il.DefineLabel ();

			il.DeclareLocal (CompilerServices.TObject);
			ilPC.DeclareLocal (CompilerServices.TObject);//used for checking propery less bindings
			ilPC.DeclareLocal (typeof(MemberInfo));//used for checking propery less bindings

			System.Reflection.Emit.Label cancel = ilPC.DefineLabel ();

			#region Unregister previous parent event handler
			//unregister previous parent handler if not null
			ilPC.Emit (OpCodes.Ldarg_2);//load old parent
			ilPC.Emit (OpCodes.Ldfld, CompilerServices.fiDSCOldDS);
			ilPC.Emit (OpCodes.Brfalse, cancel);//old parent is null

			ilPC.Emit (OpCodes.Ldarg_2);//load old parent
			ilPC.Emit (OpCodes.Ldfld, CompilerServices.fiDSCOldDS);
			//Load cached delegate
			ilPC.Emit(OpCodes.Ldarg_0);//load ref to this instanciator onto the stack
			ilPC.Emit(OpCodes.Ldfld, CompilerServices.fiTemplateBinding);

			//add template bindings dynValueChanged delegate to new parent event
			ilPC.Emit(OpCodes.Callvirt, CompilerServices.eiValueChange.RemoveMethod);//call remove event
			#endregion

			ilPC.MarkLabel(cancel);

			#region check if new parent is null
			cancel = ilPC.DefineLabel ();
			ilPC.Emit (OpCodes.Ldarg_2);//load datasource change arg
			ilPC.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
			ilPC.Emit (OpCodes.Brfalse, cancel);//new ds is null
			#endregion

			int i = 0;
			foreach (KeyValuePair<string, List<MemberAddress>> bindingCase in bindings ) {

				System.Reflection.Emit.Label nextTest = il.DefineLabel ();

				#region member name test
				//load source member name
				il.Emit (OpCodes.Ldarg_1);
				il.Emit (OpCodes.Ldfld, CompilerServices.fiVCMbName);

				il.Emit (OpCodes.Ldstr, bindingCase.Key);//load name to test
				il.Emit (OpCodes.Ldc_I4_4);//StringComparison.Ordinal
				il.Emit (OpCodes.Callvirt, CompilerServices.stringEquals);
				il.Emit (OpCodes.Brfalse, nextTest);//if not equal, jump to next case
				#endregion

				#region destination member affectations

				foreach (MemberAddress ma in bindingCase.Value) {
					if (ma.Address.Count == 0){
						Debug.WriteLine("\t\tBUG: reverse template binding in normal template binding");
						continue;//template binding
					}
					//first we try to get memberInfo of new parent, if it doesn't exist, it's a propery less binding
					ilPC.Emit (OpCodes.Ldarg_2);//load new parent onto the stack for handler addition
					ilPC.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
					ilPC.Emit (OpCodes.Stloc_0);//save new parent
					//get parent type
					ilPC.Emit (OpCodes.Ldloc_0);//push parent instance
					ilPC.Emit (OpCodes.Ldstr, bindingCase.Key);//load member name
					ilPC.Emit (OpCodes.Call, CompilerServices.miGetMembIinfoWithRefx);
					ilPC.Emit (OpCodes.Stloc_1);//save memberInfo
					ilPC.Emit (OpCodes.Ldloc_1);//push mi for test if null
					System.Reflection.Emit.Label propLessReturn = ilPC.DefineLabel ();
					ilPC.Emit (OpCodes.Brfalse, propLessReturn);


					//first we have to load destination instance onto the stack, it is access
					//with graphic tree functions deducted from nodes topology
					il.Emit (OpCodes.Ldarg_0);//load source instance of ValueChanged event
					CompilerServices.emitGetChild (il, typeof(TemplatedControl), -1);
					CompilerServices.emitGetInstance (il, ma.Address);

					ilPC.Emit (OpCodes.Ldarg_2);//load destination instance to set actual value of member
					ilPC.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
					CompilerServices.emitGetChild (ilPC, typeof(TemplatedControl), -1);
					CompilerServices.emitGetInstance (ilPC, ma.Address);

					//load new value
					il.Emit (OpCodes.Ldarg_1);
					il.Emit (OpCodes.Ldfld, CompilerServices.fiVCNewValue);

					//for the parent changed dyn meth we need to fetch actual value for initialisation thrue reflexion
					ilPC.Emit (OpCodes.Ldloc_0);//push root instance of instanciator as parentChanged source
					ilPC.Emit (OpCodes.Ldloc_1);//push mi for value fetching
					ilPC.Emit (OpCodes.Call, CompilerServices.miGetValWithRefx);

					CompilerServices.emitConvert (il, ma.Property.PropertyType);
					CompilerServices.emitConvert (ilPC, ma.Property.PropertyType);

					il.Emit (OpCodes.Callvirt, ma.Property.GetSetMethod());
					ilPC.Emit (OpCodes.Callvirt, ma.Property.GetSetMethod());

					ilPC.MarkLabel(propLessReturn);
				}
				#endregion
				il.Emit (OpCodes.Br, endMethod);
				il.MarkLabel (nextTest);

				i++;
			}
			//il.Emit (OpCodes.Pop);
			il.MarkLabel (endMethod);
			il.Emit (OpCodes.Ret);

			//store template bindings in instanciator
			templateBinding = dm.CreateDelegate (typeof(EventHandler<ValueChangeEventArgs>));

			#region emit LogicalParentChanged method

			//load new parent onto the stack for handler addition
			ilPC.Emit (OpCodes.Ldarg_2);
			ilPC.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);

			//Load cached delegate
			ilPC.Emit(OpCodes.Ldarg_0);//load ref to this instanciator onto the stack
			ilPC.Emit(OpCodes.Ldfld, CompilerServices.fiTemplateBinding);

			//add template bindings dynValueChanged delegate to new parent event
			ilPC.Emit(OpCodes.Callvirt, CompilerServices.eiValueChange.AddMethod);//call add event

			ilPC.MarkLabel (cancel);
			ilPC.Emit (OpCodes.Ret);

			//store dschange delegate in instatiator instance for access while instancing graphic object
			int delDSIndex = cachedDelegates.Count;
			cachedDelegates.Add(dmPC.CreateDelegate (CompilerServices.ehTypeDSChange, this));
			#endregion

			pContext.emitCachedDelegateHandlerAddition(delDSIndex, CompilerServices.eiLogicalParentChanged);
		}
		/// <summary>
		/// create the valuechanged handler, the datasourcechanged handler and emit event handling
		/// </summary>
		void emitDataSourceBindings(Context pContext, BindingDefinition bindingDef){
			DynamicMethod dm = null;
			ILGenerator il = null;
			int dmVC = 0;
			PropertyInfo piSource = pContext.CurrentNodeType.GetProperty(bindingDef.SourceMember);
			//if no dataSource member name is provided, valuechange is not handle and datasource change
			//will be used as origine value
			string delName = "dyn_DSvalueChanged" + NewId;
			if (!string.IsNullOrEmpty(bindingDef.TargetMember)){
				#region create valuechanged method
				dm = new DynamicMethod (delName,
					typeof (void),
					CompilerServices.argsBoundValueChange, true);

				il = dm.GetILGenerator (256);

				System.Reflection.Emit.Label endMethod = il.DefineLabel ();

				il.DeclareLocal (CompilerServices.TObject);

				il.Emit (OpCodes.Nop);

				//load value changed member name onto the stack
				il.Emit (OpCodes.Ldarg_2);
				il.Emit (OpCodes.Ldfld, CompilerServices.fiVCMbName);

				//test if it's the expected one
				il.Emit (OpCodes.Ldstr, bindingDef.TargetMember);
				il.Emit (OpCodes.Ldc_I4_4);//StringComparison.Ordinal
				il.Emit (OpCodes.Callvirt, CompilerServices.stringEquals);
				il.Emit (OpCodes.Brfalse, endMethod);
				//set destination member with valueChanged new value
				//load destination ref
				il.Emit (OpCodes.Ldarg_0);
				//load new value onto the stack
				il.Emit (OpCodes.Ldarg_2);
				il.Emit (OpCodes.Ldfld, CompilerServices.fiVCNewValue);

				//by default, source value type is deducted from target member type to allow
				//memberless binding, if targetMember exists, it will be used to determine target
				//value type for conversion

				CompilerServices.emitConvert (il, piSource.PropertyType);

				il.Emit (OpCodes.Callvirt, piSource.GetSetMethod ());

				il.MarkLabel (endMethod);
				il.Emit (OpCodes.Ret);

				//vc dyn meth is stored in a cached list, it will be bound to datasource only
				//when datasource of source graphic object changed
				dmVC = dsValueChangedDynMeths.Count;
				dsValueChangedDynMeths.Add (dm);
				#endregion
			}

			#region emit dataSourceChanged event handler
			//now we create the datasource changed method that will init the destination member with
			//the actual value of the origin member of the datasource and then will bind the value changed
			//dyn methode.
			//dm is bound to the instanciator instance to have access to cached dyn meth and delegates
			dm = new DynamicMethod ("dyn_dschanged" + NewId,
				typeof (void),
				CompilerServices.argsBoundDSChange, true);

			il = dm.GetILGenerator (256);

			il.DeclareLocal (CompilerServices.TObject);//used for checking propery less bindings
			il.DeclareLocal (typeof(MemberInfo));//used for checking propery less bindings
			System.Reflection.Emit.Label cancel = il.DefineLabel ();
			System.Reflection.Emit.Label cancelInit = il.DefineLabel ();

			il.Emit (OpCodes.Nop);

			emitRemoveOldDataSourceHandler(il, "ValueChanged", delName,false);

			if (!string.IsNullOrEmpty(bindingDef.TargetMember)){
				if (bindingDef.TwoWay){
					System.Reflection.Emit.Label cancelRemove = il.DefineLabel ();
					//remove handler if not null
					il.Emit (OpCodes.Ldarg_2);//load old parent
					il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCOldDS);
					il.Emit (OpCodes.Brfalse, cancelRemove);//old parent is null

					//remove handler
					il.Emit (OpCodes.Ldarg_1);//3d arg: instance bound to delegate (the source)
					il.Emit (OpCodes.Ldstr, "ValueChanged");//2nd arg event name
					il.Emit (OpCodes.Ldarg_2);//1st arg load old datasource
					il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCOldDS);
					il.Emit (OpCodes.Call, CompilerServices.miRemEvtHdlByTarget);
					il.MarkLabel(cancelRemove);
				}
				il.Emit (OpCodes.Ldarg_2);//load datasource change arg
				il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
				il.Emit (OpCodes.Brfalse, cancel);//new ds is null
			}

			#region fetch initial Value
			if (!string.IsNullOrEmpty(bindingDef.TargetMember)){
				il.Emit (OpCodes.Ldarg_2);//load new datasource
				il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
				il.Emit (OpCodes.Ldstr, bindingDef.TargetMember);//load member name
				il.Emit (OpCodes.Call, CompilerServices.miGetMembIinfoWithRefx);
				il.Emit (OpCodes.Stloc_1);//save memberInfo
				il.Emit (OpCodes.Ldloc_1);//push mi for test if null
				il.Emit (OpCodes.Brfalse, cancelInit);//propertyLessBinding
			}

			il.Emit (OpCodes.Ldarg_1);//load source of dataSourceChanged which is the dest instance
			il.Emit (OpCodes.Ldarg_2);//load new datasource
			il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
			if (!string.IsNullOrEmpty(bindingDef.TargetMember)){
				il.Emit (OpCodes.Ldloc_1);//push mi for value fetching
				il.Emit (OpCodes.Call, CompilerServices.miGetValWithRefx);
			}
			CompilerServices.emitConvert (il, piSource.PropertyType);
			il.Emit (OpCodes.Callvirt, piSource.GetSetMethod ());
			#endregion

			if (!string.IsNullOrEmpty(bindingDef.TargetMember)){
				il.MarkLabel(cancelInit);
				//check if new dataSource implement IValueChange
				il.Emit (OpCodes.Ldarg_2);//load new datasource
				il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
				il.Emit (OpCodes.Isinst, typeof(IValueChange));
				il.Emit (OpCodes.Brfalse, cancel);

				il.Emit(OpCodes.Ldarg_0);//load ref to this instanciator onto the stack
				il.Emit (OpCodes.Ldarg_1);//load datasource change source
				il.Emit (OpCodes.Ldarg_2);//load new datasource
				il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
				il.Emit(OpCodes.Ldc_I4, dmVC);//load index of dynmathod
				il.Emit (OpCodes.Call, CompilerServices.miDSChangeEmitHelper);

				il.MarkLabel (cancel);

				if (bindingDef.TwoWay){
					il.Emit (OpCodes.Ldarg_1);//arg1: dataSourceChange source, the origine of the binding
					il.Emit (OpCodes.Ldstr, bindingDef.SourceMember);//arg2: orig member
					il.Emit (OpCodes.Ldarg_2);//arg3: new datasource
					il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCNewDS);
					il.Emit (OpCodes.Ldstr, bindingDef.TargetMember);//arg4: dest member
					il.Emit (OpCodes.Call, CompilerServices.miDSReverseBinding);
				}

			}
			il.Emit (OpCodes.Ret);

			//store dschange delegate in instatiator instance for access while instancing graphic object
			int delDSIndex = cachedDelegates.Count;
			cachedDelegates.Add(dm.CreateDelegate (CompilerServices.ehTypeDSChange, this));
			#endregion

			pContext.emitCachedDelegateHandlerAddition(delDSIndex, CompilerServices.eiDSChange);
		}
		/// <summary>
		/// Two way binding for datasource, graphicObj=>dataSource link, datasource value has priority
		/// and will be set as init for source property (in emitDataSourceBindings func)
		/// </summary>
		/// <param name="orig">Graphic object instance, source of binding</param>
		/// <param name="origMember">Origin member name</param>
		/// <param name="dest">datasource instance, target of the binding</param>
		/// <param name="destMember">Destination member name</param>
		static void dataSourceReverseBinding(IValueChange orig, string origMember, object dest, string destMember){
			Type tOrig = orig.GetType ();
			Type tDest = dest.GetType ();
			PropertyInfo piOrig = tOrig.GetProperty (origMember);
			PropertyInfo piDest = tDest.GetProperty (destMember);

			if (piDest == null) {
				Debug.WriteLine ("Member '{0}' not found in new DataSource '{1}' of '{2}'", destMember, dest, orig);
				return;
			}
			#if DEBUG_BINDING
			Debug.WriteLine ("DS Reverse binding: Member '{0}' found in new DS '{1}' of '{2}'", destMember, dest, orig);
			#endif

			#region ValueChanged emit
			DynamicMethod dm = new DynamicMethod ("dyn_valueChanged" + NewId,
				typeof (void), CompilerServices.argsBoundValueChange, true);
			ILGenerator il = dm.GetILGenerator (256);

			System.Reflection.Emit.Label endMethod = il.DefineLabel ();

			il.DeclareLocal (CompilerServices.TObject);
			il.Emit (OpCodes.Nop);

			//load value changed member name onto the stack
			il.Emit (OpCodes.Ldarg_2);
			il.Emit (OpCodes.Ldfld, CompilerServices.fiVCMbName);

			//test if it's the expected one
			il.Emit (OpCodes.Ldstr, origMember);
			il.Emit (OpCodes.Ldc_I4_4);//StringComparison.Ordinal
			il.Emit (OpCodes.Callvirt, CompilerServices.stringEquals);
			il.Emit (OpCodes.Brfalse, endMethod);
			//set destination member with valueChanged new value
			//load destination ref
			il.Emit (OpCodes.Ldarg_0);
			//load new value onto the stack
			il.Emit (OpCodes.Ldarg_2);
			il.Emit (OpCodes.Ldfld, CompilerServices.fiVCNewValue);

			CompilerServices.emitConvert (il, piOrig.PropertyType, piDest.PropertyType);

			il.Emit (OpCodes.Callvirt, piDest.GetSetMethod ());

			il.MarkLabel (endMethod);
			il.Emit (OpCodes.Ret);
			#endregion

			orig.ValueChanged += (EventHandler<ValueChangeEventArgs>)dm.CreateDelegate (typeof(EventHandler<ValueChangeEventArgs>), dest);
		}
        #endregion

        /// <summary>
        /// Emits remove old data source event handler.
        /// </summary>
        /// <param name="il"></param>
        /// <param name="eventName"></param>
        /// <param name="delegateName"></param>
        /// <param name="DSSide"></param>
        private void emitRemoveOldDataSourceHandler(ILGenerator il, string eventName, string delegateName, bool DSSide = true)
        {
			System.Reflection.Emit.Label cancel = il.DefineLabel ();

			il.Emit (OpCodes.Ldarg_2);//load old parent
			il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCOldDS);
			il.Emit (OpCodes.Brfalse, cancel);//old parent is null

			//remove handler
			if (DSSide)
            {
                //event is defined in the dataSource instance
				il.Emit (OpCodes.Ldarg_2);//1st arg load old datasource
				il.Emit (OpCodes.Ldfld, CompilerServices.fiDSCOldDS);
			}
            else//the event is in the source
				il.Emit (OpCodes.Ldarg_1);//1st arg load old datasource
			il.Emit (OpCodes.Ldstr, eventName);//2nd arg event name
			il.Emit (OpCodes.Ldstr, delegateName);//3d arg: delegate name
			il.Emit (OpCodes.Call, CompilerServices.miRemEvtHdlByName);
			il.MarkLabel(cancel);
		}
	}
}

