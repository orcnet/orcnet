﻿using System;
using System.Diagnostics;

namespace OrcNet.UI
{
	public class PerformanceMeasure : IValueChange {
		#region IValueChange implementation
		public event EventHandler<ValueChangeEventArgs> ValueChanged;
		public virtual void NotifyValueChanged(string MemberName, object _value)
		{
			if (ValueChanged != null)				
				ValueChanged.Invoke(this, new ValueChangeEventArgs(MemberName, _value));
		}
		#endregion

		public Stopwatch timer = new Stopwatch ();
		public string Name;
		public long current, minimum, maximum, total, cptMeasures;
		public long cancelLimit;

		public PerformanceMeasure(string name = "unamed", long _cancelLimit = 0){
			Name = name;
			cancelLimit = _cancelLimit;
			ResetStats ();
		}

		public void StartCycle(){
			timer.Restart();
		}
		public void StopCycle(){
			timer.Stop();
			computeStats ();
		}
		public void NotifyChanges(){
			if (cptMeasures == 0)
				return;
			NotifyValueChanged("minimum", minimum);
			NotifyValueChanged("maximum", maximum);
			NotifyValueChanged("current", current);
			//			NotifyValueChanged("total", total);
			//			NotifyValueChanged("cptMeasures", cptMeasures);
			NotifyValueChanged("mean", total / cptMeasures);
		}

		void computeStats(){			
			current = timer.ElapsedTicks;
			if (current < cancelLimit)
				return;
			cptMeasures++;
			total += timer.ElapsedTicks;
			if (timer.ElapsedTicks < minimum)
				minimum = timer.ElapsedTicks;
			if (timer.ElapsedTicks > maximum)
				maximum = timer.ElapsedTicks;			
		}
		void ResetStats(){
			Debug.WriteLine("reset measure cpt:{0}",cptMeasures);
			cptMeasures = total = current = maximum = 0;
			minimum = long.MaxValue;
		}
		void onResetClick(object sender, MouseButtonEventArgs e){
			ResetStats();
		}
	}
}

