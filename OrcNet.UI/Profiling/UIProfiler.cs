﻿using OrcNet.Core.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrcNet.UI.Profiling
{
    /// <summary>
    /// Definition of the <see cref="UIProfiler"/> class.
    /// </summary>
    public class UIProfiler : AProfiler, IUIProfiler
    {

    }
}
