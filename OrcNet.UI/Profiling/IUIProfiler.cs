﻿using OrcNet.Core;

namespace OrcNet.UI.Profiling
{
    /// <summary>
    /// Definition of the <see cref="IUIProfiler"/> interface.
    /// </summary>
    public interface IUIProfiler : IService
    {

    }
}
