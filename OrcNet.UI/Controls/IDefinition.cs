﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="IDefinition"/> interface.
    /// </summary>
    public interface IDefinition
    {
        #region Properties

        /// <summary>
        /// Gets the definition index.
        /// </summary>
        int Index
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the user size
        /// </summary>
        GridLength UserSize
        {
            get;
        }

        /// <summary>
        /// Gets the user min size.
        /// </summary>
        double UserMinSize
        {
            get;
        }

        /// <summary>
        /// Gets the user max size.
        /// </summary>
        double UserMaxSize
        {
            get;
        }
        
        #endregion Properties
    }
}
