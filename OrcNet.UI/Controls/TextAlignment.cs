﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TextAlignment"/> enumeration.
    /// </summary>
    public enum TextAlignment
    {
        /// <summary>
        /// Center the text.
        /// </summary>
        Center,

        /// <summary>
        /// Justify the text.
        /// </summary>
        Justify,

        /// <summary>
        /// Set the text to the left
        /// </summary>
        Left,

        /// <summary>
        /// Set the text to the right.
        /// </summary>
        Right
    }
}
