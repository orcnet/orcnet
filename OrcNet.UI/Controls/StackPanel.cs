﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="StackPanel"/> class.
    /// </summary>
	public class StackPanel : APanel
    {
        #region Fields

        /// <summary>
        /// Stores the current stretched element.
        /// </summary>
        private FrameworkElement mStretchedElement;

        /// <summary>
        /// Stores the stack orientation.
        /// </summary>
        private Orientation mOrientation;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the stack orientation.
        /// </summary>
        public Orientation Orientation
        {
            get
            {
                return this.mOrientation;
            }
            set
            {
                if ( this.mOrientation == value )
                {
                    return;
                }

                this.mOrientation = value;
                this.NotifyPropertyChanged();
                this.OnOrientationChanged();
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StackPanel"/> class.
        /// </summary>
        public StackPanel() : 
        base()
		{
            this.mOrientation = Orientation.Horizontal;
		}

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Delegate called on orientation changes.
        /// </summary>
        protected virtual void OnOrientationChanged()
        {

        }

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            Size lSize = base.MeasureOverride();

            switch ( this.Orientation )
            {
                case Orientation.Horizontal:
                    lSize.Width  = (int)(this.mContentSize.Width + this.Margin.Left + this.Margin.Right);
                    break;
                case Orientation.Vertical:
                    lSize.Height = (int)(this.mContentSize.Height + this.Margin.Top + this.Margin.Bottom);
                    break;
            }

            return lSize;
        }

        /// <summary>
        /// Applies a children layout constrain.
        /// </summary>
        /// <param name="pLayoutType">The layout constrain type.</param>
        protected override void ChildrenLayoutingConstraints(ref LayoutingType pLayoutType)
        {
            // Prevent child repositionning in the direction of the stacking
            if ( this.Orientation == Orientation.Horizontal )
            {
                pLayoutType &= ~LayoutingType.X;
            }
            else
            {
                pLayoutType &= ~LayoutingType.Y;
            }
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            if ( pLayoutType == LayoutingType.ArrangeChildren )
            {
                //allow 1 child to have size to 0 if stack has fixed or streched size policy,
                //this child will occupy remaining space
                //if stack size policy is Fit, no child may have stretch enabled
                //in the direction of stacking.
                this.ComputeChildrenPositions();
                
                return this.Slot;
            }

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Delegate called on child layout changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnChildLayoutChanges(object pSender, LayoutingEventArgs pEventArgs)
        {
            FrameworkElement lElement = pSender as FrameworkElement;
            switch ( pEventArgs.LayoutType )
            {
                case LayoutingType.Width:
                    if ( this.Orientation == Orientation.Horizontal )
                    {
                        if ( lElement.HorizontalAlignment == HorizontalAlignment.Stretch )
                        {
                            if ( this.mStretchedElement == null && 
                                 this.Width != cFitSizeValue )
                            {
                                this.mStretchedElement = lElement;
                            }
                            else if (this.mStretchedElement != lElement)
                            {
                                Rectangle lSlot = lElement.Slot;
                                lSlot.Width = 0;
                                lElement.Slot = lSlot;
                                lElement.Width = cFitSizeValue;
                                return;
                            }
                        }
                        else
                        {
                            this.mContentSize.Width += lElement.Slot.Width - lElement.LastSlots.Width;
                        }

                        if ( this.mStretchedElement != null )
                        {
                            int lNewWidth = (int)Math.Max( this.ClientRectangle.Width - this.mContentSize.Width /*- Spacing * (this.Children.Count - 1)*/,
                                                           this.mStretchedElement.MinWidth );
                            if ( this.mStretchedElement.MaxWidth > 0 )
                            {
                                lNewWidth = (int)Math.Min( lNewWidth, this.mStretchedElement.MaxWidth );
                            }

                            if ( lNewWidth != this.mStretchedElement.Slot.Width )
                            {
                                Rectangle lSlot = this.mStretchedElement.Slot;
                                lSlot.Width = lNewWidth;
                                this.mStretchedElement.Slot = lSlot;
                                this.mStretchedElement.IsDirty = true;
#if DEBUG_LAYOUTING
					Debug.WriteLine ("\tAdjusting Width of " + this.mStretchedElement.ToString());
#endif
                                this.mStretchedElement.LayoutUpdated -= this.OnChildLayoutChanges;
                                this.mStretchedElement.OnLayoutChanges( LayoutingType.Width );
                                this.mStretchedElement.LayoutUpdated += this.OnChildLayoutChanges;

                                Rectangle lLastSlot = this.mStretchedElement.LastSlots;
                                lLastSlot.Width = this.mStretchedElement.Slot.Width;
                                this.mStretchedElement.LastSlots = lLastSlot;
                            }
                        }

                        if ( this.Width == cFitSizeValue )
                        {
                            this.RegisterForLayouting(LayoutingType.Width);
                        }

                        this.RegisterForLayouting(LayoutingType.ArrangeChildren);
                        return;
                    }
                    break;
                case LayoutingType.Height:
                    if ( this.Orientation == Orientation.Vertical )
                    {
                        if ( lElement.VerticalAlignment == VerticalAlignment.Stretch )
                        {
                            if ( this.mStretchedElement == null && 
                                 this.Height != cFitSizeValue )
                            {
                                this.mStretchedElement = lElement;
                            }
                            else if (this.mStretchedElement != lElement)
                            {
                                Rectangle lSlot = lElement.Slot;
                                lSlot.Height = 0;
                                lElement.Slot = lSlot;
                                lElement.Height = cFitSizeValue;
                                return;
                            }
                        }
                        else
                        {
                            this.mContentSize.Height += lElement.Slot.Height - lElement.LastSlots.Height;
                        }

                        if ( this.mStretchedElement != null )
                        {
                            int lNewHeight = (int)Math.Max( this.ClientRectangle.Height - this.mContentSize.Height /*- Spacing * (this.Children.Count - 1)*/,
                                                            this.mStretchedElement.MinHeight );
                            if ( this.mStretchedElement.MaxHeight > 0 )
                            {
                                lNewHeight = (int)Math.Min( lNewHeight, this.mStretchedElement.MaxHeight );
                            }

                            if ( lNewHeight != this.mStretchedElement.Slot.Height )
                            {
                                Rectangle lSlot = this.mStretchedElement.Slot;
                                lSlot.Height = lNewHeight;
                                this.mStretchedElement.Slot = lSlot;
                                this.mStretchedElement.IsDirty = true;
#if DEBUG_LAYOUTING
					Debug.WriteLine ("\tAdjusting Height of " + this.mStretchedElement.ToString());
#endif
                                this.mStretchedElement.LayoutUpdated -= this.OnChildLayoutChanges;
                                this.mStretchedElement.OnLayoutChanges( LayoutingType.Height );
                                this.mStretchedElement.LayoutUpdated += this.OnChildLayoutChanges;

                                Rectangle lLastSlot = this.mStretchedElement.LastSlots;
                                lLastSlot.Height = this.mStretchedElement.Slot.Height;
                                this.mStretchedElement.LastSlots = lLastSlot;
                            }
                        }

                        if ( this.Height == cFitSizeValue )
                        {
                            this.RegisterForLayouting(LayoutingType.Height);
                        }

                        this.RegisterForLayouting(LayoutingType.ArrangeChildren);
                        return;
                    }
                    break;
            }

            base.OnChildLayoutChanges(pSender, pEventArgs);
        }

        /// <summary>
        /// Computes the children position.
        /// </summary>
        private void ComputeChildrenPositions()
        {
            int lPosition = 0;
            for ( int lCurr = 0; lCurr < this.Children.Count; lCurr++ )
            {
                UIElement lChild = this.Children[ lCurr ] as UIElement;
                if ( lChild == null ||
                     lChild.IsVisible == false )
                {
                    continue;
                }

                if ( this.Orientation == Orientation.Horizontal )
                {
                    Rectangle lSlot = lChild.Slot;
                    lSlot.X = lPosition;
                    lChild.Slot = lSlot;
                    lPosition += lChild.Slot.Width;
                }
                else
                {
                    Rectangle lSlot = lChild.Slot;
                    lSlot.Y = lPosition;
                    lChild.Slot = lSlot;
                    lPosition += lChild.Slot.Height;
                }
            }

            this.mIsDirty = true;
        }

        #endregion Methods
	}
}
