﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="CornerRadius"/> structure.
    /// </summary>
    public struct CornerRadius
    {
        #region Fields

        /// <summary>
        /// Stores the top left corner radius.
        /// </summary>
        private double mTopLeft;

        /// <summary>
        /// Stores the top right corner radius.
        /// </summary>
        private double mTopRight;

        /// <summary>
        /// Stores the bottom right corner radius.
        /// </summary>
        private double mBottomRight;

        /// <summary>
        /// Stores the bottom left corner radius.
        /// </summary>
        private double mBottomLeft;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the maximum radius of this corner radius.
        /// </summary>
        public double Max
        {
            get
            {
                return System.Math.Max( this.mTopLeft,
                                        System.Math.Max( this.mTopRight,
                                        System.Math.Max( this.mBottomRight, this.mBottomLeft ) ) );
            }
        }

        /// <summary>
        /// Gets or sets the top left corner radius.
        /// </summary>
        public double TopLeft
        {
            get
            {
                return this.mTopLeft;
            }
            set
            {
                this.mTopLeft = value;
            }
        }

        /// <summary>
        /// Gets or sets the top right corner radius.
        /// </summary>
        public double TopRight
        {
            get
            {
                return this.mTopRight;
            }
            set
            {
                this.mTopRight = value;
            }
        }

        /// <summary>
        /// Gets or sets the bottom right corner radius.
        /// </summary>
        public double BottomRight
        {
            get
            {
                return this.mBottomRight;
            }
            set
            {
                this.mBottomRight = value;
            }
        }

        /// <summary>
        /// Gets or sets the bottom left corner radius.
        /// </summary>
        public double BottomLeft
        {
            get
            {
                return this.mBottomLeft;
            }
            set
            {
                this.mBottomLeft = value;
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CornerRadius"/> class.
        /// </summary>
        /// <param name="pUniformRadius">The uniform radius.</param>
        public CornerRadius(double pUniformRadius) :
        this( pUniformRadius, pUniformRadius, pUniformRadius, pUniformRadius )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CornerRadius"/> class.
        /// </summary>
        /// <param name="pTopLeft">The top left corner radius.</param>
        /// <param name="pTopRight">The top right corner radius.</param>
        /// <param name="pBottomRight">The bottom right corner radius.</param>
        /// <param name="pBottomLeft">The bottom left corner radius.</param>
        public CornerRadius(double pTopLeft, double pTopRight, double pBottomRight, double pBottomLeft)
        {
            this.mTopLeft     = pTopLeft;
            this.mTopRight    = pTopRight;
            this.mBottomRight = pBottomRight;
            this.mBottomLeft  = pBottomLeft;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Checks whether two corner radius objects are different or not.
        /// </summary>
        /// <param name="pFirst">The first.</param>
        /// <param name="pSecond">The second.</param>
        /// <returns>True if different, false otherwise.</returns>
        public static bool operator !=(CornerRadius pFirst, CornerRadius pSecond)
        {
            return pFirst.Equals( pSecond ) == false;
        }

        /// <summary>
        /// Checks whether two corner radius objects are different or not.
        /// </summary>
        /// <param name="pFirst">The first.</param>
        /// <param name="pSecond">The second.</param>
        /// <returns>True if different, false otherwise.</returns>
        public static bool operator ==(CornerRadius pFirst, CornerRadius pSecond)
        {
            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Checks whether this corner radius is equal to another object.
        /// </summary>
        /// <param name="pOther">The other object.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            if ( pOther is CornerRadius == false )
            {
                return false;
            }

            return this.Equals( (CornerRadius)pOther );
        }

        /// <summary>
        /// Checks whether this corner radius is equal to another corner radius.
        /// </summary>
        /// <param name="pOther">The other corner radius.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(CornerRadius pOther)
        {
            return this.mTopLeft     == pOther.mTopLeft &&
                   this.mTopRight    == pOther.mTopRight &&
                   this.mBottomRight == pOther.mBottomRight &&
                   this.mBottomLeft  == pOther.mBottomLeft;
        }

        /// <summary>
        /// Gets the corner radius hashcode.
        /// </summary>
        /// <returns>The hashcode.</returns>
        public override int GetHashCode()
        {
            return this.mTopLeft.GetHashCode() ^ this.mTopRight.GetHashCode() ^ this.mBottomRight.GetHashCode() ^ this.mBottomLeft.GetHashCode();
        }

        #endregion Methods
    }
}
