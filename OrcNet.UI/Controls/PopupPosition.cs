﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Popup"/> enumeration.
    /// </summary>
    public enum PopupPosition
    {
        /// <summary>
        /// Top position
        /// </summary>
        Top = 0,

        /// <summary>
        /// Bottom position
        /// </summary>
        Bottom,

        /// <summary>
        /// Left position
        /// </summary>
        Left,

        /// <summary>
        /// Right position
        /// </summary>
        Right
    }
}
