﻿using Cairo;
using OrcNet.UI.Collections;
using System;
using System.Diagnostics;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Slider"/> class.
    /// </summary>
	public class Slider : ARangeBase
    {
        #region Fields

        /// <summary>
        /// Stores the tick frequency at which ticks must be shown if no ticks collection.
        /// </summary>
        private double mTickFrequency;

        /// <summary>
        /// Stores the slider moves density.
        /// </summary>
        private double mDensity;

        /// <summary>
        /// Stores the constant thumb template name.
        /// </summary>
        private const string ThumbTemplateName = "Track";

        /// <summary>
        /// Stores the slider handle to drag.
        /// </summary>
        private Thumb mSliderHandle;

        /// <summary>
        /// Stores the slider ticks.
        /// </summary>
        private DoubleCollection mTicks;

        /// <summary>
        /// Stores the tick placement on the slider if needed.
        /// </summary>
        private TickPlacement mTickPlacement;

        /// <summary>
        /// Stores the slider orientation.
        /// </summary>
        private Orientation mOrientation;

        /// <summary>
        /// Stores the flag indicating whether the handle must move automatically to the closest tick of the current value position.
        /// </summary>
        private bool mIsSnapToTickEnabled;

        /// <summary>
        /// Stores the thumb center offset from the slider.
        /// </summary>
        private double mThumbCenterOffset;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the tick frequency at which ticks must be shown if no ticks collection.
        /// </summary>
        public double TickFrequency
        {
            get
            {
                return this.mTickFrequency;
            }
            set
            {
                if ( this.mTickFrequency == value )
                {
                    return;
                }

                this.mTickFrequency = value;

                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }

        /// <summary>
        /// Gets or sets the slider ticks.
        /// </summary>
        public DoubleCollection Ticks
        {
            get
            {
                return this.mTicks;
            }
            set
            {
                if ( this.mTicks == value )
                {
                    return;
                }

                this.mTicks = value;

                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the handle must move automatically to the closest tick of the current value position.
        /// </summary>
        public bool IsSnapToTickEnabled
        {
            get
            {
                return this.mIsSnapToTickEnabled;
            }
            set
            {
                if ( this.mIsSnapToTickEnabled == value )
                {
                    return;
                }

                this.mIsSnapToTickEnabled = value;

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the tick placement on the slider if needed.
        /// </summary>
        public TickPlacement TickPlacement
        {
            get
            {
                return this.mTickPlacement;
            }
            set
            {
                if ( this.mTickPlacement == value )
                {
                    return;
                }

                this.mTickPlacement = value;

                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }

        /// <summary>
        /// Gets or sets the slider orientation.
        /// </summary>
        public Orientation Orientation
        {
            get
            {
                return this.mOrientation;
            }
            set
            {
                if ( this.mOrientation == value )
                {
                    return;
                }

                this.mOrientation = value;

                this.NotifyPropertyChanged();
                this.RegisterForLayouting( LayoutingType.All );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Slider"/> class.
        /// </summary>
        public Slider() : 
        this( 0.0, 100.0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Slider"/> class.
        /// </summary>
        /// <param name="pMinimum">The numeric minimum.</param>
        /// <param name="pMaximum">THe numeric maximum.</param>
        public Slider(double pMinimum, double pMaximum) :
        base( pMinimum, pMaximum )
        {
            this.mTickFrequency = 1.0;
            this.mTicks = new DoubleCollection();
            this.mOrientation = Orientation.Horizontal;
        }

        #endregion Constructor
        
        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if ( this.mSliderHandle != null )
            {
                this.mSliderHandle.DragStarted   -= this.OnSliderHandleDragStarted;
                this.mSliderHandle.DragChanged   -= this.OnSliderHandleDragChanged;
                this.mSliderHandle.DragCompleted -= this.OnSliderHandleDragCompleted;
            }

            this.mSliderHandle = this.GetTemplateChild( ThumbTemplateName ) as Thumb;

            if ( this.mSliderHandle != null )
            {
                this.mSliderHandle.DragStarted   += this.OnSliderHandleDragStarted;
                this.mSliderHandle.DragChanged   += this.OnSliderHandleDragChanged;
                this.mSliderHandle.DragCompleted += this.OnSliderHandleDragCompleted;
            }
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            double lIncrease;
            double lThumb;
            double lDecrease;
            bool lIsVertical = this.Orientation == Orientation.Vertical;
            this.ComputeSliderLengths( this.DesiredSize, lIsVertical, out lDecrease, out lThumb, out lIncrease );

            this.mThumbCenterOffset = lIncrease + (lThumb * 0.5);

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Delegate called on slider handle drag started.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArg"></param>
        private void OnSliderHandleDragStarted(object pSender, DragStartedEventArgs pEventArg)
        {
            
        }

        /// <summary>
        /// Delegate called on slider handle drag changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSliderHandleDragChanged(object pSender, DragDeltaEventArgs pEventArgs)
        {
            double lNewValue = this.Value + this.ValueFromDistance( pEventArgs.HorizontalDelta, pEventArgs.VerticalDelta );
            if ( DoubleUtil.IsFinite( lNewValue ) )
            {
                this.UpdateValue( lNewValue );
            }
        }

        /// <summary>
        /// Delegate called on slider handle drag complited or canceled.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSliderHandleDragCompleted(object pSender, DragCompletedEventArgs pEventArgs)
        {
            
        }

        /// <summary>
        /// Gets the value from a shifted point.
        /// </summary>
        /// <param name="pPoint"></param>
        /// <returns></returns>
        public virtual double ValueFromPoint(Point pPoint)
        {
            double lValue;

            // Computes the distance from center of thumb to given point.
            if ( this.Orientation == Orientation.Horizontal )
            {
                lValue = this.Value + this.ValueFromDistance( pPoint.X - this.mThumbCenterOffset, 
                                                              pPoint.Y - (this.ClientRectangle.Height * 0.5) );
            }
            else
            {
                lValue = this.Value + this.ValueFromDistance( pPoint.X - (this.ClientRectangle.Width * 0.5),
                                                              pPoint.Y - this.mThumbCenterOffset );
            }

            return Math.Max( this.Minimum, Math.Min( this.Maximum, lValue ) );
        }

        /// <summary>
        /// Gets the value from shifted distances.
        /// </summary>
        /// <param name="pHorizontal"></param>
        /// <param name="pVertical"></param>
        /// <returns></returns>
        public virtual double ValueFromDistance(double pHorizontal, double pVertical)
        {
            double lScale = 1;
            
            if ( this.Orientation == Orientation.Horizontal )
            {
                return lScale * pHorizontal * this.mDensity;
            }
            else
            {
                return lScale * pVertical * this.mDensity;
            }
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            if ( this.Maximum <= 0 )
            {
                return;
            }

            this.DrawGraduations( pDrawingContext );

            this.DrawCursor( pDrawingContext );
        }
        
        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.Button == MouseButton.Left &&
                 this.mSliderHandle.IsMouseOver == false )
            {
                // Jump to the cursor position in the slider.
                double lNewValue = this.ValueFromPoint( pEventArgs.Position );
                if ( DoubleUtil.IsFinite( lNewValue ) )
                {
                    this.UpdateValue( lNewValue );
                }
            }

            base.OnMouseDown( pSender, pEventArgs );
        }
        
        /// <summary>
        /// Delegate called on maximum changes
        /// </summary>
        /// <param name="pOldMaximum">The old maximum.</param>
        /// <param name="pNewMaximum">The new maximum.</param>
        protected override void OnMaximumChanged(double pOldMaximum, double pNewMaximum)
        {
            base.OnMaximumChanged( pOldMaximum, pNewMaximum );

            this.LargeIncrement = this.Maximum / 10.0;
            this.SmallIncrement = this.LargeIncrement / 5.0;
        }

        /// <summary>
        /// Draws the slider graduations.
        /// </summary>
        /// <param name="pDrawingContext"></param>
		private void DrawGraduations(Context pDrawingContext)
		{
            Rectangle lClientRegion = this.ClientRectangle;
			PointD lStart;
			PointD lEnd;
            bool lIsHorizontal = this.Orientation == Orientation.Horizontal;
            if ( lIsHorizontal )
            {
				lStart = lClientRegion.TopLeft + new Point( this.mSliderHandle == null ? 0 : this.mSliderHandle.DesiredSize.Width / 2, lClientRegion.Height / 2);
				lEnd = lClientRegion.TopRight + new Point( this.mSliderHandle == null ? 0 : -this.mSliderHandle.DesiredSize.Width / 2, lClientRegion.Height / 2);
			}
            else
            {
				lStart = lClientRegion.TopLeft + new Point( lClientRegion.Width / 2, this.mSliderHandle == null ? 0 : this.mSliderHandle.DesiredSize.Height / 2);
				lEnd = lClientRegion.BottomLeft + new Point( lClientRegion.Width / 2, this.mSliderHandle == null ? 0 : -this.mSliderHandle.DesiredSize.Height / 2);
			}

			this.InternalDrawGraduations( pDrawingContext, lStart, lEnd, lIsHorizontal );
		}

        /// <summary>
        /// Draws the slider graduations.
        /// </summary>
        /// <param name="pDrawingContext">THe drawing context.</param>
        /// <param name="pStart">The start point</param>
        /// <param name="pEnd">The end point</param>
        /// <param name="pIsHorizontal">The flag indicating whether the slider is horizontal or vertical.</param>
		protected virtual void InternalDrawGraduations(Context pDrawingContext, PointD pStart, PointD pEnd, bool pIsHorizontal)
        {
            this.Foreground.SetAsSource( pDrawingContext );

            pDrawingContext.LineWidth = 2;
            pDrawingContext.MoveTo( pStart );
            pDrawingContext.LineTo( pEnd );

            pDrawingContext.Stroke();

            // Ticks are half the width of the slider bar.
            pDrawingContext.LineWidth = pDrawingContext.LineWidth * 0.5;

            Rectangle lClientRegion = this.ClientRectangle;
            if ( this.Ticks != null &&
                 this.Ticks.Count > 0 )
            {
                for ( int lCurr = 0; lCurr < this.Ticks.Count; lCurr++ )
                {
                    double lLineLength = lClientRegion.Height / 3;
                    if ( lCurr % this.LargeIncrement != 0 )
                    {
                        lLineLength /= 3;
                    }

                    double lTickX = this.Ticks[ lCurr ];
                    PointD lTickStart = new PointD( pStart.X + lTickX, 
                                                    pStart.Y );
                    pDrawingContext.MoveTo( lTickStart );
                    pDrawingContext.LineTo( new PointD( lTickStart.X,
                                                        lTickStart.Y + lLineLength ) );
                }
            }
            else
            {
                for ( double lCurr = this.Minimum; lCurr <= this.Maximum - this.Minimum; lCurr += this.TickFrequency )
                {
                    double lLineLength = lClientRegion.Height / 3;
                    if ( lCurr % this.LargeIncrement != 0 )
                    {
                        lLineLength /= 3;
                    }

                    PointD lTickStart = new PointD( pStart.X + lCurr, 
                                                    pStart.Y );
                    pDrawingContext.MoveTo( lTickStart );
                    pDrawingContext.LineTo( new PointD( lTickStart.X,
                                                        lTickStart.Y + lLineLength ) );
                }
            }

            pDrawingContext.Stroke();
        }

        /// <summary>
        /// Draw the slider cursor.
        /// </summary>
        /// <param name="pDrawingContext"></param>
		protected virtual void DrawCursor(Context pDrawingContext)
        {
            if ( this.mSliderHandle != null )
            {
                this.mSliderHandle.Paint( ref pDrawingContext );
            }
        }
        
        /// <summary>
        /// Updates the slider value.
        /// </summary>
        /// <param name="pNewValue"></param>
        private void UpdateValue(double pNewValue)
        {
            double lSnappedValue = SnapToTick( pNewValue );

            if ( lSnappedValue != this.Value )
            {
                this.Value = Math.Max( this.Minimum, Math.Min( this.Maximum, lSnappedValue ) );
            }
        }

        /// <summary>
        /// Modify the given value to the closest tick position if any, otherwise, increment by the small increment value.
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        private double SnapToTick(double pValue)
        {
            if ( this.IsSnapToTickEnabled )
            {
                double lTickFrequency = this.TickFrequency;
                double lPrevious = this.Minimum;
                double lNext = this.Maximum;
 
                DoubleCollection lTicks = this.Ticks;
 
                if ( lTicks != null && 
                     lTicks.Count > 0 )
                {
                    for ( int lCurr = 0; lCurr < lTicks.Count; lCurr++ )
                    {
                        double lTick = lTicks[ lCurr ];
                        if ( DoubleUtil.AreClose( lTick, pValue ) )
                        {
                            return pValue;
                        }
 
                        if ( DoubleUtil.LessThan( lTick, pValue ) && 
                             DoubleUtil.GreaterThan( lTick, lPrevious ) )
                        {
                            lPrevious = lTick;
                        }
                        else if ( DoubleUtil.GreaterThan( lTick ,pValue ) && 
                                  DoubleUtil.LessThan( lTick, lNext ) )
                        {
                            lNext = lTick;
                        }
                    }
                }
                else if ( DoubleUtil.GreaterThan( lTickFrequency, 0.0 ) )
                {
                    lPrevious = this.Minimum + (Math.Round( ((pValue - this.Minimum) / lTickFrequency)) * lTickFrequency );
                    lNext = Math.Min( this.Maximum, lPrevious + lTickFrequency );
                }
 
                // Choose the closest value between Previous and Next. If tie, snap to 'Next'.
                pValue = DoubleUtil.GreaterThanOrClose( pValue, (lPrevious + lNext) * 0.5) ? lNext : lPrevious;
            }
 
            return pValue;
        }

        /// <summary>
        /// Computes the sliders length to update the thumb position.
        /// </summary>
        /// <param name="pArrangeSize"></param>
        /// <param name="pIsVertical"></param>
        /// <param name="pDecreaseButtonLength"></param>
        /// <param name="pThumbLength"></param>
        /// <param name="pIncreaseButtonLength"></param>
        private void ComputeSliderLengths(Size pArrangeSize, bool pIsVertical, out double pDecreaseButtonLength, out double pThumbLength, out double pIncreaseButtonLength)
        {
            double lMin = Minimum;
            double lRange  = Math.Max( 0.0, Maximum - lMin );
            double lOffset = Math.Min( lRange, this.Value - lMin );

            double lTrackLength;

            // Compute thumb size
            if ( pIsVertical )
            {
                lTrackLength = pArrangeSize.Height;
                pThumbLength = this.mSliderHandle == null ? 0 : this.mSliderHandle.DesiredSize.Height;
            }
            else
            {
                lTrackLength = pArrangeSize.Width;
                pThumbLength = this.mSliderHandle == null ? 0 : this.mSliderHandle.DesiredSize.Width;
            }

            CoerceLength( ref pThumbLength, lTrackLength );

            double lRemainingTrackLength = lTrackLength - pThumbLength;

            pDecreaseButtonLength = lRemainingTrackLength * lOffset / lRange;
            CoerceLength(ref pDecreaseButtonLength, lRemainingTrackLength);

            pIncreaseButtonLength = lRemainingTrackLength - pDecreaseButtonLength;
            CoerceLength(ref pIncreaseButtonLength, lRemainingTrackLength);

            Debug.Assert(pDecreaseButtonLength >= 0.0 && pDecreaseButtonLength <= lRemainingTrackLength, "decreaseButtonLength is outside bounds");
            Debug.Assert(pIncreaseButtonLength >= 0.0 && pIncreaseButtonLength <= lRemainingTrackLength, "increaseButtonLength is outside bounds");

            this.mDensity = lRange / lRemainingTrackLength;
        }
        
        #endregion Methods
    }
}
