﻿using Cairo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TextBlock"/> class.
    /// </summary>
    public class TextBlock : FrameworkElement
    {
        #region Fields

        /// <summary>
        /// Stores the block text
        /// </summary>
		private string mText = "Text";

        /// <summary>
        /// Stores the flag indicating whether the text must be auto wrapped depending on the client region.
        /// </summary>
        private TextWrapping mTextWrapping;

        /// <summary>
        /// Stores the control text font.
        /// </summary>
        private Font mFont;

        /// <summary>
        /// Stores the control background brush.
        /// </summary>
        private ABrush mBackground;

        /// <summary>
        /// Stores the control foreground brush.
        /// </summary>
        private ABrush mForeground;

        /// <summary>
        /// Stores the flag indicating whether the line cache is invalid or not.
        /// </summary>
        private bool mIsLineCacheInvalid;

        /// <summary>
        /// Stores the text splitted this.mLines.
        /// </summary>
        private List<string> mLines;

        /// <summary>
        /// Stores the text alignment.
        /// </summary>
        private TextAlignment mTextAlignment;
        
        /// <summary>
        /// Stores the text region
        /// </summary>
        private Rectangle mTextRegion;

        /// <summary>
        /// Stores the text width ratio.
        /// </summary>
        private float mWidthRatio = 1f;

        /// <summary>
        /// Stores the text height ratio.
        /// </summary>
        private float mHeightRatio = 1f;

        /// <summary>
        /// Stores the font extents
        /// </summary>
        private FontExtents mFontExtents;

        /// <summary>
        /// Stores the text extents
        /// </summary>
        private TextExtents mTextExtents;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the font style.
        /// </summary>
        public FontStyle FontStyle
        {
            get
            {
                return this.mFont.Style;
            }
            set
            {
                if ( this.mFont.Style != value )
                {
                    this.mFont.Style = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the font style.
        /// </summary>
        public FontWeight FontWeight
        {
            get
            {
                return this.mFont.Weight;
            }
            set
            {
                if ( this.mFont.Weight != value )
                {
                    this.mFont.Weight = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the font size.
        /// </summary>
        public int FontSize
        {
            get
            {
                return this.mFont.Size;
            }
            set
            {
                if ( this.mFont.Size != value )
                {
                    this.mFont.Size = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the control background brush.
        /// </summary>
        public ABrush Background
        {
            get
            {
                return this.mBackground;
            }
            set
            {
                if ( this.mBackground != value )
                {
                    this.mClearElementDrawingRegion = false;

                    if ( value != null )
                    {
                        this.mBackground = value;
                        this.NotifyPropertyChanged();
                        this.InvalidateVisual();

                        this.mClearElementDrawingRegion = this.mBackground.IsClearColor;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the control foreground brush.
        /// </summary>
        public ABrush Foreground
        {
            get
            {
                return this.mForeground;
            }
            set
            {
                if ( this.mForeground != value )
                {
                    this.mForeground = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateVisual();
                }
            }
        }
        
        /// <summary>
        /// Gets or sets the text alignment.
        /// </summary>
        public TextAlignment TextAlignment
        {
            get
            {
                return this.mTextAlignment;
            }
            set
            {
                if ( value == this.mTextAlignment )
                {
                    return;
                }

                this.mTextAlignment = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }
        
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text
        {
            get
            {
                return this.Lines.Aggregate( (i, j) => i + ATextBox.LineBreak + j );
            }
            set
            {
                if (string.Equals(value, this.mText, StringComparison.Ordinal))
                {
                    return;
                }

                this.mText = value;

                if (string.IsNullOrEmpty(this.mText))
                {
                    this.mText = string.Empty;
                }

                this.mIsLineCacheInvalid = true;

                this.NotifyPropertyChanged();
                this.OnTextChanged( new TextChangeEventArgs( this.mText ) );
                this.InvalidateArrange();
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the text must be auto wrapped depending on the client region.
        /// </summary>
        public TextWrapping TextWrapping
        {
            get
            {
                return this.mTextWrapping;
            }
            set
            {
                if ( this.mTextWrapping == value )
                {
                    return;
                }

                this.mTextWrapping = value;
                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }

        /// <summary>
        /// Gets the set of lines in the text.
        /// </summary>
        protected List<string> Lines
        {
            get
            {
                if ( this.mIsLineCacheInvalid )
                {
                    this.mLines = Regex.Split( this.mText, "\r\n|\r|\n|\\\\n" ).ToList();
                    this.mIsLineCacheInvalid = false;
                }

                return this.mLines;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBlock"/> class.
        /// </summary>
        public TextBlock() :
        this( "Text" )
		{

		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBlock"/> class.
        /// </summary>
        /// <param name="pText">The initial text.</param>
		public TextBlock(string pText) : 
        base()
		{
            this.mIsLineCacheInvalid = true;
            this.mTextWrapping = TextWrapping.NoWrap;
            this.Text = pText;
		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on text changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnTextChanged(TextChangeEventArgs pEventArgs)
		{
			
		}

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            Size lReturn;
            using ( ImageSurface lSurface = new ImageSurface( Format.Argb32, 10, 10 ) )
            {
                using ( Context lDrawingContext = new Context( lSurface ) )
                {
                    lDrawingContext.SelectFontFace( this.mFont.Name, this.mFont.Slant, this.mFont.Wheight );
                    lDrawingContext.SetFontSize( this.FontSize );
                    
                    this.mFontExtents = lDrawingContext.FontExtents;
                    this.mTextExtents = new TextExtents();

                    int lLineCount = this.Lines.Count;
                    // ensure minimal height = text line height
                    if ( lLineCount == 0 )
                    {
                        lLineCount = 1;
                    }

                    int lHeight = (int)(Math.Ceiling( this.mFontExtents.Height * lLineCount ) + this.Margin.Top + this.Margin.Bottom);
                    int lWidth  = -1;
                    try
                    {
                        foreach ( string lLine in this.Lines )
                        {
                            string lLineStr = lLine.Replace( "\t", new string(' ', ATextBox.TabSize ) );

                            TextExtents lTextExtents = lDrawingContext.TextExtents( lLineStr );

                            if ( lTextExtents.XAdvance > this.mTextExtents.XAdvance)
                            {
                                this.mTextExtents = lTextExtents;
                            }
                        }
                    }
                    catch ( Exception )
                    {
                        lWidth = - 1;
                    }

                    lWidth = (int)(Math.Ceiling( this.mTextExtents.XAdvance ) + this.Margin.Left + this.Margin.Right);

                    lReturn = new Size( lWidth, lHeight );
                }
            }

            return lReturn;
        }
        
        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            Rectangle lBackgroundRegion = new Rectangle( this.Slot.Size );

            this.Background.SetAsSource( pDrawingContext, lBackgroundRegion );
            CairoHelpers.CairoRectangle( pDrawingContext, lBackgroundRegion, -1 );
            pDrawingContext.Fill();

            pDrawingContext.SelectFontFace(this.mFont.Name, this.mFont.Slant, this.mFont.Wheight);
            pDrawingContext.SetFontSize(this.FontSize);
            pDrawingContext.FontOptions = PresentationSource.FontRenderingOptions;
            pDrawingContext.Antialias = PresentationSource.Antialias;

            this.mTextRegion = new Rectangle( this.Measure() );
            this.mTextRegion.Width  -= (int)(this.Margin.Left + this.Margin.Right);
            this.mTextRegion.Height -= (int)(this.Margin.Top + this.Margin.Bottom);

            this.mWidthRatio  = 1f;
            this.mHeightRatio = 1f;

            Rectangle lRegion = this.ClientRectangle;

            this.mTextRegion.X = lRegion.X;
            this.mTextRegion.Y = lRegion.Y;

            // Was in TextRun code... See if it is relevant.
            //if ( this.Width < 0 || 
            //     this.Height < 0 || 
            //     this.mTextRegion.Width > lRegion.Width )
            //{
            //    this.mTextRegion.X = lRegion.X;
            //    this.mTextRegion.Y = lRegion.Y;
            //}
            //else
            {
                if ( this.HorizontalAlignment == HorizontalAlignment.Stretch )
                {
                    mWidthRatio = lRegion.Width / (float)mTextRegion.Width;
                    if ( this.VerticalAlignment != VerticalAlignment.Stretch )
                    {
                        mHeightRatio = mWidthRatio;
                    }
                }

                if ( this.VerticalAlignment == VerticalAlignment.Stretch )
                {
                    mHeightRatio = lRegion.Height / (float)mTextRegion.Height;
                    if ( this.HorizontalAlignment != HorizontalAlignment.Stretch )
                    {
                        mWidthRatio = mHeightRatio;
                    }
                }

                if ( this.mTextWrapping == TextWrapping.Wrap )
                {
                    this.mTextRegion.Width  = (int)(this.mWidthRatio  * lRegion.Width);
                    this.mTextRegion.Height = (int)(this.mHeightRatio * lRegion.Height);
                }
                else
                {
                    this.mTextRegion.Width  = (int)(this.mWidthRatio  * this.mTextRegion.Width);
                    this.mTextRegion.Height = (int)(this.mHeightRatio * this.mTextRegion.Height);
                }

                switch ( this.TextAlignment )
                {
                    //case Alignment.TopLeft:     //ok
                    //    mTextRegion.X = lRegion.X;
                    //    mTextRegion.Y = lRegion.Y;
                    //    break;
                    //case Alignment.Top:   //ok
                    //    mTextRegion.Y = lRegion.Y;
                    //    mTextRegion.X = lRegion.X + lRegion.Width / 2 - mTextRegion.Width / 2;
                    //    break;
                    //case Alignment.TopRight:    //ok
                    //    mTextRegion.Y = lRegion.Y;
                    //    mTextRegion.X = lRegion.Right - mTextRegion.Width;
                    //    break;
                    case TextAlignment.Left://ok
                        mTextRegion.X = lRegion.X;
                        mTextRegion.Y = lRegion.Y + lRegion.Height / 2 - mTextRegion.Height / 2;
                        break;
                    case TextAlignment.Right://ok
                        mTextRegion.X = lRegion.X + lRegion.Width - mTextRegion.Width;
                        mTextRegion.Y = lRegion.Y + lRegion.Height / 2 - mTextRegion.Height / 2;
                        break;
                    //case Alignment.Bottom://ok
                    //    mTextRegion.X = lRegion.Width / 2 - mTextRegion.Width / 2;
                    //    mTextRegion.Y = lRegion.Height - mTextRegion.Height;
                    //    break;
                    //case Alignment.BottomLeft://ok
                    //    mTextRegion.X = lRegion.X;
                    //    mTextRegion.Y = lRegion.Bottom - mTextRegion.Height;
                    //    break;
                    //case Alignment.BottomRight://ok
                    //    mTextRegion.Y = lRegion.Bottom - mTextRegion.Height;
                    //    mTextRegion.X = lRegion.Right - mTextRegion.Width;
                    //    break;
                    case TextAlignment.Center://ok
                    case TextAlignment.Justify://ok
                        mTextRegion.X = lRegion.X + lRegion.Width / 2 - mTextRegion.Width / 2;
                        mTextRegion.Y = lRegion.Y + lRegion.Height / 2 - mTextRegion.Height / 2;
                        break;
                }
            }

            pDrawingContext.FontMatrix = new Matrix( mWidthRatio * this.FontSize, 0, 0, mHeightRatio * this.FontSize, 0, 0 );
            this.mFontExtents = pDrawingContext.FontExtents;

            int lCurrLineCount = 0;
            for ( int lCurr = 0; lCurr < this.Lines.Count; lCurr++ )
            {
                string lLine = this.Lines[ lCurr ].Replace( "\t", new string( ' ', ATextBox.TabSize ) );
                int lLineLength = this.ComputeLineLength( lLine, lRegion, pDrawingContext );
                //Rectangle lLineRegion = new Rectangle( this.mTextRegion.X,
                //                                       this.mTextRegion.Y + (int)Math.Ceiling( lCurr * this.mFontExtents.Height ),
                //                                       lLineLength,
                //                                       (int)Math.Ceiling( this.mFontExtents.Height ) );

                if ( string.IsNullOrWhiteSpace( lLine ) )
                {
                    lCurrLineCount++;
                    continue;
                }

                this.Foreground.SetAsSource( pDrawingContext );

                double lX = this.mTextRegion.X;
                double lHeightExtent = this.mFontExtents.Height * lCurr;
                if ( this.mTextWrapping == TextWrapping.Wrap )
                {
                    lHeightExtent = this.mFontExtents.Height * lCurrLineCount;
                }
                double lY = this.mTextRegion.Y + this.mFontExtents.Ascent + lHeightExtent;

                pDrawingContext.MoveTo( lX, lY );
                pDrawingContext.ShowText( lLine );
                pDrawingContext.Fill();

                lCurrLineCount++;
            }
        }

        /// <summary>
        /// Computes the line length.
        /// </summary>
        /// <param name="pCurrentLine"></param>
        /// <param name="pRegion"></param>
        /// <param name="pDrawingContext"></param>
        /// <returns></returns>
        private int ComputeLineLength(string pCurrentLine, Rectangle pRegion, Context pDrawingContext)
        {
            List<string> lLines = new List<string>();
            int lLineLength = (int)pDrawingContext.TextExtents( pCurrentLine ).XAdvance;
            if ( this.mTextWrapping == TextWrapping.Wrap )
            {
                if ( lLineLength > pRegion.Width )
                {
                    string lTempLine = string.Empty;
                    int lCurrentChar = 0;
                    while ( lCurrentChar < pCurrentLine.Length)
                    {
                        lTempLine += pCurrentLine[ lCurrentChar ];
                        if ((int)pDrawingContext.TextExtents( lTempLine ).XAdvance > pRegion.Width )
                        {
                            lTempLine = lTempLine.Remove( lTempLine.Length - 1 );
                            lLines.Add( lTempLine );
                            lTempLine = string.Empty;
                            continue;
                        }

                        lCurrentChar++;
                    }
                    
                    lLines.Add( lTempLine );
                }
            }
            else
            {
                lLines.Add( pCurrentLine );
            }

            foreach ( string lCurrentLine in lLines )
            {
                lLineLength = (int)pDrawingContext.TextExtents( lCurrentLine ).XAdvance;

            }

            return lLineLength;
        }
        
        #endregion Methods
    }
}
