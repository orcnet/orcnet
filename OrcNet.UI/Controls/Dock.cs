﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Dock"/> enumeration.
    /// </summary>
    public enum Dock
    {
        /// <summary>
        /// Dock to bottom
        /// </summary>
        Bottom,

        /// <summary>
        /// Dock to left
        /// </summary>
        Left,

        /// <summary>
        /// Dock to right
        /// </summary>
        Right,

        /// <summary>
        /// Dock to top.
        /// </summary>
        Top
    }
}
