﻿using Cairo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the abstract <see cref="ATextBox"/> base class.
    /// </summary>
    public abstract class ATextBox : Control
    {
        #region Statics

        /// <summary>
        /// Stores the text tabulation empty char count.
        /// </summary>
        public static int TabSize = 4;

        /// <summary>
        /// Stores the text line break character(s).
        /// </summary>
        public static string LineBreak = "\r\n";

        /// <summary>
        /// Stores the text box copied text.
        /// </summary>
        private static string Clipboard;

        #endregion Statics

        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the Shift key is pressed or not.
        /// </summary>
        private bool mIsShiftPressed;

        /// <summary>
        /// Stores the block text
        /// </summary>
		private string mText = "Text";

        /// <summary>
        /// Stores the flag indicating whether the line cache is invalid or not.
        /// </summary>
        private bool mIsLineCacheInvalid;

        /// <summary>
        /// Stores the text splitted this.mLines.
        /// </summary>
        private List<string> mLines;

        /// <summary>
        /// Stores the selection brush.
        /// </summary>
        private ABrush mSelectionBrush;

        /// <summary>
        /// Stores the selection opacity.
        /// </summary>
        private double mSelectionOpacity;

        /// <summary>
        /// Stores the caret brush.
        /// </summary>
        private ABrush mCaretBrush;

        /// <summary>
        /// Stores the current column posiion
        /// 0 based cursor position in string
        /// </summary>
        private int mCurrentCol;

        /// <summary>
        /// Stores the current line position.
        /// </summary>
        private int mCurrentLine;

        /// <summary>
        /// Stores the mouse coord in widget space, filled only when clicked
        /// </summary>
        protected Point mMouseLocalPos = -1;
        
        /// <summary>
        /// Stores the selection start (row,column)
        /// </summary>
        private Point mSelectionStart = -1;

        /// <summary>
        /// Stores the selection end (row,column)
        /// </summary>
        private Point mSelectionEnd = -1;
        
        /// <summary>
        /// Stores the flag indicating whether the selection is in progress or not.
        /// </summary>
        private bool mIsSelectionInProgress = false;

        /// <summary>
        /// Stores the font extents
        /// </summary>
        protected FontExtents mFontExtents;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the selection brush.
        /// </summary>
        public ABrush SelectionBrush
        {
            get
            {
                return this.mSelectionBrush;
            }
            set
            {
                if ( this.mSelectionBrush == value )
                {
                    return;
                }

                this.mSelectionBrush = value;

                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the selection opacity.
        /// </summary>
        public double SelectionOpacity
        {
            get
            {
                return this.mSelectionOpacity;
            }
            set
            {
                if ( this.mSelectionOpacity == value )
                {
                    return;
                }

                this.mSelectionOpacity = value;

                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the caret brush.
        /// </summary>
        public ABrush CaretBrush
        {
            get
            {
                return this.mCaretBrush;
            }
            set
            {
                if ( this.mCaretBrush == value )
                {
                    return;
                }

                this.mCaretBrush = value;

                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        protected string TextInternal
        {
            get
            {
                return this.Lines.Aggregate( (i, j) => i + LineBreak + j );
            }
            set
            {
                if (string.Equals(value, this.mText, StringComparison.Ordinal))
                {
                    return;
                }

                this.mText = value;

                if (string.IsNullOrEmpty(this.mText))
                {
                    this.mText = string.Empty;
                }

                this.mIsLineCacheInvalid = true;

                this.OnTextChanged( new TextChangeEventArgs( this.TextInternal ) );
                this.InvalidateArrange();
            }
        }
        
        /// <summary>
        /// Gets the line count.
        /// </summary>
        protected int LineCountInternal
        {
            get
            {
                return this.Lines.Count;
            }
        }

        /// <summary>
        /// Gets the set of lines in the text.
        /// </summary>
        protected List<string> Lines
        {
            get
            {
                if ( this.mIsLineCacheInvalid )
                {
                    this.mLines = Regex.Split( this.mText, "\r\n|\r|\n|\\\\n" ).ToList();
                    this.mIsLineCacheInvalid = false;
                }

                return this.mLines;
            }
        }

        /// <summary>
        /// Gets or sets the current column.
        /// </summary>
        protected int CurrentColumn
        {
            get
            {
                return this.mCurrentCol;
            }
            set
            {
                if ( value == this.mCurrentCol )
                {
                    return;
                }

                if ( value < 0 )
                {
                    this.mCurrentCol = 0;
                }
                else
                {
                    int lLength = this.Lines[ this.mCurrentCol ].Length;
                    if ( value > lLength )
                    {
                        this.mCurrentCol = lLength;
                    }
                    else
                    {
                        this.mCurrentCol = value;
                    }
                }

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current line.
        /// </summary>
        protected int CurrentLine
        {
            get
            {
                return this.mCurrentLine;
            }
            set
            {
                if ( value == this.mCurrentLine )
                {
                    return;
                }

                if ( value >= this.Lines.Count )
                {
                    this.mCurrentLine = this.Lines.Count - 1;
                }
                else if ( value < 0 )
                {
                    this.mCurrentLine = 0;
                }
                else
                {
                    this.mCurrentLine = value;
                }

                // Force recheck of currentCol for bounding
                int lPrevious = this.mCurrentCol;
                this.mCurrentCol = 0;
                this.CurrentColumn = lPrevious;

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the current caret position.
        /// </summary>
        protected Point CurrentPosition
        {
            get
            {
                return new Point( this.CurrentColumn, this.CurrentLine );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the selection is empty or not.
        /// </summary>
        protected bool IsSelectionEmpty
        {
            get
            {
                return this.mSelectionEnd < 0;
            }
        }

        /// <summary>
        /// Gets or sets the start selection position in char units
        /// </summary>
        protected Point SelectionStartInternal
        {
            get
            {
                return this.mSelectionStart;
            }
            set
            {
                if (value == this.mSelectionStart)
                {
                    return;
                }

                this.mSelectionStart = value;

                this.OnSelectionChanged( null );
            }
        }

        /// <summary>
        /// Gets or sets the end selection position in char units.
        /// </summary>
        protected Point SelectionEndInternal
        {
            get
            {
                return this.mSelectionEnd;
            }
            set
            {
                if (value == this.mSelectionEnd)
                {
                    return;
                }

                this.mSelectionEnd = value;

                this.OnSelectionChanged( null );
            }
        }
        /// <summary>
        /// return char at CurrentLine, CurrentColumn
        /// </summary>
        protected char CurrentChar
        {
            get
            {
                return this.Lines[CurrentLine][CurrentColumn];
            }
        }
        
        /// <summary>
        /// Gets the start position of the selection
        /// </summary>
        protected Point StartPositionInternal
        {
            get
            {
                return this.SelectionEndInternal < 0 || this.SelectionStartInternal.Y < this.SelectionEndInternal.Y ? this.SelectionStartInternal :
                       this.SelectionStartInternal.Y > this.SelectionEndInternal.Y ? this.SelectionEndInternal :
                       this.SelectionStartInternal.X < this.SelectionEndInternal.X ? this.SelectionStartInternal : this.SelectionEndInternal;
            }
        }

        /// <summary>
        /// Gets the end position of the selection
        /// </summary>
        protected Point EndPositionInternal
        {
            get
            {
                return this.SelectionEndInternal < 0 || this.SelectionStartInternal.Y > this.SelectionEndInternal.Y ? this.SelectionStartInternal :
                       this.SelectionStartInternal.Y < this.SelectionEndInternal.Y ? this.SelectionEndInternal :
                       this.SelectionStartInternal.X > this.SelectionEndInternal.X ? this.SelectionStartInternal : this.SelectionEndInternal;
            }
        }
        
        /// <summary>
        /// Gets the selected text.
        /// </summary>
        protected string SelectedTextInternal
        {
            get
            {
                if (this.SelectionEndInternal < 0 || this.SelectionStartInternal < 0)
                {
                    return string.Empty;
                }

                if (this.StartPositionInternal.Y == this.EndPositionInternal.Y)
                {
                    return this.Lines[this.StartPositionInternal.Y].Substring(this.StartPositionInternal.X, this.EndPositionInternal.X - this.StartPositionInternal.X);
                }

                string lTemp = string.Empty;
                lTemp = this.Lines[this.StartPositionInternal.Y].Substring(this.StartPositionInternal.X);
                for (int lCurr = this.StartPositionInternal.Y + 1; lCurr < this.EndPositionInternal.Y; lCurr++)
                {
                    lTemp += LineBreak + this.Lines[lCurr];
                }

                lTemp += LineBreak + this.Lines[this.EndPositionInternal.Y].Substring(0, this.EndPositionInternal.X);
                return lTemp;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the selection is in progress or not.
        /// </summary>
        protected bool IsSelectionInProgress
        {
            get
            {
                return this.mIsSelectionInProgress;
            }
            set
            {
                this.mIsSelectionInProgress = value;
                this.OnSelectionChanged( null );
                this.InvalidateVisual();
            }
        }

        #endregion Properties

        #region Events

        /// <summary>
        /// Event fired on text selection changes.
        /// </summary>
        public event EventHandler SelectionChanged;

        /// <summary>
        /// Event fired on text changes.
        /// </summary>
		public event EventHandler<TextChangeEventArgs> TextChanged;

        #endregion Events

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ATextBox"/> class.
        /// </summary>
        protected ATextBox() :
        this( "Text" )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ATextBox"/> class.
        /// </summary>
        /// <param name="pInitialText">The initial text.</param>
        protected ATextBox(string pInitialText)
        {
            this.mCurrentCol = 0;
            this.mCurrentLine = 0;
            this.mSelectionStart = -1;
            this.mSelectionEnd = -1;
            this.TextInternal = pInitialText;
            this.mIsLineCacheInvalid = true;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Selects the overall text.
        /// </summary>
        public void SelectAll()
        {
            // TO DO: implement
        }

        /// <summary>
        /// Delegate called on UIElement mouse enter.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseEnter(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseEnter( pSender, pEventArgs );

            this.Cursor = XCursor.Text;
        }

        /// <summary>
        /// Delegate called on UIElement mouse leave.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseLeave(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseLeave( pSender, pEventArgs );

            this.Cursor = XCursor.Default;
        }

        /// <summary>
        /// Delegate called on mouse up.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseUp(object pSender, MouseButtonEventArgs pEventArgs)
        {
            base.OnMouseUp( pSender, pEventArgs );

            if ( this.IsSelectionInProgress == false )
            {
                return;
            }

            this.UpdateMouseLocalPosition( pEventArgs.Position );
            this.IsSelectionInProgress = false;
        }

        /// <summary>
        /// Delegate called on UIelement mouse moves.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseMove(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseMove( pSender, pEventArgs );

            if ( (this.IsSelectionInProgress && this.IsFocused) == false )
            {
                return;
            }

            this.UpdateMouseLocalPosition( pEventArgs.Position );

            this.InvalidateVisual();
        }

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( this.IsFocused )
            {
                this.UpdateMouseLocalPosition( pEventArgs.Position );
                this.mSelectionStart = -1;
                this.mSelectionEnd   = -1;
                this.IsSelectionInProgress = true;
            }

            base.OnMouseDown( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on mouse double click.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDoubleClick(object pSender, MouseButtonEventArgs pEventArgs)
        {
            base.OnMouseDoubleClick( pSender, pEventArgs );

            this.MoveToWordStart();
            this.mSelectionStart = this.CurrentPosition;
            this.MoveToWordEnd();
            this.mSelectionEnd   = this.CurrentPosition;
            this.IsSelectionInProgress = false;
        }
        
        /// <summary>
        /// Delegate called on key pressed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal override void OnKeyDown(object pSender, KeyboardKeyEventArgs pEventArgs)
        {
            base.OnKeyDown( pSender, pEventArgs );

            this.mIsShiftPressed = pEventArgs.Shift;

            Key lKey = pEventArgs.Key;
            switch ( lKey )
            {
                case Key.Back:
                    if (this.CurrentPosition == 0)
                    {
                        return;
                    }
                    this.RemoveChar();
                    break;
                case Key.Clear:
                    break;
                case Key.Delete:
                    if ( this.IsSelectionEmpty )
                    {
                        if ( this.MoveRight() == false )
                        {
                            return;
                        }
                    }
                    else if ( pEventArgs.Shift )
                    {
                        ATextBox.Clipboard = this.SelectedTextInternal;
                    }

                    this.RemoveChar();
                    break;
                case Key.Enter:
                case Key.KeypadEnter:
                    if ( this.IsSelectionEmpty == false )
                    {
                        this.RemoveChar();
                    }

                    this.AppendLine();
                    break;
                case Key.Escape:
                    this.TextInternal = string.Empty;
                    this.CurrentColumn = 0;
                    this.SelectionEndInternal = -1;
                    break;
                case Key.Home:
                    if (pEventArgs.Shift)
                    {
                        if ( this.IsSelectionEmpty )
                        {
                            this.SelectionStartInternal = this.CurrentPosition;
                        }

                        if (pEventArgs.Control)
                        {
                            this.CurrentLine = 0;
                        }

                        this.CurrentColumn = 0;
                        this.SelectionEndInternal = this.CurrentPosition;
                        break;
                    }

                    this.SelectionEndInternal = -1;
                    if (pEventArgs.Control)
                    {
                        this.CurrentLine = 0;
                    }

                    this.CurrentColumn = 0;
                    break;
                case Key.End:
                    if (pEventArgs.Shift)
                    {
                        if ( this.IsSelectionEmpty )
                        {
                            this.SelectionStartInternal = this.CurrentPosition;
                        }

                        if (pEventArgs.Control)
                        {
                            this.CurrentLine = int.MaxValue;
                        }

                        this.CurrentColumn = int.MaxValue;
                        this.SelectionEndInternal = this.CurrentPosition;
                        break;
                    }

                    this.SelectionEndInternal = -1;
                    if ( pEventArgs.Control )
                    {
                        this.CurrentLine = int.MaxValue;
                    }

                    this.CurrentColumn = int.MaxValue;
                    break;
                case Key.Insert:
                    if ( pEventArgs.Shift )
                    {
                        this.Append( ATextBox.Clipboard );
                    }

                    else if ( pEventArgs.Control && 
                              this.IsSelectionEmpty == false )
                    {
                        ATextBox.Clipboard = this.SelectedTextInternal;
                    }

                    break;
                case Key.Left:
                    if ( pEventArgs.Shift )
                    {
                        if ( this.IsSelectionEmpty )
                        {
                            this.SelectionStartInternal = this.CurrentPosition;
                        }

                        if (pEventArgs.Control)
                        {
                            this.MoveToWordStart();
                        }
                        else if ( this.MoveLeft() == false )
                        {
                            return;
                        }

                        this.SelectionEndInternal = this.CurrentPosition;
                        break;
                    }

                    this.SelectionEndInternal = -1;
                    if ( pEventArgs.Control )
                    {
                        this.MoveToWordStart();
                    }
                    else
                    {
                        this.MoveLeft();
                    }
                    break;
                case Key.Right:
                    if ( pEventArgs.Shift )
                    {
                        if ( this.IsSelectionEmpty )
                        {
                            this.SelectionStartInternal = this.CurrentPosition;
                        }
                        if (pEventArgs.Control)
                        {
                            this.MoveToWordEnd();
                        }
                        else if ( this.MoveRight() == false )
                        {
                            return;
                        }

                        this.SelectionEndInternal = this.CurrentPosition;
                        break;
                    }

                    this.SelectionEndInternal = -1;
                    if ( pEventArgs.Control )
                    {
                        this.MoveToWordEnd();
                    }
                    else
                    {
                        this.MoveRight();
                    }
                    break;
                case Key.Up:
                    this.LineUp();
                    break;
                case Key.Down:
                    this.LineDown();
                    break;
                case Key.Menu:
                    break;
                case Key.NumLock:
                    break;
                case Key.PageDown:
                    break;
                case Key.PageUp:
                    break;
                case Key.RWin:
                    break;
                case Key.Tab:
                    this.Append("\t");
                    break;
                default:
                    break;
            }

            this.InvalidateArrange();
        }

        /// <summary>
        /// Delegate called on any key pressed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal override void OnKeyPress(object pSender, KeyPressEventArgs pEventArgs)
        {
            base.OnKeyPress( pSender, pEventArgs );

            this.Append( pEventArgs.KeyChar.ToString() );

            this.mSelectionEnd = -1;
            this.mSelectionStart = new Point( this.CurrentColumn, this.mSelectionStart.Y );

            this.OnSelectionChanged( null );

            this.InvalidateArrange();
        }

        /// <summary>
        /// Delegate called on key released.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal override void OnKeyUp(object pSender, KeyboardKeyEventArgs pEventArgs)
        {
            base.OnKeyUp( pSender, pEventArgs );

            this.mIsShiftPressed = pEventArgs.Shift;
        }

        /// <summary>
        /// Delegate called on element got focus event.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnGotFocus(object pSender, EventArgs pEventArgs)
        {
            base.OnGotFocus( pSender, pEventArgs );

            string lLastLine = this.Lines.LastOrDefault();

            this.mSelectionStart = new Point( 0, 0 );
            this.mSelectionEnd   = new Point( lLastLine != null ? lLastLine.Length : 0, this.Lines.Count - 1);
            this.InvalidateVisual();
        }

        /// <summary>
        /// Delegate called on element lost focus event.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnLostFocus(object pSender, EventArgs pEventArgs)
        {
            base.OnLostFocus( pSender, pEventArgs );

            this.mSelectionStart = -1;
            this.mSelectionEnd   = -1;
            this.InvalidateVisual();
        }

        /// <summary>
        /// Moves the caret to the previous line.
        /// </summary>
        public void LineUp()
        {
            if ( this.mIsShiftPressed )
            {
                if (this.IsSelectionEmpty)
                {
                    this.SelectionStartInternal = this.CurrentPosition;
                }

                this.CurrentLine--;
                this.SelectionEndInternal = this.CurrentPosition;
            }
            else
            {
                this.SelectionEndInternal = -1;
                this.CurrentLine--;
            }
        }

        /// <summary>
        /// Moves the caret to the next line.
        /// </summary>
        public void LineDown()
        {
            if ( this.mIsShiftPressed )
            {
                if (this.IsSelectionEmpty)
                {
                    this.SelectionStartInternal = this.CurrentPosition;
                }

                this.CurrentLine++;
                this.SelectionEndInternal = this.CurrentPosition;
            }
            else
            {
                this.SelectionEndInternal = -1;
                this.CurrentLine++;
            }
        }

        /// <summary>
		/// Moves cursor one char to the left.
		/// </summary>
		/// <returns><c>true</c> if move succeed</returns>
		private bool MoveLeft()
        {
            int lTemp = this.mCurrentCol - 1;
            if ( lTemp < 0 )
            {
                if ( this.mCurrentLine == 0 )
                {
                    return false;
                }

                this.CurrentLine--;
                this.CurrentColumn = int.MaxValue;
            }
            else
            {
                this.CurrentColumn = lTemp;
            }

            return true;
        }
        /// <summary>
        /// Moves cursor one char to the right.
        /// </summary>
        /// <returns><c>true</c> if move succeed</returns>
        private bool MoveRight()
        {
            int lTemp = this.mCurrentCol + 1;
            if ( lTemp > this.Lines[ this.mCurrentLine ].Length )
            {
                if ( this.CurrentLine == this.Lines.Count - 1 )
                {
                    return false;
                }

                this.CurrentLine++;
                this.CurrentColumn = 0;
            }
            else
            {
                this.CurrentColumn = lTemp;
            }

            return true;
        }

        /// <summary>
        /// Moves to the start of the word
        /// </summary>
        protected void MoveToWordStart()
        {
            this.CurrentColumn--;

            //skip white spaces
            while ( char.IsLetterOrDigit(this.CurrentChar) == false && 
                    this.CurrentColumn > 0 )
            {
                this.CurrentColumn--;
            }

            while (char.IsLetterOrDigit(this.Lines[this.CurrentLine][this.CurrentColumn]) && 
                   this.CurrentColumn > 0)
            {
                this.CurrentColumn--;
            }

            if ( char.IsLetterOrDigit(this.CurrentChar) == false )
            {
                this.CurrentColumn++;
            }
        }

        /// <summary>
        /// Moves to the end of the word
        /// </summary>
        protected void MoveToWordEnd()
        {
            // skip white spaces
            if ( this.CurrentColumn >= this.Lines[ this.CurrentLine].Length - 1)
            {
                return;
            }

            while ( char.IsLetterOrDigit(this.CurrentChar) == false && 
                    this.CurrentColumn < this.Lines[ this.CurrentLine ].Length - 1)
            {
                this.CurrentColumn++;
            }

            while (char.IsLetterOrDigit(this.CurrentChar) &&
                   this.CurrentColumn < this.Lines[ this.CurrentLine ].Length - 1 )
            {
                this.CurrentColumn++;
            }

            if (char.IsLetterOrDigit(this.CurrentChar))
            {
                this.CurrentColumn++;
            }
        }
        
        /// <summary>
        /// Delegate called on text changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnTextChanged(TextChangeEventArgs pEventArgs)
        {
            this.NotifyTextChanged( pEventArgs );
        }

        /// <summary>
        /// Delegate called on selection changes.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected virtual void OnSelectionChanged(EventArgs pEventArgs)
        {
            this.NotifySelectionChanged( pEventArgs );
        }

        /// <summary>
        /// Removes a single character.
        /// </summary>
        protected void RemoveChar()
        {
            if ( this.IsSelectionEmpty )
            {
                if ( this.CurrentColumn == 0 )
                {
                    if ( this.CurrentLine == 0 && this.Lines.Count == 1 )
                    {
                        return;
                    }

                    this.CurrentLine--;
                    this.CurrentColumn = this.Lines[ this.CurrentLine ].Length;
                    this.Lines[ this.CurrentLine ] += this.Lines[ this.CurrentLine + 1 ];
                    this.Lines.RemoveAt( this.CurrentLine + 1 );

                    this.OnTextChanged( new TextChangeEventArgs( this.TextInternal ) );
                    return;
                }

                this.CurrentColumn--;
                this.Lines[ this.CurrentLine ] = this.Lines[ this.CurrentLine ].Remove( this.CurrentColumn, 1 );
            }
            else
            {
                int lLinesToRemove = this.EndPositionInternal.Y - this.StartPositionInternal.Y + 1;
                int l = this.StartPositionInternal.Y;

                if ( lLinesToRemove > 0 )
                {
                    this.Lines[l] = this.Lines[l].Remove(this.StartPositionInternal.X, this.Lines[l].Length - this.StartPositionInternal.X) +
                    this.Lines[this.EndPositionInternal.Y].Substring(this.EndPositionInternal.X, this.Lines[this.EndPositionInternal.Y].Length - this.EndPositionInternal.X);
                    l++;

                    for (int lCurr = 0; lCurr < lLinesToRemove - 1; lCurr++)
                    {
                        this.Lines.RemoveAt(l);
                    }

                    this.CurrentLine   = this.StartPositionInternal.Y;
                    this.CurrentColumn = this.StartPositionInternal.X;
                }
                else
                {
                    this.Lines[l] = this.Lines[l].Remove(this.StartPositionInternal.X, this.EndPositionInternal.X - this.StartPositionInternal.X);
                }

                this.CurrentColumn = this.StartPositionInternal.X;
                this.SelectionStartInternal = -1;
                this.SelectionEndInternal = -1;
            }

            this.OnTextChanged( new TextChangeEventArgs( this.TextInternal ) );
        }
        /// <summary>
        /// Insert new string at caret position, should be sure no line break is inside.
        /// </summary>
        /// <param name="pText">String.</param>
        public void Append(string pText)
        {
            if ( this.IsSelectionEmpty == false )
            {
                this.RemoveChar();
            }

            string[] lNewLines = Regex.Split( pText, "\r\n|\r|\n|" + @"\\n" ).ToArray();
            this.Lines[ this.CurrentLine ] = this.Lines[ this.CurrentLine ].Insert( this.CurrentColumn, lNewLines[ 0 ] );
            this.CurrentColumn += lNewLines[0].Length;
            for ( int lCurr = 1; lCurr < lNewLines.Length; lCurr++ )
            {
                this.AppendLine();
                this.Lines[ this.CurrentLine ] = this.Lines[ this.CurrentLine ].Insert( this.CurrentColumn, lNewLines[ lCurr ] );
                this.CurrentColumn += lNewLines[ lCurr ].Length;
            }

            this.OnTextChanged( new TextChangeEventArgs( this.TextInternal ) );
        }
        /// <summary>
        /// Insert a line break.
        /// </summary>
        protected void AppendLine()
        {
            this.Lines.Insert(CurrentLine + 1, this.Lines[CurrentLine].Substring(CurrentColumn));
            this.Lines[CurrentLine] = this.Lines[CurrentLine].Substring(0, CurrentColumn);
            CurrentLine++;
            CurrentColumn = 0;

            this.OnTextChanged( new TextChangeEventArgs( this.TextInternal ) );
        }

        /// <summary>
        /// Notifies the selection has changed.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected void NotifySelectionChanged(EventArgs pEventArgs)
        {
            if ( this.SelectionChanged != null )
            {
                this.SelectionChanged( this, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies the text has changed.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected void NotifyTextChanged(TextChangeEventArgs pEventArgs)
        {
            if ( this.TextChanged != null )
            {
                this.TextChanged( this, pEventArgs );
            }
        }
        
        /// <summary>
        /// Updates the mouse local position.
        /// </summary>
        /// <param name="pPosition"></param>
        private void UpdateMouseLocalPosition(Point pPosition)
        {
            this.mMouseLocalPos = pPosition - this.ScreenCoordinates( this.Slot ).TopLeft - this.ClientRectangle.TopLeft;
            if ( this.mMouseLocalPos.X < 0 )
            {
                this.mMouseLocalPos.X = 0;
            }

            if ( this.mMouseLocalPos.Y < 0 )
            {
                this.mMouseLocalPos.Y = 0;
            }
        }

        #endregion Methods
    }
}
