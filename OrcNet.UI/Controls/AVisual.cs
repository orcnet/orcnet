﻿using Cairo;
using OrcNet.UI.Helpers;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="AVisual"/> class.
    /// </summary>
    public abstract class AVisual : ADispatcherObject, INotifyPropertyChanged, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the visual is a root element or not.
        /// </summary>
        private bool mIsRootElement;

        /// <summary>
        /// Stores the element index in the parent child array.
        /// </summary>
        internal int ParentIndex;

        /// <summary>
        /// Stores the static id provider.
        /// </summary>
        private static ulong sNextId = 0;

        /// <summary>
        /// Stores the flag indicating whether the visual is dirty or not.
        /// </summary>
        protected bool mIsDirty;

        /// <summary>
        /// Stores the visual element internal Id.
        /// </summary>
        private ulong mId;
        
        /// <summary>
        /// Stores the flag indicating whether the cache is enabled or not.
        /// </summary>
        private bool mIsCacheEnabled = false;

        /// <summary>
        /// Stores the flag indicating whether or not to clip to the client rectangle.
        /// </summary>
        private bool mClipToClientRect = true;

        /// <summary>
        /// Stores the visual clip region.
        /// </summary>
        private Rectangles mVisualClip;

        /// <summary>
        /// Stores the visual parent.
        /// </summary>
        private AVisual mVisualParent;

        /// <summary>
        /// Stores the current size and position of this visual.
        /// </summary>
        protected Rectangle mSlot;

        /// <summary>
        /// Keeps the last slot components to track changes and update others accordingly if needed.
        /// </summary>
        protected Rectangle mLastSlots;
        
        /// <summary>
        /// Stores the flag indicating whether visual has been queued to be re drawn or not.
        /// </summary>
        protected bool mIsQueuedForDraw;

        /// <summary>
        /// Gets or sets the visual presentation source.
        /// </summary>
        protected PresentationSource mPresentationSource;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on parent changes.
        /// </summary>
        public event EventHandler<PropertyValueChangedEventArgs> ParentChanged;

        /// <summary>
        /// Event fired on property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the visual is a root element or not.
        /// </summary>
        internal bool IsRootElement
        {
            get
            {
                return this.mIsRootElement;
            }
            set
            {
                this.mIsRootElement = value;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the visual is dirty or not.
        /// </summary>
        internal bool IsDirty
        {
            get
            {
                return this.mIsDirty;
            }
            set
            {
                this.mIsDirty = value;
            }
        }

        /// <summary>
        /// Gets the visual element internal Id.
        /// </summary>
        public ulong Id
        {
            get
            {
                return this.mId;
            }
        }
        
        /// <summary>
        /// Gets or sets the flag indicating whether the cache is enabled or not.
        /// </summary>
        public virtual bool IsCacheEnabled
        {
            get
            {
                return this.mIsCacheEnabled;
            }
            set
            {
                if ( this.mIsCacheEnabled != value )
                {
                    this.mIsCacheEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether or not to clip to the client rectangle.
        /// </summary>
        public virtual bool ClipToClientRect
        {
            get
            {
                return this.mClipToClientRect;
            }
            set
            {
                if ( this.mClipToClientRect != value )
                {
                    this.mClipToClientRect = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the visual clip region.
        /// </summary>
        public Rectangles VisualClip
        {
            get
            {
                return this.mVisualClip;
            }
        }

        /// <summary>
        /// Gets the client region.
        /// </summary>
        public virtual Rectangle ClientRectangle
        {
            get
            {
                return this.mSlot.Size;
            }
        }

        /// <summary>
        /// Gets the visual parent.
        /// </summary>
        public AVisual VisualParent
        {
            get
            {
                return this.mVisualParent;
            }
            internal set
            {
                if ( this.mVisualParent != value )
                {
                    PropertyValueChangedEventArgs lArgs = new PropertyValueChangedEventArgs( this.mVisualParent, value );
                    lock ( this )
                    {
                        this.mVisualParent = value;
                    }

                    this.OnVisualParentChanged( lArgs );
                    this.NotifyPropertyChanged( /*"VisualParent"*/ );
                }
            }
        }

        /// <summary>
        /// Gets the visual slot.
        /// </summary>
        public virtual Rectangle Slot
        {
            get
            {
                return this.mSlot;
            }
            internal set
            {
                this.mSlot = value;
            }
        }

        /// <summary>
        /// Gets or sets the last slot components to track changes and update others accordingly if needed.
        /// </summary>
        internal Rectangle LastSlots
        {
            get
            {
                return this.mLastSlots;
            }
            set
            {
                this.mLastSlots = value;
            }
        }

        /// <summary>
        /// Gets the visual children count.
        /// </summary>
        public virtual int VisualChildrenCount
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the visual presentation source.
        /// </summary>
        public PresentationSource PresentationSource
        {
            get
            {
                if ( this.mPresentationSource == null )
                {
                    // The root is linked to the source once added as root,
                    // so if noone gave the source to any visual, it attempts to get it
                    // by itself.
                    this.mPresentationSource = PresentationSource.FromVisual( this );
                }

                return this.mPresentationSource;
            }
            set
            {
                if ( this.mPresentationSource != value )
                {
                    this.mPresentationSource = value;
                    this.Initialize();
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AVisual"/> class.
        /// </summary>
        protected AVisual()
        {
            this.mId = sNextId++;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes visual specificities.
        /// </summary>
        protected virtual void Initialize()
        {
            // TO DO: Init stuff.
        }

        /// <summary>
        /// Gets the visual root visual element.
        /// </summary>
        /// <param name="pVisual"></param>
        /// <returns></returns>
        internal static AVisual GetRootVisual(AVisual pVisual)
        {
            AVisual lRootVisual = pVisual;
            AVisual lParentVisual;

            while ( lRootVisual != null && 
                   ((lParentVisual = VisualTreeHelper.GetParent( lRootVisual )) != null))
            {
                lRootVisual = lParentVisual;
            }


            return lRootVisual;
        }

        /// <summary>
        /// Renders the visual.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        internal void Render(Context pDrawingContext)
        {
            this.RenderContent( pDrawingContext );
        }

        /// <summary>
        /// Renders the visual content of the visual.
        /// </summary>
        /// <param name="pDrawingContext"></param>
        protected virtual void RenderContent(Context pDrawingContext)
        {
            // Override.
        }

        /// <summary>
        /// Checks whether the given visual is a child of this visual.
        /// </summary>
        /// <param name="pDescendant">The visual to check.</param>
        /// <returns>True if child, false otherwise.</returns>
        public bool IsAncestorOf(AVisual pDescendant)
        {
            return this.IsAncestorOfInternal( pDescendant );
        }

        /// <summary>
        /// Internal IsAncestorOf
        /// </summary>
        /// <param name="pDescendant">The visual to check.</param>
        /// <returns>True if child, false otherwise.</returns>
        protected virtual bool IsAncestorOfInternal(AVisual pDescendant)
        {
            return false;
        }

        /// <summary>
        /// Checks whether the given visual is an ancestor of this visual.
        /// </summary>
        /// <param name="pAncestor">The visual to check.</param>
        /// <returns>True if parent, false otherwise.</returns>
        public bool IsDescendantOf(AVisual pAncestor)
        {
            return this.IsDescendantOfInternal( pAncestor );
        }

        /// <summary>
        /// Internal IsDescendantOf
        /// </summary>
        /// <param name="pAncestor">The visual to check.</param>
        /// <returns>True if parent, false otherwise.</returns>
        protected virtual bool IsDescendantOfInternal(AVisual pAncestor)
        {
            return pAncestor.IsAncestorOf( this );
        }

        /// <summary>
        /// Gets the visual child at the given index if any.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        protected virtual AVisual GetVisualChild(int pIndex)
        {
            throw new ArgumentOutOfRangeException("pIndex", pIndex, "Invalid index as there is no child at all.");
        }

        /// <summary>
        /// Registers a new region to clip.
        /// </summary>
        /// <param name="pClip">The new clip region.</param>
        public virtual void RegisterClip(Rectangle pClip)
        {
            if ( this.mIsCacheEnabled && 
                 this.mIsDirty == false )
            {
                this.mVisualClip.AddRectangle( pClip + this.ClientRectangle.Position );
            }

            if ( this.mVisualParent != null )
            {
                this.mVisualParent.RegisterClip( pClip + this.mSlot.Position + this.ClientRectangle.Position );
            }
        }

        /// <summary>
        /// Gets the context coordinate for the given region.
        /// </summary>
        /// <param name="pRegion"></param>
        /// <returns></returns>
        public virtual Rectangle ContextCoordinates(Rectangle pRegion)
        {
            //if ( this.mVisualParent == null )
            //{
            //    return pRegion + this.mVisualParent.ClientRectangle.Position;
            //}

            return this.mVisualParent.IsCacheEnabled ?
                   pRegion + this.mVisualParent.ClientRectangle.Position :
                   this.mVisualParent.ContextCoordinates( pRegion );
        }

        /// <summary>
        /// Gets the screen coordinates for the given region.
        /// </summary>
        /// <param name="pRegion"></param>
        /// <returns></returns>
        public virtual Rectangle ScreenCoordinates(Rectangle pRegion)
        {
            return this.mVisualParent.ScreenCoordinates( pRegion ) + 
                   this.mVisualParent.Slot.Position +
                   this.mVisualParent.ClientRectangle.Position;
        }

        /// <summary>
        /// Notifies that a property has changed.
        /// </summary>
        /// <param name="pPropertyName">The changed property name.</param>
        protected void NotifyPropertyChanged([CallerMemberName]string pPropertyName = null)
        {
            if ( this.PropertyChanged != null )
            {
                this.PropertyChanged( this, new PropertyChangedEventArgs( pPropertyName ) );
            }
        }

        /// <summary>
        /// Delegate called on visual parent changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnVisualParentChanged(PropertyValueChangedEventArgs pEventArgs)
        {
            if ( this.ParentChanged != null )
            {
                this.ParentChanged.Raise( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on visual children changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnVisualChildrenChanged(PropertyValueChangedEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Clone the visual element.
        /// </summary>
        /// <returns>The clone.</returns>
        public object Clone()
        {
            AVisual lClone = this.InternalClone();
            lClone.mVisualClip = new Rectangles();
            lClone.mIsCacheEnabled = this.mIsCacheEnabled;
            lClone.mClipToClientRect = this.mClipToClientRect;
            return lClone;
        }

        /// <summary>
        /// Clone the visual element.
        /// </summary>
        /// <returns>The clone.</returns>
        protected abstract AVisual InternalClone();

        /// <summary>
        /// Helper method to provide access to AddVisualChild for the VisualCollection.
        /// </summary>
        internal void InternalAddVisualChild(AVisual pChild)
        {
            this.AddVisualChild( pChild );
        }

        /// <summary>
        /// Helper method to provide access to RemoveVisualChild for the VisualCollection.
        /// </summary>
        internal void InternalRemoveVisualChild(AVisual pChild)
        {
            this.RemoveVisualChild( pChild );
        }

        /// <summary>
        /// Adds a child 
        /// </summary>
        /// <param name="pChild"></param>
        protected void AddVisualChild(AVisual pChild)
        {
            if ( pChild == null )
            {
                return;
            }

            if ( pChild.VisualParent != null )
            {
                throw new ArgumentException( "Already has parent." );
            }

            pChild.VisualParent = this;

            this.OnVisualChildrenChanged( new PropertyValueChangedEventArgs( null, pChild ) );
            pChild.OnVisualParentChanged( new PropertyValueChangedEventArgs( null, this ) );
        }

        /// <summary>
        /// Removes a child.
        /// </summary>
        /// <param name="pChild"></param>
        protected void RemoveVisualChild(AVisual pChild)
        {
            if ( pChild == null || 
                 pChild.VisualParent == null )
            {
                return;
            }

            if ( pChild.VisualParent != this )
            {
                throw new ArgumentException( "Visual is not a child." );
            }

            pChild.VisualParent = null;

            pChild.OnVisualParentChanged( new PropertyValueChangedEventArgs( this, null ) );
            this.OnVisualChildrenChanged( new PropertyValueChangedEventArgs( pChild, null ) );
        }
        
        #endregion Methods
    }
}
