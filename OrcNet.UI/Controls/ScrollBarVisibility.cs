﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ScrollBarVisibility"/> enumeration
    /// </summary>
    public enum ScrollBarVisibility
    {
        /// <summary>
        /// No scrollbar at all.
        /// </summary>
        Disabled = 0,

        /// <summary>
        /// Scrollbar only if content goes beyond the client region.
        /// </summary>
        Auto,

        /// <summary>
        /// Scrollbar is never visible.
        /// </summary>
        Hidden,

        /// <summary>
        /// Scrollbar is always visible.
        /// </summary>
        Visible
    }
}
