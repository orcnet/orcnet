﻿using System;
using OrcNet.UI.Input;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ColorSelector"/> class.
    /// </summary>
	public class ColorSelector : Control
	{
        #region Constants

        /// <summary>
        /// 
        /// </summary>
        const double cDiv = 255.0;

        /// <summary>
        /// 
        /// </summary>
        const double cColDiv = 1.0 / cDiv;

        #endregion Constants

        #region Fields

        /// <summary>
        /// Stores the current mouse position.
        /// </summary>
        protected Point mMousePosition;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorSelector"/> class.
        /// </summary>
        public ColorSelector() : 
        base()
		{

		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on UIelement mouse moves.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseMove(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseMove( pSender, pEventArgs );

            if ( InputManager.Instance.Mouse.LeftButton == ButtonState.Released )
            {
                return;
            }

            this.UpdateMouseLocalPos( pEventArgs.Position );
        }

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            base.OnMouseDown( pSender, pEventArgs );

            if ( pEventArgs.Button == MouseButton.Left )
            {
                this.UpdateMouseLocalPos( pEventArgs.Position );
            }
        }
        
        /// <summary>
        /// Updates the mouse position in cache.
        /// </summary>
        /// <param name="pPosition"></param>
		protected virtual void UpdateMouseLocalPos(Point pPosition)
        {
            Rectangle lRectangle   = this.ScreenCoordinates( this.Slot );
			Rectangle lClientBound = this.ClientRectangle;
			this.mMousePosition = pPosition - lRectangle.Position;
            this.mMousePosition.X = Math.Max( lClientBound.X, mMousePosition.X);
            this.mMousePosition.X = Math.Min( lClientBound.Right, mMousePosition.X);
			this.mMousePosition.Y = Math.Max( lClientBound.Y, mMousePosition.Y);
            this.mMousePosition.Y = Math.Min( lClientBound.Bottom, mMousePosition.Y);
		}

        #endregion Methods
    }
}

