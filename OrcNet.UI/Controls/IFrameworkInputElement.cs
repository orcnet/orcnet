﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="IFrameworkInputElement"/> interface.
    /// </summary>
    public interface IFrameworkInputElement
    {
        #region Properties

        /// <summary>
        /// Gets or sets the framework element name.
        /// </summary>
        string Name
        {
            get;
            set;
        }

        #endregion Properties
    }
}
