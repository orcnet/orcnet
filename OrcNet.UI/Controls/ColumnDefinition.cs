﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ColumnDefinition"/> class.
    /// </summary>
    public class ColumnDefinition : IDefinition
    {
        #region Properties

        /// <summary>
        /// Gets the column index.
        /// </summary>
        public int Index
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the user size
        /// </summary>
        GridLength IDefinition.UserSize
        {
            get
            {
                return this.Width;
            }
        }

        /// <summary>
        /// Gets the user min size.
        /// </summary>
        double IDefinition.UserMinSize
        {
            get
            {
                return this.MinWidth;
            }
        }

        /// <summary>
        /// Gets the user max size.
        /// </summary>
        double IDefinition.UserMaxSize
        {
            get
            {
                return this.MaxWidth;
            }
        }
        
        /// <summary>
        /// Gets the real column width.
        /// </summary>
        public double ActualWidth
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets the column width.
        /// </summary>
        public GridLength Width
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the column max width.
        /// </summary>
        public double MaxWidth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the column min width.
        /// </summary>
        public double MinWidth
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnDefinition"/> class.
        /// </summary>
        public ColumnDefinition()
        {
            this.Index = 0;
            this.ActualWidth = 0.0;
            this.Width = new GridLength( 1.0, GridUnitType.Star );
            this.MinWidth = 0.0;
            this.MaxWidth = double.PositiveInfinity;
        }

        #endregion Constructor
    }
}
