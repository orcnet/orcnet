﻿
namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="FontStyle"/> class.
    /// </summary>
    public enum FontStyle
    {
        /// <summary>
        /// Normal style.
        /// </summary>
        Normal,

        /// <summary>
        /// Italic style.
        /// </summary>
        Italic,

        /// <summary>
        /// Oblique style.
        /// </summary>
        Oblique
    }
}
