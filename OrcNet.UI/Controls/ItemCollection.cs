﻿using OrcNet.UI.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ItemCollection"/> class.
    /// </summary>
    public class ItemCollection : CollectionView, IList
    {
        #region Fields

        /// <summary>
        /// Stores the empty instance of element.
        /// </summary>
        private static readonly List<object> sEmptyList = new List<object>();

        /// <summary>
        /// Stores the items source.
        /// </summary>
        private IEnumerable mItemsSource;

        /// <summary>
        /// Stores the collection owner.
        /// </summary>
        private WeakReference mOwner;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the parent model.
        /// </summary>
        internal AVisual ModelParent
        {
            get
            {
                return this.mOwner.Target as AVisual;
            }
        }

        /// <summary>
        /// Gets the parent model as framework element.
        /// </summary>
        internal FrameworkElement ModelParentFE
        {
            get
            {
                return ModelParent as FrameworkElement;
            }
        }

        /// <summary>
        /// Gets the items source.
        /// </summary>
        internal IEnumerable ItemsSource
        {
            get
            {
                return this.mItemsSource;
            }
        }

        /// <summary>
        /// Gets or sets the element at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        public object this[int pIndex]
        {
            get
            {
                return this.GetItemAt( pIndex );
            }

            set
            {
                if ( pIndex < 0 ||
                     pIndex >= this.Count )
                {
                    return;
                }

                object lOldElement = this.InternalSourceCollection[ pIndex ];
                this.InternalSourceCollection[ pIndex ] = value;

                this.NotifyCollectionChanged( new List<object>() { value }, pIndex, new List<object>() { lOldElement }, pIndex, NotifyCollectionChangedAction.Replace );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is of fixed size or not.
        /// </summary>
        bool IList.IsFixedSize
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is read only or not.
        /// </summary>
        bool IList.IsReadOnly
        {
            get
            {
                return this.InternalSourceCollection.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is synchronized or not.
        /// </summary>
        bool ICollection.IsSynchronized
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the sync root object
        /// </summary>
        object ICollection.SyncRoot
        {
            get
            {
                return this.SyncRoot;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemCollection"/> class.
        /// </summary>
        /// <param name="pOwner">The owner object.</param>
        public ItemCollection(FrameworkElement pOwner)
        {
            this.mOwner = new WeakReference( pOwner );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Set a logical parent to the given item.
        /// </summary>
        /// <param name="pItem"></param>
        private void SetModelParent(object pItem)
        {
            if ( this.ModelParentFE != null && 
                 pItem is FrameworkElement )
            {
                LogicalTreeHelper.AddLogicalChild( this.ModelParentFE, pItem );
            }
        }

        /// <summary>
        /// Removes the logical parent to the given item.
        /// </summary>
        /// <param name="pItem"></param>
        private void ClearModelParent(object pItem)
        {
            if ( this.ModelParentFE != null && 
                 pItem is FrameworkElement )
            {
                LogicalTreeHelper.RemoveLogicalChild( this.ModelParentFE, pItem );
            }
        }

        /// <summary>
        /// Sets the collection into items source mode.
        /// </summary>
        /// <param name="pValue"></param>
        internal void SetItemsSource(IEnumerable pValue)
        {
            this.mItemsSource = pValue;
        }

        /// <summary>
        /// Adds a new element to the collection.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns>The new element index if added, -1 otherwise.</returns>
        public int Add(object pElement)
        {
            lock ( this.SyncRoot )
            {
                this.InternalSourceCollection.Add( pElement );
            }
                
            int lIndex = this.Count - 1;

            // Set the parent of this item.
            this.SetModelParent( pElement );

            this.NotifyCollectionChanged( new List<object>() { pElement }, lIndex, sEmptyList, -1, NotifyCollectionChangedAction.Add );

            return lIndex;
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            lock ( this.SyncRoot )
            {
                try
                {
                    foreach ( object lChild in this.InternalSourceCollection )
                    {
                        this.ClearModelParent( lChild );
                    }
                }
                finally
                {
                    this.InternalSourceCollection.Clear();
                }
            }

            this.NotifyCollectionChanged( sEmptyList, -1, sEmptyList, -1, NotifyCollectionChangedAction.Reset );
        }
        
        /// <summary>
        /// Copies the collection into an array to the given index.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pIndex"></param>
        public void CopyTo(Array pArray, int pIndex)
        {
            if ( pArray == null)
            {
                throw new ArgumentNullException("pArray");
            }

            if ( pArray.Rank != 1)
            {
                throw new ArgumentException("Bad rank");
            }

            if ((pIndex < 0) ||
                (pArray.Length - pIndex < this.Count))
            {
                throw new ArgumentOutOfRangeException("index");
            }

            // System.Array does not have a CopyTo method that takes a count. Therefore
            // the loop is programmed here out.
            for ( int lCurr = 0; lCurr < this.Count; lCurr++ )
            {
                pArray.SetValue( this.InternalSourceCollection[ lCurr ], lCurr + pIndex );
            }
        }
        
        /// <summary>
        /// Insert the given element to the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pElement"></param>
        public void Insert(int pIndex, object pElement)
        {
            lock ( this.SyncRoot )
            {
                this.InternalSourceCollection.Insert( pIndex, pElement );
            }

            this.SetModelParent( pElement );

            this.NotifyCollectionChanged( new List<object>() { pElement }, pIndex, sEmptyList, -1, NotifyCollectionChangedAction.Add );
        }

        /// <summary>
        /// Removes the given element from the collection.
        /// </summary>
        /// <param name="pElement"></param>
        public void Remove(object pElement)
        {
            int lIndex = this.IndexOf( pElement );

            this.Remove( pElement, lIndex );
        }

        /// <summary>
        /// Removes the element at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        public void RemoveAt(int pIndex)
        {
            if ( pIndex < 0 ||
                 pIndex >= this.Count )
            {
                return;
            }

            object lElement = this.InternalSourceCollection[ pIndex ];

            this.Remove( lElement, pIndex );
        }

        /// <summary>
        /// Removes an element.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pIndex"></param>
        private void Remove(object pElement, int pIndex)
        {
            lock ( this.SyncRoot )
            {
                this.InternalSourceCollection.Remove( pElement );
            }

            this.ClearModelParent( pElement );
            
            this.NotifyCollectionChanged( sEmptyList, -1, new List<object>() { pElement }, pIndex, NotifyCollectionChangedAction.Remove );
        }

        #endregion Methods
    }
}
