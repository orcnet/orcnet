﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="IWindow"/> interface.
    /// </summary>
    public interface IWindow
    {
        #region Properties

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the window height.
        /// </summary>
        double Height
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the window width.
        /// </summary>
        double Width
        {
            get;
            set;
        }
        
        #endregion Properties
    }
}
