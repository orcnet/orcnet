﻿using System;
using System.Globalization;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="NumericUpDown"/> class.
    /// </summary>
	public class NumericUpDown : Control
    {
        #region Fields

        /// <summary>
        /// Stores the constant Textbox template name.
        /// </summary>
        private const string TextBoxTemplateName = "TextBoxValue";

        /// <summary>
        /// Stores the constant Up button template name.
        /// </summary>
        private const string UpButtonTemplateName = "UpButton";

        /// <summary>
        /// Stores the constant Up button template name.
        /// </summary>
        private const string DownButtonTemplateName = "DownButton";

        /// <summary>
        /// Stores the value.
        /// </summary>
        private double mValue;

        /// <summary>
        /// Stores the textbox.
        /// </summary>
        private TextBox mTextBox;

        /// <summary>
        /// Stores the Up button increasing the spinner value.
        /// </summary>
        private Button mUpButton;

        /// <summary>
        /// Stores the Up button decreasing the spinner value.
        /// </summary>
        private Button mDownButton;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public double Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                if ( this.mValue == value )
                {
                    return;
                }

                double lOldValue = this.mValue;

                this.mValue = value;
                this.NotifyPropertyChanged();

                this.OnValueChanged( lOldValue, this.mValue );

                this.InvalidateVisual();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericUpDown"/> class.
        /// </summary>
        public NumericUpDown()
        {
            this.mValue = 0.0;
        }
        
        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if ( this.mUpButton != null )
            {
                this.mUpButton.Click -= this.OnUpButtonClick;
            }

            if ( this.mDownButton != null )
            {
                this.mDownButton.Click -= this.OnDownButtonClick;
            }

            if ( this.mTextBox != null )
            {
                this.mTextBox.LostFocus -= this.OnTextBoxLostFocus;
            }

            this.mUpButton   = this.GetTemplateChild( UpButtonTemplateName ) as Button;
            this.mDownButton = this.GetTemplateChild( DownButtonTemplateName ) as Button;
            this.mTextBox    = this.GetTemplateChild( TextBoxTemplateName ) as TextBox;

            if ( this.mUpButton != null )
            {
                this.mUpButton.Click += this.OnUpButtonClick;
                this.mUpButton.IsFocusable = false;
            }

            if ( this.mDownButton != null )
            {
                this.mDownButton.Click += this.OnDownButtonClick;
                this.mDownButton.IsFocusable = false;
            }

            if ( this.mTextBox != null )
            {
                this.mTextBox.LostFocus += this.OnTextBoxLostFocus;
            }
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            if ( this.mTextBox != null )
            {
                this.mTextBox.Paint( ref pDrawingContext );
            }

            if ( this.mUpButton != null )
            {
                this.mUpButton.Paint( ref pDrawingContext );
            }

            if ( this.mDownButton != null )
            {
                this.mDownButton.Paint( ref pDrawingContext );
            }
        }

        /// <summary>
        /// Delegate called on value changes.
        /// </summary>
        /// <param name="pOldValue"></param>
        /// <param name="pNewValue"></param>
        private void OnValueChanged(double pOldValue, double pNewValue)
        {
            this.CoerceValue( pNewValue );
        }

        /// <summary>
        /// Delegate called on up button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnUpButtonClick(object pSender, EventArgs pEventArgs)
        {
            this.Value++;
        }

        /// <summary>
        /// Delegate called on down button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnDownButtonClick(object pSender, EventArgs pEventArgs)
        {
            this.Value--;
        }

        /// <summary>
        /// Delegate called on text box lost focus.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnTextBoxLostFocus(object pSender, EventArgs pEventArgs)
        {
            if ( this.mTextBox != null )
            {
                this.Value = ParseValue( this.mTextBox.Text );
            }
        }

        /// <summary>
        /// Parses the text box value.
        /// </summary>
        /// <param name="pSource"></param>
        /// <returns></returns>
        private double ParseValue(string pSource)
        {
            double lValue;
            double.TryParse( pSource, out lValue );

            return lValue;
        }

        /// <summary>
        /// Coerces the value.
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        private double CoerceValue(double pValue)
        {
            this.mTextBox.Text = pValue.ToString( CultureInfo.CurrentCulture );

            return pValue;
        }

        #endregion Methods
    }
}

