﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Button"/> class.
    /// </summary>
    public class Button : AButtonBase
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the button is the default one or not.
        /// </summary>
        private bool mIsDefault;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the button is the default one or not.
        /// </summary>
        public bool IsDefault
        {
            get
            {
                return this.mIsDefault;
            }
            set
            {
                if ( this.mIsDefault == value )
                {
                    return;
                }

                this.mIsDefault = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Button"/> class.
        /// </summary>
        public Button() : 
        base()
        {
            this.mIsDefault = false;
        }

        #endregion Constructor
	}
}
