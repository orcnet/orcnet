﻿using Cairo;
using OrcNet.Core.Datastructures;
using OrcNet.UI.Enumerators;
using OrcNet.UI.Helpers;
using OrcNet.UI.Ximl;
using System.Collections;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ContentControl"/> class.
    /// 
    /// NOTE: Can only contain one piece of content as child.
    /// </summary>
    public class ContentControl : Control, IAddChild
    {
        #region Fields
        
        /// <summary>
        /// Stores the content of the control.
        /// </summary>
        private object mContent;

        /// <summary>
        /// Stores the content template to apply to display the content of this control.
        /// </summary>
        private DataTemplate mContentTemplate;

        /// <summary>
        /// Stores the content template selector allowing users to provides a custom style selection to the content of this control.
        /// </summary>
        private DataTemplateSelector mContentTemplateSelector;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the content of the control.
        /// </summary>
        public object Content
        {
            get
            {
                return this.mContent;
            }
            set
            {
                if ( this.mContent != value )
                {
                    object lPrevious = this.mContent;

                    this.mContent = value;

                    this.OnContentChanged( lPrevious, this.mContent );

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the content template to apply to display the content of this control.
        /// </summary>
        public DataTemplate ContentTemplate
        {
            get
            {
                return this.mContentTemplate;
            }
            set
            {
                if ( this.mContentTemplate != value )
                {
                    DataTemplate lPrevious = this.mContentTemplate;

                    this.mContentTemplate = value;

                    this.OnContentTemplateChanged( lPrevious, this.mContentTemplate );

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the content template selector allowing users to provides a custom style selection to the content of this control.
        /// </summary>
        public DataTemplateSelector ContentTemplateSelector
        {
            get
            {
                return this.mContentTemplateSelector;
            }
            set
            {
                if ( this.mContentTemplateSelector != value )
                {
                    DataTemplateSelector lPrevious = this.mContentTemplateSelector;

                    this.mContentTemplateSelector = value;

                    this.OnContentTemplateSelectorChanged( lPrevious, this.mContentTemplateSelector );

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the control has a content or not.
        /// </summary>
        public bool HasContent
        {
            get
            {
                return this.mContent != null ? true : false;
            }
        }

        /// <summary>
        /// Gets the set of logical children if any.
        /// </summary>
        protected internal override IEnumerator LogicalChildren
        {
            get
            {
                object lContent = this.Content;

                if ( lContent == null )
                {
                    return EmptyEnumerator<object>.Empty;
                }

                // If the current ContentControl is in a Template.VisualTree and is meant to host
                // the content for the container then that content shows up as the logical child
                // for the container and not for the current ContentControl.
                AVisual lTemplatedParent = this.TemplatedParent;
                if ( lTemplatedParent != null )
                {
                    AVisual lCast = lContent as AVisual;
                    if ( lCast != null )
                    {
                        AVisual lLogicalParent = LogicalTreeHelper.GetParent( lCast );
                        if ( lLogicalParent != null &&
                             lLogicalParent != this )
                        {
                            return EmptyEnumerator<object>.Empty;
                        }
                    }
                }

                return new ContentModelTreeEnumerator( this, lContent );
            }
        }

        /// <summary>
        /// Gets the visual children count.
        /// </summary>
        public override int VisualChildrenCount
        {
            get
            {
                return this.Content == null ? 0 : 1;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentControl"/> class.
        /// </summary>
        public ContentControl()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new child to a container.
        /// </summary>
        /// <param name="pChild"></param>
        void IAddChild.AddChild(object pChild)
        {
            this.AddChild( pChild );
        }

        /// <summary>
        /// Adds a new child to a container.
        /// </summary>
        /// <param name="pChild"></param>
        protected virtual void AddChild(object pChild)
        {
            if ( this.Content == null ||
                 pChild == null )
            {
                this.Content = pChild;
            }
        }

        /// <summary>
        /// Finds the visual having the given name
        /// </summary>
        /// <param name="pName">The name to look for.</param>
        /// <returns></returns>
        public override AVisual FindByName(string pName)
		{
            AVisual lVisual = base.FindByName( pName );
            if ( lVisual != null )
            {
                return lVisual;
            }

			return this.mContent is FrameworkElement  ? (this.mContent as FrameworkElement).FindByName( pName ) : null;
		}

        /// <summary>
        /// Internal IsAncestorOf
        /// </summary>
        /// <param name="pDescendant">The visual to check.</param>
        /// <returns>True if child, false otherwise.</returns>
        protected override bool IsAncestorOfInternal(AVisual pDescendant)
        {
            return this.mContent == pDescendant ? true :
                   this.mContent is FrameworkElement ? (this.mContent as FrameworkElement).IsAncestorOf( pDescendant ) : false;
        }
        
        /// <summary>
        /// Delegate called on Data context changed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        public override void OnDataContextChanged(object pSender, PropertyValueChangedEventArgs pEventArgs)
        {
            base.OnDataContextChanged( pSender, pEventArgs );

            if ( this.mContent is FrameworkElement )
            {
                FrameworkElement lChild = this.mContent as FrameworkElement;
                if ( lChild.IsLocalDataContextNull & lChild.IsLocalLogicalParentNull )
                {
                    lChild.OnDataContextChanged( pSender, pEventArgs );
                }
            }
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            FrameworkElement lChild = this.mContent as FrameworkElement;
            if ( lChild != null )
            {
                //force sizing to fit if sizing on children and child has stretched size
                switch ( pLayoutType )
                {
                    case LayoutingType.Width:
                        {
                            if ( this.Width == cFitSizeValue )
                            {
                                lChild.Width = cFitSizeValue;
                            }
                        }
                        break;
                    case LayoutingType.Height:
                        {
                            if ( this.Height == cFitSizeValue )
                            {
                                lChild.Height = cFitSizeValue;
                            }
                        }
                        break;
                }
            }

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            if ( this.Template == null )
            {
                // If no template definition, draw the background by calling
                // the ancestor method.
                base.OnRender( pDrawingContext );
            }

            pDrawingContext.Save();

            if ( this.ClipToClientRect )
            {
                // Clip to client zone
                CairoHelpers.CairoRectangle( pDrawingContext, this.ClientRectangle, -1 );
                pDrawingContext.Clip();
            }

            UIElement lChildAsElement = this.mContent as UIElement;
            if ( lChildAsElement != null )
            {
                if ( lChildAsElement.IsVisible )
                {
                    lChildAsElement.Paint( ref pDrawingContext );
                }
            }

            pDrawingContext.Restore();
        }

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected internal override void OnLayoutChanges(LayoutingType pLayoutType)
        {
            base.OnLayoutChanges( pLayoutType );

            FrameworkElement lChild = this.mContent as FrameworkElement;
            if ( lChild == null )
            {
                return;
            }

            LayoutingType lChildLayoutType = LayoutingType.None;

            if ( pLayoutType == LayoutingType.Width )
            {
                if ( lChild.Left == 0 )
                {
                    lChildLayoutType |= LayoutingType.X;
                }
                else
                {
                    lChildLayoutType |= LayoutingType.Width;
                    if ( lChild.Width < 100 && 
                         lChild.Left == 0 )
                    {
                        lChildLayoutType |= LayoutingType.X;
                    }
                }
            }
            else if ( pLayoutType == LayoutingType.Height )
            {
                if ( lChild.Top == 0 )
                {
                    lChildLayoutType |= LayoutingType.Y;
                }
                else
                {
                    lChildLayoutType |= LayoutingType.Height;
                    if ( lChild.Height < 100 && 
                         lChild.Top == 0 )
                    {
                        lChildLayoutType |= LayoutingType.Y;
                    }
                }
            }

            if ( lChildLayoutType == LayoutingType.None )
            {
                return;
            }

            lChild.RegisterForLayouting( lChildLayoutType );
        }
        
        /// <summary>
        /// Delegate called on content changes.
        /// </summary>
        /// <param name="pOldContent">The old one.</param>
        /// <param name="pNewContent">The new one.</param>
        protected virtual void OnContentChanged(object pOldContent, object pNewContent)
        {
            FrameworkElement lOldElement = pOldContent as FrameworkElement;
            if ( lOldElement != null )
            {
                this.RemoveLogicalChild( pOldContent );

                this.mContentSize = new Size( 0, 0 );
                lOldElement.LayoutUpdated -= this.OnChildLayoutChanges;
                lOldElement.VisualParent  = null;
                this.InvalidateArrange();
            }

            FrameworkElement lNewElement = pNewContent as FrameworkElement;
            if ( lNewElement != null )
            {
                AVisual lLogicalParent = LogicalTreeHelper.GetParent( lNewElement );
                if ( lLogicalParent != null )
                {
                    // If the new content was previously hooked up to the logical
                    // tree then we sever it from the old parent. 
                    LogicalTreeHelper.RemoveLogicalChild( lLogicalParent, pNewContent );
                }

                this.AddLogicalChild( pNewContent );

                lNewElement.VisualParent  = this;
                lNewElement.LayoutUpdated += this.OnChildLayoutChanges;
                this.mContentSize = lNewElement.Slot.Size;

                lNewElement.State = LayoutingType.None;
                lNewElement.RegisterForLayouting( LayoutingType.Sizing );
            }

            this.NotifyPropertyChanged( "HasContent" );
        }

        /// <summary>
        /// Delegate called on content template changes.
        /// </summary>
        /// <param name="pOldContent">The old one.</param>
        /// <param name="pNewContent">The new one.</param>
        protected virtual void OnContentTemplateChanged(DataTemplate pOldContent, DataTemplate pNewContent)
        {
            
        }

        /// <summary>
        /// Delegate called on content template changes.
        /// </summary>
        /// <param name="pOldContent">The old one.</param>
        /// <param name="pNewContent">The new one.</param>
        protected virtual void OnContentTemplateSelectorChanged(DataTemplateSelector pOldContent, DataTemplateSelector pNewContent)
        {
            
        }

        /// <summary>
        /// Delegate called on child layout changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
		protected virtual void OnChildLayoutChanges(object pSender, LayoutingEventArgs pEventArgs)
		{			
			UIElement lAsElement = pSender as UIElement;

			if ( pEventArgs.LayoutType == LayoutingType.Width )
            {
                if ( this.HorizontalAlignment == HorizontalAlignment.Stretch )
                {
                    return;
                }

				this.mContentSize.Width = lAsElement.Slot.Width;
				this.RegisterForLayouting( LayoutingType.Width );
			}
            else if ( pEventArgs.LayoutType == LayoutingType.Height )
            {
                if ( this.VerticalAlignment == VerticalAlignment.Stretch )
                {
                    return;
                }

				this.mContentSize.Height = lAsElement.Slot.Height;
				this.RegisterForLayouting( LayoutingType.Height );
			}
		}

        /// <summary>
        /// Internal drawing cache update.
        /// </summary>
        /// <param name="pElementRegion">The element drawing region.</param>
        /// <param name="pSurface">The drawing surface.</param>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void InternalUpdateCache(ref Rectangle pElementRegion, ImageSurface pSurface, Context pDrawingContext)
        {
            Context lTempContext = new Context( pSurface );

            if ( this.VisualClip.Count > 0 )
            {
                this.VisualClip.ClearAndClip( lTempContext );

                this.OnRender( lTempContext );
            }

            lTempContext.Dispose();
        }
        
        /// <summary>
        /// Delegate called on mouse hover.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseHover(MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseHover( pEventArgs );

            UIElement lChildAsEleemnt = this.mContent as UIElement;
            if ( lChildAsEleemnt != null )
            {
                if ( lChildAsEleemnt.IsMouseIn( pEventArgs.Position ) )
                {
                    lChildAsEleemnt.OnMouseHover( pEventArgs );
                }
            }
        }
        
        #endregion Methods
    }
}
