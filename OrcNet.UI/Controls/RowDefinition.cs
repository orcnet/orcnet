﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="RowDefinition"/> class.
    /// </summary>
    public class RowDefinition : IDefinition
    {
        #region Properties

        /// <summary>
        /// Gets the row index.
        /// </summary>
        public int Index
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the user size
        /// </summary>
        GridLength IDefinition.UserSize
        {
            get
            {
                return this.Height;
            }
        }

        /// <summary>
        /// Gets the user min size.
        /// </summary>
        double IDefinition.UserMinSize
        {
            get
            {
                return this.MinHeight;
            }
        }

        /// <summary>
        /// Gets the user max size.
        /// </summary>
        double IDefinition.UserMaxSize
        {
            get
            {
                return this.MaxHeight;
            }
        }

        /// <summary>
        /// Gets the real row height.
        /// </summary>
        public double ActualHeight
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets the row width.
        /// </summary>
        public GridLength Height
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the row max width.
        /// </summary>
        public double MaxHeight
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the row min width.
        /// </summary>
        public double MinHeight
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RowDefinition"/> class.
        /// </summary>
        public RowDefinition()
        {
            this.Index = 0;
            this.Height = new GridLength( 1.0, GridUnitType.Star );
            this.ActualHeight = 0.0;
            this.MinHeight = 0.0;
            this.MaxHeight = double.PositiveInfinity;
        }

        #endregion Constructor
    }
}
