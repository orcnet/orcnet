﻿using Cairo;
using OrcNet.UI.Input;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="UIElement"/> class.
    /// </summary>
    public class UIElement : AVisual
    {
        #region Constants

        /// <summary>
        /// Stores the constant that disable caching for any element that would reach this size.
        /// </summary>
        public const int MaxCacheSize = 2048;

        #endregion Constants

        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the measure is dirty or not.
        /// </summary>
        private bool mIsMeasureDirty;

        /// <summary>
		/// Stores the computed size on each child layout changes.
		/// In stacking widget, it is used to compute the remaining space for the stretched
		/// element inside the stack, which is never added to the contentSize, instead, its size
		/// is deducted from (VisualParent.ClientRectangle - ContentSize)
		/// </summary>
		protected Size mContentSize;

        /// <summary>
        /// Stores the desired size.
        /// </summary>
        private Size mDesiredSize;

        /// <summary>
        /// Keeps the last painted slot on screen to clear traces if moved or resized.
        /// </summary>
        protected Rectangle mLastPaintedSlots;

        /// <summary>
        /// Stores the flag indicating whether the element drawing region must be cleared or not.
        /// </summary>
        protected bool mClearElementDrawingRegion;

        /// <summary>
        /// Stores the drawing Cache, if null, a redraw is done, cached or not
        /// </summary>
		private byte[] mRawDrawingSurface;

        /// <summary>
        /// Stores the element layout state.
        /// </summary>
        private LayoutingType mState;

        /// <summary>
        /// Stores the flag indicating whether the element is focusable or not.
        /// </summary>
        private bool mIsFocusable;

        /// <summary>
        /// Stores the flag indicating whether the element is active or not.
        /// </summary>
        private bool mIsActive;

        /// <summary>
        /// Stores the flag indicating whether the element is enabled or not.
        /// </summary>
        private bool mIsEnabled;

        /// <summary>
        /// Stores the flag indicating whether the element is focused or not.
        /// </summary>
        private bool mIsFocused;

        /// <summary>
        /// Stores the flag indicating whether the element allow mouse repeat or not.
        /// </summary>
        private bool mAllowMouseRepeat;

        /// <summary>
        /// Stores the flag indicating whether the element is visible or not.
        /// </summary>
        protected bool mIsVisible;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on mouse wheel changes.
        /// </summary>
        public event EventHandler<MouseWheelEventArgs> MouseWheelChanged;

        /// <summary>
        /// Event fired on mouse up changes.
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseUp;

        /// <summary>
        /// Event fired on mouse down changes.
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseDown;

        /// <summary>
        /// Event fired on mouse click changes.
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseClick;
        
        /// <summary>
        /// Event fired on mouse moves.
        /// </summary>
        public event EventHandler<MouseMoveEventArgs> MouseMove;

        /// <summary>
        /// Event fired on mouse entered in the element region.
        /// </summary>
        public event EventHandler<MouseMoveEventArgs> MouseEnter;

        /// <summary>
        /// Event fired on mouse left the element region.
        /// </summary>
        public event EventHandler<MouseMoveEventArgs> MouseLeave;

        /// <summary>
        /// Event fired on key down changes.
        /// </summary>
        public event EventHandler<KeyboardKeyEventArgs> KeyDown;

        /// <summary>
        /// Event fired on key up changes.
        /// </summary>
        public event EventHandler<KeyboardKeyEventArgs> KeyUp;

        /// <summary>
        /// Event fired on any key press.
        /// </summary>
        public event EventHandler<KeyPressEventArgs> KeyPress;

        /// <summary>
        /// Event fired on element got focus.
        /// </summary>
        public event EventHandler GotFocus;

        /// <summary>
        /// Event fired on element lost focus.
        /// </summary>
        public event EventHandler LostFocus;

        /// <summary>
        /// Event fired on element enable state changes.
        /// </summary>
        public event EventHandler<bool> IsEnableChanged;

        /// <summary>
        /// Event fired on layout updated.
        /// </summary>
        public event EventHandler<LayoutingEventArgs> LayoutUpdated;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the arrange constraint set by the LayoutManager.
        /// </summary>
        internal LayoutingType ArrangeConstraint
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the flag indicating whether the measure is valid or not.
        /// </summary>
        public bool IsMeasureValid
        {
            get
            {
                return this.IsMeasureDirty == false;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the measure is dirty or not.
        /// </summary>
        internal bool IsMeasureDirty
        {
            get
            {
                return this.mIsMeasureDirty;
            }
            set
            {
                if ( this.mIsMeasureDirty == value )
                {
                    return;
                }

                this.mIsMeasureDirty = value;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse if over the element or not.
        /// </summary>
        public bool IsMouseOver
        {
            get
            {
                return InputManager.Instance.HoverElement == this;
            }
        }

        /// <summary>
        /// Gets the element layout state.
        /// </summary>
        public LayoutingType State
        {
            get
            {
                return this.mState;
            }
            protected internal set
            {
                this.mState = value;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the element is active or not.
        /// </summary>
        public bool IsActive
        {
            get
            {
                return this.mIsActive;
            }
            protected internal set
            {
                if ( this.mIsActive != value )
                {
                    this.mIsActive = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the element is enabled or not.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return this.mIsEnabled && this.IsEnabledCore;
            }
            set
            {
                if ( this.mIsEnabled != value )
                {
                    this.mIsEnabled = value;

                    this.OnIsEnableChanged( this, this.mIsEnabled );

                    this.NotifyPropertyChanged();

                    this.InvalidateVisual();
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the element is enabled or not.
        /// </summary>
        protected virtual bool IsEnabledCore
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the element is focused or not.
        /// </summary>
        public virtual bool IsFocused
        {
            get
            {
                return this.mIsFocused;
            }
            protected internal set
            {
                if ( this.mIsFocused != value )
                {
                    this.mIsFocused = value;

                    if ( this.mIsFocused )
                    {
                        this.OnGotFocus( this, null );
                    }
                    else
                    {
                        this.OnLostFocus( this, null );
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the element is focusable or not.
        /// </summary>
        public bool IsFocusable
        {
            get
            {
                return this.mIsFocusable;
            }
            set
            {
                if ( this.mIsFocusable != value )
                {
                    this.mIsFocusable = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the element allow mouse repeat or not.
        /// </summary>
        public bool AllowMouseRepeat
        {
            get
            {
                return this.mAllowMouseRepeat;
            }
            set
            {
                if ( this.mAllowMouseRepeat != value )
                {
                    this.mAllowMouseRepeat = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the element is visible or not.
        /// </summary>
        public virtual bool IsVisible
        {
            get
            {
                return this.mIsVisible;
            }
            set
            {
                if ( this.mIsVisible != value )
                {
                    this.mIsVisible = value;

                    // Register for a layout pass.
                    if ( this.mIsVisible )
                    {
                        this.RegisterForLayouting( LayoutingType.Sizing );
                    }
                    // Release the UI space it takes.
                    else
                    {
                        this.mSlot.Width = 0;
                        if ( this.LayoutUpdated != null )
                        {
                            this.LayoutUpdated.Raise( this, new LayoutingEventArgs(LayoutingType.Width) );
                        }

                        this.mSlot.Height = 0;
                        if ( this.LayoutUpdated != null )
                        {
                            this.LayoutUpdated.Raise( this, new LayoutingEventArgs(LayoutingType.Height) );
                        }
                        
                        if ( this.VisualParent != null )
                        {
                            ContextLayoutManager.From( this.Dispatcher ).RegisterLayouted( this );
                        }

                        this.mLastSlots.Width = this.mLastSlots.Height = 0;
                    }

                    // Trigger a mouse to handle possible hover changes
                    InputManager.Instance.ProcessMouseMove( InputManager.Instance.Mouse.X, InputManager.Instance.Mouse.Y);

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether or not to clip to the client rectangle.
        /// </summary>
        public override bool ClipToClientRect
        {
            get
            {
                return base.ClipToClientRect;
            }

            set
            {
                base.ClipToClientRect = value;
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets the size of the element computed during the Measure process.
        /// </summary>
        public Size DesiredSize
        {
            get
            {
                if ( this.IsVisible == false )
                {
                    return new Size( 0 );
                }
                else
                {
                    return this.mDesiredSize;
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UIElement"/> class.
        /// </summary>
        public UIElement()
        {
            this.mIsFocusable = false;
            this.mIsFocused   = false;
            this.mClearElementDrawingRegion = false;
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Focus the element.
        /// </summary>
        /// <returns></returns>
        public bool Focus()
        {
            if ( InputManager.Instance.FocusElement != this )
            {
                InputManager.Instance.FocusElement = this;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Delegate called on presentation source changes.
        /// </summary>
        /// <param name="pHasSource"></param>
        internal virtual void OnPresentationSourceChanged(bool pHasSource)
        {
            if ( pHasSource == false && 
                 InputManager.Instance.FocusElement == this )
            {
                InputManager.Instance.FocusElement = null;
            }
        }

        /// <summary>
        /// Delegate called on element got focus event.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected virtual void OnGotFocus(object pSender, EventArgs pEventArgs)
        {
            if ( this.GotFocus != null )
            {
                this.GotFocus( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on element lost focus event.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected virtual void OnLostFocus(object pSender, EventArgs pEventArgs)
        {
            if ( this.LostFocus != null )
            {
                this.LostFocus( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on element enable state changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pIsEnabled">The flag indicating whether the element is enabled or not.</param>
        public virtual void OnIsEnableChanged(object pSender, bool pIsEnabled)
        {
            if ( this.IsEnableChanged != null )
            {
                this.IsEnableChanged( pSender, pIsEnabled );
            }
        }

        /// <summary>
        /// Delegate called on key pressed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal virtual void OnKeyDown(object pSender, KeyboardKeyEventArgs pEventArgs)
        {
            if ( this.KeyDown != null )
            {
                this.KeyDown( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on key released.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal virtual void OnKeyUp(object pSender, KeyboardKeyEventArgs pEventArgs)
        {
            if ( this.KeyUp != null )
            {
                this.KeyUp( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on any key pressed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal virtual void OnKeyPress(object pSender, KeyPressEventArgs pEventArgs)
        {
            if ( this.KeyPress != null )
            {
                this.KeyPress( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Checks whether the given mouse point is inside this element or not.
        /// </summary>
        /// <param name="pScreenPoint">The screen point to check.</param>
        /// <returns>True if contained, false otherwise.</returns>
        protected internal bool IsMouseIn(Point pScreenPoint)
        {
            try
            {
                if (!(this.mIsVisible & this.mIsEnabled))
                {
                    return false;
                }

                if ( this.ScreenCoordinates( this.mSlot ).ContainsOrIsEqual( pScreenPoint ) )
                {
                    return this.InternalIsMouseIn( pScreenPoint );
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        /// <summary>
        /// Internal method allowing specific check to know whether the given point is inside this element or not.
        /// </summary>
        /// <param name="pScreenPoint">The point to check.</param>
        /// <returns>True if contained, false otherwise.</returns>
        protected virtual bool InternalIsMouseIn(Point pScreenPoint)
        {
            ScrollViewer lScroller = this.VisualParent as ScrollViewer;
            if ( lScroller == null )
            {
                if ( this.VisualParent is UIElement )
                {
                    return (this.VisualParent as UIElement).IsMouseIn( pScreenPoint );
                }
                else
                {
                    return true;
                }
            }

            return lScroller.IsMouseIn( lScroller.SavedMousePos );
        }

        /// <summary>
        /// Delegate called on mouse hover.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseHover(MouseMoveEventArgs pEventArgs)
        {
            if ( InputManager.Instance.HoverElement != this )
            {
                InputManager.Instance.HoverElement = this;

                this.OnMouseEnter( this, pEventArgs );
            }

            this.OnMouseMove( this, pEventArgs );//without this, window border doesn't work, should be removed
        }

        /// <summary>
        /// Delegate called on UIElement mouse enter.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseEnter(object pSender, MouseMoveEventArgs pEventArgs)
        {
            if ( this.MouseEnter != null )
            {
                this.MouseEnter.Raise( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on UIElement mouse leave.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseLeave(object pSender, MouseMoveEventArgs pEventArgs)
        {
            if ( this.MouseLeave != null )
            {
                this.MouseLeave.Raise( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on UIelement mouse moves.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseMove(object pSender, MouseMoveEventArgs pEventArgs)
        {
            UIElement lParent = this.VisualParent as UIElement;
            if ( lParent != null )
            {
                lParent.OnMouseMove( pSender, pEventArgs);
            }

            if ( this.MouseMove != null )
            {
                this.MouseMove.Raise( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( InputManager.Instance.ActiveElement == null )
            {
                InputManager.Instance.ActiveElement = this;
            }

            if ( this.IsFocusable && 
                 InputManager.FocusOnHover == false )
            {
                BubblingMouseButtonEventArg lEventArgs = pEventArgs as BubblingMouseButtonEventArg;
                if ( lEventArgs.Focused == null )
                {
                    lEventArgs.Focused = this;
                    InputManager.Instance.FocusElement = this;
                }
            }

            // propagate event to the top
            UIElement lParent = this.VisualParent as UIElement;
            if ( lParent != null )
            {
                lParent.OnMouseDown( pSender, pEventArgs );
            }

            if ( this.MouseDown != null )
            {
                this.MouseDown.Raise( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on mouse up.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseUp(object pSender, MouseButtonEventArgs pEventArgs)
        {
            // Propagate event to the top
            UIElement lParent = this.VisualParent as UIElement;
            if ( lParent != null )
            {
                lParent.OnMouseUp( pSender, pEventArgs );
            }

            if ( this.MouseUp != null )
            {
                this.MouseUp.Raise( this, pEventArgs );
            }
            
            if ( this.IsMouseIn( pEventArgs.Position ) && 
                 this.IsActive )
            {
                if ( InputManager.Instance.CanBeDoubleClickCandidate() )
                {
                    InputManager.Instance.DoubleClickCandidate = this;
                }

                this.OnMouseClick( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on mouse click.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseClick(object pSender, MouseButtonEventArgs pEventArgs)
        {
            // Propagate event to the top
            UIElement lParent = this.VisualParent as UIElement;
            if ( lParent != null )
            {
                lParent.OnMouseClick( pSender, pEventArgs );
            }

            if ( this.MouseClick != null )
            {
                this.MouseClick.Raise( this, pEventArgs );
            }
        }
        
        /// <summary>
        /// Delegate called on mouse wheel.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseWheel(object pSender, MouseWheelEventArgs pEventArgs)
        {
            // Propagate event to the top
            UIElement lParent = this.VisualParent as UIElement;
            if ( lParent != null )
            {
                lParent.OnMouseWheel( pSender, pEventArgs );
            }

            if ( this.MouseWheelChanged != null )
            {
                this.MouseWheelChanged.Raise( this, pEventArgs );
            }
        }
        
        /// <summary>
        /// Invalidates the element arrangment.
        /// </summary>
        public void InvalidateArrange()
        {
            this.mIsDirty = true;

            this.InternalInvalidateArrange();
        }

        /// <summary>
        /// Internal invalidates arrange method.
        /// </summary>
        protected virtual void InternalInvalidateArrange()
        {
            // If no layouting to do, registers as layouted straight.
            if ( this.mState == LayoutingType.None )
            {
                ContextLayoutManager.From( this.Dispatcher ).RegisterLayouted( this );
            }
        }

        /// <summary>
        /// Invalidates the element measurement.
        /// </summary>
        public void InvalidateMeasure()
        {
            if ( this.IsMeasureDirty == false )
            {
                this.InternalInvalidateMeasure();

                this.IsMeasureDirty = true;
            }
        }

        /// <summary>
        /// Internal invalidates measure method.
        /// </summary>
        protected virtual void InternalInvalidateMeasure()
        {
            this.State = LayoutingType.None;
            this.RegisterForLayouting( LayoutingType.Sizing | LayoutingType.ArrangeChildren );
        }

        /// <summary>
        /// Invalidates the element rendering and process a layout pass.
        /// </summary>
        public void InvalidateVisual()
        {
            this.mIsDirty = true;

            // If no layouting to do, registers as layouted straight.
            if ( this.mState == LayoutingType.None )
            {
                ContextLayoutManager.From( this.Dispatcher ).RegisterLayouted( this );
            }
        }

        /// <summary>
        /// Renders the visual content of the visual.
        /// </summary>
        /// <param name="pDrawingContext"></param>
        protected override void RenderContent(Context pDrawingContext)
        {
            this.Paint( ref pDrawingContext );

            base.RenderContent( pDrawingContext );
        }

        /// <summary>
        /// Recursive painting routine on the parent context of the actual cached version of the widget 
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
		protected internal virtual void Paint(ref Context pDrawingContext)
        {
            //TODO: this test should not be necessary
            if ( this.mSlot.Height < 0 || 
                 this.mSlot.Width  < 0 || 
                 this.VisualParent == null )
            {
                return;
            }

            lock ( this )
            {
                this.mLastPaintedSlots = this.mSlot;

                if ( this.IsCacheEnabled )
                {
                    if ( this.mSlot.Width > MaxCacheSize || 
                         this.mSlot.Height > MaxCacheSize )
                    {
                        this.IsCacheEnabled = false;
                    }
                }

                if ( this.IsCacheEnabled )
                {
                    if ( this.mIsDirty )
                    {
                        this.RecreateCache();
                    }

                    this.UpdateCache( pDrawingContext );
                    if ( this.mIsEnabled == false )
                    {
                        this.PaintElementAsDisabled( pDrawingContext, this.mSlot + this.VisualParent.ClientRectangle.Position );
                    }
                }
                else
                {
                    Rectangle lElementRegion = this.mSlot + this.VisualParent.ClientRectangle.Position;
                    pDrawingContext.Save();

                    pDrawingContext.Translate( lElementRegion.X, lElementRegion.Y );

                    this.OnRender( pDrawingContext );

                    if ( this.mIsEnabled == false )
                    {
                        this.PaintElementAsDisabled( pDrawingContext, Slot );
                    }

                    pDrawingContext.Restore();
                }
            }
        }

        /// <summary>
        /// Paint the element as being disabled.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        /// <param name="pElementRegion">The element drawing region.</param>
        private void PaintElementAsDisabled(Context pDrawingContext, Rectangle pElementRegion)
        {
            pDrawingContext.Operator = Operator.Xor;
            pDrawingContext.SetSourceRGBA( 0.6, 0.6, 0.6, 0.3 );
            pDrawingContext.Rectangle( pElementRegion );
            pDrawingContext.Fill();
            pDrawingContext.Operator = Operator.Over;
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected virtual void OnRender(Context pDrawingContext)
        {
            // Override..
        }

        /// <summary>
        /// Internal drawing context creation on a cached surface limited to slot size
		/// and triggers the effective drawing routine 
        /// </summary>
		protected virtual void RecreateCache()
        {
            int lStride = 4 * this.mSlot.Width;

            int lDrawingSurfaceSize = Math.Abs( lStride ) * this.mSlot.Height;
            this.mRawDrawingSurface = new byte[ lDrawingSurfaceSize ];
            this.mIsDirty = false;
            using ( ImageSurface lDrawingSurface = new ImageSurface( this.mRawDrawingSurface, Format.Argb32, this.mSlot.Width, this.mSlot.Height, lStride ) )
            {
                using ( Context lContext = new Context( lDrawingSurface ) )
                {
                    lContext.Antialias = PresentationSource.Antialias;
                    this.OnRender( lContext );
                }

                lDrawingSurface.Flush();
            }
        }

        /// <summary>
        /// Update the drawing cache.
        /// </summary>
        /// <param name="pDrawingContext"></param>
        protected void UpdateCache(Context pDrawingContext)
        {
            Rectangle lElementRegion = this.mSlot + this.VisualParent.ClientRectangle.Position;
            using ( ImageSurface lSurfaceCache = new ImageSurface( this.mRawDrawingSurface, Format.Argb32, this.mSlot.Width, this.mSlot.Height, 4 * this.mSlot.Width ) )
            {
                this.InternalUpdateCache( ref lElementRegion, lSurfaceCache, pDrawingContext );

                pDrawingContext.SetSourceSurface( lSurfaceCache, lElementRegion.X, lElementRegion.Y );
                pDrawingContext.Paint();
            }
            
            this.VisualClip.Reset();
        }

        /// <summary>
        /// Internal drawing cache update.
        /// </summary>
        /// <param name="pElementRegion">The element drawing region.</param>
        /// <param name="pSurface">The drawing surface.</param>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected virtual void InternalUpdateCache(ref Rectangle pElementRegion, ImageSurface pSurface, Context pDrawingContext)
        {
            if ( this.mClearElementDrawingRegion )
            {
                pDrawingContext.Save();
                pDrawingContext.Operator = Operator.Clear;
                pDrawingContext.Rectangle( pElementRegion );
                pDrawingContext.Fill();
                pDrawingContext.Restore();
            }
        }

        /// <summary> 
        /// Update layout component only one at a time, this is where the computation of alignement
		/// and size take place.
		/// The redrawing will only be triggered if final slot size has changed
        /// </summary>
		/// <returns>True if layouting was possible, False if conditions were not met and LQI has to be re-queued</returns>
		public bool UpdateLayout()
        {
            Rectangle lRect = this.Arrange( this.ArrangeConstraint );
            if ( lRect.Width  < 0 || 
                 lRect.Height < 0 )
            {
                return false;
            }

            return true;
        }
        
        /// <summary>
        /// Registers a layout part to update.
        /// </summary>
        /// <param name="pLayoutType">The layout part to register for update.</param>
        protected internal virtual void RegisterForLayouting(LayoutingType pLayoutType)
        {
            if ( this.VisualParent == null )
            {
                return;
            }

            ContextLayoutManager lLayoutManager = ContextLayoutManager.From( this.Dispatcher );
            lock ( lLayoutManager.LayoutMutex )
            {
                //prevent queueing same LayoutingType for this
                pLayoutType &= (~this.mState);

                if ( pLayoutType == LayoutingType.None )
                {
                    return;
                }
                
                // Apply constraints depending on parent type
                if ( this.VisualParent is UIElement )
                {
                    (this.VisualParent as UIElement).ChildrenLayoutingConstraints( ref pLayoutType );
                }
                
                if ( pLayoutType == LayoutingType.None )
                {
                    return;
                }

                // enqueue LQI LayoutingTypes separately
                if ( pLayoutType.HasFlag( LayoutingType.Width ) )
                {
                    lLayoutManager.ArrangeQueue.Enqueue(new LayoutItem(LayoutingType.Width, this));
                }

                if ( pLayoutType.HasFlag( LayoutingType.Height ) )
                {
                    lLayoutManager.ArrangeQueue.Enqueue(new LayoutItem(LayoutingType.Height, this));
                }

                if ( pLayoutType.HasFlag( LayoutingType.X ) )
                {
                    lLayoutManager.ArrangeQueue.Enqueue(new LayoutItem(LayoutingType.X, this));
                }

                if ( pLayoutType.HasFlag( LayoutingType.Y ) )
                {
                    lLayoutManager.ArrangeQueue.Enqueue(new LayoutItem(LayoutingType.Y, this));
                }

                if ( pLayoutType.HasFlag( LayoutingType.ArrangeChildren ) )
                {
                    lLayoutManager.ArrangeQueue.Enqueue(new LayoutItem(LayoutingType.ArrangeChildren, this));
                }
            }
        }

        /// <summary>
        /// Applies a children layout constrain.
        /// </summary>
        /// <param name="pLayoutType">The layout constrain type.</param>
        protected virtual void ChildrenLayoutingConstraints(ref LayoutingType pLayoutType)
        {
            // Bothing to do.
        }

        /// <summary>
        /// Computes the size of the element.
        /// </summary>
        /// <returns></returns>
        public Size Measure()
        {
            Size lOldDesired = this.mDesiredSize;

            this.mDesiredSize =  this.MeasureCore();

            this.IsMeasureDirty = false;

            return this.mDesiredSize;
        }

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns></returns>
		protected virtual Size MeasureCore()
        {
            return this.mContentSize;
        }

        /// <summary>
        /// Delegate called on child desired size changes.
        /// </summary>
        /// <param name="pChild"></param>
        protected virtual void OnChildDesiredSizeChanged(UIElement pChild)
        {
            if ( this.IsMeasureValid )
            {
                this.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to arrange.</param>
        /// <returns>The new element region.</returns>
        public Rectangle Arrange(LayoutingType pLayoutType)
        {
            //unset bit, it would be reset if LQI is re-queued
            this.mState &= (~pLayoutType);

            Rectangle lResult = this.ArrangeCore( pLayoutType );

            // if no layouting remains in queue for item, register as layouted.
            if ( this.mState == LayoutingType.None && 
                 this.mIsDirty )
            {
                ContextLayoutManager.From( this.Dispatcher ).RegisterLayouted( this );
            }

            return lResult;
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected virtual Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            return this.mSlot;
        }

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected internal virtual void OnLayoutChanges(LayoutingType pLayoutType)
        {
            switch ( pLayoutType )
            {
                case LayoutingType.Width:
                    this.RegisterForLayouting( LayoutingType.X );
                    break;
                case LayoutingType.Height:
                    this.RegisterForLayouting( LayoutingType.Y );
                    break;
            }
            
            this.NotifyLayoutChanged( new LayoutingEventArgs( pLayoutType ) );
        }

        /// <summary>
        /// Notifies the layout has changed.
        /// </summary>
        /// <param name="pArgs"></param>
        protected internal void NotifyLayoutChanged(LayoutingEventArgs pArgs)
        {
            if ( this.LayoutUpdated != null )
            {
                this.LayoutUpdated.Raise( this, pArgs );
            }
        }

        /// <summary>
        /// Clone the visual element.
        /// </summary>
        /// <returns>The clone.</returns>
        protected override AVisual InternalClone()
        {
            return new UIElement();
        }
        
        #endregion Methods
    }
}
