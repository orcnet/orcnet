﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ColorPicker"/> class.
    /// </summary>
	public class ColorPicker : ContentControl
	{
        #region Constants

        /// <summary>
        /// Stores the channel divider.
        /// </summary>
        private const double cDiv = 255.0;

        /// <summary>
        /// Stores the inverse channel divider.
        /// </summary>
        private const double cColorDiv = 1.0 / cDiv;

        #endregion Constants

        #region Fields

        /// <summary>
        /// Stores the current color.
        /// </summary>
        private Color mCurrentColor;

        /// <summary>
        /// Stores the Hue component of the color.
        /// </summary>
        private double mHue;

        /// <summary>
        /// Stores the Saturation component of the color.
        /// </summary>
        private double mSaturation;

        /// <summary>
        /// Stores the Value component of the color.
        /// </summary>
        private double mValue;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the Red parameter.
        /// </summary>
		public virtual double R
        {
			get
            {
                return Math.Round(mCurrentColor.R * cDiv);
            }
			set
            {
                if (R == value)
                {
                    return;
                }

				mCurrentColor.R = value * cColorDiv;
                this.NotifyPropertyChanged();
                this.ToHSV();
                this.NotifyColorHasChanged();
			}
		}

        /// <summary>
        /// Gets or sets the Green parameter.
        /// </summary>
		public virtual double G
        {
			get
            {
                return Math.Round(mCurrentColor.G * cDiv);
            }
			set
            {
                if (G == value)
                {
                    return;
                }

				mCurrentColor.G = value * cColorDiv;
                this.NotifyPropertyChanged();
                this.NotifyColorHasChanged();
                this.ToHSV();
			}
		}

        /// <summary>
        /// Gets or sets the Blue parameter.
        /// </summary>
		public virtual double B
        {
			get
            {
                return Math.Round(mCurrentColor.B * cDiv);
            }
			set
            {
                if (B == value)
                {
                    return;
                }

				this.mCurrentColor.B = value * cColorDiv;
                this.NotifyPropertyChanged();
                this.NotifyColorHasChanged();
                this.ToHSV();
			}
		}

        /// <summary>
        /// Gets or sets the Alpha parameter.
        /// </summary>
		public virtual double A
        {
			get
            {
                return Math.Round(mCurrentColor.A * cDiv);
            }
			set
            {
                if (A == value)
                {
                    return;
                }

				this.mCurrentColor.A = value * cColorDiv;
                this.NotifyPropertyChanged();
                this.NotifyColorHasChanged();
                this.ToHSV();
			}
		}

        /// <summary>
        /// Gets or sets the Hue parameter.
        /// </summary>
		public virtual double H
        {
			get
            {
                return Math.Round(this.mHue, 3);
            }
			set
            {
                if (H == value)
                {
                    return;
                }

				this.mHue = value;
                this.NotifyPropertyChanged("H");
                this.ToRGB();
			}
		}

        /// <summary>
        /// Gets or sets the Saturation parameter.
        /// </summary>
		public virtual double S
        {
			get
            {
                return Math.Round(this.mSaturation, 2);
            }
			set
            {
                if (S == value)
                {
                    return;
                }

				this.mSaturation = value;
                this.NotifyPropertyChanged("S");
				this.ToRGB();
			}
		}

        /// <summary>
        /// Gets or sets the Value parameter.
        /// </summary>
		public virtual double V
        {
			get
            {
                return Math.Round(this.mValue, 2);
            }
			set
            {
                if (V == value)
                {
                    return;
                }

				this.mValue = value;
                this.NotifyPropertyChanged("V");
                this.ToRGB();
			}
		}

        /// <summary>
        /// Gets or sets the picker selected color as Brush.
        /// </summary>
		public virtual ABrush SelectedColor
        {
			get
            {
                return new SolidColorBrush(mCurrentColor);
            }
			set
            {
                if (value == null)
                {
                    mCurrentColor = default(Color);
                }
                else if (value is SolidColorBrush)
                {
                    Color c = (value as SolidColorBrush).Color;
                    if (mCurrentColor == c)
                    {
                        return;
                    }

                    mCurrentColor = c;
                }

                this.NotifyColorHasChanged();
                this.NotifyRGBAHasChanged();
                this.ToHSV();
			}
		}

        /// <summary>
        /// Gets or sets the selected raw color as Color.
        /// </summary>
		public virtual Color SelectedRawColor
        {
			get
            {
                return mCurrentColor;
            }
			set
            {
                if (mCurrentColor == value)
                {
                    return;
                }

				this.mCurrentColor = value;
                this.NotifyColorHasChanged();
                this.NotifyRGBAHasChanged();
                this.ToHSV();
			}
		}

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorPicker"/> class.
        /// </summary>
        public ColorPicker() : 
        base()
		{

		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Notifies the current color has changed.
        /// </summary>
        private void NotifyColorHasChanged()
        {
			this.NotifyPropertyChanged( "SelectedColor" );
            this.NotifyPropertyChanged( "SelectedRawColor" );
            string n = mCurrentColor.ToString();
            if (char.IsLetter(n[0]))
            {
                this.NotifyPropertyChanged( "SelectedColorName" );
            }
            else
            {
                this.NotifyPropertyChanged( "SelectedColorName" );
            }

            this.NotifyPropertyChanged( "HexColor" );
		}

        /// <summary>
        /// Notify the RGBA components have changed.
        /// </summary>
		private void NotifyRGBAHasChanged()
        {
            this.NotifyPropertyChanged("R");
			this.NotifyPropertyChanged("G");
			this.NotifyPropertyChanged("B");
            this.NotifyPropertyChanged("A");
		}

        /// <summary>
        /// Notifies the HSV components have changed.
        /// </summary>
		private void NotifyHSVHasChanged()
        {
			this.NotifyPropertyChanged("H");
			this.NotifyPropertyChanged("S");
            this.NotifyPropertyChanged("V");
		}

        /// <summary>
        /// Turns a HSV color to a RGB one.
        /// </summary>
		private void ToHSV()
        {
			Color lColor = mCurrentColor;
            lColor.ResetName();
            double lMin = Math.Min(lColor.R, Math.Min(lColor.G, lColor.B)); //Min. value of RGB
            double lMax = Math.Max(lColor.R, Math.Max(lColor.G, lColor.B));	//Max. value of RGB
			double lDiff = lMax - lMin;							//Delta RGB value

			this.mValue = lMax;

			if ( lDiff == 0 )//This is a gray, no chroma...
			{
				this.mHue = 0;
				this.mSaturation = 0;
			}
            // Chromatic data...
            else
            {
				this.mSaturation = lDiff / lMax;

				double diffR = (((lMax - lColor.R) / 6.0) + (lDiff / 2.0)) / lDiff;
				double diffG = (((lMax - lColor.G) / 6.0) + (lDiff / 2.0)) / lDiff;
				double diffB = (((lMax - lColor.B) / 6.0) + (lDiff / 2.0)) / lDiff;

                if (lColor.R == lMax)
                {
                    this.mHue = diffB - diffG;
                }
                else if (lColor.G == lMax)
                {
                    this.mHue = (1.0 / 3.0) + diffR - diffB;
                }
                else if (lColor.B == lMax)
                {
                    this.mHue = (2.0 / 3.0) + diffG - diffR;
                }

                if (this.mHue < 0)
                {
                    this.mHue += 1;
                }

                if (this.mHue > 1)
                {
                    this.mHue -= 1;
                }
			}

            this.NotifyHSVHasChanged();
		}

        /// <summary>
        /// Turns a HSV color into a RGB one.
        /// </summary>
        private void ToRGB()
        {
            this.mCurrentColor = Color.FromHSV( this.mHue, this.mValue, this.mSaturation, mCurrentColor.A );
            this.NotifyColorHasChanged();
            this.NotifyRGBAHasChanged();
		}

        #endregion Methods
    }
}

