﻿using Cairo;
using OrcNet.UI.Input;
using OrcNet.UI.Ximl;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Control"/> class.
    /// 
    /// NOTE: Allow appearance customization giving a control template.
    /// </summary>
    public class Control : FrameworkElement
    {
        #region Fields

        /// <summary>
        /// Stores the control text font.
        /// </summary>
        private Font mFont;

        /// <summary>
        /// Stores the control background brush.
        /// </summary>
        private ABrush mBackground;

        /// <summary>
        /// Stores the control foreground brush.
        /// </summary>
        private ABrush mForeground;

        /// <summary>
        /// Stores the control's template.
        /// </summary>
        private ControlTemplate mTemplate;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the control's template.
        /// </summary>
        public ControlTemplate Template
        {
            get
            {
                return this.mTemplate;
            }
            set
            {
                ControlTemplate lOldTemplate = this.mTemplate;
                this.mTemplate = value;

                this.OnTemplateChanged( lOldTemplate, this.mTemplate );

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the template to apply to the element if any.
        /// </summary>
        internal override FrameworkTemplate TemplateInternal
        {
            get
            {
                return this.Template;
            }
            set
            {
                this.Template = value as ControlTemplate;
            }
        }

        /// <summary>
        /// Gets or sets the font familly.
        /// </summary>
        public string FontFamilly
        {
            get
            {
                return this.mFont.Name;
            }
            set
            {
                if ( this.mFont.Name != value )
                {
                    this.mFont.Name = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the font style.
        /// </summary>
        public FontStyle FontStyle
        {
            get
            {
                return this.mFont.Style;
            }
            set
            {
                if ( this.mFont.Style != value )
                {
                    this.mFont.Style = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the font style.
        /// </summary>
        public FontWeight FontWeight
        {
            get
            {
                return this.mFont.Weight;
            }
            set
            {
                if ( this.mFont.Weight != value )
                {
                    this.mFont.Weight = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the font size.
        /// </summary>
        public int FontSize
        {
            get
            {
                return this.mFont.Size;
            }
            set
            {
                if ( this.mFont.Size != value )
                {
                    this.mFont.Size = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the control background brush.
        /// </summary>
        public ABrush Background
        {
            get
            {
                return this.mBackground;
            }
            set
            {
                if ( this.mBackground != value )
                {
                    this.mClearElementDrawingRegion = false;

                    if ( value != null )
                    {
                        this.mBackground = value;
                        this.NotifyPropertyChanged();
                        this.InvalidateVisual();

                        this.mClearElementDrawingRegion = this.mBackground.IsClearColor;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the control foreground brush.
        /// </summary>
        public ABrush Foreground
        {
            get
            {
                return this.mForeground;
            }
            set
            {
                if ( this.mForeground != value )
                {
                    this.mForeground = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateVisual();
                }
            }
        }

        #endregion Properties

        #region Events

        /// <summary>
        /// Event fired on mouse double clicks.
        /// </summary>
        public event EventHandler<MouseButtonEventArgs> MouseDoubleClick;

        #endregion Events

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
        public Control()
        {
            this.mFont = new Font();
            this.mFont.Name  = "droid";
            this.mFont.Size  = 10;
            this.mForeground = Color.White;
            this.mBackground = Color.Transparent;
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Delegate called on template changes.
        /// </summary>
        /// <param name="pOldTemplate">The previous template.</param>
        /// <param name="pNewTemplate">The new template.</param>
        protected virtual void OnTemplateChanged(ControlTemplate pOldTemplate, ControlTemplate pNewTemplate)
        {

        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            Rectangle lBackground = new Rectangle( this.mSlot.Size );

            this.mBackground.SetAsSource( pDrawingContext, lBackground );

            CairoHelpers.CairoRectangle( pDrawingContext, lBackground, -1 );

            pDrawingContext.Fill();
        }

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( InputManager.Instance.IsDoubleClicking( this ) )
            {
                this.OnMouseDoubleClick( this, pEventArgs );
            }

            InputManager.Instance.DoubleClickCandidate = null;

            base.OnMouseDown( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on mouse double click.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseDoubleClick(object pSender, MouseButtonEventArgs pEventArgs)
        {
            // Propagate event to the top
            Control lParent = this.VisualParent as Control;
            if ( lParent != null )
            {
                lParent.OnMouseDoubleClick( pSender, pEventArgs );
            }

            if ( this.MouseDoubleClick != null )
            {
                this.MouseDoubleClick.Raise( this, pEventArgs );
            }
        }


        #endregion Methods
    }
}
