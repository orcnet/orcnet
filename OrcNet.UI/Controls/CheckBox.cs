﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="CheckBox"/> class.
    /// </summary>
	public class CheckBox : ToggleButton
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
        public CheckBox() : 
        base()
		{
            
        }

        #endregion Constructor
    }
}
