﻿using System.IO;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DirectoryView"/> class.
    /// </summary>
	public class DirectoryView : TreeView
    {
        #region Fields

        /// <summary>
        /// Stores the current directory.
        /// </summary>
        private string mCurrentDirectory = "/";

        /// <summary>
        /// Stores the flag indicating whether all files must be shown or not.
        /// </summary>
        private bool mShowFiles;
        
        #endregion Fields
        
        #region Properties
        
        /// <summary>
        /// Gets or sets the flag indicating whether all files must be shown or not.
        /// </summary>
        public virtual bool ShowFiles
        {
            get
            {
                return mShowFiles;
            }
            set
            {
                if (mShowFiles == value)
                {
                    return;
                }

                this.mShowFiles = value;
                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged( "FileSystemEntries" );
            }
        }
        
        /// <summary>
        /// Gets or sets the current directory.
        /// </summary>
        public virtual string CurrentDirectory
        {
            get
            {
                return mCurrentDirectory;
            }
            set
            {
                if (mCurrentDirectory == value)
                {
                    return;
                }

                this.mCurrentDirectory = value;
                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged( "FileSystemEntries" );
            }
        }
        
        /// <summary>
        /// Gets the set of file system entries.
        /// </summary>
        public FileSystemInfo[] FileSystemEntries
        {
            get
            {
                return string.IsNullOrEmpty(CurrentDirectory) ? null :
                    mShowFiles ?
                    new DirectoryInfo(CurrentDirectory).GetFileSystemInfos() :
                    new DirectoryInfo(CurrentDirectory).GetDirectories();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DirectoryView"/> class.
        /// </summary>
        public DirectoryView() : 
        base()
		{
            this.mShowFiles = true;
        }

        #endregion Constructor
    }
}

