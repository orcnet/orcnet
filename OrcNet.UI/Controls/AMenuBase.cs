﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the abstract <see cref="AMenuBase"/> base class.
    /// </summary>
    public abstract class AMenuBase : ItemsControl
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AMenuBase"/> class.
        /// </summary>
        protected AMenuBase()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on UIElement mouse leave.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseLeave(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseLeave( pSender, pEventArgs );

            // Auto close the menu.

        }

        #endregion Methods
    }
}
