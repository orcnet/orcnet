﻿using System;
using OrcNet.UI.Collections;
using OrcNet.Core;
using System.Collections;
using OrcNet.UI.Enumerators;
using System.Diagnostics;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Window"/> class.
    /// </summary>
	public class Window : ContentControl, IWindow
	{
        #region Statics

        /// <summary>
        /// Stores the default title threshold so the mouse position be considered in or out of the title.
        /// </summary>
        public static int TitleThreshold = 15;

        #endregion Statics

        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the mouse left button is down or not.
        /// </summary>
        private bool mIsLeftDown;

        /// <summary>
        /// Stores the flag indicating whether the native window has been created but not shown yet or not.
        /// </summary>
        private bool mIsNativeWindowCreatedButNotShown;

        /// <summary>
        /// Stores the presentation source 
        /// </summary>
        private PresentationSource mSource;

        /// <summary>
        /// Stores the flag indicating whether the style is dirty or not.
        /// </summary>
        private bool mIsStyleDirty = false;
        
        /// <summary>
        /// Stores the styles flags.
        /// </summary>
        private int mStyleFlags;

        /// <summary>
        /// Stores the flag indicating whether the window has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the flag indicating whether the application the window is for, is shutting down or not.
        /// </summary>
        private bool mIsAppShuttingDown;

        /// <summary>
        /// Stores the flag indicating whether the cancellation on window closing must be ignored or not.
        /// </summary>
        private bool mIgnoreCancel;

        /// <summary>
        /// Stores the flag indicating whether the window is being shown as dialog or not.
        /// </summary>
        private bool mIsShowingAsDialog;

        /// <summary>
        /// Stores the flag indicating whether the window is being closed or not.
        /// </summary>
        private bool mIsClosing;

        /// <summary>
        /// Stores the parent window if any.
        /// </summary>
        private Window mParent;

        /// <summary>
        /// Stores the set of  windows owned by this window.
        /// </summary>
        private WindowCollection mOwnedWindows;

        /// <summary>
        /// Stores the window title.
        /// </summary>
        private string mTitle;

        /// <summary>
        /// Stores the window style.
        /// </summary>
        private WindowStyle mWindowStyle;

        /// <summary>
        /// Stores the window state.
        /// </summary>
        private WindowState mWindowState;

        /// <summary>
        /// Stores the flag indicating whether the window is top most or not.
        /// </summary>
        private bool mIsTopMost;

        /// <summary>
        /// Stores the window icon.
        /// </summary>
        private ImageSource mIcon;

        /// <summary>
        /// Stores the window resize mode.
        /// </summary>
        private ResizeMode mResizeMode;

        /// <summary>
        /// Stores the dialog result if opened as dialog window.
        /// </summary>
        private bool? mDialogResult;
        
        /// <summary>
        /// Stores the flag indicating whether the mouse is over the border or not.
        /// </summary>
		bool mIsHoverBorder = false;

        /// <summary>
        /// Stores the restore bounds to use to go back to normal window state.
        /// </summary>
		private Rectangle mRestoreBounds;
        
        /// <summary>
        /// Stores the current drag position if any.
        /// </summary>
        private DragPosition mCurrentDrag;
        
        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on source initialized.
        /// </summary>
        public event EventHandler SourceInitialized;

        /// <summary>
        /// Event fired on Z order changing when the window becomes top most.
        /// </summary>
        public event EventHandler Activated;

        /// <summary>
        /// Event fired on window closed.
        /// </summary>
        public event EventHandler Closed;

        /// <summary>
        /// Event fired on window closing and allow to stop the closing process.
        /// </summary>
        public event CancelEventDelegate Closing;

        /// <summary>
        /// Event fired on Z order changing when the window goes behind another.
        /// </summary>
        public event EventHandler Deactivated;

        /// <summary>
        /// Event fired on window location changes.
        /// </summary>
        public event EventHandler LocationChanged;

        /// <summary>
        /// Event fired on window state changes.
        /// </summary>
        public event EventHandler StateChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the current drag position if any.
        /// </summary>
        private DragPosition CurrentDrag
        {
            get
            {
                return this.mCurrentDrag;
            }
            set
            {
                if ( this.mCurrentDrag == value )
                {
                    return;
                }

                this.mCurrentDrag = value;

                this.UpdateCursor();
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the native window has been created but not shown yet or not.
        /// </summary>
        internal bool IsNativeWindowCreatedButNotShown
        {
            get
            {
                return this.mIsNativeWindowCreatedButNotShown;
            }
            private set
            {
                this.mIsNativeWindowCreatedButNotShown = value;
            }
        }

        /// <summary>
        /// Checks whether the source window is null or not.
        /// </summary>
        internal bool IsSourceWindowNull
        {
            get
            {
                return this.mSource == null;
            }
        }
        
        /// <summary>
        /// Gets the flag indicating whether window is active or not.
        /// </summary>
        internal bool IsActiveWindow
        {
            get
            {
                return this.mSource.IsActive;
            }
        }

        /// <summary>
        /// Gets the source window.
        /// </summary>
        internal PresentationSource SourceWindow
        {
            get
            {
                return this.mSource;
            }
        }

        /// <summary>
        /// Gets the composition target.
        /// </summary>
        internal PresentationTarget CompositionTarget
        {
            get
            {
                if ( this.SourceWindow != null )
                {
                    PresentationTarget lCompositionTarget = this.SourceWindow.CompositionTarget;
                    if ( lCompositionTarget != null && 
                         lCompositionTarget.IsDisposed == false )
                    {
                        return lCompositionTarget;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the window size.
        /// </summary>
        internal Size WindowSize
        {
            get
            {
                return this.mSource.WindowWrapper.WindowSize;
            }
        }

        /// <summary>
        /// Gets the styles flags.
        /// </summary>
        internal int StyleInternal
        {
            get
            {
                if ( this.IsSourceWindowNull )
                {
                    return this.mStyleFlags;
                }

                return this.mSource.SourceStyle;
            }
            set
            {
                if ( this.mStyleFlags == value )
                {
                    return;
                }

                this.mStyleFlags = value;

                this.mIsStyleDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the window's owner if any.
        /// </summary>
        public Window Owner
        {
            get
            {
                return this.mParent;
            }
            set
            {
                if ( value == this )
                {
                    throw new ArgumentException( "Cannot be its own owner window." );
                }

                if ( this.mIsShowingAsDialog )
                {
                    throw new InvalidOperationException( "Cannot set an owner after showing a dialog window." );
                }

                if ( value != null && 
                     value.IsSourceWindowNull )
                {
                    // Try to be specific in the error message.
                    if ( value.IsDisposed )
                    {
                        throw new InvalidOperationException( "Cannot set an owner to a closed window." );
                    }

                    throw new InvalidOperationException( "Cannot set an owner to window that does have its native window created." );
                }

                if ( this.mParent == value )
                {
                    return;
                }

                if ( this.IsDisposed == false )
                {
                    if ( value != null )
                    {
                        WindowCollection lOwnedWindows = this.OwnedWindows;
                        if ( lOwnedWindows.Contains( value ) )
                        {
                            throw new ArgumentException( string.Format( "Attempting to set a parent window that is one of the current one's children.", value, this));
                        }
                    }

                    if ( this.mParent != null )
                    {
                        this.mParent.OwnedWindowsInternal.Remove( this );
                    }
                }
                
                this.mParent = value;

                if ( this.IsDisposed )
                {
                    return;
                }

                if ( this.mParent != null )
                {
                    this.mParent.OwnedWindowsInternal.Add( this );
                }
            }
        }

        /// <summary>
        /// Gets or sets the window title.
        /// </summary>
        public string Title
        {
            get
            {
                return this.mTitle;
            }
            set
            {
                if ( this.mTitle == value ||
                     string.IsNullOrEmpty( value ) )
                {
                    return;
                }

                this.mTitle = value;

                this.NotifyPropertyChanged();

                this.OnTitleChanged();

                this.InvalidateVisual();
            }
        }
        
        /// <summary>
        /// Gets or sets the flag indicating whether the window is top most or not.
        /// </summary>
        public bool IsTopMost
        {
            get
            {
                return this.mIsTopMost;
            }
            set
            {
                if ( this.mIsTopMost == value )
                {
                    return;
                }

                this.mIsTopMost = value;

                if ( this.mIsTopMost )
                {
                    this.Activate();
                }
                else
                {

                }

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the window style.
        /// </summary>
        public WindowStyle WindowStyle
        {
            get
            {
                return this.mWindowStyle;
            }
            set
            {
                if ( this.mWindowStyle == value )
                {
                    return;
                }

                this.mWindowStyle = value;

                this.NotifyPropertyChanged();

                this.OnStyleChanged( EventArgs.Empty );
                
                this.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Gets or sets the window state.
        /// </summary>
        public WindowState WindowState
        {
            get
            {
                return this.mWindowState;
            }
            set
            {
                if ( this.mWindowState == value )
                {
                    return;
                }

                this.mWindowState = value;

                this.NotifyPropertyChanged();

                this.OnStateChanged( EventArgs.Empty );

                this.InvalidateVisual();
            }
        }
        
        /// <summary>
        /// Gets or sets the window resize mode.
        /// </summary>
        public ResizeMode ResizeMode
        {
            get
            {
                return this.mResizeMode;
            }
            set
            {
                if ( this.mResizeMode == value )
                {
                    return;
                }

                this.mResizeMode = value;

                this.NotifyPropertyChanged();

                this.OnResizeModeChanged();
            }
        }
        
        /// <summary>
        /// Gets the restore bounds to use to go back to normal window state.
        /// </summary>
		public Rectangle RestoreBounds
        {
            get
            {
                if ( this.IsSourceWindowNull ||
                     this.CompositionTarget == null )
                {
                    return Rectangle.Empty;
                }

                return this.mRestoreBounds;
            }
        }

        /// <summary>
        /// Gets or sets the window icon.
        /// </summary>
        public ImageSource Icon
        {
            get
            {
                return this.mIcon;
            }
            set
            {
                if ( this.mIcon == value )
                {
                    return;
                }

                this.mIcon = value;

                this.NotifyPropertyChanged();

                this.OnIconChanged( this.mIcon );

                this.InvalidateVisual();
            }
        }
        
        /// <summary>
        /// Gets or sets the dialog result if opened as dialog window.
        /// </summary>
        public bool? DialogResult
        {
            get
            {
                return this.mDialogResult;
            }
            set
            {
                if ( this.mIsShowingAsDialog )
                {
                    Debug.Assert( this.IsSourceWindowNull == false, "IsSourceWindowNull cannot be true when IsShowingAsDialog is true");

                    if ( this.mDialogResult != value )
                    {
                        this.mDialogResult = value;

                        if ( this.mIsClosing == false )
                        {
                            this.Close();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the set of  windows owned by this window.
        /// </summary>
        public WindowCollection OwnedWindows
        {
            get
            {
                return new WindowCollection( this.OwnedWindowsInternal );
            }
        }

        /// <summary>
        /// Gets the windows internal collection.
        /// </summary>
        private WindowCollection OwnedWindowsInternal
        {
            get
            {
                if ( this.mOwnedWindows == null )
                {
                    this.mOwnedWindows = new WindowCollection();
                }

                return this.mOwnedWindows;
            }
        }

        /// <summary>
        /// Gets the set of logical children if any.
        /// </summary>
        protected internal override IEnumerator LogicalChildren
        {
            get
            {
                return new SingleChildEnumerator( this.Content );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Window"/> class.
        /// </summary>
        public Window() : 
        base()
        {
            this.mIsDisposed   = false;
            this.mDialogResult = false;
            this.mWindowStyle = WindowStyle.BorderedWindow;
            this.mWindowState = WindowState.Normal;
            this.mResizeMode  = ResizeMode.CanResize;
			this.mCurrentDrag = DragPosition.None;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            base.OnMouseDown( pSender, pEventArgs );

            this.mIsLeftDown = false;

            if ( pEventArgs.Button == MouseButton.Left )
            {
                this.mIsLeftDown = true;
            }
        }

        /// <summary>
        /// Delegate called on UIelement mouse moves.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseMove(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseMove( pSender, pEventArgs );
            
            // Checks whether in the border or not.
            if ( this.IsInBorder( pEventArgs ) )
            {
                // Processes window resizing if needed.
                this.ProcessResize( pEventArgs.XDelta, pEventArgs.YDelta );
            }
            // Checks whether in the title bar or not.
            else if ( this.IsInTitle( pEventArgs ) )
            {
                // Processes window moving if needed.
                this.ProcessMove( pEventArgs.XDelta, pEventArgs.YDelta );
            }
        }

        /// <summary>
        /// Delegate called on mouse up.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseUp(object pSender, MouseButtonEventArgs pEventArgs)
        {
            base.OnMouseUp( pSender, pEventArgs );

            if ( pEventArgs.Button == MouseButton.Left )
            {
                this.mIsLeftDown = false;
            }
        }

        /// <summary>
        /// Processes window moving.
        /// </summary>
        /// <param name="pXDelta">The X delta.</param>
        /// <param name="pYDelta">The Y delta.</param>
        private void ProcessMove(double pXDelta, double pYDelta)
        {
            if ( this.IsFocused &&
                 this.mIsLeftDown )
            {
                int lCurrentLeft;
                int lCurrentTop;
                int lCurrentWidth;
                int lCurrentHeight;
                GetCurrentWindowInfo( this, out lCurrentLeft, out lCurrentTop, out lCurrentWidth, out lCurrentHeight );

                this.Left = (int)(lCurrentLeft + pXDelta);
                this.Top  = (int)(lCurrentTop  + pYDelta);
            }
        }

        /// <summary>
        /// Processes window resizing.
        /// </summary>
        /// <param name="pXDelta">The X delta.</param>
        /// <param name="pYDelta">The Y delta.</param>
        private void ProcessResize(double pXDelta, double pYDelta)
        {
            if ( this.IsFocused &&
                 this.mIsLeftDown )
            {
                int lCurrentLeft;
                int lCurrentTop;
                int lCurrentWidth;
                int lCurrentHeight;
                GetCurrentWindowInfo( this, out lCurrentLeft, out lCurrentTop, out lCurrentWidth, out lCurrentHeight );
                
                switch ( this.mCurrentDrag )
                {
                    case DragPosition.N:
                        {
                            this.Height = lCurrentHeight - pYDelta;
                            if ( this.Height == lCurrentHeight - pYDelta )
                            {
                                this.Top = (int)(lCurrentTop + pYDelta);
                            }
                        }
                        break;
                    case DragPosition.S:
                        {
                            this.Height = lCurrentHeight + pYDelta;
                        }
                        break;
                    case DragPosition.W:
                        {
                            this.Width = lCurrentWidth - pXDelta;
                            if ( this.Width == lCurrentWidth - pXDelta )
                            {
                                this.Left = (int)(lCurrentLeft + pXDelta);
                            }
                        }
                        break;
                    case DragPosition.E:
                        {
                            this.Width = lCurrentWidth + pXDelta;
                        }
                        break;
                    case DragPosition.NW:
                        {
                            this.Height = lCurrentHeight - pYDelta;
                            if ( this.Height == lCurrentHeight - pYDelta )
                            {
                                this.Top = (int)(lCurrentTop + pYDelta);
                            }
                            this.Width = lCurrentWidth - pXDelta;
                            if ( this.Width == lCurrentWidth - pXDelta )
                            {
                                this.Left = (int)(lCurrentLeft + pXDelta);
                            }
                        }
                        break;
                    case DragPosition.NE:
                        {
                            this.Height = lCurrentHeight - pYDelta;
                            if ( this.Height == lCurrentHeight - pYDelta )
                            {
                                this.Top = (int)(lCurrentTop + pYDelta);
                            }
                            this.Width = lCurrentWidth + pXDelta;
                        }
                        break;
                    case DragPosition.SW:
                        {
                            this.Width = lCurrentWidth - pXDelta;
                            if ( this.Width == lCurrentWidth - pXDelta )
                            {
                                this.Left = (int)(lCurrentLeft + pXDelta);
                            }
                            this.Height = lCurrentHeight + pYDelta;
                        }
                        break;
                    case DragPosition.SE:
                        {
                            this.Height = lCurrentHeight + pYDelta;
                            this.Width  = lCurrentWidth + pXDelta;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Checks whether the mouse is in the title bar or not.
        /// </summary>
        /// <param name="pEventArgs"></param>
        /// <returns></returns>
        protected virtual bool IsInTitle(MouseMoveEventArgs pEventArgs)
        {
            if ( Math.Abs( pEventArgs.Position.Y - this.Slot.Y ) < (Border.BorderThreshold + TitleThreshold) )
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the mouse is in the border or not.
        /// </summary>
        /// <param name="pEventArgs">The mouse move event arguments.</param>
        /// <returns>True if over border, false otherwise.</returns>
        protected virtual bool IsInBorder(MouseMoveEventArgs pEventArgs)
        {
            // Is near top boundary.
            if ( Math.Abs( pEventArgs.Position.Y - this.Slot.Y ) < Border.BorderThreshold )
            {
                // Is North west?
                if ( Math.Abs( pEventArgs.Position.X - this.Slot.X ) < Border.BorderThreshold )
                {
                    this.CurrentDrag = DragPosition.NW;
                }
                // Is North est?
                else if ( Math.Abs( pEventArgs.Position.X - this.Slot.Right ) < Border.BorderThreshold )
                {
                    this.CurrentDrag = DragPosition.NE;
                }
                // Otherwise, is North.
                else
                {
                    this.CurrentDrag = DragPosition.N;
                }
            }
            // Is Near bottom boundary.
            else if ( Math.Abs( pEventArgs.Position.Y - this.Slot.Bottom ) < Border.BorderThreshold )
            {
                // Is South west?
                if ( Math.Abs( pEventArgs.Position.X - this.Slot.X ) < Border.BorderThreshold )
                {
                    this.CurrentDrag = DragPosition.SW;
                }
                // Is South est?
                else if ( Math.Abs( pEventArgs.Position.X - this.Slot.Right ) < Border.BorderThreshold )
                {
                    this.CurrentDrag = DragPosition.SE;
                }
                // Otherwise, is South.
                else
                {
                    this.CurrentDrag = DragPosition.S;
                }
            }
            // Else, Is West?
            else if ( Math.Abs( pEventArgs.Position.X - this.Slot.X ) < Border.BorderThreshold )
            {
                this.CurrentDrag = DragPosition.W;
            }
            // Finally, is Est?
            else if ( Math.Abs( pEventArgs.Position.X - this.Slot.Right ) < Border.BorderThreshold )
            {
                this.CurrentDrag = DragPosition.E;
            }
            else
            {
                // Not over border.
                this.CurrentDrag = DragPosition.None;
            }
            
            // Will be None if did not satisfied above tests.
            return this.CurrentDrag != DragPosition.None;
        }

        /// <summary>
        /// Gets the current window info.
        /// </summary>
        /// <param name="pWindow">The window</param>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        /// <param name="pWidth"></param>
        /// <param name="pHeight"></param>
        protected static void GetCurrentWindowInfo(Window pWindow, out int pX, out int pY, out int pWidth, out int pHeight)
        {
            pX = pWindow.Left;
            pY = pWindow.Top;

            // Coerces X and Y values.
            if ( pX == 0 )
            {
                pX = pWindow.Slot.Left;
                pWindow.Left = pX;
            }

            if ( pY == 0 )
            {
                pY = pWindow.Slot.Top;
                pWindow.Top = pY;
            }
            
            pWidth  = pWindow.Slot.Width;
            pHeight = pWindow.Slot.Height;
        }

        /// <summary>
        /// Updates the cursor.
        /// </summary>
        private void UpdateCursor()
        {
            switch ( this.mCurrentDrag )
            {
                case DragPosition.None:
                    this.Cursor = XCursor.Default;
                    break;
                case DragPosition.N:
                    this.Cursor = XCursor.V;
                    break;
                case DragPosition.S:
                    this.Cursor = XCursor.V;
                    break;
                case DragPosition.E:
                    this.Cursor = XCursor.H;
                    break;
                case DragPosition.W:
                    this.Cursor = XCursor.H;
                    break;
                case DragPosition.NW:
                    this.Cursor = XCursor.NW;
                    break;
                case DragPosition.NE:
                    this.Cursor = XCursor.NE;
                    break;
                case DragPosition.SW:
                    this.Cursor = XCursor.SW;
                    break;
                case DragPosition.SE:
                    this.Cursor = XCursor.SE;
                    break;
            }
        }

        /// <summary>
        /// Overridable size measure process.
        /// </summary>
        /// <returns></returns>
        protected override Size MeasureOverride()
        {
            this.mContentSize = this.MeasureOverrideHelper();

            // TO DO: Constrains to window min max 

            return base.MeasureOverride();
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            if ( this.IsSourceWindowNull || 
                 this.CompositionTarget == null )
            {
                return base.ArrangeCore( pLayoutType );
            }

            // TO DO: Constrains to window min max.

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Shows the window.
        /// </summary>
        public void Show()
        {
            if ( this.mIsDisposed )
            {
                return;
            }

            if ( this.mIsClosing )
            {
                return;
            }

            this.InternalShow( true );
        }

        /// <summary>
        /// Shows the window as modal window.
        /// </summary>
        /// <returns></returns>
        public bool? ShowDialog()
        {
            if ( this.mIsDisposed )
            {
                return false;
            }

            if ( this.mIsClosing )
            {
                return false;
            }

            if ( this.IsVisible )
            {
                throw new InvalidOperationException( "Dialog window already visible." );
            }
            else if ( this.mIsShowingAsDialog )
            {
                throw new InvalidOperationException( "Already showing as dialog." );
            }

            try
            {
                this.mIsShowingAsDialog = true;
                this.Show();
            }
            catch
            {
                this.mIsShowingAsDialog = false;

                throw;
            }
            finally
            {
                this.mIsShowingAsDialog = false;
            }

            return this.mDialogResult;
        }

        /// <summary>
        /// Hides the window.
        /// </summary>
        public void Hide()
        {
            if ( this.mIsDisposed )
            {
                return;
            }

            this.InternalShow( false );
        }

        /// <summary>
        /// Closes the window.
        /// </summary>
        public void Close()
        {
            this.InternalClose( false, false );
        }

        /// <summary>
        /// Activates the window, that is, set it as top most window.
        /// </summary>
        /// <returns></returns>
        public bool Activate()
        {
            this.VerifySourceWindowShowState();

            if ( this.IsSourceWindowNull ||
                 this.CompositionTarget == null )
            {
                return false;
            }
            
            this.mSource.SetToForeground( this, true );
            //this.PresentationSource.TopWindows++;

            return true;
        }

        ///// <summary>
        ///// Deactivate the window, that is, set to background.
        ///// </summary>
        //private void Deactivate()
        //{
        //    this.PresentationSource.TopWindows--;
        //}

        /// <summary>
        /// Checks whether the source window is created but not shown yet or not.
        /// </summary>
        private void VerifySourceWindowShowState()
        {
            if ( this.mIsNativeWindowCreatedButNotShown )
            {
                throw new InvalidOperationException( "Must be shown before such an action." );
            }
        }

        /// <summary>
        /// Create window only if needed.
        /// </summary>
        private void SafeCreateWindowDuringShow()
        {
            if ( this.IsSourceWindowNull )
            {
                this.CreateSourceWindow( true );
            }
            else if ( this.IsNativeWindowCreatedButNotShown )
            {
                this.SetRootVisualAndUpdate();
                this.IsNativeWindowCreatedButNotShown = false;
            }
        }

        /// <summary>
        /// Set the window root visual ans update if needed.
        /// </summary>
        internal void SetRootVisualAndUpdate()
        {
            this.SetRootVisual();

            if ( this.IsSourceWindowNull == false )
            {
                // TO DO: Update native window if needed.
            }
        }

        /// <summary>
        /// Creates the source native window.
        /// </summary>
        /// <param name="pShow">The flag indicating whether the window must be shown straight or not.</param>
        internal void CreateSourceWindow(bool pShow)
        {
            if ( this.mIsDisposed )
            {
                return;
            }

            if ( this.mIsClosing )
            {
                return;
            }

            double lRequestedTop = 0;
            double lRequestedLeft = 0;
            double lRequestedWidth = 0;
            double lRequestedHeight = 0;

            this.GetRequestedDimensions( ref lRequestedLeft, 
                                         ref lRequestedTop, 
                                         ref lRequestedWidth, 
                                         ref lRequestedHeight );

            this.InitializesStyle();

            NativeWindowParameters lParameters = this.CreateWindowSourceParameters();

            this.mSource = new PresentationSource( lParameters );
            this.mSource.Disposed += this.OnSourceDisposed;

            this.IsNativeWindowCreatedButNotShown = pShow == false;

            this.SetInitialState( lRequestedTop, lRequestedLeft, lRequestedWidth, lRequestedHeight );

            this.OnSourceInitialized( EventArgs.Empty );
        }

        /// <summary>
        /// Delegate called on source disposale.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSourceDisposed(object pSender, EventArgs pEventArgs)
        {
            if ( this.IsDisposed == false )
            {
                this.InternalDispose();
            }
        }

        /// <summary>
        /// Initializes the window native style.
        /// </summary>
        internal virtual void InitializesStyle()
        {
            this.StyleInternal = (int)StyleFlags.HasMenu;

            switch ( this.WindowStyle )
            {
                case WindowStyle.None:
                    {
                        this.StyleInternal &= ~(int)StyleFlags.HasBorder;
                    }
                    break;
                case WindowStyle.BorderedWindow:
                    {
                        this.StyleInternal |= (int)StyleFlags.HasBorder;
                    }
                    break;
            }

            switch ( this.WindowState )
            {
                case WindowState.Normal:
                    break;
                case WindowState.Minimized:
                    {
                        this.StyleInternal |= (int)StyleFlags.Minimized;
                    }
                    break;
                case WindowState.Maximized:
                    {
                        this.StyleInternal |= (int)StyleFlags.Maximized;
                    }
                    break;
            }

            this.InitializeResizibility();
        }

        /// <summary>
        /// Set the initial window position and size.
        /// </summary>
        /// <param name="pRequestedTop"></param>
        /// <param name="pRequestedLeft"></param>
        /// <param name="pRequestedWidth"></param>
        /// <param name="pRequestedHeight"></param>
        internal virtual void SetInitialState(double pRequestedTop, double pRequestedLeft, double pRequestedWidth, double pRequestedHeight)
        {
            // TO DO: Set the X, Y, Width, and Height values.

        }
        
        /// <summary>
        /// Creates the window parameters.
        /// </summary>
        /// <returns></returns>
        internal virtual NativeWindowParameters CreateWindowSourceParameters()
        {
            NativeWindowHooks lHooks = new NativeWindowHooks( this.OnNativeWindowSizeChanged,
                                                              this.OnNativeWindowClose,
                                                              this.OnNativeWindowVisibilityChanged,
                                                              this.OnNativeWindowActivate );
            NativeWindowParameters lParam = new NativeWindowParameters( this.Title, lHooks );
            lParam.WindowStyleFlags = this.StyleInternal;

            return lParam;
        }

        /// <summary>
        /// Delegate called on native window size changes.
        /// </summary>
        /// <param name="pWindow"></param>
        /// <param name="pNewSize"></param>
        private void OnNativeWindowSizeChanged(INativeWindow pWindow, Size pNewSize)
        {

        }

        /// <summary>
        /// Delegate called on native window visibility changes.
        /// </summary>
        /// <param name="pWindow"></param>
        /// <param name="pIsVisible"></param>
        private void OnNativeWindowVisibilityChanged(INativeWindow pWindow, bool pIsVisible)
        {

        }

        /// <summary>
        /// Delegate called on native window close.
        /// </summary>
        /// <param name="pWindow"></param>
        private void OnNativeWindowClose(INativeWindow pWindow)
        {
            if ( this.IsSourceWindowNull ||
                 this.CompositionTarget == null )
            {
                return;
            }

            this.mIsClosing = true;

            CancelEventArgs lArgs = new CancelEventArgs( false );
            try
            {
                this.OnClosing( lArgs );
            }
            catch
            {
                this.CloseWindowFromNativeClose();
                throw;
            }

            if ( this.ShouldCloseWindow( lArgs.Cancel ) )
            {
                this.CloseWindowFromNativeClose();
            }
            else
            {
                this.mIsClosing = false;

                this.mDialogResult = null;
            }
        }

        /// <summary>
        /// Closes the window from the native window close call.
        /// </summary>
        private void CloseWindowFromNativeClose()
        {
            if ( this.mIsShowingAsDialog )
            {
                this.DoDialogHide();
            }

            this.ClearRootVisual();
        }

        /// <summary>
        /// Delegate called on native window activate.
        /// </summary>
        /// <param name="pWindow"></param>
        private void OnNativeWindowActivate(INativeWindow pWindow)
        {

        }

        /// <summary>
        /// Gets the requested dimensions for this window.
        /// </summary>
        /// <param name="pRequestedLeft"></param>
        /// <param name="pRequestedTop"></param>
        /// <param name="pRequestedWidth"></param>
        /// <param name="pRequestedHeight"></param>
        internal virtual void GetRequestedDimensions(ref double pRequestedLeft, ref double pRequestedTop, ref double pRequestedWidth, ref double pRequestedHeight)
        {
            pRequestedTop = this.Top;
            pRequestedLeft = this.Left;
            pRequestedWidth = this.Width;
            pRequestedHeight = this.Height;
        }

        /// <summary>
        /// Internal window close.
        /// </summary>
        /// <param name="pIsAppShutdown"></param>
        /// <param name="pIgnoreCancel"></param>
        internal void InternalClose(bool pIsAppShutdown, bool pIgnoreCancel)
        {
            if ( this.mIsClosing )
            {
                return;
            }

            if ( this.mIsDisposed == true )
            {
                return;
            }

            this.mIsAppShuttingDown = pIsAppShutdown;
            this.mIgnoreCancel = pIgnoreCancel;

            this.mIsClosing = true;

            // Event handler exception continuality: if exception occurs in Closing event handler, the
            // cleanup action is to finish closing.
            CancelEventArgs lArgs = new CancelEventArgs( false );
            try
            {
                this.OnClosing( lArgs );
            }
            catch
            {
                this.CloseWindow();
                throw;
            }

            if ( ShouldCloseWindow( lArgs.Cancel ) )
            {
                this.CloseWindow();
            }
            else
            {
                this.mIsClosing = false;
            }
        }

        /// <summary>
        /// Sets the window as root visual.
        /// </summary>
        internal void SetRootVisual()
        {
            if ( this.IsSourceWindowNull == false )
            {
                this.mSource.RootVisual = this;
            }
        }

        /// <summary>
        /// Clears the window by setting no root visual.
        /// </summary>
        internal void ClearRootVisual()
        {
            if ( this.mSource.RootVisual != null )
            {
                this.mSource.RootVisual = null;
            }
        }

        /// <summary>
        /// Initializes resizibility.
        /// </summary>
        private void InitializeResizibility()
        {
            this.StyleInternal &= ~(int)(StyleFlags.CanMinimize | StyleFlags.CanMaximize);
            
            switch ( this.ResizeMode )
            {
                case ResizeMode.NoResize:
                    break;
                case ResizeMode.CanMinimize:
                    this.StyleInternal |= (int)StyleFlags.CanMinimize;
                    break;
                case ResizeMode.CanResize:
                case ResizeMode.CanResizeWithGrip:
                    this.StyleInternal |= (int)(StyleFlags.CanMinimize | StyleFlags.CanMaximize);
                    break;
                default:
                    Debug.Assert(false, "Invalid value for ResizeMode");
                    break;
            }
        }

        /// <summary>
        /// Delegate called on resize mode changes.
        /// </summary>
        private void OnResizeModeChanged()
        {
            if ( this.IsSourceWindowNull == false && 
                 this.CompositionTarget != null )
            {
                this.InitializeResizibility();
            }
        }

        /// <summary>
        /// Delegate called on source initialized.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected virtual void OnSourceInitialized(EventArgs pEventArgs)
        {
            if ( this.SourceInitialized != null )
            {
                this.SourceInitialized( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on window activated.
        /// </summary>
        /// <param name="pArgs"></param>
        protected virtual void OnActivated(EventArgs pArgs)
        {
            this.NotifyActivated( pArgs );
        }

        /// <summary>
        /// Delegate called on window closed.
        /// </summary>
        /// <param name="pArgs"></param>
        protected virtual void OnClosed(EventArgs pArgs)
        {
            this.NotifyClosed( pArgs );
        }

        /// <summary>
        /// Delegate called on window closing.
        /// </summary>
        /// <param name="pArgs"></param>
        protected virtual void OnClosing(CancelEventArgs pArgs)
        {
            this.NotifyClosing( pArgs );
        }

        /// <summary>
        /// Delegate called on window deactivated.
        /// </summary>
        /// <param name="pArgs"></param>
        protected virtual void OnDeactivated(EventArgs pArgs)
        {
            this.NotifyDeactivated( pArgs );
        }

        /// <summary>
        /// Delegate called on icon changes.
        /// </summary>
        /// <param name="pIcon">The new icon.</param>
        private void OnIconChanged(ImageSource pIcon)
        {
            if ( this.IsSourceWindowNull == false &&
                 this.CompositionTarget != null )
            {
                this.UpdateIcon();
            }
        }

        /// <summary>
        /// Delegate called on window location changed.
        /// </summary>
        /// <param name="pArgs"></param>
        protected virtual void OnLocationChanged(EventArgs pArgs)
        {
            this.NotifyLocationChanged( pArgs );
        }

        /// <summary>
        /// Updates the window title.
        /// </summary>
        /// <param name="pTitle"></param>
        internal virtual void UpdateTitle(string pTitle)
        {
            if ( this.IsSourceWindowNull == false && 
                 this.CompositionTarget != null )
            {
                this.mSource.WindowWrapper.UpdateTitle( pTitle );
            }
        }

        /// <summary>
        /// Delegate called on title changes.
        /// </summary>
        private void OnTitleChanged()
        {
            this.UpdateTitle( this.Title );
        }

        /// <summary>
        /// Delegate called on window style changes.
        /// </summary>
        /// <param name="pArgs"></param>
        protected virtual void OnStyleChanged(EventArgs pArgs)
        {
            if ( this.IsSourceWindowNull == false &&
                 this.CompositionTarget != null )
            {
                this.InitializesStyle();
            }
        }

        /// <summary>
        /// Delegate called on window state changed.
        /// </summary>
        /// <param name="pArgs"></param>
        protected virtual void OnStateChanged(EventArgs pArgs)
        {
            if ( this.IsSourceWindowNull == false &&
                 this.CompositionTarget != null )
            {
                if ( this.IsVisible )
                {
                    int lStyle = this.StyleInternal;
                    switch ( this.WindowState )
                    {
                        case WindowState.Normal:
                            {
                                if( (lStyle & (int)StyleFlags.Maximized) == (int)StyleFlags.Maximized ||
                                    (lStyle & (int)StyleFlags.Minimized) == (int)StyleFlags.Minimized )
                                {
                                    // Restore the window.
                                    this.SourceWindow.WindowWrapper.Restore();
                                }
                            }
                            break;
                        case WindowState.Minimized:
                            {
                                if( (lStyle & (int)StyleFlags.Minimized) != (int)StyleFlags.Minimized )
                                {
                                    // Minimize the window.
                                    this.SourceWindow.WindowWrapper.Minimize();
                                }
                            }
                            break;
                        case WindowState.Maximized:
                            {
                                if( (lStyle & (int)StyleFlags.Maximized) != (int)StyleFlags.Maximized )
                                {
                                    // Maximize the window.
                                    this.SourceWindow.WindowWrapper.Maximize();
                                }
                            }
                            break;
                    }
                }
            }

            this.NotifyStateChanged( pArgs );
        }

        /// <summary>
        /// Closes the window
        /// </summary>
        private void CloseWindow()
        {
            this.InternalDispose();
 
            // raise Closed event
            this.OnClosed( EventArgs.Empty );
        }

        /// <summary>
        /// Checks whether the window should close or not.
        /// </summary>
        /// <param name="pCancelled"></param>
        /// <returns></returns>
        private bool ShouldCloseWindow(bool pCancelled)
        {
            return pCancelled == false || 
                   this.mIsAppShuttingDown || 
                   this.mIgnoreCancel;
        }

        /// <summary>
        /// Internal dispose.
        /// </summary>
        private void InternalDispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.mIsDisposed = true;

                this.UpdateOwnedWindowOnClose();

                try
                {
                    this.ClearSourceWindow();
                }
                finally
                {
                    this.mIsClosing = false;
                }
            }
        }

        /// <summary>
        /// Clears the 
        /// </summary>
        private void ClearSourceWindow()
        {
            if ( this.mSource != null )
            {
                this.mSource.Disposed -= this.OnSourceDisposed;
                this.mSource = null;
            }
        }

        /// <summary>
        /// Manages owned window list 
        /// </summary>
        private void UpdateOwnedWindowOnClose()
        {
            WindowCollection lOwnedWindows = this.OwnedWindowsInternal;
            while ( lOwnedWindows.Count > 0 )
            {
                // if parent window is closing, child window Closing cannot be cancelled.
                (lOwnedWindows[ 0 ] as Window).InternalClose( false, true );
            }

            if ( this.Owner != null )
            {
                this.Owner.OwnedWindowsInternal.Remove( this );
            }
        }

        /// <summary>
        /// Notifies the window becomes top most.
        /// </summary>
        private void NotifyActivated(EventArgs pArgs)
        {
            if ( this.Activated != null )
            {
                this.Activated( this, pArgs );
            }
        }

        /// <summary>
        /// Notifies the window closed.
        /// </summary>
        private void NotifyClosed(EventArgs pArgs)
        {
            if ( this.Closed != null )
            {
                this.Closed( this, pArgs );
            }
        }

        /// <summary>
        /// Notifies the window closing and allow to stop the closing process.
        /// </summary>
        private void NotifyClosing(CancelEventArgs pArgs)
        {
            if ( this.Closing != null )
            {
                this.Closing( this, pArgs );
            }
        }

        /// <summary>
        /// Notifies the window goes behind another.
        /// </summary>
        private void NotifyDeactivated(EventArgs pArgs)
        {
            if ( this.Deactivated != null )
            {
                this.Deactivated( this, pArgs );
            }
        }

        /// <summary>
        /// Notifies the window location changes.
        /// </summary>
        private void NotifyLocationChanged(EventArgs pArgs)
        {
            if ( this.LocationChanged != null )
            {
                this.LocationChanged( this, pArgs );
            }
        }

        /// <summary>
        /// Notifies the window state changes.
        /// </summary>
        private void NotifyStateChanged(EventArgs pArgs)
        {
            if ( this.StateChanged != null )
            {
                this.StateChanged( this, pArgs );
            }
        }

        /// <summary>
        /// Internal show helper method.
        /// </summary>
        /// <param name="pIsVisible"></param>
        /// <returns></returns>
        private object InternalShow(bool pIsVisible)
        {
            if ( this.mIsDisposed )
            {
                return null;
            }
 
            this.mIsClosing = false;
 
            if ( this.IsVisible == pIsVisible )
            {
                return null;
            }
 
            if ( pIsVisible )
            {
                if ( Application.Instance.IsShuttingDown )
                {
                    return null;
                }

                this.SafeCreateWindowDuringShow();

                this.IsVisible = true;
            }
            else
            {
                if ( this.mIsShowingAsDialog )
                {
                    this.DoDialogHide();
                }

                this.IsVisible = false;
            }

            // TO DO: Finish...
            if ( this.IsSourceWindowNull == false )
            {
                if ( pIsVisible )
                {
                    // Show native window
                }
                else
                {
                    // Hide native window.
                }
            }
            
            if ( this.mIsShowingAsDialog && 
                 this.IsVisible )
            {
                try
                {
                    this.CreateModelDialog();
                }
                finally
                {
                    
                }
            }
 
            return null;
        }

        /// <summary>
        /// Updates the icon of window(s).
        /// </summary>
        private void UpdateIcon()
        {
            foreach ( Window lOwnedWindow in this.OwnedWindowsInternal )
            {
                lOwnedWindow.Icon = this.Icon;
            }
        }

        /// <summary>
        /// Creates the window as Dialog.
        /// </summary>
        private void CreateModelDialog()
        {

        }

        /// <summary>
        /// Hide the dialog window.
        /// </summary>
        private void DoDialogHide()
        {
            Debug.Assert( this.mIsShowingAsDialog == true, "IsShowingAsDialog must be true when DoDialogHide is called");

            if ( this.mDialogResult == null )
            {
                this.mDialogResult = false;
            }

            this.mIsShowingAsDialog = false;


        }

        /// <summary>
        /// Measures the window content.
        /// </summary>
        /// <returns></returns>
        private Size MeasureOverrideHelper()
        {
            if ( this.IsSourceWindowNull ||
                 this.CompositionTarget == null )
            {
                return new Size( 0 );
            }

            if ( this.VisualChildrenCount > 0 )
            {
                UIElement lChild = this.GetVisualChild( 0 ) as UIElement;
                if ( lChild != null )
                {
                    Size lChildDesiredSize = lChild.Measure();
                    return lChildDesiredSize;
                }
            }

            return this.mSource.WindowWrapper.WindowSize;
        }

        #endregion Methods

        #region Inner Classes

        /// <summary>
        /// Definition of the window <see cref="DragPosition"/> enumeration.
        /// </summary>
        private enum DragPosition
        {
            /// <summary>
            /// No drag handle, drag the window itself.
            /// </summary>
            None,

            /// <summary>
            /// The window north drag handle.
            /// </summary>
            N,

            /// <summary>
            /// The window south drag handle.
            /// </summary>
            S,

            /// <summary>
            /// The window east drag handle.
            /// </summary>
            E,

            /// <summary>
            /// The window west drag handle.
            /// </summary>
            W,

            /// <summary>
            /// The window north-west drag handle.
            /// </summary>
            NW,

            /// <summary>
            /// The window north-east drag handle.
            /// </summary>
            NE,

            /// <summary>
            /// The window south-west drag handle.
            /// </summary>
            SW,

            /// <summary>
            /// The window south-east drag handle.
            /// </summary>
            SE,
        }

        #endregion Inner Classes
    }
}

