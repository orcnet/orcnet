﻿using Cairo;
using OrcNet.Core.Logger;
using OrcNet.UI.GraphicObjects;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Image"/> class.
    /// </summary>
	public class Image : FrameworkElement
	{
        #region Fields

        /// <summary>
        /// Stores the image source.
        /// </summary>
        private ImageSource mSource;

        /// <summary>
        /// Stores the Sub Svg if any to aim.
        /// </summary>
		private string mSubSvg;

        /// <summary>
        /// Stores the flag indicating whether the image must fit the region or not.
        /// </summary>
        private bool mIsScaled;

        /// <summary>
        /// Stores the flag indicating whether the image must preserves its proportions or not.
        /// </summary>
        private bool mKeepProportions;

        /// <summary>
        /// Stores how the image should be stretched to fill the destination rectangle.
        /// </summary>
        private Stretch mStretch;

        /// <summary>
        /// Stores the image opacity to apply
        /// </summary>
		private double mOpacity;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets how the image should be stretched to fill the destination rectangle.
        /// </summary>
        public Stretch Stretch
        {
            get
            {
                return this.mStretch;
            }
            set
            {
                if ( this.mStretch != value )
                {
                    this.mStretch = value;
                    this.NotifyPropertyChanged();

                    if ( this.mSource != null )
                    {
                        this.mSource.Stretch = value;
                        this.InvalidateArrange();
                    }
                }
            }
        }
        
        /// <summary>
        /// Gets or sets the image path.
        /// </summary>
		public string Path
        {
			get
            {
                return this.mSource == null ? "" : this.mSource.Path;
            }
			set
            {
                if ( value == this.Path )
                {
                    return;
                }

				try
                {
                    if ( string.IsNullOrEmpty( value ) )
                    {
                        this.Source = null;
                    }
                    else
                    {
                        // Freeze like???
                        lock ( this.mPresentationSource.LayoutMutex )
                        {
                            this.LoadImage( value );
                        }
                    }
				}
                catch ( Exception pEx )
                {
                    LogManager.Instance.Log( pEx );
					this.mSource = null;
				}

                this.NotifyPropertyChanged();
			}
		}

        /// <summary>
        /// Gets or sets the Svg sub image.
        /// </summary>
		public string SvgSub
        {
			get
            {
                return this.mSubSvg;
            }
			set
            {
                if ( this.mSubSvg == value )
                {
                    return;
                }

                this.mSubSvg = value;
                this.InvalidateArrange();
			}
		}

        /// <summary>
        /// Gets or sets the image source.
        /// </summary>
		public ImageSource Source
        {
			get
            {
                return this.mSource;
            }
			set
            {
                if ( this.mSource == value )
                {
                    return;
                }

                this.mSource = value;
                this.NotifyPropertyChanged();
                this.InvalidateArrange();
			}
		}

        /// <summary>
        /// Gets or sets the image opacity to apply
        /// </summary>
		public virtual double Opacity
        {
			get
            {
                return this.mOpacity;
            }
			set
            {
                if ( this.mOpacity == value )
                {
                    return;
                }

				this.mOpacity = value;

                this.NotifyPropertyChanged();
                this.InvalidateVisual();
			}
		}

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class.
        /// </summary>
        public Image () : 
        base()
		{
            this.mStretch = Stretch.Uniform;
            this.mOpacity = 1.0;
		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Loads an image from file.
        /// </summary>
        /// <param name="pPath">The image path.</param>
        protected void LoadImage(string pPath)
		{
			ImageSource lSource;
            if ( pPath.EndsWith(".svg", true, System.Globalization.CultureInfo.InvariantCulture) )
            {
                lSource = new SvgImageSource();
            }
            else
            {
                lSource = new BmpImageSource();
            }

			lSource.LoadImage( pPath );
            lSource.Stretch = this.mStretch;

			this.mSource = lSource;
		}

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            if ( this.mSource == null )
            {
                return new Size( (int)(this.Margin.Left + this.Margin.Right),
                                 (int)(this.Margin.Top + this.Margin.Bottom) );
            }

            return new Size( (int)(this.mSource.Width + this.Margin.Left + this.Margin.Right),
                             (int)(this.mSource.Height + this.Margin.Top + this.Margin.Bottom) );
        }
        
        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            Rectangle lBackground = new Rectangle( this.mSlot.Size );

            // Transparent background if image.
            SolidColorBrush lDefaultImageBackground = new SolidColorBrush( Color.Transparent );
            lDefaultImageBackground.SetAsSource( pDrawingContext, lBackground );

            CairoHelpers.CairoRectangle( pDrawingContext, lBackground, -1 );

            pDrawingContext.Fill();

            if ( this.mSource == null )
            {
                return;
            }

            this.mSource.Render( pDrawingContext, this.ClientRectangle, this.mSubSvg );

            if ( this.Opacity < 1.0 )
            {
                pDrawingContext.SetSourceRGBA( 0.0, 0.0, 0.0, 1.0 - this.Opacity );
                pDrawingContext.Operator = Operator.DestOut;
                pDrawingContext.Rectangle( this.ClientRectangle );
                pDrawingContext.Fill();
                pDrawingContext.Operator = Operator.Over;
            }
        }

        #endregion Methods
    }
}
