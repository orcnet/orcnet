﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TreeViewItem"/> class.
    /// </summary>
    public class TreeViewItem : HeaderedItemsControl
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the item is expanded or not.
        /// </summary>
        private bool mIsExpanded;

        /// <summary>
        /// Stores the flag indicating whether the item is selected or not.
        /// </summary>
        private bool mIsSelected;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on item expanded.
        /// </summary>
        public event EventHandler Expanded;

        /// <summary>
        /// Event fired on item collapsed.
        /// </summary>
        public event EventHandler Collapsed;

        /// <summary>
        /// Event fired on item selected.
        /// </summary>
        public event EventHandler Selected;

        /// <summary>
        /// Event fired on item unselected.
        /// </summary>
        public event EventHandler Unselected;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the element contains a selected item or not.
        /// </summary>
        private bool ContainsSelection
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the immediate parent ItemsControl.
        /// </summary>
        internal ItemsControl ParentItemsControl
        {
            get
            {
                return ItemsControl.ItemsControlFromItemContainer( this );
            }
        }

        /// <summary>
        /// Gets the parent tree view item. Will be Null if the parent is a TreeView.
        /// </summary>
        internal TreeViewItem ParentTreeViewItem
        {
            get
            {
                return this.ParentItemsControl as TreeViewItem;
            }
        }

        /// <summary>
        /// Gets the top TreeView.
        /// </summary>
        internal TreeView ParentTreeView
        {
            get
            {
                ItemsControl lParent = this.ParentItemsControl;
                while ( lParent != null )
                {
                    TreeView lTreeView = lParent as TreeView;
                    if ( lTreeView != null )
                    {
                        return lTreeView;
                    }

                    lParent = ItemsControl.ItemsControlFromItemContainer( lParent );
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the item is expanded or not.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return this.mIsExpanded;
            }
            set
            {
                if ( this.mIsExpanded == value )
                {
                    return;
                }

                this.mIsExpanded = value;
                this.NotifyPropertyChanged();

                if ( this.mIsExpanded )
                {
                    this.OnExpanded( null );
                }
                else
                {
                    this.OnCollapsed( null );
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the item is selected or not.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.mIsSelected;
            }
            set
            {
                if ( this.mIsSelected == value )
                {
                    return;
                }

                this.mIsSelected = value;
                this.NotifyPropertyChanged();

                if ( this.mIsSelected )
                {
                    this.OnSelected( null );
                }
                else
                {
                    this.OnUnselected( null );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewItem"/> class.
        /// </summary>
        public TreeViewItem()
        {
            this.mIsExpanded = false;
            this.mIsSelected = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on element got focus event.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnGotFocus(object pSender, EventArgs pEventArgs)
        {
            this.Select( true );
            base.OnGotFocus( pSender, pEventArgs );
        }

        /// <summary>
        /// Selects the item.
        /// </summary>
        /// <param name="pIsSelected"></param>
        private void Select(bool pIsSelected)
        {
            TreeView lTree = this.ParentTreeView;
            ItemsControl lParent = this.ParentItemsControl;
            if ( lTree != null && 
                 lParent != null )
            {
                // Give the TreeView a reference to this container and its data
                //object data = lParent.GetItemOrContainerFromContainer(this);
                lTree.ChangeSelection( this.DataContext, this, pIsSelected );

                // Making focus of TreeViewItem synchronize with selection if needed.
                if ( pIsSelected &&
                     this.IsFocused == false )
                {
                    this.Focus();
                }
            }
        }

        /// <summary>
        /// Updates the contains selection flag over the TreeViewItem hierarchy.
        /// </summary>
        /// <param name="pIsSelected"></param>
        internal void UpdateContainsSelection(bool pIsSelected)
        {
            TreeViewItem lParent = this.ParentTreeViewItem;
            while ( lParent != null )
            {
                lParent.ContainsSelection = pIsSelected;
                lParent = lParent.ParentTreeViewItem;
            }
        }

        /// <summary>
        /// Expands recursively the item children
        /// </summary>
        public void ExpandSubtree()
        {
            ExpandRecursive( this );
        }

        /// <summary>
        /// Recursively expand all item in the subtree.
        /// </summary>
        /// <param name="pItem"></param>
        private static void ExpandRecursive(TreeViewItem pItem)
        {
            if ( pItem == null )
            {
                return;
            }

            if ( pItem.IsExpanded == false )
            {
                pItem.IsExpanded = true;
            }

            pItem.ApplyTemplate();
            pItem.UpdateLayout();

            // TO DO: Change with VirtualizingPanel process.

            for ( int lCurr = 0; lCurr < pItem.Items.Count; lCurr++ )
            {
                TreeViewItem lSubItem = pItem.Items[ lCurr ] as TreeViewItem;
                if ( lSubItem != null )
                {
                    ExpandRecursive( lSubItem );
                }
            }
        }

        /// <summary>
        /// Delegate called on item expanded.
        /// </summary>
        /// <param name="pEventArgs"></param>
		protected virtual void OnExpanded(EventArgs pEventArgs)
		{
			this.NotifyExpanded( pEventArgs );
		}

        /// <summary>
        /// Delegate called on item collapsed
        /// </summary>
        /// <param name="pEventArgs"></param>
		protected virtual void OnCollapsed(EventArgs pEventArgs)
		{
			this.NotifyCollapsed( pEventArgs );
		}

        /// <summary>
        /// Delegate called on item selected.
        /// </summary>
        /// <param name="pEventArgs"></param>
		protected virtual void OnSelected(EventArgs pEventArgs)
		{
			this.NotifySelected( pEventArgs );
		}

        /// <summary>
        /// Delegate called on item unselected
        /// </summary>
        /// <param name="pEventArgs"></param>
		protected virtual void OnUnselected(EventArgs pEventArgs)
		{
			this.NotifyUnselected( pEventArgs );
		}

        /// <summary>
        /// Notifies the item expanded.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifyExpanded(EventArgs pEventArgs)
        {
            if ( this.Expanded != null )
            {
                this.Expanded( this, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies the item collapsed.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifyCollapsed(EventArgs pEventArgs)
        {
            if ( this.Collapsed != null )
            {
                this.Collapsed( this, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies the item selected.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifySelected(EventArgs pEventArgs)
        {
            if ( this.Selected != null )
            {
                this.Selected( this, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies the item unselected.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifyUnselected(EventArgs pEventArgs)
        {
            if ( this.Unselected != null )
            {
                this.Unselected( this, pEventArgs );
            }
        }

        #endregion Methods
    }
}
