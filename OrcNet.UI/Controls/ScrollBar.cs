﻿using Cairo;
using System;
using System.Diagnostics;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ScrollBar"/> class.
    /// </summary>
	public class ScrollBar : ARangeBase
    {
        #region Fields

        /// <summary>
        /// Stores the maximum distance that can be dragged before it snaps back. 
        /// </summary>
        private const double MaxPerpendicularDelta = 150;

        /// <summary>
        /// Stores the constant thumb template name.
        /// </summary>
        private const string ThumbTemplateName = "Track";

        /// <summary>
        /// Stores the scrollbar moves density.
        /// </summary>
        private double mDensity;

        /// <summary>
        /// Stores the thumb center offset from the slider.
        /// </summary>
        private double mThumbCenterOffset;

        /// <summary>
        /// Stores the flag indicating whether the scrollbar
        /// </summary>
        private bool mHasScrolled;

        /// <summary>
        /// Stores the scroll bar handle to drag.
        /// </summary>
        private Thumb mScrollHandle;

        /// <summary>
        /// Stores the scrollbar previous value reinit on drag started, so it be reset if needed.
        /// </summary>
        private double mPreviousValue;

        /// <summary>
        /// Stores the scroll bar orientation
        /// </summary>
        private Orientation mOrientation;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on scroll changes.
        /// </summary>
        public event EventHandler<ScrollingEventArgs> Scroll;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the scrollbar extent currently visible..
        /// </summary>
        public double ViewportSize
        {
            get
            {
                if ( this.Orientation == Orientation.Horizontal )
                {
                    return this.ClientRectangle.Width;
                }

                return this.ClientRectangle.Height;
            }
        }

        /// <summary>
        /// Gets the scroll bar orientation
        /// </summary>
        public Orientation Orientation
        {
            get
            {
                return this.mOrientation;
            }
            set
            {
                if ( this.mOrientation == value )
                {
                    return;
                }

                this.mOrientation = value;
                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrollBar"/> class.
        /// </summary>
        public ScrollBar() : 
        base()
        {
            this.mHasScrolled = false;
            this.mOrientation = Orientation.Vertical;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if ( this.mScrollHandle != null )
            {
                this.mScrollHandle.DragStarted   -= this.OnSliderHandleDragStarted;
                this.mScrollHandle.DragChanged   -= this.OnSliderHandleDragChanged;
                this.mScrollHandle.DragCompleted -= this.OnSliderHandleDragCompleted;
            }

            this.mScrollHandle = this.GetTemplateChild( ThumbTemplateName ) as Thumb;

            if ( this.mScrollHandle != null )
            {
                this.mScrollHandle.DragStarted   += this.OnSliderHandleDragStarted;
                this.mScrollHandle.DragChanged   += this.OnSliderHandleDragChanged;
                this.mScrollHandle.DragCompleted += this.OnSliderHandleDragCompleted;
            }
        }
        
        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            if ( this.Maximum <= 0 )
            {
                return;
            }

            if ( this.mScrollHandle != null )
            {
                this.mScrollHandle.Paint( ref pDrawingContext );
            }
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            double lIncrease;
            double lThumb;
            double lDecrease;
            bool lIsVertical = this.Orientation == Orientation.Vertical;

            double lViewportSize = Math.Max( 0.0, this.ViewportSize );

            if ( this.ComputeScrollBarLengths( this.DesiredSize, lViewportSize, lIsVertical, out lDecrease, out lThumb, out lIncrease ) == false )
            {
                
            }

            this.mThumbCenterOffset = lIncrease + (lThumb * 0.5);

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.Button == MouseButton.Left &&
                 this.mScrollHandle.IsMouseOver == false )
            {
                // Jump to the cursor position in the slider.
                double lNewValue = this.ValueFromPoint( pEventArgs.Position );
                if ( DoubleUtil.IsFinite( lNewValue ) )
                {
                    this.Value = Math.Min( Math.Max( lNewValue, this.Minimum ), this.Maximum );
                }
            }

            base.OnMouseDown( pSender, pEventArgs );
        }
        
        /// <summary>
        /// Delegate called on slider handle drag started.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArg"></param>
        private void OnSliderHandleDragStarted(object pSender, DragStartedEventArgs pEventArg)
        {
            this.mHasScrolled   = false;
            this.mPreviousValue = this.Value;
        }

        /// <summary>
        /// Delegate called on slider handle drag changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSliderHandleDragChanged(object pSender, DragDeltaEventArgs pEventArgs)
        {
            this.UpdateValue( pEventArgs.HorizontalDelta, pEventArgs.VerticalDelta );
        }

        /// <summary>
        /// Delegate called on slider handle drag complited or canceled.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSliderHandleDragCompleted(object pSender, DragCompletedEventArgs pEventArgs)
        {
            if ( this.mHasScrolled )
            {
                this.FinishDrag();
                this.NotifyScroll( new ScrollingEventArgs( ScrollEventType.EndScroll, this.Value ) );
            }
        }

        /// <summary>
        /// Terminates the drag process.
        /// </summary>
        private void FinishDrag()
        {
            
        }

        /// <summary>
        /// Update the scrollbar value based on the slider
        /// </summary>
        /// <param name="pHorizontalDragDelta"></param>
        /// <param name="pVerticalDragDelta"></param>
        private void UpdateValue(double pHorizontalDragDelta, double pVerticalDragDelta)
        {
            if ( this.mScrollHandle != null )
            {
                double lNewDelta = this.ValueFromDistance( pHorizontalDragDelta, pVerticalDragDelta );
                if ( DoubleUtil.IsFinite( lNewDelta ) &&  
                     DoubleUtil.IsZero( lNewDelta ) == false )
                {
                    double lCurrentValue = this.Value;
                    double lNewValue = Math.Min( Math.Max( lCurrentValue + lNewDelta, this.Minimum ), this.Maximum );
 
                    double lPerpendicularDragDelta;
 
                    // Compare distances from thumb for horizontal and vertical orientations
                    if ( this.Orientation == Orientation.Horizontal )
                    {
                        lPerpendicularDragDelta = Math.Abs( pVerticalDragDelta );
                    }
                    else // this.Orientation == Orientation.Vertical
                    {
                        lPerpendicularDragDelta = Math.Abs( pHorizontalDragDelta );
                    }
 
                    if ( DoubleUtil.GreaterThan( lPerpendicularDragDelta, MaxPerpendicularDelta ) )
                    {
                        lNewValue = this.mPreviousValue;
                    }
 
                    if ( DoubleUtil.AreClose( lCurrentValue, lNewValue ) == false )
                    {
                        this.mHasScrolled = true;
                        this.Value = lNewValue;
                        this.NotifyScroll( new ScrollingEventArgs( ScrollEventType.ThumbTrack, this.Value ) );
                    }
                }
            }
        }

        /// <summary>
        /// Gets the value from a shifted point.
        /// </summary>
        /// <param name="pPoint"></param>
        /// <returns></returns>
        public virtual double ValueFromPoint(Point pPoint)
        {
            double lValue;

            // Computes the distance from center of thumb to given point.
            if ( this.Orientation == Orientation.Horizontal )
            {
                lValue = this.Value + this.ValueFromDistance( pPoint.X - this.mThumbCenterOffset, 
                                                              pPoint.Y - (this.ClientRectangle.Height * 0.5) );
            }
            else
            {
                lValue = this.Value + this.ValueFromDistance( pPoint.X - (this.ClientRectangle.Width * 0.5),
                                                              pPoint.Y - this.mThumbCenterOffset );
            }

            return Math.Max( this.Minimum, Math.Min( this.Maximum, lValue ) );
        }

        /// <summary>
        /// Gets the value from shifted distances.
        /// </summary>
        /// <param name="pHorizontal"></param>
        /// <param name="pVertical"></param>
        /// <returns></returns>
        public virtual double ValueFromDistance(double pHorizontal, double pVertical)
        {
            double lScale = 1;
            
            if ( this.Orientation == Orientation.Horizontal )
            {
                return lScale * pHorizontal * this.mDensity;
            }
            else
            {
                return lScale * pVertical * this.mDensity;
            }
        }

        /// <summary>
        /// Notifies scroll changes.
        /// </summary>
        protected void NotifyScroll(ScrollingEventArgs pEventArgs)
        {
            if( this.Scroll != null )
            {
                this.Scroll( this, pEventArgs );
            }
        }

        /// <summary>
        /// Computes the scrollbar length to update the thumb position.
        /// </summary>
        /// <param name="pArrangeSize"></param>
        /// <param name="pViewportSize"></param>
        /// <param name="pIsVertical"></param>
        /// <param name="pDecreaseButtonLength"></param>
        /// <param name="pThumbLength"></param>
        /// <param name="pIncreaseButtonLength"></param>
        /// <returns></returns>
        private bool ComputeScrollBarLengths(Size pArrangeSize, double pViewportSize, bool pIsVertical, out double pDecreaseButtonLength, out double pThumbLength, out double pIncreaseButtonLength)
        {
            double lMinimum = this.Minimum;
            double lRange   = Math.Max( 0.0, this.Maximum - lMinimum );
            double lOffset  = Math.Min( lRange, this.Value - lMinimum );

            Debug.Assert( DoubleUtil.GreaterThanOrClose( lOffset, 0.0 ), "Invalid offest (negative value).");

            double lExtent = Math.Max( 0.0, lRange ) + pViewportSize;

            double lTrackLength;

            // Compute thumb size
            double lThumbMinLength;
            if ( pIsVertical )
            {
                lTrackLength = pArrangeSize.Height;
                object lButtonHeightResource = this.TryFindResource( "VerticalScrollBarButtonHeight" );
                double lButtonHeight = lButtonHeightResource is double ? (double)lButtonHeightResource : 10.0;
                lThumbMinLength = Math.Floor( lButtonHeight * 0.5 );
            }
            else
            {
                lTrackLength = pArrangeSize.Width;
                object lButtonWidthResource = this.TryFindResource( "HorizontalScrollBarButtonWidth" );
                double lButtonWidth = lButtonWidthResource is double ? (double)lButtonWidthResource : 10.0;
                lThumbMinLength = Math.Floor( lButtonWidth * 0.5 );
            }

            pThumbLength = lTrackLength * pViewportSize / lExtent;
            CoerceLength( ref pThumbLength, lTrackLength );

            pThumbLength = Math.Max( lThumbMinLength, pThumbLength );


            // If we don't have enough content to scroll, disable the track.
            bool lHasNotEnoughContentToScroll = DoubleUtil.LessThanOrClose( lRange, 0.0 );
            bool lIsThumbLongerThanTrack = pThumbLength > lTrackLength;

            // if there's not enough content or the thumb is longer than the track, 
            // hide the track and don't arrange the pieces
            if (lHasNotEnoughContentToScroll || lIsThumbLongerThanTrack)
            {
                if ( this.IsVisible )
                {
                    this.IsVisible = false;
                }

                this.mThumbCenterOffset = double.NaN;
                this.mDensity = double.NaN;
                pDecreaseButtonLength = 0.0;
                pIncreaseButtonLength = 0.0;
                return false; // don't arrange
            }
            else if ( this.IsVisible == false )
            {
                this.IsVisible = true;
            }

            // Compute lengths of increase and decrease button
            double lRemainingTrackLength = lTrackLength - pThumbLength;
            pDecreaseButtonLength = lRemainingTrackLength * lOffset / lRange;
            CoerceLength( ref pDecreaseButtonLength, lRemainingTrackLength );

            pIncreaseButtonLength = lRemainingTrackLength - pDecreaseButtonLength;
            CoerceLength( ref pIncreaseButtonLength, lRemainingTrackLength );

            this.mDensity = lRange / lRemainingTrackLength;

            return true;
        }

        #endregion Methods
    }
}
