﻿using Cairo;
using OrcNet.Core.Datastructures;
using OrcNet.UI.Enumerators;
using System;
using System.Collections;

namespace OrcNet.UI
{
    /// <summary>
    /// Implement drawing and layouting for a single child, but
    /// does not implement IXmlSerialisation to allow reuse of container
    /// behaviour for widgets that have other xml hierarchy: example
    /// TemplatedControl may have 3 children (template,templateItem,content) but
    /// behave exactely as a container for layouting and drawing
    /// </summary>
    public class Decorator : FrameworkElement, IAddChild
	{
        #region Fields
        
        /// <summary>
        /// Stores the single child to decorate.
        /// </summary>
        protected UIElement mChild;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the current single child element.
        /// </summary>
        public virtual UIElement Child
        {
            get
            {
                return this.mChild;
            }
            set
            {
                if ( this.mChild != value )
                {
                    if ( this.mChild != null )
                    {
                        this.RemoveVisualChild( this.mChild );

                        this.RemoveLogicalChild( this.mChild );

                        this.mContentSize = new Size( 0, 0 );
                        this.mChild.LayoutUpdated -= this.OnChildLayoutChanges;
                    }

                    this.mChild = value;

                    if ( this.mChild != null )
                    {
                        this.AddLogicalChild( this.mChild );
                        
                        this.AddVisualChild( this.mChild );

                        this.mChild.LayoutUpdated += this.OnChildLayoutChanges;
                        this.mContentSize = this.mChild.Slot.Size;
                    }

                    this.InvalidateMeasure();
                }
            }
        }

        /// <summary>
        /// Gets the set of logical children if any.
        /// </summary>
        protected internal override IEnumerator LogicalChildren
        {
            get
            {
                if ( this.mChild == null )
                {
                    return new EmptyEnumerator<object>();
                }

                return new SingleChildEnumerator( this.mChild );
            }
        }

        /// <summary>
        /// Gets the visual children count.
        /// </summary>
        public override int VisualChildrenCount
        {
            get
            {
                return this.mChild == null ? 0 : 1;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Decorator"/> class.
        /// </summary>
        public Decorator() : 
        base()
		{

		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds 
        /// </summary>
        /// <param name="pChild"></param>
        void IAddChild.AddChild(object pChild)
        {
            if ( (pChild is UIElement) == false )
            {
                throw new ArgumentException( "Not at least a UIElement." );
            }

            if ( this.Child != null )
            {
                throw new ArgumentException( "Already has a child." );
            }

            this.Child = pChild as UIElement;
        }

        /// <summary>
        /// Internal invalidates measure method.
        /// </summary>
        protected override void InternalInvalidateMeasure()
        {
            this.State = LayoutingType.None;
            this.RegisterForLayouting( LayoutingType.Sizing );
        }

        /// <summary>
        /// Finds the visual having the given name
        /// </summary>
        /// <param name="pName">The name to look for.</param>
        /// <returns></returns>
        public override AVisual FindByName(string pName)
		{
            if ( this.Name == pName )
            {
                return this;
            }

			return this.mChild is FrameworkElement  ? (this.mChild as FrameworkElement).FindByName( pName ) : null;
		}

        /// <summary>
        /// Internal IsAncestorOf
        /// </summary>
        /// <param name="pDescendant">The visual to check.</param>
        /// <returns>True if child, false otherwise.</returns>
        protected override bool IsAncestorOfInternal(AVisual pDescendant)
        {
            return this.mChild == pDescendant ? true :
                   this.mChild == null ? false : this.mChild.IsAncestorOf( pDescendant );
        }
        
        /// <summary>
        /// Delegate called on Data context changed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        public override void OnDataContextChanged(object pSender, PropertyValueChangedEventArgs pEventArgs)
        {
            base.OnDataContextChanged( pSender, pEventArgs );

            if ( this.mChild is FrameworkElement )
            {
                FrameworkElement lChild = this.mChild as FrameworkElement;
                if ( lChild.IsLocalDataContextNull & lChild.IsLocalLogicalParentNull )
                {
                    lChild.OnDataContextChanged( pSender, pEventArgs );
                }
            }
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            FrameworkElement lChild = this.mChild as FrameworkElement;
            if ( lChild != null )
            {
                //force sizing to fit if sizing on children and child has stretched size
                switch ( pLayoutType )
                {
                    case LayoutingType.Width:
                        {
                            if ( this.Width == cFitSizeValue )
                            {
                                lChild.Width = cFitSizeValue;
                            }
                        }
                        break;
                    case LayoutingType.Height:
                        {
                            if ( this.Height == cFitSizeValue )
                            {
                                lChild.Height = cFitSizeValue;
                            }
                        }
                        break;
                }
            }

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected internal override void OnLayoutChanges(LayoutingType pLayoutType)
        {
            base.OnLayoutChanges( pLayoutType );

            FrameworkElement lChild = this.mChild as FrameworkElement;
            if ( lChild == null )
            {
                return;
            }

            LayoutingType lChildLayoutType = LayoutingType.None;

            if ( pLayoutType == LayoutingType.Width )
            {
                if ( lChild.Left == 0 )
                {
                    lChildLayoutType |= LayoutingType.X;
                }
                else
                {
                    lChildLayoutType |= LayoutingType.Width;
                    if ( lChild.Width < 100 && 
                         lChild.Left == 0 )
                    {
                        lChildLayoutType |= LayoutingType.X;
                    }
                }
            }
            else if ( pLayoutType == LayoutingType.Height )
            {
                if ( lChild.Top == 0 )
                {
                    lChildLayoutType |= LayoutingType.Y;
                }
                else
                {
                    lChildLayoutType |= LayoutingType.Height;
                    if ( lChild.Height < 100 && 
                         lChild.Top == 0 )
                    {
                        lChildLayoutType |= LayoutingType.Y;
                    }
                }
            }

            if ( lChildLayoutType == LayoutingType.None )
            {
                return;
            }

            this.mChild.RegisterForLayouting( lChildLayoutType );
        }
        
        /// <summary>
        /// Delegate called on child layout changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
		protected virtual void OnChildLayoutChanges(object pSender, LayoutingEventArgs pEventArgs)
		{			
			UIElement lAsElement = pSender as UIElement;

			if ( pEventArgs.LayoutType == LayoutingType.Width )
            {
                if ( this.HorizontalAlignment == HorizontalAlignment.Stretch )
                {
                    return;
                }

				this.mContentSize.Width = lAsElement.Slot.Width;
				this.RegisterForLayouting( LayoutingType.Width );
			}
            else if ( pEventArgs.LayoutType == LayoutingType.Height )
            {
                if ( this.VerticalAlignment == VerticalAlignment.Stretch )
                {
                    return;
                }

				this.mContentSize.Height = lAsElement.Slot.Height;
				this.RegisterForLayouting( LayoutingType.Height );
			}
		}

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            pDrawingContext.Save();

            if ( this.ClipToClientRect )
            {
                // Clip to client zone
                CairoHelpers.CairoRectangle( pDrawingContext, this.ClientRectangle, -1 );
                pDrawingContext.Clip();
            }

            if ( this.mChild != null )
            {
                if ( this.mChild.IsVisible )
                {
                    this.mChild.Paint( ref pDrawingContext );
                }
            }

            pDrawingContext.Restore();
        }
        
        /// <summary>
        /// Internal drawing cache update.
        /// </summary>
        /// <param name="pElementRegion">The element drawing region.</param>
        /// <param name="pSurface">The drawing surface.</param>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void InternalUpdateCache(ref Rectangle pElementRegion, ImageSurface pSurface, Context pDrawingContext)
        {
            Context lTempContext = new Context( pSurface );

            if ( this.VisualClip.Count > 0 )
            {
                this.VisualClip.ClearAndClip( lTempContext );

                this.OnRender( lTempContext );
            }

            lTempContext.Dispose();
        }
        
        /// <summary>
        /// Delegate called on mouse hover.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseHover(MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseHover( pEventArgs );

            if ( this.mChild != null )
            {
                if ( this.mChild.IsMouseIn( pEventArgs.Position ) )
                {
                    this.mChild.OnMouseHover( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Gets the visual child at the given index if any.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        protected override AVisual GetVisualChild(int pIndex)
        {
            if ( this.mChild == null || 
                 pIndex != 0 )
            {
                throw new ArgumentOutOfRangeException("pIndex", pIndex, "Out of range index.");
            }

            return this.mChild;
        }
        
        #endregion Methods
    }
}

