﻿using OrcNet.Core;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ICommand"/> interface.
    /// </summary>
    public interface ICommand : IObservable
    {
        #region Events

        /// <summary>
        /// Event fired on CanExecute changes.
        /// </summary>
        event EventHandler CanExecuteChanged;

        #endregion Events

        #region Methods

        /// <summary>
        /// Checks whether the command can be executed or not.
        /// </summary>
        /// <param name="pParameter">The optional parameter.</param>
        /// <returns>True if can execute, false otherwise.</returns>
        bool CanExecute(object pParameter);

        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="pParameter">The optional parameter.</param>
        void Execute(object pParameter);

        #endregion Methods
    }
}
