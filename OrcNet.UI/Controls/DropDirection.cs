﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DropDirection"/> enumeration
    /// </summary>
    public enum DropDirection
    {
        /// <summary>
        /// Opening upward
        /// </summary>
        UpWard = 0,

        /// <summary>
        /// Opening downward
        /// </summary>
        Downward,

        /// <summary>
        /// Opening leftward
        /// </summary>
        Leftward,

        /// <summary>
        /// Opening rightward
        /// </summary>
        Rightward
    }
}
