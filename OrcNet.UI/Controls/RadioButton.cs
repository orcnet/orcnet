﻿using System;
using System.Collections;

// Typedefs
using RadioGroup = System.Collections.Generic.List<System.WeakReference<OrcNet.UI.RadioButton>>;
using GroupContainer = System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<System.WeakReference<OrcNet.UI.RadioButton>>>;
using OrcNet.UI.Helpers;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="RadioButton"/> class.
    /// </summary>
    public class RadioButton : ToggleButton
    {
        #region Fields

        /// <summary>
        /// Stores the group name(s) to elements.
        /// </summary>
        private static GroupContainer sGroupNameToElements;

        /// <summary>
        /// Stores the radio button group name if any.
        /// </summary>
        private string mGroupName;

        #endregion Fields
        
        #region Properties

        /// <summary>
        /// Gets or sets the radio button group name if any.
        /// </summary>
        public string GroupName
        {
            get
            {
                return this.mGroupName;
            }
            set
            {
                if ( this.mGroupName == value )
                {
                    return;
                }

                string lOldName = this.mGroupName;
                this.mGroupName = value;
                this.NotifyPropertyChanged();
                this.OnGroupNameChanged( lOldName, this.mGroupName );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioButton"/> class.
        /// </summary>
        public RadioButton() : 
        base()
        {
            this.mGroupName = string.Empty;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on button checked.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected override void OnChecked(EventArgs pEventArgs)
        {
            this.UpdateRadioButtonGroup();

            base.OnChecked( pEventArgs );
        }

        /// <summary>
        /// Delegate called on group name changes.
        /// </summary>
        /// <param name="pOldGroup"></param>
        /// <param name="pNewGroup"></param>
        private void OnGroupNameChanged(string pOldGroup, string pNewGroup)
        {
            // Unregister the old group name if set
            if ( string.IsNullOrEmpty( pOldGroup ) == false )
            {
                Unregister( pOldGroup, this );
            }

            // Register the new group name is set
            if ( string.IsNullOrEmpty( pNewGroup ) == false )
            {
                Register( pNewGroup, this );
            }
        }

        /// <summary>
        /// Updates the radio button group.
        /// </summary>
        private void UpdateRadioButtonGroup()
        {
            string lGroupName = this.GroupName;
            if ( string.IsNullOrEmpty( lGroupName ) == false )
            {
                if ( sGroupNameToElements == null )
                {
                    sGroupNameToElements = new GroupContainer();
                }

                lock ( sGroupNameToElements )
                {
                    // Get all elements bound to this key and remove this element
                    RadioGroup lGroup;
                    if ( sGroupNameToElements.TryGetValue( lGroupName, out lGroup ) )
                    {
                        for ( int i = 0; i < lGroup.Count;)
                        {
                            RadioButton lRadioButton;
                            if ( lGroup[i].TryGetTarget( out lRadioButton ) )
                            {
                                // Uncheck all checked RadioButtons different from the current one
                                if ( lRadioButton != this &&
                                     lRadioButton.IsChecked == true )
                                {
                                    lRadioButton.IsChecked = false;
                                }

                                i++;
                            }
                            else
                            {
                                lGroup.RemoveAt( i );
                            }
                        }
                    }
                }
            }
            else // Logical parent should be the group
            {
                AVisual lParent = this.LogicalParent;
                if ( lParent != null )
                {
                    // Traverse logical children
                    IEnumerable lChildren = LogicalTreeHelper.GetChildren( lParent );
                    foreach ( object lChild in lChildren )
                    {
                        RadioButton lRadioButton = lChild as RadioButton;
                        if ( lRadioButton != null && 
                             lRadioButton != this && 
                             string.IsNullOrEmpty( lRadioButton.GroupName ) &&
                             lRadioButton.IsChecked == true )
                        {
                            lRadioButton.IsChecked = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Registers a radio button to a given group.
        /// </summary>
        /// <param name="pGroupName">The group name.</param>
        /// <param name="pRadioButton">The radio button.</param>
        private static void Register(string pGroupName, RadioButton pRadioButton)
        {
            if ( sGroupNameToElements == null )
            {
                sGroupNameToElements = new GroupContainer();
            }

            lock ( sGroupNameToElements )
            {
                RadioGroup lGroupElements;
                if ( sGroupNameToElements.TryGetValue( pGroupName, out lGroupElements ) == false )
                {
                    lGroupElements = new RadioGroup();
                    sGroupNameToElements[ pGroupName ] = lGroupElements;
                }
                else
                {
                    PurgeDead( lGroupElements, null );
                }
                
                lGroupElements.Add( new WeakReference<RadioButton>( pRadioButton ) );
            }
        }

        /// <summary>
        /// Unregisters a radio button from a given group.
        /// </summary>
        /// <param name="pGroupName"></param>
        /// <param name="pRadioButton"></param>
        private static void Unregister(string pGroupName, RadioButton pRadioButton)
        {
            if ( sGroupNameToElements == null )
            {
                return;
            }

            lock ( sGroupNameToElements )
            {
                // Get all elements bound to this key and remove this element
                RadioGroup lGroupElements;
                if ( sGroupNameToElements.TryGetValue( pGroupName, out lGroupElements ) )
                {
                    PurgeDead( lGroupElements, pRadioButton );
                    if ( lGroupElements.Count == 0 )
                    {
                        sGroupNameToElements.Remove( pGroupName );
                    }
                }
            }
        }

        /// <summary>
        /// Purges dead remaining slots.
        /// </summary>
        /// <param name="pElements"></param>
        /// <param name="pElementToRemove"></param>
        private static void PurgeDead(RadioGroup pElements, object pElementToRemove)
        {
            for (int i = 0; i < pElements.Count;)
            {
                RadioButton lButton;
                if ( pElements[i].TryGetTarget( out lButton ) == false || 
                     lButton == pElementToRemove )
                {
                    pElements.RemoveAt( i );
                }
                else
                {
                    i++;
                }
            }
        }

        #endregion Methods
    }
}
