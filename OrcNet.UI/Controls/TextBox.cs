﻿using Cairo;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TextBox"/> class.
    /// </summary>
    public class TextBox : ATextBox
    {
        #region Fields
        
        /// <summary>
        /// Stores the text alignment.
        /// </summary>
        private TextAlignment mTextAlignment;

        /// <summary>
        /// Stores the text region
        /// </summary>
        private Rectangle mTextRegion;

        /// <summary>
        /// Stores the text width ratio.
        /// </summary>
        private float mWidthRatio = 1f;

        /// <summary>
        /// Stores the text height ratio.
        /// </summary>
        private float mHeightRatio = 1f;

        /// <summary>
        /// Stores the text extents
        /// </summary>
        private TextExtents mTextExtents;

        /// <summary>
        /// Stores the selection start cursor position.
        /// </summary>
        private double mSelectionStartCursorPos = -1;

        /// <summary>
        /// Stores the selection end cursor position.
        /// </summary>
        private double mSelectionEndCursorPos = -1;

        /// <summary>
        /// Stores the caret position in cairo units in widget client coord.
        /// </summary>
        private double mTextCursorPos;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text
        {
            get
            {
                return this.TextInternal;
            }
            set
            {
                this.TextInternal = value;
            }
        }

        /// <summary>
        /// Gets or sets the text alignment.
        /// </summary>
        public TextAlignment TextAlignment
        {
            get
            {
                return this.mTextAlignment;
            }
            set
            {
                if ( value == this.mTextAlignment )
                {
                    return;
                }

                this.mTextAlignment = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets the selected text.
        /// </summary>
        public string SelectedText
        {
            get
            {
                return this.SelectedTextInternal;
            }
            // TO DO: Make a setter???
        }

        /// <summary>
        /// Gets or sets the start selection position in char units
        /// </summary>
        public Point SelectionStart
        {
            get
            {
                return this.SelectionStartInternal;
            }
            set
            {
                this.SelectionStartInternal = value;
                this.NotifyPropertyChanged("SelectedText");
            }
        }

        /// <summary>
        /// Gets or sets the end selection position in char units.
        /// </summary>
        public Point SelectionEnd
        {
            get
            {
                return this.SelectionEndInternal;
            }
            set
            {
                this.SelectionEndInternal = value;
                this.NotifyPropertyChanged("SelectedText");
            }
        }

        /// <summary>
        /// Gets the line count.
        /// </summary>
        public int LineCount
        {
            get
            {
                return this.LineCountInternal;
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBox"/> class.
        /// </summary>
        public TextBox() :
        this( "Text" )
		{

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBox"/> class.
        /// </summary>
        /// <param name="pInitialText">The initial text.</param>
		public TextBox(string pInitialText) : 
        base( pInitialText )
		{

		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on text changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected override void OnTextChanged(TextChangeEventArgs pEventArgs)
        {
            this.NotifyPropertyChanged("Text");

            base.OnTextChanged( pEventArgs );
        }

        /// <summary>
        /// Delegate called on selection changes.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected override void OnSelectionChanged(EventArgs pEventArgs)
        {
            this.NotifyPropertyChanged("SelectedText");

            base.OnSelectionChanged( pEventArgs );
        }

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            Size lReturn;
            using ( ImageSurface lSurface = new ImageSurface( Format.Argb32, 10, 10 ) )
            {
                using ( Context lDrawingContext = new Context( lSurface ) )
                {
                    lDrawingContext.SelectFontFace( this.FontFamilly, this.FontStyle.ToCairo(), this.FontWeight.ToCairo() );
                    lDrawingContext.SetFontSize( this.FontSize );


                    this.mFontExtents = lDrawingContext.FontExtents;
                    this.mTextExtents = new TextExtents();

                    int lLineCount = this.Lines.Count;
                    // ensure minimal height = text line height
                    if ( lLineCount == 0 )
                    {
                        lLineCount = 1;
                    }

                    int lHeight = (int)(Math.Ceiling( this.mFontExtents.Height * lLineCount ) + this.Margin.Top + this.Margin.Bottom);
                    int lWidth  = -1;
                    try
                    {
                        foreach ( string lLine in this.Lines )
                        {
                            string lLineStr = lLine.Replace( "\t", new string(' ', ATextBox.TabSize ) );

                            TextExtents lTextExtents = lDrawingContext.TextExtents( lLineStr );

                            if ( lTextExtents.XAdvance > this.mTextExtents.XAdvance)
                            {
                                this.mTextExtents = lTextExtents;
                            }
                        }
                    }
                    catch ( Exception )
                    {
                        lWidth = - 1;
                    }

                    lWidth = (int)(Math.Ceiling( this.mTextExtents.XAdvance ) + this.Margin.Left + this.Margin.Right);

                    lReturn = new Size( lWidth, lHeight );
                }
            }

            return lReturn;
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );
            
            pDrawingContext.SelectFontFace( this.FontFamilly, this.FontStyle.ToCairo(), this.FontWeight.ToCairo() );
            pDrawingContext.SetFontSize( this.FontSize );
            pDrawingContext.FontOptions = PresentationSource.FontRenderingOptions;
            pDrawingContext.Antialias = PresentationSource.Antialias;

            this.mTextRegion = new Rectangle( this.Measure() );
            this.mTextRegion.Width  -= (int)(this.Margin.Left + this.Margin.Right);
            this.mTextRegion.Height -= (int)(this.Margin.Top + this.Margin.Bottom);

            this.mWidthRatio  = 1f;
            this.mHeightRatio = 1f;

            Rectangle lRegion = this.ClientRectangle;

            this.mTextRegion.X = lRegion.X;
            this.mTextRegion.Y = lRegion.Y;

            if ( this.HorizontalAlignment == HorizontalAlignment.Stretch )
            {
                mWidthRatio = lRegion.Width / (float)mTextRegion.Width;
                if ( this.VerticalAlignment != VerticalAlignment.Stretch )
                {
                    mHeightRatio = mWidthRatio;
                }
            }

            if ( this.VerticalAlignment == VerticalAlignment.Stretch )
            {
                mHeightRatio = lRegion.Height / (float)mTextRegion.Height;
                if ( this.HorizontalAlignment != HorizontalAlignment.Stretch )
                {
                    mWidthRatio = mHeightRatio;
                }
            }

            this.mTextRegion.Width  = (int)(mWidthRatio * mTextRegion.Width);
            this.mTextRegion.Height = (int)(mHeightRatio * mTextRegion.Height);

            switch ( this.TextAlignment )
            {
                //case Alignment.TopLeft:     //ok
                //    mTextRegion.X = lRegion.X;
                //    mTextRegion.Y = lRegion.Y;
                //    break;
                //case Alignment.Top:   //ok
                //    mTextRegion.Y = lRegion.Y;
                //    mTextRegion.X = lRegion.X + lRegion.Width / 2 - mTextRegion.Width / 2;
                //    break;
                //case Alignment.TopRight:    //ok
                //    mTextRegion.Y = lRegion.Y;
                //    mTextRegion.X = lRegion.Right - mTextRegion.Width;
                //    break;
                case TextAlignment.Left://ok
                    mTextRegion.X = lRegion.X;
                    mTextRegion.Y = lRegion.Y + lRegion.Height / 2 - mTextRegion.Height / 2;
                    break;
                case TextAlignment.Right://ok
                    mTextRegion.X = lRegion.X + lRegion.Width - mTextRegion.Width;
                    mTextRegion.Y = lRegion.Y + lRegion.Height / 2 - mTextRegion.Height / 2;
                    break;
                //case Alignment.Bottom://ok
                //    mTextRegion.X = lRegion.Width / 2 - mTextRegion.Width / 2;
                //    mTextRegion.Y = lRegion.Height - mTextRegion.Height;
                //    break;
                //case Alignment.BottomLeft://ok
                //    mTextRegion.X = lRegion.X;
                //    mTextRegion.Y = lRegion.Bottom - mTextRegion.Height;
                //    break;
                //case Alignment.BottomRight://ok
                //    mTextRegion.Y = lRegion.Bottom - mTextRegion.Height;
                //    mTextRegion.X = lRegion.Right - mTextRegion.Width;
                //    break;
                case TextAlignment.Center://ok
                case TextAlignment.Justify://ok
                    mTextRegion.X = lRegion.X + lRegion.Width / 2 - mTextRegion.Width / 2;
                    mTextRegion.Y = lRegion.Y + lRegion.Height / 2 - mTextRegion.Height / 2;
                    break;
            }

            pDrawingContext.FontMatrix = new Matrix( mWidthRatio * this.FontSize, 0, 0, mHeightRatio * this.FontSize, 0, 0 );
            this.mFontExtents = pDrawingContext.FontExtents;

            // Draw the text carret.
            this.DrawCarret( pDrawingContext );

            for ( int lCurr = 0; lCurr < this.Lines.Count; lCurr++ )
            {
                string lLine = this.Lines[ lCurr ].Replace( "\t", new string( ' ', ATextBox.TabSize ) );
                int lLineLength = (int)pDrawingContext.TextExtents( lLine ).XAdvance;
                Rectangle lLineRegion = new Rectangle( mTextRegion.X,
                                                       mTextRegion.Y + (int)Math.Ceiling( lCurr * this.mFontExtents.Height ),
                                                       lLineLength,
                                                       (int)Math.Ceiling( this.mFontExtents.Height ) );

                if ( string.IsNullOrWhiteSpace( lLine ) )
                {
                    continue;
                }

                this.Foreground.SetAsSource( pDrawingContext );
                pDrawingContext.MoveTo( lLineRegion.X, 
                                        mTextRegion.Y + this.mFontExtents.Ascent + this.mFontExtents.Height * lCurr );
                pDrawingContext.ShowText( lLine );
                pDrawingContext.Fill();

                // Add selection color to selected text.
                if ( this.SelectionEnd >= 0 && 
                     lCurr >= this.StartPositionInternal.Y && 
                     lCurr <= this.EndPositionInternal.Y )
                {
                    pDrawingContext.SetSourceColor( this.SelectionBrush );

                    Rectangle lSelectionRegion = lLineRegion;

                    int lCursorPositionStart = (int)this.mSelectionStartCursorPos;
                    int lCursorPositionEnd   = (int)this.mSelectionEndCursorPos;

                    if ( this.SelectionStart.Y > this.SelectionEnd.Y )
                    {
                        lCursorPositionStart = lCursorPositionEnd;
                        lCursorPositionEnd = (int)this.mSelectionStartCursorPos;
                    }

                    if ( lCurr == this.StartPositionInternal.Y )
                    {
                        lSelectionRegion.Width -= lCursorPositionStart;
                        lSelectionRegion.Left += lCursorPositionStart;
                    }

                    if ( lCurr == this.EndPositionInternal.Y )
                    {
                        lSelectionRegion.Width -= (lLineLength - lCursorPositionEnd);
                    }

                    pDrawingContext.Rectangle( lSelectionRegion );
                    pDrawingContext.FillPreserve();
                    pDrawingContext.Save();
                    pDrawingContext.Clip();
                    pDrawingContext.SetSourceColor( this.Foreground );
                    pDrawingContext.MoveTo( lLineRegion.X, this.mTextRegion.Y + this.mFontExtents.Ascent + this.mFontExtents.Height * lCurr );
                    pDrawingContext.ShowText( lLine );
                    pDrawingContext.Fill();
                    pDrawingContext.Restore();
                }
            }
        }

        /// <summary>
        /// Draws the text carret.
        /// </summary>
        /// <param name="pDrawingContext"></param>
        private void DrawCarret(Context pDrawingContext)
        {
            if ( this.IsFocused )
			{
                if ( this.mMouseLocalPos >= 0 )
                {
                    ComputeTextCursor(pDrawingContext);

                    if ( this.IsSelectionInProgress )
                    {
                        if ( this.SelectionStart < 0 )
                        {
                            this.SelectionStart = this.CurrentPosition;
                            this.mSelectionStartCursorPos = this.mTextCursorPos;
                            this.SelectionEnd = -1;
                        }
                        else
                        {
                            this.SelectionEnd = this.CurrentPosition;
                            if ( this.SelectionEnd == this.SelectionStart )
                            {
                                this.SelectionEnd = -1;
                            }
                            else
                            {
                                this.mSelectionEndCursorPos = this.mTextCursorPos;
                            }
                        }
                    }
                    else
                    {
                        this.ComputeTextCursorPosition(pDrawingContext);
                    }
                }
                else
                {
                    this.ComputeTextCursorPosition(pDrawingContext);
                }

                this.Foreground.SetAsSource(pDrawingContext);
				pDrawingContext.LineWidth = 1.0;
                pDrawingContext.MoveTo(0.5 + this.mTextCursorPos + this.mTextRegion.X, this.mTextRegion.Y + this.CurrentLine * this.mFontExtents.Height);
                pDrawingContext.LineTo(0.5 + this.mTextCursorPos + this.mTextRegion.X, this.mTextRegion.Y + (this.CurrentLine + 1) * this.mFontExtents.Height);
                pDrawingContext.Stroke();
			}
        }

        /// <summary>
		/// Updates the Current Column, line and TextCursorPos
		/// from the mouse Local Position.
		/// </summary>
		private void ComputeTextCursor(Context pDrawingContext)
        {
            TextExtents lTextExtents;
            double cPos = 0f;

            this.CurrentLine = (int)( this.mMouseLocalPos.Y / this.mFontExtents.Height );

            //fix cu
            if ( this.CurrentLine >= this.Lines.Count )
            {
                this.CurrentLine = this.Lines.Count - 1;
            }

            for ( int lCurr = 0; lCurr < this.Lines[ this.CurrentLine ].Length; lCurr++ )
            {
                string lChar = this.Lines[ this.CurrentLine ].Substring( lCurr, 1 );
                if ( lChar == "\t")
                {
                    lChar = new string(' ', ATextBox.TabSize);
                }

                lTextExtents = pDrawingContext.TextExtents( lChar );

                double lHalfWidth = lTextExtents.XAdvance / 2;

                if ( this.mMouseLocalPos.X <= cPos + lHalfWidth )
                {
                    this.CurrentColumn  = lCurr;
                    this.mTextCursorPos = cPos;
                    this.mMouseLocalPos = -1;
                    return;
                }

                cPos += lTextExtents.XAdvance;
            }

            this.CurrentColumn = this.Lines[ this.CurrentLine ].Length;
            this.mTextCursorPos = cPos;

            // reset Mouse Local Position
            this.mMouseLocalPos = -1;
        }

        /// <summary>
        /// Computes offsets in cairo units
        /// </summary>
        /// <param name="pDrawingContext">The cairo context.</param>
        private void ComputeTextCursorPosition(Context pDrawingContext)
        {
            if ( this.SelectionStart >= 0 )
            {
                this.mSelectionStartCursorPos = this.GetXFromTextPointer( pDrawingContext, this.SelectionStart );
            }

            if ( this.SelectionEnd >= 0 )
            {
                this.mSelectionEndCursorPos = this.GetXFromTextPointer( pDrawingContext, this.SelectionEnd );
            }

            this.mTextCursorPos = this.GetXFromTextPointer( pDrawingContext, new Point( this.CurrentColumn, this.CurrentLine ) );
        }

        /// <summary>
        /// Computes the x offset in cairo unit from text position
        /// </summary>
        /// <param name="pDrawingContext">The cairo context.</param>
        /// <param name="pPoint">The text caret position.</param>
        /// <returns>The x offset in cairo unit from text position</returns>
        private double GetXFromTextPointer(Context pDrawingContext, Point pPoint)
        {
            try
            {
                string lText = this.Lines[ pPoint.Y ].Substring( 0, pPoint.X ).Replace( "\t", new string(' ', ATextBox.TabSize ) );
                return pDrawingContext.TextExtents( lText ).XAdvance;
            }
            catch
            {
                return -1;
            }
        }

        #endregion Methods
    } 
}
