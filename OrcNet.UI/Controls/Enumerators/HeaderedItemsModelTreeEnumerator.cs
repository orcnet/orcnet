﻿using System;
using System.Collections;
using System.Diagnostics;

namespace OrcNet.UI.Enumerators
{
    /// <summary>
    /// Definition of the <see cref="HeaderedItemsModelTreeEnumerator"/> class.
    /// </summary>
    internal class HeaderedItemsModelTreeEnumerator : ModelTreeEnumerator
    {
        #region Fields

        /// <summary>
        /// Stores the enumerator owner.
        /// </summary>
        private HeaderedItemsControl mOwner;

        /// <summary>
        /// Stores the items enumerator.
        /// </summary>
        private IEnumerator mItems;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the current object.
        /// </summary>
        protected override object Current
        {
            get
            {
                if ( this.Index > 0 )
                {
                    return this.mItems.Current;
                }

                return base.Current;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the content has changed since the enumeration start or not.
        /// </summary>
        protected override bool IsUnchanged
        {
            get
            {
                object lHeader = this.Content;
                return Object.ReferenceEquals( lHeader, this.mOwner.Header );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderedItemsModelTreeEnumerator"/> class.
        /// </summary>
        /// <param name="pOwner">The enumerator owner.</param>
        /// <param name="pItems">The headered control items.</param>
        /// <param name="pHeader">The headered control header.</param>
        internal HeaderedItemsModelTreeEnumerator(HeaderedItemsControl pOwner, IEnumerator pItems, object pHeader) : 
        base( pHeader )
        {
            Debug.Assert( pOwner != null, "The owner should be non-null.");
            Debug.Assert( pItems != null, "The items should be non-null.");
            Debug.Assert( pHeader != null, "The header should be non-null. If Header was null, the base ItemsControl enumerator should have been used.");

            this.mOwner = pOwner;
            this.mItems = pItems;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Moves to the next object.
        /// </summary>
        /// <returns>True if moved to the next, false otherwise.</returns>
        protected override bool MoveNext()
        {
            if ( this.Index >= 0 )
            {
                this.Index++;
                return this.mItems.MoveNext();
            }

            return base.MoveNext();
        }

        /// <summary>
        /// Resets the enumerator.
        /// </summary>
        protected override void Reset()
        {
            base.Reset();

            this.mItems.Reset();
        }

        #endregion Methods
    }
}
