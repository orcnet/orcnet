﻿using System.Diagnostics;

namespace OrcNet.UI.Enumerators
{
    /// <summary>
    /// Definition of the <see cref="ContentModelTreeEnumerator"/> class.
    /// </summary>
    internal class ContentModelTreeEnumerator : ModelTreeEnumerator
    {
        #region Fields

        /// <summary>
        /// Stores the content enumerator owner.
        /// </summary>
        private ContentControl mOwner;

        #endregion Fields

        #region Properties

        protected override bool IsUnchanged
        {
            get
            {
                return object.ReferenceEquals( Content, this.mOwner.Content );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentModelTreeEnumerator"/> class.
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pContent"></param>
        internal ContentModelTreeEnumerator(ContentControl pOwner, object pContent) : 
        base( pContent )
        {
            Debug.Assert( pOwner != null, "The owner should be non-null." );

            this.mOwner = pOwner;
        }

        #endregion Constructor
    }
}
