﻿using System.Diagnostics;

namespace OrcNet.UI.Enumerators
{
    /// <summary>
    /// Definition of the <see cref="HeaderedContentModelTreeEnumerator"/> class.
    /// </summary>
    internal class HeaderedContentModelTreeEnumerator : ModelTreeEnumerator
    {
        #region Fields

        /// <summary>
        /// Stores the enumerator owner.
        /// </summary>
        private HeaderedContentControl mOwner;

        /// <summary>
        /// Stores the enumerator content of an headered control.
        /// </summary>
        private object mContent;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the current object.
        /// </summary>
        protected override object Current
        {
            get
            {
                if ( this.Index == 1 && 
                     this.mContent != null )
                {
                    return this.mContent;
                }

                return base.Current;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the content has changed since the enumeration start or not.
        /// </summary>
        protected override bool IsUnchanged
        {
            get
            {
                object lHeader = this.Content;    // Header was passed to the base so that it would appear in index 0
                return object.ReferenceEquals( lHeader, this.mOwner.Header ) &&
                       object.ReferenceEquals( this.mContent, this.mOwner.Content );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderedContentControl"/> class.
        /// </summary>
        /// <param name="pOwner">The enumerator owner</param>
        /// <param name="pContent">The headered control content.</param>
        /// <param name="pHeader">The headered control header.</param>
        internal HeaderedContentModelTreeEnumerator(HeaderedContentControl pOwner, object pContent, object pHeader) : 
        base( pHeader )
        {
            Debug.Assert( pOwner != null, "The Owner should be non-null.");
            Debug.Assert( pHeader != null, "Header should be non-null. If Header was null, the base ContentControl enumerator should have been used.");

            this.mOwner = pOwner;
            this.mContent = pContent;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Moves to the next object.
        /// </summary>
        /// <returns>True if moved to the next, false otherwise.</returns>
        protected override bool MoveNext()
        {
            if ( this.mContent != null )
            {
                if ( this.Index == 0 )
                {
                    // Moving from the header to content
                    this.Index++;
                    this.VerifyUnchanged();
                    return true;
                }
                else if ( this.Index == 1 )
                {
                    // Going from content to the end
                    this.Index++;
                    return false;
                }
            }

            return base.MoveNext();
        }

        #endregion Methods
    }
}
