﻿using System.Collections;

namespace OrcNet.UI.Enumerators
{
    /// <summary>
    /// Definition of the <see cref="SingleChildEnumerator"/> class.
    /// </summary>
    internal class SingleChildEnumerator : IEnumerator
    {
        #region Fields

        /// <summary>
        /// Stores the current index.
        /// </summary>
        private int mIndex = -1;

        /// <summary>
        /// Stores the enumerator element count.
        /// </summary>
        private int mCount = 0;

        /// <summary>
        /// Stores the single child.
        /// </summary>
        private object mChild;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the current element.
        /// </summary>
        object IEnumerator.Current
        {
            get
            {
                return (this.mIndex == 0) ? this.mChild : null;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SingleChildEnumerator"/> class.
        /// </summary>
        /// <param name="pChild"></param>
        internal SingleChildEnumerator(object pChild)
        {
            this.mChild = pChild;
            this.mCount = pChild == null ? 0 : 1;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Moves to the next object.
        /// </summary>
        /// <returns></returns>
        bool IEnumerator.MoveNext()
        {
            mIndex++;
            return mIndex < mCount;
        }

        /// <summary>
        /// Resets the enumerator.
        /// </summary>
        void IEnumerator.Reset()
        {
            mIndex = -1;
        }

        #endregion Methods
    }
}
