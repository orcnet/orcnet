﻿using System;
using System.Collections;

namespace OrcNet.UI.Enumerators
{
    /// <summary>
    /// Definition of the <see cref="ModelTreeEnumerator"/> class.
    /// </summary>
    internal abstract class ModelTreeEnumerator : IEnumerator
    {
        #region Fields

        /// <summary>
        /// Stores the enumerator current index.
        /// </summary>
        private int mIndex = -1;

        /// <summary>
        /// Stores the enumerator single content.
        /// </summary>
        private object mContent;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the current object.
        /// </summary>
        object IEnumerator.Current
        {
            get
            {
                return this.Current;
            }
        }

        /// <summary>
        /// Gets the content.
        /// </summary>
        protected object Content
        {
            get
            {
                return this.mContent;
            }
        }

        /// <summary>
        /// Gets the current index.
        /// </summary>
        protected int Index
        {
            get
            {
                return this.mIndex; ;
            }
            set
            {
                this.mIndex = value;
            }
        }

        /// <summary>
        /// Gets the current object.
        /// </summary>
        protected virtual object Current
        {
            get
            {
                if ( this.mIndex == 0 )
                {
                    return this.mContent;
                }

                throw new InvalidOperationException( "Invalid index." );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the content has changed since the enumeration start or not.
        /// </summary>
        protected abstract bool IsUnchanged
        {
            get;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelTreeEnumerator"/> class.
        /// </summary>
        /// <param name="pContent">The enumerator single content</param>
        internal ModelTreeEnumerator(object pContent)
        {
            this.mContent = pContent;
        }

        #endregion Constructor
        
        #region Methods
        
        /// <summary>
        /// Moves to the next object.
        /// </summary>
        /// <returns></returns>
        bool IEnumerator.MoveNext()
        {
            return this.MoveNext();
        }

        /// <summary>
        /// Resets the enumerator.
        /// </summary>
        void IEnumerator.Reset()
        {
            this.Reset();
        }

        /// <summary>
        /// Moves to the next object.
        /// </summary>
        /// <returns>True if moved to the next, false otherwise.</returns>
        protected virtual bool MoveNext()
        {
            if ( this.mIndex < 1 )
            {
                // single content, can move next to 0 and that's it.
                this.mIndex++;

                if ( this.mIndex == 0 )
                {
                    this.VerifyUnchanged();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Resets the enumerator.
        /// </summary>
        protected virtual void Reset()
        {
            this.VerifyUnchanged();
            this.mIndex = -1;
        }
        
        /// <summary>
        /// Checks whether the content is unchanged or not since the enumeration start.
        /// </summary>
        protected void VerifyUnchanged()
        {
            // If the content has changed, then throw an exception
            if ( this.IsUnchanged == false )
            {
                throw new InvalidOperationException( "Enumerator content changed." );
            }
        }

        #endregion Methods
    }
}
