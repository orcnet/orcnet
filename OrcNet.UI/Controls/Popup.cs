﻿using OrcNet.Core.Datastructures;
using OrcNet.Core.Logger;
using OrcNet.UI.Enumerators;
using OrcNet.UI.Helpers;
using OrcNet.UI.Input;
using System;
using System.Collections;
using System.Diagnostics;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Popup"/> class.
    /// </summary>
    public class Popup : FrameworkElement, IAddChild
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the content can be transparent or not.
        /// </summary>
        private bool mAllowsTransparency = false;

        /// <summary>
        /// Stores the popup source window
        /// </summary>
        private PresentationSource mWindow;

        /// <summary>
        /// Stores the flag indicating whether the popup content is visible or not.
        /// </summary>
        private bool mIsOpen;

        /// <summary>
        /// Stores the popup content.
        /// </summary>
        private FrameworkElement mContent;
        
        /// <summary>
        /// Stores the popup position.
        /// </summary>
        private PopupPosition mPopupPosition;
        
        #endregion Fields
        
        #region Events

        /// <summary>
        /// Event fired on popup opened.
        /// </summary>
        public event EventHandler Opened;

        /// <summary>
        /// Event fired on popup closed.
        /// </summary>
		public event EventHandler Closed;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the content can be transparent or not.
        /// </summary>
        public bool AllowsTransparency
        {
            get
            {
                return this.mAllowsTransparency;
            }
            set
            {
                if ( this.mAllowsTransparency == value )
                {
                    return;
                }

                this.mAllowsTransparency = value;

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the popup content.
        /// </summary>
        public FrameworkElement Content
        {
			get
            {
                return this.mContent;
            }
			set
            {
                FrameworkElement lOldContent = this.mContent;

				this.mContent = value;

                this.OnContentChanged( this.mContent, lOldContent );

                this.NotifyPropertyChanged();
			}
		}

        /// <summary>
        /// Gets or sets the flag indicating whether the popup content is visible or not.
        /// </summary>
		public bool IsOpen
		{
			get
            {
                return this.mIsOpen;
            }
			set
			{
				if (value == this.mIsOpen)
                {
					return;
                }

				this.mIsOpen = value;

                this.NotifyPropertyChanged();

                if ( this.mIsOpen )
                {
                    this.OnPopupOpened( null );
                }
                else
                {
                    this.OnPopupClosed( null );
                }
			}
		}
        
		/// <summary>
        /// Gets or sets the popup position.
        /// </summary>
		public PopupPosition PopupPosition
        {
			get
            {
                return this.mPopupPosition;
            }
			set
            {
                if ( this.mPopupPosition == value )
                {
                    return;
                }

				this.mPopupPosition = value;
                this.NotifyPropertyChanged();
			}
		}

        /// <summary>
        /// Gets the set of logical children if any.
        /// </summary>
        protected internal override IEnumerator LogicalChildren
        {
            get
            {
                object lContent = this.Content;

                if ( lContent == null )
                {
                    return EmptyEnumerator<object>.Empty;
                }

                return new PopupModelTreeEnumerator( this, lContent );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Popup"/> class.
        /// </summary>
        public Popup()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds the popup child.
        /// </summary>
        /// <param name="pChild"></param>
        void IAddChild.AddChild(object pChild)
        {
            UIElement lElement = pChild as UIElement;
            if ( lElement == null && 
                 pChild != null )
            {
                throw new ArgumentException("Cannot add child other than UIElement based.");
            }

            this.Content = lElement as FrameworkElement;
        }

        /// <summary>
        /// Shows the popup window.
        /// </summary>
        private void ShowWindow()
        {
            if ( this.mWindow != null &&
                 this.mWindow.IsDisposed == false )
            {
                this.mWindow.WindowWrapper.Show();
            }
        }

        /// <summary>
        /// Hides the popup window.
        /// </summary>
        private void HideWindow()
        {
            if ( this.mWindow != null &&
                 this.mWindow.IsDisposed == false )
            {
                this.mWindow.WindowWrapper.Hide();

                this.DestroyWindow();
            }
        }

        /// <summary>
        /// Sets the root visual to the popup window.
        /// </summary>
        private void SetRootVisual()
        {
            this.SetWindowRootVisualInternal( this.Content );
        }

        /// <summary>
        /// Sets the popup window root visual.
        /// </summary>
        /// <param name="pContent"></param>
        private void SetWindowRootVisualInternal(AVisual pContent)
        {
            this.mWindow.RootVisual = pContent;
        }

        /// <summary>
        /// Creates the popup window.
        /// </summary>
        private void CreateWindow()
        {
            // Create a new if needed.
            bool lNeedNewWindow = this.mWindow == null || this.mWindow.IsDisposed;
            if ( lNeedNewWindow )
            {
                this.BuildWindow( this );

                this.SetRootVisual();
            }
            else
            {
                this.Reposition();
            }

            bool lIsWindowAlive = this.mWindow != null || this.mWindow.IsDisposed == false;
            if ( lIsWindowAlive )
            {
                this.ShowWindow();

                this.OnPopupOpened( EventArgs.Empty );
            }
        }

        /// <summary>
        /// Builds the popup window.
        /// </summary>
        /// <param name="pTargetVisual"></param>
        private void BuildWindow(AVisual pTargetVisual)
        {
            FrameworkElement lFE = pTargetVisual as FrameworkElement;
            this.BuildWindowInternal( lFE.Left, lFE.Top, this.AllowsTransparency, pTargetVisual, null );
        }

        /// <summary>
        /// Destroys the popup window.
        /// </summary>
        private void DestroyWindow()
        {
            if ( this.mWindow != null &&
                 this.mWindow.IsDisposed == false )
            {
                this.DestroyWindowInternal();

                this.OnPopupClosed( EventArgs.Empty );
            }
        }

        /// <summary>
        /// Builds the window for the popup.
        /// </summary>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        /// <param name="pIsTransparentBackground"></param>
        /// <param name="pPlacementTarget"></param>
        /// <param name="pHooks"></param>
        private void BuildWindowInternal(int pX, int pY, bool pIsTransparentBackground, AVisual pPlacementTarget, NativeWindowHooks pHooks)
        {
            INativeWindow lParent = null;
            PresentationSource lSource = PresentationSource.FromVisual( pPlacementTarget );
            if ( lSource != null )
            {
                lParent = lSource.WindowWrapper.Window;
            }

            NativeWindowParameters lParam = new NativeWindowParameters( string.Empty, pHooks );
            lParam.X = pX;
            lParam.Y = pY;
            lParam.WindowStyleFlags = (int)StyleFlags.Popup;

            if ( lParent != null )
            {
                lParam.Parent = lParent;
            }

            this.mWindow = new PresentationSource( lParam );

            if ( pIsTransparentBackground )
            {
                this.mWindow.CompositionTarget.BackgroundColor = Color.Transparent;
            }
            else
            {
                this.mWindow.CompositionTarget.BackgroundColor = Color.Black;
            }
        }

        /// <summary>
        /// Destroys the window.
        /// </summary>
        private void DestroyWindowInternal()
        {
            PresentationSource lSource = this.mWindow;

            this.mWindow = null;

            if ( lSource.IsDisposed == false )
            {
                try
                {
                    lSource.RootVisual = null;
                    lSource.Dispose();
                }
                catch( Exception pEx )
                {
                    LogManager.Instance.Log( pEx );
                }
            }
        }

        /// <summary>
        /// Finds the top most visual element.
        /// </summary>
        /// <param name="pVisual"></param>
        /// <returns></returns>
        private static AVisual FindTopMostVisual(AVisual pVisual)
        {
            AVisual lRoot = null;
            AVisual lCurrent = pVisual;

            while ( lCurrent != null )
            {
                lRoot = lCurrent;

                lCurrent = VisualTreeHelper.GetParent( lCurrent );
            }

            return lRoot;
        }

        /// <summary>
        /// Reposition the overall content in the popup.
        /// </summary>
        internal void Reposition()
        {
            this.PositionContentInternal( LayoutingType.X );
            this.PositionContentInternal( LayoutingType.Y );
        }

        /// <summary>
        /// Position the content in the popup 
        /// </summary>
        /// <param name="pLayoutingType"></param>
        private void PositionContentInternal(LayoutingType pLayoutingType)
        {
            AVisual lVisualParent = this.Content.VisualParent;
            if (lVisualParent == null)
            {
                return;
            }

            Rectangle lRegion = this.ScreenCoordinates(this.Slot);
            if (pLayoutingType == LayoutingType.X)
            {
                if ( this.PopupPosition.HasFlag( PopupPosition.Right ) )
                {
                    if (lRegion.Right + this.Content.Slot.Width > lVisualParent.ClientRectangle.Right)
                    {
                        this.Content.Left = lRegion.Left - this.Content.Slot.Width;
                    }
                    else
                    {
                        this.Content.Left = lRegion.Right;
                    }
                }
                else if ( this.PopupPosition.HasFlag( PopupPosition.Left ) )
                {
                    if (lRegion.Left - this.Content.Slot.Width < lVisualParent.ClientRectangle.Left)
                    {
                        this.Content.Left = lRegion.Right;
                    }
                    else
                    {
                        this.Content.Left = lRegion.Left - this.Content.Slot.Width;
                    }
                }
                else
                {
                    if (this.Content.Slot.Width < lVisualParent.ClientRectangle.Width)
                    {
                        if (lRegion.Left + this.Content.Slot.Width > lVisualParent.ClientRectangle.Right)
                        {
                            this.Content.Left = lVisualParent.ClientRectangle.Right - this.Content.Slot.Width;
                        }
                        else
                        {
                            this.Content.Left = lRegion.Left;
                        }
                    }
                    else
                    {
                        this.Content.Left = 0;
                    }
                }
            }
            else if (pLayoutingType == LayoutingType.Y)
            {
                if (this.Content.Slot.Height < lVisualParent.ClientRectangle.Height)
                {
                    if ( this.PopupPosition.HasFlag( PopupPosition.Bottom ) )
                    {
                        if (lRegion.Bottom + this.Content.Slot.Height > lVisualParent.ClientRectangle.Bottom)
                        {
                            this.Content.Top = lRegion.Top - this.Content.Slot.Height;
                        }
                        else
                        {
                            this.Content.Top = lRegion.Bottom;
                        }
                    }
                    else if ( this.PopupPosition.HasFlag( PopupPosition.Top ) )
                    {
                        if (lRegion.Top - this.Content.Slot.Height < lVisualParent.ClientRectangle.Top)
                        {
                            this.Content.Top = lRegion.Bottom;
                        }
                        else
                        {
                            this.Content.Top = lRegion.Top - this.Content.Slot.Height;
                        }
                    }
                    else
                    {
                        this.Content.Top = lRegion.Top;
                    }
                }
                else
                {
                    this.Content.Top = 0;
                }
            }
        }
		
        /// <summary>
        /// Delegate called on mouse click.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseClick(object pSender, MouseButtonEventArgs pEventArgs)
        {
            this.IsOpen = !this.IsOpen;

            base.OnMouseClick( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on UIElement mouse leave.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseLeave(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseLeave( pSender, pEventArgs );

            this.IsOpen = false;
        }
        
        /// <summary>
        /// Internal method allowing specific check to know whether the given point is inside this element or not.
        /// </summary>
        /// <param name="pScreenPoint">The point to check.</param>
        /// <returns>True if contained, false otherwise.</returns>
        protected override bool InternalIsMouseIn(Point pScreenPoint)
        {
            bool lIsInContent = false;
            if ( this.Content != null &&
                 this.Content.VisualParent != null )
            {
                lIsInContent = this.Content.IsMouseIn( pScreenPoint );
            }

            return lIsInContent;
        }

        /// <summary>
        /// Delegate called on mouse hover.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseHover(MouseMoveEventArgs pEventArgs)
        {
            if ( this.Content != null &&
                 this.Content.VisualParent != null )
            {
                if ( this.Content.IsMouseIn( pEventArgs.Position ) )
                {
                    if ( InputManager.Instance.HoverElement != this )
                    {
                        InputManager.Instance.HoverElement = this;

                        this.OnMouseEnter( this, pEventArgs );
                    }

                    this.Content.OnMouseHover( pEventArgs );
                    return;
                }
            }

            base.OnMouseHover( pEventArgs );
        }

        /// <summary>
        /// Delegate called on popup opened.
        /// </summary>
        /// <param name="pEventArgs"></param>
		protected virtual void OnPopupOpened(EventArgs pEventArgs)
		{
            if (this.Content != null)
            {
                this.Content.IsVisible = true;
                
                if ( this.Content.LogicalParent != this )
                {
                    this.Content.LogicalParent = this;
                }
                
                this.Reposition();
            }

            if ( this.Opened != null )
            {
                this.Opened( this, pEventArgs );
            }
		}

        /// <summary>
        /// Delegate called on popup closed.
        /// </summary>
        /// <param name="pEventArgs"></param>
		protected virtual void OnPopupClosed(EventArgs pEventArgs)
		{
			if ( this.Content != null )
            {
				this.Content.IsVisible = false;
			}

			if ( this.Closed != null )
            {
                this.Closed( this, pEventArgs );
            }
		}

        /// <summary>
        /// Delegate called on content layout changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected void OnContentLayoutChanged(object pSender, LayoutingEventArgs pEventArgs)
		{
            if (pEventArgs.LayoutType.HasFlag(LayoutingType.Width))
            {
                this.PositionContentInternal(LayoutingType.X);
            }

            if (pEventArgs.LayoutType.HasFlag(LayoutingType.Height))
            {
                this.PositionContentInternal(LayoutingType.Y);
            }
		}

        /// <summary>
        /// Delegate called on popup content changed.
        /// </summary>
        /// <param name="pNewContent"></param>
        /// <param name="pOldContent"></param>
        protected virtual void OnContentChanged(FrameworkElement pNewContent, FrameworkElement pOldContent)
        {
            if ( pOldContent != null )
            {
                this.RemoveLogicalChild( pOldContent );

                pOldContent.LayoutUpdated -= this.OnContentLayoutChanged;
            }

            if ( pNewContent != null )
            {
                this.AddLogicalChild( pNewContent );

                pNewContent.LayoutUpdated += this.OnContentLayoutChanged;

                pNewContent.HorizontalAlignment = HorizontalAlignment.Left;
                pNewContent.VerticalAlignment = VerticalAlignment.Top;
            }

            this.Reposition();
        }
        
        #endregion Methods

        #region Inner Classes

        /// <summary>
        /// Definition of the <see cref="PopupModelTreeEnumerator"/> class.
        /// </summary>
        private class PopupModelTreeEnumerator : ModelTreeEnumerator
        {
            #region Fields

            /// <summary>
            /// Stores the popup content owner.
            /// </summary>
            private Popup mPopup;

            #endregion Fields

            #region Properties

            /// <summary>
            /// Checks whether the enumerator has changed since the last enumeration or not.
            /// </summary>
            protected override bool IsUnchanged
            {
                get
                {
                    return object.ReferenceEquals( this.Content, this.mPopup.Content );
                }
            }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="PopupModelTreeEnumerator"/> class.
            /// </summary>
            /// <param name="pPopup"></param>
            /// <param name="pContent"></param>
            internal PopupModelTreeEnumerator(Popup pPopup, object pContent) : 
            base( pContent )
            {
                Debug.Assert( pPopup != null, "The popup should not be null.");
                Debug.Assert( pContent != null, "The child should not be null.");

                this.mPopup = pPopup;
            }

            #endregion Constructor
        }

        #endregion Inner Classes
    }
}
