﻿using OrcNet.Core;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ICommandSource"/> interface.
    /// </summary>
    public interface ICommandSource : IObserver
    {
        #region Properties

        /// <summary>
        /// Gets the command that will be executed.
        /// </summary>
        ICommand Command
        {
            get;
        }

        /// <summary>
        /// Gets the optional command's parameter.
        /// </summary>
        object CommandParameter
        {
            get;
        }

        #endregion Properties
    }
}
