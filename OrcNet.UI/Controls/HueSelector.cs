﻿using Cairo;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="HueSelector"/> class.
    /// </summary>
	public class HueSelector : ColorSelector
	{
        #region Fields

        /// <summary>
        /// Stores the selector orientation.
        /// </summary>
        private Orientation mOrientation;

        /// <summary>
        /// Stores the Hue value.
        /// </summary>
        private double mHue;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the selector orientation.
        /// </summary>
        public virtual Orientation Orientation
        {
            get
            {
                return this.mOrientation;
            }
            set
            {
                if ( this.mOrientation == value )
                {
                    return;
                }

                this.mOrientation = value;
                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }

        /// <summary>
        /// Gets or sets the Hue value.
        /// </summary>
        public virtual double Hue
        {
            get
            {
                return this.mHue;
            }
            set
            {
                if ( this.mHue == value )
                {
                    return;
                }

                this.mHue = value;
                this.NotifyPropertyChanged();
                this.UpdateMousePosFromHue();
                this.InvalidateVisual();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="HueSelector"/> class.
        /// </summary>
        public HueSelector() : 
        base()
		{
            this.mOrientation = Orientation.Horizontal;
		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            Rectangle lRectangle = this.ClientRectangle;

			GradientType lType = GradientType.Horizontal;
            if ( this.mOrientation == Orientation.Vertical )
            {
                lType = GradientType.Vertical;
            }

            GradientBrush lGradient = new GradientBrush( lType );

            lGradient.GradientStops.Add( new GradientStop( 0,     new Color( 1, 0, 0, 1 ) ) );
            lGradient.GradientStops.Add( new GradientStop( 0.167, new Color( 1, 1, 0, 1 ) ) );
            lGradient.GradientStops.Add( new GradientStop( 0.333, new Color( 0, 1, 0, 1 ) ) );
            lGradient.GradientStops.Add( new GradientStop( 0.5,   new Color( 0, 1, 1, 1 ) ) );
            lGradient.GradientStops.Add( new GradientStop( 0.667, new Color( 0, 0, 1, 1 ) ) );
            lGradient.GradientStops.Add( new GradientStop( 0.833, new Color( 1, 0, 1, 1 ) ) );
            lGradient.GradientStops.Add( new GradientStop( 1,     new Color( 1, 0, 0, 1 ) ) );

            lGradient.SetAsSource( pDrawingContext, lRectangle );
            CairoHelpers.CairoRectangle( pDrawingContext, lRectangle, -1 );
            pDrawingContext.Fill();
        }

        /// <summary>
        /// Recursive painting routine on the parent context of the actual cached version of the widget 
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected internal override void Paint(ref Context pDrawingContext)
		{
            base.Paint( ref pDrawingContext );

			Rectangle lRegionBound = this.Slot + this.VisualParent.ClientRectangle.Position;
            pDrawingContext.Save();

            pDrawingContext.Translate( lRegionBound.X, lRegionBound.Y );

            pDrawingContext.SetSourceColor( Color.White );
			Rectangle lRegion = this.ClientRectangle;
			if ( this.mOrientation == Orientation.Horizontal )
            {
				lRegion.Width = 4;
                lRegion.X = this.mMousePosition.X - 2;
			}
            else
            {
				lRegion.Height = 4;
                lRegion.Y = this.mMousePosition.Y - 2;
			}

            CairoHelpers.CairoRectangle( pDrawingContext, lRegion, 2 );
            pDrawingContext.SetSourceColor( Color.White );
			pDrawingContext.LineWidth = 2.0;
			pDrawingContext.Stroke();
            pDrawingContext.Restore();
		}

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected internal override void OnLayoutChanges(LayoutingType pLayoutType)
		{
            base.OnLayoutChanges( pLayoutType );

            if ( this.mOrientation == Orientation.Horizontal )
            {
                if ( pLayoutType == LayoutingType.Width )
                {
                    this.UpdateMousePosFromHue();
                }
            }
            else if ( pLayoutType == LayoutingType.Height )
            {
                this.UpdateMousePosFromHue();
            }
		}

        /// <summary>
        /// Updates the mouse position in cache.
        /// </summary>
        /// <param name="pPosition"></param>
		protected override void UpdateMouseLocalPos(Point pPosition)
		{
			base.UpdateMouseLocalPos( pPosition );

            if ( this.mOrientation == Orientation.Horizontal)
            {
                this.mHue = (double)this.mMousePosition.X / this.ClientRectangle.Width;
            }
            else
            {
                this.mHue = (double)this.mMousePosition.Y / this.ClientRectangle.Height;
            }

			this.NotifyPropertyChanged( "Hue" );
            this.InvalidateVisual();
		}

        /// <summary>
        /// Update the mouse position from the current Hue value.
        /// </summary>
		private void UpdateMousePosFromHue()
        {
            if ( this.mOrientation == Orientation.Horizontal )
            {
                this.mMousePosition.X = (int)Math.Floor( this.mHue * (double)ClientRectangle.Width );
            }
            else
            {
                this.mMousePosition.Y = (int)Math.Floor( this.mHue * (double)ClientRectangle.Height );
            }
		}

        #endregion Methods
    }
}

