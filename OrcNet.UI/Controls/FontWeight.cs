﻿
namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="FontWeight"/> class.
    /// </summary>
    public enum FontWeight
    {
        /// <summary>
        /// Normal density of the strokes.
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Heavy density of the strokes.
        /// </summary>
        Bold = 1
    }
}
