﻿using System;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Border"/> class.
    /// </summary>
	public class Border : Decorator
	{
        #region Statics

        /// <summary>
        /// Stores the Threshold to catch borders for sizing
        /// </summary>
        internal static int BorderThreshold = 5;

        #endregion Statics

        #region Fields

        /// <summary>
        /// Stores the control background brush.
        /// </summary>
        private ABrush mBackground;

        /// <summary>
        /// Stores the border corner radius.
        /// </summary>
        private CornerRadius mCornerRadius;

        /// <summary>
        /// Stores the border thickness.
        /// </summary>
        private Thickness mBorderThickness;

        /// <summary>
        /// Stores the border brush.
        /// </summary>
        private SolidColorBrush mBorderBrush;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the control background brush.
        /// </summary>
        public ABrush Background
        {
            get
            {
                return this.mBackground;
            }
            set
            {
                if ( this.mBackground != value )
                {
                    this.mBackground = value;
                    this.InvalidateVisual();
                }
            }
        }

        /// <summary>
        /// Gets the client region.
        /// </summary>
        public override Rectangle ClientRectangle
        {
            get
            {
                Rectangle lClientRegion = base.ClientRectangle;
                // For now, use the max thickness value.
                int lMaxThickness = (int)this.mBorderThickness.Max;
                lClientRegion.Inflate( -lMaxThickness );
                return lClientRegion;
            }
        }

        /// <summary>
        /// Gets or sets the border thickness.
        /// </summary>
        public Thickness BorderThickness
        {
            get
            {
                return this.mBorderThickness;
            }
            set
            {
                if ( this.mBorderThickness != value )
                {
                    this.mBorderThickness = value;
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the border corner radius.
        /// </summary>
        public CornerRadius CornerRadius
        {
            get
            {
                return this.mCornerRadius;
            }
            set
            {
                if ( this.mCornerRadius != value )
                {
                    this.mCornerRadius = value;
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the border brush.
        /// </summary>
        public SolidColorBrush BorderBrush
        {
            get
            {
                return this.mBorderBrush;
            }
            set
            {
                if ( this.mBorderBrush != value )
                {
                    this.mBorderBrush = value;
                    this.InvalidateArrange();
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Border"/> class.
        /// </summary>
        public Border() : 
        base()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            Size lSize = base.MeasureOverride();

            return lSize < Size.Zero ? lSize : new Size((int)(lSize.Width + this.mBorderThickness.Left + this.mBorderThickness.Right),
                                                        (int)(lSize.Height + this.mBorderThickness.Top + this.mBorderThickness.Bottom)); ;
        }
        
        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            Rectangle lBackground = new Rectangle( this.Slot.Size );

            double lMaxRadius = this.mCornerRadius.Max;
            double lMaxMargin = this.Margin.Max;
            double lMaxBorder = this.mBorderThickness.Max;

            this.mBackground.SetAsSource( pDrawingContext, lBackground );
            CairoHelpers.CairoRectangle( pDrawingContext, lBackground, lMaxRadius, lMaxBorder ); // Max for now.
            pDrawingContext.Fill();

            if ( lMaxBorder > 0 )
            {
                this.mBorderBrush.SetAsSource( pDrawingContext, lBackground );
                CairoHelpers.CairoRectangle( pDrawingContext, lBackground, lMaxRadius, lMaxBorder );
            }

            pDrawingContext.Save();
            if ( this.ClipToClientRect )
            {
                // Clip to client zone
                CairoHelpers.CairoRectangle( pDrawingContext, this.ClientRectangle, Math.Max( 0.0, lMaxRadius - lMaxMargin ) );
                pDrawingContext.Clip();
            }

            if ( this.Child != null)
            {
                this.Child.Paint( ref pDrawingContext );
            }

            pDrawingContext.Restore();
        }
        
		#endregion Methods
	}
}

