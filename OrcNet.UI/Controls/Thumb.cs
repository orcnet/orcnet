﻿namespace OrcNet.UI
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Thumb"/> class.
    /// </summary>
    public class Thumb : Control
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the mouse is pressed on the thumb or not.
        /// </summary>
        private bool mIsDragging;

        /// <summary>
        /// Stores the thumb point which the mouse has been clicked down at.
        /// </summary>
        private Point mOriginThumbPoint;

        /// <summary>
        /// Stores the position of the mouse at the click down.
        /// </summary>
        private Point mOriginScreenCoordPosition;

        /// <summary>
        /// Stores the position of the mouse at the previous Drag delta event.
        /// </summary>
        private Point mPreviousScreenCoordPosition;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on thumb drag started.
        /// </summary>
        public event DragStartedDelegate DragStarted;

        /// <summary>
        /// Event fired on thumb drag changes.
        /// </summary>
        public event DragDeltaDelegate DragChanged;

        /// <summary>
        /// Event fired on thumb drag complited or canceled.
        /// </summary>
        public event DragCompletedDelegate DragCompleted;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the mouse is pressed on the thumb or not.
        /// </summary>
        public bool IsDragging
        {
            get
            {
                return this.mIsDragging;
            }
            protected set
            {
                if ( this.mIsDragging == value )
                {
                    return;
                }

                bool lOldValue = this.mIsDragging;

                this.mIsDragging = value;

                this.NotifyPropertyChanged();
                this.OnDraggingChanged( lOldValue, this.mIsDragging );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Thumb"/> class.
        /// </summary>
        public Thumb()
        {
            this.mIsDragging = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.Mouse.LeftButton == ButtonState.Pressed )
            {
                if ( this.IsDragging == false )
                {
                    this.Focus();

                    this.IsDragging = true;

                    this.mOriginThumbPoint = this.GetLocalThumbPosition( pEventArgs.Position );
                    this.mPreviousScreenCoordPosition = this.mOriginScreenCoordPosition = pEventArgs.Position;

                    try
                    {
                        this.NotifyDragStarted( new DragStartedEventArgs( this.mOriginThumbPoint.X, this.mOriginThumbPoint.Y ) );
                    }
                    catch
                    {
                        this.CancelDrag();
                    }
                }
            }

            base.OnMouseDown( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on UIelement mouse moves.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseMove(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseMove( pSender, pEventArgs );

            if ( this.IsDragging )
            {
                if ( pEventArgs.Mouse.LeftButton == ButtonState.Pressed )
                {
                    // Compute the thumb local position
                    Point lThumbCoordPosition  = this.GetLocalThumbPosition( pEventArgs.Position );

                    Point lScreenCoordPosition = pEventArgs.Position;
                    if ( lScreenCoordPosition != this.mPreviousScreenCoordPosition )
                    {
                        this.mPreviousScreenCoordPosition = lScreenCoordPosition;
                        this.NotifyDragChanged( new DragDeltaEventArgs( lThumbCoordPosition.X - this.mOriginThumbPoint.X,
                                                                        lThumbCoordPosition.Y - this.mOriginThumbPoint.Y ) );
                    }
                }
                else
                {
                    this.IsDragging = false;
                    this.mOriginThumbPoint.X = 0;
                    this.mOriginThumbPoint.Y = 0;
                }
            }
        } 

        /// <summary>
        /// Delegate called on mouse up.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseUp(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.Mouse.LeftButton == ButtonState.Pressed )
            {
                if ( this.IsDragging )
                {
                    this.IsDragging = false;

                    Point lPosition = pEventArgs.Position;
                    this.NotifyDragComplited( new DragCompletedEventArgs( lPosition.X - this.mOriginScreenCoordPosition.X,
                                                                          lPosition.Y - this.mOriginScreenCoordPosition.Y,
                                                                          false ) );
                }
            }

            base.OnMouseUp( pSender, pEventArgs );
        }

        /// <summary>
        /// Cancels the dragging in process.
        /// </summary>
        public void CancelDrag()
        {
            if ( this.IsDragging )
            {
                this.IsDragging = false;
                this.NotifyDragComplited( new DragCompletedEventArgs( this.mPreviousScreenCoordPosition.X - this.mOriginScreenCoordPosition.X, 
                                                                      this.mPreviousScreenCoordPosition.Y - this.mOriginScreenCoordPosition.Y, 
                                                                      true ) );
            }
        }

        /// <summary>
        /// Delegate called on Is Dragging property changes.
        /// </summary>
        /// <param name="pOldValue"></param>
        /// <param name="pNewValue"></param>
        protected virtual void OnDraggingChanged(bool pOldValue, bool pNewValue)
        {

        }

        /// <summary>
        /// Notifies a drag has started.
        /// </summary>
        /// <param name="pEventArgs"></param>
        private void NotifyDragStarted(DragStartedEventArgs pEventArgs)
        {
            if ( this.DragStarted != null )
            {
                this.DragStarted( this, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies a drag changes.
        /// </summary>
        /// <param name="pEventArgs"></param>
        private void NotifyDragChanged(DragDeltaEventArgs pEventArgs)
        {
            if ( this.DragChanged != null )
            {
                this.DragChanged( this, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies a drag complited or canceled.
        /// </summary>
        /// <param name="pEventArgs"></param>
        private void NotifyDragComplited(DragCompletedEventArgs pEventArgs)
        {
            if ( this.DragCompleted != null )
            {
                this.DragCompleted( this, pEventArgs );
            }
        }

        /// <summary>
        /// Gets the local thumb position from the screen mouse position.
        /// </summary>
        /// <param name="pScreenPosition"></param>
        /// <returns></returns>
        private Point GetLocalThumbPosition(Point pScreenPosition)
        {
            return pScreenPosition - this.ScreenCoordinates( this.Slot ).TopLeft - this.ClientRectangle.TopLeft;
        }

        #endregion Methods
    }
}
