﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TextWrapping"/> enumeration.
    /// </summary>
    public enum TextWrapping
    {
        /// <summary>
        /// No text wrap.
        /// </summary>
        NoWrap = 0,

        /// <summary>
        /// Text wrap.
        /// </summary>
        Wrap
    }
}
