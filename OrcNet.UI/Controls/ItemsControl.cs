﻿using OrcNet.UI.Helpers;
using OrcNet.UI.Ximl;
using System;
using System.Collections;
using System.Threading;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ItemsControl"/> class.
    /// 
    /// NOTE: Can contain a set of items as child content.
    /// </summary>
    public class ItemsControl : Control, IAddChild
    {
        #region Fields

        /// <summary>
        /// Stores the items control panel in charge of defining the children positions.
        /// </summary>
        private APanel mItemsHost;

        /// <summary>
        /// Stores the scroll host.
        /// </summary>
        private ScrollViewer mScrollHost;

        /// <summary>
        /// Stores the items panel template owning the panel type to create.
        /// </summary>
        private ItemsPanelTemplate mItemsPanelTemplate;
        
        /// <summary>
        /// Stores the style applied to each item.
        /// </summary>
        private Ximl.Style mItemStyle;

        /// <summary>
        /// Stores the item template.
        /// </summary>
        private DataTemplate mItemTemplate;

        /// <summary>
        /// Stores the item template selector.
        /// </summary>
        private DataTemplateSelector mItemTemplateSelector;
        
        /// <summary>
        /// Stores the set of items in the control.
        /// </summary>
        protected ItemCollection mItems;

        /// <summary>
        /// Stores the loading thread.
        /// </summary>
        private UIThread mLoadingThread = null;

        /// <summary>
        /// Stores the flag indicating whether the items loading has been cancelled or not.
        /// </summary>
        private volatile bool mHasCancelledLoading;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on Items cleared.
        /// </summary>
        public event EventHandler<EventArgs> ItemsCleared;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the style applied to each item.
        /// </summary>
        public Ximl.Style ItemStyle
        {
            get
            {
                return this.mItemStyle;
            }
            set
            {
                this.mItemStyle = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the item template.
        /// </summary>
        public DataTemplate ItemTemplate
        {
            get
            {
                return this.mItemTemplate;
            }
            set
            {
                this.mItemTemplate = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the item template selector.
        /// </summary>
        public DataTemplateSelector ItemTemplateSelector
        {
            get
            {
                return this.mItemTemplateSelector;
            }
            set
            {
                this.mItemTemplateSelector = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the items panel template owning the panel type to create.
        /// </summary>
        public ItemsPanelTemplate ItemsPanel
        {
            get
            {
                return this.mItemsPanelTemplate;
            }
            set
            {
                this.mItemsPanelTemplate = value;
                this.NotifyPropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets the flag indicating whether the control has any item or not.
        /// </summary>
        public bool HasItems
        {
            get
            {
                return this.Items.Count > 0;
            }
        }

        /// <summary>
        /// Gets or sets the item host.
        /// </summary>
        internal APanel ItemsHost
        {
            get
            {
                return this.mItemsHost;
            }
            set
            {
                APanel lOldHost = this.mItemsHost;

                this.mItemsHost = value;

                this.OnItemHostChanged( this.mItemsHost, lOldHost );
            }
        }

        /// <summary>
        /// Stores the scroll host.
        /// </summary>
        internal ScrollViewer ScrollHost
        {
            get
            {
                if ( this.mItemsHost == null )
                {
                    return null;
                }

                AVisual lCurrent = this.mItemsHost;
                while ( this.mScrollHost == null &&
                        lCurrent != this &&
                        lCurrent != null )
                {
                    this.mScrollHost = lCurrent as ScrollViewer;
                    lCurrent = lCurrent.VisualParent;
                }

                return this.mScrollHost;
            }
        }

        /// <summary>
        /// Gets the set of items in the control.
        /// </summary>
        public ItemCollection Items
        {
            get
            {
                // Creates on demand.
                if ( this.mItems == null )
                {
                    this.CreateItemCollection();
                }

                return this.mItems;
            }
        }

        /// <summary>
        /// Gets or sets the item source collection
        /// </summary>
        public IEnumerable ItemSource
        {
            get
            {
                return this.Items.ItemsSource;
            }
            set
            {
                if ( this.Items.ItemsSource == value )
                {
                    return;
                }

                this.OnItemSourceChanging();

                this.ClearItems();

                this.Items.SetItemsSource( value );

                if ( value != null )
                {
                    this.mLoadingThread = new UIThread( this, this.LoadingWorker );
                    //this.mLoadingThread.Finished += (object pSender, EventArgs pEventArgs) => (pSender as ItemsControl).NotifyItemsLoaded( pSender, pEventArgs );
                    this.mLoadingThread.Start();
                }

                this.OnItemSourceChanged();

                this.NotifyPropertyChanged();
                this.NotifyPropertyChanged( "HasItems" );
            }
        } 

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemsControl"/> class.
        /// </summary>
        public ItemsControl()
        {
            
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Adds a new child to the control.
        /// </summary>
        /// <param name="pChild">The new child to add to the control.</param>
        public void AddChild(object pChild)
        {
            FrameworkElement lFE = pChild as FrameworkElement;
            if ( lFE != null )
            {
                this.AddChildInternal( lFE );
            }
            else
            {
                // Use a type converter to try to convert anything into a FrameworkElement.
                // E.g: string to TextBlock and so on...
            }
        }

        /// <summary>
        /// Creates a new item.
        /// </summary>
        /// <param name="pDataContext"></param>
        /// <returns></returns>
        private FrameworkElement CreateItem(object pDataContext)
        {
            DataTemplate lItemTemplate = null;
            if ( this.mItemTemplateSelector != null )
            {
                lItemTemplate = this.mItemTemplateSelector.SelectTemplate( pDataContext, this.mItemsHost );
            }
            else
            {
                lItemTemplate = this.mItemTemplate;
            }

            if ( lItemTemplate == null )
            {
                // Assign a default template.
            }

            FrameworkElement lElement = lItemTemplate.LoadContent() as FrameworkElement;
            lItemTemplate.ApplyTemplateContent( lElement, pDataContext );

            return lElement;
        }

        /// <summary>
        /// Adds a child
        /// </summary>
        /// <param name="pChild"></param>
        protected virtual void AddChildInternal(UIElement pChild)
        {
            if ( pChild == null )
            {
                return;
            }
            
            this.Items.Add( pChild );

            this.NotifyPropertyChanged( "HasItems" );
        }

        /// <summary>
        /// Removes a child
        /// </summary>
        /// <param name="pChild"></param>
        protected virtual void RemoveChildInternal(UIElement pChild)
        {
            if ( pChild == null )
            {
                return;
            }
            
            this.Items.Remove( pChild );
            
            if ( this.Items.Count == 0 )
            {
                this.NotifyPropertyChanged( "HasItems" );
            }
        }

        /// <summary>
        /// Clears the children.
        /// </summary>
        protected virtual void ClearItems()
        {
            this.Items.Clear();
            
            this.NotifyPropertyChanged( "HasItems" );
        }
        
        /// <summary>
        /// Delegate called on item host changes.
        /// </summary>
        /// <param name="pNewHost"></param>
        /// <param name="pOldHost"></param>
        protected virtual void OnItemHostChanged(APanel pNewHost, APanel pOldHost)
        {
            if ( pOldHost != null )
            {
                pOldHost.VisualParent  = null;
                pOldHost.LogicalParent = null;
            }

            if ( pNewHost != null )
            {
                pNewHost.VisualParent  = this;
                pNewHost.LogicalParent = this;
            }
        }

        /// <summary>
        /// Internal IsAncestorOf
        /// </summary>
        /// <param name="pDescendant">The visual to check.</param>
        /// <returns>True if child, false otherwise.</returns>
        protected override bool IsAncestorOfInternal(AVisual pDescendant)
        {
            foreach ( AVisual lElement in this.Items )
            {
                if ( lElement == pDescendant )
                {
                    return true;
                }

                if ( lElement.IsAncestorOf( pDescendant ) )
                {
                    return true;
                }
            }

            return base.IsAncestorOfInternal( pDescendant );
        }
        
        /// <summary>
        /// Delegate called on item source changing.
        /// </summary>
        protected virtual void OnItemSourceChanging()
        {

        }

        /// <summary>
        /// Delegate called on item source changed.
        /// </summary>
        protected virtual void OnItemSourceChanged()
        {

        }

        /// <summary>
        /// Creates the item collection.
        /// </summary>
        private void CreateItemCollection()
        {
            this.mItems = new ItemCollection( this );
        }
        
        /// <summary>
        /// Thread worker job delegate.
        /// </summary>
        /// <param name="pData">The data to load.</param>
        private void LoadingWorker(object pData)
        {
            // Loads item(s).
            this.LoadingJobInternal( pData as IList );
        }

        /// <summary>
        /// Loading job to process on item source changes.
        /// </summary>
        /// <param name="pData"></param>
        protected virtual void LoadingJobInternal(IList pData)
        {
            if ( pData == null )
            {
                return;
            }

            if ( this.mHasCancelledLoading )
            {
                return;
            }

            int lCounter = 0;
            while ( lCounter < pData.Count )
            {
                if ( this.mHasCancelledLoading )
                {
                    return;
                }

                FrameworkElement lItem = this.CreateItem( pData[ lCounter ] );

                this.AddChildInternal( lItem );
            }
                
            Thread.Sleep( 1 );
        }
        
        /// <summary>
        /// Cancels the loading thread.
        /// </summary>
        private void CancelLoadingThread()
        {
            if ( this.mLoadingThread != null )
            {
                this.mLoadingThread.Cancel();
            }
        }
        
        /// <summary>
        /// Finds the visual having the given name
        /// </summary>
        /// <param name="pName">The name to look for.</param>
        /// <returns></returns>
        public override AVisual FindByName(string pName)
        {
            if ( this.Name == pName )
            {
                return this;
            }

            AVisual lElement = null;
            foreach ( FrameworkElement lChild in this.Items )
            {
                lElement = lChild.FindByName( pName );
                if ( lElement != null )
                {
                    return lElement;
                }
            }

            return null;
        }
                
        ///<summary>
        /// Return the ItemsControl that owns the given container element
        ///</summary>
        ///<param name="pContainer">The container to get the parent item control of.</param>
        public static ItemsControl ItemsControlFromItemContainer(AVisual pContainer)
        {
            UIElement lUI = pContainer as UIElement;
            if ( lUI == null )
            {
                return null;
            }
 
            // ui appeared in items collection
            ItemsControl lItemControl = LogicalTreeHelper.GetParent( lUI ) as ItemsControl;
            if ( lItemControl != null )
            {
                // this is the right ItemsControl as long as the item
                // is (or is eligible to be) its own container
                //IGeneratorHost host = lItemControl as IGeneratorHost;
                //if (host.IsItemItsOwnContainer( lItemControl ))
                //{
                //    return lItemControl;
                //}
                //else
                {
                    //return null;
                    return lItemControl;
                }
            }
 
            lUI = VisualTreeHelper.GetParent( lUI ) as UIElement;
 
            return ItemsControl.GetItemsOwner( lUI );
        }

        /// <summary>
        ///     Returns the ItemsControl for which element is an ItemsHost.
        ///     More precisely, if element is marked by setting IsItemsHost="true"
        ///     in the style for an ItemsControl, or if element is a panel created
        ///     by the ItemsPresenter for an ItemsControl, return that ItemsControl.
        ///     Otherwise, return null.
        /// </summary>
        public static ItemsControl GetItemsOwner(AVisual pElement)
        {
            ItemsControl lContainer = null;
            APanel lPanel = pElement as APanel;

            if ( lPanel != null && 
                 lPanel.IsItemsHost )
            {
                // TO DO: Implement...
                //// see if element was generated for an ItemsPresenter
                //ItemsPresenter ip = ItemsPresenter.FromPanel( lPanel );
                //if (ip != null)
                //{
                //    // if so use the element whose style begat the ItemsPresenter
                //    lContainer = ip.Owner;
                //}
                //else
                {
                    // otherwise use element's templated parent
                    lContainer = lPanel.TemplatedParent as ItemsControl;
                }
            }

            return lContainer;
        }

        /// <summary>
        /// Gets the items owner
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        internal static AVisual GetItemsOwnerInternal(AVisual pElement)
        {
            ItemsControl lUnused;
            return GetItemsOwnerInternal( pElement, out lUnused );
        }

        /// <summary>
        /// Gets the items owner
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pItemsControl"></param>
        /// <returns></returns>
        internal static AVisual GetItemsOwnerInternal(AVisual pElement, out ItemsControl pItemsControl)
        {
            AVisual lContainer = null;
            APanel lPanel = pElement as APanel;
            pItemsControl = null;

            if ( lPanel != null && 
                 lPanel.IsItemsHost )
            {
                //// see if element was generated for an ItemsPresenter
                //ItemsPresenter ip = ItemsPresenter.FromPanel( lPanel );

                //if (ip != null)
                //{
                //    // if so use the element whose style begat the ItemsPresenter
                //    lContainer = ip.TemplatedParent;
                //    pItemsControl = ip.Owner;
                //}
                //else
                {
                    // otherwise use element's templated parent
                    lContainer = lPanel.TemplatedParent;
                    pItemsControl = lContainer as ItemsControl;
                }
            }

            return lContainer;
        }

        #endregion Methods
    }
}
