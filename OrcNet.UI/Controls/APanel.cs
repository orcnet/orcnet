﻿using Cairo;
using OrcNet.UI.Collections;
using OrcNet.UI.Input;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="APanel"/> class.
    /// </summary>
    public abstract class APanel : FrameworkElement, IAddChild
    {
        #region Fields

        /// <summary>
        /// Stores the sync root.
        /// </summary>
        protected readonly object mSyncRoot = new object();

        /// <summary>
        /// Stores the set of children.
        /// </summary>
        private UIElementCollection mChildren;

        /// <summary>
        /// Stores the control background brush.
        /// </summary>
        private ABrush mBackground;
        
        /// <summary>
        /// Stores the largest collection element.
        /// </summary>
        private FrameworkElement mLargestChild = null;

        /// <summary>
        /// Stores the tallest collection element.
        /// </summary>
        private FrameworkElement mTallestChild = null;

        /// <summary>
        /// Stores the flag indicating whether the panel is an Items host or not.
        /// </summary>
        private bool mIsItemsHost;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on children cleared.
        /// </summary>
        public event EventHandler<EventArgs> ChildrenCleared;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the visual children count.
        /// </summary>
        public override int VisualChildrenCount
        {
            get
            {
                if ( this.mChildren == null )
                {
                    return 0;
                }

                return this.mChildren.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the panel has any child or not.
        /// </summary>
        public bool HasChildren
        {
            get
            {
                return this.InternalChildren.Count > 0;
            }
        }

        /// <summary>
        /// Gets the set of children.
        /// </summary>
        public UIElementCollection Children
        {
            get
            {
                return this.InternalChildren;
            }
        }

        /// <summary>
        /// Gets the set of children.
        /// </summary>
        protected internal UIElementCollection InternalChildren
        {
            get
            {
                if ( this.mChildren == null )
                {
                    if ( this.IsItemsHost )
                    {
                        // Children must not have their logical parent reset.
                        this.EnsureEmptyChildren( null );

                        // Create a generator.
                        // EnsureGenerator();
                    }
                    else
                    {
                        this.EnsureEmptyChildren( this );
                    }
                }
                
                return this.mChildren;
            }
        }

        /// <summary>
        /// Gets the control background brush.
        /// </summary>
        public ABrush Background
        {
            get
            {
                return this.mBackground;
            }
            set
            {
                if ( this.mBackground != value )
                {
                    this.mBackground = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateVisual();
                }
            }
        }
        
        /// <summary>
        /// Gets the largest collection element.
        /// </summary>
        internal FrameworkElement LargestChild
        {
            get
            {
                return this.mLargestChild;
            }
        }

        /// <summary>
        /// Gets the tallest collection element.
        /// </summary>
        internal FrameworkElement TallestChild
        {
            get
            {
                return this.mTallestChild;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the panel is an Items host or not.
        /// </summary>
        public bool IsItemsHost
        {
            get
            {
                return this.mIsItemsHost;
            }
            set
            {
                if ( this.mIsItemsHost == value )
                {
                    return;
                }

                bool lPrevious = this.mIsItemsHost;
                this.mIsItemsHost = value;

                this.OnIsItemsHostChanged( lPrevious, this.mIsItemsHost );

                this.NotifyPropertyChanged();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APanel"/> class.
        /// </summary>
        protected APanel()
        {
            
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new child to a container.
        /// </summary>
        /// <param name="pChild"></param>
        public void AddChild(object pChild)
        {
            if ( pChild == null )
            {
                throw new ArgumentNullException("pChild");
            }

            if ( this.IsItemsHost )
            {
                throw new InvalidOperationException( "Item Host cannot add children." );
            }

            UIElement lElement = pChild as UIElement;

            if ( lElement == null )
            {
                throw new ArgumentException( "Unexpected child type." );
            }

            this.AddChild( lElement );
        }

        /// <summary>
        /// Ensures the children collection be created or empty.
        /// </summary>
        /// <param name="pLogicalParent"></param>
        private void EnsureEmptyChildren(FrameworkElement pLogicalParent)
        {
            if ( this.mChildren == null || 
                 this.mChildren.LogicalParent != pLogicalParent )
            {
                this.mChildren = this.CreateUIElementCollection( pLogicalParent );
            }
            else
            {
                this.ClearChildren();
            }
        }

        /// <summary>
        /// Creates a new UIElementCollection. Panel-derived class can create its own version of
        /// UIElementCollection -derived class to add cached information to every child or to
        /// intercept any Add/Remove actions (for example, for incremental layout update)
        /// </summary>
        protected virtual UIElementCollection CreateUIElementCollection(FrameworkElement pLogicalParent)
        {
            return new UIElementCollection( this, pLogicalParent );
        }

        /// <summary>
        /// Delegate called on is items host flag changes.
        /// </summary>
        /// <param name="pOldValue">The previous value.</param>
        /// <param name="pNewValue">The new value.</param>
        protected virtual void OnIsItemsHostChanged(bool pOldValue, bool pNewValue)
        {
            AVisual lParent = ItemsControl.GetItemsOwnerInternal( this );
            ItemsControl lItemsControl = lParent as ItemsControl;
            APanel lOldItemsHost = null;

            if ( lItemsControl != null )
            {
                lOldItemsHost = lItemsControl.ItemsHost;
                lItemsControl.ItemsHost = this;
            }

            //if ( lOldItemsHost != null &&
            //     lOldItemsHost != this )
            //{

            //}
        }

        /// <summary>
        /// Adds a new child to the panel.
        /// </summary>
        /// <param name="pChild"></param>
        protected internal virtual void AddChild(UIElement pChild)
        {
            if ( pChild == null )
            {
                return;
            }

            lock ( this.mSyncRoot )
            {
                pChild.LayoutUpdated += this.OnChildLayoutChanges;
                this.Children.Add( pChild );
            }
            
            this.NotifyPropertyChanged( "HasChildren" );
        }

        /// <summary>
        /// Removes a child from the panel.
        /// </summary>
        /// <param name="pChild"></param>
        protected internal virtual void RemoveChild(UIElement pChild)
        {
            if ( pChild == null )
            {
                return;
            }
            
            pChild.LayoutUpdated -= this.OnChildLayoutChanges;

            lock ( this.mSyncRoot )
            {
                this.Children.Remove( pChild );
            }

            if ( pChild == this.mLargestChild && 
                 this.Width == cFitSizeValue )
            {
                this.SearchLargestChild();
            }

            if ( pChild == this.mTallestChild && 
                 this.Height == cFitSizeValue )
            {
                this.SearchTallestChild();
            }
            
            if ( this.mChildren.Count == 0 )
            {
                this.NotifyPropertyChanged( "HasChildren" );
            }
        }

        /// <summary>
        /// Clears the children.
        /// </summary>
        protected internal virtual void ClearChildren()
        {
            lock ( this.mSyncRoot )
            {
                if ( this.mChildren != null && 
                     this.mChildren.Count > 0 )
                {
                    this.mChildren.ClearInternal( pElt => pElt.LayoutUpdated -= this.OnChildLayoutChanges );
                }
            }

            this.ResetChildrenMaxSize();
            this.NotifyChildrenCleared();

            this.NotifyPropertyChanged( "HasChildren" );
        }

        /// <summary>
        /// Internal IsAncestorOf
        /// </summary>
        /// <param name="pDescendant">The visual to check.</param>
        /// <returns>True if child, false otherwise.</returns>
        protected override bool IsAncestorOfInternal(AVisual pDescendant)
        {
            if ( this.mChildren == null ||
                 this.mChildren.Count == 0 )
            {
                return false;
            }

            foreach ( UIElement lElement in this.mChildren )
            {
                if ( lElement == pDescendant )
                {
                    return true;
                }

                if ( lElement.IsAncestorOf( pDescendant ) )
                {
                    return true;
                }
            }

            return false;
        }
        
        /// <summary>
        /// Delegate called on Data context changed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        public override void OnDataContextChanged(object pSender, PropertyValueChangedEventArgs pEventArgs)
        {
            base.OnDataContextChanged( pSender, pEventArgs );

            lock (this.mSyncRoot)
            {
                if ( this.mChildren != null )
                {
                    foreach ( FrameworkElement lElement in this.mChildren )
                    {
                        if (lElement.IsLocalDataContextNull & lElement.IsLocalLogicalParentNull)
                        {
                            lElement.OnDataContextChanged(pSender, pEventArgs);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Finds the visual having the given name
        /// </summary>
        /// <param name="pName">The name to look for.</param>
        /// <returns></returns>
        public override AVisual FindByName(string pName)
        {
            if (this.Name == pName)
            {
                return this;
            }

            AVisual lElement = null;
            lock (this.mSyncRoot)
            {
                if ( this.mChildren != null )
                {
                    foreach (FrameworkElement lChild in this.mChildren)
                    {
                        lElement = lChild.FindByName(pName);
                        if (lElement != null)
                        {
                            break;
                        }
                    }
                }
            }

            return lElement;
        }

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            Size lSize = base.MeasureOverride();

            if ( this.mChildren != null &&
                 this.mChildren.Count > 0 )
            {
                if (this.mLargestChild == null)
                {
                    this.SearchLargestChild();
                }

                if (this.mLargestChild == null)
                {
                    (this.mChildren[0] as FrameworkElement).Width = cFitSizeValue;
                    lSize.Width = -1; // cancel actual sizing to let child computation take place
                }

                if (this.mTallestChild == null)
                {
                    this.SearchTallestChild();
                }

                if (this.mTallestChild == null)
                {
                    (this.mChildren[0] as FrameworkElement).Height = cFitSizeValue;
                    lSize.Height = -1;
                }
            }

            return lSize;
        }

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected internal override void OnLayoutChanges(LayoutingType pLayoutType)
        {
            base.OnLayoutChanges( pLayoutType );

            if ( this.mChildren == null ||
                 this.mChildren.Count == 0 )
            {
                return;
            }

            // position smaller objects in group when group size is fit
            switch (pLayoutType)
            {
                case LayoutingType.Width:
                    foreach (FrameworkElement lElement in this.mChildren)
                    {
                        if (lElement.Width == cFitSizeValue)
                        {
                            lElement.RegisterForLayouting(LayoutingType.Width);
                        }
                        else
                        {
                            lElement.RegisterForLayouting(LayoutingType.X);
                        }
                    }
                    break;
                case LayoutingType.Height:
                    foreach (FrameworkElement lElement in this.mChildren)
                    {
                        if (lElement.Height == cFitSizeValue)
                        {
                            lElement.RegisterForLayouting(LayoutingType.Height);
                        }
                        else
                        {
                            lElement.RegisterForLayouting(LayoutingType.Y);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            Rectangle lBackground = new Rectangle( this.mSlot.Size );

            this.Background.SetAsSource( pDrawingContext, lBackground );

            CairoHelpers.CairoRectangle( pDrawingContext, lBackground, -1 );

            pDrawingContext.Fill();

            pDrawingContext.Save();

            if ( this.ClipToClientRect )
            {
                // clip to client zone
                CairoHelpers.CairoRectangle( pDrawingContext, this.ClientRectangle, -1 );
                pDrawingContext.Clip();
            }

            lock ( this.mSyncRoot )
            {
                if ( this.mChildren != null )
                {
                    foreach ( UIElement lElement in this.mChildren )
                    {
                        lElement.Paint( ref pDrawingContext );
                    }
                }
            }

            pDrawingContext.Restore();
        }

        /// <summary>
        /// Internal drawing cache update.
        /// </summary>
        /// <param name="pElementRegion">The element drawing region.</param>
        /// <param name="pSurface">The drawing surface.</param>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void InternalUpdateCache(ref Rectangle pElementRegion, ImageSurface pSurface, Context pDrawingContext)
        {
            Context lGraphicContext = new Context(pSurface);

            if ( this.VisualClip.Count > 0 )
            {
                this.VisualClip.ClearAndClip( lGraphicContext );

                Rectangle lBackground = new Rectangle( this.mSlot.Size );

                this.Background.SetAsSource( lGraphicContext, lBackground );

                CairoHelpers.CairoRectangle( lGraphicContext, lBackground, -1 );

                lGraphicContext.Fill();
                
                // clip to client zone
                CairoHelpers.CairoRectangle( lGraphicContext, this.ClientRectangle, -1 );
                lGraphicContext.Clip();

                lock (this.mSyncRoot)
                {
                    if ( this.mChildren != null )
                    {
                        foreach (FrameworkElement lElement in this.mChildren)
                        {
                            if (lElement.IsVisible == false)
                            {
                                continue;
                            }

                            if (this.VisualClip.Intersect(lElement.Slot + this.ClientRectangle.Position))
                            {
                                lElement.Paint(ref lGraphicContext);
                            }
                        }
                    }
                }

#if DEBUG_CLIP_RECTANGLE
				this.VisualClip.Stroke( lGraphicContext, Color.Amaranth.AdjustAlpha(0.8) );
#endif
            }

            lGraphicContext.Dispose();
        }

        /// <summary>
        /// Delegate called on mouse hover.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseHover(MouseMoveEventArgs pEventArgs)
        {
            if ( InputManager.Instance.HoverElement != this )
            {
                InputManager.Instance.HoverElement = this;
                this.OnMouseEnter( this, pEventArgs );
            }

            if ( this.mChildren != null )
            {
                for ( int lCurr = this.mChildren.Count - 1; lCurr >= 0; lCurr-- )
                {
                    if ( this.mChildren[ lCurr ].IsMouseIn( pEventArgs.Position ) )
                    {
                        this.mChildren[ lCurr ].OnMouseHover( pEventArgs );
                        return;
                    }
                }
            }

            base.OnMouseHover( pEventArgs );
        }

        /// <summary>
        /// Looks for the largest child.
        /// </summary>
        protected void SearchLargestChild()
        {
#if DEBUG_LAYOUTING
			Debug.WriteLine("\tSearch largest child");
#endif
            this.mLargestChild = null;
            this.mContentSize.Width = 0;

            if ( this.mChildren != null )
            {
                for (int lCurr = 0; lCurr < this.mChildren.Count; lCurr++)
                {
                    if (this.mChildren[lCurr].IsVisible == false)
                    {
                        continue;
                    }

                    if (this.mChildren[lCurr].State.HasFlag(LayoutingType.Width))
                    {
                        continue;
                    }

                    if (this.mChildren[lCurr].Slot.Width > this.mContentSize.Width)
                    {
                        this.mContentSize.Width = this.mChildren[lCurr].Slot.Width;
                        this.mLargestChild = this.mChildren[lCurr] as FrameworkElement;
                    }
                }
            }
        }

        /// <summary>
        /// Looks for the tallest child.
        /// </summary>
        protected void SearchTallestChild()
        {
#if DEBUG_LAYOUTING
			Debug.WriteLine("\tSearch tallest child");
#endif
            this.mTallestChild = null;
            this.mContentSize.Height = 0;

            if ( this.mChildren != null )
            {
                for (int lCurr = 0; lCurr < this.mChildren.Count; lCurr++)
                {
                    if (this.mChildren[lCurr].IsVisible == false)
                    {
                        continue;
                    }

                    if (this.mChildren[lCurr].State.HasFlag(LayoutingType.Height))
                    {
                        continue;
                    }

                    if (this.mChildren[lCurr].Slot.Height > this.mContentSize.Height)
                    {
                        this.mContentSize.Height = this.mChildren[lCurr].Slot.Height;
                        this.mTallestChild = this.mChildren[lCurr] as FrameworkElement;
                    }
                }
            }
        }

        /// <summary>
        /// Resets the tracked objects.
        /// </summary>
        protected void ResetChildrenMaxSize()
        {
            this.mLargestChild = null;
            this.mTallestChild = null;
            this.mContentSize = 0;
        }

        /// <summary>
        /// Delegate called on child layout changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected virtual void OnChildLayoutChanges(object pSender, LayoutingEventArgs pEventArgs)
        {
            FrameworkElement lElement = pSender as FrameworkElement;

            switch (pEventArgs.LayoutType)
            {
                case LayoutingType.Width:
                    if (this.Width != cFitSizeValue)
                    {
                        return;
                    }

                    if (lElement.Slot.Width > this.mContentSize.Width)
                    {
                        this.mLargestChild = lElement;
                        this.mContentSize.Width = lElement.Slot.Width;
                    }
                    else if (lElement == this.mLargestChild)
                    {
                        this.SearchLargestChild();
                    }

                    this.RegisterForLayouting(LayoutingType.Width);
                    break;
                case LayoutingType.Height:
                    if (this.Height != cFitSizeValue)
                    {
                        return;
                    }

                    if (lElement.Slot.Height > this.mContentSize.Height)
                    {
                        this.mTallestChild = lElement;
                        this.mContentSize.Height = lElement.Slot.Height;
                    }
                    else if (lElement == this.mTallestChild)
                    {
                        this.SearchTallestChild();
                    }

                    this.RegisterForLayouting(LayoutingType.Height);
                    break;
            }
        }
        
        /// <summary>
        /// Notifies the children have been cleared.
        /// </summary>
        protected void NotifyChildrenCleared()
        {
            if ( this.ChildrenCleared != null )
            {
                this.ChildrenCleared( this, new EventArgs() );
            }
        }

        /// <summary>
        /// Gets the visual child at the given index if any.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        protected override AVisual GetVisualChild(int pIndex)
        {
            if ( this.mChildren == null )
            {
                throw new ArgumentOutOfRangeException("pIndex", pIndex, "Out of range index");
            }
            
            return this.mChildren[ pIndex ];
        }

        #endregion Methods
    }
}
