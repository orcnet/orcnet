﻿using OrcNet.Core.Datastructures;
using OrcNet.UI.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Grid"/> class.
    /// 
    /// Simple grid container
    /// Allow symetric placement of children on a grid,
    /// excedental child (above grid sizing) are ignored
    /// and invisible child keep their place in the grid
    /// </summary>
    public class Grid : APanel, IAddChild
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the cell structure is dirty or not.
        /// </summary>
        private bool mIsCellStructureDirty;

        /// <summary>
        /// Stores the cell info by child element.
        /// </summary>
        private Dictionary<UIElement, CellInfo> mCellsInfo;

        /// <summary>
        /// Stores the flag indicating whether the grid lines must be drawn or not.
        /// </summary>
        private bool mShowGridLines;

        /// <summary>
        /// Stores the set of row definitions.
        /// </summary>
        private RowDefinitionCollection mRows;

        /// <summary>
        /// Stores the set of column definitions.
        /// </summary>
        private ColumnDefinitionCollection mColumns;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the grid lines must be drawn or not.
        /// </summary>
        public bool ShowGridLines
        {
            get
            {
                return this.mShowGridLines;
            }
            set
            {
                if ( this.mShowGridLines == value )
                {
                    return;
                }

                this.mShowGridLines = value;

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the column count.
        /// </summary>
        private int ColumnCount
        {
            get
            {
                return this.mColumns.Count;
            }
        }

        /// <summary>
        /// Gets the row count.
        /// </summary>
        private int RowCount
        {
            get
            {
                return this.mRows.Count;
            }
        }

        /// <summary>
        /// Gets a single cell width.
        /// </summary>
        private int CellWidth
        {
            get
            {
                return (this.Slot.Width - (this.ColumnCount - 1)) / this.ColumnCount;
            }
        }

        /// <summary>
        /// Gets a single cell height.
        /// </summary>
        private int CellHeight
        {
            get
            {
                return (this.Slot.Height - (this.RowCount - 1)) / this.RowCount;
            }
        }

        /// <summary>
        /// Gets the set of row definitions.
        /// </summary>
        public RowDefinitionCollection RowDefinitions
        {
            get
            {
                return this.mRows;
            }
        }

        /// <summary>
        /// Gets the set of column definitions.
        /// </summary>
        public ColumnDefinitionCollection ColumnDefinitions
        {
            get
            {
                return this.mColumns;
            }
        }

        /// <summary>
        /// Gets the set of logical children if any.
        /// </summary>
        protected internal override IEnumerator LogicalChildren
        {
            get
            {
                bool lHasNoChildren = base.VisualChildrenCount == 0 || this.IsItemsHost;
                if ( lHasNoChildren )
                {
                    if ( ( this.mRows == null || this.RowCount == 0 ) &&
                         ( this.mColumns == null || this.ColumnCount == 0 ) )
                    {
                        return new EmptyEnumerator<object>();
                    }
                }

                return new GridChildrenCollectionEnumerator( this, lHasNoChildren == false );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Grid"/> class.
        /// </summary>
        public Grid() : 
        base()
		{
            this.mShowGridLines = false;
            this.mIsCellStructureDirty = true;
            this.mRows = new RowDefinitionCollection();
            this.mColumns = new ColumnDefinitionCollection();
            this.mCellsInfo = new Dictionary<UIElement, CellInfo>();

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the given element column index.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns>The column index, -1 otherwise.</returns>
        public int GetColumn(UIElement pElement)
        {
            CellInfo lInfo;
            if ( this.mCellsInfo.TryGetValue( pElement, out lInfo ) )
            {
                return lInfo.ColumnIndex;
            }

            return -1;
        }

        /// <summary>
        /// Gets the given element column span.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns>The column span, -1 otherwise.</returns>
        public int GetColumnSpan(UIElement pElement)
        {
            CellInfo lInfo;
            if ( this.mCellsInfo.TryGetValue( pElement, out lInfo ) )
            {
                return lInfo.ColumnSpan;
            }

            return -1;
        }

        /// <summary>
        /// Gets the given element row index.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns>The row index, -1 otherwise.</returns>
        public int GetRow(UIElement pElement)
        {
            CellInfo lInfo;
            if ( this.mCellsInfo.TryGetValue( pElement, out lInfo ) )
            {
                return lInfo.RowIndex;
            }

            return -1;
        }

        /// <summary>
        /// Gets the given element row span.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns>The row span, -1 otherwise.</returns>
        public int GetRowSpan(UIElement pElement)
        {
            CellInfo lInfo;
            if ( this.mCellsInfo.TryGetValue( pElement, out lInfo ) )
            {
                return lInfo.RowSpan;
            }

            return -1;
        }

        /// <summary>
        /// Sets the given element column index.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pIndex">The element column index.</param>
        public void SetColumn(UIElement pElement, int pIndex)
        {
            CellInfo lInfo;
            if ( this.mCellsInfo.TryGetValue( pElement, out lInfo ) )
            {
                lInfo.ColumnIndex = pIndex;
            }
            else
            {
                this.mCellsInfo[ pElement ] = lInfo = new CellInfo() { ColumnIndex = pIndex };
            }

            this.OnCellChanged( lInfo );
        }

        /// <summary>
        /// Sets the given element column span.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pSpan">The element column span.</param>
        public void SetColumnSpan(UIElement pElement, int pSpan)
        {
            CellInfo lInfo;
            if ( this.mCellsInfo.TryGetValue( pElement, out lInfo ) )
            {
                lInfo.ColumnSpan = pSpan;
            }
            else
            {
                this.mCellsInfo[ pElement ] = lInfo = new CellInfo() { ColumnSpan = pSpan };
            }

            this.OnCellChanged( lInfo );
        }

        /// <summary>
        /// Sets the given element row index.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pIndex">The element row index.</param>
        public void SetRow(UIElement pElement, int pIndex)
        {
            CellInfo lInfo;
            if ( this.mCellsInfo.TryGetValue( pElement, out lInfo ) )
            {
                lInfo.RowIndex = pIndex;
            }
            else
            {
                this.mCellsInfo[ pElement ] = lInfo = new CellInfo() { RowIndex = pIndex };
            }

            this.OnCellChanged( lInfo );
        }

        /// <summary>
        /// Sets the given element row span.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pSpan">The element row span.</param>
        public void SetRowSpan(UIElement pElement, int pSpan)
        {
            CellInfo lInfo;
            if ( this.mCellsInfo.TryGetValue( pElement, out lInfo ) )
            {
                lInfo.RowSpan = pSpan;
            }
            else
            {
                this.mCellsInfo[ pElement ] = lInfo = new CellInfo() { RowSpan = pSpan };
            }

            this.OnCellChanged( lInfo );
        }

        /// <summary>
        /// Computes the children positions.
        /// </summary>
        private void ComputeChildrenPositions()
		{
            if ( this.mIsCellStructureDirty )
            {
                this.ValidateCells();
                this.mIsCellStructureDirty = false;
            }

            int lRowCellCount    = this.RowCount;
            int lColumnCellCount = this.ColumnCount;
            int lCellWidth  = this.CellWidth;
			int lCellHeight = this.CellHeight;
            foreach ( KeyValuePair<UIElement, CellInfo> lCell in this.mCellsInfo )
            {
                if ( lCell.Key.IsVisible == false )
                {
                    continue;
                }

                FrameworkElement lElement = lCell.Key as FrameworkElement;
                CellInfo lInfo = lCell.Value as CellInfo;
                // Ensure Item are not realigned
                lElement.HorizontalAlignment = HorizontalAlignment.Left;
                lElement.VerticalAlignment   = VerticalAlignment.Top;
                lElement.Left   = lInfo.ColumnIndex * (lCellWidth);
                lElement.Top    = lInfo.RowIndex * (lCellHeight);
                lElement.Width  = lCellWidth * lInfo.ColumnSpan;
                lElement.Height = lCellHeight * lInfo.RowSpan;
            }
		}

        /// <summary>
        /// Validates the cells info so that each element be placed at an empty spot or at a spot .
        /// </summary>
        private void ValidateCells()
        {
            foreach ( CellInfo lCell in this.mCellsInfo.Values )
            {
                //  read indices from the corresponding properties

                //      clamp to value < number_of_columns
                //      column >= 0 is guaranteed.
                lCell.ColumnIndex = Math.Min( lCell.ColumnIndex, this.ColumnCount - 1 );
                //      clamp to value < number_of_rows
                //      row >= 0 is guaranteed.
                lCell.RowIndex = Math.Min( lCell.RowIndex, this.RowCount - 1 );

                //  read span properties

                //      clamp to not exceed beyond right side of the grid
                //      column_span > 0 is guaranteed.
                lCell.ColumnSpan = Math.Min( lCell.ColumnSpan, this.ColumnCount - lCell.ColumnIndex );

                //      clamp to not exceed beyond bottom side of the grid
                //      row_span > 0 is guaranteed.
                lCell.RowSpan = Math.Min( lCell.RowSpan, this.RowCount - lCell.RowIndex );

                Debug.Assert( 0 <= lCell.ColumnIndex && lCell.ColumnIndex < this.ColumnCount );
                Debug.Assert( 0 <= lCell.RowIndex && lCell.RowIndex < this.RowCount );

                lCell.LengthTypeU = this.GetLengthTypeForRange( this.mRows, lCell.ColumnIndex, lCell.ColumnSpan );
                lCell.LengthTypeV = this.GetLengthTypeForRange( this.mColumns, lCell.RowIndex, lCell.RowSpan );
            }
        }

        /// <summary>
        /// Gets a cell's length type for the given range.
        /// </summary>
        /// <param name="pDefinitions"></param>
        /// <param name="pStartIndex"></param>
        /// <param name="pCellCount"></param>
        /// <returns></returns>
        private GridUnitType GetLengthTypeForRange(IEnumerable<IDefinition> pDefinitions, int pStartIndex, int pCellCount)
        {
            Debug.Assert( 0 < pCellCount && 0 <= pStartIndex && (pStartIndex + pCellCount) <= pDefinitions.Count() );

            GridUnitType lLengthType = GridUnitType.Auto;
            int lIndex = pStartIndex + pCellCount - 1;

            do
            {
                lLengthType |= pDefinitions.ElementAt( lIndex ).UserSize.GridUnitType;
            }
            while ( --lIndex >= pStartIndex );

            return lLengthType;
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            if ( pLayoutType == LayoutingType.ArrangeChildren )
            {
                this.ComputeChildrenPositions();
                
                return this.Slot;
            }

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Adds a new child to the panel.
        /// </summary>
        /// <param name="pChild"></param>
        protected internal override void AddChild(UIElement pChild)
		{
			base.AddChild( pChild );

            this.mCellsInfo[ pChild ] = new CellInfo();

            this.RegisterForLayouting( LayoutingType.ArrangeChildren );
		}

        /// <summary>
        /// Removes a child from the panel.
        /// </summary>
        /// <param name="pChild"></param>
		protected internal override void RemoveChild(UIElement pChild)
		{
			base.RemoveChild( pChild );

            this.mCellsInfo.Remove( pChild );
            
            this.RegisterForLayouting( LayoutingType.ArrangeChildren );
		}

        /// <summary>
        /// Delegate called on cell changed.
        /// </summary>
        /// <param name="pCell"></param>
        private void OnCellChanged(CellInfo pCell)
        {
            this.mIsCellStructureDirty = true;
            this.InvalidateMeasure();
        }

        #endregion Methods
        
        #region Inner Classes

        /// <summary>
        /// Definition of the <see cref="CellInfo"/> class.
        /// </summary>
        private class CellInfo
        {
            #region Fields

            /// <summary>
            /// Stores the cell row index.
            /// </summary>
            private int mRowIndex;

            /// <summary>
            /// Stores the cell column index.
            /// </summary>
            private int mColumnIndex;

            /// <summary>
            /// Stores the cell row span.
            /// </summary>
            private int mRowSpan;

            /// <summary>
            /// Stores the cell column span.
            /// </summary>
            private int mColumnSpan;

            /// <summary>
            /// Stores the cell's width length type.
            /// </summary>
            private GridUnitType mLengthTypeU;

            /// <summary>
            /// Stores the cell's height length type.
            /// </summary>
            private GridUnitType mLengthTypeV;

            #endregion Fields

            #region Properties

            /// <summary>
            /// Gets or sets the cell's width length type.
            /// </summary>
            public GridUnitType LengthTypeU
            {
                get
                {
                    return this.mLengthTypeU;
                }
                set
                {
                    this.mLengthTypeU = value;
                }
            }

            /// <summary>
            /// Gets or sets the cell's height length type.
            /// </summary>
            public GridUnitType LengthTypeV
            {
                get
                {
                    return this.mLengthTypeV;
                }
                set
                {
                    this.mLengthTypeV = value;
                }
            }

            /// <summary>
            /// Gets the flag indicating whether the width is auto sized or not.
            /// </summary>
            public bool IsAutoWidth
            {
                get
                {
                    return (this.mLengthTypeU & GridUnitType.Auto) == GridUnitType.Auto;
                }
            }

            /// <summary>
            /// Gets the flag indicating whether the width is star sized or not.
            /// </summary>
            public bool IsStarWidth
            {
                get
                {
                    return (this.mLengthTypeU & GridUnitType.Star) == GridUnitType.Star;
                }
            }

            /// <summary>
            /// Gets the flag indicating whether the height is auto sized or not.
            /// </summary>
            public bool IsAutoHeight
            {
                get
                {
                    return (this.mLengthTypeV & GridUnitType.Auto) == GridUnitType.Auto;
                }
            }

            /// <summary>
            /// Gets the flag indicating whether the height is star sized or not.
            /// </summary>
            public bool IsStarHeight
            {
                get
                {
                    return (this.mLengthTypeV & GridUnitType.Star) == GridUnitType.Star;
                }
            }

            /// <summary>
            /// Gets or sets the cell row index.
            /// </summary>
            public int RowIndex
            {
                get
                {
                    return this.mRowIndex;
                }
                set
                {
                    this.mRowIndex = value;
                }
            }

            /// <summary>
            /// Gets or sets the cell column index.
            /// </summary>
            public int ColumnIndex
            {
                get
                {
                    return this.mColumnIndex;
                }
                set
                {
                    this.mColumnIndex = value;
                }
            }

            /// <summary>
            /// Gets or sets the cell row span.
            /// </summary>
            public int RowSpan
            {
                get
                {
                    return this.mRowSpan;
                }
                set
                {
                    this.mRowSpan = value;
                }
            }

            /// <summary>
            /// Gets or sets the cell column span.
            /// </summary>
            public int ColumnSpan
            {
                get
                {
                    return this.mColumnSpan;
                }
                set
                {
                    this.mColumnSpan = value;
                }
            }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="CellInfo"/> class.
            /// </summary>
            public CellInfo()
            {
                this.mRowIndex = this.mColumnIndex = 0;
                this.mRowSpan = this.mColumnSpan = 1;
            }

            #endregion Constructor
        }

        /// <summary>
        /// Implementation of a simple enumerator of grid's logical children
        /// </summary>
        private class GridChildrenCollectionEnumerator : IEnumerator
        {
            #region Fields

            /// <summary>
            /// Stores the current enumerator index.
            /// </summary>
            private int mCurrentEnumerator;

            /// <summary>
            /// Stores the current child.
            /// </summary>
            private object mCurrentChild;

            /// <summary>
            /// Stores the column definition enumerator.
            /// </summary>
            private IEnumerator<ColumnDefinition> mEnumerator0;

            /// <summary>
            /// Stores the row definition enumerator.
            /// </summary>
            private IEnumerator<RowDefinition> mEnumerator1;

            /// <summary>
            /// Stores the UIelement collection.
            /// </summary>
            private UIElementCollection mEnumerator2Collection;

            /// <summary>
            /// Stores the second enumerator index.
            /// </summary>
            private int mEnumerator2Index;

            /// <summary>
            /// Stores the second enumerator count.
            /// </summary>
            private int mEnumerator2Count;

            #endregion Fields

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="GridChildrenCollectionEnumerator"/> class.
            /// </summary>
            /// <param name="pGrid"></param>
            /// <param name="pIncludeChildren"></param>
            internal GridChildrenCollectionEnumerator(Grid pGrid, bool pIncludeChildren)
            {
                this.mCurrentEnumerator = -1;
                this.mEnumerator0 = pGrid.ColumnDefinitions.GetEnumerator();
                this.mEnumerator1 = pGrid.RowDefinitions.GetEnumerator();
                this.mEnumerator2Index = 0;
                if ( pIncludeChildren )
                {
                    this.mEnumerator2Collection = pGrid.Children;
                    this.mEnumerator2Count = this.mEnumerator2Collection.Count;
                }
                else
                {
                    this.mEnumerator2Collection = null;
                    this.mEnumerator2Count = 0;
                }
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// Moves to the next child.
            /// </summary>
            /// <returns></returns>
            public bool MoveNext()
            {
                while ( this.mCurrentEnumerator < 3 )
                {
                    if ( this.mCurrentEnumerator >= 0 )
                    {
                        switch ( this.mCurrentEnumerator )
                        {
                            case (0):
                                if ( this.mEnumerator0.MoveNext() )
                                {
                                    this.mCurrentChild = this.mEnumerator0.Current;
                                    return true;
                                }
                                break;
                            case (1):
                                if ( this.mEnumerator1.MoveNext() )
                                {
                                    this.mCurrentChild = this.mEnumerator1.Current;
                                    return true;
                                }
                                break;
                            case (2):
                                if ( this.mEnumerator2Index < this.mEnumerator2Count )
                                {
                                    this.mCurrentChild = this.mEnumerator2Collection[ this.mEnumerator2Index ];
                                    this.mEnumerator2Index++;
                                    return (true);
                                }
                                break;
                        }
                    }

                    this.mCurrentEnumerator++;
                }
                return (false);
            }

            /// <summary>
            /// Gets the current object.
            /// </summary>
            public object Current
            {
                get
                {
                    if ( this.mCurrentEnumerator == -1 )
                    {
                        throw new InvalidOperationException( "Enumerator not started." );
                    }

                    if ( this.mCurrentEnumerator >= 3 )
                    {
                        throw new InvalidOperationException( "Reached the end of the enumerator." );
                    }
                    
                    return this.mCurrentChild;
                }
            }

            /// <summary>
            /// Resets the enumerator.
            /// </summary>
            public void Reset()
            {
                this.mCurrentEnumerator = -1;
                this.mCurrentChild = null;
                this.mEnumerator0.Reset();
                this.mEnumerator1.Reset();
                this.mEnumerator2Index = 0;
            }

            #endregion Methods
        }

        #endregion Inner Classes.
    }
}
