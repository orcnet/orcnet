﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ARangeBase"/> class.
    /// </summary>
	public abstract class ARangeBase : Control
    {
        #region Fields

        /// <summary>
        /// Stores the current value.
        /// </summary>
        private double mCurrentValue;

        /// <summary>
        /// Stores the minimum value.
        /// </summary>
        private double mMinValue;

        /// <summary>
        /// Stores the maximum value.
        /// </summary>
        private double mMaxValue;

        /// <summary>
        /// Stores the smallest step.
        /// </summary>
        private double mSmallStep;

        /// <summary>
        /// Stores the biggest step.
        /// </summary>
        private double mBigStep;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on value changes.
        /// </summary>
        public event EventHandler<PropertyValueChangedEventArgs> ValueChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the minimum value.
        /// </summary>
        public double Minimum
        {
            get
            {
                return this.mMinValue;
            }
            set
            {
                if ( this.mMinValue == value )
                {
                    return;
                }

                double lOldValue = this.mMinValue;

                this.mMinValue = value;
                this.NotifyPropertyChanged();

                this.OnMinimumChanged( lOldValue, this.mMinValue );

                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the maximum value.
        /// </summary>
        public double Maximum
        {
            get
            {
                return this.mMaxValue;
            }
            set
            {
                if ( this.mMaxValue == value )
                {
                    return;
                }

                double lOldValue = this.mMaxValue;

                this.mMaxValue = value;
                this.NotifyPropertyChanged();

                this.OnMaximumChanged( lOldValue, this.mMaxValue );

                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the smallest step.
        /// </summary>
        public double SmallIncrement
        {
            get
            {
                return this.mSmallStep;
            }
            set
            {
                if ( this.mSmallStep == value )
                {
                    return;
                }

                this.mSmallStep = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the biggest step.
        /// </summary>
        public double LargeIncrement
        {
            get
            {
                return this.mBigStep;
            }
            set
            {
                if ( this.mBigStep == value )
                {
                    return;
                }

                this.mBigStep = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the current value.
        /// </summary>
        public double Value
        {
            get
            {
                return mCurrentValue;
            }
            set
            {
                if ( value == mCurrentValue )
                {
                    return;
                }

                double lOldValue = this.mCurrentValue;

                if (value < this.mMinValue)
                {
                    this.mCurrentValue = this.mMinValue;
                }
                else if (value > this.mMaxValue)
                {
                    this.mCurrentValue = this.mMaxValue;
                }
                else
                {
                    this.mCurrentValue = value;
                }

                this.NotifyPropertyChanged();
                this.OnValueChanged( lOldValue, this.mCurrentValue );
                this.InvalidateArrange();
            }
        }

        #endregion Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ARangeBase"/> class.
        /// </summary>
        protected ARangeBase() : 
        this( 0.0, 100.0 )
		{

		}

        /// <summary>
        /// Initializes a new instance of the <see cref="ARangeBase"/> class.
        /// </summary>
        /// <param name="pMinimum">The numeric minimum.</param>
        /// <param name="pMaximum">THe numeric maximum.</param>
		protected ARangeBase(double pMinimum, double pMaximum) : 
        base()
		{
            this.mCurrentValue = 0.0;
            this.mMinValue  = pMinimum;
            this.mMaxValue  = pMaximum;
            this.mSmallStep = 1.0;
            this.mBigStep   = 5.0;
		}

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Delegate called on maximum changes
        /// </summary>
        /// <param name="pOldMaximum">The old maximum.</param>
        /// <param name="pNewMaximum">The new maximum.</param>
        protected virtual void OnMaximumChanged(double pOldMaximum, double pNewMaximum)
        {

        }

        /// <summary>
        /// Delegate called on minimum changes
        /// </summary>
        /// <param name="pOldMinimum">The old minimum.</param>
        /// <param name="pNewMinimum">The new minimum.</param>
        protected virtual void OnMinimumChanged(double pOldMinimum, double pNewMinimum)
        {

        }

        /// <summary>
        /// Delegate called on value changes.
        /// </summary>
        /// <param name="pOldValue">The old value</param>
        /// <param name="pNewValue">The new value</param>
        protected virtual void OnValueChanged(double pOldValue, double pNewValue)
        {
            if ( this.ValueChanged != null )
            {
                this.ValueChanged( this, new PropertyValueChangedEventArgs( pOldValue, pNewValue ) );
            }
        }

        /// <summary>
        /// Forces the length to be greater than 0 and smaller than the track length.
        /// </summary>
        /// <param name="pComponentLength"></param>
        /// <param name="pTrackLength"></param>
        protected static void CoerceLength(ref double pComponentLength, double pTrackLength)
        {
            if ( pComponentLength < 0)
            {
                pComponentLength = 0.0;
            }
            else if ( pComponentLength > pTrackLength || 
                      double.IsNaN( pComponentLength ) )
            {
                pComponentLength = pTrackLength;
            }
        }
        
        #endregion Methods
    }
}

