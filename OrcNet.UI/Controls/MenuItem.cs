﻿using System;
using OrcNet.Core;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="MenuItem"/> class.
    /// </summary>
	public class MenuItem : HeaderedItemsControl, ICommandSource
	{
        #region Fields

        /// <summary>
        /// Stores the popup template name.
        /// </summary>
        private const string PopupTemplateName = "Popup";

        /// <summary>
        /// Stores the menu command.
        /// </summary>
        private ICommand mCommand;

        /// <summary>
        /// Stores the menu command parameter.
        /// </summary>
        private object mCommandParameter;

        /// <summary>
        /// Stores the menu icon.
        /// </summary>
        private ImageSource mIcon;

        /// <summary>
        /// Stores the submenu popup.
        /// </summary>
        private Popup mPopup;

        /// <summary>
        /// Stores the flag indicating whether the submenu is opened or not.
        /// </summary>
        private bool mIsSubmenuOpened;

        Measure popWidth, popHeight;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on item submenu opened.
        /// </summary>
        public event EventHandler SubmenuOpened;

        /// <summary>
        /// Event fired on item submenu closed.
        /// </summary>
		public event EventHandler SubmenuClosed;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the command that will be executed.
        /// </summary>
        public ICommand Command
        {
            get
            {
                return this.mCommand;
            }
            set
            {
                if ( this.mCommand == value )
                {
                    return;
                }

                ICommand lOldCommand = this.mCommand;

                this.mCommand = value;

                this.OnCommandChanged( this.mCommand, lOldCommand );

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the optional command's parameter.
        /// </summary>
        public object CommandParameter
        {
            get
            {
                return this.mCommandParameter;
            }
            set
            {
                if ( this.mCommandParameter == value )
                {
                    return;
                }

                this.mCommandParameter = value;

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the submenu is opened or not.
        /// </summary>
		public bool IsSubmenuOpened
        {
			get
            {
                return this.mIsSubmenuOpened;
            }
			set
            {
				if ( this.mIsSubmenuOpened == value )
                {
                    return;
                }

				this.mIsSubmenuOpened = value;

                this.NotifyPropertyChanged();

                if ( this.mIsSubmenuOpened )
                {
                    this.OnSubmenuOpened( null );
                }
                else
                {
                    this.OnSubmenuClosed( null );
                }
			}
		}

        /// <summary>
        /// Gets the flag indicating whether the element is enabled or not.
        /// </summary>
        protected override bool IsEnabledCore
        {
			get
            {
                return this.Command == null ? base.IsEnabledCore : this.Command.CanExecute( this.CommandParameter );
            }
		}
        
		/// <summary>
        /// Gets or sets the menu icon.
        /// </summary>
		public ImageSource Icon
        {
			get
            {
                return this.mIcon;
            }
			set
            {
                if ( this.mIcon == value )
                {
                    return;
                }

				this.mIcon = value;

                this.NotifyPropertyChanged();
			}
		}
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItem"/> class.
        /// </summary>
        public MenuItem() : 
        base()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if ( this.mPopup != null )
            {
                this.mPopup.Closed -= this.OnPopupClosed;
            }

            this.mPopup = GetTemplateChild( PopupTemplateName ) as Popup;

            if ( this.mPopup != null )
            {
                this.mPopup.Closed += this.OnPopupClosed;
            }
        }

        /// <summary>
        /// Delegate called on opup closed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArg"></param>
        private void OnPopupClosed(object pSender, EventArgs pEventArg)
        {
            this.OnSubmenuClosed( pEventArg );
        }

        /// <summary>
        /// Delegate called on submenu opened.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnSubmenuOpened(EventArgs pEventArgs)
        {
            if ( this.SubmenuOpened != null )
            {
                this.SubmenuOpened( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on submenu closed.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnSubmenuClosed(EventArgs pEventArgs)
        {
            if ( this.SubmenuClosed != null )
            {
                this.SubmenuClosed( this, pEventArgs );
            }
        }

        /// <summary>
        /// Adds a child
        /// </summary>
        /// <param name="pChild"></param>
        protected override void AddChildInternal(UIElement pChild)
        {
            base.AddChildInternal( pChild );

            if ( pChild is Popup )
            {
                (pChild as Popup).PopupPosition = PopupPosition.Right;
            }
        }

        /// <summary>
        /// Delegate called on menu item click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnMenuItemClick(object pSender, MouseButtonEventArgs pEventArgs)
		{
            if ( this.mCommand != null )
            {
                this.mCommand.Execute( this.mCommandParameter );
            }
		}

        /// <summary>
        /// Internal method allowing specific check to know whether the given point is inside this element or not.
        /// </summary>
        /// <param name="pScreenPoint">The point to check.</param>
        /// <returns>True if contained, false otherwise.</returns>
        protected override bool InternalIsMouseIn(Point pScreenPoint)
        {
            return this.IsEnabled ? 
                   base.InternalIsMouseIn( pScreenPoint ) || (this.mPopup != null && this.mPopup.IsMouseIn( pScreenPoint )) : 
                   false;
        }
        
        /// <summary>
        /// Delegate called on UIElement mouse enter.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseEnter(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseEnter( pSender, pEventArgs );

            if ( this.IsSubmenuOpened == false )
            {
                this.IsSubmenuOpened = this.Items.Count > 0;
            }
        }

        /// <summary>
        /// Delegate called on UIElement mouse leave.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseLeave(object pSender, MouseMoveEventArgs pEventArgs)
        {
            if ( this.IsSubmenuOpened )
            {
                this.IsSubmenuOpened = false;
            }

            base.OnMouseLeave( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on command changed.
        /// </summary>
        /// <param name="pNewCommand"></param>
        /// <param name="pOldCommand"></param>
        protected virtual void OnCommandChanged(ICommand pNewCommand, ICommand pOldCommand)
        {
            if ( pOldCommand != null )
            {
                if ( pOldCommand is Command )
                {
                    (pOldCommand as Command).OnCanExecuteChanged();
                }
                
                pOldCommand.CanExecuteChanged -= this.OnCommandCanExecuteChanged;
                pOldCommand.RemovePropertyObserver( this );
            }

            if ( pNewCommand != null )
            {
                pNewCommand.CanExecuteChanged += this.OnCommandCanExecuteChanged;
                pNewCommand.AddPropertyObserver( this );

                if ( pNewCommand is Command )
                {
                    (pNewCommand as Command).OnCanExecuteChanged();
                }
            }
        }

        /// <summary>
        /// Delegate called on command can execute changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnCommandCanExecuteChanged(object pSender, EventArgs pEventArgs)
        {
            this.NotifyPropertyChanged( "IsEnabled" );
        }

        /// <summary>
        /// Delegate called on property changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        public void OnObservablePropertyChanged(IObservable pSender, OrcNet.Core.PropertyChangedEventArgs pEventArgs)
        {
            // Nothing to do.
        }

        #endregion Methods
    }
}

