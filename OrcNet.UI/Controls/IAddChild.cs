﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="IAddChild"/> interface allowing to add a child to a container.
    /// </summary>
    public interface IAddChild
    {
        #region Methods

        /// <summary>
        /// Adds a new child to a container.
        /// </summary>
        /// <param name="pChild"></param>
        void AddChild(object pChild);

        #endregion Methods
    }
}
