﻿using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ProgressBar"/> class.
    /// </summary>
    public class ProgressBar : ARangeBase
    {
        #region Fields

        /// <summary>
        /// Stores the constant indicator template name.
        /// </summary>
        private const string IndicatorTemplateName = "Indicator";

        /// <summary>
        /// Stores the progreesion indicator element
        /// </summary>
        private FrameworkElement mIndicator;

        /// <summary>
        /// Stores the progress bar orientation.
        /// </summary>
        private Orientation mOrientation;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the progress bar orientation.
        /// </summary>
        public Orientation Orientation
        {
            get
            {
                return this.mOrientation;
            }
            set
            {
                if ( this.mOrientation == value )
                {
                    return;
                }

                this.mOrientation = value;
                this.NotifyPropertyChanged();

                this.InvalidateVisual();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressBar"/> class.
        /// </summary>
        public ProgressBar() : 
        base()
        {
            this.mOrientation = Orientation.Horizontal;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.mIndicator = this.GetTemplateChild( IndicatorTemplateName ) as FrameworkElement;
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            if ( this.Maximum == 0.0 )
            {
                return;
            }

            if ( this.mIndicator != null )
            {
                this.mIndicator.Paint( ref pDrawingContext );
            }
        }
        
        /// <summary>
        /// Delegate called on value changes.
        /// </summary>
        /// <param name="pOldValue">The old value</param>
        /// <param name="pNewValue">The new value</param>
        protected override void OnValueChanged(double pOldValue, double pNewValue)
        {
            base.OnValueChanged( pOldValue, pNewValue );

            this.SetProgressBarIndicatorLength();
        }

        /// <summary>
        /// Delegate called on minimum changes
        /// </summary>
        /// <param name="pOldMinimum">The old minimum.</param>
        /// <param name="pNewMinimum">The new minimum.</param>
        protected override void OnMinimumChanged(double pOldMinimum, double pNewMinimum)
        {
            base.OnMinimumChanged( pOldMinimum, pNewMinimum );

            this.SetProgressBarIndicatorLength();
        }

        /// <summary>
        /// Delegate called on maximum changes
        /// </summary>
        /// <param name="pOldMaximum">The old maximum.</param>
        /// <param name="pNewMaximum">The new maximum.</param>
        protected override void OnMaximumChanged(double pOldMaximum, double pNewMaximum)
        {
            base.OnMaximumChanged( pOldMaximum, pNewMaximum );

            this.SetProgressBarIndicatorLength();
        }

        /// <summary>
        /// Updates the progression indicator new 
        /// </summary>
        private void SetProgressBarIndicatorLength()
        {
            if ( this.mIndicator != null )
            {
                double lMin = this.Minimum;
                double lMax = this.Maximum;
                double lVal = this.Value;
                
                double lPercent = lMax <= lMin ? 1.0 : (lVal - lMin) / (lMax - lMin);

                if ( this.Orientation == Orientation.Horizontal )
                {
                    this.mIndicator.Width = lPercent * this.DesiredSize.Width;
                }
                else
                {
                    this.mIndicator.Height = lPercent * this.DesiredSize.Height;
                }
            }
        }

        #endregion Methods
    }
}
