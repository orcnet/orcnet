﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DummyTemplate"/> class.
    /// </summary>
	public class DummyTemplate : ContentControl
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DummyTemplate"/> class.
        /// </summary>
        public DummyTemplate() : 
        base()
		{

		}

        #endregion Constructor
    }
}

