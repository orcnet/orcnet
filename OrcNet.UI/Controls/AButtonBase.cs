﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the abstract <see cref="AButtonBase"/> base class.
    /// </summary>
    public abstract class AButtonBase : ContentControl
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the button is pressed or not.
        /// </summary>
        private bool mIsPressed;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on button clicks.
        /// </summary>
        public event EventHandler Click;

        #endregion Events

        #region Properties

        /// <summary>
        /// Stores the flag indicating whether the button is pressed or not.
        /// </summary>
        public bool IsPressed
        {
            get
            {
                return this.mIsPressed;
            }
            set
            {
                if ( this.mIsPressed != value )
                {
                    this.mIsPressed = value;
                    this.NotifyPropertyChanged();
                    
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AButtonBase"/> class.
        /// </summary>
        protected AButtonBase() : 
        base()
        {
            this.mIsPressed = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            this.IsPressed = true;

            base.OnMouseDown( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on mouse click.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseClick(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( this.Click != null )
            {
                this.Click( this, new EventArgs() );
            }

            base.OnMouseClick( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on mouse up.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseUp(object pSender, MouseButtonEventArgs pEventArgs)
        {
            this.IsPressed = false;

            base.OnMouseUp( pSender, pEventArgs );
        }

        #endregion Methods
    }
}
