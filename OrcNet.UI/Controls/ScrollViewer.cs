﻿using System;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ScrollViewer"/> class.
    /// </summary>
	public class ScrollViewer : ContentControl
	{
        #region Fields

        /// <summary>
        /// Stores the constant horizontal ScrollBar template name.
        /// </summary>
        private const string HorizontalScrollBarTemplateName = "HorizontalScrollBar";

        /// <summary>
        /// Stores the constant vertical ScrollBar template name.
        /// </summary>
        private const string VerticalScrollBarTemplateName = "VerticalScrollBar";

        /// <summary>
        /// Stores the vertical scrollbar.
        /// </summary>
        private ScrollBar mVerticalScrollBar;

        /// <summary>
        /// Stores the horizontal scrollbar.
        /// </summary>
        private ScrollBar mHorizontalScrollBar;

        /// <summary>
        /// Stores the current mouse position.
        /// </summary>
        private Point mSavedMousePos;

        /// <summary>
        /// Stores the vertical scrollbar visibility.
        /// </summary>
        private ScrollBarVisibility mVerticalScrollBarVisibility;

        /// <summary>
        /// Stores the horizontal scrollbar visibility.
        /// </summary>
		private ScrollBarVisibility mHorizontalScrollBarVisibility;

        /// <summary>
        /// Stores the flag indicating whether the vertical scrollbar is visible or not.
        /// </summary>
        private bool mIsVerticalScrollBarVisible;

        /// <summary>
        /// Stores the flag indicating whether the horizontal scrollbar is visible or not.
        /// </summary>
        private bool mIsHorizontalScrollBarVisible;

        /// <summary>
        /// Stores the scroll current X position
        /// </summary>
		private double mHorizontalOffset = 0.0;

        /// <summary>
        /// Stores the scroll current Y position
        /// </summary>
		private double mVerticalOffset = 0.0;

        /// <summary>
        /// Stores the scroll speed.
        /// </summary>
		private int mScrollSpeed;

        #endregion Fields
        
        #region Events

        /// <summary>
        /// Event fired on scrolled.
        /// </summary>
        public event EventHandler<ScrollChangedEventArgs> ScrollChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the current mouse position.
        /// </summary>
        internal Point SavedMousePos
        {
            get
            {
                return this.mSavedMousePos;
            }
            set
            {
                this.mSavedMousePos = value;
            }
        }

        /// <summary>
        /// Gets or sets the vertical scrollbar visibility.
        /// </summary>
        public ScrollBarVisibility VerticalScrollBarVisibility
        {
            get
            {
                return this.mVerticalScrollBarVisibility;
            }
            set
            {
                this.mVerticalScrollBarVisibility = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the horizontal scrollbar visibility.
        /// </summary>
		public ScrollBarVisibility HorizontalScrollBarVisibility
        {
            get
            {
                return this.mHorizontalScrollBarVisibility;
            }
            set
            {
                this.mHorizontalScrollBarVisibility = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the vertical scrollbar is visible or not.
        /// </summary>
        public bool IsVerticalScrollBarVisible
        {
            get
            {
                return this.mIsVerticalScrollBarVisible;
            }
            private set
            {
                if ( this.mIsVerticalScrollBarVisible == value )
                {
                    return;
                }

                this.mIsVerticalScrollBarVisible = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the horizontal scrollbar is visible or not.
        /// </summary>
        public bool IsHorizontalScrollBarVisible
        {
            get
            {
                return this.mIsHorizontalScrollBarVisible;
            }
            private set
            {
                if ( this.mIsHorizontalScrollBarVisible == value )
                {
                    return;
                }

                this.mIsHorizontalScrollBarVisible = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the current scroll horizontal offset.
        /// </summary>
		public double HorizontalOffset
        {
			get
            {
				return this.mHorizontalOffset;
			}
			private set
            {
				if ( this.mHorizontalOffset == value )
                {
                    return;
                }

                double lOldValue = this.mHorizontalOffset;

				if ( value < 0.0 )
                {
                    this.mHorizontalOffset = 0.0;
                }
				else if ( value > this.ScrollableWidth )
                {
                    this.mHorizontalOffset = this.ScrollableWidth;
                }
				else
                {
                    this.mHorizontalOffset = value;
                }

                this.NotifyPropertyChanged();
				this.InvalidateVisual();

                this.OnScrollChanged( new ScrollChangedEventArgs( this.HorizontalOffset, this.VerticalOffset, this.HorizontalOffset - lOldValue, 0 ) );
			}
		}

        /// <summary>
        /// Gets the current scroll vertical offset.
        /// </summary>
		public double VerticalOffset
        {
			get
            {
				return this.mVerticalOffset;
			}
			private set
            {
                if ( this.mVerticalOffset == value )
                {
                    return;
                }

                double lOldValue = this.mVerticalOffset;

                if ( value < 0.0 )
                {
                    this.mVerticalOffset = 0.0;
                }
                else if ( value > this.ScrollableHeight )
                {
                    this.mVerticalOffset = this.ScrollableHeight;
                }
                else
                {
                    this.mVerticalOffset = value;
                }

				this.NotifyPropertyChanged();
				this.InvalidateVisual();

                this.OnScrollChanged( new ScrollChangedEventArgs( this.HorizontalOffset, this.VerticalOffset, 0, this.VerticalOffset - lOldValue ) );
			}
		}

        /// <summary>
        /// Gets the horizontal size of the scrolled content.
        /// </summary>
        public double ExtentWidth
        {
            get
            {
                AVisual lContent = this.Content as AVisual;
                return lContent.Slot.Width;
            }
        }

        /// <summary>
        /// Gets the vertical size of the scrolled content.
        /// </summary>
        public double ExtentHeight
        {
            get
            {
                AVisual lContent = this.Content as AVisual;
                return lContent.Slot.Height;
            }
        }

        /// <summary>
        /// Gets the maximum scrollable horizontal size.
        /// </summary>
		public double ScrollableWidth
        {
			get
            {
				try
                {
					return Math.Max( 0.0, this.ExtentWidth - this.ViewportWidth );
				}
                catch
                {
					return 0;
				}
			}
		}

        /// <summary>
        /// Gets the maximum scrollable vertical size.
        /// </summary>
		public double ScrollableHeight
        {
            get
            {
                try
                {
					return Math.Max( 0.0, this.ExtentHeight - this.ViewportHeight );
				}
                catch
                {
					return 0;
				}
            }
        }

        /// <summary>
        /// Gets the horizontal size of the scrolling viewport.
        /// </summary>
        public double ViewportWidth
        {
            get
            {
                return this.ClientRectangle.Width;
            }
        }

        /// <summary>
        /// Gets the vertical size of the scrolling viewport.
        /// </summary>
        public double ViewportHeight
        {
            get
            {
                return this.ClientRectangle.Height;
            }
        }

        /// <summary>
        /// Gets or sets the scrolling speed.
        /// </summary>
		public int ScrollSpeed
        {
			get
            {
                return this.mScrollSpeed;
            }
			set
            {
				this.mScrollSpeed = value;
				this.NotifyPropertyChanged();
			}
		}

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initialies a new instance of the <see cref="ScrollViewer"/> class.
        /// </summary>
        public ScrollViewer() : 
        base()
        {
            this.mHorizontalOffset = 0;
            this.mVerticalOffset = 0;
            this.mIsHorizontalScrollBarVisible  = true;
            this.mIsVerticalScrollBarVisible    = true;
            this.mHorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            this.mVerticalScrollBarVisibility   = ScrollBarVisibility.Visible;
            this.mScrollSpeed = 30;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if ( this.mHorizontalScrollBar != null )
            {
                this.mHorizontalScrollBar.Scroll -= this.OnHorizontalScroll;
            }

            this.mHorizontalScrollBar = this.GetTemplateChild( HorizontalScrollBarTemplateName ) as ScrollBar;

            if ( this.mHorizontalScrollBar != null )
            {
                this.mHorizontalScrollBar.Scroll += this.OnHorizontalScroll;
            }

            if ( this.mVerticalScrollBar != null )
            {
                this.mVerticalScrollBar.Scroll -= this.OnVerticalScroll;
            }

            this.mVerticalScrollBar = this.GetTemplateChild( VerticalScrollBarTemplateName ) as ScrollBar;

            if ( this.mVerticalScrollBar != null )
            {
                this.mVerticalScrollBar.Scroll += this.OnVerticalScroll;
            }
        }

        /// <summary>
        /// Delegate called on horizontal scrolling.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnHorizontalScroll(object pSender, ScrollingEventArgs pEventArgs)
        {
            this.HorizontalOffset = this.mHorizontalScrollBar.Value;
        }

        /// <summary>
        /// Delegate called on horizontal scrolling.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnVerticalScroll(object pSender, ScrollingEventArgs pEventArgs)
        {
            this.VerticalOffset = this.mVerticalScrollBar.Value;
        }

        /// <summary>
        /// Overridable size measure process.
        /// </summary>
        /// <returns></returns>
        protected override Size MeasureOverride()
        {
            int lCount = this.VisualChildrenCount;

            UIElement lChild = (lCount > 0) ? this.GetVisualChild( 0 ) as UIElement : null;
            ScrollBarVisibility vsbv = this.VerticalScrollBarVisibility;
            ScrollBarVisibility hsbv = this.HorizontalScrollBarVisibility;

            if ( lChild != null )
            {
                try
                {
                    bool vsbAuto = (vsbv == ScrollBarVisibility.Auto);
                    bool hsbAuto = (hsbv == ScrollBarVisibility.Auto);
                    bool vDisableScroll = (vsbv == ScrollBarVisibility.Disabled);
                    bool hDisableScroll = (hsbv == ScrollBarVisibility.Disabled);
                    bool vv = (vsbv == ScrollBarVisibility.Visible) ? true : false;
                    bool hv = (hsbv == ScrollBarVisibility.Visible) ? true : false;

                    this.IsVerticalScrollBarVisible   = vv;
                    this.IsHorizontalScrollBarVisible = hv;
                    
                    try
                    {
                        // Measure our visual tree.
                        //InChildMeasurePass1 = true;
                        lChild.Measure();
                    }
                    finally
                    {
                        //InChildMeasurePass1 = false;
                    }
                    
                    if ( hsbAuto || vsbAuto )
                    {
                        bool lMakeHorizontalBarVisible = hsbAuto && DoubleUtil.GreaterThan( this.ExtentWidth, this.ViewportWidth );
                        bool lMakeVerticalBarVisible   = vsbAuto && DoubleUtil.GreaterThan( this.ExtentHeight, this.ViewportHeight );

                        if ( lMakeHorizontalBarVisible )
                        {
                            if ( this.IsHorizontalScrollBarVisible == false )
                            {
                                this.IsHorizontalScrollBarVisible = true;
                            }
                        }

                        if ( lMakeVerticalBarVisible )
                        {
                            if ( this.IsVerticalScrollBarVisible == false )
                            {
                                this.IsVerticalScrollBarVisible = true;
                            }
                        }

                        if ( lMakeHorizontalBarVisible || 
                             lMakeVerticalBarVisible )
                        {
                            // Remeasure our visual tree.
                            // Requires this extra invalidation because we need to remeasure Grid which is not neccessarily dirty now
                            // since we only invlaidated scrollbars but we don't have LayoutUpdate loop at our disposal here
                            //InChildInvalidateMeasure = true;
                            lChild.InvalidateMeasure();

                            try
                            {
                                //InChildMeasurePass2 = true;
                                lChild.Measure();
                            }
                            finally
                            {
                                //InChildMeasurePass2 = false;
                            }
                        }

                        //if both are Auto, then appearance of one scrollbar may causes appearance of another.
                        //If we don't re-check here, we get some part of content covered by auto scrollbar and can never reach to it since
                        //another scrollbar may not appear (in cases when viewport==extent) - bug 1199443
                        if ( hsbAuto && 
                             vsbAuto && 
                             lMakeHorizontalBarVisible != lMakeVerticalBarVisible )
                        {
                            bool lMakeHorizontalBarVisible2 = !lMakeHorizontalBarVisible && DoubleUtil.GreaterThan( this.ExtentWidth, this.ViewportWidth );
                            bool lMakeVerticalBarVisible2 = !lMakeVerticalBarVisible && DoubleUtil.GreaterThan( this.ExtentHeight, this.ViewportHeight );

                            if ( lMakeHorizontalBarVisible2 )
                            {
                                if ( this.IsHorizontalScrollBarVisible == false )
                                {
                                    this.IsHorizontalScrollBarVisible = true;
                                }
                            }
                            else if ( lMakeVerticalBarVisible2 ) //only one can be true
                            {
                                if ( this.IsVerticalScrollBarVisible == false )
                                {
                                    this.IsVerticalScrollBarVisible = true;
                                }
                            }

                            if ( lMakeHorizontalBarVisible2 || 
                                 lMakeVerticalBarVisible2 )
                            {
                                // Remeasure our visual tree.
                                // Requires this extra invalidation because we need to remeasure Grid which is not neccessarily dirty now
                                // since we only invlaidated scrollbars but we don't have LayoutUpdate loop at our disposal here
                                //InChildInvalidateMeasure = true;
                                lChild.InvalidateMeasure();

                                try
                                {
                                    //InChildMeasurePass3 = true;
                                    lChild.Measure();
                                }
                                finally
                                {
                                    //InChildMeasurePass3 = false;
                                }
                            }

                        }
                    }
                }
                catch
                {

                }

                //desiredSize = child.DesiredSize;
            }

            return base.MeasureOverride();
        }

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected internal override void OnLayoutChanges(LayoutingType pLayoutType)
		{
			base.OnLayoutChanges( pLayoutType );

			this.NotifyPropertyChanged( "ScrollableWidth" );
			this.NotifyPropertyChanged( "ScrollableHeight" );
        }

        /// <summary>
        /// Delegate called on child layout changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnChildLayoutChanges(object pSender, LayoutingEventArgs pEventArgs)
		{
            double lMaxScrollY = this.ScrollableHeight;
            if ( this.IsVerticalScrollBarVisible )
            {
				if ( pEventArgs.LayoutType == LayoutingType.Height )
                {
					if ( lMaxScrollY < this.VerticalOffset )
                    {
						this.VerticalOffset = lMaxScrollY;
					}

					this.NotifyPropertyChanged( "ScrollableHeight" );
				}
			}

            double lMaxScrollX = this.ScrollableWidth;
            if ( this.IsHorizontalScrollBarVisible )
            {
                if ( pEventArgs.LayoutType == LayoutingType.Width )
                {
				    if ( lMaxScrollX < this.HorizontalOffset )
                    {
                        this.HorizontalOffset = lMaxScrollX;
				    }
				
                    this.NotifyPropertyChanged( "ScrollableWidth" );
			    }
            }
		}

        /// <summary>
        /// Delegate called on scroll child a ItemsControl cleared.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
		private void OnPanelCleared(object pSender, EventArgs pEventArgs)
        {
			this.VerticalOffset   = 0;
			this.HorizontalOffset = 0;
		}

        /// <summary>
        /// Delegate called on content changes.
        /// </summary>
        /// <param name="pOldContent">The old one.</param>
        /// <param name="pNewContent">The new one.</param>
        protected override void OnContentChanged(object pOldContent, object pNewContent)
        {
            UIElement lOldContent = pOldContent as UIElement;
            APanel lOldPanel = pOldContent as APanel;
            if ( lOldContent != null )
            {
                lOldContent.LayoutUpdated -= this.OnChildLayoutChanges;
                if ( lOldPanel != null )
                {
                    lOldPanel.ChildrenCleared -= this.OnPanelCleared;
                }
            }

            UIElement lNewContent = pNewContent as UIElement;
            APanel lNewPanel = pNewContent as APanel;
            if ( lNewContent != null )
            {
                lNewContent.LayoutUpdated += this.OnChildLayoutChanges;
                if ( lNewPanel != null )
                {
                    lNewPanel.ChildrenCleared += this.OnPanelCleared;
                }
            }

            base.OnContentChanged( pOldContent, pNewContent );
        }

        /// <summary>
        /// Gets the screen coordinates for the given region.
        /// </summary>
        /// <param name="pRegion"></param>
        /// <returns></returns>
        public override Rectangle ScreenCoordinates(Rectangle pRegion)
		{
			return base.ScreenCoordinates( pRegion ) - new Point( (int)this.HorizontalOffset, (int)this.VerticalOffset );
		}

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            Rectangle lBackground = new Rectangle( this.mSlot.Size );

            this.Background.SetAsSource( pDrawingContext, lBackground );

            CairoHelpers.CairoRectangle( pDrawingContext, lBackground, -1 );

            pDrawingContext.Fill();

            pDrawingContext.Save();

            if ( this.ClipToClientRect )
            {
                // Clip to client zone
                CairoHelpers.CairoRectangle( pDrawingContext, this.ClientRectangle, -1 );
                pDrawingContext.Clip();
            }

            pDrawingContext.Translate( -this.HorizontalOffset, -this.VerticalOffset );

            UIElement lChildAsElement = this.Content as UIElement;
            if ( lChildAsElement != null )
            {
                if ( lChildAsElement.IsVisible )
                {
                    lChildAsElement.Paint( ref pDrawingContext );
                }
            }

            pDrawingContext.Restore();
        }

        /// <summary>
        /// Internal method allowing specific check to know whether the given point is inside this element or not.
        /// </summary>
        /// <param name="pScreenPoint">The point to check.</param>
        /// <returns>True if contained, false otherwise.</returns>
        protected override bool InternalIsMouseIn(Point pScreenPoint)
        {
            return true;
        }

        /// <summary>
        /// Delegate called on mouse hover.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseHover(MouseMoveEventArgs pEventArgs)
        {
            this.mSavedMousePos = pEventArgs.Position;
            Point lPoint = pEventArgs.Position - new Point( (int)this.HorizontalOffset, (int)this.VerticalOffset );

            base.OnMouseHover( new MouseMoveEventArgs( lPoint.X, lPoint.Y, pEventArgs.XDelta, pEventArgs.YDelta ) );
        }

        /// <summary>
        /// Delegate called on mouse wheel.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseWheel(object pSender, MouseWheelEventArgs pEventArgs)
        {
            if (this.Content == null)
            {
                return;
            }

            if ( pEventArgs.Delta < 0 )
            {
                MouseWheelDown();
            }
            else
            {
                MouseWheelUp();
            }
        }
        
        /// <summary>
        /// Delegate called on UIelement mouse moves.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseMove(object pSender, MouseMoveEventArgs pEventArgs)
        {
            this.mSavedMousePos.X += pEventArgs.XDelta;
            this.mSavedMousePos.Y += pEventArgs.YDelta;

            base.OnMouseMove( pSender, new MouseMoveEventArgs( this.mSavedMousePos.X, this.mSavedMousePos.Y, pEventArgs.XDelta, pEventArgs.YDelta ) );
        }

        /// <summary>
        /// Registers a new region to clip.
        /// </summary>
        /// <param name="pClip">The new clip region.</param>
		public override void RegisterClip(Rectangle pClip)
        {
            base.RegisterClip( pClip - new Point( (int)this.HorizontalOffset, (int)this.VerticalOffset ) );
        }

        /// <summary>
        /// Delegate called on key pressed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal override void OnKeyDown(object pSender, KeyboardKeyEventArgs pEventArgs)
        {
            this.ScrollInDirection( pEventArgs );

            base.OnKeyDown( pSender, pEventArgs );
        }

        /// <summary>
        /// Scroll in the Keyboard key corresponding direction
        /// </summary>
        /// <param name="pEventArgs"></param>
        internal void ScrollInDirection(KeyboardKeyEventArgs pEventArgs)
        {
            bool lHasControlDown = (pEventArgs.Modifiers & KeyModifiers.Control) != 0;
            bool lHasAltDown = (pEventArgs.Modifiers & KeyModifiers.Alt) != 0;

            // Alt + Key is not handled
            if ( lHasAltDown == false )
            {
                switch ( pEventArgs.Key )
                {
                    case Key.Left:
                        LineLeft();
                        break;
                    case Key.Right:
                        LineRight();
                        break;
                    case Key.Up:
                        LineUp();
                        break;
                    case Key.Down:
                        LineDown();
                        break;
                    //case Key.PageUp:
                    //    PageUp();
                    //    break;
                    //case Key.PageDown:
                    //    PageDown();
                    //    break;
                    case Key.Home:
                        if ( lHasControlDown )
                        {
                            ScrollToTop();
                        }
                        else
                        {
                            ScrollToLeftEnd();
                        }
                        break;
                    case Key.End:
                        if ( lHasControlDown )
                        {
                            ScrollToBottom();
                        }
                        else
                        {
                            ScrollToRightEnd();
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Delegate called on scroll changes.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected virtual void OnScrollChanged(ScrollChangedEventArgs pEventArgs)
        {
            this.NotifyScrollChanged( pEventArgs );
        }

        /// <summary>
        /// Notifies the scroll position has changed.
        /// </summary>
        /// <param name="pEventArgs"></param>
        private void NotifyScrollChanged(ScrollChangedEventArgs pEventArgs)
        {
            if ( this.ScrollChanged != null )
            {
                this.ScrollChanged( this, pEventArgs );
            }
        }

        /// <summary>
        /// Scroll to the left.
        /// </summary>
        public void LineLeft()
        {
            this.mHorizontalScrollBar.Value -= this.ScrollSpeed;
        }

        /// <summary>
        /// Scroll to the right.
        /// </summary>
        public void LineRight()
        {
            this.mHorizontalScrollBar.Value += this.ScrollSpeed;
        }

        /// <summary>
        /// Scroll up.
        /// </summary>
        public void LineUp()
        {
            this.mVerticalScrollBar.Value -= this.ScrollSpeed;
        }

        /// <summary>
        /// Scroll down.
        /// </summary>
        public void LineDown()
        {
            this.mVerticalScrollBar.Value += this.ScrollSpeed;
        }

        /// <summary>
        /// Scrolls straight to the top.
        /// </summary>
        public void ScrollToTop()
        {
            this.mVerticalScrollBar.Value = 0;
        }

        /// <summary>
        /// Scrolls straight to the bottom.
        /// </summary>
        public void ScrollToBottom()
        {
            this.mVerticalScrollBar.Value = this.ScrollableHeight;
        }

        /// <summary>
        /// Scrolls straight to the left end.
        /// </summary>
        public void ScrollToLeftEnd()
        {
            this.mHorizontalScrollBar.Value = 0;
        }

        /// <summary>
        /// Scrolls straight to the right end.
        /// </summary>
        public void ScrollToRightEnd()
        {
            this.mHorizontalScrollBar.Value = this.ScrollableWidth;
        }

        /// <summary>
        /// Scroll down on wheel down
        /// </summary>
        public void MouseWheelDown()
        {
            this.mVerticalScrollBar.Value += this.ScrollSpeed;
        }

        /// <summary>
        /// Scroll up on wheel up
        /// </summary>
        public void MouseWheelUp()
        {
            this.mVerticalScrollBar.Value -= this.ScrollSpeed;
        }

        #endregion Methods
    }
}
