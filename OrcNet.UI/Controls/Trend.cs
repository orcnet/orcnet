﻿using System.Collections.Generic;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Trend"/> class.
    /// </summary>
	public class Trend : Control
    {
        #region Fields

        /// <summary>
        /// Stores the minimum value.
        /// </summary>
        private double mMinimum;

        /// <summary>
        /// Stores the maximum value.
        /// </summary>
        private double mMaximum;

        /// <summary>
        /// Stores the low threshold
        /// </summary>
        private double mLowThreshold;

        /// <summary>
        /// Stores the high threshold
        /// </summary>
        private double mHighThreshold;
        
        /// <summary>
        /// Stores the low threshold brush
        /// </summary>
        private ABrush mLowThresholdBrush;

        /// <summary>
        /// Stores the high threshold brush
        /// </summary>
        private ABrush mHighThresholdBrush;

        /// <summary>
        /// Stores the maximum value count.
        /// </summary>
		private int mMaxValueCount;

        /// <summary>
        /// Stores the set of values.
        /// </summary>
		private List<double> mValues;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the maximum value count.
        /// </summary>
        public virtual int MaxValueCount
        {
            get
            {
                return this.mMaxValueCount;
            }
            set
            {
                if ( this.mMaxValueCount == value )
                {
                    return;
                }

                this.mMaxValueCount = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the minimum value.
        /// </summary>
        public virtual double Minimum
        {
            get
            {
                return this.mMinimum;
            }
            set
            {
                if ( this.mMinimum == value )
                {
                    return;
                }

                this.mMinimum = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }
        
        /// <summary>
        /// Gets or sets the maximum value.
        /// </summary>
        public virtual double Maximum
        {
            get
            {
                return this.mMaximum;
            }
            set
            {
                if ( this.mMaximum == value )
                {
                    return;
                }

                this.mMaximum = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the low threshold
        /// </summary>
        public virtual double LowThreshold
        {
            get
            {
                return this.mLowThreshold;
            }
            set
            {
                if ( this.mLowThreshold == value )
                {
                    return;
                }

                this.mLowThreshold = value;
                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }

        /// <summary>
        /// Gets or sets the high threshold
        /// </summary>
        public virtual double HighThreshold
        {
            get
            {
                return this.mHighThreshold;
            }
            set
            {
                if ( this.mHighThreshold == value )
                {
                    return;
                }

                this.mHighThreshold = value;
                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }
        
        /// <summary>
        /// Gets or sets the low threshold brush
        /// </summary>
        public virtual ABrush LowThresholdFill
        {
            get
            {
                return this.mLowThresholdBrush;
            }
            set
            {
                if ( this.mLowThresholdBrush == value )
                {
                    return;
                }

                this.mLowThresholdBrush = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the high threshold brush
        /// </summary>
        public virtual ABrush HighThresholdFill
        {
            get
            {
                return this.mHighThresholdBrush;
            }
            set
            {
                if ( this.mHighThresholdBrush == value )
                {
                    return;
                }

                this.mHighThresholdBrush = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Trend"/> class.
        /// </summary>
        public Trend()
        {
            this.mMinimum = 0.0;
            this.mMaximum = 100.0;
            this.mLowThreshold = 20.0;
            this.mHighThreshold = 80.0;
            this.mMaxValueCount = 100;
            this.mValues = new List<double>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new value to the trend.
        /// </summary>
        /// <param name="pNewValue">The new value.</param>
        public virtual void AddValue(double pNewValue)
		{
            this.mValues.Add( pNewValue );

            // Removes the oldest values if exceeding the maximum allowed count.
            while ( this.mValues.Count > this.mMaxValueCount )
            {
                this.mValues.RemoveAt( 0 );
            }

            this.InvalidateVisual();
		}

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            if ( this.mValues.Count == 0 )
            {
                return;
            }

            Rectangle lRegion = this.ClientRectangle;

            int lCurr = this.mValues.Count - 1;

            double lCurrX = (double)lRegion.Right;
            double lHeightScale = (double)lRegion.Height / (this.Maximum - this.Minimum);
            double lStepX = (double)lRegion.Width / (double)(this.mMaxValueCount - 1);

            pDrawingContext.LineWidth = 1.0;
            pDrawingContext.SetDash( new double[] { 1.0 }, 0.0 );



            this.LowThresholdFill.SetAsSource( pDrawingContext );
            pDrawingContext.MoveTo( lRegion.Left,  lRegion.Bottom - this.LowThreshold * lHeightScale );
            pDrawingContext.LineTo( lRegion.Right, lRegion.Bottom - this.LowThreshold * lHeightScale );

            pDrawingContext.Stroke();

            this.HighThresholdFill.SetAsSource( pDrawingContext );
            pDrawingContext.MoveTo( lRegion.Left,  (this.Maximum - this.HighThreshold ) * lHeightScale );
            pDrawingContext.LineTo( lRegion.Right, (this.Maximum - this.HighThreshold ) * lHeightScale );

            pDrawingContext.Stroke();

            pDrawingContext.MoveTo( lCurrX, this.mValues[lCurr] * lHeightScale );

            Foreground.SetAsSource( pDrawingContext );
            pDrawingContext.SetDash( new double[] { }, 0.0 );

            while ( lCurr >= 0 )
            {
                pDrawingContext.LineTo( lCurrX, lRegion.Bottom - this.mValues[lCurr] * lHeightScale );
                lCurrX -= lStepX;
                lCurr--;
            }

            pDrawingContext.Stroke();
        }
        
        #endregion Methods
    }
}

