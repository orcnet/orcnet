﻿using System;
using OrcNet.UI.Input;
using OrcNet.UI.Ximl;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TabItem"/> class.
    /// </summary>
	public class TabItem : HeaderedContentControl
	{
        #region Fields

        /// <summary>
        /// Stores the tab item focus state.
        /// </summary>
        private FocusState mTabItemFocusState = FocusState.DefaultValue;

        /// <summary>
        /// Stores the flag indicating whether the item is selected or not.
        /// </summary>
        private bool mIsSelected;

        /// <summary>
        /// Stores the flag indicating whether the user is holding the cursor on the item or not.
        /// </summary>
        private bool mIsHoldingItem = false;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the item is selected or not.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.mIsSelected;
            }
            set
            {
                if ( this.mIsSelected == value )
                {
                    return;
                }

                this.mIsSelected = value;
                this.NotifyPropertyChanged();

                if ( this.mIsSelected )
                {
                    this.OnSelected( null );
                }
                else
                {
                    this.OnUnselected( null );
                }
            }
        }

        /// <summary>
        /// Gets the Tab item strip placement.
        /// </summary>
        public Dock TabStripPlacement
        {
            get
            {
                TabControl lControl = this.TabControlParent;
                return lControl != null ? lControl.TabStripPlacement : Dock.Top;
            }
        }

        /// <summary>
        /// Gets the parent Tab Control.
        /// </summary>
        private TabControl TabControlParent
        {
            get
            {
                return ItemsControl.ItemsControlFromItemContainer( this ) as TabControl;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TabItem"/> class.
        /// </summary>
        public TabItem() : 
        base()
        {
            this.mIsSelected = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Sets the focus on this tab item.
        /// </summary>
        /// <returns></returns>
        internal bool SetFocus()
        {
            bool lReturnValue = false;

            if ( this.HasFlag( FocusState.SettingFocus ) == false )
            {
                TabItem lCurrentFocus = InputManager.Instance.FocusElement as TabItem;

                // If current focus was another TabItem in the same TabControl - dont set focus on content
                bool lSetFocusOnContent = lCurrentFocus == this || lCurrentFocus == null || lCurrentFocus.TabControlParent != this.TabControlParent;
                this.SetFocusState( FocusState.SettingFocus, true );
                this.SetFocusState( FocusState.SetFocusOnContent, lSetFocusOnContent );
                try
                {
                    lReturnValue = this.Focus() || lSetFocusOnContent;
                }
                finally
                {
                    this.SetFocusState( FocusState.SettingFocus, false );
                    this.SetFocusState( FocusState.SetFocusOnContent, false );
                }
            }

            return lReturnValue;
        }

        /// <summary>
        /// Delegate called on element got focus event.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnGotFocus(object pSender, EventArgs pEventArgs)
        {
            base.OnGotFocus( pSender, pEventArgs );

            if ( this.IsSelected == false &&
                 this.TabControlParent != null )
            {
                this.IsSelected = true;
            }
        }

        /// <summary>
        /// Delegate called on content changes.
        /// </summary>
        /// <param name="pOldContent">The old one.</param>
        /// <param name="pNewContent">The new one.</param>
        protected override void OnContentChanged(object pOldContent, object pNewContent)
        {
            base.OnContentChanged( pOldContent, pNewContent );

            if ( this.IsSelected )
            {
                TabControl lTabControl = this.TabControlParent;
                if ( lTabControl != null )
                {
                    lTabControl.SelectedContent = pNewContent;
                }
            }
        }

        /// <summary>
        /// Delegate called on content template changes.
        /// </summary>
        /// <param name="pOldContent">The old one.</param>
        /// <param name="pNewContent">The new one.</param>
        protected override void OnContentTemplateChanged(DataTemplate pOldContent, DataTemplate pNewContent)
        {
            base.OnContentTemplateChanged( pOldContent, pNewContent );

            if ( this.IsSelected )
            {
                TabControl lTabControl = this.TabControlParent;
                if ( lTabControl != null )
                {
                    lTabControl.SelectedContentTemplate = pNewContent;
                }
            }
        }

        /// <summary>
        /// Delegate called on content template changes.
        /// </summary>
        /// <param name="pOldContent">The old one.</param>
        /// <param name="pNewContent">The new one.</param>
        protected override void OnContentTemplateSelectorChanged(DataTemplateSelector pOldContent, DataTemplateSelector pNewContent)
        {
            base.OnContentTemplateSelectorChanged( pOldContent, pNewContent );

            if ( this.IsSelected )
            {
                TabControl lTabControl = this.TabControlParent;
                if ( lTabControl != null )
                {
                    lTabControl.SelectedContentTemplateSelector = pNewContent;
                }
            }
        }

        /// <summary>
        /// Delegate called on UIelement mouse down.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseDown(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( this.IsSelected == false )
            {
                this.SetFocus();
            }

            this.mIsHoldingItem = true;

            base.OnMouseDown( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on UIelement mouse moves.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseMove(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseMove( pSender, pEventArgs );

            if ( (this.IsFocused && this.mIsHoldingItem) == false )
            {
                return;
            }

            // TO DO: Create the Tab moving process over rows...
            //TabControl lParent = this.TabControlParent;
            //TabItem lPrevious = null;
            //TabItem lNext = null;
            //int tmp = TabOffset + pEventArgs.XDelta;
            //if (tmp < lParent.Spacing)
            //{
            //    TabOffset = lParent.Spacing;
            //}
            //else if (tmp > Parent.getSlot().Width - TabTitle.Slot.Width - lParent.Spacing)
            //{
            //    TabOffset = Parent.getSlot().Width - TabTitle.Slot.Width - lParent.Spacing;
            //}
            //else
            //{
            //    int idx = lParent.Items.IndexOf(this);
            //    if (idx > 0 && pEventArgs.XDelta < 0)
            //    {
            //        lPrevious = lParent.Items[idx - 1] as TabItem;

            //        if (tmp < lPrevious.TabOffset + lPrevious.TabTitle.Slot.Width / 2)
            //        {
            //            lParent.Items.RemoveAt(idx);
            //            lParent.Items.Insert(idx - 1, this);
            //            lParent.SelectedTab = idx - 1;
            //            lParent.UpdateLayout(LayoutingType.ArrangeChildren);
            //        }

            //    }
            //    else if (idx < lParent.Items.Count - 1 && pEventArgs.XDelta > 0)
            //    {
            //        lNext = lParent.Items[idx + 1] as TabItem;
            //        if (tmp > lNext.TabOffset - lNext.TabTitle.Slot.Width / 2)
            //        {
            //            lParent.Items.RemoveAt(idx);
            //            lParent.Items.Insert(idx + 1, this);
            //            lParent.SelectedTab = idx + 1;
            //            lParent.UpdateLayout(LayoutingType.ArrangeChildren);
            //        }
            //    }
            //    TabOffset = tmp;
            //}
        }

        /// <summary>
        /// Delegate called on mouse up.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseUp(object pSender, MouseButtonEventArgs pEventArgs)
        {
            this.mIsHoldingItem = false;

            TabControl lParent = this.TabControlParent;
            if ( lParent != null )
            {
                lParent.Arrange( LayoutingType.ArrangeChildren );
            }

            base.OnMouseUp( pSender, pEventArgs );
        }

        /// <summary>
        /// Internal method allowing specific check to know whether the given point is inside this element or not.
        /// </summary>
        /// <param name="pScreenPoint">The point to check.</param>
        /// <returns>True if contained, false otherwise.</returns>
        protected override bool InternalIsMouseIn(Point pScreenPoint)
        {
            if ( this.IsVisible == false )
            {
                return false;
            }

            bool lIsMouseInTitle = false;
            AVisual lHeader = this.Header as AVisual;
            if ( lHeader != null )
            {
                lIsMouseInTitle = lHeader.ScreenCoordinates( lHeader.Slot ).ContainsOrIsEqual( pScreenPoint );
            }

            if ( this.IsSelected == false )
            {
                return lIsMouseInTitle;
            }

            AVisual lContent = this.Content as AVisual;
            if ( lContent != null )
            {
                return lContent.ScreenCoordinates( lContent.Slot ).ContainsOrIsEqual( pScreenPoint ) ||
                       lIsMouseInTitle;
            }

            return lIsMouseInTitle;
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            pDrawingContext.Save();

            AVisual lHeader = this.Header as AVisual;

            pDrawingContext.MoveTo( 0.5, 
                                    lHeader.Slot.Bottom - 0.5 );
            pDrawingContext.LineTo( lHeader.Slot.Left,
                                    lHeader.Slot.Bottom - 0.5 );
            pDrawingContext.CurveTo( lHeader.Slot.Left, 
                                     lHeader.Slot.Bottom - 0.5,
                                     lHeader.Slot.Left, 
                                     0.5,
                                     lHeader.Slot.Left, 
                                     0.5 );
            pDrawingContext.LineTo( lHeader.Slot.Right, 
                                    0.5 );
            pDrawingContext.CurveTo( lHeader.Slot.Right, 
                                     0.5,
                                     lHeader.Slot.Right, 
                                     lHeader.Slot.Bottom - 0.5,
                                     lHeader.Slot.Right,
                                     lHeader.Slot.Bottom - 0.5 );
            pDrawingContext.LineTo( this.Slot.Width - 0.5,
                                    lHeader.Slot.Bottom - 0.5 );


            pDrawingContext.LineTo( this.Slot.Width - 0.5, 
                                    this.Slot.Height - 0.5 );
            pDrawingContext.LineTo( 0.5, 
                                    this.Slot.Height - 0.5 );
            pDrawingContext.ClosePath();
            pDrawingContext.LineWidth = 2;
            this.Foreground.SetAsSource( pDrawingContext );
            pDrawingContext.StrokePreserve();

            pDrawingContext.Clip();
            
            base.OnRender( pDrawingContext );

            pDrawingContext.Restore();
        }

        /// <summary>
        /// Delegate called on item selected.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected virtual void OnSelected(EventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called on item unselected.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected virtual void OnUnselected(EventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Checks whether the item has the given focus state or not.
        /// </summary>
        /// <param name="pState"></param>
        /// <returns>True if has the flag, false otherwise.</returns>
        private bool HasFlag(FocusState pState)
        {
            return (this.mTabItemFocusState & pState) != 0;
        }

        /// <summary>
        /// Modify the focus state.
        /// </summary>
        /// <param name="pState"></param>
        /// <param name="pValue"></param>
        private void SetFocusState(FocusState pState, bool pValue)
        {
            if ( pValue )
            {
                this.mTabItemFocusState |= pState;
            }
            else
            {
                this.mTabItemFocusState &= ~pState;
            }
        }

        #endregion Methods
        
        #region Inner Classes
        
        /// <summary>
        /// Declaration of the <see cref="FocusState"/> class.
        /// </summary>
        [Flags]
        private enum FocusState
        {
            /// <summary>
            /// Doing Nothing.
            /// </summary>
            DefaultValue = 0,

            /// <summary>
            /// Determine whether the user wants to set focus on active TabItem content or not.
            /// </summary>
            SetFocusOnContent = 0x10,

            /// <summary>
            /// Indicates whether the TabItem is in the process of setting focus or not.
            /// </summary>
            SettingFocus = 0x20,
        }
        
        #endregion Inner Classes
    }
}

