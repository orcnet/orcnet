﻿using OrcNet.UI.Collections;
using OrcNet.UI.Helpers;
using System;
using System.Collections.Generic;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="DockPanel"/> class.
    /// </summary>
    public class DockPanel : APanel
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the last child must be stretch and fill the remaining space or not.
        /// </summary>
        private bool mStretchLastChild;

        /// <summary>
        /// Stores the panels info by child element.
        /// </summary>
        private Dictionary<UIElement, DockInfo> mPanelsInfo;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the last child must be stretch and fill the remaining space or not.
        /// </summary>
        public bool StretchLastChild
        {
            get
            {
                return this.mStretchLastChild;
            }
            set
            {
                if ( this.mStretchLastChild == value )
                {
                    return;
                }

                this.mStretchLastChild = value;

                this.NotifyPropertyChanged();
                this.InvalidateArrange();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DockPanel"/> class.
        /// </summary>
        public DockPanel()
        {
            this.mPanelsInfo = new Dictionary<UIElement, DockInfo>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the given element dock position.
        /// </summary>
        /// <param name="pElement">The element the dock position must be get for.</param>
        /// <returns>The dock position.</returns>
        public Dock GetDock(UIElement pElement)
        {
            if ( pElement == null )
            {
                throw new ArgumentNullException("pElement");
            }

            DockInfo lInfo;
            if ( this.mPanelsInfo.TryGetValue( pElement, out lInfo ) )
            {
                return lInfo.Placement;
            }

            return Dock.Bottom;
        }

        /// <summary>
        /// Sets the given element the supplied dock position.
        /// </summary>
        /// <param name="pElement">The element the dock position must be set for.</param>
        /// <param name="pDock">The element dock position.</param>
        public void SetDock(UIElement pElement, Dock pDock)
        {
            if ( pElement == null )
            {
                throw new ArgumentNullException("pElement");
            }
            
            this.mPanelsInfo[ pElement ] = new DockInfo() { Placement = pDock };

            DockPanel lParent = VisualTreeHelper.GetParent( pElement ) as DockPanel;
            if ( lParent != null )
            {
                lParent.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            UIElementCollection lChildren = this.InternalChildren;

            double lParentWidth = 0;   // Our current required width due to lChildren thus far.
            double lParentHeight = 0;   // Our current required height due to lChildren thus far.
            double lAccumulatedWidth = 0;   // Total width consumed by lChildren.
            double lAccumulatedHeight = 0;   // Total height consumed by lChildren.

            for ( int lCurr = 0, lCount = lChildren.Count; lCurr < lCount; ++lCurr )
            {
                UIElement lChild = lChildren[ lCurr ];
                Size lChildDesiredSize; // Contains the return size from the child measure.

                if ( lChild == null ||
                     lChild.IsVisible == false )
                {
                    continue;
                }
                
                // Measure the child.
                lChildDesiredSize = lChild.Measure();
                
                switch ( this.GetDock( lChild ) )
                {
                    case Dock.Left:
                    case Dock.Right:
                        lParentHeight = Math.Max( lParentHeight, lAccumulatedHeight + lChildDesiredSize.Height );
                        lAccumulatedWidth += lChildDesiredSize.Width;
                        break;

                    case Dock.Top:
                    case Dock.Bottom:
                        lParentWidth = Math.Max( lParentWidth, lAccumulatedWidth + lChildDesiredSize.Width );
                        lAccumulatedHeight += lChildDesiredSize.Height;
                        break;
                }
            }

            // Make sure the final accumulated size is reflected in parentSize.
            lParentWidth  = Math.Max( lParentWidth, lAccumulatedWidth );
            lParentHeight = Math.Max( lParentHeight, lAccumulatedHeight );

            this.mContentSize = new Size( (int)lParentWidth, (int)lParentHeight );

            return base.MeasureOverride();
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            UIElementCollection lChildren = this.InternalChildren;
            int lTotalChildCount = lChildren.Count;
            int lNonFillChildrenCount = lTotalChildCount - (this.StretchLastChild ? 1 : 0);

            double lAccumulatedLeft = 0;
            double lAccumulatedTop = 0;
            double lAccumulatedRight = 0;
            double lAccumulatedBottom = 0;

            for ( int lCurr = 0; lCurr < lTotalChildCount; ++lCurr )
            {
                UIElement lChild = lChildren[ lCurr ];
                if ( lChild == null )
                {
                    continue;
                }

                Size lChildDesiredSize = lChild.DesiredSize;
                Rectangle lChildRegion = new Rectangle( (int)lAccumulatedLeft,
                                                        (int)lAccumulatedTop,
                                                        (int)Math.Max( 0.0, this.Slot.Width - (lAccumulatedLeft + lAccumulatedRight)),
                                                        (int)Math.Max( 0.0, this.Slot.Height - (lAccumulatedTop + lAccumulatedBottom)));

                if ( lCurr < lNonFillChildrenCount )
                {
                    switch ( this.GetDock( lChild ) )
                    {
                        case Dock.Left:
                            lAccumulatedLeft += lChildDesiredSize.Width;
                            lChildRegion.Width = lChildDesiredSize.Width;
                            break;

                        case Dock.Right:
                            lAccumulatedRight += lChildDesiredSize.Width;
                            lChildRegion.X = (int)Math.Max( 0.0, this.Slot.Width - lAccumulatedRight );
                            lChildRegion.Width = lChildDesiredSize.Width;
                            break;

                        case Dock.Top:
                            lAccumulatedTop += lChildDesiredSize.Height;
                            lChildRegion.Height = lChildDesiredSize.Height;
                            break;

                        case Dock.Bottom:
                            lAccumulatedBottom += lChildDesiredSize.Height;
                            lChildRegion.Y = (int)Math.Max( 0.0, this.Slot.Height - lAccumulatedBottom );
                            lChildRegion.Height = lChildDesiredSize.Height;
                            break;
                    }
                }

                lChild.Slot = lChildRegion;

                lChild.Arrange( pLayoutType );
            }

            return base.ArrangeCore( pLayoutType );
        }

        #endregion Methods

        #region Inner Classes

        /// <summary>
        /// Definition of the <see cref="DockInfo"/> class.
        /// </summary>
        private class DockInfo
        {
            #region Fields

            /// <summary>
            /// Stores the docking emplacement.
            /// </summary>
            private Dock mPlacement;

            #endregion Fields

            #region Properties

            /// <summary>
            /// Gets or sets the docking emplacement.
            /// </summary>
            public Dock Placement
            {
                get
                {
                    return this.mPlacement;
                }
                set
                {
                    this.mPlacement = value;
                }
            }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="DockInfo"/> class.
            /// </summary>
            public DockInfo()
            {
                this.mPlacement = Dock.Bottom;
            }

            #endregion Constructor
        }

        #endregion Inner Classes
    }
}
