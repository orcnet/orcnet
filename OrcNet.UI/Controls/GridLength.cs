﻿using System;
using System.Globalization;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="GridLength"/> class.
    /// </summary>
    public struct GridLength : IEquatable<GridLength>
    {
        #region Fields

        /// <summary>
        /// Stores the length type
        /// </summary>
        private GridUnitType mUnitType;

        /// <summary>
        /// Stores the length value.
        /// </summary>
        private double mValue;

        /// <summary>
        /// Stores the static Auto grid length.
        /// </summary>
        private static readonly GridLength sAuto = new GridLength(1.0, GridUnitType.Auto);

        #endregion Fields

        #region Properties 

        /// <summary>
        /// Gets an initialized Auto GridLength value. 
        /// </summary>
        public static GridLength Auto
        {
            get
            {
                return sAuto;
            }
        }

        /// <summary> 
        /// Returns <c>true</c> if this GridLength instance holds
        /// an absolute (pixel) value. 
        /// </summary>
        public bool IsAbsolute { get { return (mUnitType == GridUnitType.Pixel); } }

        /// <summary> 
        /// Returns <c>true</c> if this GridLength instance is
        /// automatic (not specified). 
        /// </summary> 
        public bool IsAuto { get { return (mUnitType == GridUnitType.Auto); } }

        /// <summary>
        /// Returns <c>true</c> if this GridLength instance holds weighted propertion
        /// of available space.
        /// </summary> 
        public bool IsStar { get { return (mUnitType == GridUnitType.Star); } }

        /// <summary> 
        /// Returns value part of this GridLength instance.
        /// </summary> 
        public double Value { get { return ((mUnitType == GridUnitType.Auto) ? 1.0 : mValue); } }

        /// <summary>
        /// Returns unit type of this GridLength instance. 
        /// </summary>
        public GridUnitType GridUnitType { get { return (mUnitType); } }

        #endregion Properties 

        #region Constructors

        /// <summary> 
        /// Constructor, initializes the GridLength as absolute value in pixels.
        /// </summary> 
        /// <param name="pPixels">Specifies the number of 'device-independent pixels'</param>
        public GridLength(double pPixels) : 
        this( pPixels, GridUnitType.Pixel )
        {

        }

        /// <summary> 
        /// Constructor, initializes the GridLength and specifies what kind of value
        /// it will hold. 
        /// </summary> 
        /// <param name="pValue">Value to be stored by this GridLength instance.</param>
        /// <param name="pType">Type of the value to be stored by this GridLength instance.</param>
        /// <remarks>If the type parameter is Auto, then passed in value is ignored and replaced with 0</remarks>
        public GridLength(double pValue, GridUnitType pType)
        {
            if ( double.IsNaN( pValue ) )
            {
                throw new ArgumentException( "Nan value" );
            }
            if ( double.IsInfinity( pValue ) )
            {
                throw new ArgumentException( "Infinite value" );
            }
            
            this.mValue    = (pType == GridUnitType.Auto) ? 0.0 : pValue;
            this.mUnitType = pType;
        }

        #endregion Constructors 
        
        #region Methods

        /// <summary> 
        /// Overloaded operator, compares 2 GridLength's.
        /// </summary> 
        /// <param name="pLength1">first GridLength to compare.</param>
        /// <param name="pLength2">second GridLength to compare.</param>
        /// <returns>true if specified GridLengths have same value and unit type.</returns> 
        public static bool operator ==(GridLength pLength1, GridLength pLength2)
        {
            return (pLength1.GridUnitType == pLength2.GridUnitType && 
                    pLength1.Value == pLength2.Value);
        }

        /// <summary>
        /// Overloaded operator, compares 2 GridLength's.
        /// </summary> 
        /// <param name="pLength1">first GridLength to compare.</param>
        /// <param name="pLength2">second GridLength to compare.</param>
        /// <returns>true if specified GridLengths have either different value or unit type.</returns>
        public static bool operator !=(GridLength pLength1, GridLength pLength2)
        {
            return (pLength1.GridUnitType != pLength2.GridUnitType || 
                    pLength1.Value != pLength2.Value);
        }

        /// <summary> 
        /// Compares this instance of GridLength with another object. 
        /// </summary>
        /// <param name="pOther">Reference to an object for comparison.</param>
        /// <returns><c>true</c>if this GridLength instance has the same value
        /// and unit type as pOther.</returns>
        override public bool Equals(object pOther)
        {
            if (pOther is GridLength)
            {
                GridLength l = (GridLength)pOther;
                return (this == l);
            }

            return false;
        }

        /// <summary>
        /// Compares this instance of GridLength with another instance. 
        /// </summary> 
        /// <param name="pLength">Grid length instance to compare.</param>
        /// <returns><c>true</c>if this GridLength instance has the same value and unit type as gridLength.</returns>
        public bool Equals(GridLength pLength)
        {
            return (this == pLength);
        }

        /// <summary>
        /// Gets the grid length hashcode.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return ((int)mValue + (int)mUnitType);
        }
        
        /// <summary>
        /// Returns the string representation of this object. 
        /// </summary>
        public override string ToString()
        {
            switch ( this.mUnitType )
            {
                //  for Auto print out "Auto". value is always "1.0"
                case (GridUnitType.Auto):
                    {
                        return ("Auto");
                    }
                //  Star has one special case when value is "1.0". 
                //  in this case drop value part and print only "Star"
                case (GridUnitType.Star):
                    {
                        return (this.mValue == 1.0 ? "*" : this.mValue.ToString(CultureInfo.InvariantCulture) + "*");
                    }

                //  for Pixel print out the numeric value. "px" can be omitted.
                default:
                    {
                        return this.mValue.ToString( CultureInfo.InvariantCulture );
                    }
            }
        }

        #endregion Methods
    }
}
