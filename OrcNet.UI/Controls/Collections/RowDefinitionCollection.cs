﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Collections
{
    /// <summary>
    /// Definition of the <see cref="RowDefinitionCollection"/> class.
    /// </summary>
    public class RowDefinitionCollection : ICollection<RowDefinition>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the set of rows.
        /// </summary>
        private List<RowDefinition> mRows;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the row count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mRows.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is readonly or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets or sets the row having the given index.
        /// </summary>
        /// <param name="pIndex">The row index.</param>
        /// <returns>The found row, null otherwise.</returns>
        public RowDefinition this[uint pIndex]
        {
            get
            {
                if ( pIndex >= this.mRows.Count )
                {
                    return null;
                }

                return this.mRows[ (int)pIndex ];
            }
            set
            {
                if ( pIndex >= this.mRows.Count )
                {
                    return;
                }

                this.mRows[ (int)pIndex ] = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RowDefinitionCollection"/> class.
        /// </summary>
        public RowDefinitionCollection()
        {
            this.mRows = new List<RowDefinition>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new row.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(RowDefinition pItem)
        {
            this.mRows.Add( pItem );
            pItem.Index = this.mRows.Count - 1;
        }

        /// <summary>
        /// Removes a row.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(RowDefinition pItem)
        {
            return this.mRows.Remove( pItem );
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            this.mRows.Clear();
        }

        /// <summary>
        /// Checks whether the collection contains the given row or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(RowDefinition pItem)
        {
            return this.mRows.Contains( pItem );
        }

        /// <summary>
        /// Copies the rows into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(RowDefinition[] pArray, int pArrayIndex)
        {
            this.mRows.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Gets the row enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<RowDefinition> GetEnumerator()
        {
            return this.mRows.GetEnumerator();
        }
        
        /// <summary>
        /// Gets the row enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.Clear();
        }

        #endregion Methods
    }
}
