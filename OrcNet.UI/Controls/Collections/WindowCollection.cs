﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Collections
{
    /// <summary>
    /// Definition of the <see cref="WindowCollection"/> class.
    /// </summary>
    public sealed class WindowCollection : ICollection<IWindow>, ICollection
    {
        #region Fields

        /// <summary>
        /// Stores the windows.
        /// </summary>
        private List<IWindow> mWindows;

        /// <summary>
        /// Stores the sync root.
        /// </summary>
        private readonly object mSyncRoot = new object();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the window count
        /// </summary>
        public int Count
        {
            get
            {
                return this.mWindows.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is read only or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the collection sync root.
        /// </summary>
        public object SyncRoot
        {
            get
            {
                return this.mSyncRoot;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is synchronized or not.
        /// </summary>
        public bool IsSynchronized
        {
            get
            {
                return (this.mWindows as ICollection).IsSynchronized;
            }
        }

        /// <summary>
        /// Gets the window at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns>The window, null otherwise.</returns>
        public IWindow this[int pIndex]
        {
            get
            {
                if ( pIndex < 0 ||
                     pIndex >= this.Count )
                {
                    return null;
                }

                return this.mWindows[ pIndex ];
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowCollection"/> class.
        /// </summary>
        public WindowCollection()
        {
            this.mWindows = new List<IWindow>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowCollection"/> class.
        /// </summary>
        /// <param name="pOther"></param>
        public WindowCollection(IEnumerable<IWindow> pOther)
        {
            this.mWindows = new List<IWindow>( pOther );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds the given window.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(IWindow pItem)
        {
            lock ( this.mSyncRoot )
            {
                this.mWindows.Add( pItem );
            }
        }

        /// <summary>
        /// Clears the windows.
        /// </summary>
        public void Clear()
        {
            lock ( this.mSyncRoot )
            {
                this.mWindows.Clear();
            }
        }

        /// <summary>
        /// Checks whether the given window is in the collection or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(IWindow pItem)
        {
            return this.mWindows.Contains( pItem );
        }

        /// <summary>
        /// Copies the windows in the given array starting at the supplied index.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(IWindow[] pArray, int pArrayIndex)
        {
            this.mWindows.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Copies the windows in the given array starting at the supplied index.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pIndex"></param>
        void ICollection.CopyTo(Array pArray, int pIndex)
        {
            if ( pArray == null )
            {
                throw new ArgumentNullException( "pArray" );
            }

            if ( pIndex < 0 || 
                 (pIndex + this.Count) > pArray.Length )
            {
                throw new ArgumentOutOfRangeException( "pIndex" );
            }

            if ( pArray.Rank != 1 )
            {
                throw new ArgumentException( "Bad Rank" );
            }

            try
            {
                int lCount = this.Count;
                for ( int lCurr = 0; lCurr < lCount; lCurr++ )
                {
                    pArray.SetValue( this.mWindows[ lCurr ], pIndex + lCurr );
                }
            }
            catch ( InvalidCastException pEx )
            {
                throw new ArgumentException( string.Format( "Bad destination array", this.GetType().Name ), pEx );
            }
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<IWindow> GetEnumerator()
        {
            return this.mWindows.GetEnumerator();
        }

        /// <summary>
        /// Removes the given window.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(IWindow pItem)
        {
            bool lResult = false;

            lock ( this.mSyncRoot )
            {
                this.mWindows.Remove( pItem );
            }

            return lResult;
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes the window at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        internal void RemoveAt(int pIndex)
        {
            lock ( this.mSyncRoot )
            {
                this.mWindows.RemoveAt( pIndex );
            }
        }

        /// <summary>
        /// Checks whether the given window is in the collection or not.
        /// </summary>
        /// <param name="pWindow"></param>
        /// <returns></returns>
        internal bool HasItem(IWindow pWindow)
        {
            lock ( this.mSyncRoot )
            {
                for ( int lCurr = 0; lCurr < this.mWindows.Count; lCurr++ )
                {
                    if ( this.mWindows[ lCurr ] == pWindow )
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        
        #endregion Methods
    }
}
