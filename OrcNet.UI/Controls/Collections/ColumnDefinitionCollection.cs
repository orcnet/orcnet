﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Collections
{
    /// <summary>
    /// Definition of the <see cref="ColumnDefinitionCollection"/> class.
    /// </summary>
    public class ColumnDefinitionCollection : ICollection<ColumnDefinition>, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the set of columns.
        /// </summary>
        private List<ColumnDefinition> mColumns;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the column count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mColumns.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is readonly or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets or sets the column having the given index.
        /// </summary>
        /// <param name="pIndex">The column index.</param>
        /// <returns>The found column, null otherwise.</returns>
        public ColumnDefinition this[uint pIndex]
        {
            get
            {
                if ( pIndex >= this.mColumns.Count )
                {
                    return null;
                }

                return this.mColumns[ (int)pIndex ];
            }
            set
            {
                if ( pIndex >= this.mColumns.Count )
                {
                    return;
                }

                this.mColumns[ (int)pIndex ] = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnDefinitionCollection"/> class.
        /// </summary>
        public ColumnDefinitionCollection()
        {
            this.mColumns = new List<ColumnDefinition>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new column.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(ColumnDefinition pItem)
        {
            this.mColumns.Add( pItem );
            pItem.Index = this.mColumns.Count - 1;
        }

        /// <summary>
        /// Removes a column.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(ColumnDefinition pItem)
        {
            return this.mColumns.Remove( pItem );
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            this.mColumns.Clear();
        }

        /// <summary>
        /// Checks whether the collection contains the given column or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(ColumnDefinition pItem)
        {
            return this.mColumns.Contains( pItem );
        }

        /// <summary>
        /// Copies the columns into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(ColumnDefinition[] pArray, int pArrayIndex)
        {
            this.mColumns.CopyTo( pArray, pArrayIndex );
        }

        /// <summary>
        /// Gets the columns enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ColumnDefinition> GetEnumerator()
        {
            return this.mColumns.GetEnumerator();
        }
        
        /// <summary>
        /// Gets the columns enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            this.Clear();
        }

        #endregion Methods
    }
}
