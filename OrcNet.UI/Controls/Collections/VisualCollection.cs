﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.UI.Collections
{
    /// <summary>
    /// Definition of the <see cref="VisualCollection"/> class.
    /// </summary>
    public class VisualCollection : ICollection
    {
        #region Fields

        /// <summary>
        /// Stores the collection owner.
        /// </summary>
        private AVisual mOwner;

        /// <summary>
        /// Stores the set of visuals.
        /// </summary>
        private List<AVisual> mItems;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the set of visuals.
        /// </summary>
        internal List<AVisual> InternalItems
        {
            get
            {
                return this.mItems;
            }
        }

        /// <summary>
        /// Gets the collection count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mItems.Count;
            }
        }

        /// <summary>
        /// Gets the IsSynchronized flag.
        /// </summary>
        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the sync root.
        /// </summary>
        public object SyncRoot
        {
            get
            {
                return this;
            }
        }

        /// <summary>
        /// Gets the visual ate the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        public AVisual this[int pIndex]
        {
            get
            {
                if ( pIndex < 0 ||
                     pIndex >= this.Count )
                {
                    return null;
                }

                return this.mItems[ pIndex ];
            }
            set
            {
                if ( pIndex < 0 ||
                     pIndex >= this.Count )
                {
                    return;
                }

                AVisual lChild = this.mItems[ pIndex ];
                if ( value == null && 
                     lChild != null )
                {
                    DisconnectChild( pIndex );
                }
                else if ( value != null )
                {
                    if ( lChild != null )
                    {
                        throw new ArgumentException( "Already in use." );
                    }

                    if ( value.VisualParent != null ||
                         value.IsRootElement )
                    {
                        throw new ArgumentException( "Already has parent." );
                    }
 
                    ConnectChild( pIndex, value );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualCollection"/> class.
        /// </summary>
        /// <param name="pParent">The collection owner.</param>
        public VisualCollection(AVisual pParent)
        {
            if ( pParent == null )
            {
                throw new ArgumentNullException("pParent");
            }

            this.mOwner = pParent;
            this.mItems = new List<AVisual>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the given visual is contained or not.
        /// </summary>
        /// <param name="pVisual"></param>
        /// <returns></returns>
        public bool Contains(AVisual pVisual)
        {
            return pVisual.VisualParent == this.mOwner;
        }

        /// <summary>
        /// Adds a new visual.
        /// </summary>
        /// <param name="pVisual"></param>
        /// <returns></returns>
        public int Add(AVisual pVisual)
        {
            if ( pVisual != null &&
                 (pVisual.VisualParent != null || pVisual.IsRootElement) )
            {
                throw new ArgumentException( "Already has a parent." );
            }

            int lAddedElements = this.Count;
            if ( pVisual != null )
            {
                this.ConnectChild( lAddedElements, pVisual );
            }

            return lAddedElements;
        }

        /// <summary>
        /// Inserts a visual at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pVisual"></param>
        public void Insert(int pIndex, AVisual pVisual)
        {
            if ( pIndex < 0 || 
                 pIndex > this.Count )
            {
                throw new ArgumentOutOfRangeException("index");
            }

            if ( pVisual != null && 
                 (pVisual.VisualParent != null || pVisual.IsRootElement) )
            {
                throw new ArgumentException( "Already has a parent." );
            }

            if ( pVisual != null )
            {
                pVisual.ParentIndex = pIndex;
                this.mItems.Insert( pIndex, pVisual );
                this.mOwner.InternalAddVisualChild( pVisual );

                // Updates other visuals index
                for ( int lCurr = this.Count - 1; lCurr >= pIndex; lCurr-- )
                {
                    AVisual lChild = this.mItems[ lCurr ];
                    if ( lChild != null )
                    {
                        lChild.ParentIndex = lCurr + 1;
                    }
                }
            }
        }

        /// <summary>
        /// Removes a visual
        /// </summary>
        /// <param name="pVisual"></param>
        public void Remove(AVisual pVisual)
        {
            InternalRemove( pVisual );
        }

        /// <summary>
        /// Removes a visual at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        public void RemoveAt(int pIndex)
        {
            InternalRemove( this.mItems[ pIndex ] );
        }

        /// <summary>
        /// Removes a visual.
        /// </summary>
        /// <param name="pVisual"></param>
        private void InternalRemove(AVisual pVisual)
        {
            int lIndexToRemove = -1;

            if ( pVisual != null )
            {
                if ( pVisual.VisualParent != this.mOwner )
                {
                    // If the Visual is not in this collection we silently return without
                    // failing. This is the same behavior that ArrayList implements. See
                    // also Windows OS Bug #1100006.
                    return;
                }
                
                lIndexToRemove = pVisual.ParentIndex;
                this.DisconnectChild( lIndexToRemove );
            }
        }

        /// <summary>
        /// Gets the index of the given visual in the collection.
        /// </summary>
        /// <param name="pVisual"></param>
        /// <returns></returns>
        public int IndexOf(AVisual pVisual)
        {
            if ( pVisual == null )
            {
                // If the passed in argument is null, we find the first index with a null
                // entry and return it.
                for ( int i = 0; i < this.Count; i++ )
                {
                    if ( this.mItems[ i ] == null)
                    {
                        return i;
                    }
                }

                // No null entry found, return -1.
                return -1;
            }
            else if ( pVisual.VisualParent != this.mOwner )
            {
                return -1;
            }
            else
            {
                return pVisual.ParentIndex;
            }
        }

        /// <summary>
        /// Connects a child at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pChild"></param>
        private void ConnectChild(int pIndex, AVisual pChild)
        {
            Debug.Assert( pChild != null);
            Debug.Assert( this.mItems[ pIndex ] == null);
            Debug.Assert( pChild.VisualParent == null );
            Debug.Assert( pChild.IsRootElement == false );

            pChild.ParentIndex = pIndex;
            this.mItems.Add( pChild );

            // Notify the Visual tree about the children changes. 
            this.mOwner.InternalAddVisualChild( pChild );
        }

        /// <summary>
        /// Disconnects a child.
        /// </summary>
        /// <param name="pIndex"></param>
        private void DisconnectChild(int pIndex)
        {
            AVisual lChild = this.mItems[ pIndex ];
            
            AVisual lOldParent  = lChild.VisualParent;
            int lOldParentIndex = lChild.ParentIndex;
            
            this.mItems.Remove( lChild );
            
            this.mOwner.InternalRemoveVisualChild( lChild );
        }

        /// <summary>
        /// Copies the collection into the given array at the given index.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pIndex"></param>
        public void CopyTo(Array pArray, int pIndex)
        {
            if ( pArray == null)
            {
                throw new ArgumentNullException("pArray");
            }

            if ( pArray.Rank != 1)
            {
                throw new ArgumentException("Bad rank");
            }

            if ((pIndex < 0) ||
                (pArray.Length - pIndex < this.Count))
            {
                throw new ArgumentOutOfRangeException("index");
            }

            // System.Array does not have a CopyTo method that takes a count. Therefore
            // the loop is programmed here out.
            for ( int lCurr = 0; lCurr < this.Count; lCurr++ )
            {
                pArray.SetValue( this.mItems[ lCurr ], lCurr + pIndex );
            }
        }

        /// <summary>
        /// Clears all elements.
        /// </summary>
        public void Clear()
        {
            while ( this.mItems.Count != 0 )
            {
                this.DisconnectChild( 0 );
            }
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return this.mItems.GetEnumerator();
        }

        #endregion Methods
    }
}
