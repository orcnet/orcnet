﻿using System;
using System.Collections;

namespace OrcNet.UI.Collections
{
    /// <summary>
    /// Definition of the <see cref="UIElementCollection"/> class.
    /// </summary>
    public class UIElementCollection : IList
    {
        #region Fields

        /// <summary>
        /// Clean up delegate to process on an UIElement on Clear if special needed.
        /// </summary>
        /// <param name="pElement"></param>
        protected internal delegate void CleanupDelegate(UIElement pElement);

        /// <summary>
        /// Stores the element owner of this children collection.
        /// </summary>
        private readonly UIElement mVisualParent;

        /// <summary>
        /// Stores the logical parent of each elements of this collection.
        /// </summary>
        private readonly FrameworkElement mLogicalParent;

        /// <summary>
        /// Stores the UI elements.
        /// </summary>
        private readonly VisualCollection mVisualChildren;

        /// <summary>
        /// Stores the sync root.
        /// </summary>
        private readonly object mSyncRoot = new object();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the visual parent.
        /// </summary>
        internal UIElement VisualParent
        {
            get
            {
                return this.mVisualParent;
            }
        }

        /// <summary>
        /// Gets the logical parent.
        /// </summary>
        internal FrameworkElement LogicalParent
        {
            get
            {
                return this.mLogicalParent;
            }
        }

        /// <summary>
        /// Gets or sets the element at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        public UIElement this[int pIndex]
        {
            get
            {
                return this.mVisualChildren[ pIndex ] as UIElement;
            }

            set
            {
                UIElement lOldElement = this.mVisualChildren[ pIndex ] as UIElement;
                if ( lOldElement != value )
                {
                    this.ClearLogicalParent( lOldElement );

                    this.mVisualChildren[ pIndex ] = value;

                    this.SetLogicalParent( value );

                    this.mVisualParent.InvalidateMeasure();
                }
            }
        }

        /// <summary>
        /// Gets or sets the element at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        object IList.this[int pIndex]
        {
            get
            {
                return this[ pIndex ];
            }

            set
            {
                this[ pIndex ] = Cast( value );
            }
        }

        /// <summary>
        /// Gets the collection count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mVisualChildren.Count;
            }
        }

        /// <summary>
        /// Gets the IsFixedSize flag.
        /// </summary>
        public bool IsFixedSize
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the IsReadOnly flag
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the IsSynchronized flag
        /// </summary>
        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the sync root.
        /// </summary>
        public object SyncRoot
        {
            get
            {
                return this.mSyncRoot;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UIElementCollection"/> class.
        /// </summary>
        /// <param name="pVisualParent">The element owner of this children collection.</param>
        /// <param name="pLogicalParent">The logical parent of each elements of this collection.</param>
        public UIElementCollection(UIElement pVisualParent, FrameworkElement pLogicalParent)
        {
            this.mVisualChildren = new VisualCollection( pVisualParent );
            this.mLogicalParent = pLogicalParent;
            this.mVisualParent  = pVisualParent;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new element.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns>The amount of elements.</returns>
        public virtual int Add(UIElement pElement)
        {
            return this.AddInternal( pElement );
        }

        /// <summary>
        /// Adds a new element.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        internal int AddInternal(UIElement pElement)
        {
            this.SetLogicalParent( pElement );
            this.mVisualChildren.Add( pElement );

            // Invalidate parent measure.
            this.mVisualParent.InvalidateMeasure();

            return this.Count;
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public virtual void Clear()
        {
            this.ClearInternal();
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        /// <param name="pClean">The optional clean up delegate.</param>
        internal void ClearInternal(CleanupDelegate pClean = null)
        {
            int lCount = this.Count;
            AVisual[] lVisuals = new AVisual[ lCount ];
            for (int i = 0; i < lCount; i++)
            {
                lVisuals[i] = this.mVisualChildren[ i ];
            }

            this.mVisualChildren.Clear();

            for ( int lCurr = 0; lCurr < lCount; lCurr++ )
            {
                UIElement lElement = lVisuals[ lCurr ] as UIElement;
                if ( lElement != null )
                {
                    pClean( lElement );
                    this.ClearLogicalParent( lElement );
                }
            }
            
            this.mVisualParent.InvalidateMeasure();
        }

        /// <summary>
        /// Checks whether the given element is in the collection.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        public bool Contains(UIElement pElement)
        {
            return this.mVisualChildren.Contains( pElement );
        }

        /// <summary>
        /// Copies the collection into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pIndex"></param>
        public void CopyTo(UIElement[] pArray, int pIndex)
        {
            this.mVisualChildren.CopyTo( pArray, pIndex );
        }
        
        /// <summary>
        /// Gets the given element index.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        public int IndexOf(UIElement pElement)
        {
            return this.mVisualChildren.IndexOf( pElement );
        }

        /// <summary>
        /// Inserts an element.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pElement"></param>
        public virtual void Insert(int pIndex, UIElement pElement)
        {
            this.InsertInternal( pIndex, pElement );
        }

        /// <summary>
        /// Inserts an element.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pElement"></param>
        internal void InsertInternal(int pIndex, UIElement pElement)
        {
            this.SetLogicalParent( pElement );
            this.mVisualChildren.Insert( pIndex, pElement );
            this.mVisualParent.InvalidateMeasure();
        }

        /// <summary>
        /// Removes the given element.
        /// </summary>
        /// <param name="pElement"></param>
        public virtual void Remove(UIElement pElement)
        {
            this.RemoveInternal( pElement );
        }

        /// <summary>
        /// Remove the element at the given position.
        /// </summary>
        /// <param name="pIndex"></param>
        public virtual void RemoveAt(int pIndex)
        {
            UIElement lElement = this.mVisualChildren[ pIndex ] as UIElement;

            this.mVisualChildren.RemoveAt( pIndex );

            if ( lElement != null )
            {
                this.ClearLogicalParent( lElement );
            }

            this.mVisualParent.InvalidateMeasure();
        }

        /// <summary>
        /// Removes the given element.
        /// </summary>
        /// <param name="pElement"></param>
        internal void RemoveInternal(UIElement pElement)
        {
            this.mVisualChildren.Remove( pElement );
            this.ClearLogicalParent( pElement );
            this.mVisualParent.InvalidateMeasure();
        }

        /// <summary>
        /// Adds a new element into the collection.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        int IList.Add(object pElement)
        {
            return Add(Cast(pElement));
        }
        
        /// <summary>
        /// Checks whether the given element is contained in the collection or not.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        bool IList.Contains(object pElement)
        {
            return Contains(pElement as UIElement);
        }

        /// <summary>
        /// Copies the collection into the given array.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pIndex"></param>
        public void CopyTo(Array pArray, int pIndex)
        {
            if ( pArray == null)
            {
                throw new ArgumentNullException("pArray");
            }

            if ( pArray.Rank != 1)
            {
                throw new ArgumentException("Bad rank");
            }

            if ((pIndex < 0) ||
                (pArray.Length - pIndex < this.Count))
            {
                throw new ArgumentOutOfRangeException("index");
            }

            // System.Array does not have a CopyTo method that takes a count. Therefore
            // the loop is programmed here out.
            for ( int lCurr = 0; lCurr < this.Count; lCurr++ )
            {
                pArray.SetValue( this.mVisualChildren[ lCurr ], lCurr + pIndex );
            }
        }

        /// <summary>
        /// Gets the collection enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return this.mVisualChildren.GetEnumerator();
        }

        /// <summary>
        /// Gets the given element index.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        int IList.IndexOf(object pElement)
        {
            return this.IndexOf( pElement as UIElement );
        }

        /// <summary>
        /// Insert the given element at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pElement"></param>
        void IList.Insert(int pIndex, object pElement)
        {
            Insert(pIndex, Cast(pElement));
        }

        /// <summary>
        /// Removes the given element from the collection.
        /// </summary>
        /// <param name="pElement"></param>
        void IList.Remove(object pElement)
        {
            this.Remove( pElement as UIElement );
        }

        /// <summary>
        /// Cast an object into a UIElement.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns></returns>
        private UIElement Cast(object pElement)
        {
            if ( pElement == null)
                throw new ArgumentException("pElement is Null");

            UIElement lElement = pElement as UIElement;

            if (lElement == null)
                throw new ArgumentException( "Not a UIElement" );

            return lElement;
        }

        /// <summary>
        /// Assigns the logical parent of the given element.
        /// </summary>
        /// <param name="pElement"></param>
        protected void SetLogicalParent(UIElement pElement)
        {
            if ( this.mLogicalParent != null )
            {
                this.mLogicalParent.AddLogicalChild( pElement );
            }
        }

        /// <summary>
        /// Removes the logical parent when element goes away from the collection.
        /// </summary>
        /// <param name="pElement"></param>
        protected void ClearLogicalParent(UIElement pElement)
        {
            if ( this.mLogicalParent != null )
            {
                this.mLogicalParent.RemoveLogicalChild( pElement );
            }
        }

        #endregion Methods
    }
}
