﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI.Collections
{
    /// <summary>
    /// Definition of the <see cref="DoubleCollection"/> class.
    /// </summary>
    public class DoubleCollection : IList, IList<double>
    {
        #region Fields

        /// <summary>
        /// Constants empty collection.
        /// </summary>
        internal static readonly IEnumerable<double> sEmpty = new DoubleCollection();

        /// <summary>
        /// Stores the internal collection.
        /// </summary>
        private List<double> mCollection;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on collection changed.
        /// </summary>
        public event EventHandler Changed;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the collection value at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        public double this[int pIndex]
        {
            get
            {
                if ( pIndex < 0 ||
                     pIndex >= this.Count )
                {
                    return 0;
                }

                return this.mCollection[ pIndex ];
            }
            set
            {
                if ( pIndex < 0 ||
                     pIndex >= this.Count )
                {
                    return;
                }

                this.mCollection[ pIndex ] = value;

                this.NotifyChanged();
            }
        }

        /// <summary>
        /// Gets the collection value at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        object IList.this[int pIndex]
        {
            get
            {
                return this[ pIndex ];
            }
            set
            {
                this[ pIndex ] = (double)value;
            }
        }

        /// <summary>
        /// Gets the collection count.
        /// </summary>
        public int Count
        {
            get
            {
                return this.mCollection.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is of fixed size or not.
        /// </summary>
        public bool IsFixedSize
        {
            get
            {
                return (this.mCollection as IList).IsFixedSize;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is read only or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return (this.mCollection as IList).IsReadOnly;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is synchronized or not.
        /// </summary>
        bool ICollection.IsSynchronized
        {
            get
            {
                return (this.mCollection as ICollection).IsSynchronized;
            }
        }

        /// <summary>
        /// Gets the collection synch root.
        /// </summary>
        object ICollection.SyncRoot
        {
            get
            {
                return this;
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DoubleCollection"/> class.
        /// </summary>
        public DoubleCollection()
        {
            this.mCollection = new List<double>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DoubleCollection"/> class.
        /// </summary>
        /// <param name="pValues">The values to store.</param>
        public DoubleCollection(IEnumerable<double> pValues)
        {
            this.mCollection = new List<double>( pValues );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds the given value.
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        int IList.Add(object pValue)
        {
            return this.InternalAdd( (double)pValue );
        }

        /// <summary>
        /// Adds the given item.
        /// </summary>
        /// <param name="pItem"></param>
        public void Add(double pItem)
        {
            this.InternalAdd( pItem );
        }

        /// <summary>
        /// Internal Add.
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        private int InternalAdd(double pValue)
        {
            int lIndex = -1;

            this.mCollection.Add( pValue );
            lIndex = this.Count - 1;

            this.NotifyChanged();

            return lIndex;
        }
        
        /// <summary>
        /// Checks whether the given value is contained or not.
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        bool IList.Contains(object pValue)
        {
            if( pValue is double )
            {
                return this.Contains( (double)pValue );
            }

            return false;
        }

        /// <summary>
        /// Checks whether the given item is contained or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(double pItem)
        {
            return this.mCollection.Contains( pItem );
        }
        
        /// <summary>
        /// Copies the collection into the given array starting at the supplied index.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pIndex"></param>
        void ICollection.CopyTo(Array pArray, int pIndex)
        {
            if ( pArray == null )
            {
                throw new ArgumentNullException( "pArray" );
            }

            if ( pIndex < 0 || 
                 (pIndex + this.Count) > pArray.Length )
            {
                throw new ArgumentOutOfRangeException( "pIndex" );
            }

            if ( pArray.Rank != 1 )
            {
                throw new ArgumentException( "Bad Rank" );
            }

            try
            {
                int lCount = this.Count;
                for ( int lCurr = 0; lCurr < lCount; lCurr++ )
                {
                    pArray.SetValue( this.mCollection[ lCurr ], pIndex + lCurr );
                }
            }
            catch ( InvalidCastException pEx )
            {
                throw new ArgumentException( string.Format( "Bad destination array", this.GetType().Name ), pEx );
            }
        }

        /// <summary>
        /// Copies the collection into the given array starting at the supplied index.
        /// </summary>
        /// <param name="pArray"></param>
        /// <param name="pArrayIndex"></param>
        public void CopyTo(double[] pArray, int pArrayIndex)
        {
            if ( pArray == null )
            {
                throw new ArgumentNullException( "pArray" );
            }

            if ( pArrayIndex < 0 ||
                 (pArrayIndex + this.Count) > pArray.Length )
            {
                throw new ArgumentOutOfRangeException( "pArrayIndex" );
            }

            this.mCollection.CopyTo( pArray, pArrayIndex );
        }
        
        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator<double> IEnumerable<double>.GetEnumerator()
        {
            return this.mCollection.GetEnumerator();
        }

        /// <summary>
        /// Gets the collection index of the given value.
        /// </summary>
        /// <param name="pValue"></param>
        /// <returns></returns>
        int IList.IndexOf(object pValue)
        {
            if ( pValue is double )
            {
                return this.IndexOf( (double)pValue );
            }

            return -1;
        }

        /// <summary>
        /// Gets the collection index of the given item.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public int IndexOf(double pItem)
        {
            return this.mCollection.IndexOf( pItem );
        }
        
        /// <summary>
        /// Inserts the given value at the specified index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pValue"></param>
        void IList.Insert(int pIndex, object pValue)
        {
            this.Insert( pIndex, (double)pValue );
        }

        /// <summary>
        /// Inserts the given item at the specified index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <param name="pItem"></param>
        public void Insert(int pIndex, double pItem)
        {
            this.mCollection.Insert( pIndex, pItem );

            this.NotifyChanged();
        }
        
        /// <summary>
        /// Removes the given value.
        /// </summary>
        /// <param name="pValue"></param>
        void IList.Remove(object pValue)
        {
            if ( pValue is double )
            {
                this.Remove( (double)pValue );
            }
        }

        /// <summary>
        /// Removes the given item.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Remove(double pItem)
        {
            bool lResult = this.mCollection.Remove( pItem );

            this.NotifyChanged();

            return lResult;
        }

        /// <summary>
        /// Removes the value at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        public void RemoveAt(int pIndex)
        {
            this.RemoveAt( pIndex );

            this.NotifyChanged();
        }

        /// <summary>
        /// Clear the collection
        /// </summary>
        public void Clear()
        {
            this.mCollection.Clear();

            this.NotifyChanged();
        }

        /// <summary>
        /// Notifies the collection has changed.
        /// </summary>
        private void NotifyChanged()
        {
            if ( this.Changed != null )
            {
                this.Changed( this, EventArgs.Empty );
            }
        }

        #endregion Methods
    }
}
