﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ResizeMode"/> enumeration.
    /// </summary>
    public enum ResizeMode
    {
        /// <summary>
        /// Cannot resize at all. No maximize and minimize actions available.
        /// </summary>
        NoResize = 0,

        /// <summary>
        /// Can only Minimize. No maximize action available.
        /// </summary>
        CanMinimize = 1,

        /// <summary>
        /// Can Minimize and Maximize.
        /// </summary>
        CanResize = 2,

        /// <summary>
        /// Can Minimize, Maximize and resize only with the bottom-right corner handle.
        /// </summary>
        CanResizeWithGrip = 3

    }
}
