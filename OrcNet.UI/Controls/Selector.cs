﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the abstract <see cref="Selector"/> base class.
    /// 
    /// NOTE: This class is in charge of managing the selection of selectable elements
    /// in an items based control.
    /// </summary>
    public abstract class Selector : ItemsControl
    {
        #region Fields

        /// <summary>
        /// Stores the selection foreground.
        /// </summary>
        private Color mSelectionForeground;

        /// <summary>
        /// Stores the selection background.
        /// </summary>
        private Color mSelectionBackground;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on selection changes.
        /// </summary>
        public event EventHandler<SelectionChangeEventArgs<object>> SelectionChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the currently selected item index.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return this.Items.CurrentPosition;
            }
            set
            {
                this.Unselect( this.SelectedItem as Control );

                this.Items.MoveCurrentToPosition( value );

                this.Select( this.SelectedItem as Control );
            }
        }

        /// <summary>
        /// Gets or sets the currently selected item.
        /// </summary>
        public virtual object SelectedItem
        {
            get
            {
                return this.Items.CurrentItem;
            }
            set
            {
                this.Unselect( this.SelectedItem as Control );

                this.Items.MoveCurrentTo( value );

                this.Select( this.SelectedItem as Control );
            }
        }

        /// <summary>
        /// Gets or sets the selection foreground.
        /// </summary>
        public virtual Color SelectionForeground
        {
            get
            {
                return this.mSelectionForeground;
            }
            set
            {
                if ( value == this.mSelectionForeground )
                {
                    return;
                }

                this.mSelectionForeground = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Gets or sets the selection background.
        /// </summary>
        public virtual Color SelectionBackground
        {
            get
            {
                return this.mSelectionBackground;
            }
            set
            {
                if ( value == this.mSelectionBackground )
                {
                    return;
                }

                this.mSelectionBackground = value;
                this.NotifyPropertyChanged();
                this.InvalidateVisual();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Selector"/> class.
        /// </summary>
        protected Selector()
        {
            this.SelectionChanged += this.OnSelectionChanged;
        }
        
        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new child to the panel.
        /// </summary>
        /// <param name="pChild"></param>
        protected override void AddChildInternal(UIElement pChild)
        {
            base.AddChildInternal( pChild );

            pChild.MouseClick += this.OnChildMouseClick;
        }

        /// <summary>
        /// Removes a child from the panel.
        /// </summary>
        /// <param name="pChild"></param>
        protected override void RemoveChildInternal(UIElement pChild)
        {
            base.RemoveChildInternal( pChild );

            pChild.MouseClick -= this.OnChildMouseClick;
        }

        /// <summary>
        /// Clears the children.
        /// </summary>
        protected override void ClearItems()
        {
            foreach ( object lItem in this.Items )
            {
                FrameworkElement lCast = lItem as FrameworkElement;
                if ( lCast != null )
                {
                    lCast.MouseClick -= this.OnChildMouseClick;
                }
            }

            base.ClearItems();
        }

        /// <summary>
        /// Delegate called on child mouse click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnChildMouseClick(object pSender, MouseButtonEventArgs pEventArgs)
        {
            this.SelectedIndex = this.Items.IndexOf( pSender );
        }

        /// <summary>
        /// Delegate called on item source changing.
        /// </summary>
        protected override void OnItemSourceChanging()
        {
            this.Unselect( this.Items.CurrentItem as Control );

            base.OnItemSourceChanging();
        }

        /// <summary>
        /// Delegate called on item source changed.
        /// </summary>
        protected override void OnItemSourceChanged()
        {
            this.Select( this.Items.CurrentItem as Control );
            
            base.OnItemSourceChanged();
        }

        /// <summary>
        /// Unselects the given element.
        /// </summary>
        /// <param name="pElement"></param>
        private void Unselect(Control pElement)
        {
            if ( pElement == null )
            {
                return;
            }

            pElement.Foreground = Color.Transparent;
            pElement.Background = Color.Transparent;
        }

        /// <summary>
        /// Selects The given element.
        /// </summary>
        /// <param name="pElement"></param>
        private void Select(Control pElement)
        {
            if ( pElement == null )
            {
                return;
            }

            pElement.Foreground = this.mSelectionForeground;
            pElement.Background = this.mSelectionBackground;

            this.NotifyPropertyChanged( "SelectedItem" );
            this.NotifyPropertyChanged( "SelectedIndex" );
            this.NotifySelectionChanged();
        }

        /// <summary>
        /// Delegate called on selection changes.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnSelectionChanged(object pSender, SelectionChangeEventArgs<object> pEventArgs)
        {

        }

        /// <summary>
        /// Notifies the current selection has changed.
        /// </summary>
        private void NotifySelectionChanged()
        {
            if ( this.SelectionChanged != null )
            {
                this.SelectionChanged( this, new SelectionChangeEventArgs<object>( this.SelectedItem, null ) );
            }
        }
        
        #endregion Methods
    }
}
