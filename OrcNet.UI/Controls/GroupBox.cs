﻿

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="GroupBox"/> class.
    /// </summary>
    public class GroupBox : HeaderedContentControl
    {
		#region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupBox"/> class.
        /// </summary>
		public GroupBox() : 
        base()
        {

        }

		#endregion
	}
}
