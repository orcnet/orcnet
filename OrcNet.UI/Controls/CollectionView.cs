﻿using OrcNet.UI.GraphicObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="CollectionView"/> class.
    /// </summary>
    public class CollectionView : ICollectionView, INotifyPropertyChanged
    {
        #region Fields
        
        /// <summary>
        /// Stores the current item selected in the collection view.
        /// </summary>
        private object mCurrentItem;

        /// <summary>
        /// Stores the current item index selected in the collection view.
        /// </summary>
        private int mCurrentPosition;

        /// <summary>
        /// Stores the filter callback.
        /// </summary>
        private Predicate<object> mFilter;

        /// <summary>
        /// Stores the source collection.
        /// </summary>
        private IList<object> mSourceCollection;

        /// <summary>
        /// Stores the sync root object.
        /// </summary>
        private object mSyncRoot = new object();

        #endregion Fields

        #region Events

        ///<summary>
        /// Raise this event before changing currency.
        ///</summary>
        public event EventHandler CurrentChanging;

        ///<summary>
        ///Raise this event after changing currency.
        ///</summary>
        public event EventHandler CurrentChanged;

        /// <summary>
        /// Event fired on property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Event fired on collection changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the sync root object.
        /// </summary>
        internal object SyncRoot
        {
            get
            {
                return mSyncRoot;
            }
        }

        /// <summary>
        /// Checks whether their is a current element in the view or not.
        /// </summary>
        private bool IsCurrentInView
        {
            get
            {
                return (0 <= CurrentPosition && CurrentPosition < Count);
            }
        }

        /// <summary>
        /// Gets the source collection.
        /// </summary>
        protected internal virtual IList<object> InternalSourceCollection
        {
            get
            {
                return this.mSourceCollection;
            }
        }

        /// <summary>
        /// Gets the source collection.
        /// </summary>
        public virtual IEnumerable SourceCollection
        {
            get
            {
                return this.mSourceCollection;
            }
        }

        /// <summary>
        /// Gets or sets the filter callback.
        /// </summary>
        public virtual Predicate<object> Filter
        {
            get
            {
                return this.mFilter;
            }
            set
            {
                if ( this.CanFilter )
                {
                    this.mFilter = value;

                    // Refresh.
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection can be filtered or not.
        /// </summary>
        public virtual bool CanFilter
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection can be sorted or not.
        /// </summary>
        public virtual bool CanSort
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection can be grouped or not.
        /// </summary>
        public virtual bool CanGroup
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the collection count.
        /// </summary>
        public virtual int Count
        {
            get
            {
                return this.mSourceCollection.Count;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is empty or not.
        /// </summary>
        public virtual bool IsEmpty
        {
            get
            {
                return this.mSourceCollection.Count == 0;
            }
        }

        /// <summary>
        /// Gets the current item selected in the collection view.
        /// </summary>
        public virtual object CurrentItem
        {
            get
            {
                return this.mCurrentItem;
            }
        }

        /// <summary>
        /// Gets the current item index selected in the collection view.
        /// </summary>
        public virtual int CurrentPosition
        {
            get
            {
                return this.mCurrentPosition;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionView"/> class.
        /// </summary>
        public CollectionView() :
        this( null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionView"/> class.
        /// </summary>
        /// <param name="pCollection"></param>
        /// <param name="pSelectFirst"></param>
        public CollectionView(IEnumerable pCollection, bool pSelectFirst = false)
        {
            this.mSourceCollection = pCollection != null ? new List<object>( pCollection.Cast<object>() ) : new List<object>();
            
            if ( pSelectFirst )
            {
                this.mCurrentItem = this.mSourceCollection.FirstOrDefault();
                if ( this.mCurrentItem != null )
                {
                    this.mCurrentPosition = 0;
                }
            }
            else
            {
                this.mCurrentItem = null;
                this.mCurrentPosition = -1;
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the given element is in the collection or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public bool Contains(object pItem)
        {
            return this.mSourceCollection.Contains( pItem );
        }

        /// <summary>
        /// Sets the given element to the top.
        /// </summary>
        /// <param name="pElement"></param>
        public void SetOnTop(object pElement)
        {
            if ( this.mSourceCollection.Contains( pElement ) )
            {
                lock ( this.mSyncRoot )
                {
                    this.mSourceCollection.Remove( pElement );
                    this.mSourceCollection.Add( pElement );
                }
            }
        }

        /// <summary>
        /// Sets the given element to the bottom.
        /// </summary>
        /// <param name="pElement"></param>
        public void SetOnBottom(object pElement)
        {
            if ( this.mSourceCollection.Contains( pElement ) )
            {
                lock ( this.mSyncRoot )
                {
                    this.mSourceCollection.Remove( pElement );
                    this.mSourceCollection.Insert( 0, pElement );
                }
            }
        }

        /// <summary>
        /// Move the 
        /// </summary>
        /// <returns></returns>
        public virtual bool MoveCurrentToFirst()
        {
            return MoveCurrentToPosition( 0 );
        }

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the last item.
        /// </summary>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        public virtual bool MoveCurrentToLast()
        {
            return MoveCurrentToPosition( this.Count - 1 );
        }

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the next item.
        /// </summary>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        public virtual bool MoveCurrentToNext()
        {
            int lNewIndex = CurrentPosition + 1;
            int lCount = this.Count;
            
            if ( lNewIndex <= lCount )
            {
                return MoveCurrentToPosition( lNewIndex );
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the previous item.
        /// </summary>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        public virtual bool MoveCurrentToPrevious()
        {
            int lNewIndex = this.CurrentPosition - 1;
            int lCount = Count;
            
            if ( lNewIndex >= -1 )
            {
                return MoveCurrentToPosition( lNewIndex );
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the given item.
        /// </summary>
        /// <param name="pItem">Move CurrentItem to this item.</param>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        public virtual bool MoveCurrentTo(object pItem)
        {
            if ( object.Equals(CurrentItem, pItem) )
            {
                // also check that we're not fooled by a false null _currentItem
                if ( pItem != null || 
                     this.IsCurrentInView )
                {
                    return this.IsCurrentInView;
                }
            }

            int lIndex = -1;
            if ( this.PassesFilter( pItem ) )
            {
                // if the item is not found IndexOf() will return -1, and
                // the MoveCurrentToPosition() below will move current to BeforeFirst
                lIndex = IndexOf( pItem );
            }

            return MoveCurrentToPosition( lIndex );
        }

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the item at the given index.
        /// </summary>
        /// <param name="pPosition">Move CurrentItem to this index</param>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        public virtual bool MoveCurrentToPosition(int pPosition)
        {
            if ( pPosition < -1 || 
                 pPosition > this.Count )
            {
                throw new ArgumentOutOfRangeException("pPosition");
            }

            
            if ( pPosition != CurrentPosition )
            {
                this.InternalMoveCurrentToPosition( pPosition );

                this.NotifyCurrentChanged();
                
                this.NotifyPropertyChanged( "CurrentItem" );
                this.NotifyPropertyChanged( "CurrentPosition" );
            }

            return IsCurrentInView;
        }

        /// <summary>
        /// Internal move current to position.
        /// </summary>
        /// <param name="pPosition"></param>
        private void InternalMoveCurrentToPosition(int pPosition)
        {
            if ( pPosition < 0 )
            {
                SetCurrent( null, -1 );
            }
            else if ( pPosition >= this.Count )
            {
                SetCurrent(null, Count);
            }
            else
            {
                SetCurrent( this.mSourceCollection[ pPosition ], pPosition );
            }
        }

        /// <summary>
        /// Gets the given object index in the list.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public virtual int IndexOf(object pItem)
        {
            return this.mSourceCollection.IndexOf( pItem );
        }

        /// <summary>
        /// Gets the item at the given index.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        public virtual object GetItemAt(int pIndex)
        {
            if ( pIndex < 0 )
                throw new ArgumentOutOfRangeException("pIndex");

            return this.mSourceCollection[ pIndex ];
        }

        /// <summary>
        /// Checks whether the given element passes filter callback or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        public virtual bool PassesFilter(object pItem)
        {
            if ( this.CanFilter && 
                 this.Filter != null)
            {
                return this.Filter( pItem );
            }

            return true;
        }

        /// <summary>
        /// Gets the collection view enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return this.mSourceCollection.GetEnumerator();
        }

        /// <summary>
        /// Sets a new item as current.
        /// </summary>
        /// <param name="pNewItem"></param>
        /// <param name="pNewPosition"></param>
        protected void SetCurrent(object pNewItem, int pNewPosition)
        {
            this.mCurrentItem = pNewItem;
            this.mCurrentPosition = pNewPosition;
        }

        /// <summary>
        /// Notifies the current item is changing.
        /// </summary>
        protected void NotifyCurrentChanging()
        {
            if( this.CurrentChanging != null )
            {
                this.CurrentChanging( this, new EventArgs() );
            }
        }

        /// <summary>
        /// Notifies the current item is changed.
        /// </summary>
        protected void NotifyCurrentChanged()
        {
            if( this.CurrentChanged != null )
            {
                this.CurrentChanged( this, new EventArgs() );
            }
        }

        /// <summary>
        /// Notifies a property has changed.
        /// </summary>
        /// <param name="pPropertyName">The property name.</param>
        protected void NotifyPropertyChanged([CallerMemberName]string pPropertyName = null)
        {
            if ( this.PropertyChanged != null )
            {
                this.PropertyChanged( this, new PropertyChangedEventArgs( pPropertyName ) );
            }
        }

        /// <summary>
        /// Notifies the collection has changed.
        /// </summary>
        /// <param name="pNewItems"></param>
        /// <param name="pNewItemIndex"></param>
        /// <param name="pOldItems"></param>
        /// <param name="pOldItemIndex"></param>
        /// <param name="pAction"></param>
        protected void NotifyCollectionChanged(IList pNewItems, int pNewItemIndex, IList pOldItems, int pOldItemIndex, NotifyCollectionChangedAction pAction)
        {
            if ( this.CollectionChanged != null )
            {
                switch ( pAction )
                {
                    case NotifyCollectionChangedAction.Add:
                        this.CollectionChanged( this, new NotifyCollectionChangedEventArgs( pAction, pNewItems ) );
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        this.CollectionChanged( this, new NotifyCollectionChangedEventArgs( pAction, pOldItems ) );
                        break;
                    case NotifyCollectionChangedAction.Replace:
                        this.CollectionChanged( this, new NotifyCollectionChangedEventArgs( pAction, pNewItems, pOldItems ) );
                        break;
                    case NotifyCollectionChangedAction.Move:
                        this.CollectionChanged( this, new NotifyCollectionChangedEventArgs( pAction, pNewItems, pNewItemIndex, pOldItemIndex ) );
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        this.CollectionChanged( this, new NotifyCollectionChangedEventArgs( pAction ) );
                        break;
                }
            }
        }

        #endregion Methods
    }
}
