﻿using OrcNet.UI.Input;
using OrcNet.UI.Style;
using OrcNet.UI.Ximl;
using OrcNet.UI.Ximl.Collections;
using System;
using System.Collections;
using System.Diagnostics;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="FrameworkElement"/> class.
    /// </summary>
    public class FrameworkElement : UIElement, IFrameworkInputElement
    {
        #region Fields

        /// <summary>
        /// Stores the element cursor.
        /// </summary>
        private XCursor mCursor;

        /// <summary>
        /// Stores the flag indicating whether the theme style is currently being updated.
        /// </summary>
        private bool mIsThemeStyleUpdateInProgress = false;

        /// <summary>
        /// Stores the Defaukt style key.
        /// </summary>
        private string mDefaultStyleKey;

        /// <summary>
        /// Stores the theme style.
        /// </summary>
        private Ximl.Style mThemeStyle;

        /// <summary>
        /// Stores the style template parent of this node.
        /// Not Null if this element is created from Template.
        /// </summary>
        internal AVisual mTemplatedParent;

        /// <summary>
        /// Stores the style template child of this node.
        /// Not Null if this element has a child that was created from Template.
        /// </summary>
        private UIElement mTemplateChild;

        /// <summary>
        /// Stores the Fit size constant value.
        /// </summary>
        protected const double cFitSizeValue = -1;

        /// <summary>
        /// Stores the flag indicaring whether the visual tree has been created via a template or not.
        /// </summary>
        private bool mHasTemplateGeneratedVisualTree;

        /// <summary>
        /// Stores the X screen position.
        /// </summary>
        private int mLeft;

        /// <summary>
        /// Stores the Y screen position.
        /// </summary>
        private int mTop;

        /// <summary>
        /// Stores the element Tag.
        /// </summary>
        private object mTag;

        /// <summary>
        /// Stores the element name. (Can be empty)
        /// </summary>
        private string mName;
        
        /// <summary>
        /// Stores the element style.
        /// </summary>
        private Ximl.Style mStyle;

        /// <summary>
        /// Stores the local defined resources.
        /// </summary>
        private ResourceDictionary mResources;

        /// <summary>
        /// Stores the data context object.
        /// </summary>
        protected object mDataContext;

        /// <summary>
        /// Stores the logical parent.
        /// </summary>
        private AVisual mLogicalParent;

        /// <summary>
        /// Gets or sets the outer margin.
        /// </summary>
        private Thickness mMargin;

        /// <summary>
        /// Stores the element width.
        /// </summary>
        private double mWidth;

        /// <summary>
        /// Stores the element height.
        /// </summary>
        private double mHeight;

        /// <summary>
        /// Stores the element minimum width.
        /// </summary>
        private double mMinWidth;

        /// <summary>
        /// Stores the element minimum height.
        /// </summary>
        private double mMinHeight;

        /// <summary>
        /// Stores the element maximum width.
        /// </summary>
        private double mMaxWidth;

        /// <summary>
        /// Stores the element maximum height.
        /// </summary>
        private double mMaxHeight;

        /// <summary>
        /// Stores the element vertical alignment.
        /// </summary>
        private VerticalAlignment mVerticalAlignment;

        /// <summary>
        /// Stores the element horizontal alignment.
        /// </summary>
        private HorizontalAlignment mHorizontalAlignment;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on data context changes.
        /// </summary>
        public event EventHandler<PropertyValueChangedEventArgs> DataContextChanged;

        /// <summary>
        /// Event fired on logical parent changes.
        /// </summary>
        public event EventHandler<PropertyValueChangedEventArgs> LogicalParentChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the element cursor.
        /// </summary>
        public XCursor Cursor
        {
            get
            {
                return this.mCursor;
            }
            set
            {
                if ( this.mCursor == value )
                {
                    return;
                }

                this.mCursor = value;

                this.NotifyPropertyChanged();

                if ( this.IsMouseOver )
                {
                    // Update cursor.
                    InputManager.Instance.MouseCursor = this.mCursor;
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the element has any logical children or not.
        /// </summary>
        internal bool HasLogicalChildren
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the style parent of this node if any.
        /// </summary>
        public AVisual TemplatedParent
        {
            get
            {
                return this.mTemplatedParent;
            }
        }

        /// <summary>
        /// Gets or sets the template child of the FrameworkElement.
        /// </summary>
        internal virtual UIElement TemplateChild
        {
            get
            {
                return this.mTemplateChild;
            }
            set
            {
                if ( value != this.mTemplateChild )
                {
                    this.RemoveVisualChild( this.mTemplateChild );
                    this.mTemplateChild = value;
                    this.AddVisualChild( this.mTemplateChild );
                }
            }
        }

        /// <summary>
        /// Gets or sets the template to apply to the element if any.
        /// </summary>
        internal virtual FrameworkTemplate TemplateInternal
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        /// <summary>
        /// Gets or sets the local defined resources.
        /// </summary>
        public ResourceDictionary Resources
        {
            get
            {
                return this.mResources;
            }
            set
            {
                this.mResources = value;
            }
        }

        /// <summary>
        /// Gets or sets the element X screen position.
        /// </summary>
        public int Left
        {
            get
            {
                return this.mLeft;
            }
            set
            {
                if ( this.mLeft != value )
                {
                    this.mLeft = value;
                    this.NotifyPropertyChanged();
                    this.RegisterForLayouting( LayoutingType.X );
                }
            }
        }

        /// <summary>
        /// Gets or sets the element Y screen position.
        /// </summary>
        public int Top
        {
            get
            {
                return this.mTop;
            }
            set
            {
                if ( this.mTop != value )
                {
                    this.mTop = value;
                    this.NotifyPropertyChanged();
                    this.RegisterForLayouting( LayoutingType.Y );
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the data context is null or not.
        /// </summary>
        internal bool IsLocalDataContextNull
        {
            get
            {
                return this.mDataContext == null;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the logical parent is null or not.
        /// </summary>
        internal bool IsLocalLogicalParentNull
        {
            get
            {
                return this.mLogicalParent == null;
            }
        }

        /// <summary>
        /// Gets or sets the visual element Tag.
        /// </summary>
        public object Tag
        {
            get
            {
                return this.mTag;
            }
            set
            {
                if ( this.mTag != value )
                {
                    this.mTag = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the visual element name. (Can be empty)
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
            set
            {
                if ( this.mName != value )
                {
                    this.mName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the logical parent.
        /// </summary>
        public AVisual LogicalParent
        {
            get
            {
                return this.mLogicalParent == null ? this.VisualParent : this.mLogicalParent;
            }
            internal set
            {
                if ( this.mLogicalParent != value )
                {
                    if ( this.mLogicalParent != null )
                    {
                        (this.mLogicalParent as FrameworkElement).DataContextChanged -= this.OnLogicalParentDataContextChanged;
                    }

                    PropertyValueChangedEventArgs lArgs = new PropertyValueChangedEventArgs( this.LogicalParent, value );
                    this.mLogicalParent = value;

                    if ( this.mLogicalParent != null )
                    {
                        (this.mLogicalParent as FrameworkElement).DataContextChanged += this.OnLogicalParentDataContextChanged;
                    }

                    this.OnLogicalParentChanged( this, lArgs );
                }
            }
        }

        /// <summary>
        /// Gets the set of logical children if any.
        /// </summary>
        protected internal virtual IEnumerator LogicalChildren
        {
            get
            {
                return null;
            }
        }
        
        /// <summary>
        /// Gets or sets the data context object.
        /// </summary>
        public object DataContext
        {
            get
            {
                return this.mDataContext == null ? this.mLogicalParent == null ? null : (this.mLogicalParent as FrameworkElement).DataContext : 
                                                                                         this.DataContext;
            }
            set
            {
                if ( this.mDataContext != value )
                {
                    PropertyValueChangedEventArgs lArgs = new PropertyValueChangedEventArgs( this.DataContext, value );

                    this.mDataContext = value;

                    this.OnDataContextChanged( this, lArgs );

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the element style.
        /// </summary>
        public virtual Ximl.Style Style
        {
            get
            {
                return this.mStyle;
            }
            set
            {
                if ( this.mStyle != value )
                {
                    this.mStyle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the Defaukt style key.
        /// </summary>
        protected internal string DefaultStyleKey
        {
            get
            {
                return this.mDefaultStyleKey;
            }
            set
            {
                if (this.mDefaultStyleKey == value)
                {
                    return;
                }

                this.mDefaultStyleKey = value;
                this.NotifyPropertyChanged();
                this.OnThemeStyleKeyChanged();
            }
        }

        /// <summary>
        /// Gets the theme style.
        /// </summary>
        internal Ximl.Style ThemeStyle
        {
            get
            {
                return this.mThemeStyle;
            }
        }

        /// <summary>
        /// Gets or sets the outer margin.
        /// </summary>
        public Thickness Margin
        {
            get
            {
                return this.mMargin;
            }
            set
            {
                if ( this.mMargin != value )
                {
                    this.mMargin = value;
                    this.NotifyPropertyChanged();
                    this.InvalidateArrange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the element width.
        /// </summary>
        public double Width
        {
            get
            {
                return this.mWidth;
            }
            set
            {
                if ( this.mWidth != value &&
                     value >= this.mMinWidth &&
                     value <= this.mMaxWidth )
                {
                    double lOldWidth = this.mWidth;

                    this.mWidth = value;
                    this.NotifyPropertyChanged();

                    this.OnWidthChanged( lOldWidth, this.mWidth );

                    this.RegisterForLayouting( LayoutingType.Width );
                }
            }
        }

        /// <summary>
        /// Gets or sets the element height.
        /// </summary>
        public double Height
        {
            get
            {
                return this.mHeight;
            }
            set
            {
                if ( this.mHeight != value &&
                     value >= this.mMinHeight &&
                     value <= this.mMaxHeight )
                {
                    double lOldHeight = this.mHeight;

                    this.mHeight = value;
                    this.NotifyPropertyChanged();

                    this.OnHeightChanged( lOldHeight, this.mHeight );

                    this.RegisterForLayouting( LayoutingType.Height );
                }
            }
        }

        /// <summary>
        /// Gets or sets the element minimum width.
        /// </summary>
        public double MinWidth
        {
            get
            {
                return this.mMinWidth;
            }
            set
            {
                if ( this.mMinWidth != value )
                {
                    this.mMinWidth = value;
                    this.NotifyPropertyChanged();
                    this.RegisterForLayouting( LayoutingType.Sizing );
                }
            }
        }

        /// <summary>
        /// Gets or sets the element minimum height.
        /// </summary>
        public double MinHeight
        {
            get
            {
                return this.mMinHeight;
            }
            set
            {
                if ( this.mMinHeight != value )
                {
                    this.mMinHeight = value;
                    this.NotifyPropertyChanged();
                    this.RegisterForLayouting( LayoutingType.Sizing );
                }
            }
        }

        /// <summary>
        /// Gets or sets the element maximum width.
        /// </summary>
        public double MaxWidth
        {
            get
            {
                return this.mMaxWidth;
            }
            set
            {
                if ( this.mMaxWidth != value )
                {
                    this.mMaxWidth = value;
                    this.NotifyPropertyChanged();
                    this.RegisterForLayouting( LayoutingType.Sizing );
                }
            }
        }

        /// <summary>
        /// Gets or sets the element maximum height.
        /// </summary>
        public double MaxHeight
        {
            get
            {
                return this.mMaxHeight;
            }
            set
            {
                if ( this.mMaxHeight != value )
                {
                    this.mMaxHeight = value;
                    this.NotifyPropertyChanged();
                    this.RegisterForLayouting( LayoutingType.Sizing );
                }
            }
        }

        /// <summary>
        /// Gets the client region.
        /// </summary>
        public override Rectangle ClientRectangle
        {
            get
            {
                Rectangle lClient = base.ClientRectangle;
                lClient.Inflate( -(int)this.Margin.Max );
                return lClient;
            }
        }

        /// <summary>
        /// Gets or sets the element vertical alignment.
        /// </summary>
        public VerticalAlignment VerticalAlignment
        {
            get
            {
                return this.mVerticalAlignment;
            }
            set
            {
                if ( this.mVerticalAlignment != value )
                {
                    this.mVerticalAlignment = value;
                    this.NotifyPropertyChanged();
                    this.RegisterForLayouting( LayoutingType.Y );
                }
            }
        }

        /// <summary>
        /// Gets or sets the element horizontal alignment.
        /// </summary>
        public HorizontalAlignment HorizontalAlignment
        {
            get
            {
                return this.mHorizontalAlignment;
            }
            set
            {
                if ( this.mHorizontalAlignment != value )
                {
                    this.mHorizontalAlignment = value;
                    this.NotifyPropertyChanged();
                    this.RegisterForLayouting( LayoutingType.X );
                }
            }
        }

        /// <summary>
        /// Gets the visual children count.
        /// </summary>
        public override int VisualChildrenCount
        {
            get
            {
                return this.mTemplateChild != null ? 1 : 0;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameworkElement"/> class.
        /// </summary>
        public FrameworkElement()
        {
            this.mHasTemplateGeneratedVisualTree = false;
            this.mVerticalAlignment   = VerticalAlignment.Center;
            this.mHorizontalAlignment = HorizontalAlignment.Center;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes visual specificities.
        /// </summary>
        protected override void Initialize()
        {
            

            base.Initialize();
        }

        /// <summary>
        /// Applies the template along the visual tree elements.
        /// </summary>
        /// <returns></returns>
        public bool ApplyTemplate()
        {
            this.OnPreApplyTemplate();

            bool lResult = false;

            FrameworkTemplate lTemplate = this.TemplateInternal;

            if ( this.mHasTemplateGeneratedVisualTree == false )
            {
                // Get the context to pass..
                object lContext = this.DataContext;

                // Create the Visual tree from the template definition.
                lResult = lTemplate.ApplyTemplateContent( this, lContext );
                if ( lResult )
                {
                    this.mHasTemplateGeneratedVisualTree = true;

                    // Process potential actions (e.g: triggers and so on).

                    this.OnApplyTemplate();
                }
            }
            
            this.OnPostApplyTemplate();

            return lResult;
        }

        /// <summary>
        /// Delegate called before a template be applied.
        /// </summary>
        internal virtual void OnPreApplyTemplate()
        {

        }

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public virtual void OnApplyTemplate()
        {

        }

        /// <summary>
        /// Delegate called after a template be applied.
        /// </summary>
        internal virtual void OnPostApplyTemplate()
        {

        }

        /// <summary>
        /// Delegate called on style key changed.
        /// </summary>
        private void OnThemeStyleKeyChanged()
        {

        }

        /// <summary>
        /// Update the theme style property.
        /// </summary>
        internal void UpdateThemeStyleProperty()
        {
            if ( this.mIsThemeStyleUpdateInProgress == false )
            {
                this.mIsThemeStyleUpdateInProgress = true;
                try
                {
                    StyleHelper.GetThemeStyle( this );
                    
                    OnThemeChanged();
                }
                finally
                {
                    this.mIsThemeStyleUpdateInProgress = false;
                }
            }
            else
            {
                throw new InvalidOperationException( "Attempting to GetThemeStyle a second time while processing it." );
            }
        }

        /// <summary>
        /// Delegate called on theme changed.
        /// </summary>
        internal virtual void OnThemeChanged()
        {

        }

        /// <summary>
        /// Delegate called 
        /// </summary>
        /// <param name="pOldValue"></param>
        /// <param name="pNewValue"></param>
        internal void OnThemeStyleChanged(Ximl.Style pOldValue, Ximl.Style pNewValue)
        {
            StyleHelper.UpdateThemeStyleCache( this, pOldValue, pNewValue, ref this.mThemeStyle );
        }

        /// <summary>
        /// Looks for a resource using its key.
        /// </summary>
        /// <param name="pResourceKey"></param>
        /// <returns></returns>
        public object TryFindResource(string pResourceKey)
        {
            if ( string.IsNullOrEmpty( pResourceKey ) )
            {
                throw new ArgumentNullException("pResourceKey");
            }

            object lResource = FrameworkElement.FindResourceInternal( this, pResourceKey );
            
            return lResource;
        }

        /// <summary>
        /// Finds a resource given its key.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pResourceKey"></param>
        /// <returns></returns>
        internal static object FindResourceInternal(FrameworkElement pElement, string pResourceKey)
        {
            object lUnused;
            return FindResourceInternal( pElement,
                                         pResourceKey,
                                         null,   // pUnlinkedParent,
                                         null,   // pBoundaryElement,
                                         out lUnused );
        }

        /// <summary>
        /// Finds the resources having the given key.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pResourceKey"></param>
        /// <param name="pUnlinkedParent"></param>
        /// <param name="pBoundaryElement"></param>
        /// <param name="pSource"></param>
        /// <returns></returns>
        internal static object FindResourceInternal(FrameworkElement pElement, string pResourceKey, object pUnlinkedParent, AVisual pBoundaryElement, out object pSource)
        {
            object lValue;
            
            try
            {

                // First try to find the resource in the tree
                if ( pElement != null || 
                     pUnlinkedParent != null )
                {
                    lValue = FindResourceInTree( pElement, pResourceKey, pUnlinkedParent, pBoundaryElement, out pSource);
                    if ( lValue != null )
                    {
                        return lValue;
                    }

                }

                // Then we try to find the resource in the App's Resources
                //Application app = Application.Current;
                //if (app != null &&
                //    (inheritanceBehavior == InheritanceBehavior.Default ||
                //     inheritanceBehavior == InheritanceBehavior.SkipToAppNow ||
                //     inheritanceBehavior == InheritanceBehavior.SkipToAppNext))
                //{
                //    lValue = app.FindResourceInternal(pResourceKey, allowDeferredResourceReference, mustReturnDeferredResourceReference);
                //    if ( lValue != null )
                //    {
                //        pSource = app;
                        
                //        return lValue;
                //    }
                //}
                
            }
            finally
            {
                
            }
            
            pSource = null;
            return null;
        }

        /// <summary>
        /// Finds a resource through the element tree.
        /// </summary>
        /// <param name="pElement"></param>
        /// <param name="pResourceKey"></param>
        /// <param name="pUnlinkedParent"></param>
        /// <param name="pBoundaryElement"></param>
        /// <param name="pSource"></param>
        /// <returns></returns>
        internal static object FindResourceInTree(FrameworkElement pElement, string pResourceKey, object pUnlinkedParent, AVisual pBoundaryElement, out object pSource)
        {
            FrameworkElement lStartNode = pElement;
            FrameworkElement lCurrent = lStartNode;
            object lValue;
            Ximl.Style lStyle;
            FrameworkTemplate lFrameworkTemplate;
            Ximl.Style lThemeStyle;
            int lLoopCount = 0;
            bool lHasParent = true;

            while ( lHasParent )
            {
                Debug.Assert( lStartNode != null || pUnlinkedParent != null,
                              "Don't call FindResource with a null pElement and pUnlinkedParent");

                if ( lLoopCount > 5 /*ContextLayoutManager.s_LayoutRecursionLimit*/)
                {
                    // We suspect a loop here because the loop count
                    // has exceeded the MAX_TREE_DEPTH expected
                    throw new InvalidOperationException( "Possible Overflow du to max recursion count reached." );
                }
                else
                {
                    lLoopCount++;
                }

                // -------------------------------------------
                //  Lookup ResourceDictionary on the current instance
                // -------------------------------------------

                lStyle = null;
                lFrameworkTemplate = null;
                lThemeStyle = null;

                lValue = pElement.FindResourceOnSelf( pResourceKey );
                if ( lValue != null )
                {
                    pSource = lCurrent;
                    
                    return lValue;
                }

                if ( lCurrent != lStartNode )
                {
                    lStyle = lCurrent.Style;
                }

                // Fetch the Template
                if ( lCurrent != lStartNode )
                {
                    lFrameworkTemplate = lCurrent.TemplateInternal;
                }

                // Fetch the ThemeStyle
                if ( lCurrent != lStartNode )
                {
                    lThemeStyle = lCurrent.ThemeStyle;
                }

                if ( lStyle != null )
                {
                    lValue = lStyle.FindResource( pResourceKey );
                    if ( lValue != null )
                    {
                        pSource = lStyle;
                        
                        return lValue;
                    }
                }

                if ( lFrameworkTemplate != null )
                {
                    lValue = lFrameworkTemplate.FindResource( pResourceKey );
                    if ( lValue != null )
                    {
                        pSource = lFrameworkTemplate;
                        
                        return lValue;
                    }
                }

                if ( lThemeStyle != null )
                {
                    lValue = lThemeStyle.FindResource( pResourceKey );
                    if ( lValue != null )
                    {
                        pSource = lThemeStyle;
                        
                        return lValue;
                    }
                }


                // If the current element that has been searched is the boundary element
                // then we need to progress no further
                if ( pBoundaryElement != null && 
                     lCurrent == pBoundaryElement )
                {
                    break;
                }

                //// If the current element for resource lookup is marked such
                //// then skip to the Application and/or System resources
                //if ( lCurrent != null && 
                //     TreeWalkHelper.SkipNext(lCurrent.InheritanceBehavior))
                //{
                //    inheritanceBehavior = lCurrent.InheritanceBehavior;
                //    break;
                //}

                // -------------------------------------------
                //  Find the next parent instance to lookup
                // -------------------------------------------

                if ( pUnlinkedParent != null )
                {
                    // This is for the special case when the parser tries to fetch
                    // a resource on an element even before it is hooked to the
                    // tree. In this case the parser passes us the pUnlinkedParent
                    // to use it for resource lookup.
                    FrameworkElement lUnlinkedParentAsFE = pUnlinkedParent as FrameworkElement;
                    if ( lUnlinkedParentAsFE != null )
                    {
                        lCurrent = lUnlinkedParentAsFE;
                        if ( lCurrent != null )
                        {
                            lHasParent = true;
                        }
                        else
                        {
                            FrameworkElement lFEParent = lUnlinkedParentAsFE.mLogicalParent as FrameworkElement;
                            if ( lFEParent != null )
                            {
                                lCurrent = lFEParent;
                                lHasParent = true;
                            }
                            else
                            {
                                lHasParent = false;
                            }
                        }
                    }
                    else
                    {
                        lHasParent = false;
                    }

                    pUnlinkedParent = null;
                }
                else
                {
                    Debug.Assert( lCurrent != null,
                                  "The current node being processed should be a FrameworkElement");

                    lCurrent = lCurrent.mLogicalParent as FrameworkElement;

                    lHasParent = lCurrent != null;
                }

                //// If the current element for resource lookup is marked such
                //// then skip to the Application and/or System resources
                //if ( lCurrent != null && 
                //     TreeWalkHelper.SkipNow(lCurrent.InheritanceBehavior))
                //{
                //    inheritanceBehavior = lCurrent.InheritanceBehavior;
                //    break;
                //}
            }

            // No matching resource was found in the tree
            pSource = null;
            return null;
        }

        /// <summary>
        /// Finds a resource having the given key on this element.
        /// </summary>
        /// <param name="pResourceKey"></param>
        /// <returns></returns>
        internal object FindResourceOnSelf(string pResourceKey)
        {
            IXimlResource lResource;
            ResourceDictionary lResources = this.Resources;
            if ( lResources != null &&
                 lResources.TryGetValue( pResourceKey, out lResource ) )
            {
                return lResource;
            }

            return null;
        }

        /// <summary>
        /// Retrieves the template child
        /// </summary>
        /// <param name="pName">The child name to look for.</param>
        /// <returns></returns>
        protected internal AVisual GetTemplateChild(string pName)
        {
            FrameworkTemplate lTemplate = this.TemplateInternal;
            if ( lTemplate != null )
            {
                return null;
            }

            return StyleHelper.FindNameInTemplateContent( pName, lTemplate ) as AVisual;
        }

        /// <summary>
        /// Gets the visual child at the given index if any.
        /// </summary>
        /// <param name="pIndex"></param>
        /// <returns></returns>
        protected override AVisual GetVisualChild(int pIndex)
        {
            if ( this.mTemplateChild == null)
            {
                throw new ArgumentOutOfRangeException("pIndex", pIndex, "No visual child");
            }

            if ( pIndex != 0 )
            {
                throw new ArgumentOutOfRangeException("pIndex", pIndex, "Out of range index.");
            }

            return this.mTemplateChild;
        }

        /// <summary>
        /// Internal invalidates arrange method.
        /// </summary>
        protected override void InternalInvalidateArrange()
        {
            if ( this.mHorizontalAlignment == HorizontalAlignment.Stretch || 
                 this.mVerticalAlignment   == VerticalAlignment.Stretch )
            {
                this.RegisterForLayouting( LayoutingType.Sizing );
            }
            else
            {
                base.InternalInvalidateArrange();
            }
        }

        /// <summary>
        /// Registers a layout part to update.
        /// </summary>
        /// <param name="pLayoutType">The layout part to register for update.</param>
        protected internal override void RegisterForLayouting(LayoutingType pLayoutType)
        {
            if ( this.VisualParent == null )
            {
                return;
            }

            lock ( ContextLayoutManager.From( this.Dispatcher ).LayoutMutex )
            {
                // Dont set position for stretched item
                if ( this.mHorizontalAlignment == HorizontalAlignment.Stretch )
                {
                    pLayoutType &= (~LayoutingType.X);
                }

                if ( this.mVerticalAlignment == VerticalAlignment.Stretch )
                {
                    pLayoutType &= (~LayoutingType.Y);
                }

                //if ( !ArrangeChildren )
                //{
                //    pLayoutType &= (~LayoutingType.ArrangeChildren);
                //}
            }

            base.RegisterForLayouting( pLayoutType );
        }

        /// <summary>
        /// Delegate called on width changes.
        /// </summary>
        /// <param name="pOldWidth">The previous width.</param>
        /// <param name="pNewWidth">The new width.</param>
        protected virtual void OnWidthChanged(double pOldWidth, double pNewWidth)
        {
            // Override.

            ////contentSize in Stacks are only update on childLayoutChange, and the single stretched
            ////child of the stack is not counted in contentSize, so when changing size policy of a child
            ////we should adapt contentSize
            ////TODO:check case when child become stretched, and another stretched item already exists.
            //if (parent is GenericStack)
            //{//TODO:check if I should test Group instead
            //    if ((parent as GenericStack).Orientation == Orientation.Horizontal)
            //    {
            //        if (lastWP == Measure.Fit)
            //            (parent as GenericStack).contentSize.Width -= this.LastSlots.Width;
            //        else
            //            (parent as GenericStack).contentSize.Width += this.LastSlots.Width;
            //    }
            //}
        }

        /// <summary>
        /// Delegate called on height changes.
        /// </summary>
        /// <param name="pOldHeight">The previous height.</param>
        /// <param name="pNewHeight">The new height.</param>
        protected virtual void OnHeightChanged(double pOldHeight, double pNewHeight)
        {
            // Override.

            //if (parent is GenericStack)
            //{
            //    if ((parent as GenericStack).Orientation == Orientation.Vertical)
            //    {
            //        if (lastHP == Measure.Fit)
            //            (parent as GenericStack).contentSize.Height -= this.LastSlots.Height;
            //        else
            //            (parent as GenericStack).contentSize.Height += this.LastSlots.Height;
            //    }
            //}
        }
        
        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            UIElement lParentElement = this.VisualParent as UIElement;

            Size lRequestedSize = new Size();
            if ( this.mWidth  == cFitSizeValue ||
                 this.mHeight == cFitSizeValue )
            {
                lRequestedSize = this.Measure();
            }

            switch ( pLayoutType )
            {
                case LayoutingType.X:
                    if ( this.Left == 0 )
                    {

                        if ( lParentElement.State.HasFlag(LayoutingType.Width) ||
                             this.State.HasFlag(LayoutingType.Width) )
                        {
                            return this.mSlot;
                        }

                        switch ( this.mHorizontalAlignment )
                        {
                            case HorizontalAlignment.Left:
                                this.mSlot.X = 0;
                                break;
                            case HorizontalAlignment.Right:
                                this.mSlot.X = lParentElement.ClientRectangle.Width - this.mSlot.Width;
                                break;
                            case HorizontalAlignment.Center:
                                this.mSlot.X = lParentElement.ClientRectangle.Width / 2 - this.mSlot.Width / 2;
                                break;
                        }
                    }
                    else
                    {
                        this.mSlot.X = this.Left;
                    }

                    if ( this.mLastSlots.X == this.mSlot.X )
                    {
                        break;
                    }

                    this.mIsDirty = true;

                    this.OnLayoutChanges( pLayoutType );

                    this.mLastSlots.X = this.mSlot.X;
                    break;
                case LayoutingType.Y:
                    if ( this.Top == 0 )
                    {

                        if ( lParentElement.State.HasFlag(LayoutingType.Height) ||
                             this.State.HasFlag(LayoutingType.Height) )
                        {
                            return this.mSlot;
                        }

                        switch ( this.mVerticalAlignment )
                        {
                            case VerticalAlignment.Top://this could be processed even if parent Height is not known
                                this.mSlot.Y = 0;
                                break;
                            case VerticalAlignment.Bottom:
                                this.mSlot.Y = lParentElement.ClientRectangle.Height - this.mSlot.Height;
                                break;
                            case VerticalAlignment.Center:
                                this.mSlot.Y = lParentElement.ClientRectangle.Height / 2 - this.mSlot.Height / 2;
                                break;
                        }
                    }
                    else
                    {
                        this.mSlot.Y = this.Top;
                    }

                    if ( this.mLastSlots.Y == this.mSlot.Y )
                    {
                        break;
                    }

                    this.mIsDirty = true;

                    this.OnLayoutChanges( pLayoutType );

                    this.mLastSlots.Y = this.mSlot.Y;
                    break;
                case LayoutingType.Width:
                    if ( this.mIsVisible )
                    {
                        if ( this.mWidth > 0 )
                        {
                            this.mSlot.Width = (int)this.mWidth;
                        }
                        else if ( this.mWidth == cFitSizeValue )
                        {
                            this.mSlot.Width = lRequestedSize.Width;
                        }
                        else if ( lParentElement.State.HasFlag(LayoutingType.Width) )
                        {
                            return this.mSlot;
                        }
                        else if ( this.mHorizontalAlignment == HorizontalAlignment.Stretch )
                        {
                            this.mSlot.Width = this.VisualParent.ClientRectangle.Width;
                        }
                        else
                        {
                            this.mSlot.Width = (int)Math.Round((double)(this.VisualParent.ClientRectangle.Width * Width) / 100.0);
                        }

                        if ( this.mSlot.Width < 0 )
                        {
                            return this.mSlot;
                        }

                        // Size constrain
                        if ( this.mSlot.Width < this.mMinWidth )
                        {
                            this.mSlot.Width = (int)this.mMinWidth;
                        }
                        else if ( this.mSlot.Width > this.mMaxWidth && 
                                  this.mMaxWidth > 0 )
                        {
                            this.mSlot.Width = (int)this.mMaxWidth;
                        }
                    }
                    else
                    {
                        this.mSlot.Width = 0;
                    }

                    if ( this.mLastSlots.Width == this.mSlot.Width )
                    {
                        break;
                    }

                    this.mIsDirty = true;

                    this.OnLayoutChanges( pLayoutType );

                    this.mLastSlots.Width = this.mSlot.Width;
                    break;
                case LayoutingType.Height:
                    if ( this.mIsVisible )
                    {
                        if ( this.mHeight > 0 )
                        {
                            this.mSlot.Height = (int)this.mHeight;
                        }
                        else if ( this.mHeight == cFitSizeValue )
                        {
                            this.mSlot.Height = lRequestedSize.Height;
                        }
                        else if ( lParentElement.State.HasFlag(LayoutingType.Height) )
                        {
                            return this.mSlot;
                        }
                        else if ( this.mVerticalAlignment == VerticalAlignment.Stretch )
                        {
                            this.mSlot.Height = this.VisualParent.ClientRectangle.Height;
                        }
                        else
                        {
                            this.mSlot.Height = (int)Math.Round((double)(this.VisualParent.ClientRectangle.Height * Height) / 100.0);
                        }

                        if ( this.mSlot.Height < 0 )
                        {
                            return this.mSlot;
                        }

                        // Size constrain
                        if ( this.mSlot.Height < this.mMinHeight )
                        {
                            this.mSlot.Height = (int)this.mMinHeight;
                        }
                        else if ( this.mSlot.Height > this.mMaxHeight &&
                                  this.mMaxHeight > 0 )
                        {
                            this.mSlot.Height = (int)this.mMaxHeight;
                        }
                    }
                    else
                    {
                        this.mSlot.Height = 0;
                    }

                    if ( this.mLastSlots.Height == this.mSlot.Height )
                    {
                        break;
                    }

                    this.mIsDirty = true;

                    this.OnLayoutChanges( pLayoutType );

                    this.mLastSlots.Height = this.mSlot.Height;
                    break;
            }
            
            return base.ArrangeCore( pLayoutType );
        }
        
        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
		protected sealed override Size MeasureCore()
        {
            // Build the visual tree first from its style.
            this.ApplyTemplate();

            Size lDesiredSize = this.MeasureOverride();

            return lDesiredSize;
        }

        /// <summary>
        /// Overridable size measure process.
        /// </summary>
        /// <returns></returns>
        protected virtual Size MeasureOverride()
        {
            return new Size( (int)(this.mContentSize.Width + this.mMargin.Left + this.mMargin.Right),
                             (int)(this.mContentSize.Height + this.mMargin.Top + this.mMargin.Bottom) );
        }

        /// <summary>
        /// Adds the given logical child to this element.
        /// </summary>
        /// <param name="pChild"></param>
        protected internal void AddLogicalChild(object pChild)
        {
            if ( pChild != null )
            {
                this.HasLogicalChildren = true;

                // Child is present; reparent him to this element
                FrameworkElement lChild = pChild as FrameworkElement;
                if ( lChild != null )
                {
                    lChild.LogicalParent = this;
                }
            }
        }
        
        /// <summary>
        /// Removes the given logical child to this element.
        /// </summary>
        /// <param name="pChild"></param>
        protected internal void RemoveLogicalChild(object pChild)
        {
            if ( pChild != null )
            {
                // Child is present
                FrameworkElement lChild = pChild as FrameworkElement;
                if ( lChild != null )
                {
                    lChild.LogicalParent = null;
                }

                // This could have been the last child, so check if we have any more children
                IEnumerator lChildren = this.LogicalChildren;

                // if null, there are no children.
                if ( lChildren == null )
                {
                    this.HasLogicalChildren = false;
                }
                else
                {
                    // If we can move next, there is at least one child
                    this.HasLogicalChildren = lChildren.MoveNext();
                }
            }
        }

        /// <summary>
        /// Delegate called on visual parent changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected override void OnVisualParentChanged(PropertyValueChangedEventArgs pEventArgs)
        {
            base.OnVisualParentChanged( pEventArgs );

            if ( this.mLogicalParent == null )
            {
                this.OnLogicalParentChanged( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on logical parent changes.
        /// </summary>
        /// <param name="pSender">The sender (this)</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnLogicalParentChanged(object pSender, PropertyValueChangedEventArgs pEventArgs)
        {
            if ( this.LogicalParentChanged != null )
            {
                this.LogicalParentChanged.Raise( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on logical parent data context changed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected virtual void OnLogicalParentDataContextChanged(object pSender, PropertyValueChangedEventArgs pEventArgs)
        {
            if ( this.IsLocalDataContextNull )
            {
                this.OnDataContextChanged( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on Data context changed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        public virtual void OnDataContextChanged(object pSender, PropertyValueChangedEventArgs pEventArgs)
        {
            if ( this.DataContextChanged != null )
            {
                this.DataContextChanged.Raise( this, pEventArgs );
            }
        }

        /// <summary>
        /// Finds the visual having the given name
        /// </summary>
        /// <param name="pName">The name to look for.</param>
        /// <returns></returns>
		public virtual AVisual FindByName(string pName)
        {
            return string.Equals( pName, this.mName, StringComparison.Ordinal ) ? this : null;
        }

        /// <summary>
        /// Clone the visual element.
        /// </summary>
        /// <returns>The clone.</returns>
        protected override AVisual InternalClone()
        {
            FrameworkElement lClone = new FrameworkElement();
            lClone.mName = this.mName != null ? this.mName.Clone() as string : null;
            lClone.mTag  = this.mTag is ICloneable ? (this.mTag as ICloneable).Clone() : null;
            lClone.mVerticalAlignment   = this.mVerticalAlignment;
            lClone.mHorizontalAlignment = this.mHorizontalAlignment;
            lClone.mMargin = this.mMargin;
            return lClone;
        }

        #endregion Methods
    }
}
