﻿using System.Collections;
using OrcNet.UI.Ximl;
using OrcNet.UI.Enumerators;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="HeaderedContentControl"/> class.
    /// </summary>
    public class HeaderedContentControl : ContentControl
    {
        #region Fields

        /// <summary>
        /// Stores the header object.
        /// </summary>
        private object mHeader;

        /// <summary>
        /// Stores the header template.
        /// </summary>
        private DataTemplate mHeaderTemplate;

        /// <summary>
        /// Stores the header data template selector.
        /// </summary>
        private DataTemplateSelector mHeaderTemplateSelector;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the header template.
        /// </summary>
        public DataTemplate HeaderTemplate
        {
            get
            {
                return this.mHeaderTemplate;
            }
            set
            {
                DataTemplate lOldTemplate = this.mHeaderTemplate;

                this.mHeaderTemplate = value;

                this.OnHeaderTemplateChanged( this.mHeaderTemplate, lOldTemplate );

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the header data template selector.
        /// </summary>
        public DataTemplateSelector HeaderTemplateSelector
        {
            get
            {
                return this.mHeaderTemplateSelector;
            }
            set
            {
                DataTemplateSelector lOldSelector = this.mHeaderTemplateSelector;

                this.mHeaderTemplateSelector = value;

                this.OnHeaderTemplateSelectorChanged( this.mHeaderTemplateSelector, lOldSelector );

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the flag indicating whether a header is present or not.
        /// </summary>
        public bool HasHeader
        {
            get
            {
                return this.Header != null;
            }
        }

        /// <summary>
        /// Gets or sets the header object.
        /// </summary>
        public object Header
        {
            get
            {
                return this.mHeader;
            }
            set
            {
                object lOldHeader = this.mHeader;

                this.mHeader = value;

                this.OnHeaderChanged( this.mHeader, lOldHeader );

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the set of logical children if any.
        /// </summary>
        protected internal override IEnumerator LogicalChildren
        {
            get
            {
                object lHeader = this.Header;

                if ( lHeader == null )
                {
                    return base.LogicalChildren;
                }

                return new HeaderedContentModelTreeEnumerator( this, this.Content, lHeader );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="HeaderedContentControl"/> class.
        /// </summary>
        public HeaderedContentControl()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on header changes.
        /// </summary>
        /// <param name="pNewHeader"></param>
        /// <param name="pOldHeader"></param>
        protected virtual void OnHeaderChanged(object pNewHeader, object pOldHeader)
        {
            this.RemoveLogicalChild( pOldHeader );
            this.AddLogicalChild( pNewHeader );
        }

        /// <summary>
        /// Delegate called on header data template changes.
        /// </summary>
        /// <param name="pNewTemplate"></param>
        /// <param name="pOldTemplate"></param>
        protected virtual void OnHeaderTemplateChanged(DataTemplate pNewTemplate, DataTemplate pOldTemplate)
        {

        }

        /// <summary>
        /// Delegate called on header data template selector changes.
        /// </summary>
        /// <param name="pNewSelector"></param>
        /// <param name="pOldSelector"></param>
        protected virtual void OnHeaderTemplateSelectorChanged(DataTemplateSelector pNewSelector, DataTemplateSelector pOldSelector)
        {

        }

        #endregion Methods
    }
}
