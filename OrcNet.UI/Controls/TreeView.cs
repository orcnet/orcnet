﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TreeView"/> class.
    /// </summary>
    public class TreeView : ItemsControl
	{
        #region Fields

        /// <summary>
        /// Stores the selected item's value.
        /// </summary>
        private object mSelectedValue;

        /// <summary>
        /// Stores the seleted item.
        /// </summary>
        private object mSelectedItem;

        /// <summary>
        /// Stores the selected item container.
        /// </summary>
        private TreeViewItem mSelectedContainer;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on selected item changes.
        /// </summary>
        public event EventHandler<SelectionChangeEventArgs<object>> SelectedItemChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the selected item.
        /// </summary>
        public object SelectedItem
        {
            get
            {
                return this.mSelectedItem;
            }
            private set
            {
                if ( this.mSelectedItem != value )
                {
                    this.mSelectedItem = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the selected item's value.
        /// </summary>
        public object SelectedValue
        {
            get
            {
                return this.mSelectedValue;
            }
            private set
            {
                if ( this.mSelectedValue != value )
                {
                    this.mSelectedValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeView"/> class.
        /// </summary>
        public TreeView() :
        base()
		{

		}

        #endregion Constructor
        
        #region Methods

        /// <summary>
        /// Delegate called on element got focus event.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnGotFocus(object pSender, EventArgs pEventArgs)
        {
            base.OnGotFocus( pSender, pEventArgs );

            if ( this.mSelectedContainer != null &&
                 this.mSelectedContainer.IsFocused == false )
            {
                this.mSelectedContainer.Focus();
            }
        }

        /// <summary>
        /// Expands the given child item.
        /// </summary>
        /// <param name="pContainer"></param>
        /// <returns></returns>
        protected virtual bool ExpandSubtree(TreeViewItem pContainer)
        {
            if ( pContainer != null )
            {
                pContainer.ExpandSubtree();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Changes the selection in the tree view.
        /// </summary>
        /// <param name="pData">The involved data.</param>
        /// <param name="pContainer">The involved item container.</param>
        /// <param name="pIsSelected">The selection state.</param>
        internal void ChangeSelection(object pData, TreeViewItem pContainer, bool pIsSelected)
        {
            object lOldValue = null;
            object lNewValue = null;
            bool lHasChanged = false;

            if ( pIsSelected )
            {
                if ( pContainer != this.mSelectedContainer )
                {
                    lOldValue = this.SelectedItem;
                    lNewValue = pData;

                    if ( this.mSelectedContainer != null )
                    {
                        this.mSelectedContainer.IsSelected = false;
                        this.mSelectedContainer.UpdateContainsSelection( false );
                    }

                    this.mSelectedContainer = pContainer;
                    this.mSelectedContainer.UpdateContainsSelection( true );
                    this.SelectedItem = pData;
                    this.UpdateSelectedValue( pData );
                    lHasChanged = true;
                }
            }
            else
            {
                if ( pContainer == this.mSelectedContainer )
                {
                    this.mSelectedContainer.UpdateContainsSelection( false );
                    this.mSelectedContainer = null;
                    this.SelectedItem = null;
                    this.UpdateSelectedValue( null );

                    lOldValue = pData;
                    lHasChanged = true;
                }
            }

            if ( pContainer.IsSelected != pIsSelected )
            {
                pContainer.IsSelected = pIsSelected;
            }

            if ( lHasChanged )
            {
                this.OnSelectedItemChanged( new SelectionChangeEventArgs<object>( lNewValue, lOldValue ) );
            }
        }

        /// <summary>
        /// Updates the selected item's value.
        /// </summary>
        /// <param name="pSelectedItem"></param>
        private void UpdateSelectedValue(object pSelectedItem)
        {
            //BindingExpression expression = PrepareSelectedValuePathBindingExpression(selectedItem);

            //if (expression != null)
            //{
            //    expression.Activate(selectedItem);
            //    object selectedValue = expression.Value;
            //    expression.Deactivate();

            //    SetValue(SelectedValuePropertyKey, selectedValue);
            //}
            //else
            //{
            //    ClearValue(SelectedValuePropertyKey);
            //}

            // For now, but then will extract through bindings if any.
            if ( pSelectedItem is FrameworkElement )
            {
                this.SelectedValue = (pSelectedItem as FrameworkElement).DataContext;
            }
            else
            {
                this.SelectedValue = null;
            }
        }

        /// <summary>
        /// Delegate called on selected item changes.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected virtual void OnSelectedItemChanged(SelectionChangeEventArgs<object> pEventArgs)
        {
            if ( this.SelectedItemChanged != null )
            {
                this.SelectedItemChanged( this, pEventArgs );
            }
        }

        #endregion Methods
    }
}

