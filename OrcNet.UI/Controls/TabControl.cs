﻿using OrcNet.UI.Ximl;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TabControl"/> class.
    /// </summary>
    public class TabControl : Selector
    {
        #region Fields

        /// <summary>
        /// Stores the tab header alignment according to content.
        /// </summary>
        private Dock mTabStripPlacement;

        /// <summary>
        /// Stores the currently selected item content.
        /// </summary>
        private object mSelectedContent;

        /// <summary>
        /// Stores the content template to apply to tabitems that dont have their ContentTemplate.
        /// </summary>
        private DataTemplate mContentTemplate;

        /// <summary>
        /// Stores the content template selector allowing users to provides a custom style selection to tabitems.
        /// </summary>
        private DataTemplateSelector mContentTemplateSelector;

        /// <summary>
        /// Stores the currently selected tabitem content template.
        /// </summary>
        private DataTemplate mSelectedContentTemplate;

        /// <summary>
        /// Stores the curently selected tabitem content template selector.
        /// </summary>
        private DataTemplateSelector mSelectedContentTemplateSelector;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the tab header alignment according to content.
        /// </summary>
        public Dock TabStripPlacement
        {
            get
            {
                return this.mTabStripPlacement;
            }
            set
            {
                if ( this.mTabStripPlacement == value )
                {
                    return;
                }

                this.mTabStripPlacement = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the currently selected item content.
        /// </summary>
        public object SelectedContent
        {
            get
            {
                return this.mSelectedContent;
            }
            internal set
            {
                if ( this.mSelectedContent == value )
                {
                    return;
                }

                this.mSelectedContent = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the content template to apply to tabitems that dont have their ContentTemplate.
        /// </summary>
        public DataTemplate ContentTemplate
        {
            get
            {
                return this.mContentTemplate;
            }
            set
            {
                if ( this.mContentTemplate == value )
                {
                    return;
                }

                this.mContentTemplate = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the content template selector allowing users to provides a custom style selection to tabitems.
        /// </summary>
        public DataTemplateSelector ContentTemplateSelector
        {
            get
            {
                return this.mContentTemplateSelector;
            }
            set
            {
                if ( this.mContentTemplateSelector == value )
                {
                    return;
                }

                this.mContentTemplateSelector = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the currently selected tabitem content template.
        /// </summary>
        public DataTemplate SelectedContentTemplate
        {
            get
            {
                return this.mSelectedContentTemplate;
            }
            internal set
            {
                if ( this.mSelectedContentTemplate == value )
                {
                    return;
                }

                this.mSelectedContentTemplate = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the curently selected tabitem content template selector.
        /// </summary>
        public DataTemplateSelector SelectedContentTemplateSelector
        {
            get
            {
                return this.mSelectedContentTemplateSelector;
            }
            internal set
            {
                if ( this.mSelectedContentTemplateSelector == value )
                {
                    return;
                }

                this.mSelectedContentTemplateSelector = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TabControl"/> class.
        /// </summary>
        public TabControl() : 
        base()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.UpdateSelectedContent();
        }

        /// <summary>
        /// Delegate called on selection changes.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected override void OnSelectionChanged(object pSender, SelectionChangeEventArgs<object> pEventArgs)
        {
            this.UpdateSelectedContent();

            TabItem lItem = this.GetSelectedTabItem();
            if ( lItem != null &&
                 lItem.IsFocused == false )
            {
                lItem.SetFocus();
            }

            base.OnSelectionChanged( pSender, pEventArgs );
        }

        /// <summary>
        /// Gets the selected tabitem.
        /// </summary>
        /// <returns></returns>
        private TabItem GetSelectedTabItem()
        {
            object lSelectedItem = this.SelectedItem;
            if ( lSelectedItem != null )
            {
                // TO DO: If not a TabItem, wrap it into a new one.
                return lSelectedItem as TabItem;
            }

            return null;
        }

        /// <summary>
        /// Updates 
        /// </summary>
        private void UpdateSelectedContent()
        {
            if ( this.SelectedIndex < 0 )
            {
                this.SelectedContent = null;
                this.SelectedContentTemplate = null;
                this.SelectedContentTemplateSelector = null;
                return;
            }

            TabItem lSelectedTabItem = this.GetSelectedTabItem();
            if ( lSelectedTabItem != null )
            {
                //FrameworkElement lVisualParent = VisualTreeHelper.GetParent( lSelectedTabItem ) as FrameworkElement;
                //if ( lVisualParent != null )
                //{
                //    KeyboardNavigation.SetTabOnceActiveElement(lVisualParent, lSelectedTabItem);
                //    KeyboardNavigation.SetTabOnceActiveElement(this, lVisualParent);
                //}

                this.SelectedContent = lSelectedTabItem.Content;
                

                // Use lSelectedTabItem's template or selector if specified, otherwise use TabControl's
                if ( lSelectedTabItem.ContentTemplate != null || 
                     lSelectedTabItem.ContentTemplateSelector != null )
                {
                    this.SelectedContentTemplate = lSelectedTabItem.ContentTemplate;
                    this.SelectedContentTemplateSelector = lSelectedTabItem.ContentTemplateSelector;
                }
                else
                {
                    this.SelectedContentTemplate = this.ContentTemplate;
                    this.SelectedContentTemplateSelector = this.ContentTemplateSelector;
                }
            }
        }

        #endregion Methods
    }
}
