﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="GridUnitType"/> enumeration.
    /// </summary>
    public enum GridUnitType : byte
    {
        /// <summary>
        /// Uses the minimum size requested by the content object.
        /// </summary>
        Auto = 0x00,

        /// <summary>
        /// Uses a fixed size in pixels provided by the user.
        /// </summary>
        Pixel = 0x01,

        /// <summary>
        /// Uses all the available space.
        /// </summary>
        Star = 0x02
    }
}
