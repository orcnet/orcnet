﻿namespace OrcNet.UI.GraphicObjects
{
    /// <summary>
    /// Definition of the <see cref="Stretch"/> enumeration.
    /// </summary>
    public enum Stretch : int
    {
        /// <summary>
        /// No stretching, original size preserved.
        /// </summary>
        None = 0x00,

        /// <summary>
        /// Resized but aspect ratio not preserved.
        /// </summary>
        Fill = 0x01,

        /// <summary>
        /// Preserves the apsect ratio.
        /// </summary>
        Uniform = 0x02
    }
}
