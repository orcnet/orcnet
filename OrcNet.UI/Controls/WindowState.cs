﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="WindowState"/> enumeration
    /// </summary>
    public enum WindowState
    {
        /// <summary>
        /// Regular size
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Minimized
        /// </summary>
        Minimized,

        /// <summary>
        /// Maximized
        /// </summary>
        Maximized
    }
}
