﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Menu"/> class.
    /// </summary>
	public class Menu : AMenuBase
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the menu is the main one or not.
        /// </summary>
        private bool mIsMainMenu;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the menu is the main one or not.
        /// </summary>
        public bool IsMainMenu
        {
            get
            {
                return this.mIsMainMenu;
            }
            set
            {
                this.mIsMainMenu = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        public Menu() :
        base()
        {
            this.mIsMainMenu = false;
        }

        #endregion Constructor
	}
}

