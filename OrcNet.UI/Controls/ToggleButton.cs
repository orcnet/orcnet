﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ToggleButton"/> class.
    /// </summary>
    public class ToggleButton : AButtonBase
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the check box has been checked or not.
        /// </summary>
        private bool mIsChecked;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired when the check box has been checked.
        /// </summary>
        public event EventHandler Checked;

        /// <summary>
        /// Event fired when the check box has been unchecked.
        /// </summary>
        public event EventHandler Unchecked;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the check box has been checked or not.
        /// </summary>
        public bool IsChecked
        {
            get
            {
                return this.mIsChecked;
            }
            set
            {
                if ( this.mIsChecked == value )
                {
                    return;
                }

                this.mIsChecked = value;

                this.NotifyPropertyChanged();

                if ( this.mIsChecked )
                {
                    this.OnChecked( null );
                }
                else
                {
                    this.OnUnchecked( null );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ToggleButton"/> class.
        /// </summary>
        public ToggleButton() : 
        base()
		{
            this.mIsChecked = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on mouse click.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseClick(object pSender, MouseButtonEventArgs pEventArgs)
        {
            this.IsChecked = !this.IsChecked;

            base.OnMouseClick( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on button checked.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnChecked(EventArgs pEventArgs)
        {
            this.NotifyChecked( pEventArgs );
        }

        /// <summary>
        /// Delegate called on button unchecked.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnUnchecked(EventArgs pEventArgs)
        {
            this.NotifyUnchecked( pEventArgs );
        }

        /// <summary>
        /// Notifies the check box has been checked.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifyChecked(EventArgs pEventArgs)
        {
            if ( this.Checked != null )
            {
                this.Checked( this, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies the check box has been unchecked.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifyUnchecked(EventArgs pEventArgs)
        {
            if ( this.Unchecked != null )
            {
                this.Unchecked( this, pEventArgs );
            }
        }

        #endregion Methods
    }
}
