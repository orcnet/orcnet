﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ComboBox"/> class.
    /// </summary>
	public class ComboBox : Selector
    {
        #region Fields

        /// <summary>
        /// Stores the popup template name.
        /// </summary>
        private const string PopupTemplateName = "Popup";

        /// <summary>
        /// Stores the drop popup.
        /// </summary>
        private Popup mPopup;

        /// <summary>
        /// Stores the drop direction.
        /// </summary>
        private DropDirection mDropDirection;

        /// <summary>
        /// Stores the flag indicating whether the drop down box is opened or not.
        /// </summary>
        private bool mIsDropBoxOpened;

        /// <summary>
        /// Stores the minimum drop down height.
        /// </summary>
        private int mMinDropBoxHeight;

        /// <summary>
        /// Stores the maximum drop down height.
        /// </summary>
        private int mMaxDropBoxHeight;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on drop down opened. 
        /// </summary>
        public EventHandler DropBoxOpened;

        /// <summary>
        /// Event fired on drop down closed. 
        /// </summary>
        public EventHandler DropBoxClosed;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the drop direction.
        /// </summary>
        public DropDirection DropDirection
        {
            get
            {
                return this.mDropDirection;
            }
            set
            {
                this.mDropDirection = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the drop box is opened or not.
        /// </summary>
        public bool IsDropBoxOpened
        {
            get
            {
                return this.mIsDropBoxOpened;
            }
            set
            {
                this.mIsDropBoxOpened = value;
                this.NotifyPropertyChanged();

                if ( this.mIsDropBoxOpened )
                {
                    this.OnDropBoxOpened( new EventArgs() );
                }
                else
                {
                    this.OnDropBoxClosed( new EventArgs() );
                }
            }
        }

        /// <summary>
        /// Gets or sets the minimum drop box height.
        /// </summary>
        public int MinDropBoxHeight
        {
            get
            {
                return this.mMinDropBoxHeight;
            }
            set
            {
                this.mMinDropBoxHeight = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the maximum drop box height.
        /// </summary>
        public int MaxDropBoxHeight
        {
            get
            {
                return this.mMaxDropBoxHeight;
            }
            set
            {
                this.mMaxDropBoxHeight = value;
                this.NotifyPropertyChanged();
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ComboBox"/> class.
        /// </summary>
        public ComboBox() : 
        base()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if ( this.mPopup != null )
            {
                this.mPopup.Closed -= this.OnPopupClosed;
            }

            this.mPopup = this.GetTemplateChild( PopupTemplateName ) as Popup;
            
            if ( this.mPopup != null )
            {
                this.mPopup.Closed += this.OnPopupClosed;
            }
        }

        /// <summary>
        /// Delegate called in popup closed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnPopupClosed(object pSender, EventArgs pEventArgs)
        {
            this.OnDropBoxClosed( pEventArgs );
        }

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected override void OnLayoutChanges(LayoutingType pLayoutType)
        {
            base.OnLayoutChanges( pLayoutType );

            //if ( pLayoutType == LayoutingType.Width )
            //{
            //    MinimumPopupSize = new Size(this.Slot.Width, minimumPopupSize.Height);
            //}
        }
        
        /// <summary>
        /// Delegate called on drop down opened.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected virtual void OnDropBoxOpened(EventArgs pEventArgs)
        {
            if ( this.DropBoxOpened != null )
            {
                this.DropBoxOpened( this, pEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on drop down closed.
        /// </summary>
        /// <param name="pEventArgs"></param>
        protected virtual void OnDropBoxClosed(EventArgs pEventArgs)
        {
            if ( this.DropBoxClosed != null )
            {
                this.DropBoxClosed( this, pEventArgs );
            }
        }
        
        #endregion Methods
    }
}
