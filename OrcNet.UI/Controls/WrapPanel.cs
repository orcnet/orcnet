﻿using OrcNet.UI.Collections;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="WrapPanel"/> class.
    /// </summary>
	public class WrapPanel : APanel
	{
        #region Fields

        /// <summary>
        /// Stores the uniform item width.
        /// </summary>
        private double mItemWidth;

        /// <summary>
        /// Stores the uniform item height.
        /// </summary>
        private double mItemHeight;

        /// <summary>
        /// Stores the panel elements orientation.
        /// </summary>
        private Orientation mOrientation;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the uniform item width.
        /// </summary>
        public double ItemWidth
        {
            get
            {
                return this.mItemWidth;
            }
            set
            {
                if ( this.mItemWidth == value )
                {
                    return;
                }

                this.mItemWidth = value;
                this.NotifyPropertyChanged();
                this.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Gets or sets the uniform item height.
        /// </summary>
        public double ItemHeight
        {
            get
            {
                return this.mItemHeight;
            }
            set
            {
                if ( this.mItemHeight == value )
                {
                    return;
                }

                this.mItemHeight = value;
                this.NotifyPropertyChanged();
                this.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Gets or sets the stack orientation.
        /// </summary>
        public Orientation Orientation
        {
            get
            {
                return this.mOrientation;
            }
            set
            {
                if ( this.mOrientation == value )
                {
                    return;
                }

                this.mOrientation = value;
                this.NotifyPropertyChanged();
                this.OnOrientationChanged();
            }
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WrapPanel"/> class.
        /// </summary>
        public WrapPanel() : 
        base()
		{
            this.mOrientation = Orientation.Horizontal;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on orientation changes.
        /// </summary>
        protected virtual void OnOrientationChanged()
        {

        }

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            UVSize lCurrLineSize  = new UVSize( this.Orientation );
            UVSize lPanelSize     = new UVSize( this.Orientation );
            UVSize lUvConstraint  = new UVSize( this.Orientation, this.Slot.Width, this.Slot.Height );
            double lItemWidth     = this.ItemWidth;
            double lItemHeight    = this.ItemHeight;
            bool lIsItemWidthSet  = DoubleUtil.IsNaN( lItemWidth ) == false;
            bool lIsItemHeightSet = DoubleUtil.IsNaN( lItemHeight ) == false;

            Size lChildConstraint = new Size( (lIsItemWidthSet ? (int)lItemWidth : this.Slot.Width ),
                                              (lIsItemHeightSet ? (int)lItemHeight : this.Slot.Height) );

            UIElementCollection lChildren = this.InternalChildren;
            for ( int lCurr = 0, lCount = lChildren.Count; lCurr < lCount; lCurr++ )
            {
                UIElement lChild = lChildren[ lCurr ] as UIElement;
                if ( lChild == null )
                {
                    continue;
                }
                
                lChild.Measure();
                
                // This is the size of the child in UV space
                UVSize lSize = new UVSize( this.Orientation, (lIsItemWidthSet ? lItemWidth : lChild.DesiredSize.Width),
                                                             (lIsItemHeightSet ? lItemHeight : lChild.DesiredSize.Height) );

                if ( DoubleUtil.GreaterThan( lCurrLineSize.U + lSize.U, lUvConstraint.U ) ) // need to switch to another line
                {
                    lPanelSize.U = Math.Max( lCurrLineSize.U, lPanelSize.U );
                    lPanelSize.V += lCurrLineSize.V;
                    lCurrLineSize = lSize;

                    if ( DoubleUtil.GreaterThan( lSize.U, lUvConstraint.U ) ) // the element is wider than the constraint - give it a separate line                    
                    {
                        lPanelSize.U = Math.Max( lSize.U, lPanelSize.U );
                        lPanelSize.V += lSize.V;
                        lCurrLineSize = new UVSize( this.Orientation );
                    }
                }
                else // continue to accumulate a line
                {
                    lCurrLineSize.U += lSize.U;
                    lCurrLineSize.V = Math.Max( lSize.V, lCurrLineSize.V );
                }
            }

            //the last line size, if any should be added
            lPanelSize.U = Math.Max( lCurrLineSize.U, lPanelSize.U );
            lPanelSize.V += lCurrLineSize.V;

            //go from UV space to W/H space
            return new Size( (int)lPanelSize.Width, (int)lPanelSize.Height );
        }

        /// <summary>
        /// Applies a children layout constrain.
        /// </summary>
        /// <param name="pLayoutType">The layout constrain type.</param>
        protected override void ChildrenLayoutingConstraints(ref LayoutingType pLayoutType)
        {
            pLayoutType &= ~LayoutingType.Positioning;
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            if ( pLayoutType == LayoutingType.ArrangeChildren )
            {
                if ( (this.State & LayoutingType.Sizing) != 0 )
                {
                    return this.Slot;
                }
                
                this.ComputeChildrenPositions();
                
                return this.Slot;
            }

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Delegate called on child layout changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnChildLayoutChanges(object pSender, LayoutingEventArgs pEventArgs)
        {
            // Children can't stretch in a WrapPanel
            FrameworkElement lElement = pSender as FrameworkElement;

            switch ( pEventArgs.LayoutType )
            {
                case LayoutingType.Width:
                    //if ( this.Orientation == Orientation.Horizontal &&
                    //     lElement.Width.Units == Unit.Percent)
                    //{
                    //    lElement.Width = cFitSizeValue;
                    //    return;
                    //}

                    this.RegisterForLayouting( LayoutingType.Width );
                    break;
                case LayoutingType.Height:
                    //if ( this.Orientation == Orientation.Vertical && 
                    //     lElement.Height.Units == Unit.Percent)
                    //{
                    //    lElement.Height = cFitSizeValue;
                    //    return;
                    //}

                    this.RegisterForLayouting( LayoutingType.Height );
                    break;
                default:
                    return;
            }

            this.RegisterForLayouting( LayoutingType.ArrangeChildren );
        }

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected internal override void OnLayoutChanges(LayoutingType pLayoutType)
        {
            switch ( pLayoutType )
            {
                case LayoutingType.Width:
                    //foreach ( UIElement lChild in this.Children )
                    //{
                    //    if ( lChild.Width.Units == Unit.Percent )
                    //    {
                    //        lChild.RegisterForLayouting( LayoutingType.Width );
                    //    }
                    //}

                    if ( this.Height == cFitSizeValue )
                    {
                        this.RegisterForLayouting( LayoutingType.Height );
                    }

                    this.RegisterForLayouting( LayoutingType.X );
                    break;
                case LayoutingType.Height:
                    //foreach ( UIElement lChild in this.Children )
                    //{
                    //    FrameworkElement lCast = lChild as FrameworkElement;
                    //    if ( lChild.Height.Units == Unit.Percent )
                    //    {
                    //        lChild.RegisterForLayouting( LayoutingType.Height );
                    //    }
                    //}

                    if ( this.Width == cFitSizeValue )
                    {
                        this.RegisterForLayouting( LayoutingType.Width );
                    }

                    this.RegisterForLayouting( LayoutingType.Y );
                    break;
                default:
                    return;
            }

            this.RegisterForLayouting( LayoutingType.ArrangeChildren );
            
            this.NotifyLayoutChanged( new LayoutingEventArgs( pLayoutType ) );
        }
        
        /// <summary>
        /// Computes the children position.
        /// </summary>
        private void ComputeChildrenPositions()
        {
            int lFirstInLine = 0;
            double lItemWidth  = this.ItemWidth;
            double lItemHeight = this.ItemHeight;
            double lAccumulatedV = 0;
            double lItemU = this.Orientation == Orientation.Horizontal ? lItemWidth : lItemHeight;
            UVSize lCurrLineSize = new UVSize( this.Orientation);
            UVSize lUVFinalSize  = new UVSize( this.Orientation, this.ClientRectangle.Width, this.ClientRectangle.Height );
            bool lIsItemWidthSet  = DoubleUtil.IsNaN( lItemWidth ) == false;
            bool lIsItemHeightSet = DoubleUtil.IsNaN( lItemHeight ) == false;
            bool lUseItemU = this.Orientation == Orientation.Horizontal ? lIsItemWidthSet : lIsItemHeightSet;

            UIElementCollection lChildren = this.InternalChildren;
            for ( int lCurr = 0, lCount = lChildren.Count; lCurr < lCount; lCurr++ )
            {
                UIElement lChild = lChildren[ lCurr ] as UIElement;
                if ( lChild == null ||
                     lChild.IsVisible == false )
                {
                    continue;
                }

                UVSize lSize = new UVSize( this.Orientation, (lIsItemWidthSet ? lItemWidth : lChild.DesiredSize.Width),
                                                             (lIsItemHeightSet ? lItemHeight : lChild.DesiredSize.Height));

                if ( DoubleUtil.GreaterThan( lCurrLineSize.U + lSize.U, lUVFinalSize.U ) ) // need to switch to another line
                {
                    this.ArrangeLine( lAccumulatedV, lCurrLineSize.V, lFirstInLine, lCurr, lUseItemU, lItemU );

                    lAccumulatedV += lCurrLineSize.V;
                    lCurrLineSize = lSize;

                    if ( DoubleUtil.GreaterThan( lSize.U, lUVFinalSize.U ) ) // the element is wider then the constraint - give it a separate line                    
                    {
                        //switch to next line which only contain one element
                        this.ArrangeLine( lAccumulatedV, lSize.V, lCurr, ++lCurr, lUseItemU, lItemU );

                        lAccumulatedV += lSize.V;
                        lCurrLineSize = new UVSize( this.Orientation );
                    }

                    lFirstInLine = lCurr;
                }
                else //continue to accumulate a line
                {
                    lCurrLineSize.U += lSize.U;
                    lCurrLineSize.V = Math.Max(lSize.V, lCurrLineSize.V);
                }
            }

            // Arrange the last line, if any
            if ( lFirstInLine < lChildren.Count )
            {
                this.ArrangeLine( lAccumulatedV, lCurrLineSize.V, lFirstInLine, lChildren.Count, lUseItemU, lItemU );
            }

            this.mIsDirty = true;
        }

        /// <summary>
        /// Arranges a line.
        /// </summary>
        /// <param name="pV"></param>
        /// <param name="pLineV"></param>
        /// <param name="pStart"></param>
        /// <param name="pEnd"></param>
        /// <param name="pUseItemU"></param>
        /// <param name="pItemU"></param>
        private void ArrangeLine(double pV, double pLineV, int pStart, int pEnd, bool pUseItemU, double pItemU)
        {
            double lU = 0;
            bool lIsHorizontal = this.Orientation == Orientation.Horizontal;

            UIElementCollection lChildren = this.InternalChildren;
            for ( int lCurr = pStart; lCurr < pEnd; lCurr++ )
            {
                UIElement lChild = lChildren[lCurr] as UIElement;
                if ( lChild != null )
                {
                    UVSize lChildSize = new UVSize( this.Orientation, lChild.DesiredSize.Width, lChild.DesiredSize.Height );
                    double lLayoutSlotU = (pUseItemU ? pItemU : lChildSize.U);
                    lChild.Slot = new Rectangle((int)(lIsHorizontal ? lU : pV),
                                                (int)(lIsHorizontal ? pV : lU),
                                                (int)(lIsHorizontal ? lLayoutSlotU : pLineV),
                                                (int)(lIsHorizontal ? pLineV : lLayoutSlotU));

                    lU += lLayoutSlotU;
                }
            }
        }

        #endregion Methods

        #region Inner Classes 

        /// <summary>
        /// Definition of the <see cref="UVSize"/> class.
        /// </summary>
        private struct UVSize
        {
            #region Fields

            /// <summary>
            /// Stores the U size.
            /// </summary>
            internal double U;

            /// <summary>
            /// Stores the V size.
            /// </summary>
            internal double V;

            /// <summary>
            /// Stores the orientation.
            /// </summary>
            private Orientation mOrientation;

            #endregion Fields

            #region Properties

            /// <summary>
            /// Gets or sets the width
            /// </summary>
            internal double Width
            {
                get
                {
                    return this.mOrientation == Orientation.Horizontal ? U : V;
                }
                set
                {
                    if ( this.mOrientation == Orientation.Horizontal )
                    {
                        U = value;
                    }
                    else
                    {
                        V = value;
                    }
                }
            }

            /// <summary>
            /// Gets or sets the height.
            /// </summary>
            internal double Height
            {
                get
                {
                    return this.mOrientation == Orientation.Horizontal ? V : U;
                }
                set
                {
                    if ( this.mOrientation == Orientation.Horizontal )
                    {
                        V = value;
                    }
                    else
                    {
                        U = value;
                    }
                }
            }

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="UVSize"/> class.
            /// </summary>
            /// <param name="pOrientation"></param>
            internal UVSize(Orientation pOrientation)
            {
                U = V = 0.0;
                this.mOrientation = pOrientation;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="UVSize"/> class.
            /// </summary>
            /// <param name="pOrientation"></param>
            /// <param name="pWidth"></param>
            /// <param name="pHeight"></param>
            internal UVSize(Orientation pOrientation, double pWidth, double pHeight)
            {
                U = V = 0.0;
                this.mOrientation = pOrientation;
                this.Width  = pWidth;
                this.Height = pHeight;
            }
            
            #endregion Constructor
        }

        #endregion Inner Classes
    }
}

