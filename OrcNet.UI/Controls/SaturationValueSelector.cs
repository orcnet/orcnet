﻿using System;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="SaturationValueSelector"/> class.
    /// </summary>
	public class SaturationValueSelector : ColorSelector
	{
        #region Fields

        /// <summary>
        /// Stores the value/luminance value.
        /// </summary>
        private double mValue;

        /// <summary>
        /// Stores the saturation value.
        /// </summary>
        private double mSaturation;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the value/luminance value.
        /// </summary>
        public virtual double Value
        {
            get
            {
                return this.mValue;
            }
            set
            {
                
                if ( this.mValue != value )
                {
                    this.mValue = value;
                    this.NotifyPropertyChanged();

                    this.mMousePosition.Y = (int)Math.Floor( (1.0 - this.mValue) * this.ClientRectangle.Height );

                    this.InvalidateVisual();
                }
            }
        }

        /// <summary>
        /// Gets or sets the saturation value.
        /// </summary>
        public virtual double Saturation
        {
            get
            {
                return this.mSaturation;
            }
            set
            {
                if ( this.mSaturation != value )
                {
                    this.mSaturation = value;
                    this.NotifyPropertyChanged();

                    this.mMousePosition.X = (int)Math.Floor( this.mSaturation * this.ClientRectangle.Width );

                    this.InvalidateVisual();
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SaturationValueSelector"/> class.
        /// </summary>
        public SaturationValueSelector() : 
        base()
		{

		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            base.OnRender( pDrawingContext );

            Rectangle lRectangle = this.ClientRectangle;

            if ( this.Foreground != null )
            {
                // TODO:test if null should be removed
                this.Foreground.SetAsSource( pDrawingContext, lRectangle );
                CairoHelpers.CairoRectangle( pDrawingContext, lRectangle, -1 );
                pDrawingContext.Fill();
            }

            GradientBrush lGradient = new GradientBrush( GradientType.Horizontal );
            lGradient.GradientStops.Add( new GradientStop( 0, new Color(1, 1, 1, 1) ) );
            lGradient.GradientStops.Add( new GradientStop( 1, new Color(1, 1, 1, 0) ) );
            lGradient.SetAsSource( pDrawingContext, lRectangle );
            CairoHelpers.CairoRectangle( pDrawingContext, lRectangle, -1 );
            pDrawingContext.Fill();
            lGradient = new GradientBrush( GradientType.Vertical );
            lGradient.GradientStops.Add( new GradientStop( 0, new Color(0, 0, 0, 0) ) );
            lGradient.GradientStops.Add( new GradientStop( 1, new Color(0, 0, 0, 1) ) );
            lGradient.SetAsSource( pDrawingContext, lRectangle );
            CairoHelpers.CairoRectangle( pDrawingContext, lRectangle, -1 );
            pDrawingContext.Fill();
        }

        /// <summary>
        /// Recursive painting routine on the parent context of the actual cached version of the widget 
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected internal override void Paint(ref Context pDrawingContext)
        {
            base.Paint( ref pDrawingContext );

            Rectangle lRegionBound = this.Slot + this.VisualParent.ClientRectangle.Position;
            pDrawingContext.Save();

            pDrawingContext.Translate( lRegionBound.X, lRegionBound.Y );

            pDrawingContext.SetSourceColor( Color.White );
            pDrawingContext.Arc( this.mMousePosition.X, this.mMousePosition.Y, 3.5, 0, Math.PI * 2.0 );
            pDrawingContext.LineWidth = 1.0;
            pDrawingContext.Stroke();

            pDrawingContext.Restore();
        }

        /// <summary>
        /// Updates the mouse position in cache.
        /// </summary>
        /// <param name="pPosition"></param>
        protected override void UpdateMouseLocalPos(Point pPosition)
        {
            base.UpdateMouseLocalPos( pPosition );

            Rectangle lClientBound = this.ClientRectangle;
            this.mSaturation = (double)this.mMousePosition.X / (double)lClientBound.Width;
            this.mValue = 1.0 - (double)this.mMousePosition.Y / (double)lClientBound.Height;
            this.NotifyPropertyChanged( "Saturation" );
            this.NotifyPropertyChanged( "Value" );

            this.InvalidateVisual();
        }

        /// <summary>
        /// Delegate called on layout changes.
        /// </summary>
        /// <param name="pLayoutType">The current layout change reason.</param>
        protected internal override void OnLayoutChanges(LayoutingType pLayoutType)
        {
            base.OnLayoutChanges( pLayoutType );

            switch ( pLayoutType )
            {
                case LayoutingType.Width:
                    this.mMousePosition.X = (int)Math.Floor( this.mSaturation * this.ClientRectangle.Width );
                    break;
                case LayoutingType.Height:
                    this.mMousePosition.Y = (int)Math.Floor( (1.0 - this.mValue) * this.ClientRectangle.Height );
                    break;
            }
        }

        #endregion Methods
    }
}

