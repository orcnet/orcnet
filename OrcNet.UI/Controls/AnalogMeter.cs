﻿using System;
using Cairo;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="AnalogMeter"/> class.
    /// </summary>
	public class AnalogMeter : ARangeBase
    {
		#region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AnalogMeter"/> class.
        /// </summary>
		public AnalogMeter() : 
        base()
		{

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pMinimum"></param>
        /// <param name="pMaximum"></param>
        /// <param name="pStep"></param>
		public AnalogMeter(double pMinimum, double pMaximum, double pStep) : 
        base( pMinimum, pMaximum )
		{
            this.SmallIncrement = pStep;
		}
        #endregion

        #region Methods

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
		{			
			base.OnRender( pDrawingContext );

			Rectangle lRectangle = this.ClientRectangle;
			Point lCenter = lRectangle.Center;

            pDrawingContext.Save();


            double aUnit = Math.PI * 2.0 / (Maximum - Minimum);
            pDrawingContext.Translate(lCenter.X, lRectangle.Height * 1.1);
            pDrawingContext.Rotate( this.Value / 4.0 * aUnit - Math.PI / 4.0);
            pDrawingContext.Translate(-lCenter.X, -lCenter.Y);

			pDrawingContext.LineWidth = 2;
            Foreground.SetAsSource(pDrawingContext);
            pDrawingContext.MoveTo(lCenter.X, 0.0);
            pDrawingContext.LineTo(lCenter.X, -lCenter.Y * 0.5);
            pDrawingContext.Stroke();

            pDrawingContext.Restore();
		}

        #endregion Methods
    }
}

