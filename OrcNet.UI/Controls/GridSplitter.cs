﻿using System;
using System.Diagnostics;
using System.Collections;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="GridSplitter"/> class.
    /// </summary>
	public class GridSplitter : Thumb
    {
        #region Fields

        /// <summary>
        /// Stores the splitter resize data used for resizing.
        /// </summary>
        private ResizeData mResizeData;

        /// <summary>
        /// Stores the resize direction indicating whether columns, rows or both must be resized.
        /// </summary>
        private GridResizeDirection mResizeDirection;

        /// <summary>
        /// Stores the resize behavior indicating whether columns or rows are resized.
        /// </summary>
        private GridResizeBehavior mResizeBehavior;

        /// <summary>
        /// Stores the increment to apply on keyboard moves.
        /// </summary>
        private double mKeyboardIncrement;

        /// <summary>
        /// Stores the increment to apply on drag moves.
        /// </summary>
        private double mDragIncrement;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the resize direction indicating whether columns, rows or both must be resized.
        /// </summary>
        public GridResizeDirection ResizeDirection
        {
            get
            {
                return this.mResizeDirection;
            }
            set
            {
                if ( this.mResizeDirection == value )
                {
                    return;
                }

                this.mResizeDirection = value;

                this.NotifyPropertyChanged();
                this.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Gets or sets the resize behavior indicating whether columns or rows are resized.
        /// </summary>
        public GridResizeBehavior ResizeBehavior
        {
            get
            {
                return this.mResizeBehavior;
            }
            set
            {
                if ( this.mResizeBehavior == value )
                {
                    return;
                }

                this.mResizeBehavior = value;

                this.NotifyPropertyChanged();
                this.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Gets or sets the increment to apply on keyboard moves.
        /// </summary>
        public double KeyboardIncrement
        {
            get
            {
                return this.mKeyboardIncrement;
            }
            set
            {
                if ( this.mKeyboardIncrement == value ||
                     value <= 0.0 ||
                     double.IsPositiveInfinity( value ) )
                {
                    return;
                }

                this.mKeyboardIncrement = value;

                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the increment to apply on drag moves.
        /// </summary>
        public double DragIncrement
        {
            get
            {
                return this.mDragIncrement;
            }
            set
            {
                if ( this.mDragIncrement == value ||
                     value <= 0.0 ||
                     double.IsPositiveInfinity( value ) )
                {
                    return;
                }

                this.mDragIncrement = value;

                this.NotifyPropertyChanged();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GridSplitter"/> class.
        /// </summary>
        public GridSplitter() : 
        base()
        {
            this.DragStarted   += this.OnDragStarted;
            this.DragChanged   += this.OnDragChanged;
            this.DragCompleted += this.OnDragCompleted;
        }
        
        #endregion Constructor
        
		#region Methods
		
        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Delegate called on UIElement mouse enter.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseEnter(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseEnter( pSender, pEventArgs );

            this.Cursor = this.GetCursor();
        }

        /// <summary>
        /// Delegate called on UIElement mouse leave.
        /// </summary>
        /// <param name="pSender">The Element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseLeave(object pSender, MouseMoveEventArgs pEventArgs)
        {
            base.OnMouseLeave( pSender, pEventArgs );

            this.Cursor = XCursor.Default;
        }

        /// <summary>
        /// Delegate called on key pressed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal override void OnKeyDown(object pSender, KeyboardKeyEventArgs pEventArgs)
        {
            Key lKey = pEventArgs.Key;
            switch ( lKey )
            {
                case Key.Escape:
                    if ( this.mResizeData != null )
                    {
                        this.CancelResize();
                    }
                    break;
                case Key.Left:
                    this.KeyboardMoveSplitter( -this.KeyboardIncrement, 0 );
                    break;
                case Key.Right:
                    this.KeyboardMoveSplitter( this.KeyboardIncrement, 0 );
                    break;
                case Key.Up:
                    this.KeyboardMoveSplitter( 0, -this.KeyboardIncrement );
                    break;
                case Key.Down:
                    this.KeyboardMoveSplitter( 0, this.KeyboardIncrement );
                    break;
            }

            base.OnKeyDown( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called on element lost focus event.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected override void OnLostFocus(object pSender, EventArgs pEventArgs)
        {
            base.OnLostFocus( pSender, pEventArgs );

            if ( this.mResizeData != null )
            {
                this.CancelResize();
            }
        }

        /// <summary>
        /// Delegate called on drag started.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArg"></param>
        private void OnDragStarted(object pSender, DragStartedEventArgs pEventArg)
        {
            Debug.Assert( this.mResizeData == null, "ResizeData is not null, DragCompleted was not called" );

            this.InitializeData();
        }

        /// <summary>
        /// Delegate called on drag changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnDragChanged(object pSender, DragDeltaEventArgs pEventArgs)
        {
            if ( this.mResizeData != null )
            {
                double lHorizontalChange = pEventArgs.HorizontalDelta;
                double lVerticalChange   = pEventArgs.VerticalDelta;

                // Round change to nearest multiple of DragIncrement
                double lDragIncrement = this.DragIncrement;
                lHorizontalChange = Math.Round( lHorizontalChange / lDragIncrement ) * lDragIncrement;
                lVerticalChange   = Math.Round( lVerticalChange / lDragIncrement ) * lDragIncrement;

                // Directly update the grid
                this.MoveSplitter( lHorizontalChange, lVerticalChange );
            }
        }

        /// <summary>
        /// Delegate called on drag completed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnDragCompleted(object pSender, DragCompletedEventArgs pEventArgs)
        {
            if ( this.mResizeData != null )
            {
                this.mResizeData = null;
            }
        }

        /// <summary>
        /// Moves the splitter using the keyboard event(s).
        /// </summary>
        /// <param name="pHorizontalChange"></param>
        /// <param name="pVerticalChange"></param>
        /// <returns></returns>
        internal bool KeyboardMoveSplitter(double pHorizontalChange, double pVerticalChange)
        {
            // If moving with the mouse, ignore keyboard motion
            if ( this.mResizeData != null )
            {
                return false;
            }
 
            this.InitializeData();
 
            // Check that we are actually able to resize
            if ( this.mResizeData == null )
            {
                return false;
            }
 
            MoveSplitter( pHorizontalChange, pVerticalChange );
 
            this.mResizeData = null;
 
            return true;
        }

        /// <summary>
        /// Moves the splitter by the given horizontal and vertical changes.
        /// </summary>
        /// <param name="pHorizontalChange"></param>
        /// <param name="pVerticalChange"></param>
        private void MoveSplitter(double pHorizontalChange, double pVerticalChange)
        {
            Debug.Assert( this.mResizeData != null, "ResizeData should not be null when calling MoveSplitter" );

            double lDelta;

            // Calculate the offset to adjust the splitter.  If layout rounding is enabled, we
            // need to round to an integer physical pixel value to avoid round-ups of children that
            // expand the bounds of the Grid.  In practice this only happens in high dpi because
            // horizontal/vertical offsets here are never fractional (they correspond to mouse movement
            // across logical pixels).  Rounding error only creeps in when converting to a physical
            // display with something other than the logical 96 dpi.
            if ( this.mResizeData.ResizeDirection == GridResizeDirection.Columns )
            {
                lDelta = pHorizontalChange;
            }
            else
            {
                lDelta = pVerticalChange;
            }

            IDefinition lDefinition1 = this.mResizeData.Definition1;
            IDefinition lDefinition2 = this.mResizeData.Definition2;
            if ( lDefinition1 != null && 
                 lDefinition2 != null )
            {
                double lActualLength1 = GetActualLength( lDefinition1 );
                double lActualLength2 = GetActualLength( lDefinition2 );

                // When splitting, Check to see if the total pixels spanned by the definitions 
                // is the same asbefore starting resize. If not cancel the drag
                if ( this.mResizeData.SplitBehavior == SplitBehavior.Split &&
                     DoubleUtil.AreClose( lActualLength1 + lActualLength2, this.mResizeData.OriginalDefinition1ActualLength + this.mResizeData.OriginalDefinition2ActualLength ) == false )
                {
                    this.CancelResize();
                    return;
                }

                double lMin;
                double lMax;
                this.GetDeltaConstraints( out lMin, out lMax );
                
                // Constrain Delta to Min/MaxWidth of columns
                lDelta = Math.Min( Math.Max( lDelta, lMin ), lMax );

                // With floating point operations there may be loss of precision to some degree. Eg. Adding a very 
                // small value to a very large one might result in the small value being ignored. In the following 
                // steps there are two floating point operations viz. lActualLength1+lDelta and lActualLength2-lDelta. 
                // It is possible that the addition resulted in loss of precision and the lDelta value was ignored, whereas 
                // the subtraction actual absorbed the lDelta value. This now means that 
                // (lDefinition1LengthNew + definition2LengthNewis) 2 factors of precision away from 
                // (lActualLength1 + lActualLength2). This can cause a problem in the subsequent drag iteration where 
                // this will be interpreted as the cancellation of the resize operation. To avoid this imprecision we use 
                // make lDefinition2LengthNew be a function of lDefinition1LengthNew so that the precision or the loss 
                // thereof can be counterbalanced. See DevDiv bug#140228 for a manifestation of this problem.

                double lDefinition1LengthNew = lActualLength1 + lDelta;
                double lDefinition2LengthNew = lActualLength1 + lActualLength2 - lDefinition1LengthNew;

                this.SetLengths( lDefinition1LengthNew, lDefinition2LengthNew );
            }
        }

        /// <summary>
        /// Initializes resize data.
        /// </summary>
        private void InitializeData()
        {
            Grid lParentGrid = this.LogicalParent as Grid;

            // If not in a grid or can't resize, do nothing
            if ( lParentGrid != null )
            {
                // Setup data used for resizing
                this.mResizeData = new ResizeData();
                this.mResizeData.Grid = lParentGrid;
                this.mResizeData.ResizeDirection = this.GetEffectiveResizeDirection();
                this.mResizeData.ResizeBehavior  = this.GetEffectiveResizeBehavior( this.mResizeData.ResizeDirection );
                this.mResizeData.SplitterThickness = Math.Min( this.Slot.Width, this.Slot.Height );

                // Store the rows and columns to resize on drag events
                if ( this.InitializeDefinitionsToResize() == false )
                {
                    // Unable to resize, clear data
                    this.mResizeData = null;
                    return;
                }

                // Setup the preview in the adorner if ShowsPreview is true
                //SetupPreview();
            }
        }

        /// <summary>
        /// Initializes the definition element(s) to resize.
        /// </summary>
        /// <returns>True if can resize, false otherwise.</returns>
        private bool InitializeDefinitionsToResize()
        {
            int lSplitterIndex;
            int lIndex1;
            int lIndex2;

            int lGridSpan = this.mResizeData.ResizeDirection == GridResizeDirection.Columns ? this.mResizeData.Grid.GetColumnSpan( this ) : this.mResizeData.Grid.GetRowSpan( this );

            if ( lGridSpan == 1 )
            {
                lSplitterIndex = this.mResizeData.ResizeDirection == GridResizeDirection.Columns ? this.mResizeData.Grid.GetColumn( this ) : this.mResizeData.Grid.GetRow( this );

                // Select the columns based on Behavior
                switch ( this.mResizeData.ResizeBehavior )
                {
                    case GridResizeBehavior.PreviousAndCurrent:
                        // Get current and previous
                        lIndex1 = lSplitterIndex - 1;
                        lIndex2 = lSplitterIndex;
                        break;
                    case GridResizeBehavior.CurrentAndNext:
                        // Get current and next
                        lIndex1 = lSplitterIndex;
                        lIndex2 = lSplitterIndex + 1;
                        break;
                    default:
                        // Get previous and next
                        lIndex1 = lSplitterIndex - 1;
                        lIndex2 = lSplitterIndex + 1;
                        break;
                }

                // Get # of rows/columns in the resize direction
                int lCount = this.mResizeData.ResizeDirection == GridResizeDirection.Columns ? this.mResizeData.Grid.ColumnDefinitions.Count : this.mResizeData.Grid.RowDefinitions.Count;

                if ( lIndex1 >= 0 && 
                     lIndex2 < lCount )
                {
                    this.mResizeData.SplitterIndex = lSplitterIndex;

                    this.mResizeData.Definition1Index = lIndex1;
                    this.mResizeData.Definition1 = GetGridDefinition( this.mResizeData.Grid, (uint)lIndex1, this.mResizeData.ResizeDirection );
                    this.mResizeData.OriginalDefinition1Length = this.mResizeData.Definition1.UserSize;
                    this.mResizeData.OriginalDefinition1ActualLength = GetActualLength( this.mResizeData.Definition1 );

                    this.mResizeData.Definition2Index = lIndex2;
                    this.mResizeData.Definition2 = GetGridDefinition( this.mResizeData.Grid, (uint)lIndex2, this.mResizeData.ResizeDirection );
                    this.mResizeData.OriginalDefinition2Length = this.mResizeData.Definition2.UserSize;
                    this.mResizeData.OriginalDefinition2ActualLength = GetActualLength( this.mResizeData.Definition2 );

                    // Determine how to resize the columns 
                    bool lIsStar1 = IsStar( this.mResizeData.Definition1 );
                    bool lIsStar2 = IsStar( this.mResizeData.Definition2 );
                    if ( lIsStar1 && 
                         lIsStar2 )
                    {
                        // If they are both stars, resize both
                        this.mResizeData.SplitBehavior = SplitBehavior.Split;
                    }
                    else
                    {
                        // One column is fixed width, resize the first one that is fixed
                        this.mResizeData.SplitBehavior = lIsStar1 == false ? SplitBehavior.Resize1 : SplitBehavior.Resize2;
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the resize direction.
        /// </summary>
        /// <returns></returns>
        private GridResizeDirection GetEffectiveResizeDirection()
        {
            GridResizeDirection lDirection = this.ResizeDirection;

            if ( lDirection == GridResizeDirection.Auto )
            {
                // When HorizontalAlignment is Left, Right or Center, resize Columns
                if ( this.HorizontalAlignment != HorizontalAlignment.Stretch )
                {
                    lDirection = GridResizeDirection.Columns;
                }
                else if ( this.VerticalAlignment != VerticalAlignment.Stretch )
                {
                    lDirection = GridResizeDirection.Rows;
                }
                else if ( this.Slot.Width <= this.Slot.Height )
                {
                    lDirection = GridResizeDirection.Columns;
                }
                else
                {
                    lDirection = GridResizeDirection.Rows;
                }

            }
            return lDirection;
        }

        /// <summary>
        /// Gets the resize behavior.
        /// </summary>
        /// <param name="pDirection"></param>
        /// <returns></returns>
        private GridResizeBehavior GetEffectiveResizeBehavior(GridResizeDirection pDirection)
        {
            GridResizeBehavior lResizeBehavior = this.ResizeBehavior;

            if ( lResizeBehavior == GridResizeBehavior.BasedOnAlignment )
            {
                if ( pDirection == GridResizeDirection.Columns )
                {
                    switch ( this.HorizontalAlignment )
                    {
                        case HorizontalAlignment.Left:
                            lResizeBehavior = GridResizeBehavior.PreviousAndCurrent;
                            break;
                        case HorizontalAlignment.Right:
                            lResizeBehavior = GridResizeBehavior.CurrentAndNext;
                            break;
                        default:
                            lResizeBehavior = GridResizeBehavior.PreviousAndNext;
                            break;
                    }
                }
                else
                {
                    switch ( this.VerticalAlignment )
                    {
                        case VerticalAlignment.Top:
                            lResizeBehavior = GridResizeBehavior.PreviousAndCurrent;
                            break;
                        case VerticalAlignment.Bottom:
                            lResizeBehavior = GridResizeBehavior.CurrentAndNext;
                            break;
                        default:
                            lResizeBehavior = GridResizeBehavior.PreviousAndNext;
                            break;
                    }
                }
            }

            return lResizeBehavior;
        }

        /// <summary>
        /// Gets the definition actual length
        /// </summary>
        /// <param name="pDefinition"></param>
        /// <returns></returns>
        private static double GetActualLength(IDefinition pDefinition)
        {
            ColumnDefinition lColumn = pDefinition as ColumnDefinition;

            return lColumn == null ? ((RowDefinition)pDefinition).ActualHeight : lColumn.ActualWidth;
        }

        /// <summary>
        /// Gets the grid definition at the given index in the supplied grid.
        /// </summary>
        /// <param name="pGrid"></param>
        /// <param name="pIndex"></param>
        /// <param name="pDirection"></param>
        /// <returns></returns>
        private static IDefinition GetGridDefinition(Grid pGrid, uint pIndex, GridResizeDirection pDirection)
        {
            return pDirection == GridResizeDirection.Columns ? (IDefinition)pGrid.ColumnDefinitions[ pIndex ] : (IDefinition)pGrid.RowDefinitions[ pIndex ];
        }

        /// <summary>
        /// Gets the flag indicating whether the definition is of star length or not.
        /// </summary>
        /// <param name="pDefinition"></param>
        /// <returns></returns>
        private static bool IsStar(IDefinition pDefinition)
        {
            return pDefinition.UserSize.IsStar;
        }

        /// <summary>
        /// Sets the definition length.
        /// </summary>
        /// <param name="pDefinition"></param>
        /// <param name="pLength"></param>
        private static void SetDefinitionLength(IDefinition pDefinition, GridLength pLength)
        {
            if ( pDefinition is ColumnDefinition )
            {
                (pDefinition as ColumnDefinition).Width = pLength;
            }
            else
            {
                (pDefinition as RowDefinition).Height = pLength;
            }
        }

        /// <summary>
        /// Gets the min and max delta constraints.
        /// </summary>
        /// <param name="pMinDelta"></param>
        /// <param name="pMaxDelta"></param>
        private void GetDeltaConstraints(out double pMinDelta, out double pMaxDelta)
        {
            double lDefinition1Len = GetActualLength( this.mResizeData.Definition1 );
            double lDefinition1Min = this.mResizeData.Definition1.UserMinSize;
            double lDefinition1Max = this.mResizeData.Definition1.UserMaxSize;

            double lDefinition2Len = GetActualLength( this.mResizeData.Definition2 );
            double lDefinition2Min = this.mResizeData.Definition2.UserMinSize;
            double lDefinition2Max = this.mResizeData.Definition2.UserMaxSize;

            // Set MinWidths to be greater than width of splitter
            if ( this.mResizeData.SplitterIndex == this.mResizeData.Definition1Index )
            {
                lDefinition1Min = Math.Max( lDefinition1Min, this.mResizeData.SplitterThickness );
            }
            else if ( this.mResizeData.SplitterIndex == this.mResizeData.Definition2Index )
            {
                lDefinition2Min = Math.Max( lDefinition2Min, this.mResizeData.SplitterThickness );
            }

            if ( this.mResizeData.SplitBehavior == SplitBehavior.Split )
            {
                // Determine the minimum and maximum the columns can be resized
                pMinDelta = -Math.Min( lDefinition1Len - lDefinition1Min, lDefinition2Max - lDefinition2Len );
                pMaxDelta = Math.Min( lDefinition1Max - lDefinition1Len, lDefinition2Len - lDefinition2Min );
            }
            else if ( this.mResizeData.SplitBehavior == SplitBehavior.Resize1 )
            {
                pMinDelta = lDefinition1Min - lDefinition1Len;
                pMaxDelta = lDefinition1Max - lDefinition1Len;
            }
            else
            {
                pMinDelta = lDefinition2Len - lDefinition2Max;
                pMaxDelta = lDefinition2Len - lDefinition2Min;
            }
        }

        /// <summary>
        /// Sets the lengths.
        /// </summary>
        /// <param name="pDefinition1Pixels"></param>
        /// <param name="pDefinition2Pixels"></param>
        private void SetLengths(double pDefinition1Pixels, double pDefinition2Pixels)
        {
            // For the case where both definition1 and 2 are stars, update all star values to match their current pixel values
            if ( this.mResizeData.SplitBehavior == SplitBehavior.Split )
            {
                IEnumerable lDefinitions = this.mResizeData.ResizeDirection == GridResizeDirection.Columns ? (IEnumerable)this.mResizeData.Grid.ColumnDefinitions : (IEnumerable)this.mResizeData.Grid.RowDefinitions;

                int lCurr = 0;
                foreach ( IDefinition lDefinition in lDefinitions )
                {
                    // For each lDefinition, if it is a star, set is value to ActualLength in stars
                    // This makes 1 star == 1 pixel in length
                    if ( lCurr == this.mResizeData.Definition1Index )
                    {
                        SetDefinitionLength( lDefinition, new GridLength( pDefinition1Pixels, GridUnitType.Star ) );
                    }
                    else if ( lCurr == this.mResizeData.Definition2Index )
                    {
                        SetDefinitionLength(lDefinition, new GridLength( pDefinition2Pixels, GridUnitType.Star ) );
                    }
                    else if ( IsStar( lDefinition ) )
                    {
                        SetDefinitionLength(lDefinition, new GridLength( GetActualLength( lDefinition ), GridUnitType.Star ) );
                    }

                    lCurr++;
                }
            }
            else if ( this.mResizeData.SplitBehavior == SplitBehavior.Resize1 )
            {
                SetDefinitionLength( this.mResizeData.Definition1, new GridLength( pDefinition1Pixels ) );
            }
            else
            {
                SetDefinitionLength( this.mResizeData.Definition2, new GridLength( pDefinition2Pixels ) );
            }
        }

        /// <summary>
        /// Gets the cursor.
        /// </summary>
        /// <returns></returns>
        private XCursor GetCursor()
        {
            switch ( this.GetEffectiveResizeDirection() )
            {
                case GridResizeDirection.Columns:
                    return XCursor.H;
                case GridResizeDirection.Rows:
                    return XCursor.V;
            }

            return XCursor.Default;
        }

        /// <summary>
        /// Cancels the resizing.
        /// </summary>
        private void CancelResize()
        {
            // Reset the columns'/rows' lengths to the saved values 
            SetDefinitionLength( this.mResizeData.Definition1, this.mResizeData.OriginalDefinition1Length );
            SetDefinitionLength( this.mResizeData.Definition2, this.mResizeData.OriginalDefinition2Length );

            this.mResizeData = null;
        }

        #endregion Methods

        #region Inner Class

        /// <summary>
        /// Definition of the resize data.
        /// </summary>
        private class ResizeData
        {
            //public bool ShowsPreview;
            //public PreviewAdorner Adorner;

            // The constraints to keep the Preview within valid ranges
            public double MinChange { get; set; }
            public double MaxChange { get; set; }

            // The grid to Resize
            public Grid Grid { get; set; }

            // cache of Resize Direction and Behavior
            public GridResizeDirection ResizeDirection { get; set; }
            public GridResizeBehavior ResizeBehavior { get; set; }

            // The columns/rows to resize
            public IDefinition Definition1 { get; set; }
            public IDefinition Definition2 { get; set; }

            // Are the columns/rows star lengths
            public SplitBehavior SplitBehavior { get; set; }

            // The index of the splitter
            public int SplitterIndex { get; set; }

            // The indices of the columns/rows
            public int Definition1Index { get; set; }
            public int Definition2Index { get; set; }

            // The original lengths of Definition1 and Definition2 (to restore lengths if user cancels resize)
            public GridLength OriginalDefinition1Length { get; set; }
            public GridLength OriginalDefinition2Length { get; set; }
            public double OriginalDefinition1ActualLength { get; set; }
            public double OriginalDefinition2ActualLength { get; set; }

            // The minimum of thickness of Splitter.  Used to ensure splitter 
            //isn't hidden by resizing a row/column smaller than the splitter
            public double SplitterThickness { get; set; }
        }

        /// <summary>
        /// Definition of the <see cref="SplitBehavior"/> enumeration.
        /// </summary>
        private enum SplitBehavior
        {
            /// <summary>
            /// Both columns/rows are star lengths
            /// </summary>
            Split,

            /// <summary>
            /// Resize 1 only
            /// </summary>
            Resize1,

            /// <summary>
            /// Resize 2 only
            /// </summary>
            Resize2,
        }

        #endregion Inner Class
    }
}

