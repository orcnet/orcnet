﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="UserControl"/> class.
    /// </summary>
    public class UserControl : ContentControl
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UserControl"/> class.
        /// </summary>
        public UserControl()
        {
            this.HorizontalAlignment = HorizontalAlignment.Stretch;
            this.VerticalAlignment   = VerticalAlignment.Stretch;
        }

        #endregion Constructor
    }
}
