﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="WindowStyle"/> enumeration.
    /// </summary>
    public enum WindowStyle
    {
        /// <summary>
        /// No border at all.
        /// </summary>
        None = 0,

        /// <summary>
        /// Window with border.
        /// </summary>
        BorderedWindow
    }
}
