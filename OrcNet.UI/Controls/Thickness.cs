﻿namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Thickness"/> structure.
    /// </summary>
    public struct Thickness
    {
        #region Properties

        /// <summary>
        /// Gets the maximum width of this thickness.
        /// </summary>
        public double Max
        {
            get
            {
                return System.Math.Max( this.Top,
                                        System.Math.Max( this.Left,
                                        System.Math.Max( this.Right, this.Bottom ) ) );
            }
        }

        /// <summary>
        /// Gets or sets the width of the upper side.
        /// </summary>
        public double Top
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width of the right side.
        /// </summary>
        public double Right
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width of the lower side.
        /// </summary>
        public double Bottom
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width of the left side.
        /// </summary>
        public double Left
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Thickness"/> class.
        /// </summary>
        /// <param name="pUniformLength">The width for all sides.</param>
        public Thickness(double pUniformLength) :
        this( pUniformLength, pUniformLength, pUniformLength, pUniformLength )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Thickness"/> class.
        /// </summary>
        /// <param name="pLeft">The width of the left side.</param>
        /// <param name="pTop">The width of the upper side.</param>
        /// <param name="pRight">The width of the right side.</param>
        /// <param name="pBottom">The width of the lower side.</param>
        public Thickness(double pLeft, double pTop, double pRight, double pBottom)
        {
            this.Top = pTop;
            this.Left = pLeft;
            this.Right = pRight;
            this.Bottom = pBottom;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether two thickness objects are different or not.
        /// </summary>
        /// <param name="pFirst">The first.</param>
        /// <param name="pSecond">The second.</param>
        /// <returns>True if different, false otherwise.</returns>
        public static bool operator !=(Thickness pFirst, Thickness pSecond)
        {
            return pFirst.Equals( pSecond ) == false;
        }

        /// <summary>
        /// Checks whether two thickness objects are different or not.
        /// </summary>
        /// <param name="pFirst">The first.</param>
        /// <param name="pSecond">The second.</param>
        /// <returns>True if different, false otherwise.</returns>
        public static bool operator ==(Thickness pFirst, Thickness pSecond)
        {
            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Checks whether this thickness is equal to another object.
        /// </summary>
        /// <param name="pOther">The other object.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            if ( pOther is Thickness == false )
            {
                return false;
            }

            return this.Equals( (Thickness)pOther );
        }

        /// <summary>
        /// Checks whether this thickness is equal to another thickness.
        /// </summary>
        /// <param name="pOther">The other thickness.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(Thickness pOther)
        {
            return this.Top == pOther.Top &&
                   this.Left == pOther.Left &&
                   this.Right == pOther.Right &&
                   this.Bottom == pOther.Bottom;
        }

        /// <summary>
        /// Gets the thickness hashcode.
        /// </summary>
        /// <returns>The hashcode.</returns>
        public override int GetHashCode()
        {
            return this.Left.GetHashCode() ^ this.Top.GetHashCode() ^ this.Right.GetHashCode() ^ this.Bottom.GetHashCode();
        }

        #endregion Methods
    }
}
