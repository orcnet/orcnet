﻿using Cairo;
using OpenTK;
using OrcNet.UI.Input;
using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="TabPanel"/> class.
    /// 
    /// Manages the TabItem position in the TabControl.
    /// </summary>
    public class TabPanel : APanel
	{
        #region Fields

        /// <summary>
        /// Stores the number of row calculated in measure and used in arrange
        /// </summary>
        private int mRowCount = 1;

        /// <summary>
        /// Stores the number of headers excluding the invisible items
        /// </summary>
        private int mVisibleHeaderCount = 0;

        /// <summary>
        /// Stores the maximum of all headers height
        /// </summary>
        private double mMaximumRowHeight = 0;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the tab control parent.
        /// </summary>
        internal TabControl TabParentInternal
        {
            get
            {
                return this.TemplatedParent as TabControl;
            }
        }

        /// <summary>
        /// Gets the tab strip placement.
        /// </summary>
        private Dock TabStripPlacement
        {
            get
            {
                Dock lPlacement = Dock.Top;
                TabControl lParent = this.TabParentInternal;
                if ( lParent != null )
                {
                    lPlacement = lParent.TabStripPlacement;
                }

                return lPlacement;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TabPanel"/> class.
        /// </summary>
        public TabPanel() : 
        base()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Internal computes the size of the element.
        /// </summary>
        /// <returns>The measured element size.</returns>
        protected override Size MeasureOverride()
        {
            Dock lTabAlignment = this.TabStripPlacement;

            this.mRowCount = 1;
            this.mVisibleHeaderCount = 0;
            this.mMaximumRowHeight = 0;

            // For top and bottom placement the panel flow its children to calculate the number of rows and
            // desired vertical size
            if ( lTabAlignment == Dock.Top || 
                 lTabAlignment == Dock.Bottom )
            {
                int lCurrentRowTabCount = 0;
                double lCurrentRowWidth = 0;
                double lMaxRowWidth = 0;
                foreach ( UIElement lChild in this.InternalChildren )
                {
                    if ( lChild.IsVisible == false )
                    {
                        continue;
                    }
 
                    this.mVisibleHeaderCount++;
 
                    // Helper measures child, and deals with Min, Max, and base Width & Height properties.
                    // Helper returns the size a lChild needs to take up (DesiredSize or property specified size).
                    lChild.Measure();

                    Size lChildSize = this.GetDesiredSizeWithoutMargin( lChild );

                    if ( this.mMaximumRowHeight < lChildSize.Height )
                    {
                        this.mMaximumRowHeight = lChildSize.Height;
                    }
    
                    if ( lCurrentRowWidth + lChildSize.Width > this.Slot.Width && 
                         lCurrentRowTabCount > 0 )
                    { 
                        // If lChild does not fit in the current row - create a new row
                        if ( lMaxRowWidth < lCurrentRowWidth )
                        {
                            lMaxRowWidth = lCurrentRowWidth;
                        }
    
                        lCurrentRowWidth = lChildSize.Width;
                        lCurrentRowTabCount = 1;
                        this.mRowCount++;
                    }
                    else
                    {
                        lCurrentRowWidth += lChildSize.Width;
                        lCurrentRowTabCount++;
                    }
                }

                if (lMaxRowWidth < lCurrentRowWidth)
                {
                    lMaxRowWidth = lCurrentRowWidth;
                }
    
                this.mContentSize.Height = (int)(this.mMaximumRowHeight * this.mRowCount);

                // If we don't have constraint or content wisth is smaller than constraint width then size to content
                if ( double.IsInfinity( this.mContentSize.Width ) || 
                     DoubleUtil.IsNaN( this.mContentSize.Width ) || 
                     lMaxRowWidth < this.Slot.Width )
                {
                    this.mContentSize.Width = (int)lMaxRowWidth;
                }
                else
                {
                    this.mContentSize.Width = this.Slot.Width;
                }
            }
            else if ( lTabAlignment == Dock.Left || 
                      lTabAlignment == Dock.Right )
            {
                foreach ( UIElement lChild in InternalChildren )
                {
                    if ( lChild.IsVisible == false )
                    {
                        continue;
                    }
 
                    this.mVisibleHeaderCount++;
 
                    // Helper measures lChild, and deals with Min, Max, and base Width & Height properties.
                    // Helper returns the size a lChild needs to take up (DesiredSize or property specified size).
                    lChild.Measure();
 
                    Size lChildSize = this.GetDesiredSizeWithoutMargin( lChild );

                    if ( this.mContentSize.Width < lChildSize.Width )
                    {
                        this.mContentSize.Width = lChildSize.Width;
                    }

                    this.mContentSize.Height += lChildSize.Height;
                }
            }

            return base.MeasureOverride();
        }

        /// <summary>
        /// Internal arranges the element layout.
        /// </summary>
        /// <param name="pLayoutType">The layout type to update.</param>
        /// <returns>The new element region.</returns>
        protected override Rectangle ArrangeCore(LayoutingType pLayoutType)
        {
            if ( pLayoutType == LayoutingType.ArrangeChildren )
            {
                Dock lTabAlignment = this.TabStripPlacement;
                if ( lTabAlignment == Dock.Top || 
                     lTabAlignment == Dock.Bottom )
                {
                    this.ArrangeHorizontal();
                }
                else if ( lTabAlignment == Dock.Left || 
                          lTabAlignment == Dock.Right )
                {
                    this.ArrangeVertical();
                }

                return this.Slot;
            }

            return base.ArrangeCore( pLayoutType );
        }

        /// <summary>
        /// Delegate called on render.
        /// </summary>
        /// <param name="pDrawingContext">The drawing context.</param>
        protected override void OnRender(Context pDrawingContext)
        {
            Rectangle lBackground = new Rectangle( this.mSlot.Size );

            this.Background.SetAsSource( pDrawingContext, lBackground );

            CairoHelpers.CairoRectangle( pDrawingContext, lBackground, -1 );

            pDrawingContext.Fill();

            pDrawingContext.Save();

            if ( this.ClipToClientRect )
            {
                // clip to client zone
                CairoHelpers.CairoRectangle( pDrawingContext, this.ClientRectangle, -1 );
                pDrawingContext.Clip();
            }

            TabControl lParent = this.TabParentInternal;
            UIElement lSelected = null;
            if ( lParent != null )
            {
                lSelected = lParent.SelectedContent as UIElement;
            }

            lock ( this.mSyncRoot )
            {
                // Draw the children unless the selected one it any.
                if ( this.Children != null )
                {
                    foreach ( UIElement lElement in this.Children )
                    {
                        if ( lElement == lSelected )
                        {
                            continue;
                        }

                        lElement.Paint( ref pDrawingContext );
                    }
                }
            }

            // Draw the selected one last.
            if ( lSelected != null )
            {
                lSelected.Paint( ref pDrawingContext );
            }

            pDrawingContext.Restore();
        }

        /// <summary>
        /// Delegate called on mouse hover.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal override void OnMouseHover(MouseMoveEventArgs pEventArgs)
        {
            if ( InputManager.Instance.HoverElement != this )
            {
                InputManager.Instance.HoverElement = this;
                this.OnMouseEnter( this, pEventArgs );
            }

            TabControl lParentControl = this.TabParentInternal;
            UIElement lSelected = null;
            if ( lParentControl != null )
            {
                lSelected = lParentControl.SelectedContent as UIElement;
            }

            if ( lSelected.IsMouseIn( pEventArgs.Position ) )
            {
                lSelected.OnMouseHover( pEventArgs );
                return;
            }
            
            if ( this.Children != null )
            {
                for ( int lCurr = this.Children.Count - 1; lCurr >= 0; lCurr-- )
                {
                    TabItem lChild = this.Children[ lCurr ] as TabItem;
                    UIElement lHeader = lChild.Header as UIElement;
                    if ( lHeader != null && 
                         lHeader.IsMouseIn( pEventArgs.Position ) )
                    {
                        lChild.OnMouseHover( pEventArgs );
                        return;
                    }
                }
            }

            base.OnMouseHover( pEventArgs );
        }
        
        /// <summary>
        /// Arranges the panel children horizontally.
        /// </summary>
        private void ArrangeHorizontal()
        {
            Dock lTabAlignment = this.TabStripPlacement;
            bool lIsMultiRow = this.mRowCount > 1;
            int lActiveRow = 0;
            int[] lSolution = new int[0];
            Vector2d lChildOffset = new Vector2d();
            double[] lHeadersSizes = this.GetHeadersSize();

            // If we have multirows, then calculate the best header distribution
            if ( lIsMultiRow )
            {
                lSolution  = this.CalculateHeaderDistribution( this.Slot.Width, lHeadersSizes );
                lActiveRow = this.GetActiveRow( lSolution );

                // TabPanel starts to layout children depend on lActiveRow which should be always on bottom (top)
                // The first row should start from Y = (this.mRowCount - 1 - lActiveRow) * this.mMaximumRowHeight
                if (lTabAlignment == Dock.Top)
                {
                    lChildOffset.Y = (this.mRowCount - 1 - lActiveRow) * this.mMaximumRowHeight;
                }

                if (lTabAlignment == Dock.Bottom && lActiveRow != 0)
                {
                    lChildOffset.Y = (this.mRowCount - lActiveRow) * this.mMaximumRowHeight;
                }
            }

            int lChildIndex = 0;
            int lSeparatorIndex = 0;
            foreach ( UIElement lChild in this.InternalChildren )
            {
                if ( lChild.IsVisible == false )
                {
                    continue;
                }

                TabItem lItem = lChild as TabItem;
                Thickness lMargin = lItem != null ? lItem.Margin : new Thickness( 0 );
                double lLeftOffset = lMargin.Left;
                double lRightOffset = lMargin.Right;
                double lTopOffset = lMargin.Top;
                double lBottomOffset = lMargin.Bottom;

                bool lLastHeaderInRow = lIsMultiRow && (lSeparatorIndex < lSolution.Length && lSolution[ lSeparatorIndex ] == lChildIndex || lChildIndex == this.mVisibleHeaderCount - 1);

                // Length left, top, right, bottom;
                Size lCellSize = new Size( (int)lHeadersSizes[ lChildIndex ], (int)this.mMaximumRowHeight );

                // Align the last header in the row; If headers are not aligned directional nav would not work correctly
                if ( lLastHeaderInRow )
                {
                    lCellSize.Width = (int)(this.Slot.Width - lChildOffset.X);
                }

                lChild.Slot = new Rectangle( (int)lChildOffset.X, (int)lChildOffset.Y, lCellSize.Width, lCellSize.Height );

                Size lChildSize = lCellSize;
                lChildSize.Height = (int)Math.Max(0d, lChildSize.Height - lTopOffset - lBottomOffset);
                lChildSize.Width  = (int)Math.Max(0d, lChildSize.Width - lLeftOffset - lRightOffset);

                // Calculate the offset for the next lChild
                lChildOffset.X += lCellSize.Width;
                if ( lLastHeaderInRow )
                {
                    if ( (lSeparatorIndex == lActiveRow && lTabAlignment == Dock.Top) ||
                         (lSeparatorIndex == lActiveRow - 1 && lTabAlignment == Dock.Bottom) )
                    {
                        lChildOffset.Y = 0d;
                    }
                    else
                    {
                        lChildOffset.Y += this.mMaximumRowHeight;
                    }

                    lChildOffset.X = 0d;
                    lSeparatorIndex++;
                }

                lChildIndex++;
            }
        }

        /// <summary>
        /// Arranges the panel children vertically.
        /// </summary>
        private void ArrangeVertical()
        {
            int lChildOffsetY = 0;
            foreach ( UIElement lChild in InternalChildren )
            {
                if ( lChild.IsVisible )
                {
                    Size lChildSize = this.GetDesiredSizeWithoutMargin( lChild );
                    lChild.Slot = new Rectangle( 0, lChildOffsetY, this.Slot.Width, lChildSize.Height);

                    // Calculate the offset for the next child
                    lChildOffsetY += lChildSize.Height;
                }
            }
        }

        /// <summary>
        /// Gets the desired size without margin.
        /// </summary>
        /// <param name="pElement"></param>
        /// <returns>The desired size without margin if any.</returns>
        private Size GetDesiredSizeWithoutMargin(UIElement pElement)
        {
            Thickness lMargin = new Thickness( 0 );
            if ( pElement is FrameworkElement )
            {
                lMargin = (pElement as FrameworkElement).Margin;
            }
            Size lDesiredSizeWithoutMargin = new Size();
            lDesiredSizeWithoutMargin.Height = (int)Math.Max( 0d, pElement.DesiredSize.Height - lMargin.Top - lMargin.Bottom );
            lDesiredSizeWithoutMargin.Width  = (int)Math.Max( 0d, pElement.DesiredSize.Width - lMargin.Left - lMargin.Right );

            return lDesiredSizeWithoutMargin;
        }

        /// <summary>
        /// Gets the headers sizes.
        /// </summary>
        /// <returns>THe set of headers sizes.</returns>
        private double[] GetHeadersSize()
        {
            double[] lHeaderSize = new double[ this.mVisibleHeaderCount ];
            int lChildIndex = 0;
            foreach ( UIElement lChild in this.InternalChildren )
            {
                if ( lChild.IsVisible == false )
                {
                    continue;
                }

                Size lChildSize = this.GetDesiredSizeWithoutMargin( lChild );
                lHeaderSize[ lChildIndex ] = lChildSize.Width;
                lChildIndex++;
            }

            return lHeaderSize;
        }

        /// <summary>
        /// Gets the active row in the panel.
        /// </summary>
        /// <param name="pSolution"></param>
        /// <returns></returns>
        private int GetActiveRow(int[] pSolution)
        {
            int lActiveRow  = 0;
            int lChildIndex = 0;
            if ( pSolution.Length > 0 )
            {
                foreach ( UIElement lChild in this.InternalChildren )
                {
                    if ( lChild.IsVisible == false )
                    {
                        continue;
                    }

                    TabItem lItem = lChild as TabItem;
                    bool lIsActiveTab = lItem != null ? lItem.IsSelected : false;

                    if ( lIsActiveTab )
                    {
                        return lActiveRow;
                    }

                    if ( lActiveRow < pSolution.Length && 
                         pSolution[ lActiveRow ] == lChildIndex )
                    {
                        lActiveRow++;
                    }

                    lChildIndex++;
                }
            }

            // If there is no selected element and aligment is Top - then the active row is the last row 
            if ( this.TabStripPlacement == Dock.Top )
            {
                lActiveRow = this.mRowCount - 1;
            }

            return lActiveRow;
        }

        /// <summary>
        /// Computes the headers distribution.
        /// </summary>
        /// <param name="pRowWidthLimit">The row width limit.</param>
        /// <param name="pHeadersWidth">The set of width of all visible headers.</param>
        /// <returns>The headers distribution solution.</returns>
        private int[] CalculateHeaderDistribution(double pRowWidthLimit, double[] pHeadersWidth)
        {
            double lBestSolutionMaxRowAverageGap = 0;
            int lHeaderCount = pHeadersWidth.Length;

            int lSeparatorCount = this.mRowCount - 1;
            double lCurrentRowWidth = 0;
            int lNumberOfHeadersInCurrentRow = 0;
            double lCurrentAverageGap = 0;
            int[] lCurrentSolution = new int[lSeparatorCount];
            int[] lBestSolution = new int[lSeparatorCount];
            int[] lRowHeaderCount = new int[this.mRowCount];
            double[] lRowWidth = new double[this.mRowCount];
            double[] lRowAverageGap = new double[this.mRowCount];
            double[] lBestSolutionRowAverageGap = new double[this.mRowCount];

            // Initialize the current state; Do the initial flow of the headers
            int lCurrentRowIndex = 0;
            for ( int lIndex = 0; lIndex < lHeaderCount; lIndex++ )
            {
                if (lCurrentRowWidth + pHeadersWidth[lIndex] > pRowWidthLimit && lNumberOfHeadersInCurrentRow > 0)
                { 
                    // if we cannot add next header - flow to next row
                    // Store current row before we go to the next
                    lRowWidth[lCurrentRowIndex] = lCurrentRowWidth; // Store the current row width
                    lRowHeaderCount[lCurrentRowIndex] = lNumberOfHeadersInCurrentRow; // For each row we store the number os headers inside
                    lCurrentAverageGap = Math.Max(0d, (pRowWidthLimit - lCurrentRowWidth) / lNumberOfHeadersInCurrentRow); // The amout of width that should be added to justify the header
                    lRowAverageGap[lCurrentRowIndex] = lCurrentAverageGap;
                    lCurrentSolution[lCurrentRowIndex] = lIndex - 1; // Separator points to the last header in the row
                    if (lBestSolutionMaxRowAverageGap < lCurrentAverageGap) // Remember the maximum of all lCurrentAverageGap
                    {
                        lBestSolutionMaxRowAverageGap = lCurrentAverageGap;
                    }

                    // Iterate to next row
                    lCurrentRowIndex++;
                    lCurrentRowWidth = pHeadersWidth[lIndex]; // Accumulate header widths on the same row
                    lNumberOfHeadersInCurrentRow = 1;
                }
                else
                {
                    lCurrentRowWidth += pHeadersWidth[lIndex]; // Accumulate header widths on the same row
                    // Increase the number of headers only if they are not collapsed (width=0)
                    if (pHeadersWidth[lIndex] != 0)
                    {
                        lNumberOfHeadersInCurrentRow++;
                    }
                }
            }

            // If everithing fit in 1 row then exit (no separators needed)
            if (lCurrentRowIndex == 0)
            {
                return new int[0];
            }

            // Add the last row
            lRowWidth[lCurrentRowIndex] = lCurrentRowWidth;
            lRowHeaderCount[lCurrentRowIndex] = lNumberOfHeadersInCurrentRow;
            lCurrentAverageGap = (pRowWidthLimit - lCurrentRowWidth) / lNumberOfHeadersInCurrentRow;
            lRowAverageGap[lCurrentRowIndex] = lCurrentAverageGap;
            if (lBestSolutionMaxRowAverageGap < lCurrentAverageGap)
            {
                lBestSolutionMaxRowAverageGap = lCurrentAverageGap;
            }

            lCurrentSolution.CopyTo(lBestSolution, 0); // Remember the first solution as initial lBestSolution
            lRowAverageGap.CopyTo(lBestSolutionRowAverageGap, 0); // lBestSolutionRowAverageGap is used in ArrangeOverride to calculate header sizes

            // Search for the best solution
            // The exit condition if when we cannot move header to the next row 
            while (true)
            {
                // Find the row with maximum AverageGap
                int lWorstRowIndex = 0; // Keep the row lIndex with maximum AverageGap
                double lMaxAverageGap = 0;

                for (int i = 0; i < this.mRowCount; i++) // for all rows
                {
                    if (lMaxAverageGap < lRowAverageGap[i])
                    {
                        lMaxAverageGap = lRowAverageGap[i];
                        lWorstRowIndex = i;
                    }
                }

                // If we are on the first row - cannot move from previous
                if (lWorstRowIndex == 0)
                {
                    break;
                }

                // From the row with maximum AverageGap we try to move a header from previous row
                int lMoveToRow = lWorstRowIndex;
                int lMoveFromRow = lMoveToRow - 1;
                int lMoveHeader = lCurrentSolution[lMoveFromRow];
                double lMovedHeaderWidth = pHeadersWidth[lMoveHeader];

                lRowWidth[lMoveToRow] += lMovedHeaderWidth;

                // If the moved header cannot fit - exit. We have the best solution already.
                if (lRowWidth[lMoveToRow] > pRowWidthLimit)
                {
                    break;
                }

                // If header is moved successfully to the worst row
                // we update the arrays keeping the row state
                lCurrentSolution[lMoveFromRow]--;
                lRowHeaderCount[lMoveToRow]++;
                lRowWidth[lMoveFromRow] -= lMovedHeaderWidth;
                lRowHeaderCount[lMoveFromRow]--;
                lRowAverageGap[lMoveFromRow] = (pRowWidthLimit - lRowWidth[lMoveFromRow]) / lRowHeaderCount[lMoveFromRow];
                lRowAverageGap[lMoveToRow] = (pRowWidthLimit - lRowWidth[lMoveToRow]) / lRowHeaderCount[lMoveToRow];

                // EvaluateSolution:
                // If the current solution is better than lBestSolution - keep it in lBestSolution
                lMaxAverageGap = 0;
                for (int i = 0; i < this.mRowCount; i++) // for all rows
                {
                    if (lMaxAverageGap < lRowAverageGap[i])
                    {
                        lMaxAverageGap = lRowAverageGap[i];
                    }
                }

                if (lMaxAverageGap < lBestSolutionMaxRowAverageGap)
                {
                    lBestSolutionMaxRowAverageGap = lMaxAverageGap;
                    lCurrentSolution.CopyTo(lBestSolution, 0);
                    lRowAverageGap.CopyTo(lBestSolutionRowAverageGap, 0);
                }
            }

            // Each header size should be increased so headers in the row stretch to fit the row
            lCurrentRowIndex = 0;
            for (int lIndex = 0; lIndex < lHeaderCount; lIndex++)
            {
                pHeadersWidth[lIndex] += lBestSolutionRowAverageGap[lCurrentRowIndex];
                if (lCurrentRowIndex < lSeparatorCount && lBestSolution[lCurrentRowIndex] == lIndex)
                {
                    lCurrentRowIndex++;
                }
            }

            // Use the best solution lBestSolution[0..lSeparatorCount-1] to layout
            return lBestSolution;
        }

        #endregion Methods
    }
}

