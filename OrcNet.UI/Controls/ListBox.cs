﻿using System.Collections;
using System.Collections.Generic;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="ListBox"/> class.
    /// </summary>
	public class ListBox : Selector
	{
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the control key is pressed or not.
        /// </summary>
        private bool mIsCtrlPressed;

        /// <summary>
        /// Stores the selected items.
        /// </summary>
        private HashSet<object> mSelection = new HashSet<object>();

        /// <summary>
        /// Stores the selection mode.
        /// </summary>
        private SelectionMode mSelectionMode;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the selected items.
        /// </summary>
        public IList SelectedItems
        {
            get
            {
                if ( this.mSelectionMode == SelectionMode.Single )
                {
                    return new List<object>() { this.SelectedItem };
                }
                else
                {
                    return new List<object>( this.mSelection );
                }
            }
        }

        /// <summary>
        /// Gets or sets the selection mode.
        /// </summary>
        public SelectionMode SelectionMode
        {
            get
            {
                return this.mSelectionMode;
            }
            set
            {
                this.mSelectionMode = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ListBox"/> class.
        /// </summary>
        public ListBox () : 
        base()
        {
            this.mSelectionMode = SelectionMode.Single;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Selects all items.
        /// </summary>
        public void SelectAll()
        {
            this.mSelection.Clear();
            foreach ( object lItem in this.ItemSource )
            {
                this.mSelection.Add( lItem );
            }
        }

        /// <summary>
        /// Unselects all items.
        /// </summary>
        public void UnselectAll()
        {
            this.mSelection.Clear();
        }

        /// <summary>
        /// Delegate called on key pressed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        protected internal override void OnKeyDown(object pSender, KeyboardKeyEventArgs pEventArgs)
        {
            base.OnKeyDown( pSender, pEventArgs );

            this.mIsCtrlPressed = false;

            if ( this.mSelectionMode == SelectionMode.Multiple )
            {
                this.mIsCtrlPressed = pEventArgs.Control;
            }
        }

        /// <summary>
        /// Delegate called on selection changes.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected override void OnSelectionChanged(object pSender, SelectionChangeEventArgs<object> pEventArgs)
        {
            base.OnSelectionChanged( pSender, pEventArgs );

            if ( this.mIsCtrlPressed )
            {
                this.mSelection.Add( this.SelectedItem );
            }
            else
            {
                this.mSelection.Clear();
            }
        }

        #endregion Methods
    }
}

