﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace OrcNet.UI.GraphicObjects
{
    /// <summary>
    /// Definition of the <see cref="ICollectionView"/> interface.
    /// </summary>
    public interface ICollectionView : IEnumerable, INotifyCollectionChanged
    {
        #region Events

        ///<summary>
        /// Raise this event before changing currency.
        ///</summary>
        event EventHandler CurrentChanging;

        ///<summary>
        ///Raise this event after changing currency.
        ///</summary>
        event EventHandler CurrentChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the source (unfiltered) collection.
        /// </summary>
        IEnumerable SourceCollection
        {
            get;
        }

        /// <summary>
        /// Gets or sets the filter callback.
        /// </summary>
        Predicate<object> Filter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the flag indicating whether the collection can be filtered or not.
        /// </summary>
        bool CanFilter
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the collection can be sorted or not.
        /// </summary>
        bool CanSort
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the collection can be grouped or not.
        /// </summary>
        bool CanGroup
        {
            get;
        }

        /// <summary>
        /// Gets the collection count.
        /// </summary>
        int Count
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the collection is empty or not.
        /// </summary>
        bool IsEmpty
        {
            get;
        }

        /// <summary>
        /// Gets the current item selected in the collection view.
        /// </summary>
        object CurrentItem
        {
            get;
        }

        /// <summary>
        /// Gets the current item index selected in the collection view.
        /// </summary>
        int CurrentPosition
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Checks whether the given element is in the collection or not.
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        bool Contains(object pItem);

        /// <summary>
        /// Set the first element as current.
        /// </summary>
        /// <returns>True if successful, false otherwise.</returns>
        bool MoveCurrentToFirst();

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the last item.
        /// </summary>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        bool MoveCurrentToLast();

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the next item.
        /// </summary>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        bool MoveCurrentToNext();

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the previous item.
        /// </summary>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        bool MoveCurrentToPrevious();

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the given item.
        /// </summary>
        /// <param name="pItem">Move CurrentItem to this item.</param>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        bool MoveCurrentTo(object pItem);

        /// <summary>
        /// Move <seealso cref="CurrentItem"/> to the item at the given index.
        /// </summary>
        /// <param name="pPosition">Move CurrentItem to this index</param>
        /// <returns>true if <seealso cref="CurrentItem"/> points to an item within the view.</returns>
        bool MoveCurrentToPosition(int pPosition);

        #endregion Methods
    }
}
