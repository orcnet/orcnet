﻿using System;

namespace OrcNet.UI
{
    /// <summary>
    /// Definition of the <see cref="Expander"/> class.
    /// </summary>
    public class Expander : HeaderedContentControl
    {
        #region Fields

        /// <summary>
        /// Stores the constant expander toogle button template name.
        /// </summary>
        private const string ExpanderToggleButtonTemplateName = "HeaderSite";

        /// <summary>
        /// Stores the flag indicating whether the expander is expanded or not.
        /// </summary>
        private bool mIsExpanded;

        /// <summary>
        /// Stores the expander toggle button.
        /// </summary>
        ToggleButton mExpanderToogleButton;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on expander expanded.
        /// </summary>
        public event EventHandler Expanded;

        /// <summary>
        /// Event fired on expander collapsed.
        /// </summary>
        public event EventHandler Collapsed;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the expander is expanded or not.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return this.mIsExpanded;
            }
            set
            {
                if (value == this.mIsExpanded)
                {
                    return;
                }

                this.mIsExpanded = value;
                
                this.NotifyPropertyChanged();

                if ( this.mIsExpanded )
                {
                    this.OnExpanded( null );
                }
                else
                {
                    this.OnCollapsed( null );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Expander"/> class.
        /// </summary>
        public Expander() : 
        base()
		{
            this.mIsExpanded = false;
		}

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on apply template.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.mExpanderToogleButton = this.GetTemplateChild( ExpanderToggleButtonTemplateName ) as ToggleButton;
        }
        
        /// <summary>
        /// Delegate called on expander expanded.
        /// </summary>
        /// <param name="pEventArgs"></param>
		protected virtual void OnExpanded(EventArgs pEventArgs)
		{
			this.NotifyExpanded( pEventArgs );
		}

        /// <summary>
        /// Delegate called on expander collapsed
        /// </summary>
        /// <param name="pEventArgs"></param>
		protected virtual void OnCollapsed(EventArgs pEventArgs)
		{
			this.NotifyCollapsed( pEventArgs );
		}

        /// <summary>
        /// Notifies the expander is expanded.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifyExpanded(EventArgs pEventArgs)
        {
            if ( this.Expanded != null )
            {
                this.Expanded( this, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies the expander is collapsed.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifyCollapsed(EventArgs pEventArgs)
        {
            if ( this.Collapsed != null )
            {
                this.Collapsed( this, pEventArgs );
            }
        }

        #endregion Methods
    }
}
