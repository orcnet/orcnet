﻿using Avalonia.Controls;
using Avalonia.Gtk.Embedding;
using Avalonia.Markup.Xaml;
using OpenTK;
using OrcNet.Core.Logger;
using System;

namespace OrcNet
{
    /// <summary>
    /// Definition of the <see cref="SampleWindow"/> class.
    /// </summary>
    public class SampleWindow : Window
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleWindow"/> class.
        /// </summary>
        /// <param name="pTitle">The window title.</param>
        public SampleWindow(string pTitle = "OrcNet Samples") :
        base()
        {
            this.InitializeComponent();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes the main window.
        /// </summary>
        private void InitializeComponent()
        {
            Core.Application.Instance.Initialize( LoggerType.CONSOLE );

            //HBox lRoot = new HBox();
            //NativeWindow lFakeWindow = new NativeWindow();
            //MainView lView = new MainView( lFakeWindow );
            //GtkAvaloniaControlHost lHost = new GtkAvaloniaControlHost()
            //{
            //    Content = lView
            //};
            //lHost.SetSizeRequest( 1000, 800 );
            //lHost.Destroyed += this.OnWindowDestroyed;

            //lRoot.PackStart( lHost, true, true, 0 );

            //this.Add( lRoot );
            //this.ShowAll();

            AvaloniaXamlLoader.Load( this );
        }

        /// <summary>
        /// Delegate called on window destroyed.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnWindowDestroyed(object pSender, EventArgs pEventArgs)
        {
            GtkAvaloniaControlHost lCast = pSender as GtkAvaloniaControlHost;
            lCast.Destroyed -= this.OnWindowDestroyed;

            // Clean files.
            Core.Logger.LogManager.Instance.Dispose();

            Environment.Exit(0);
        }

        #endregion Methods
    }
}
