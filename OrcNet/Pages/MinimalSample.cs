﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Math;
using OrcNet.Core.Service;
using OrcNet.Core.UI;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Services;
using OrcNet.UI.View;
using System.Runtime.InteropServices;
using System.Text;

namespace OrcNet.Pages
{
    /// <summary>
    /// Definition of the <see cref="MinimalSample"/> class.
    /// </summary>
    public class MinimalSample : ASample
    {
        #region Fields

        /// <summary>
        /// Stores the texture data ptr.
        /// </summary>
        private GCHandle mTexturePtr;
        
        /// <summary>
        /// Stores the sample's minimal mesh to render.
        /// </summary>
        private Mesh<Vertex2, uint> mMesh;

        /// <summary>
        /// Stores the pipeline pass use to render the mesh.
        /// </summary>
        private PipelinePass mPass;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MinimalSample"/> class.
        /// </summary>
        /// <param name="pView">The view sample content will be rendered to.</param>
        public MinimalSample(OrcView pView) :
        base( pView, new ViewParameters() {  }.SetSize( 512, 512 ) )
        {

        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the sample.
        /// </summary>
        protected override void OnInitialize()
        {
            this.mMesh = new Mesh<Vertex2, uint>( PrimitiveType.TriangleStrip, MeshUsage.GPU_STATIC );
            this.mMesh.AddAttributeType( 0, 2, AttributeType.FLOAT_BASED, false );

            // Adds the vertices
            this.mMesh.AddVertex( new Vertex2( -1, -1 ) );
            this.mMesh.AddVertex( new Vertex2( +1, -1 ) );
            this.mMesh.AddVertex( new Vertex2( -1, +1 ) );
            this.mMesh.AddVertex( new Vertex2( +1, +1 ) );

            // Creates a 2D texture with 4x4 pixels, using one 8bits channel
            // per pixel, with a magnification filter in nearest mode
            byte[] lData = 
            {
                0, 255, 0, 255,
                255, 0, 255, 0,
                0, 255, 0, 255,
                255, 0, 255, 0
            };

            this.mTexturePtr = GCHandle.Alloc( lData, GCHandleType.Pinned );

            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            BufferLayoutParameters lBufferParameters = new BufferLayoutParameters();
            Texture2D lTexture = new Texture2D( 4, 4, 
                                                PixelInternalFormat.R8, 
                                                PixelFormat.Red,
                                                PixelType.UnsignedByte,
                                                lTextureParameters,
                                                lBufferParameters, 
                                                new CPUBuffer( this.mTexturePtr.AddrOfPinnedObject() ) );

            // Creates a program made of a single module,
            // itself made of a single fragment shader
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform sampler2D sampler;" );
            lBuilder.AppendLine( "uniform vec2 scale;" );
            lBuilder.AppendLine( "layout(location = 0) out vec4 data;" );
            lBuilder.AppendLine( "void main() { data = texture(sampler, gl_FragCoord.xy * scale).rrrr; }" );
            this.mPass = new PipelinePass( new PipelineDescription( 
                                           new PipelineStage( 330, 
                                                              lBuilder.ToString(), 
                                                              StageType.FRAGMENT ) ) );

            // Sets the texture.
            TextureUniform lUniform = this.mPass.GetUniform<TextureUniform>( new UniformName( "sampler" ) );
            if ( lUniform != null )
            {
                lUniform.Value = lTexture;
            }

            base.OnInitialize();
        }

        /// <summary>
        /// Internal render method to override.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute time.</param>
        /// <param name="pDeltaTime">The elasped time since last render.</param>
        /// <returns>True if anything has been rendered, false otherwise.</returns>
        protected override bool OnRender(double pAbsoluteTime, double pDeltaTime)
        {
            this.FrameBuffer.Clear( true, false, false );

            this.FrameBuffer.Draw( this.mPass, this.mMesh );

            return true;
        }

        /// <summary>
        /// Internal resize method to override.
        /// </summary>
        /// <param name="pWidth">The new width.</param>
        /// <param name="pHeight">The new height.</param>
        protected override void OnResize(int pWidth, int pHeight)
        {
            base.OnResize( pWidth, pHeight );
            
            Vector2FUniform lUniform = this.mPass.GetUniform<Vector2FUniform>( new UniformName( "scale" ) );
            if ( lUniform != null )
            {
                lUniform.Value = new Vector2F( 1.0f / pWidth, 
                                               1.0f / pHeight );
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDisposed()
        {
            this.mView.Rendering -= this.OnRender;
            this.mView.Resizing  -= this.OnResize;
            
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.Dispose();
            }

            this.mTexturePtr.Free();

            if ( this.mMesh != null )
            {
                this.mMesh.Dispose();
                this.mMesh = null;
            }

            if ( this.mPass != null )
            {
                this.mPass.Dispose();
                this.mPass = null;
            }

            base.OnDisposed();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
