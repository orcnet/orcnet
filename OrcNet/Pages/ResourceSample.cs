﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Math;
using OrcNet.Core.Resource;
using OrcNet.Core.Resource.Loaders;
using OrcNet.Core.Service;
using OrcNet.Core.UI;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Services;
using OrcNet.UI.View;
using System;
using System.IO;

namespace OrcNet.Pages
{
    /// <summary>
    /// Definition of the <see cref="ResourceSample"/> class.
    /// </summary>
    public class ResourceSample : ASample
    {
        #region Fields
        
        /// <summary>
        /// Stores the cube mesh.
        /// </summary>
        private MeshBuffers mCube;

        /// <summary>
        /// Stores the plane mesh.
        /// </summary>
        private MeshBuffers mPlane;

        /// <summary>
        /// Stores the first pipeline pass.
        /// </summary>
        private PipelinePass mPass1;

        /// <summary>
        /// Stores the second pipeline pass.
        /// </summary>
        private PipelinePass mPass2;

        /// <summary>
        /// Stores the world camera uniform.
        /// </summary>
        private Vector3FUniform mWorldCamera;

        /// <summary>
        /// Stores the local to world matrix uniform of the first object.
        /// </summary>
        private Matrix4FUniform mLocalToWorld1;

        /// <summary>
        /// Stores the local to screen matrix uniform of the first object.
        /// </summary>
        private Matrix4FUniform mLocalToScreen1;

        /// <summary>
        /// Stores the local to world matrix uniform of the second object.
        /// </summary>
        private Matrix4FUniform mLocalToWorld2;

        /// <summary>
        /// Stores the local to screen matrix uniform of the second object.
        /// </summary>
        private Matrix4FUniform mLocalToScreen2;

        /// <summary>
        /// Stores the field of view in degrees.
        /// </summary>
        private float mFov;

        /// <summary>
        /// Stores the alpha camera rotation angle in degrees.
        /// </summary>
        private float mAlpha;

        /// <summary>
        /// Stores the theta camera rotation angle in degrees.
        /// </summary>
        private float mTheta;

        /// <summary>
        /// Stores the distance of the camera to the scene.
        /// </summary>
        private float mDistance;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceSample"/> class.
        /// </summary>
        /// <param name="pView">The view sample content will be rendered to.</param>
        public ResourceSample(OrcView pView) :
        base( pView, new ViewParameters() { UseDepth = true }.SetSize( 1024, 768 ) )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the sample.
        /// </summary>
        protected override void OnInitialize()
        {
            this.mView.MouseMoved   += this.OnMouseMoved;
            this.mView.MouseWheeled += this.OnMouseWheeled;

            this.mFov      = 80.0f;
            this.mAlpha    = 135.0f;
            this.mTheta    = 45.0f;
            this.mDistance = 15.0f;
            
            string lAssetDirectory    = Path.Combine( Environment.CurrentDirectory, @"..\Resources\Assets");
            string lTexturesDirectory = Path.Combine( lAssetDirectory, "Textures" );
            string lShadersDirectory  = Path.Combine( lAssetDirectory, "Shaders" );
            string lMeshesDirectory   = Path.Combine( lAssetDirectory, "Meshes" );

            XMLResourceLoader lLoader = new XMLResourceLoader();
            lLoader.AddPath( lTexturesDirectory );
            lLoader.AddPath( lShadersDirectory );
            lLoader.AddPath( lMeshesDirectory );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );
            
            IResource lCubeResource = lResourceService.LoadResource( "cube.mesh" );
            if ( lCubeResource != null )
            {
                this.mCube = lCubeResource.OwnedObject as MeshBuffers;
            }

            IResource lPlaneResource = lResourceService.LoadResource( "plane.mesh" );
            if ( lPlaneResource != null )
            {
                this.mPlane = lPlaneResource.OwnedObject as MeshBuffers;
            }

            IResource lPass1Resource = lResourceService.LoadResource( "camera;spotlight;plastic;" );
            if ( lPass1Resource != null )
            {
                this.mPass1 = lPass1Resource.OwnedObject as PipelinePass;
            }

            IResource lPass2Resource = lResourceService.LoadResource( "camera;spotlight;texturedPlastic;" );
            if ( lPass2Resource != null )
            {
                this.mPass2 = lPass2Resource.OwnedObject as PipelinePass;
            }
            
            this.mWorldCamera    = this.mPass1.GetUniform<Vector3FUniform>( new UniformName( "worldCameraPos" ) );
            this.mLocalToWorld1  = this.mPass1.GetUniform<Matrix4FUniform>( new UniformName( "localToWorld" ) );
            this.mLocalToScreen1 = this.mPass1.GetUniform<Matrix4FUniform>( new UniformName( "localToScreen" ) );
            this.mLocalToWorld2  = this.mPass2.GetUniform<Matrix4FUniform>( new UniformName( "localToWorld" ) );
            this.mLocalToScreen2 = this.mPass2.GetUniform<Matrix4FUniform>( new UniformName( "localToScreen" ) );
            
            this.FrameBuffer.Parameters.DepthDescription.DepthTest( true, DepthFunction.Less );

            base.OnInitialize();
        }
        
        /// <summary>
        /// Internal render method to override.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute time.</param>
        /// <param name="pDeltaTime">The elasped time since last render.</param>
        /// <returns>True if anything has been rendered, false otherwise.</returns>
        protected override bool OnRender(double pAbsoluteTime, double pDeltaTime)
        {
            // Update resources if changed.
            //IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            //lResourceService.UpdateResources();

            this.FrameBuffer.Clear( true, false, true );

            AMatrix<float> lCameraToWorld = Matrix4F.RotateX( 90 );
            lCameraToWorld = lCameraToWorld * Matrix4F.RotateY(-this.mAlpha);
            lCameraToWorld = lCameraToWorld * Matrix4F.RotateX(-this.mTheta);
            lCameraToWorld = lCameraToWorld * Matrix4F.Translate( new Vector3F( 0.0f, 0.0f, this.mDistance ));

            bool lResult;
            AMatrix<float> lWorldToCamera = lCameraToWorld.Inverse( float.Epsilon, out lResult ) as AMatrix<float>;

            Viewport lViewport = this.FrameBuffer.Parameters[ new FrameBufferParameters.ViewportIndex( 0 ) ];
            float lWidth  = lViewport.Width;
            float lHeight = lViewport.Height;
            float lFov = (float)MathUtility.ToDegrees( 2 * System.Math.Atan( lHeight / lWidth * System.Math.Tan( MathUtility.ToRadians( this.mFov / 2 ) ) ) );
            Matrix4F lCameraToScreen = Matrix4F.PerspectiveProjection( lFov, lWidth / lHeight, 0.1f, 1e5f );

            this.mWorldCamera.Value = lCameraToWorld.Multiply( Vector3F.ZERO ) as Vector3F;

            this.mLocalToWorld1.Value  = Matrix4F.RotateZ( 15.0f );
            this.mLocalToScreen1.Value = lCameraToScreen * lWorldToCamera * Matrix4F.RotateZ( 15.0f );
            this.FrameBuffer.Draw( this.mPass1, this.mCube, this.mCube.PrimitiveMode, 0, this.mCube.VertexCount, 1, this.mCube.IndexCount );

            this.mLocalToWorld2.Value  = Matrix4F.Translate( new Vector3F( 0.0f, 0.0f, -2.0f ) ) * Matrix4F.RotateZ( 180.0f );
            this.mLocalToScreen2.Value = lCameraToScreen * lWorldToCamera * Matrix4F.Translate( new Vector3F( 0.0f, 0.0f, -2.0f ) ) * Matrix4F.RotateZ( 180.0f );
            this.FrameBuffer.Draw( this.mPass2, this.mPlane, this.mPlane.PrimitiveMode, 0, this.mPlane.VertexCount, 1, this.mPlane.IndexCount );

            return true;
        }

        /// <summary>
        /// Internal resize method to override.
        /// </summary>
        /// <param name="pWidth">The new width.</param>
        /// <param name="pHeight">The new height.</param>
        protected override void OnResize(int pWidth, int pHeight)
        {
            base.OnResize( pWidth, pHeight );
            
            // Reinit the depth test function.
            this.FrameBuffer.Parameters.DepthDescription.DepthTest( true, DepthFunction.Less );
        }

        /// <summary>
        /// Delegate called on mouse wheeled events.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnMouseWheeled(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.Delta > 0 ) // Wheel up?
            {
                this.mDistance *= 1.05f;
            }
            else
            {
                this.mDistance /= 1.05f;
            }
        }

        /// <summary>
        /// Delegate called on mouse moved events.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnMouseMoved(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.Button == MouseButtons.Left )
            {
                this.mAlpha = (float)(pEventArgs.X / this.mView.Width * 360.0);
                this.mTheta = (float)((pEventArgs.Y / this.mView.Height - 0.5) * 180.0);
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDisposed()
        {
            this.mView.Rendering    -= this.OnRender;
            this.mView.Resizing     -= this.OnResize;
            this.mView.MouseMoved   -= this.OnMouseMoved;
            this.mView.MouseWheeled -= this.OnMouseWheeled;
            
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.Dispose();
            }
            
            if ( this.mCube != null )
            {
                this.mCube.Dispose();
                this.mCube = null;
            }

            if ( this.mPlane != null )
            {
                this.mPlane.Dispose();
                this.mPlane = null;
            }

            if ( this.mPass1 != null )
            {
                this.mPass1.Dispose();
                this.mPass1 = null;
            }

            if ( this.mPass2 != null )
            {
                this.mPass2.Dispose();
                this.mPass2 = null;
            }

            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.Dispose();

            base.OnDisposed();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
