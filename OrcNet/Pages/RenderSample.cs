﻿using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Image;
using OrcNet.Core.Math;
using OrcNet.Core.Service;
using OrcNet.Core.UI;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Services;
using OrcNet.UI.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace OrcNet.Pages
{
    /// <summary>
    /// Definition of the <see cref="MinimalSample"/> class.
    /// </summary>
    public class RenderSample : ASample
    {
        #region Fields

        /// <summary>
        /// Stores the logo data ptr.
        /// </summary>
        private GCHandle mLogoPtr;

        /// <summary>
        /// Stores the cube mesh.
        /// </summary>
        private Mesh<P3_N3_UV_C, uint> mCube;

        /// <summary>
        /// Stores the plane mesh.
        /// </summary>
        private Mesh<P3_N3_UV_C, uint> mPlane;

        /// <summary>
        /// Stores the first pipeline pass.
        /// </summary>
        private PipelinePass mPass1;

        /// <summary>
        /// Stores the second pipeline pass.
        /// </summary>
        private PipelinePass mPass2;

        /// <summary>
        /// Stores the world camera uniform.
        /// </summary>
        private Vector3FUniform mWorldCamera;

        /// <summary>
        /// Stores the local to world matrix uniform of the first object.
        /// </summary>
        private Matrix4FUniform mLocalToWorld1;

        /// <summary>
        /// Stores the local to screen matrix uniform of the first object.
        /// </summary>
        private Matrix4FUniform mLocalToScreen1;

        /// <summary>
        /// Stores the local to world matrix uniform of the second object.
        /// </summary>
        private Matrix4FUniform mLocalToWorld2;

        /// <summary>
        /// Stores the local to screen matrix uniform of the second object.
        /// </summary>
        private Matrix4FUniform mLocalToScreen2;

        /// <summary>
        /// Stores the field of view in degrees.
        /// </summary>
        private float mFov;

        /// <summary>
        /// Stores the alpha camera rotation angle in degrees.
        /// </summary>
        private float mAlpha;

        /// <summary>
        /// Stores the theta camera rotation angle in degrees.
        /// </summary>
        private float mTheta;

        /// <summary>
        /// Stores the distance of the camera to the scene.
        /// </summary>
        private float mDistance;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderSample"/> class.
        /// </summary>
        /// <param name="pView">The view sample content will be rendered to.</param>
        public RenderSample(OrcView pView) :
        base( pView, new ViewParameters() { UseDepth = true }.SetSize( 1024, 768 ) )
        {
            
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Initializes the sample.
        /// </summary>
        protected override void OnInitialize()
        {
            this.mView.MouseMoved   += this.OnMouseMoved;
            this.mView.MouseWheeled += this.OnMouseWheeled;

            this.mFov      = 80.0f;
            this.mAlpha    = 135.0f;
            this.mTheta    = 45.0f;
            this.mDistance = 15.0f;

            this.mCube = new Mesh<P3_N3_UV_C, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            this.mCube.AddAttributeType( 0, 3, AttributeType.FLOAT_BASED, false );
            this.mCube.AddAttributeType( 1, 3, AttributeType.FLOAT_BASED, false );
            this.mCube.AddAttributeType( 2, 2, AttributeType.FLOAT_BASED, false );
            this.mCube.AddAttributeType( 3, 4, AttributeType.BYTE_BASED, true );

            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, +1, 0, 0, +1, 0, 0, 255, 0, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, +1, 0, 0, +1, 1, 0, 255, 0, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, +1, 0, 0, +1, 1, 1, 255, 0, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, +1, 0, 0, +1, 1, 1, 255, 0, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, +1, 0, 0, +1, 0, 1, 255, 0, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, +1, 0, 0, +1, 0, 0, 255, 0, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, +1, +1, 0, 0, 0, 0, 0, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, -1, +1, 0, 0, 1, 0, 0, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, -1, +1, 0, 0, 1, 1, 0, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, -1, +1, 0, 0, 1, 1, 0, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, +1, +1, 0, 0, 0, 1, 0, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, +1, +1, 0, 0, 0, 0, 0, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, +1, 0, +1, 0, 0, 0, 0, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, +1, 0, +1, 0, 1, 0, 0, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, -1, 0, +1, 0, 1, 1, 0, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, -1, 0, +1, 0, 1, 1, 0, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, -1, 0, +1, 0, 0, 1, 0, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, +1, 0, +1, 0, 0, 0, 0, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, -1, 0, 0, -1, 0, 0, 0, 255, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, -1, 0, 0, -1, 1, 0, 0, 255, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, -1, 0, 0, -1, 1, 1, 0, 255, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, -1, 0, 0, -1, 1, 1, 0, 255, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, +1, -1, 0, 0, -1, 0, 1, 0, 255, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, -1, 0, 0, -1, 0, 0, 0, 255, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, -1, -1, 0, 0, 0, 0, 255, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, +1, -1, 0, 0, 1, 0, 255, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, +1, -1, 0, 0, 1, 1, 255, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, +1, -1, 0, 0, 1, 1, 255, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, +1, -1, -1, 0, 0, 0, 1, 255, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, -1, -1, 0, 0, 0, 0, 255, 0, 255, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, -1, 0, -1, 0, 0, 0, 255, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, -1, 0, -1, 0, 1, 0, 255, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, +1, 0, -1, 0, 1, 1, 255, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(+1, -1, +1, 0, -1, 0, 1, 1, 255, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, +1, 0, -1, 0, 0, 1, 255, 255, 0, 0));
            this.mCube.AddVertex( new P3_N3_UV_C(-1, -1, -1, 0, -1, 0, 0, 0, 255, 255, 0, 0));

            this.mPlane = new Mesh<P3_N3_UV_C, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            this.mPlane.AddAttributeType( 0, 3, AttributeType.FLOAT_BASED, false );
            this.mPlane.AddAttributeType( 1, 3, AttributeType.FLOAT_BASED, false );
            this.mPlane.AddAttributeType( 2, 2, AttributeType.FLOAT_BASED, false );
            this.mPlane.AddAttributeType( 3, 4, AttributeType.BYTE_BASED, true );

            this.mPlane.AddVertex( new P3_N3_UV_C(-10, +10, 0, 0, 0, +1, 0, 0, 248, 166, 10, 0));
            this.mPlane.AddVertex( new P3_N3_UV_C(+10, +10, 0, 0, 0, +1, 1, 0, 248, 166, 10, 0));
            this.mPlane.AddVertex( new P3_N3_UV_C(+10, -10, 0, 0, 0, +1, 1, 1, 248, 166, 10, 0));
            this.mPlane.AddVertex( new P3_N3_UV_C(+10, -10, 0, 0, 0, +1, 1, 1, 248, 166, 10, 0));
            this.mPlane.AddVertex( new P3_N3_UV_C(-10, -10, 0, 0, 0, +1, 0, 1, 248, 166, 10, 0));
            this.mPlane.AddVertex( new P3_N3_UV_C(-10, +10, 0, 0, 0, +1, 0, 0, 248, 166, 10, 0));

            string lAssetDirectory = Path.Combine( Environment.CurrentDirectory, @"..\Resources\Assets" );
            string lTexturesDirectory = Path.Combine( lAssetDirectory, "Textures" );
            string lShadersDirectory  = Path.Combine( lAssetDirectory, "Shaders" );
            
            ITexture lLogoTexture;
            IImageService lImageService = ServiceManager.Instance.GetService<IImageService>();
            if ( lImageService == null )
            {
                throw new NullReferenceException( "No image service." );
            }

            int lWidth;
            int lHeight;
            int lChannels;
  
            string lTexturePath = Path.Combine( lTexturesDirectory, "checker.png" );
            if( string.IsNullOrEmpty( lTexturePath ) ||
                File.Exists( lTexturePath ) == false )
            {
                throw new NullReferenceException( "No texture path." );
            }

            byte[] lLogoData = lImageService.Load( lTexturePath, out lWidth, out lHeight, out lChannels );
            if ( lLogoData == null )
            {
                throw new NullReferenceException( "No logo data." );
            }

            this.mLogoPtr = GCHandle.Alloc( lLogoData, GCHandleType.Pinned );
            if ( this.mLogoPtr.IsAllocated == false )
            {
                throw new InvalidOperationException( "Cannot allocate anymore GCHAndle" );
            }

            if ( lChannels == 3 )
            {
                lLogoTexture = new Texture2D( lWidth, lHeight, 
                                              PixelInternalFormat.Rgb8, 
                                              PixelFormat.Rgb, 
                                              PixelType.UnsignedByte, 
                                              new TextureParameters(), 
                                              new BufferLayoutParameters(), 
                                              new CPUBuffer( this.mLogoPtr.AddrOfPinnedObject() ) );
            }
            else
            {
                lLogoTexture = new Texture2D( lWidth, lHeight, 
                                              PixelInternalFormat.Rgba8, 
                                              PixelFormat.Rgba, 
                                              PixelType.UnsignedByte, 
                                              new TextureParameters(), 
                                              new BufferLayoutParameters(), 
                                              new CPUBuffer( this.mLogoPtr.AddrOfPinnedObject() ) );
            }
            
            PipelineDescription lCamera    = new PipelineDescription( 330, File.ReadAllText( Path.Combine( lShadersDirectory, "camera.glsl" ) ) );
            PipelineDescription lSpotlight = new PipelineDescription( 330, File.ReadAllText( Path.Combine( lShadersDirectory, "spotlight.glsl" ) ) );
            PipelineDescription lPlastic   = new PipelineDescription( new PipelineStage( 330, File.ReadAllText( Path.Combine( lShadersDirectory, "plasticVS.glsl" ) ), StageType.VERTEX ), 
                                                                      new PipelineStage( 330, File.ReadAllText( Path.Combine( lShadersDirectory, "plasticFS.glsl" ) ), StageType.FRAGMENT ) );
            PipelineDescription lTexturedPlastic = new PipelineDescription( 330, File.ReadAllText( Path.Combine( lShadersDirectory, "texturedPlastic.glsl" ) ) );

            List<PipelineDescription> lPassDescriptions1 = new List<PipelineDescription>();
            lPassDescriptions1.Add( lCamera );
            lPassDescriptions1.Add( lSpotlight );
            lPassDescriptions1.Add( lPlastic );
            this.mPass1 = new PipelinePass( lPassDescriptions1 );

            List<PipelineDescription> lPassDescriptions2 = new List<PipelineDescription>();
            lPassDescriptions2.Add( lCamera );
            lPassDescriptions2.Add( lSpotlight );
            lPassDescriptions2.Add( lTexturedPlastic );
            this.mPass2 = new PipelinePass( lPassDescriptions2 );

            Vector3FUniform lLightPosUniform = this.mPass1.GetUniform<Vector3FUniform>( new UniformName( "worldLightPos" ) );
            if ( lLightPosUniform != null )
            {
                lLightPosUniform.Value = new Vector3F( 3.0f, 3.0f, 3.0f );
            }

            Vector3FUniform lLightDirUniform = this.mPass1.GetUniform<Vector3FUniform>( new UniformName( "worldLightDir" ) );
            if ( lLightDirUniform != null )
            {
                lLightDirUniform.Value = new Vector3F( -0.5f, -0.5f, -0.707107f );
            }

            Vector2FUniform lLightAngleUniform = this.mPass1.GetUniform<Vector2FUniform>( new UniformName( "spotlightAngle" ) );
            if ( lLightAngleUniform != null )
            {
                lLightAngleUniform.Value = new Vector2F( 0.4f, 0.6f );
            }

            TextureUniform lTextureUniform = this.mPass2.GetUniform<TextureUniform>( new UniformName( "tex" ) );
            if ( lTextureUniform != null )
            {
                lTextureUniform.Value = lLogoTexture;
            }
            
            this.mWorldCamera    = this.mPass1.GetUniform<Vector3FUniform>( new UniformName( "worldCameraPos" ) );
            this.mLocalToWorld1  = this.mPass1.GetUniform<Matrix4FUniform>( new UniformName( "localToWorld" ) );
            this.mLocalToScreen1 = this.mPass1.GetUniform<Matrix4FUniform>( new UniformName( "localToScreen" ) );
            this.mLocalToWorld2  = this.mPass2.GetUniform<Matrix4FUniform>( new UniformName( "localToWorld" ) );
            this.mLocalToScreen2 = this.mPass2.GetUniform<Matrix4FUniform>( new UniformName( "localToScreen" ) );
            
            this.FrameBuffer.Parameters.DepthDescription.DepthTest( true, DepthFunction.Less );

            base.OnInitialize();
        }
        
        /// <summary>
        /// Internal render method to override.
        /// </summary>
        /// <param name="pAbsoluteTime">The absolute time.</param>
        /// <param name="pDeltaTime">The elasped time since last render.</param>
        /// <returns>True if anything has been rendered, false otherwise.</returns>
        protected override bool OnRender(double pAbsoluteTime, double pDeltaTime)
        {
            this.FrameBuffer.Clear( true, false, true );

            AMatrix<float> lCameraToWorld = Matrix4F.RotateX( 90 );
            lCameraToWorld = lCameraToWorld * Matrix4F.RotateY(-this.mAlpha);
            lCameraToWorld = lCameraToWorld * Matrix4F.RotateX(-this.mTheta);
            lCameraToWorld = lCameraToWorld * Matrix4F.Translate( new Vector3F( 0.0f, 0.0f, this.mDistance ));

            bool lResult;
            AMatrix<float> lWorldToCamera = lCameraToWorld.Inverse( float.Epsilon, out lResult ) as AMatrix<float>;

            Viewport lViewport = this.FrameBuffer.Parameters[ new FrameBufferParameters.ViewportIndex( 0 ) ];
            float lWidth  = lViewport.Width;
            float lHeight = lViewport.Height;
            float lFov = (float)MathUtility.ToDegrees( 2 * System.Math.Atan( lHeight / lWidth * System.Math.Tan( MathUtility.ToRadians( this.mFov / 2 ) ) ) );
            Matrix4F lCameraToScreen = Matrix4F.PerspectiveProjection( lFov, lWidth / lHeight, 0.1f, 1e5f );

            this.mWorldCamera.Value = lCameraToWorld.Multiply( Vector3F.ZERO ) as Vector3F;

            this.mLocalToWorld1.Value  = Matrix4F.RotateZ( 15.0f );
            this.mLocalToScreen1.Value = lCameraToScreen * lWorldToCamera * Matrix4F.RotateZ( 15.0f );
            this.FrameBuffer.Draw( this.mPass1, this.mCube );

            this.mLocalToWorld2.Value  = Matrix4F.Translate( new Vector3F( 0.0f, 0.0f, -2.0f ) ) * Matrix4F.RotateZ( 180.0f );
            this.mLocalToScreen2.Value = lCameraToScreen * lWorldToCamera * Matrix4F.Translate( new Vector3F( 0.0f, 0.0f, -2.0f ) ) * Matrix4F.RotateZ( 180.0f );
            this.FrameBuffer.Draw( this.mPass2, this.mPlane );

            return true;
        }

        /// <summary>
        /// Internal resize method to override.
        /// </summary>
        /// <param name="pWidth">The new width.</param>
        /// <param name="pHeight">The new height.</param>
        protected override void OnResize(int pWidth, int pHeight)
        {
            base.OnResize( pWidth, pHeight );
            
            // Reinit the depth test function.
            this.FrameBuffer.Parameters.DepthDescription.DepthTest( true, DepthFunction.Less );
        }

        /// <summary>
        /// Delegate called on mouse wheeled events.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnMouseWheeled(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.Delta > 0 ) // Wheel up?
            {
                this.mDistance *= 1.05f;
            }
            else
            {
                this.mDistance /= 1.05f;
            }
        }

        /// <summary>
        /// Delegate called on mouse moved events.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnMouseMoved(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.Button == MouseButtons.Left )
            {
                this.mAlpha = (float)(pEventArgs.X / this.mView.Width * 360.0);
                this.mTheta = (float)((pEventArgs.Y / this.mView.Height - 0.5) * 180.0);
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDisposed()
        {
            this.mView.Rendering    -= this.OnRender;
            this.mView.Resizing     -= this.OnResize;
            this.mView.MouseMoved   -= this.OnMouseMoved;
            this.mView.MouseWheeled -= this.OnMouseWheeled;
            
            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            if ( lRenderService != null )
            {
                lRenderService.Dispose();
            }
            
            this.mLogoPtr.Free();

            if ( this.mCube != null )
            {
                this.mCube.Dispose();
                this.mCube = null;
            }

            if ( this.mPlane != null )
            {
                this.mPlane.Dispose();
                this.mPlane = null;
            }

            if ( this.mPass1 != null )
            {
                this.mPass1.Dispose();
                this.mPass1 = null;
            }

            if ( this.mPass2 != null )
            {
                this.mPass2.Dispose();
                this.mPass2 = null;
            }

            base.OnDisposed();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
