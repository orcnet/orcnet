﻿namespace OrcNet
{
    /// <summary>
    /// Definition of the <see cref="P3_N3_UV_C"/> structure.
    /// </summary>
    public struct P3_N3_UV_C
    {
        #region Properties

        /// <summary>
        /// Gets or sets the X position component
        /// </summary>
        public float X
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Y position component
        /// </summary>
        public float Y
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Z position component
        /// </summary>
        public float Z
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the X normal component
        /// </summary>
        public float Nx
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Y normal component
        /// </summary>
        public float Ny
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Z normal component
        /// </summary>
        public float Nz
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the U texture coordinate component
        /// </summary>
        public float U
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the V texture coordinate component
        /// </summary>
        public float V
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Red color component.
        /// </summary>
        public byte R
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Green color component.
        /// </summary>
        public byte G
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Blue color component.
        /// </summary>
        public byte B
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Alpha color component.
        /// </summary>
        public byte A
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="P3_N3_UV_C"/> class.
        /// </summary>
        /// <param name="pX"></param>
        /// <param name="pY"></param>
        /// <param name="pZ"></param>
        /// <param name="pNx"></param>
        /// <param name="pNy"></param>
        /// <param name="pNz"></param>
        /// <param name="pU"></param>
        /// <param name="pV"></param>
        /// <param name="pR"></param>
        /// <param name="pG"></param>
        /// <param name="pB"></param>
        /// <param name="pA"></param>
        public P3_N3_UV_C(float pX = 0.0f, float pY = 0.0f, float pZ = 0.0f, float pNx = 0.0f, float pNy = 0.0f, float pNz = 0.0f,
                          float pU = 0.0f, float pV = 0.0f, byte pR = 255, byte pG = 255, byte pB = 255, byte pA = 255)
        {
            this.X = pX;
            this.Y = pY;
            this.Z = pZ;
            this.Nx = pNx;
            this.Ny = pNy;
            this.Nz = pNz;
            this.U = pU;
            this.V = pV;
            this.R = pR;
            this.G = pG;
            this.B = pB;
            this.A = pA;
        }

        #endregion Constructor
    }
}
