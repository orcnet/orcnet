﻿using Avalonia;
using Avalonia.Logging.Serilog;
using Serilog;
using System;

namespace OrcNet.Loader
{
    /// <summary>
    /// The Orc Net samples entry point class definition.
    /// </summary>
    static class EntryPoint
    {
        /// <summary>
        /// The entry point's main method.
        /// </summary>
        /// <param name="pParameters">The application parameters.</param>
        [STAThread]
        static void Main(string[] pParameters)
        {
            // This main program will simply offer different Orc samples
            // acting like How-tos.
            InitializeLogging();

            AppBuilder.Configure<App>()
                      .UseGtk()
                      .UseCairo()
                      .Start<SampleWindow>();
        }
        
        /// <summary>
        /// Initializes avalonia logger.
        /// </summary>
        private static void InitializeLogging()
        {
#if DEBUG
            SerilogLogger.Initialize( new LoggerConfiguration()
                                      .MinimumLevel.Warning()
                                      .WriteTo.Trace(outputTemplate: "{Area}: {Message}")
                                      .CreateLogger() );
#endif
        }
    }
}
