﻿using Avalonia;
using Avalonia.Gtk.Embedding;
using Avalonia.Markup.Xaml;
using Gtk;

namespace OrcNet
{
    /// <summary>
    /// Definition of the <see cref="MainWindow"/> class.
    /// </summary>
    public class MainWindow : Window
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="pTitle">The window title.</param>
        public MainWindow(string pTitle = "OrcNet Samples") :
        base( pTitle )
        {
            this.InitializeComponent();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes the main window.
        /// </summary>
        private void InitializeComponent()
        {
            HBox lRoot = new HBox();
            VBox lLeft = new VBox();
            lLeft.Add(new Button("I'm GTK button"));
            lLeft.Add(new Calendar());

            lRoot.PackEnd( lLeft, false, false, 0 );

            GtkAvaloniaControlHost lHost = new GtkAvaloniaControlHost()
            {
                Content = new MainView()
            };
            lHost.SetSizeRequest( 600, 600 );

            lRoot.PackStart( lHost, true, true, 0 );

            this.Add( lRoot );
            this.ShowAll();
        }

        #endregion Methods
    }
}
