﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Shapes;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Media.Imaging;
using OrcNet.Core.Logger;
using OrcNet.Pages;
using System;

namespace OrcNet
{
    /// <summary>
    /// Definition of the <see cref="SampleWindow"/> class.
    /// </summary>
    public class SampleWindow : Window
    {
        #region DependencyProperty

        /// <summary>
        /// Is displaying sample dependency property.
        /// </summary>
        public static readonly StyledProperty<bool> IsDisplayingSampleProperty;

        /// <summary>
        /// Start page image sample dependency property.
        /// </summary>
        public static readonly StyledProperty<IBitmap> StartPageImageProperty;

        /// <summary>
        /// Close image sample dependency property.
        /// </summary>
        public static readonly StyledProperty<IBitmap> CloseImageProperty;

        /// <summary>
        /// Minimize image sample dependency property.
        /// </summary>
        public static readonly StyledProperty<IBitmap> MinimizeImageProperty;

        /// <summary>
        /// Maximize image sample dependency property.
        /// </summary>
        public static readonly StyledProperty<IBitmap> MaximizeImageProperty;

        #endregion DependencyProperty

        #region Fields

        /// <summary>
        /// Stores the start page image bitmap.
        /// </summary>
        private readonly static Bitmap sStartPageImage;

        /// <summary>
        /// Stores the close image bitmap.
        /// </summary>
        private readonly static Bitmap sCloseImage;

        /// <summary>
        /// Stores the minimize image bitmap.
        /// </summary>
        private readonly static Bitmap sMinimizeImage;

        /// <summary>
        /// Stores the maximize image bitmap.
        /// </summary>
        private readonly static Bitmap sMaximizeImage;

        /// <summary>
        /// Stores the title bar shape.
        /// </summary>
        private Rectangle mTitleBarShape;

        /// <summary>
        /// Stores the start page button.
        /// </summary>
        private Button mStartPageButton;

        /// <summary>
        /// Stores the minimize button.
        /// </summary>
        private Button mMinimizeWindow;

        /// <summary>
        /// Stores the maximize button.
        /// </summary>
        private Button mMaximizeWindow;

        /// <summary>
        /// Stores the close button.
        /// </summary>
        private Button mCloseWindow;

        /// <summary>
        /// Stores the sample 1 button.
        /// </summary>
        private Button mSample1Button;

        /// <summary>
        /// Stores the sample 2 button.
        /// </summary>
        private Button mSample2Button;

        /// <summary>
        /// Stores the sample 3 button.
        /// </summary>
        private Button mSample3Button;

        /// <summary>
        /// Stores the sample 4 button.
        /// </summary>
        private Button mSample4Button;

        /// <summary>
        /// Stores the sample 5 button.
        /// </summary>
        private Button mSample5Button;

        /// <summary>
        /// Stores the sample 6 button.
        /// </summary>
        private Button mSample6Button;

        /// <summary>
        /// Stores the start page grid.
        /// </summary>
        private Grid mStartPage;

        /// <summary>
        /// Stores the rendering grid.
        /// </summary>
        private Grid mRendering;

        /// <summary>
        /// Stores the rendered OrcView.
        /// </summary>
        private UI.View.OrcView mRenderingView;

        /// <summary>
        /// Stores the current sample.
        /// </summary>
        private ASample mCurrentSample;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the start page image source.
        /// </summary>
        public IBitmap StartPageImage
        {
            get
            {
                return GetValue( StartPageImageProperty );
            }
        }

        /// <summary>
        /// Gets the close image source.
        /// </summary>
        public IBitmap CloseImage
        {
            get
            {
                return GetValue( CloseImageProperty );
            }
        }

        /// <summary>
        /// Gets the minimize image bitmap.
        /// </summary>
        public IBitmap MinimizeImage
        {
            get
            {
                return sMinimizeImage;
            }
        }

        /// <summary>
        /// Gets the maximize image bitmap.
        /// </summary>
        public IBitmap MaximizeImage
        {
            get
            {
                return sMaximizeImage;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the window is displaying a sample or not.
        /// </summary>
        public bool IsDisplayingSample
        {
            get
            {
                return (bool)GetValue( IsDisplayingSampleProperty );
            }
            private set
            {
                SetValue( IsDisplayingSampleProperty, value );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="SampleWindow"/> class.
        /// </summary>
        static SampleWindow()
        {
            string lStartPageIconName = System.IO.Path.Combine( Environment.CurrentDirectory, @"..\Resources\Assets\Textures\OrcIcon.ico" );
            string lCloseIconName     = System.IO.Path.Combine( Environment.CurrentDirectory, @"..\Resources\Window\Icons\CloseWindow.ico" );
            string lMinimizeIconName  = System.IO.Path.Combine( Environment.CurrentDirectory, @"..\Resources\Window\Icons\MinimizeWindow.ico" );
            string lMaximizeIconName  = System.IO.Path.Combine( Environment.CurrentDirectory, @"..\Resources\Window\Icons\MaximizeWindow.ico" );
            sStartPageImage = new Bitmap( lStartPageIconName );
            sCloseImage     = new Bitmap( lCloseIconName );
            sMinimizeImage  = new Bitmap( lMinimizeIconName );
            sMaximizeImage  = new Bitmap( lMaximizeIconName );

            IsDisplayingSampleProperty = AvaloniaProperty.Register<SampleWindow, bool>( "IsDisplayingSample", false );
            StartPageImageProperty     = AvaloniaProperty.Register<SampleWindow, IBitmap>( "StartPageImage", sStartPageImage );
            CloseImageProperty         = AvaloniaProperty.Register<SampleWindow, IBitmap>( "CloseImage", sCloseImage );
            MinimizeImageProperty      = AvaloniaProperty.Register<SampleWindow, IBitmap>( "MinimizeImage", sMinimizeImage );
            MaximizeImageProperty      = AvaloniaProperty.Register<SampleWindow, IBitmap>( "MaximizeImage", sMaximizeImage );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleWindow"/> class.
        /// </summary>
        public SampleWindow()
        {
            this.InitializeComponent();
        }

        #endregion Constructor

        #region Methods
        
        #region Methods Internal

        /// <summary>
        /// Initializes the main window.
        /// </summary>
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load( this );

            Core.Application.Instance.Initialize( LoggerType.NONE );

            this.Title = "Avalonia OrcNet Samples";

            this.DataContext = this;

            this.Icon = new Avalonia.Controls.WindowIcon( sStartPageImage );

            // Equivalent to WindowStyle="None" of Wpf.
            this.HasSystemDecorations = false;

            // Title bar shape.
            this.mTitleBarShape = this.FindControl<Rectangle>( "mTitleBarShape" );
            this.mTitleBarShape.PointerPressed += this.OnTitleBarMouseDown;

            // The main grids.
            this.mStartPage = this.FindControl<Grid>( "mStartPage" );
            this.mRendering = this.FindControl<Grid>( "mRendering" );

            // Title bar buttons.
            this.mStartPageButton = this.FindControl<Button>( "mStartPageButton" );
            this.mStartPageButton.Click += this.OnStartPageButtonClick;
            this.mMinimizeWindow  = this.FindControl<Button>( "mMinimizeWindow" );
            this.mMinimizeWindow.Click += this.OnMinimizeButtonClick;
            this.mMaximizeWindow  = this.FindControl<Button>( "mMaximizeWindow" );
            this.mMaximizeWindow.Click += this.OnMaximizeButtonClick;
            this.mCloseWindow     = this.FindControl<Button>( "mCloseWindow" );
            this.mCloseWindow.Click += this.OnCloseButtonClick;

            // Start page buttons.
            this.mSample1Button = this.FindControl<Button>( "mSample1Button" );
            this.mSample1Button.Click += this.OnSampleButtonClick;
            this.mSample2Button = this.FindControl<Button>( "mSample2Button" );
            this.mSample2Button.Click += this.OnSampleButtonClick;
            this.mSample3Button = this.FindControl<Button>( "mSample3Button" );
            this.mSample3Button.Click += this.OnSampleButtonClick;
            this.mSample4Button = this.FindControl<Button>( "mSample4Button" );
            this.mSample4Button.Click += this.OnSampleButtonClick;
            this.mSample5Button = this.FindControl<Button>( "mSample5Button" );
            this.mSample5Button.Click += this.OnSampleButtonClick;
            this.mSample6Button = this.FindControl<Button>( "mSample6Button" );
            this.mSample6Button.Click += this.OnSampleButtonClick;

            this.mRenderingView   = this.mRendering.Children[ 0 ] as OrcNet.UI.View.OrcView;
                
            this.AttachedToLogicalTree += this.OnAttachedToLogicalTree;
        }

        /// <summary>
        /// Adjusts the WindowSize to correct parameters when Maximize button is clicked
        /// </summary>
        private void AdjustWindowSize()
        {
            if ( this.WindowState == Avalonia.Controls.WindowState.Maximized )
            {
                this.WindowState = Avalonia.Controls.WindowState.Normal;
            }
            else
            {
                this.WindowState = Avalonia.Controls.WindowState.Maximized;
            }
        }

        #endregion Methods Internal

        #region Methods Events

        /// <summary>
        /// Delegate called on window loaded.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnAttachedToLogicalTree(object pSender, Avalonia.LogicalTree.LogicalTreeAttachmentEventArgs pEventArgs)
        {
            
        }

        /// <summary>
        /// Delegate called on title bar mouse down.
        /// Drag if single-click, resize if double-click
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnTitleBarMouseDown(object pSender, Avalonia.Input.PointerPressedEventArgs pEventArgs)
        {
            if ( pEventArgs.MouseButton == Avalonia.Input.MouseButton.Left )
            {
                if ( pEventArgs.ClickCount == 2 )
                {
                    this.AdjustWindowSize();
                }
                else
                {
                    this.BeginMoveDrag();
                }
            }
        }
        
        /// <summary>
        /// Delegate called on close button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnCloseButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            Environment.Exit( 0 );
        }

        /// <summary>
        /// Delegate called on thumb close button info click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnCloseThumbButtonInfoClick(object pSender, EventArgs pEventArgs)
        {
            Environment.Exit( 0 );
        }

        /// <summary>
        /// Delegate called on maximize button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnMaximizeButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            this.AdjustWindowSize();
        }

        /// <summary>
        /// Delegate called on minimize button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnMinimizeButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            this.WindowState = Avalonia.Controls.WindowState.Minimized;
        }
        
        /// <summary>
        /// Delegate called in sample button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnSampleButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            Button lSender = pSender as Button;
            if ( lSender != null )
            {
                Type lSampleType = null;
                if ( lSender.Name == "mSample1Button" )
                {
                    lSampleType = typeof(MinimalSample);
                }
                else if ( lSender.Name == "mSample2Button" )
                {
                    lSampleType = typeof(ResourceSample);
                }
                else if ( lSender.Name == "mSample3Button" )
                {
                    lSampleType = typeof(RenderSample);
                }
                else if ( lSender.Name == "mSample4Button" )
                {
                    lSampleType = typeof(SceneGraphSample);
                }
                else if ( lSender.Name == "mSample5Button" )
                {
                    lSampleType = typeof(SceneGraphResourceSample);
                }
                else if ( lSender.Name == "mSample6Button" )
                {
                    lSampleType = typeof(TessellationSample);
                }

                // Auto create.
                if ( lSampleType != null )
                {
                    System.Reflection.ConstructorInfo lCtor = lSampleType.GetConstructor( new Type[] { typeof(UI.View.OrcView) } );
                    if ( lCtor != null )
                    {
                        this.IsDisplayingSample = true;

                        // Hide the start page.
                        this.mStartPage.IsVisible = false;
                        
                        // Plugs the new sample with the rendering view.
                        this.mCurrentSample = lCtor.Invoke( new object[] { this.mRenderingView } ) as ASample;
                        this.mCurrentSample.Initialize();

                        // Show the rendering panel.
                        this.mRendering.IsVisible = true;
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called on start page button click.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnStartPageButtonClick(object pSender, RoutedEventArgs pEventArgs)
        {
            this.IsDisplayingSample = false;

            this.mStartPage.IsVisible = true;

            if ( this.mCurrentSample != null )
            {
                this.mCurrentSample.Dispose();
                this.mCurrentSample = null;
            }
            
            this.mRendering.IsVisible = false;
        }

        #endregion Methods Events

        #endregion Methods
    }
}
