﻿using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Media.Imaging;
using OpenTK;
using OrcNet.Pages;
using System;

namespace OrcNet
{
    /// <summary>
    /// Definition of the <see cref="MainView"/> class.
    /// </summary>
    public class MainView : UserControl
    {
        #region Fields
        
        /// <summary>
        /// Stores the fake context window that is used to create opentk context.
        /// </summary>
        private NativeWindow mContextWindow;

        /// <summary>
        /// Stores the current sample.
        /// </summary>
        private ASample mCurrentSample;

        /// <summary>
        /// Stores the background image.
        /// </summary>
        private Image mBackgroundImage;

        /// <summary>
        /// Stores the start page grid.
        /// </summary>
        private Grid mStartPage;

        /// <summary>
        /// Stores the rendering grid.
        /// </summary>
        private Grid mRendering;

        /// <summary>
        /// Stores the start page button.
        /// </summary>
        private Button mStartPageButton;
        
        /// <summary>
        /// Stores the start page button.
        /// </summary>
        private Button mScreenshotButton;

        /// <summary>
        /// Stores the sample 1 button.
        /// </summary>
        private Button mSample1Button;

        /// <summary>
        /// Stores the sample 2 button.
        /// </summary>
        private Button mSample2Button;

        /// <summary>
        /// Stores the sample 3 button.
        /// </summary>
        private Button mSample3Button;

        /// <summary>
        /// Stores the sample 4 button.
        /// </summary>
        private Button mSample4Button;

        /// <summary>
        /// Stores the sample 5 button.
        /// </summary>
        private Button mSample5Button;

        /// <summary>
        /// Stores the sample 6 button.
        /// </summary>
        private Button mSample6Button;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the OpenTK native window providing Context.
        /// </summary>
        internal NativeWindow OpenTKWindow
        {
            get
            {
                return this.mContextWindow;
            }
            set
            {
                this.mContextWindow = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MainView"/> class.
        /// </summary>
        public MainView()
        {
            this.InitializeComponent();
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Delegate called on screenshot button clicked.
        /// </summary>
        /// <param name="pSender">The button.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnScreenshotClicked(object pSender, RoutedEventArgs pEventArgs)
        {
            if ( this.mCurrentSample != null )
            {
                this.mCurrentSample.RequestScreenshot();
            }
        }

        /// <summary>
        /// Initializes the main view control.
        /// </summary>
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load( this );

            this.DataContext = this;

            // Load the background image.
            string lBackgroundPath = System.IO.Path.Combine( Environment.CurrentDirectory, @"..\Resources\Assets\Textures\OrcNetBackground.png" );
            Bitmap lBackgroundImage = new Bitmap( lBackgroundPath );
            this.mBackgroundImage = this.FindControl<Image>( "mBackgroundImage" );
            this.mBackgroundImage.Source = lBackgroundImage;
            
            this.mSample1Button = this.FindControl<Button>( "mSample1Button" );
            this.mSample1Button.Click += this.OnSample1Click;

            this.mSample2Button = this.FindControl<Button>( "mSample2Button" );
            this.mSample2Button.Click += this.OnSample2Click;

            this.mSample3Button = this.FindControl<Button>( "mSample3Button" );
            this.mSample3Button.Click += this.OnSample3Click;

            this.mSample4Button = this.FindControl<Button>( "mSample4Button" );
            this.mSample4Button.Click += this.OnSample4Click;

            this.mSample5Button = this.FindControl<Button>( "mSample5Button" );
            this.mSample5Button.Click += this.OnSample5Click;

            this.mSample6Button = this.FindControl<Button>( "mSample6Button" );
            this.mSample6Button.Click += this.OnSample6Click;

            this.mStartPageButton = this.FindControl<Button>( "mBackToStartButton" );
            this.mStartPageButton.Click += this.OnBackToStartClick;

            this.mScreenshotButton = this.FindControl<Button>( "mScreenshotButton" );
            this.mScreenshotButton.Click += this.OnScreenshotClicked;

            this.mStartPage = this.FindControl<Grid>( "mStartPage" );
            this.mRendering = this.FindControl<Grid>( "mRendering" );
        }
        
        /// <summary>
        /// Delegate called on Sample 1 click events.
        /// </summary>
        /// <param name="pSender">The button.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnSample1Click(object pSender, RoutedEventArgs pEventArgs)
        {
            // Activate the back to start page button.
            this.mStartPageButton.IsEnabled = true;

            // Hide the grid displaying all buttons
            this.mStartPage.IsVisible = false;

            // Create the sample 1
            this.mRendering.Children.Add( this.mCurrentSample = new MinimalSample( this.mContextWindow ) );

            // Shows the grid containing the sample 1.
            this.mRendering.IsVisible = true;
        }

        /// <summary>
        /// Delegate called on Sample 2 click events.
        /// </summary>
        /// <param name="pSender">The button.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnSample2Click(object pSender, RoutedEventArgs pEventArgs)
        {
            // Activate the back to start page button.
            this.mStartPageButton.IsEnabled = true;

            // Hide the grid displaying all buttons
            this.mStartPage.IsVisible = false;

            // Create the sample 2
            this.mRendering.Children.Add( this.mCurrentSample = new ResourceSample( this.mContextWindow ) );

            // Shows the grid containing the sample 2.
            this.mRendering.IsVisible = true;
        }

        /// <summary>
        /// Delegate called on Sample 3 click events.
        /// </summary>
        /// <param name="pSender">The button.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnSample3Click(object pSender, RoutedEventArgs pEventArgs)
        {
            // Activate the back to start page button.
            this.mStartPageButton.IsEnabled = true;

            // Hide the grid displaying all buttons
            this.mStartPage.IsVisible = false;

            // Create the sample 3
            this.mRendering.Children.Add( this.mCurrentSample = new RenderSample( this.mContextWindow ) );

            // Shows the grid containing the sample 3.
            this.mRendering.IsVisible = true;
        }

        /// <summary>
        /// Delegate called on Sample 4 click events.
        /// </summary>
        /// <param name="pSender">The button.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnSample4Click(object pSender, RoutedEventArgs pEventArgs)
        {
            // Activate the back to start page button.
            this.mStartPageButton.IsEnabled = true;

            // Hide the grid displaying all buttons
            this.mStartPage.IsVisible = false;

            // Create the sample 4
            this.mRendering.Children.Add( this.mCurrentSample = new SceneGraphSample( this.mContextWindow ) );

            // Shows the grid containing the sample 4.
            this.mRendering.IsVisible = true;
        }

        /// <summary>
        /// Delegate called on Sample 5 click events.
        /// </summary>
        /// <param name="pSender">The button.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnSample5Click(object pSender, RoutedEventArgs pEventArgs)
        {
            // Activate the back to start page button.
            this.mStartPageButton.IsEnabled = true;

            // Hide the grid displaying all buttons
            this.mStartPage.IsVisible = false;

            // Create the sample 5
            this.mRendering.Children.Add( this.mCurrentSample = new SceneGraphResourceSample( this.mContextWindow ) );

            // Shows the grid containing the sample 5.
            this.mRendering.IsVisible = true;
        }

        /// <summary>
        /// Delegate called on Sample 6 click events.
        /// </summary>
        /// <param name="pSender">The button.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnSample6Click(object pSender, RoutedEventArgs pEventArgs)
        {
            // Activate the back to start page button.
            this.mStartPageButton.IsEnabled = true;

            // Hide the grid displaying all buttons
            this.mStartPage.IsVisible = false;

            // Create the sample 6
            this.mRendering.Children.Add( this.mCurrentSample = new TessellationSample( this.mContextWindow ) );

            // Shows the grid containing the sample 6.
            this.mRendering.IsVisible = true;
        }

        /// <summary>
        /// Delegate called on back to start click events.
        /// </summary>
        /// <param name="pSender">The button.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnBackToStartClick(object pSender, RoutedEventArgs pEventArgs)
        {
            // Deactivate the back to start page button.
            this.mStartPageButton.IsEnabled = false;

            // Releases the current sample.
            this.mCurrentSample = null;

            // Hides the sample that was being rendered.
            foreach ( ASample lSample in this.mRendering.Children )
            {
                lSample.Release();
            }
            this.mRendering.Children.Clear();
            
            this.mRendering.IsVisible = false;
            
            // Shows the start page grid.
            this.mStartPage.IsVisible = true;
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
