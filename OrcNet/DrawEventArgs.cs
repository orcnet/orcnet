﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;

namespace OrcNet
{
    /// <summary>
    /// Definition of the <see cref="DrawEventArgs"/> class.
    /// </summary>
    [DebuggerDisplay("Format = {Format}, Type = {Type}, Redrawn = {Redrawn}")]
    public class DrawEventArgs : EventArgs
    {
        #region Properties

        /// <summary>
        /// true if handler requires drawing (refresh image)
        /// initially false , means skip refreshing the image
        /// </summary>
        public bool Redrawn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the rendering pixel format.
        /// </summary>
        public PixelFormat Format
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the rendering pixel type.
        /// </summary>
        public PixelType Type
        {
            get;
            set;
        }
        
        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawEventArgs"/> class.
        /// </summary>
        public DrawEventArgs()
        {
            this.Redrawn = false;
            this.Format  = PixelFormat.Rgb;
            this.Type    = PixelType.UnsignedByte;
        }

        #endregion Constructor
    }
}
