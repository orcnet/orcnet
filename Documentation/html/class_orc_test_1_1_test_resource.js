var class_orc_test_1_1_test_resource =
[
    [ "TestResource", "class_orc_test_1_1_test_resource.html#aef82de89f32777344cd2b6c9e1f40f56", null ],
    [ "TestDescriptionResourceUpdateRemovedSubroutine", "class_orc_test_1_1_test_resource.html#a6ac05f4392d332801a0b1276f3199e81", null ],
    [ "TestDescriptionResourceUpdateRemovedUniform", "class_orc_test_1_1_test_resource.html#a5903bad9111ca1faa86f92990ce28079", null ],
    [ "TestDescriptionResourceUpdateWithSubroutineUniforms", "class_orc_test_1_1_test_resource.html#ab0066bda22fc5f3866123c54057034a2", null ],
    [ "TestDescriptionResourceUpdateWithTextureUniforms", "class_orc_test_1_1_test_resource.html#ac734acaf966ccbeca98cf306eefd1088", null ],
    [ "TestDescriptionResourceUpdateWithUniformBlocks", "class_orc_test_1_1_test_resource.html#a442297eea4c97461e5128c58acba33b1", null ],
    [ "TestPipelineDescriptionResource", "class_orc_test_1_1_test_resource.html#ae73fc64d7429e3b804d5054953baed29", null ],
    [ "TestPipelineDescriptionResourceUpdate", "class_orc_test_1_1_test_resource.html#a3cfa85e5c4939acd3a6535bebd6bc0fc", null ],
    [ "TestTextureResourceUpdate", "class_orc_test_1_1_test_resource.html#a1da8374b9f57c3754ac805709e8f96fb", null ]
];