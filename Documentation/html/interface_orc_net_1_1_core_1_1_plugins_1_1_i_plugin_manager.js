var interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager =
[
    [ "GetPlugin", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#afabda2add671a78e6e5cba88a0ff3575", null ],
    [ "GetPlugin", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a1c8766bd9a8cd6f6091808498fc060f1", null ],
    [ "GetPlugin< T >", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a1aea9027e580f2ad7daddc0965e1cef8", null ],
    [ "GetPlugin< T >", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a02a083a287202fc5d535911e33349a2a", null ],
    [ "Initialize", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#aea76af7b39191594409a7574b50a1890", null ],
    [ "LoadAllPlugins", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a44844b1fb5c3469ab8b9a539de2be1ea", null ],
    [ "CachePolicy", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a7c7164e260e70a13947afe1d67baec9f", null ],
    [ "CreatePluginsFolder", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a9e925cb79fb7279c2775f873d2b7b90f", null ],
    [ "DefaultConfiguration", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a83510f5ff0c2e3d9d0fc9686d0082f24", null ],
    [ "KeepFileHandle", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a2279e6599a7375e5d2721ea49b7b3c51", null ],
    [ "PluginsFolder", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a39c66a8d33bbd611d43dbd793651cff7", null ],
    [ "PluginLoaded", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a81efdb1e27a69589e8562a8492954856", null ],
    [ "PluginUnloaded", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a8db1f66a674e3e553753ad7a94de6bf0", null ]
];