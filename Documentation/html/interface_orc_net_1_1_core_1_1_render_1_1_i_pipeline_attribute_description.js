var interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description =
[
    [ "AttributeSize", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html#a9c8ede74e7606ba21fb093e8ef3403a5", null ],
    [ "ComponentCount", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html#a248c16f2f83e9efff795491118c383e8", null ],
    [ "Index", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html#a5dc549a4929d03b9bee9adfb3c91f01e", null ],
    [ "MustBeNormalized", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html#a171f2836977df695788c572b8be15fb5", null ],
    [ "Offset", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html#a194f7ed570e760c2fcfdebbf2f27b5fd", null ],
    [ "Stride", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html#ab80f92d858832c4f509f067bbb2d3e7a", null ],
    [ "UntypedData", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html#a9deda074fcf308a74a59c6ddfed99daf", null ]
];