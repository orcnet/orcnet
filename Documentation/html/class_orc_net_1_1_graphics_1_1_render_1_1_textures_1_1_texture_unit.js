var class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit =
[
    [ "TextureUnit", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit.html#a320301266414393653892f2fd21cd239", null ],
    [ "Bind", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit.html#a643c667f07f6864f01d8d9ff6fbb659e", null ],
    [ "CurrentBindedSampler", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit.html#a701239799e42da982203b02078d48ccc", null ],
    [ "CurrentBindedTexture", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit.html#ad813373f33c5916f8b71ba8f2be61439", null ],
    [ "IsAvailable", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit.html#a858d3acba755568f691eecf3a99a7c7a", null ],
    [ "LastBindingTime", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit.html#ac1d8aeb81157225166bb3d542d6b1053", null ]
];