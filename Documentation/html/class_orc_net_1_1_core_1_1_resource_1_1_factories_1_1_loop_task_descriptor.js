var class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor =
[
    [ "LoopTaskDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#ae7bdcc6bf84c9a7b0d694a2191b39fb9", null ],
    [ "Create", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#a1201a545fc2424d35f1d4e79dc915680", null ],
    [ "FactoryType", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#a800664ab14796715b07f3446a5ddb347", null ],
    [ "Flag", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#ab104e5691ad9b3cd3ed74bf76bcfddfd", null ],
    [ "OnlyVisible", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#a2d9e5e13e7e641a50fb4b41bd215a526", null ],
    [ "Parallelize", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#a21416b4046ca67be9c5b8afce315ed2e", null ],
    [ "SubTaskFactory", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#a59c867d7c972866c0629aa4338f2696c", null ],
    [ "VariableName", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#a39512c6ddbf7f77d88c70e53dad9e80c", null ]
];