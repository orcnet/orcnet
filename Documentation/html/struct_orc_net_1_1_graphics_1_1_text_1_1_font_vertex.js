var struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex =
[
    [ "FontVertex", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#a78bd08217bf1549929540e4aee488fca", null ],
    [ "A", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#a29909d2a73a718fe374866d41d3c364e", null ],
    [ "B", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#a189c9e9c9bc260ed00c6739ab45803ce", null ],
    [ "G", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#aed06966a3b4f9513c06ce108d96b5d20", null ],
    [ "R", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#aab36dd1f83149ea879696e31160de2f2", null ],
    [ "UV1", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#a815853033c4622c0dfecb12e197dd1ed", null ],
    [ "UV2", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#a09a724d402e57de3d8df258b46b40b1d", null ],
    [ "UV3", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#aa2a5cb8a1de4ef9343c1270c98d94230", null ],
    [ "UV4", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html#a2e510fdcddb9c81b754c4553f1c8eed3", null ]
];