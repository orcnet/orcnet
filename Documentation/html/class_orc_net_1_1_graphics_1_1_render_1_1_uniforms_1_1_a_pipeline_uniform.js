var class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_a_pipeline_uniform =
[
    [ "APipelineUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_a_pipeline_uniform.html#a173773c5c588230978d72c301e416ffd", null ],
    [ "SendUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_a_pipeline_uniform.html#a7195a9d679f06cd2485417dface18269", null ],
    [ "Name", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_a_pipeline_uniform.html#ad6fb6a1f6860297826ee38b1344ab76b", null ],
    [ "Type", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_a_pipeline_uniform.html#a56b8f9e48a3098aafa8b3c20e0545857", null ],
    [ "Value", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_a_pipeline_uniform.html#aae781edc09ee90bba26a997b6433a01e", null ],
    [ "Value", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_a_pipeline_uniform.html#ab4849eb08a74d16d6e7936602fe90eda", null ]
];