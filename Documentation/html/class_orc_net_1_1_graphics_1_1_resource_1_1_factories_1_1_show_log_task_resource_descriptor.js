var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor =
[
    [ "ShowLogTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#ae5489633af540c32ad0c6bf4197f7c4d", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#a1c18b0a89f73ef186fd4597f9981032b", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#a09a31c32569312d0dbe873d9e585970b", null ],
    [ "FontHeight", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#a298fae46ca601be12719529f77001597", null ],
    [ "MaxLineCount", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#ac68efa76fb544cf44c8378e6ae20faf1", null ],
    [ "ScreenX", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#a9763388d9dc46676daa7c4366a6c25bc", null ],
    [ "ScreenY", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#af313197e2f63693a9c8df569e0f32bfb", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#ab1a207caf6e97ca604eff8c137f6b2e7", null ],
    [ "TextFont", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#acea6375c4706c151ad0d3b03b876bc71", null ],
    [ "TextPass", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html#ae0a0e2ba411cc46bad9baf9461022d43", null ]
];