var interface_orc_net_1_1_core_1_1_resource_1_1_i_resource =
[
    [ "PreUpdate", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource.html#a4d31ec4d3e1d41a1dd9175b401f47463", null ],
    [ "Update", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource.html#a791518b888507e42963486c8c6286ffb", null ],
    [ "IsModified", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource.html#afc1bf14c86be3bae6b5d9843d2341edd", null ],
    [ "Name", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource.html#a42bc9474d670c5e73f4c7284f649f1cf", null ],
    [ "OwnedObject", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource.html#ac9ef242c6a9bc407120825d7f8f36974", null ],
    [ "UpdateOrder", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource.html#a8d15d0ca740920a8a141b1e80768300b", null ]
];