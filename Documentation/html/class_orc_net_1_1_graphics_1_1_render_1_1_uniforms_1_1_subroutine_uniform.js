var class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform =
[
    [ "SubroutineUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#ad3ebb09a98a560073a400327810ae78d", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a41ccf5da0d91629bfff6a834a3fb750b", null ],
    [ "SendUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a234774a4af210404084f2be7ad20f5fc", null ],
    [ "Block", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a685ac67a9c8b596e331dcf5a5505c669", null ],
    [ "CompatibleSubroutineNames", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a84a6e9dc4c4970cb9a230d86bf7e89a8", null ],
    [ "Location", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#aa00a90ebb64e77cdfc4ca1ee23a04c22", null ],
    [ "Name", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#ad21157d10608ee3ee062a0320a4efff2", null ],
    [ "Owner", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#aa9e4c63c22b16b89f0bed4dddd82e7a9", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#af9a842a38509ae4f4bd9aff234057289", null ],
    [ "Stage", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a6bf4884511687c12c082c42d30f20d6c", null ],
    [ "SubroutineName", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a63f5523bad27632ff557d88bdeff198f", null ],
    [ "Type", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a2c0fe739e36cf604fe309a8a59d5180e", null ],
    [ "UntypedValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a2fef89d4f3e88a2c32635ef17f69c47d", null ],
    [ "Value", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html#a32877c4a7025c6f6684e5328655e308b", null ]
];