var namespace_orc_net_1_1_core_1_1_task =
[
    [ "ATask", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html", "class_orc_net_1_1_core_1_1_task_1_1_a_task" ],
    [ "ATaskObservable", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable" ],
    [ "IScheduler", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler.html", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler" ],
    [ "ITask", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html", "interface_orc_net_1_1_core_1_1_task_1_1_i_task" ],
    [ "ITaskContainer", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_container.html", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_container" ],
    [ "ITaskFactory", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_factory.html", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_factory" ],
    [ "ITaskObservable", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_observable.html", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_observable" ],
    [ "ITaskObserver", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_observer.html", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_observer" ],
    [ "ITaskProfiler", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler" ],
    [ "ParallelScheduler", "class_orc_net_1_1_core_1_1_task_1_1_parallel_scheduler.html", "class_orc_net_1_1_core_1_1_task_1_1_parallel_scheduler" ],
    [ "TaskComparer", "class_orc_net_1_1_core_1_1_task_1_1_task_comparer.html", "class_orc_net_1_1_core_1_1_task_1_1_task_comparer" ],
    [ "TaskDependencyException", "class_orc_net_1_1_core_1_1_task_1_1_task_dependency_exception.html", "class_orc_net_1_1_core_1_1_task_1_1_task_dependency_exception" ],
    [ "TaskSet", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html", "class_orc_net_1_1_core_1_1_task_1_1_task_set" ]
];