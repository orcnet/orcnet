var class_orc_net_1_1_core_1_1_task_1_1_a_task =
[
    [ "ATask", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a211b3b0c7845b68a9534b451c2f6f466", null ],
    [ "Execute", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a4e1d93d5f2f16ce3d9f675efaec5675f", null ],
    [ "OnDispose", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a4fca2d5dace436c617cc658c0f73ab36", null ],
    [ "PostExecution", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a79495e324116a9402139d5273e7cf3cc", null ],
    [ "PreExecute", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#abeab9125108603a23609a2ae4ff9d396", null ],
    [ "Prepare", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#aeef745042c8f4a8737746df43ff5016d", null ],
    [ "CompletionDate", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a29a1db7717fda1f994cf10807dc217c4", null ],
    [ "Complexity", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a89bcafd662bf1986e80acbc33bdaa94c", null ],
    [ "Context", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a14afd5ec3302c7584769c28006886780", null ],
    [ "DeadLine", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#aa9aaa6e266a565213172fa0f8c2ca167", null ],
    [ "ExpectedDuration", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a97959b01840b21460b350de416439d6b", null ],
    [ "Id", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a5b87df773adc4d8c8d67cafea1e26c50", null ],
    [ "IsDone", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#a7d06c46ddae594e1028a223d7b36bded", null ],
    [ "IsGPUTask", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#adc64b84705f816e8f1494efa4c5161ca", null ],
    [ "PredecessorsCompletionDate", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html#aca1f1cd2e9a5eeb8a0cd1b5569bf266b", null ]
];