var class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description =
[
    [ "SamplingDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#a25f1d41350e1765e48bae9cf231889ba", null ],
    [ "Clone", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#aed7e686e31c785d9dce39f350685b8ef", null ],
    [ "ForceOpacity", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#a69bd4362b7492edead90dd7b7a9c0d22", null ],
    [ "FragmentCoverage", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#a32abfe3a6cb6b32a1455129033a0f926", null ],
    [ "MinSampleCount", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#af9b2bf0052d46994bb549f4b8698d032", null ],
    [ "SampleAlphaToCoverage", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#a682a25f0fe91af1dfda9aa5e53f2b359", null ],
    [ "SampleMask", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#a3b6ce9875383215b8e9456f87fe502fe", null ],
    [ "SampleShading", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#a97d0aa464a90ac6a2ba6351436c1ee67", null ],
    [ "UseMultiSampling", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html#ad198af5dc130bbd90af6fa934d89f8d5", null ]
];