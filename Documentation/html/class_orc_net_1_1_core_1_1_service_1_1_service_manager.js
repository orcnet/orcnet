var class_orc_net_1_1_core_1_1_service_1_1_service_manager =
[
    [ "CreateService", "class_orc_net_1_1_core_1_1_service_1_1_service_manager.html#a755cb26af43739df978a65a183d10bae", null ],
    [ "GetService", "class_orc_net_1_1_core_1_1_service_1_1_service_manager.html#a013b7445cf74afb82ce79d8dd987e805", null ],
    [ "GetService< T >", "class_orc_net_1_1_core_1_1_service_1_1_service_manager.html#a0b730815bb40c44cab16e749b23f0aa3", null ],
    [ "RegisterService", "class_orc_net_1_1_core_1_1_service_1_1_service_manager.html#a15eac5f89200c405f5ec7c3e5a54aa5e", null ],
    [ "UnregisterService", "class_orc_net_1_1_core_1_1_service_1_1_service_manager.html#aaa6678d687cc201010a68c801fda4fe8", null ],
    [ "Instance", "class_orc_net_1_1_core_1_1_service_1_1_service_manager.html#a629c4c753c543cb6ad50dcd4a7bc64e6", null ]
];