var class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description =
[
    [ "PolygonDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#a03a161a7e26df6edc6a9a76968845b79", null ],
    [ "Clone", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#a9e1563a7ab5cc01c256345d36499b48d", null ],
    [ "AreFrontFacesCW", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#ad9f8de80b670cd46142048f0813ecde0", null ],
    [ "BackMode", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#ada371a7f1e47b3916e6608749e5ace83", null ],
    [ "CullBack", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#a88dce21051d633612e81b739e55faffc", null ],
    [ "CullFront", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#a67fd65d0d709ef559fda8044415438c4", null ],
    [ "FrontMode", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#a967a048a1dd8626a6b2344c62f5fa4d6", null ],
    [ "PolygonOffset", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#a71b94969d0c5cb974dee6cd5ccf9f36a", null ],
    [ "PolygonOffsets", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#a2e5514337e85e2f1f6f5cfeb97d1958d", null ],
    [ "SmoothesPolygons", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html#a5fa4fc6eceb932231b2092ebcb85ea72", null ]
];