var class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory =
[
    [ "SetTransformsTaskFactory", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a35b72597692756891483f3280eb106f3", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#ab66a82cd91839e5ac2c6945837f32855", null ],
    [ "CameraToScreenUniform", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#acfe6f8c71cfa0fd5ceba654d8c0eaf3e", null ],
    [ "CameraToScreenUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a558871a9f2ddf5b873d3fc94a9cbf825", null ],
    [ "CameraToWorldUniform", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#aa4477d12ff6320ad80e09868b08a5974", null ],
    [ "CameraToWorldUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#aef177fce5c448b735a0bc6b371822fc6", null ],
    [ "LastPass", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#acc7e0fa80b6c31b058d38ac4d19aaf5b", null ],
    [ "LocalToScreenUniform", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a5465e5d53841c9220d858e801a65839a", null ],
    [ "LocalToScreenUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#abb4318fc41e85bd6cd65b562eb8401a1", null ],
    [ "LocalToWorldUniform", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#ae6979ebfa4ce8668dea94e1bfd8a09c8", null ],
    [ "LocalToWorldUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a6753b2df897ee6515fc28a624cde5447", null ],
    [ "PipelineDescription", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#ae5c37438408c13f770434f417216f26b", null ],
    [ "Screen", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#adcf9be4d34880b1a00a8eb9449b5c655", null ],
    [ "ScreenToCameraUniform", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a363c0eab99ed78413f706860a587edea", null ],
    [ "ScreenToCameraUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a9b878daa1323c5bc57c0ecfb2d39c379", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a14fd6da05423e3bdef64ea89c61ee627", null ],
    [ "TimeUniform", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a3a8af232430c73b6489941678b97b062", null ],
    [ "TimeUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#aef65fe6ceb2fb3941dc3921c9e4c4453", null ],
    [ "WorldDirection", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a0723bcd114e5b20320e56fc52e45d974", null ],
    [ "WorldOriginUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#ae56bf4a35aa1d324f30af35915caff76", null ],
    [ "WorldPosition", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#addd6b4293cadf64d285670bcd337f950", null ],
    [ "WorldToScreenUniform", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#a059848b57caac4fbe16be9d0cc632822", null ],
    [ "WorldToScreenUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#aa55f5b193d1960d418c75399d1585958", null ],
    [ "WorldZUnitUniformName", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html#ac30bd6792d9df3f442d0c0609c2cee1e", null ]
];