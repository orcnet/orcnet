var class_orc_net_1_1_diagnostics_1_1_task_1_1_task_statistics =
[
    [ "TaskStatistics", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_statistics.html#a07fb4f30f021f5fc6817da9106766940", null ],
    [ "DurationSqrSum", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_statistics.html#ae09bb97505c246b8ec75032c0ee9024a", null ],
    [ "DurationSum", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_statistics.html#a7d6899e9dd3f6d9f4e4db47a506bcd0e", null ],
    [ "MaxDuration", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_statistics.html#a9e1e8e5a770d0911f1e8838066180182", null ],
    [ "MinDuration", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_statistics.html#a76a5e13526820c382596c684bf66fecf", null ],
    [ "TaskCount", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_statistics.html#a579738a24ac9967b0f22597a43cf9a36", null ]
];