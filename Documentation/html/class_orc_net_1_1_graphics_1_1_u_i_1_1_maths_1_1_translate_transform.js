var class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform =
[
    [ "TranslateTransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform.html#aac4a6e09d398d24eb191cafb60bb5cb8", null ],
    [ "TranslateTransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform.html#a5783ab463102335591a1d9aa5297eef2", null ],
    [ "Clone", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform.html#ace8dfbaa3c0e64dbe04a158cf52a496a", null ],
    [ "IsIdentity", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform.html#adf7249a829ad061ff57a8b2b3f917d28", null ],
    [ "Value", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform.html#ae67a158e182ca871a4d485e5ca63a9be", null ],
    [ "X", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform.html#a29a8cd9e59b2a3c0836542581c723c3b", null ],
    [ "Y", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform.html#a6e6b5a2560e0e70872a37c096d76d371", null ]
];