var class_orc_net_1_1_core_1_1_logger_1_1_log_manager =
[
    [ "CreateLoggerDelegate", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#a0b314c2c5c5e5281b9f5dbfe5483b4d2", null ],
    [ "Dispose", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#ac45d4bc7ec908f11030de3eee8bc237a", null ],
    [ "Initialize", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#afba0477a10b11bf428ea310a6dcc8b41", null ],
    [ "Log", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#aaf3204dbadf91a316eeb4ae624335029", null ],
    [ "Log", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#a0223f7631c8d41149fd26decfc42b422", null ],
    [ "Log", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#a244206f4bf8fabb7eef8304f579a4dc9", null ],
    [ "CachedLogs", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#aa30f116b7b4d5cb0879a1f08fbe363c4", null ],
    [ "Instance", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#ad1677e0284dd3cd6ec6a8e556c8b00a8", null ],
    [ "MessageCount", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html#a2a9e01f84ef43dedcd6f1ead96de3cc0", null ]
];