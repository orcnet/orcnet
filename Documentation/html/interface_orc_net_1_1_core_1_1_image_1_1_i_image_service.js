var interface_orc_net_1_1_core_1_1_image_1_1_i_image_service =
[
    [ "IsHDRFormat", "interface_orc_net_1_1_core_1_1_image_1_1_i_image_service.html#a9e3618e82b6fcfb0116be527885deb53", null ],
    [ "Load", "interface_orc_net_1_1_core_1_1_image_1_1_i_image_service.html#ad4b04a7e76a47a5b81ea3ea504456f1a", null ],
    [ "Load", "interface_orc_net_1_1_core_1_1_image_1_1_i_image_service.html#aa4511671374ca2cddaea85dbc855a3b8", null ],
    [ "LoadHDRFormat", "interface_orc_net_1_1_core_1_1_image_1_1_i_image_service.html#a60f40e03ffe16db02be2ddf224f25c45", null ],
    [ "Save", "interface_orc_net_1_1_core_1_1_image_1_1_i_image_service.html#a9e5241ef88d5f671523ee43c12d326d2", null ]
];