var class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style =
[
    [ "Style", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#a86aff99c9cc16c2f16e8fba39c648179", null ],
    [ "Style", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#aafc7f694c3a8410aadfc20c19ef43bb2", null ],
    [ "Style", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#a2abf3c88ebaa5f758a923a134a2842cd", null ],
    [ "GetHashCode", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#ae263f752b9b5db1db99c767a303cc6e0", null ],
    [ "Seal", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#a865b2e00fa8fb5c4d580a32bc6b8c0d3", null ],
    [ "BasedOn", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#af673f13706301da974a851026b4cb274", null ],
    [ "IsSealed", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#a39d75ac46a9e212e589bb03b20bbebd1", null ],
    [ "Resources", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#ad0321675895554ef09914193e62213f3", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#a7c17867850162deee4a47bf59c3c17d6", null ],
    [ "TargetType", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html#a7a8184ad40d4b88e1684d7ec88f510db", null ]
];