var class_orc_net_1_1_graphics_1_1_render_1_1_scissor =
[
    [ "Scissor", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor.html#a955c1b794afcc998422fea7c74147635", null ],
    [ "Clone", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor.html#a3f3a19a98b3389cf67065bb120764b5f", null ],
    [ "Bottom", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor.html#aebf5e8f5117f7689508ba9f0e69a9e2d", null ],
    [ "Height", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor.html#ad77cf27bf63fde3eac68e7df572b4e9c", null ],
    [ "Left", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor.html#a08db20376cc5b92de53dc8e6f71c1594", null ],
    [ "Width", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor.html#a67baa6ad88b8474b8ec83a29ec07ad77", null ]
];