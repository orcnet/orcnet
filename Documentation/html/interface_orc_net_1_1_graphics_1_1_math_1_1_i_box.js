var interface_orc_net_1_1_graphics_1_1_math_1_1_i_box =
[
    [ "Contains", "interface_orc_net_1_1_graphics_1_1_math_1_1_i_box.html#a3a0fbe3823fe9a604f61d367c6144996", null ],
    [ "Intersects", "interface_orc_net_1_1_graphics_1_1_math_1_1_i_box.html#a62217c1887ad5c12bb9107116738864e", null ],
    [ "Merge", "interface_orc_net_1_1_graphics_1_1_math_1_1_i_box.html#ae02408fa931e2794c7b03848904502ad", null ],
    [ "IsFinite", "interface_orc_net_1_1_graphics_1_1_math_1_1_i_box.html#a7edd99e5804e09ac4386bf5b79739169", null ],
    [ "IsInfinite", "interface_orc_net_1_1_graphics_1_1_math_1_1_i_box.html#a69d05cc493f5d513b3b88cc1330c57c2", null ],
    [ "IsNull", "interface_orc_net_1_1_graphics_1_1_math_1_1_i_box.html#a08d8121cce64ede906d065d2fbfe5413", null ]
];