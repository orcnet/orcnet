var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_descriptor =
[
    [ "Texture1DDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_descriptor.html#a96584cf6fd0a234937590046132a6041", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_descriptor.html#a59558e70a1805f7da009214eda966fe8", null ],
    [ "OnDispose", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_descriptor.html#ab5396f8c961fab83ad56456a1349966f", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_descriptor.html#ab2381573f0fab6619c710a8e28d612ff", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_descriptor.html#a712ef9eec1c453ad644a4cd351b3d5f3", null ],
    [ "Width", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_descriptor.html#a9460f542d2321a33178d0e1124272337", null ]
];