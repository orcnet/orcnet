var class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry =
[
    [ "EllipseGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a42021769d155ec40eb09ae8b601bc64e", null ],
    [ "EllipseGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a6c4e9c75eb70acdc57867fbf463a2544", null ],
    [ "EllipseGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#ae820e5fe140d7df525b7d15d09168c9e", null ],
    [ "EllipseGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a657c5a436109b1863d503b17b1c47eca", null ],
    [ "Clone", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#ad095169c30ec0bb33feb4d36569f156e", null ],
    [ "FillContainsWithDetail", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a7be08ebfdc0054e9c4729e806f429f66", null ],
    [ "IsEmpty", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#ab7967d693a6618c8f42b81b47d5edba5", null ],
    [ "MakeEmptyGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a5dc551f69ba4c9b8f95d3f3a7893278d", null ],
    [ "MayHaveCurves", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#ade7d5cbb6dfbca7a6b0805c99b4babf1", null ],
    [ "StrokeContainsWithDetail", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a47fe13d59355888c93122d6951c71514", null ],
    [ "Bounds", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a1d37b82cc6a9ca04c8d5ba9cac4078fe", null ],
    [ "Center", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#aab4a5a112928c4d41ccdd0ea7d1dbcde", null ],
    [ "RadiusX", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a1c7ad08ac3557004bb1a52f1cb2af36b", null ],
    [ "RadiusY", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html#a89fc9242132481be7586a22c302f9e57", null ]
];