var class_orc_net_1_1_core_1_1_math_1_1_box2_i =
[
    [ "Box2I", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#aed662fadba7d45b837c222eea697162c", null ],
    [ "Box2I", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#af9442443587f5d0e953f12ed0d968821", null ],
    [ "Box2I", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#afcbc7f2207aacddd022dd0d47119d653", null ],
    [ "Contains", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#acc92b594a155a3df2e042b4991baa2c6", null ],
    [ "Contains", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#ae4568989b9b5699d1fd2b178a87776ac", null ],
    [ "DistanceSqTo", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#af1ef0258fc14c97f633943adf1ef6a51", null ],
    [ "DistanceTo", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#acbcee6bffbcc1eba759ade3bdf007a12", null ],
    [ "Enlarge", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#a8cb3d3355177d10bab5756ff8bd7248b", null ],
    [ "Enlarge", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#a440da76fba9ee469253560c4a8b2f273", null ],
    [ "GetNearestBoxPoint", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#a6f6e28986dec60da9497941a5e909934", null ],
    [ "InternalClone", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#a3e86cd9c8b5c76e7b255b3017cd23494", null ],
    [ "InternalMerge", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#a54cb7dcdb4337601e3ecc9a23a308707", null ],
    [ "Intersects", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#a827b3a606342b6ebd2a97b42f425c95f", null ],
    [ "Area", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#ab7c556c2b716b52b3166cb763c88af41", null ],
    [ "Center", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#a6774689e705101377e3709a0f40e0437", null ],
    [ "Extents", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#af3fa6e39a9026c3c397e70e96d1c2ac7", null ],
    [ "Height", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#acc504d08af683469c07eeafab96ef7a5", null ],
    [ "Width", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html#aae07664bbccc25f87abe2a6f6373489e", null ]
];