var namespace_orc_net_1_1_graphics_1_1_math_1_1_extensions =
[
    [ "Matrix4dExtensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_matrix4d_extensions.html", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_matrix4d_extensions" ],
    [ "Matrix4Extensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_matrix4_extensions.html", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_matrix4_extensions" ],
    [ "Vector2dExtensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector2d_extensions.html", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector2d_extensions" ],
    [ "Vector2Extensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector2_extensions.html", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector2_extensions" ],
    [ "Vector3dExtensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector3d_extensions.html", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector3d_extensions" ],
    [ "Vector4dExtensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector4d_extensions.html", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector4d_extensions" ]
];