var interface_orc_net_1_1_core_1_1_task_1_1_i_task_container =
[
    [ "AddTask", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_container.html#a5caacd58d378e86c9484186379e9d6c4", null ],
    [ "RemoveTask", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_container.html#ab9844ffbc118c88161135560e91cd53f", null ],
    [ "IsEmpty", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_container.html#ad7fbf9dfb91ea6b097358d8564a25e63", null ],
    [ "Tasks", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_container.html#a573972eb0f9c2dad519c2eca8d0e2511", null ]
];