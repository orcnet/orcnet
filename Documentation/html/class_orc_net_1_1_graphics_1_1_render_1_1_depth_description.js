var class_orc_net_1_1_graphics_1_1_render_1_1_depth_description =
[
    [ "DepthDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_depth_description.html#a361c00086af4c70ba0b0cf7f9ddf37e1", null ],
    [ "Clone", "class_orc_net_1_1_graphics_1_1_render_1_1_depth_description.html#a788c46adbe75dd5c0d838d6383ba5686", null ],
    [ "DepthTest", "class_orc_net_1_1_graphics_1_1_render_1_1_depth_description.html#a5cf5bed0c2da4e2b6bf6662b52373755", null ],
    [ "DepthFunction", "class_orc_net_1_1_graphics_1_1_render_1_1_depth_description.html#a4a550a30f00126a69b2878f5bb9e07f5", null ],
    [ "UseDepth", "class_orc_net_1_1_graphics_1_1_render_1_1_depth_description.html#ab0280415851af2ce10906a87b8a90ef8", null ]
];