var class_orc_net_1_1_graphics_1_1_g_p_u_timer =
[
    [ "GPUTimer", "class_orc_net_1_1_graphics_1_1_g_p_u_timer.html#a1bef26210c3bfb3b82184e10c585a9e1", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_g_p_u_timer.html#aadb676de293591030537b73764560fdf", null ],
    [ "End", "class_orc_net_1_1_graphics_1_1_g_p_u_timer.html#a71b7e7398858f54fd3ba7c3232039f7d", null ],
    [ "Start", "class_orc_net_1_1_graphics_1_1_g_p_u_timer.html#a29ad43276c97621a932802da9a1ada36", null ],
    [ "AverageTime", "class_orc_net_1_1_graphics_1_1_g_p_u_timer.html#a4b9f071a0522e857a93dec1ddd68d5eb", null ],
    [ "LastDuration", "class_orc_net_1_1_graphics_1_1_g_p_u_timer.html#aba73a5557bd2cfb313b7a81c57c9f5f6", null ]
];