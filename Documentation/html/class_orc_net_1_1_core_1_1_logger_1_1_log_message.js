var class_orc_net_1_1_core_1_1_logger_1_1_log_message =
[
    [ "LogMessage", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#a9671955fcfca62f41168d2903f46799d", null ],
    [ "Dispose", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#a9a2def18e0cf852b120c4fd3886b2013", null ],
    [ "operator LogMessage", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#a421b3bf07e5b3a0dc249425eb02e2814", null ],
    [ "ToString", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#a52d73dd6d8a6b77c097cc1ccffa23050", null ],
    [ "ToXml", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#aa6bb2a57fcc32fba1987673e79b73665", null ],
    [ "Context", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#a262a819d2fb2612082274c9303bae05e", null ],
    [ "Message", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#a3f5ad5d563106e8623f962f2db5cbeac", null ],
    [ "StackTrace", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#a6d541194e8ef8c4f2e780233bc44bbc6", null ],
    [ "Type", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html#a7cf6e3ed40e80b5d45fdc7c7e98560ad", null ]
];