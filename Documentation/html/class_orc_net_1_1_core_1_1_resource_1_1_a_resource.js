var class_orc_net_1_1_core_1_1_resource_1_1_a_resource =
[
    [ "AResource", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#a9ad0fbb8f07758027f36773d84e6a46f", null ],
    [ "Dispose", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#a7f68c4c25bb558670f0847c1c0fc7f84", null ],
    [ "OnDispose", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#af774120f34c75825b05ac5d5b2d8a62b", null ],
    [ "PreUpdate", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#a42cd1b3241f15e6284fa89847bed86a0", null ],
    [ "Swap", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#a44387a5c084276c2b2e05568a382bf1f", null ],
    [ "Update", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#ada6163f6ce507c2e89d74e325e9f2bec", null ],
    [ "mDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#ac355576f47886479565f1a4ff1057a1b", null ],
    [ "mEditedDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#afd8df664f299928cdb5d79c3939e2700", null ],
    [ "mOldValue", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#a39c70c279b35a68467144f9eb3baf2e9", null ],
    [ "IsModified", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#af584ae8758c8f317363c09049d58671f", null ],
    [ "Name", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#a87cb046d65fa1f948d3b3ff57ba0c401", null ],
    [ "OwnedObject", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#a52b47b531baa530506fde28b2b836fd8", null ],
    [ "OwnedObject", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#a70453e53455c8b6304c5e23ce648a5df", null ],
    [ "Size", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#ad63fe52e3d87b856b50afba8ec605608", null ],
    [ "UpdateOrder", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html#aad4f5ec11c43e7996bbbbf32065552bb", null ]
];