var class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_input_event_args =
[
    [ "InputEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_input_event_args.html#adc14d25cd554709b90eb06abe1f7c4e5", null ],
    [ "GetData", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_input_event_args.html#a8193a0c358484a3bdf5c43387a354728", null ],
    [ "SetData", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_input_event_args.html#a42a5c561b19176379ae5aa66cdd347cf", null ],
    [ "Device", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_input_event_args.html#a63b4b7311c5522e80f0d4e66e7ffa8c2", null ],
    [ "TimeStamp", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_input_event_args.html#a7c0ca635b5387640740e9b2c9d5713f9", null ]
];