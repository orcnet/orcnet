var class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_parameters =
[
    [ "TextureParameters", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_parameters.html#ad2fedbdda79ad56284cd5b7dc4aadfa8", null ],
    [ "MaxLODIndex", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_parameters.html#a2ce999e1073c899bd00a7781d7ab3b92", null ],
    [ "MinLODIndex", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_parameters.html#a6332fd73832b4da9782b97ad599be7ba", null ],
    [ "Swizzle", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_parameters.html#a8e05bd980ab45a37ea0986066b6b031d", null ]
];