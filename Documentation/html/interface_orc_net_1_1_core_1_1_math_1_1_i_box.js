var interface_orc_net_1_1_core_1_1_math_1_1_i_box =
[
    [ "Cast< OtherT >", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a03a7767e6127b8f1b6244dac26447d2f", null ],
    [ "Contains", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a516f0c60e565512f0962115ff7e2300c", null ],
    [ "Contains", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a411f10b87ea811e5bfa3094e86ef6875", null ],
    [ "DistanceSqTo", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a1b24b9b319c462491396acd29cedb34d", null ],
    [ "DistanceTo", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#aadfd9cbe443b18b97a86b7f090c5a525", null ],
    [ "Enlarge", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a6f41733e1f89e9a7337b810d61f51284", null ],
    [ "Enlarge", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a3f804aede709ec8e8dc27508b81adbb9", null ],
    [ "GetNearestBoxPoint", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a27bb2c60b7546aea5d0a392364415200", null ],
    [ "Intersects", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a60bf25a63087f9cb0661f4e3af8cf95c", null ],
    [ "Merge", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#ab2568b33da7339f3f2d0efddf7750dff", null ],
    [ "Center", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a55afe4a85db77804a61427a7294af54d", null ],
    [ "Extents", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#ab66e35a3c6105481452f8560ef454245", null ],
    [ "IsFinite", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#aaeddb26aef10a2d618ea7c9eaae2d0a9", null ],
    [ "IsInfinite", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a17fb72fb039915cc7bb166f27c6ba1ee", null ],
    [ "IsNull", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#afc1c97145f5d5c4dca55519f8609d9b4", null ],
    [ "Maximum", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a3a0053dafb646bc58fbf4d63f6a0a207", null ],
    [ "Minimum", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html#a68d35c22f16bf365b3b6cb7da4bac32e", null ]
];