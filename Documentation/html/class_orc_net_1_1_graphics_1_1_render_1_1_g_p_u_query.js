var class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query =
[
    [ "GPUQuery", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#a1fd55f4e096d243c38d748f327c01d66", null ],
    [ "Begin", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#aae9fa4cd74e2db940f695cb955c710fe", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#afc6e839a6c5f730c1452c8e749e59be8", null ],
    [ "End", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#a27b3d1c0f4c5f4460bc16c0cb01dea83", null ],
    [ "Id", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#a543c322da3b735d1bf8d9741f0bd81d2", null ],
    [ "IsResultAvailable", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#a3ca344c94c9256b4c94fa866d47da83a", null ],
    [ "Reason", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#acebf40caaf3808b458ffc69d4f769082", null ],
    [ "Result", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#a2dab0213e8d584f85135b1f96a29e3b6", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html#a745dc5c079576ab151dd2e28505b8ab1", null ]
];