var interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform =
[
    [ "SendUniform", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform.html#a9c57255a29f39a99038b91fbb889d961", null ],
    [ "Block", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform.html#a441fc5236a5843be063aad5468967120", null ],
    [ "Location", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform.html#ac26a51987843d693da951da756cd4a18", null ],
    [ "Name", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform.html#a6d6fa5460a26c0bc0849055e3a0d1abd", null ],
    [ "Owner", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform.html#a88deef1f74b7d5f4c570761db3705107", null ],
    [ "Type", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform.html#ac81182b70e43f04c91b8c2361ad25c14", null ],
    [ "UntypedValue", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform.html#a1c2c45d66d84ff4de61146a094436dc4", null ]
];