var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_font_resource_descriptor =
[
    [ "FontResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_font_resource_descriptor.html#a376978c2e4cbd32c904e721fb45dca33", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_font_resource_descriptor.html#af2236b4bd3e995cd5d38f59a35b66470", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_font_resource_descriptor.html#ae490cc501f4f8016d64b9d10667fd8d0", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_font_resource_descriptor.html#aa0bf17c404e53b51ca5913b0ca9b3f9e", null ],
    [ "TextFont", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_font_resource_descriptor.html#a876a40548e9837b1cad807b944212242", null ]
];