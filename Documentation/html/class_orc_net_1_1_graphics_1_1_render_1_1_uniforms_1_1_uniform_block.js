var class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block =
[
    [ "UniformBlock", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#ad698f1d3442109be8e14e56e370cca92", null ],
    [ "GetUniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#a8ba6a4a6679f44c0b8776adbaad6addd", null ],
    [ "Buffer", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#a6742a7197237c9d6dbb9e88301b81535", null ],
    [ "Index", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#a5436fb4f1915834df69b017b2569bce9", null ],
    [ "IsMapped", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#ac8f4b0842bce29bdc9422e2f978da912", null ],
    [ "Name", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#aa580d1a9d2a7d9791ab59e4be94b97e4", null ],
    [ "Owner", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#abc3c542f5c5a1a69f36bccf749aba060", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#a5d9a790fac02091fb6fde5ddc270dae3", null ],
    [ "this[string pName]", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#abefa73b989a272dd98e904290dd4dfd2", null ],
    [ "TotalSize", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#a2b36f74d595f2e35beb6a28eeaf84b2a", null ],
    [ "UniformCount", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#a3d3ac10935685b271c56df9be9f60994", null ],
    [ "Uniforms", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html#a4606b243e1af1640d4cc0578268d1149", null ]
];