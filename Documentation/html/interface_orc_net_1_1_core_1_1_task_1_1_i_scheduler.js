var interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler =
[
    [ "Execute", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler.html#a41fbb66fd478e049f76d38cc48d768d8", null ],
    [ "ReSchedule", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler.html#a40969178c1538eb8281fb66649da79b5", null ],
    [ "Schedule", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler.html#a97917146db5dd2289657de77bb5942e2", null ],
    [ "SupportsCPUPrefetch", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler.html#a2cdc7b6c1215f7417acf168cdc9ad96a", null ],
    [ "SupportsGPUPrefetch", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler.html#acb285ae24d504ba649e800e4e7f20474", null ]
];