var class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor =
[
    [ "SchedulerDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html#ad24fec455ca144902c458077556a0dc6", null ],
    [ "Create", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html#a504a2cfd48f34954ef2b6cc25b4ae9e6", null ],
    [ "FactoryType", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html#a0dbeb7b489cc499907861d61df19cc14", null ],
    [ "FrameRate", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html#af56786132cfce70dae74f5045bef59ca", null ],
    [ "PrefetchRate", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html#a79703f354b86e773e00a155e31c71e9e", null ],
    [ "PrefetchSize", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html#a3e44057c39aad30565059880552228eb", null ],
    [ "Size", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html#a560c1d55054d025300c051496433296c", null ],
    [ "ThreadCount", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html#a36d34c51a80ff5f90e2b8be03da2337c", null ]
];