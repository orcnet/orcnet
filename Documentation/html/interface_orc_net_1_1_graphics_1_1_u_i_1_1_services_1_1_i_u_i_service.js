var interface_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_i_u_i_service =
[
    [ "Render", "interface_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_i_u_i_service.html#aa30b7c0094e9252bcfc340c06fd0e546", null ],
    [ "UpdateLayout", "interface_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_i_u_i_service.html#a228edfb0f81dd2d4eaca7416cf98bf94", null ],
    [ "Dispatcher", "interface_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_i_u_i_service.html#a5ab16268b78e12c5286ae2953b4a8501", null ],
    [ "PresentationSource", "interface_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_i_u_i_service.html#a38b6c3ea33b2e29e166ea282bba9a281", null ],
    [ "LayoutUpdated", "interface_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_i_u_i_service.html#aaf3a35b38624f6dfd57cd4ed1a660e31", null ]
];