var class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description =
[
    [ "StencilDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a7532316783910c6a6c87f2b37b2cb521", null ],
    [ "Clone", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#aeda245030a06843ca09785f3bc6b876a", null ],
    [ "StencilTest", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a7e16bdb9fb908f93332a61c639807be5", null ],
    [ "StencilTest", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a45ac6f5a289efc196c138d6f416c497e", null ],
    [ "BackDepthFailAction", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#aa25ec3c9d94243d56c14179ca76c45b4", null ],
    [ "BackDepthPassAction", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a7ef949128ba781eacec2ac931117dc98", null ],
    [ "BackFunction", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#ae14b8e06d343eaa36e825563370b2e0a", null ],
    [ "BackMask", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#acd1406b264e0928ee0cdad9eb852dec6", null ],
    [ "BackReference", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a0c89f92742c8a2ab71b30e3ffc47c0d2", null ],
    [ "BackStencilFailAction", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a10ccebe82412c8c1c2aa026e8b81e5e6", null ],
    [ "FrontDepthFailAction", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#aebc93c8f6dcec0997045f7ede85d7313", null ],
    [ "FrontDepthPassAction", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a7baecd73b8412c400f7031cbe30a109e", null ],
    [ "FrontFunction", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a90e1fff47b67d94359b1602fe868acb3", null ],
    [ "FrontMask", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a80cae4eed6f29a11631b0525d12d1ad5", null ],
    [ "FrontReference", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a81a70e38a2ab3e9949c4cc1ece206496", null ],
    [ "FrontStencilFailAction", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a0f775e050ea48cd368570a75c8303e68", null ],
    [ "UseStencil", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html#a465d5d05bc8ddc6494828abb55739b37", null ]
];