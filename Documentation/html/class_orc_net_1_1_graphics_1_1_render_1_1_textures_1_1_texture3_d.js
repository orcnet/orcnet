var class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d =
[
    [ "Texture3D", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#a638437cb9a57c1259b6bcd14f4c3f69a", null ],
    [ "Initialize", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#a7594365830b3907d5e531a8339e399bd", null ],
    [ "SetCompressedMipmap", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#a643741c49dfa35a4caba08ecadc1ea6b", null ],
    [ "SetMipmap", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#a3ee93adcb10686ae8c84adbbed3f3726", null ],
    [ "Swap", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#a91efe750c9a0b83d284741dc52300e16", null ],
    [ "Depth", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#aa4d8adf1edc19c5035160ef8e5f60d49", null ],
    [ "Height", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#a1b52299db7ca00553d6389d08c354b1b", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#ac2c4b963cb326fb5151969ad3b6ca3f0", null ],
    [ "Width", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#afd19eb2d784712f18e9c202b41817874", null ]
];