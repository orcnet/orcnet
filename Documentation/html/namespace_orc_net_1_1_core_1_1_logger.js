var namespace_orc_net_1_1_core_1_1_logger =
[
    [ "AFileLogger", "class_orc_net_1_1_core_1_1_logger_1_1_a_file_logger.html", "class_orc_net_1_1_core_1_1_logger_1_1_a_file_logger" ],
    [ "ALogger", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger" ],
    [ "ConsoleLogger", "class_orc_net_1_1_core_1_1_logger_1_1_console_logger.html", "class_orc_net_1_1_core_1_1_logger_1_1_console_logger" ],
    [ "FileLogger", "class_orc_net_1_1_core_1_1_logger_1_1_file_logger.html", "class_orc_net_1_1_core_1_1_logger_1_1_file_logger" ],
    [ "HTMLLogger", "class_orc_net_1_1_core_1_1_logger_1_1_h_t_m_l_logger.html", "class_orc_net_1_1_core_1_1_logger_1_1_h_t_m_l_logger" ],
    [ "ILogger", "interface_orc_net_1_1_core_1_1_logger_1_1_i_logger.html", "interface_orc_net_1_1_core_1_1_logger_1_1_i_logger" ],
    [ "LogManager", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager" ],
    [ "LogMessage", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html", "class_orc_net_1_1_core_1_1_logger_1_1_log_message" ],
    [ "XMLLogger", "class_orc_net_1_1_core_1_1_logger_1_1_x_m_l_logger.html", "class_orc_net_1_1_core_1_1_logger_1_1_x_m_l_logger" ]
];