var class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d =
[
    [ "Texture2D", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#a67ab9d1e3526e6eb8f4cd0c0c79b6d4c", null ],
    [ "Initialize", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#aac1d5105a968a786ec043f1f035fbf4c", null ],
    [ "SetCompressedMipmap", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#a417f7fdcf726409d93705270e15991e2", null ],
    [ "SetMipmap", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#a0351489ff3d64da98170f26274e0e2cf", null ],
    [ "SetTexture", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#a0f960152683ffda03ef25c02be0b83b9", null ],
    [ "Swap", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#a1917520c93b04d3fe13077669b49f7a3", null ],
    [ "Height", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#a2dfbb8bba8dc64e25461ec1f335d8d56", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#ad8c0640defd298905deb2174a0e7714a", null ],
    [ "Width", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html#a0bbf805b2a523cb51e16668180b15dfb", null ]
];