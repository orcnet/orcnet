var interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service =
[
    [ "Close", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service.html#a427cd9511361dfa4ed6507ec38ac95dd", null ],
    [ "InvalidateResource", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service.html#a0b5ad985e79fb9174068ef5c2e8edd10", null ],
    [ "LoadResource", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service.html#af13053b4ed99b8d48fe5cb212d2376fa", null ],
    [ "LoadResource", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service.html#a8014c95c7c2e4394aad403d0b86647d3", null ],
    [ "ModifyLoader", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service.html#a8e5dab55f0e7d99553f6d5bc20dd804c", null ],
    [ "UpdateResources", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service.html#a4e46e5d36e7cdc682922b3a6c153ce63", null ],
    [ "Loader", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service.html#a7a7622effa004fd39e66425574cd7b4c", null ]
];