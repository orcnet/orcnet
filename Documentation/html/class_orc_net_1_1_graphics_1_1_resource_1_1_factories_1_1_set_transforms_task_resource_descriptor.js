var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_transforms_task_resource_descriptor =
[
    [ "SetTransformsTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_transforms_task_resource_descriptor.html#ae84a34ed46e3ee85f61a9bf5e118faa5", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_transforms_task_resource_descriptor.html#a579c2c293ad8d808a301342cffb5ae05", null ],
    [ "Factory", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_transforms_task_resource_descriptor.html#a4c5a4bf228842a8c903c75cd2246b3bf", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_transforms_task_resource_descriptor.html#aca9958e9fdb72cc01c378f8ff9b92e32", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_transforms_task_resource_descriptor.html#ad46067653c2b22d21db6d12a379f1d38", null ]
];