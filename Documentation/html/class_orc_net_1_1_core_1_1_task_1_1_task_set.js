var class_orc_net_1_1_core_1_1_task_1_1_task_set =
[
    [ "TaskSet", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#a8c070446e63aeccc5ad2d6df71af6d6c", null ],
    [ "AddTask", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#aceb98e804e1ecd6a995f3f32df71caa8", null ],
    [ "Execute", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#a7f39eaa04fb5d5972ef58a9ac6e2e0fd", null ],
    [ "OnDispose", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#af36704dbcc4da18910bec257ef837180", null ],
    [ "OnObservablePropertyChanged", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#acaaf42b5d1d8dea50fed49cffa8beabc", null ],
    [ "OnTaskCompletionDateChanged", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#a2c1828b85a2502bdc2fdf6d1e4b48b49", null ],
    [ "OnTaskStateChanged", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#a1d52025425ce253d1093b58f36d166b5", null ],
    [ "PostExecution", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#afd572cb583ab8374b20515f8040a0bf2", null ],
    [ "PreExecute", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#acaf50481f921b19b0b41ea40470a291a", null ],
    [ "Prepare", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#ac00dddea2255877a61c024db7a28e7a1", null ],
    [ "RemoveTask", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#a249497544a6cf8e2eefbbd2f35d3afb8", null ],
    [ "Complexity", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#a7a1853dd3299c072ab013ac424f3e4a0", null ],
    [ "IsEmpty", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#ab80c3fde7222f76c745d222c9d1f7939", null ],
    [ "LeafTasks", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#ae099d0e394ab5fbe88117b8d7c04b31d", null ],
    [ "ParentTasks", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#af341e8b64380a92b6f6720fc7b76fa14", null ],
    [ "PredecessorsCompletionDate", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#a988a5d21284cb83e92dd3fc15d970302", null ],
    [ "Tasks", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html#a706b194260b3b0780f01f3a97d5b4cec", null ]
];