var class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform =
[
    [ "TextureUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#a60d086d993a810171541b8b05fd51235", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#a35f43f3692cad887d81c0cd9e28025ba", null ],
    [ "SendUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#ab8532f52ccbb6344eab8aa0f163bef5f", null ],
    [ "mBlock", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#a067678ffd5cfb7e48429c5dad2291aa8", null ],
    [ "mLocation", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#abc9ae446576fdb695961008d459cab3c", null ],
    [ "mOwner", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#ac293623b49db235d6ef2afaa62e3c3cf", null ],
    [ "Block", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#a7d0feed8271ba3bcb97f061fb89bb6e6", null ],
    [ "Location", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#a45e804e443a24619ff59e0e4ecc74b61", null ],
    [ "Name", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#aba5af842ae8c78eb9cad9958462d3520", null ],
    [ "Owner", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#aeca049f972d6ff0bf573627c0b88327c", null ],
    [ "Sampler", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#ae9155e88bd84ede9cc5aeb293480bb9c", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#ae032351022b922956840ea52b6db8652", null ],
    [ "Type", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#a74b18498accae05acdea60c66164a478", null ],
    [ "UnitId", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#acf39c65351838a6fe7fa8ff59e838e58", null ],
    [ "UntypedValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#a4419021eb9540be769b36611dd8705c3", null ],
    [ "Value", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html#a0867c68b4a76827d756cacc2ee932970", null ]
];