var class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object =
[
    [ "DependencyObject", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#ababee69b8620ac077eacc26308d1d69a", null ],
    [ "AddPropertyObserver", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#af659671f6c41924ab17cd359eb5db4e6", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#ad9414d566f0cb13915927a11bc3f7cb2", null ],
    [ "InformPropertyChangedListener< T >", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#a242bc006975581c4c93a0897e6ad75af", null ],
    [ "NotifyPropertyChanged< T >", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#a2016a23b81864c7b1ef6ec60eddc2dd3", null ],
    [ "OnDispose", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#a080c5bcb43f3bb74a634fea775f24a63", null ],
    [ "OnPropertyChanged", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#a5560ab308071f29236e44050f5847364", null ],
    [ "RemovePropertyObserver", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#a04e24d586e2577aa29b5db205c98e5b5", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html#a841e453dd9150ae45f206db7254cb7bc", null ]
];