var class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer =
[
    [ "GPUBuffer", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#aa3ba62484e02da796b765b9b4b1f0495", null ],
    [ "Bind", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a6e7c7c68de20318d9c73560bba617bea", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a73a3d8243ad39886a6c85fe642bbcfc1", null ],
    [ "GetData", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a26f4486a6392b6b7fe397594b33af70b", null ],
    [ "GetSubData", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a42bf1849a072fabe8035d9b6fcc69173", null ],
    [ "GetSubData< TReturn >", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#afa82c3b600d1d126e780dc31bf56149f", null ],
    [ "Map", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a6a062df5d926cc04d939d0f47f4ff548", null ],
    [ "OnDispose", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a6e06c1082bacccc297d8a71fa09cd271", null ],
    [ "SetData", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#ab37795d8a782bc662cea8ee607081746", null ],
    [ "SetData", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a16d632a9bc759f84633be6ccd11d8569", null ],
    [ "SetSubData", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a253bbaaa78e056d87abd87cb28a14f7c", null ],
    [ "Unbind", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a789a52562a2741ca7c9b4425e63ae85c", null ],
    [ "Unmap", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#ad7cca63b37475aeb263f6753d626244c", null ],
    [ "BufferId", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#ab4121b4018b382b938e6f4ed421ed5de", null ],
    [ "BufferSize", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a69c46fcd13f1e04def31d01d079fa3f6", null ],
    [ "CurrentUniformUnit", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a573112dd39c2aee43b6ae2365f52bc4f", null ],
    [ "IsInvalid", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a89ce99f50e8072f9065cab605484ea7b", null ],
    [ "MappedData", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a3f199cf7ec4f1a67f8595872986a0bb7", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html#a14b667ee92ba63fe0324181e4b914595", null ]
];