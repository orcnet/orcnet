var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor =
[
    [ "Texture2DArrayDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html#a15244b1df1ad2af518f8b370047b79e1", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html#a09d974275fc6412e027a23b0b18e5362", null ],
    [ "OnDispose", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html#a4f414b93c500559609a6ca2d82a2377c", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html#ab0fd2062a7e5efc51247287f273f7177", null ],
    [ "Height", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html#ab0c4ce64016ac4bb4b1c85b9b6f43021", null ],
    [ "LayerCount", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html#a5395aee1d2b0a0dfc7d6dca654617844", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html#ae58ffd7827b92192fe579767595083e2", null ],
    [ "Width", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html#a9f699f9db40c8ee20a3b9c116af9b660", null ]
];