var namespace_orc_net_1_1_diagnostics =
[
    [ "Memory", "namespace_orc_net_1_1_diagnostics_1_1_memory.html", "namespace_orc_net_1_1_diagnostics_1_1_memory" ],
    [ "Task", "namespace_orc_net_1_1_diagnostics_1_1_task.html", "namespace_orc_net_1_1_diagnostics_1_1_task" ],
    [ "AProfiler", "class_orc_net_1_1_diagnostics_1_1_a_profiler.html", "class_orc_net_1_1_diagnostics_1_1_a_profiler" ],
    [ "IProfiler", "interface_orc_net_1_1_diagnostics_1_1_i_profiler.html", null ]
];