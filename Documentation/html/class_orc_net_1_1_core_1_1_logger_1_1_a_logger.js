var class_orc_net_1_1_core_1_1_logger_1_1_a_logger =
[
    [ "ALogger", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#a4c2ddb7adf0e54dd019057db047a914e", null ],
    [ "Dispose", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#a4a5d9a1ba810219fa400626586889ef7", null ],
    [ "Log", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#a6907bbd4b4ddfec6aa1ff2b3d971d049", null ],
    [ "OnDispose", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#a89138883e3b42b6822da198c91cb344b", null ],
    [ "mMaxType", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#a571b4f63bef9638a03523e46ab2b9ffc", null ],
    [ "mMinType", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#a539e75f3ea7eb98c07b19b5be65c4f68", null ],
    [ "MaxType", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#aceb4c67677ca773b4ae30ce49c02ed2d", null ],
    [ "MinType", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#a8efd42a016ab2200d1722fcbc90a180f", null ],
    [ "Type", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html#ad3b9569eeb9035bdb96be7a81a88b382", null ]
];