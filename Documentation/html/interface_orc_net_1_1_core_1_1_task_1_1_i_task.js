var interface_orc_net_1_1_core_1_1_task_1_1_i_task =
[
    [ "Execute", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html#aa94d6eb7b22f5e7e7d74ccee5fc555ce", null ],
    [ "Complexity", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html#aa8c80b2afbbef353d427f2d3637ed770", null ],
    [ "Context", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html#a1a95706806bc51a49c286bbefff3e1cf", null ],
    [ "DeadLine", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html#a82fb24d6abf7a765f0ca9b12d538def8", null ],
    [ "ExpectedDuration", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html#af9666a8a6deedfd7ce6efbabf7ebd3f7", null ],
    [ "Id", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html#a031ad7a606319b2fbbd2cfee78b93298", null ],
    [ "IsDone", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html#af22c3468f1441c09d2ef944cc79f14da", null ],
    [ "IsGPUTask", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html#ab846506aa618cd892f271e30a68deb47", null ]
];