var class_orc_net_1_1_core_1_1_resource_1_1_resource_manager =
[
    [ "ResourceManager", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#a96930b40385089ef533edc6ae7c3e058", null ],
    [ "Close", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#a69ea228339d4c58b20e71dc0fe055709", null ],
    [ "InvalidateResource", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#abbb71605268cf65ac5f2a0e4146e2100", null ],
    [ "LoadResource", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#ab2e5debd927a858d6b59aa49e4db21ca", null ],
    [ "LoadResource", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#a417edb860515264a01feb4dc7336f680", null ],
    [ "ModifyLoader", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#acb4832b9ffbc8bf531c09537ce5ea317", null ],
    [ "OnDispose", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#ad8dd606753e926c564a63aa6e8a6d8c3", null ],
    [ "UpdateResources", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#a9b56ca1d42db29058b581a2e717d3fee", null ],
    [ "IsCore", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#aa872c3aa52cae4d269338ad42409bacc", null ],
    [ "Loader", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#a41b7450010c3d9051ceb354b3c80ecfd", null ],
    [ "Name", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html#ae216c21d4a8d0a935f712fb09c0650fb", null ]
];