var namespace_orc_net_1_1_core_1_1_extensions =
[
    [ "AssemblyExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_assembly_extensions.html", "class_orc_net_1_1_core_1_1_extensions_1_1_assembly_extensions" ],
    [ "ByteArrayExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_byte_array_extensions.html", "class_orc_net_1_1_core_1_1_extensions_1_1_byte_array_extensions" ],
    [ "DateTimeExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_date_time_extensions.html", "class_orc_net_1_1_core_1_1_extensions_1_1_date_time_extensions" ],
    [ "FileExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_file_extensions.html", "class_orc_net_1_1_core_1_1_extensions_1_1_file_extensions" ],
    [ "StringExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_string_extensions.html", "class_orc_net_1_1_core_1_1_extensions_1_1_string_extensions" ],
    [ "TypeExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_type_extensions.html", "class_orc_net_1_1_core_1_1_extensions_1_1_type_extensions" ],
    [ "XElementExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_x_element_extensions.html", "class_orc_net_1_1_core_1_1_extensions_1_1_x_element_extensions" ]
];