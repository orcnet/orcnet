var class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source =
[
    [ "PresentationSource", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source.html#aacfcf56f69abb9369e03fd9b6c60b363", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source.html#a34fd0d5d3e29fb0c5df4a1de228bfd09", null ],
    [ "OnRootChanged", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source.html#ad1073b1fa0432bb978043e60df4deae4", null ],
    [ "RootVisual", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source.html#a5795cf000e2a6d55b924f7cf5485c876", null ],
    [ "RootVisualInternal", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source.html#afdf11b3100c28f6505b3427d1dab2362", null ],
    [ "SizeToContentMode", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source.html#ad71e0905401a4eed337c6d728e08b89b", null ],
    [ "Source", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source.html#abc4c494de6c83d0ab6f5903a1c4af0dc", null ]
];