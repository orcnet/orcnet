var class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback =
[
    [ "TransformFeedback", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html#a7f0c926b236eff7cec98a8f190464c58", null ],
    [ "Bind", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html#a4ce985eb9586ed887715db9663ce14d5", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html#ac6582ec6fe623e5ab0bb695282a2fade", null ],
    [ "RecordVertexVarying", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html#a062d2c27a18e1868d21cf6743f050010", null ],
    [ "RecordVertexVarying", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html#a94b275ec63738b68aa54c06e5b11f2d7", null ],
    [ "Reset", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html#a43a0add472137d61b7ac15bb5d5be347", null ],
    [ "Id", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html#a35c65b45d7833528f082299d3dbc4c6e", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html#ab0d72dee163241dfe695fb14225c7c54", null ]
];