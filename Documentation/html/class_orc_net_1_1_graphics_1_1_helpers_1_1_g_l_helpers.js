var class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers =
[
    [ "Enable", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a29b3945a9332e1042737110ae34cc0ad", null ],
    [ "GetBufferIndex", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a0af5f0cea8c16567a04f34aeddf5cbac", null ],
    [ "GetError", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a04f60e19c7a0e5d6c8e3e854bc1ca904", null ],
    [ "GetFormatSize", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a0cd2e6ef090f9c62be3dae30c772f772", null ],
    [ "GetMaxTextureUnits", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a0f8330d8abb508e33a1f1f6f8a14a432", null ],
    [ "GetTextureComponents", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a19e36243603c87528100b9e59aaa6b6f", null ],
    [ "GetUniformMaxBufferUnits", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a738aeaa87cf855cc6129e8d2319d0415", null ],
    [ "IsStageCompilationSucceeded", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a43bf49fcc779c6f558898d19dae81dde", null ],
    [ "LogStageCompilationFeedbacks", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#ac3c0aa3a4ae373402120d62425b76c62", null ],
    [ "MajorVersion", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a5e78a679444f729e980087e4c04c5027", null ],
    [ "MinorVersion", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#ae5b98b43fe08abc8a52f3adeea4bace4", null ],
    [ "cMAX_TEXTURE_UNITS", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html#a332278cc5bf41038d6db40c0966ecd1b", null ]
];