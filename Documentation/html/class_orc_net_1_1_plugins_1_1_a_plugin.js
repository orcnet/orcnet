var class_orc_net_1_1_plugins_1_1_a_plugin =
[
    [ "APlugin", "class_orc_net_1_1_plugins_1_1_a_plugin.html#abc5b804d8013950c226166634d348974", null ],
    [ "APlugin", "class_orc_net_1_1_plugins_1_1_a_plugin.html#ac8154671143dbfc6916476b3cb979d03", null ],
    [ "Dispose", "class_orc_net_1_1_plugins_1_1_a_plugin.html#accd6d960c28ae9dcc3c8d3a8308ae4c4", null ],
    [ "GetService< T >", "class_orc_net_1_1_plugins_1_1_a_plugin.html#a2fd3c6f601f2527efee64e26cb69d655", null ],
    [ "Initialize", "class_orc_net_1_1_plugins_1_1_a_plugin.html#a2e25855e4bc41e00413b413bb08eee86", null ],
    [ "OnDisposed", "class_orc_net_1_1_plugins_1_1_a_plugin.html#a1b3533db3e85762e511e356e325ed145", null ],
    [ "OnInitialize", "class_orc_net_1_1_plugins_1_1_a_plugin.html#ab008172ea73ff7f25d90cf6d099baad6", null ],
    [ "mServicesContrainers", "class_orc_net_1_1_plugins_1_1_a_plugin.html#a78bb98ab8fc8ff9c4f368ecc8533c9c0", null ],
    [ "AssemblyLocation", "class_orc_net_1_1_plugins_1_1_a_plugin.html#a363d9e98da7db4ee6eca80cf60e3db7b", null ],
    [ "PluginConfiguration", "class_orc_net_1_1_plugins_1_1_a_plugin.html#ae1e2c96a4de7217e87ceb2fcdbd0dec6", null ],
    [ "Services", "class_orc_net_1_1_plugins_1_1_a_plugin.html#aa0ce2eb74a889392ba4fdbed006c9975", null ]
];