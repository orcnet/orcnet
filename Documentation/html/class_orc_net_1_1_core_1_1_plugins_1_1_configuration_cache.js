var class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache =
[
    [ "ConfigurationCache", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache.html#af77d7e86859f1d668806529aa5bbb77d", null ],
    [ "AllowSlidingExpiration", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache.html#a8e5cc180c80a56fa2fb58e76604a1869", null ],
    [ "AutoReloadOnCacheExpiration", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache.html#ad8786eb59d94641d9b949ced8b86052d", null ],
    [ "CacheExpirationInterval", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache.html#a9a524ba5c5cde998fd747869d600bdc9", null ],
    [ "FilesystemWatcherDelay", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache.html#a0046d71ab6ac00403e1ba51932e69a49", null ],
    [ "Type", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache.html#aa6e2237da660fe77f2d43e208b2b66df", null ]
];