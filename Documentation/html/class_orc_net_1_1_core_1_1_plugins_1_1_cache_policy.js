var class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy =
[
    [ "CachePolicy", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#a18ab6d9bedbfc86cb1b987d171130f42", null ],
    [ "GetFileWatchCachePolicy", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#aa3e095be2b9727f7d5d902eff725ce04", null ],
    [ "GetHybridCachePolicy", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#aeb4e79860bc98f02d581220aa7f29d4a", null ],
    [ "GetTimeIntervalCachePolicy", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#ad03b44beb92cf80e1606dcc242a5caaa", null ],
    [ "AllowSlidingExpiration", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#af40939f2f4be11a1ff489f998f068c90", null ],
    [ "AutoReloadOnCacheExpiration", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#a2b84382112ea4aa458cf81a9811e934c", null ],
    [ "CacheExpirationInterval", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#a19cda74f02562aec51ad77974435b1e4", null ],
    [ "FilesystemWatcherDelay", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#ae53573c61f1c536b2bfea628f10cc303", null ],
    [ "PolicyType", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html#ac778c277dca7a454754ed18756cb054c", null ]
];