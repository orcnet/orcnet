var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor =
[
    [ "ATextureResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#ad89c40a74226ea4dac92f275ffada57e", null ],
    [ "ATextureResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#ad350d1455668261586a72bde800dce04", null ],
    [ "OnDispose", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#a9946e8e66ecbcd59c2f2072e8484586b", null ],
    [ "ReadParameters", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#a82ce3e0bb71d4648ec4cad48a956699e", null ],
    [ "ReadParameters", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#a41a808b6c9870d8991b64a5455941d3d", null ],
    [ "BufferParameters", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#a4dac59bd2115405f9681c66aa6212436", null ],
    [ "Format", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#a583545351c0a04fce28488fb60c024a7", null ],
    [ "InternalFormat", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#aa5c52de7056b53a8b390a1ba94019fb8", null ],
    [ "Parameters", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#a663f4b12568d33fb84e3ea2c1f17ffeb", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#a100af90d9c07a727a0de1b86e1e2a4fa", null ],
    [ "Type", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html#a7c9689f7568e9122dae56a926a216643", null ]
];