var interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers =
[
    [ "AddAttributeBuffer", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html#a30552e9887447694f954b98d87c4915f", null ],
    [ "AttributesPerVertex", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html#a199970bec5c48aeae0f570105d0c9c09", null ],
    [ "Bounds", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html#a16fc097c97bbb077cf6409ffcec2466c", null ],
    [ "IndexCount", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html#a6c7e918f7f2e2c1b939f58a3fa329ed3", null ],
    [ "IndicesBuffer", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html#a92a1f10fcc0fd7876eac4b4f000d70ff", null ],
    [ "this[uint pIndex]", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html#ac76ccdf6277df6c3d3449f7895bd331b", null ],
    [ "VertexCount", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html#af06b5df03f2b900f259b3a7cefd65cb3", null ]
];