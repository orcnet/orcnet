var namespace_orc_net =
[
    [ "Constants", "namespace_orc_net_1_1_constants.html", "namespace_orc_net_1_1_constants" ],
    [ "Core", "namespace_orc_net_1_1_core.html", "namespace_orc_net_1_1_core" ],
    [ "Diagnostics", "namespace_orc_net_1_1_diagnostics.html", "namespace_orc_net_1_1_diagnostics" ],
    [ "Graphics", "namespace_orc_net_1_1_graphics.html", "namespace_orc_net_1_1_graphics" ],
    [ "Plugins", "namespace_orc_net_1_1_plugins.html", "namespace_orc_net_1_1_plugins" ],
    [ "PluginTest", "namespace_orc_net_1_1_plugin_test.html", "namespace_orc_net_1_1_plugin_test" ],
    [ "UI", "namespace_orc_net_1_1_u_i.html", "namespace_orc_net_1_1_u_i" ]
];