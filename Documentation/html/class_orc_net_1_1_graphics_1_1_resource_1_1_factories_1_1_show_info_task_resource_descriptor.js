var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor =
[
    [ "ShowInfoTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#acc5f71fe9b5a0bf193fdfeae142e0ce9", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#a9fa0741256a9c571169d1e8041376b7d", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#af5aaa65e3b997c12e1983db4475c7e1f", null ],
    [ "FontHeight", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#a246a679272ce9e9e88f190bb7a725275", null ],
    [ "MaxLineCount", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#ad7cda791f839b025bd233d5ec8072721", null ],
    [ "ScreenX", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#adb22527e227d68ca635cea5043da08a3", null ],
    [ "ScreenY", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#af68942f6c43b710a253471e19cec34eb", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#a429deb2e55cf6916e5893e1fb814dd25", null ],
    [ "TextColor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#affdce586aea06a88df29c750a63924e9", null ],
    [ "TextFont", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#a77e6196f50e9c8425bcb5ae29e2fa059", null ],
    [ "TextPass", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html#a8ef99f2a5b8812d1141ad3e88f03402d", null ]
];