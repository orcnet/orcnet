var class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager =
[
    [ "Bind", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#a1e9d2dc14dba8a43a17c4a1b82e229d1", null ],
    [ "BindToAvailableUnit", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#a0003c9258c1c03668b540b9fbc84f976", null ],
    [ "BindToAvailableUnit", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#a15025b1fa5c206376e0f6a00b25e7c63", null ],
    [ "FindAvailableTextureUnit", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#aa89da7320366d27894c718f609dae521", null ],
    [ "Unbind", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#a528fb45c731446a96e4d78c979191197", null ],
    [ "Unbind", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#a132271abef751d3d5e7491f0e2cb7dc9", null ],
    [ "UnbindAll", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#a0eba7f71d38c6f04c445132dbcf52614", null ],
    [ "AnyUnitUsed", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#a75b32baf948e71eeb3ed4c0d18a4ce17", null ],
    [ "Instance", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html#abcbe9f0353e07b6ef692331262ccab07", null ]
];