var namespace_orc_net_1_1_core_1_1_plugins =
[
    [ "CachePolicy", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy" ],
    [ "ConfigurationCache", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache.html", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache" ],
    [ "IPlugin", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin.html", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin" ],
    [ "IPluginManager", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager" ],
    [ "IPluginService", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_service.html", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_service" ],
    [ "PluginConfiguration", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration.html", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration" ],
    [ "PluginEventArgs", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_event_args.html", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_event_args" ]
];