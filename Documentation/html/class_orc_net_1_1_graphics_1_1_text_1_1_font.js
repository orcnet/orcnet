var class_orc_net_1_1_graphics_1_1_text_1_1_font =
[
    [ "Font", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#af92350976a21bb68980b48a7435220f4", null ],
    [ "AddLine", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a33786657ee2d96ead23a14e16bbd1717", null ],
    [ "AddLineCentered", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a3c325d6af185042d0d495a0f472ce867", null ],
    [ "Dispose", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a70e7cd99570f07db3be0ba12284d4c38", null ],
    [ "GetSize", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a69705cf751f7b34f2e4e970dbbd6bc15", null ],
    [ "mCharWidths", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a571aeaeb138cc8517dc56a50b0c6d6fb", null ],
    [ "mColumnCount", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a20b23e85de42fae3984d165fe80af1a2", null ],
    [ "mFontText", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#ab05f961eb8a5c416862047f46ebebc89", null ],
    [ "mHasFixedSize", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a171f29702e0b512cf0b43510def1265e", null ],
    [ "mInvalidChar", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a8300e4764439512873bd87f4d5848041", null ],
    [ "mMaxValidChar", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a5e68808b5a44a60edeedb6a2885c3f26", null ],
    [ "mMinValidChar", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a0464a0df833798d87cd65ccefce5674f", null ],
    [ "mRowCount", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#ae89a4ad7f5f8a8911fae4f50a946042d", null ],
    [ "Creator", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a30a95fea168fd4af58e4212838564c30", null ],
    [ "Image", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#af1e940188c77c814e637528fbb1dedf6", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a6fbe91438cfe4427cd670104007ccd04", null ],
    [ "this[char pChar]", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a8bb041e7946a15cc71da128208619f6f", null ],
    [ "TileAspectRatio", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a42ffd65b5576cbfa9c6a530b9cbc2550", null ],
    [ "TileHeight", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a1009ef6cee5e5a5dc722c1697398792d", null ],
    [ "TileWidth", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html#a6030c32f19e9a7ed8ab34faca2a524d5", null ]
];