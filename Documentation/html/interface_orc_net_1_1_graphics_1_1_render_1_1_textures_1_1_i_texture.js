var interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture =
[
    [ "AddUser", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a0b52690a9075aa84c3a2e31a344184a0", null ],
    [ "GenerateMipmaps", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a0d83cda1768ae0aa474b37b0110e3d25", null ],
    [ "GetCompressedMipmap", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a0c0be131140dc64104c68ffcfdfc1abc", null ],
    [ "GetMipmap", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a0b4a3b9724e34f4b141a69aa4de696c8", null ],
    [ "GetMipmapCompressedSize", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a63b095eb7a2a16605a978ff0ffe2ae2d", null ],
    [ "IsUsedBy", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a8799fd8f090a410091a4177b72807fce", null ],
    [ "RemoveUser", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a909b20b41683cf5965684ea848c06ac3", null ],
    [ "Swap", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a237f00458936dbb95df08de28c34f7cc", null ],
    [ "ComponentCount", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a870467d8df570155d1b089277ba1f64b", null ],
    [ "Format", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#ae5792fec36f9a58d32fc007402646e04", null ],
    [ "HasMipmaps", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a6e93ea36cccde5ad3af07459a1e66149", null ],
    [ "InternalFormat", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a53d3cdb7fdc8ab55813d9049c1165f6e", null ],
    [ "IsCompressed", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#aef14038b443e968243af1d6e020e3719", null ],
    [ "TextureId", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#a294877295059686292124e6ffaa6cdcd", null ],
    [ "TextureType", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html#aed84c72b063fa677aea9af1d86cec33a", null ]
];