var hierarchy =
[
    [ "OrcNet.Core.Math.ABox2< double >", "class_orc_net_1_1_core_1_1_math_1_1_a_box2.html", [
      [ "OrcNet.Core.Math.Box2D", "class_orc_net_1_1_core_1_1_math_1_1_box2_d.html", null ]
    ] ],
    [ "OrcNet.Core.Math.ABox2< float >", "class_orc_net_1_1_core_1_1_math_1_1_a_box2.html", [
      [ "OrcNet.Core.Math.Box2F", "class_orc_net_1_1_core_1_1_math_1_1_box2_f.html", null ]
    ] ],
    [ "OrcNet.Core.Math.ABox2< Half >", "class_orc_net_1_1_core_1_1_math_1_1_a_box2.html", [
      [ "OrcNet.Core.Math.Box2H", "class_orc_net_1_1_core_1_1_math_1_1_box2_h.html", null ]
    ] ],
    [ "OrcNet.Core.Math.ABox2< int >", "class_orc_net_1_1_core_1_1_math_1_1_a_box2.html", [
      [ "OrcNet.Core.Math.Box2I", "class_orc_net_1_1_core_1_1_math_1_1_box2_i.html", null ]
    ] ],
    [ "OrcNet.Core.Math.ABox3< double >", "class_orc_net_1_1_core_1_1_math_1_1_a_box3.html", [
      [ "OrcNet.Core.Math.Box3D", "class_orc_net_1_1_core_1_1_math_1_1_box3_d.html", null ]
    ] ],
    [ "OrcNet.Core.Math.ABox3< float >", "class_orc_net_1_1_core_1_1_math_1_1_a_box3.html", [
      [ "OrcNet.Core.Math.Box3F", "class_orc_net_1_1_core_1_1_math_1_1_box3_f.html", null ]
    ] ],
    [ "OrcNet.Core.Math.ABox3< Half >", "class_orc_net_1_1_core_1_1_math_1_1_a_box3.html", [
      [ "OrcNet.Core.Math.Box3H", "class_orc_net_1_1_core_1_1_math_1_1_box3_h.html", null ]
    ] ],
    [ "OrcNet.Core.Math.ABox3< int >", "class_orc_net_1_1_core_1_1_math_1_1_a_box3.html", [
      [ "OrcNet.Core.Math.Box3I", "class_orc_net_1_1_core_1_1_math_1_1_box3_i.html", null ]
    ] ],
    [ "OrcNet.Graphics.UI.Core.AHitTestParameters", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_a_hit_test_parameters.html", [
      [ "OrcNet.Graphics.UI.Core.PointHitTestParameters", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_point_hit_test_parameters.html", null ]
    ] ],
    [ "OrcNet.Graphics.UI.Core.AHitTestResult", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_a_hit_test_result.html", [
      [ "OrcNet.Graphics.UI.Core.PointHitTestResult", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_point_hit_test_result.html", null ]
    ] ],
    [ "OrcNet.Graphics.UI.Core.AInputReport", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_a_input_report.html", null ],
    [ "OrcNet.Core.Math.AMatrix< float >", "class_orc_net_1_1_core_1_1_math_1_1_a_matrix.html", null ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix2Uniform< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix2_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Matrix2DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix2_d_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix2Uniform< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix2_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Matrix2FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix2_f_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix2Value< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix2_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2_d_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix2Value< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix2_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2_f_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix2Value< int >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix2_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix3Uniform< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix3_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Matrix3DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix3_d_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix3Uniform< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix3_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Matrix3FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix3_f_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix3Value< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix3_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3_d_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix3Value< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix3_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3_f_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix3Value< int >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix3_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix4Uniform< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix4_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Matrix4DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix4_d_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix4Uniform< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix4_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Matrix4FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix4_f_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix4Value< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix4_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4_d_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix4Value< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix4_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4_f_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix4Value< int >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix4_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixUniform< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Matrix2x3DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix2x3_d_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix2x4DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix2x4_d_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix3x2DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix3x2_d_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix3x4DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix3x4_d_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix4x2DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix4x2_d_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix4x3DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix4x3_d_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixUniform< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Matrix2x3FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix2x3_f_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix2x4FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix2x4_f_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix3x2FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix3x2_f_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix3x4FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix3x4_f_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix4x2FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix4x2_f_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Matrix4x3FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix4x3_f_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixValue< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2x3DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2x3_d_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2x4DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2x4_d_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3x2DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3x2_d_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3x4DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3x4_d_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4x2DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4x2_d_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4x3DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4x3_d_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixValue< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2x3FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2x3_f_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2x4FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2x4_f_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3x2FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3x2_f_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3x4FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3x4_f_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4x2FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4x2_f_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4x3FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4x3_f_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixValue< int >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2x3IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2x3_i_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix2x4IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix2x4_i_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3x2IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3x2_i_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix3x4IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix3x4_i_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4x2IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4x2_i_value.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.Matrix4x3IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_matrix4x3_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< bool >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.BoolUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_bool_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.DoubleUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_double_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.FloatUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_float_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< int >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.IntUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_int_uniform.html", null ]
    ] ],
    [ "APipelineUniform< Tuple< bool, bool >>", null, [
      [ "OrcNet.Graphics.Render.Uniforms.Vector2BUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector2_b_uniform.html", null ]
    ] ],
    [ "APipelineUniform< Tuple< bool, bool, bool >>", null, [
      [ "OrcNet.Graphics.Render.Uniforms.Vector3BUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector3_b_uniform.html", null ]
    ] ],
    [ "APipelineUniform< Tuple< bool, bool, bool, bool >>", null, [
      [ "OrcNet.Graphics.Render.Uniforms.Vector4BUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector4_b_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< uint >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.UIntUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_u_int_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector2D >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector2DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector2_d_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector2F >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector2FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector2_f_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector2I >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector2IUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector2_i_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector2UI >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector2UIUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector2_u_i_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector3D >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector3DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector3_d_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector3F >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector3FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector3_f_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector3I >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector3IUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector3_i_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector3UI >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector3UIUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector3_u_i_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector4D >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector4DUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector4_d_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector4F >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector4FUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector4_f_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector4I >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector4IUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector4_i_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< Vector4UI >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Vector4UIUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_vector4_u_i_uniform.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< bool >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.BoolValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_bool_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< double >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.DoubleValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_double_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< float >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.FloatValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_float_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< int >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.IntValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_int_value.html", null ]
    ] ],
    [ "APipelineValue< Tuple< bool, bool >>", null, [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector2BValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector2_b_value.html", null ]
    ] ],
    [ "APipelineValue< Tuple< bool, bool, bool >>", null, [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector3BValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector3_b_value.html", null ]
    ] ],
    [ "APipelineValue< Tuple< bool, bool, bool, bool >>", null, [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector4BValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector4_b_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< uint >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.UIntValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_u_int_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector2D >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector2DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector2_d_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector2F >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector2FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector2_f_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector2I >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector2IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector2_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector2UI >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector2UIValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector2_u_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector3D >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector3DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector3_d_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector3F >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector3FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector3_f_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector3I >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector3IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector3_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector3UI >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector3UIValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector3_u_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector4D >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector4DValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector4_d_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector4F >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector4FValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector4_f_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector4I >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector4IValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector4_i_value.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< Vector4UI >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", [
      [ "OrcNet.Graphics.Render.Uniforms.Values.Vector4UIValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector4_u_i_value.html", null ]
    ] ],
    [ "OrcNet.Core.Application", "class_orc_net_1_1_core_1_1_application.html", null ],
    [ "OrcNet.Core.Resource.AResource< DrawMeshTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.DrawMeshTaskResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_draw_mesh_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< Font >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.FontResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_font_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< LoopTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Core.Resource.LoopTaskResource", "class_orc_net_1_1_core_1_1_resource_1_1_loop_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< MeshBuffers >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.MeshBuffersResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_mesh_buffers_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< MethodTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Core.Resource.MethodTaskResource", "class_orc_net_1_1_core_1_1_resource_1_1_method_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< ParallelScheduler >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Core.Resource.SchedulerResource", "class_orc_net_1_1_core_1_1_resource_1_1_scheduler_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< PipelineDescription >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.PipelineDescriptionResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_pipeline_description_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< PipelinePass >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.PipelinePassResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_pipeline_pass_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< SceneNode >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.SceneNodeResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_scene_node_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< SequenceTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Core.Resource.SequenceTaskResource", "class_orc_net_1_1_core_1_1_resource_1_1_sequence_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< SetPassTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.SetPassTaskResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_set_pass_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< SetStateTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.SetStateTaskResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_set_state_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< SetTargetTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.SetTargetTaskResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_set_target_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< SetTransformsTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.SetTransformsTaskResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_set_transforms_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< ShowInfoTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.ShowInfoTaskResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_show_info_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< ShowLogTaskFactory >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.ShowLogTaskResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_show_log_task_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< Texture1D >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.Texture1DResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_texture1_d_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< Texture1DArray >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.Texture1DArrayResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_texture1_d_array_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< Texture2D >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.Texture2DResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_texture2_d_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< Texture2DArray >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.Texture2DArrayResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_texture2_d_array_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< Texture3D >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.Texture3DResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_texture3_d_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< TextureCube >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.TextureCubeResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_texture_cube_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< TextureCubeArray >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.TextureCubeArrayResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_texture_cube_array_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.AResource< TextureRectangle >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", [
      [ "OrcNet.Graphics.Resource.TextureRectangleResource", "class_orc_net_1_1_graphics_1_1_resource_1_1_texture_rectangle_resource.html", null ]
    ] ],
    [ "OrcNet.Core.Extensions.AssemblyExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_assembly_extensions.html", null ],
    [ "OrcNet.Graphics.Render.AttributeTypeExtensions", "class_orc_net_1_1_graphics_1_1_render_1_1_attribute_type_extensions.html", null ],
    [ "OrcNet.UI.Extensions.AvaloniaExtensions", "class_orc_net_1_1_u_i_1_1_extensions_1_1_avalonia_extensions.html", null ],
    [ "OrcNet.UI.AvaloniaToOrcNetHelpers", "class_orc_net_1_1_u_i_1_1_avalonia_to_orc_net_helpers.html", null ],
    [ "OrcNet.Core.Timer.BaseTimer", "class_orc_net_1_1_core_1_1_timer_1_1_base_timer.html", [
      [ "OrcNet.Graphics.GPUTimer", "class_orc_net_1_1_graphics_1_1_g_p_u_timer.html", null ]
    ] ],
    [ "OrcNet.Graphics.Render.BlendingDescription.BlendingAlphaDestinationFactorIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_blending_description_1_1_blending_alpha_destination_factor_index.html", null ],
    [ "OrcNet.Graphics.Render.BlendingDescription.BlendingAlphaEquationIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_blending_description_1_1_blending_alpha_equation_index.html", null ],
    [ "OrcNet.Graphics.Render.BlendingDescription.BlendingAlphaSourceFactorIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_blending_description_1_1_blending_alpha_source_factor_index.html", null ],
    [ "OrcNet.Graphics.Render.BlendingDescription.BlendingRGBDestinationFactorIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_blending_description_1_1_blending_r_g_b_destination_factor_index.html", null ],
    [ "OrcNet.Graphics.Render.BlendingDescription.BlendingRGBEquationIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_blending_description_1_1_blending_r_g_b_equation_index.html", null ],
    [ "OrcNet.Graphics.Render.BlendingDescription.BlendingRGBSourceFactorIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_blending_description_1_1_blending_r_g_b_source_factor_index.html", null ],
    [ "OrcNet.Graphics.Render.BlendingDescription.BlendingStateIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_blending_description_1_1_blending_state_index.html", null ],
    [ "OrcNet.Graphics.Render.BufferIdExtensions", "class_orc_net_1_1_graphics_1_1_render_1_1_buffer_id_extensions.html", null ],
    [ "OrcNet.Graphics.Render.BufferLayoutParameters", "class_orc_net_1_1_graphics_1_1_render_1_1_buffer_layout_parameters.html", null ],
    [ "OrcNet.Core.Extensions.ByteArrayExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_byte_array_extensions.html", null ],
    [ "OrcNet.Core.Plugins.CachePolicy", "class_orc_net_1_1_core_1_1_plugins_1_1_cache_policy.html", null ],
    [ "OrcNet.Graphics.Render.FrameBufferParameters.ColorMaskIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_frame_buffer_parameters_1_1_color_mask_index.html", null ],
    [ "OrcNet.Core.Math.ColumnIndex", "struct_orc_net_1_1_core_1_1_math_1_1_column_index.html", null ],
    [ "OrcNet.Core.Plugins.ConfigurationCache", "class_orc_net_1_1_core_1_1_plugins_1_1_configuration_cache.html", null ],
    [ "OrcNet.Constants.Constants", "class_orc_net_1_1_constants_1_1_constants.html", null ],
    [ "OrcNet.Graphics.UI.Render.DashStyles", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_dash_styles.html", null ],
    [ "OrcNet.Core.Extensions.DateTimeExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_date_time_extensions.html", null ],
    [ "OrcNet.Graphics.Render.FrameBufferParameters.DepthRangeIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_frame_buffer_parameters_1_1_depth_range_index.html", null ],
    [ "Dictionary< TKey, HashSet< TValue >>", null, [
      [ "OrcNet.Core.Datastructures.Multimap< TKey, TValue >", "class_orc_net_1_1_core_1_1_datastructures_1_1_multimap.html", null ]
    ] ],
    [ "EventArgs", null, [
      [ "OrcNet.Core.Plugins.PluginEventArgs", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_event_args.html", null ],
      [ "OrcNet.Graphics.UI.Core.AutomaticEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_automatic_event_args.html", null ],
      [ "OrcNet.Graphics.UI.Core.NotifyInputEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_notify_input_event_args.html", [
        [ "OrcNet.Graphics.UI.Core.ProcessInputEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_process_input_event_args.html", [
          [ "OrcNet.Graphics.UI.Core.PreProcessInputEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_pre_process_input_event_args.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Graphics.UI.Core.RoutedEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_routed_event_args.html", [
        [ "OrcNet.Graphics.UI.Core.InputEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_input_event_args.html", [
          [ "OrcNet.Graphics.UI.Core.KeyboardEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_keyboard_event_args.html", [
            [ "OrcNet.Graphics.UI.Core.KeyboardFocusChangedEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_keyboard_focus_changed_event_args.html", null ],
            [ "OrcNet.Graphics.UI.Core.KeyEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args.html", null ]
          ] ],
          [ "OrcNet.Graphics.UI.Core.MarkerInputEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_marker_input_event_args.html", null ],
          [ "OrcNet.Graphics.UI.Core.MouseEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_mouse_event_args.html", [
            [ "OrcNet.Graphics.UI.Core.MouseButtonEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_mouse_button_event_args.html", null ],
            [ "OrcNet.Graphics.UI.Core.MouseWheelEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_mouse_wheel_event_args.html", null ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "OrcNet.Graphics.UI.Core.EventRoute", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_event_route.html", null ],
    [ "Exception", null, [
      [ "OrcNet.Core.Task.TaskDependencyException", "class_orc_net_1_1_core_1_1_task_1_1_task_dependency_exception.html", null ]
    ] ],
    [ "OrcNet.Core.Extensions.FileExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_file_extensions.html", null ],
    [ "OrcNet.Graphics.Text.FontVertex", "struct_orc_net_1_1_graphics_1_1_text_1_1_font_vertex.html", null ],
    [ "OrcNet.Graphics.UI.Helpers.GeometryHelper", "class_orc_net_1_1_graphics_1_1_u_i_1_1_helpers_1_1_geometry_helper.html", null ],
    [ "OrcNet.Graphics.Helpers.GLHelpers", "class_orc_net_1_1_graphics_1_1_helpers_1_1_g_l_helpers.html", null ],
    [ "OrcNet.Core.Math.IBox< float >", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html", null ],
    [ "ICloneable", null, [
      [ "OrcNet.Core.Math.IBox< T >", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html", [
        [ "OrcNet.Core.Math.ABox< T >", "class_orc_net_1_1_core_1_1_math_1_1_a_box.html", [
          [ "OrcNet.Core.Math.ABox2< T >", "class_orc_net_1_1_core_1_1_math_1_1_a_box2.html", null ],
          [ "OrcNet.Core.Math.ABox3< T >", "class_orc_net_1_1_core_1_1_math_1_1_a_box3.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Core.Math.IMatrix< T >", "interface_orc_net_1_1_core_1_1_math_1_1_i_matrix.html", [
        [ "OrcNet.Core.Math.AMatrix< T >", "class_orc_net_1_1_core_1_1_math_1_1_a_matrix.html", [
          [ "OrcNet.Core.Math.Matrix2< T >", "class_orc_net_1_1_core_1_1_math_1_1_matrix2.html", null ],
          [ "OrcNet.Core.Math.Matrix3< T >", "class_orc_net_1_1_core_1_1_math_1_1_matrix3.html", null ],
          [ "OrcNet.Core.Math.Matrix4< T >", "class_orc_net_1_1_core_1_1_math_1_1_matrix4.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Core.Math.IVector< T >", "interface_orc_net_1_1_core_1_1_math_1_1_i_vector.html", [
        [ "OrcNet.Core.Math.AVector< T >", "class_orc_net_1_1_core_1_1_math_1_1_a_vector.html", [
          [ "OrcNet.Core.Math.Vector2< T >", "class_orc_net_1_1_core_1_1_math_1_1_vector2.html", null ],
          [ "OrcNet.Core.Math.Vector3< T >", "class_orc_net_1_1_core_1_1_math_1_1_vector3.html", null ],
          [ "OrcNet.Core.Math.Vector4< T >", "class_orc_net_1_1_core_1_1_math_1_1_vector4.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Graphics.Render.BlendingDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_blending_description.html", null ],
      [ "OrcNet.Graphics.Render.DepthDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_depth_description.html", null ],
      [ "OrcNet.Graphics.Render.DepthRange", "class_orc_net_1_1_graphics_1_1_render_1_1_depth_range.html", null ],
      [ "OrcNet.Graphics.Render.LineDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_line_description.html", null ],
      [ "OrcNet.Graphics.Render.PointDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_point_description.html", null ],
      [ "OrcNet.Graphics.Render.PolygonDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html", null ],
      [ "OrcNet.Graphics.Render.SamplingDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html", null ],
      [ "OrcNet.Graphics.Render.Scissor", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor.html", null ],
      [ "OrcNet.Graphics.Render.ScissorDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor_description.html", null ],
      [ "OrcNet.Graphics.Render.Viewport", "class_orc_net_1_1_graphics_1_1_render_1_1_viewport.html", null ],
      [ "OrcNet.Graphics.UI.Maths.ATransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_a_transform.html", [
        [ "OrcNet.Graphics.UI.Maths.MatrixTransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_matrix_transform.html", null ],
        [ "OrcNet.Graphics.UI.Maths.RotateTransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_rotate_transform.html", null ],
        [ "OrcNet.Graphics.UI.Maths.ScaleTransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_scale_transform.html", null ],
        [ "OrcNet.Graphics.UI.Maths.SkewTransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_skew_transform.html", null ],
        [ "OrcNet.Graphics.UI.Maths.TransformGroup", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_transform_group.html", null ],
        [ "OrcNet.Graphics.UI.Maths.TranslateTransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_translate_transform.html", null ]
      ] ]
    ] ],
    [ "IComparable", null, [
      [ "OrcNet.Core.SceneGraph.SceneNodeMethod", "class_orc_net_1_1_core_1_1_scene_graph_1_1_scene_node_method.html", null ],
      [ "OrcNet.Graphics.Render.Textures.SamplerParameters", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_sampler_parameters.html", [
        [ "OrcNet.Graphics.Render.Textures.TextureParameters", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_parameters.html", null ]
      ] ]
    ] ],
    [ "IComparer", null, [
      [ "OrcNet.Core.Task.TaskComparer", "class_orc_net_1_1_core_1_1_task_1_1_task_comparer.html", null ]
    ] ],
    [ "IDictionary", null, [
      [ "OrcNet.Graphics.UI.DataStructures.StyleResources", "class_orc_net_1_1_graphics_1_1_u_i_1_1_data_structures_1_1_style_resources.html", null ]
    ] ],
    [ "IDictionary< TKey, HashSet< TValue >>", null, [
      [ "OrcNet.Core.Datastructures.IMultimap< TKey, TValue >", "interface_orc_net_1_1_core_1_1_datastructures_1_1_i_multimap.html", [
        [ "OrcNet.Core.Datastructures.Multimap< TKey, TValue >", "class_orc_net_1_1_core_1_1_datastructures_1_1_multimap.html", null ]
      ] ]
    ] ],
    [ "IDisposable", null, [
      [ "OrcNet.Core.IObservable", "interface_orc_net_1_1_core_1_1_i_observable.html", [
        [ "OrcNet.Core.AObservable", "class_orc_net_1_1_core_1_1_a_observable.html", [
          [ "OrcNet.Core.Task.ATaskObservable", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html", [
            [ "OrcNet.Core.Task.ATask", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html", [
              [ "OrcNet.Core.Task.TaskSet", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html", null ],
              [ "OrcNet.Graphics.Task.DrawMeshTask", "class_orc_net_1_1_graphics_1_1_task_1_1_draw_mesh_task.html", null ],
              [ "OrcNet.Graphics.Task.SetPassTask", "class_orc_net_1_1_graphics_1_1_task_1_1_set_pass_task.html", null ],
              [ "OrcNet.Graphics.Task.SetStateTask", "class_orc_net_1_1_graphics_1_1_task_1_1_set_state_task.html", null ],
              [ "OrcNet.Graphics.Task.SetTargetTask", "class_orc_net_1_1_graphics_1_1_task_1_1_set_target_task.html", null ],
              [ "OrcNet.Graphics.Task.SetTransformsTask", "class_orc_net_1_1_graphics_1_1_task_1_1_set_transforms_task.html", null ],
              [ "OrcNet.Graphics.Task.ShowInfoTask", "class_orc_net_1_1_graphics_1_1_task_1_1_show_info_task.html", null ]
            ] ]
          ] ],
          [ "OrcNet.Graphics.Render.BlendingDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_blending_description.html", null ],
          [ "OrcNet.Graphics.Render.DepthDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_depth_description.html", null ],
          [ "OrcNet.Graphics.Render.FrameBufferParameters", "class_orc_net_1_1_graphics_1_1_render_1_1_frame_buffer_parameters.html", null ],
          [ "OrcNet.Graphics.Render.LineDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_line_description.html", null ],
          [ "OrcNet.Graphics.Render.OcclusionDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_occlusion_description.html", null ],
          [ "OrcNet.Graphics.Render.PointDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_point_description.html", null ],
          [ "OrcNet.Graphics.Render.PolygonDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_polygon_description.html", null ],
          [ "OrcNet.Graphics.Render.SamplingDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_sampling_description.html", null ],
          [ "OrcNet.Graphics.Render.ScissorDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_scissor_description.html", null ],
          [ "OrcNet.Graphics.Render.StencilDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html", null ],
          [ "OrcNet.Graphics.UI.Maths.AGeneralTransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_a_general_transform.html", [
            [ "OrcNet.Graphics.UI.Maths.ATransform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_a_transform.html", null ],
            [ "OrcNet.Graphics.UI.Maths.GeneralTransformGroup", "class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_general_transform_group.html", null ]
          ] ],
          [ "OrcNet.Graphics.UI.Render.ABrush", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_brush.html", [
            [ "OrcNet.Graphics.UI.Render.ATileBrush", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_tile_brush.html", [
              [ "OrcNet.Graphics.UI.Render.ImageBrush", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_image_brush.html", null ]
            ] ]
          ] ],
          [ "OrcNet.Graphics.UI.Render.AGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html", [
            [ "OrcNet.Graphics.UI.Render.EllipseGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_ellipse_geometry.html", null ],
            [ "OrcNet.Graphics.UI.Render.LineGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_line_geometry.html", null ],
            [ "OrcNet.Graphics.UI.Render.RectangleGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_rectangle_geometry.html", null ]
          ] ],
          [ "OrcNet.Graphics.UI.Render.DashStyle", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_dash_style.html", null ],
          [ "OrcNet.Graphics.UI.Render.Pen", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_pen.html", null ]
        ] ],
        [ "OrcNet.Core.Task.ITaskObservable", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_observable.html", [
          [ "OrcNet.Core.Task.ATaskObservable", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html", null ],
          [ "OrcNet.Core.Task.ITask", "interface_orc_net_1_1_core_1_1_task_1_1_i_task.html", [
            [ "OrcNet.Core.Task.ATask", "class_orc_net_1_1_core_1_1_task_1_1_a_task.html", null ]
          ] ]
        ] ],
        [ "OrcNet.Graphics.UI.Core.DependencyObject", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html", [
          [ "OrcNet.Graphics.UI.Core.AVisual", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_a_visual.html", [
            [ "OrcNet.Graphics.UI.Core.UIElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_u_i_element.html", [
              [ "OrcNet.Graphics.UI.Framework.FrameworkElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_framework_1_1_framework_element.html", null ]
            ] ]
          ] ],
          [ "OrcNet.Graphics.UI.Core.ContentElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_content_element.html", [
            [ "OrcNet.Graphics.UI.Framework.FrameworkContentElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_framework_1_1_framework_content_element.html", null ]
          ] ]
        ] ]
      ] ],
      [ "OrcNet.Core.IService", "interface_orc_net_1_1_core_1_1_i_service.html", [
        [ "OrcNet.Core.Image.IImageService", "interface_orc_net_1_1_core_1_1_image_1_1_i_image_service.html", [
          [ "OrcNet.Graphics.Image.ImageService", "class_orc_net_1_1_graphics_1_1_image_1_1_image_service.html", null ]
        ] ],
        [ "OrcNet.Core.IMemoryProfiler", "interface_orc_net_1_1_core_1_1_i_memory_profiler.html", [
          [ "OrcNet.Diagnostics.Memory.MemoryProfiler", "class_orc_net_1_1_diagnostics_1_1_memory_1_1_memory_profiler.html", null ]
        ] ],
        [ "OrcNet.Core.Plugins.IPluginManager", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html", null ],
        [ "OrcNet.Core.Plugins.IPluginService", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_service.html", [
          [ "OrcNet.Plugins.Services.APluginService", "class_orc_net_1_1_plugins_1_1_services_1_1_a_plugin_service.html", [
            [ "OrcNet.PluginTest.PluginServiceTest", "class_orc_net_1_1_plugin_test_1_1_plugin_service_test.html", null ]
          ] ]
        ] ],
        [ "OrcNet.Core.Resource.IResourceService", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_service.html", [
          [ "OrcNet.Core.Resource.ResourceManager", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html", null ]
        ] ],
        [ "OrcNet.Core.SceneGraph.ISceneService", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html", [
          [ "OrcNet.Graphics.Services.SceneManager", "class_orc_net_1_1_graphics_1_1_services_1_1_scene_manager.html", null ]
        ] ],
        [ "OrcNet.Core.Service.AService", "class_orc_net_1_1_core_1_1_service_1_1_a_service.html", [
          [ "OrcNet.Core.Resource.ResourceManager", "class_orc_net_1_1_core_1_1_resource_1_1_resource_manager.html", null ],
          [ "OrcNet.Diagnostics.AProfiler", "class_orc_net_1_1_diagnostics_1_1_a_profiler.html", [
            [ "OrcNet.Diagnostics.Memory.MemoryProfiler", "class_orc_net_1_1_diagnostics_1_1_memory_1_1_memory_profiler.html", null ],
            [ "OrcNet.Diagnostics.Task.TaskProfiler", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html", null ]
          ] ],
          [ "OrcNet.Graphics.Image.ImageService", "class_orc_net_1_1_graphics_1_1_image_1_1_image_service.html", null ],
          [ "OrcNet.Graphics.Services.RenderService", "class_orc_net_1_1_graphics_1_1_services_1_1_render_service.html", null ],
          [ "OrcNet.Graphics.Services.SceneManager", "class_orc_net_1_1_graphics_1_1_services_1_1_scene_manager.html", null ],
          [ "OrcNet.Graphics.UI.Services.UIService", "class_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_u_i_service.html", null ],
          [ "OrcNet.Plugins.Services.APluginService", "class_orc_net_1_1_plugins_1_1_services_1_1_a_plugin_service.html", null ]
        ] ],
        [ "OrcNet.Core.Task.ITaskProfiler", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html", [
          [ "OrcNet.Diagnostics.Task.TaskProfiler", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html", null ]
        ] ],
        [ "OrcNet.Graphics.Services.IRenderService", "interface_orc_net_1_1_graphics_1_1_services_1_1_i_render_service.html", [
          [ "OrcNet.Graphics.Services.RenderService", "class_orc_net_1_1_graphics_1_1_services_1_1_render_service.html", null ]
        ] ],
        [ "OrcNet.Graphics.UI.Services.IUIService", "interface_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_i_u_i_service.html", [
          [ "OrcNet.Graphics.UI.Services.UIService", "class_orc_net_1_1_graphics_1_1_u_i_1_1_services_1_1_u_i_service.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Core.Logger.ILogger", "interface_orc_net_1_1_core_1_1_logger_1_1_i_logger.html", [
        [ "OrcNet.Core.Logger.ALogger", "class_orc_net_1_1_core_1_1_logger_1_1_a_logger.html", [
          [ "OrcNet.Core.Logger.AFileLogger", "class_orc_net_1_1_core_1_1_logger_1_1_a_file_logger.html", [
            [ "OrcNet.Core.Logger.FileLogger", "class_orc_net_1_1_core_1_1_logger_1_1_file_logger.html", null ],
            [ "OrcNet.Core.Logger.HTMLLogger", "class_orc_net_1_1_core_1_1_logger_1_1_h_t_m_l_logger.html", null ],
            [ "OrcNet.Core.Logger.XMLLogger", "class_orc_net_1_1_core_1_1_logger_1_1_x_m_l_logger.html", null ]
          ] ],
          [ "OrcNet.Core.Logger.ConsoleLogger", "class_orc_net_1_1_core_1_1_logger_1_1_console_logger.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Core.Logger.LogManager", "class_orc_net_1_1_core_1_1_logger_1_1_log_manager.html", null ],
      [ "OrcNet.Core.Logger.LogMessage", "class_orc_net_1_1_core_1_1_logger_1_1_log_message.html", null ],
      [ "OrcNet.Core.Plugins.IPlugin", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin.html", [
        [ "OrcNet.Plugins.APlugin", "class_orc_net_1_1_plugins_1_1_a_plugin.html", [
          [ "OrcNet.PluginTest.PluginTest", "class_orc_net_1_1_plugin_test_1_1_plugin_test.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Core.Plugins.IPluginManager", "interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html", null ],
      [ "OrcNet.Core.Render.IFrameBuffer", "interface_orc_net_1_1_core_1_1_render_1_1_i_frame_buffer.html", [
        [ "OrcNet.Graphics.Render.FrameBuffer", "class_orc_net_1_1_graphics_1_1_render_1_1_frame_buffer.html", null ]
      ] ],
      [ "OrcNet.Core.Render.IMesh", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh.html", [
        [ "OrcNet.Core.Render.Generic.IMesh< V, I >", "interface_orc_net_1_1_core_1_1_render_1_1_generic_1_1_i_mesh.html", [
          [ "OrcNet.Graphics.Mesh.Mesh< V, I >", "class_orc_net_1_1_graphics_1_1_mesh_1_1_mesh.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Core.Render.IMeshBuffers", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html", [
        [ "OrcNet.Graphics.Mesh.MeshBuffers", "class_orc_net_1_1_graphics_1_1_mesh_1_1_mesh_buffers.html", null ]
      ] ],
      [ "OrcNet.Core.Render.IPipelineAttributeDescription", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html", [
        [ "OrcNet.Graphics.Render.PipelineAttributeDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_attribute_description.html", null ]
      ] ],
      [ "OrcNet.Core.Render.IPipelineDescription", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_description.html", [
        [ "OrcNet.Graphics.Render.PipelineDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_description.html", null ]
      ] ],
      [ "OrcNet.Core.Render.IPipelinePass", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_pass.html", [
        [ "OrcNet.Graphics.Render.PipelinePass", "class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_pass.html", null ]
      ] ],
      [ "OrcNet.Core.Resource.AResourceDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource_descriptor.html", [
        [ "OrcNet.Core.Resource.Factories.LoopTaskDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html", null ],
        [ "OrcNet.Core.Resource.Factories.MethodTaskDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_method_task_descriptor.html", null ],
        [ "OrcNet.Core.Resource.Factories.SchedulerDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_scheduler_descriptor.html", null ],
        [ "OrcNet.Core.Resource.Factories.SequenceTaskDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_sequence_task_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.ATextureResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_a_texture_resource_descriptor.html", [
          [ "OrcNet.Graphics.Resource.Factories.Texture1DArrayDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_array_descriptor.html", null ],
          [ "OrcNet.Graphics.Resource.Factories.Texture1DDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture1_d_descriptor.html", null ],
          [ "OrcNet.Graphics.Resource.Factories.Texture2DArrayDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_array_descriptor.html", null ],
          [ "OrcNet.Graphics.Resource.Factories.Texture2DDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html", null ],
          [ "OrcNet.Graphics.Resource.Factories.Texture3DDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture3_d_descriptor.html", null ],
          [ "OrcNet.Graphics.Resource.Factories.TextureCubeArrayDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_cube_array_descriptor.html", null ],
          [ "OrcNet.Graphics.Resource.Factories.TextureCubeDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_cube_descriptor.html", null ],
          [ "OrcNet.Graphics.Resource.Factories.TextureRectangleDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor.html", null ]
        ] ],
        [ "OrcNet.Graphics.Resource.Factories.DrawMeshTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_draw_mesh_task_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.FontResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_font_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.MeshBuffersResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.PipelineDescriptionDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_pipeline_description_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.PipelinePassResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_pipeline_pass_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.SceneNodeResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_scene_node_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.SetPassTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_pass_task_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.SetStateTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_state_task_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.SetTargetTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_target_task_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.SetTransformsTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_transforms_task_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.ShowInfoTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_info_task_resource_descriptor.html", null ],
        [ "OrcNet.Graphics.Resource.Factories.ShowLogTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_show_log_task_resource_descriptor.html", null ]
      ] ],
      [ "OrcNet.Core.Resource.IResource", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource.html", [
        [ "OrcNet.Core.Resource.AResource< TClass >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", null ]
      ] ],
      [ "OrcNet.Core.Resource.IResourceCreatable", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_creatable.html", [
        [ "OrcNet.Core.Render.IMeshBuffers", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html", null ],
        [ "OrcNet.Core.Render.IPipelineDescription", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_description.html", null ],
        [ "OrcNet.Core.Render.IPipelinePass", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_pass.html", null ],
        [ "OrcNet.Core.SceneGraph.ISceneNode", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_node.html", [
          [ "OrcNet.Graphics.SceneGraph.SceneNode", "class_orc_net_1_1_graphics_1_1_scene_graph_1_1_scene_node.html", null ]
        ] ],
        [ "OrcNet.Core.Task.IScheduler", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler.html", [
          [ "OrcNet.Core.Task.ParallelScheduler", "class_orc_net_1_1_core_1_1_task_1_1_parallel_scheduler.html", null ]
        ] ],
        [ "OrcNet.Core.Task.ITaskFactory", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_factory.html", [
          [ "OrcNet.Core.SceneGraph.ATaskFactory", "class_orc_net_1_1_core_1_1_scene_graph_1_1_a_task_factory.html", [
            [ "OrcNet.Core.SceneGraph.LoopTaskFactory", "class_orc_net_1_1_core_1_1_scene_graph_1_1_loop_task_factory.html", null ],
            [ "OrcNet.Core.SceneGraph.MethodTaskFactory", "class_orc_net_1_1_core_1_1_scene_graph_1_1_method_task_factory.html", null ],
            [ "OrcNet.Core.SceneGraph.SequenceTaskFactory", "class_orc_net_1_1_core_1_1_scene_graph_1_1_sequence_task_factory.html", null ],
            [ "OrcNet.Graphics.Task.Factories.DrawMeshTaskFactory", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_draw_mesh_task_factory.html", null ],
            [ "OrcNet.Graphics.Task.Factories.SetPassTaskFactory", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_pass_task_factory.html", null ],
            [ "OrcNet.Graphics.Task.Factories.SetStateTaskFactory", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_state_task_factory.html", null ],
            [ "OrcNet.Graphics.Task.Factories.SetTargetTaskFactory", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_target_task_factory.html", null ],
            [ "OrcNet.Graphics.Task.Factories.SetTransformsTaskFactory", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_transforms_task_factory.html", null ],
            [ "OrcNet.Graphics.Task.Factories.ShowInfoTaskFactory", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_show_info_task_factory.html", [
              [ "OrcNet.Graphics.Task.Factories.ShowLogTaskFactory", "class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_show_log_task_factory.html", null ]
            ] ]
          ] ]
        ] ],
        [ "OrcNet.Graphics.Render.Textures.ITexture", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html", [
          [ "OrcNet.Graphics.Render.Textures.ATexture", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_a_texture.html", [
            [ "OrcNet.Graphics.Render.Textures.Texture1D", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture1_d.html", null ],
            [ "OrcNet.Graphics.Render.Textures.Texture1DArray", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture1_d_array.html", null ],
            [ "OrcNet.Graphics.Render.Textures.Texture2D", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d.html", null ],
            [ "OrcNet.Graphics.Render.Textures.Texture2DArray", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d_array.html", null ],
            [ "OrcNet.Graphics.Render.Textures.Texture2DMultisample", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d_multisample.html", null ],
            [ "OrcNet.Graphics.Render.Textures.Texture2DMultisampleArray", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture2_d_multisample_array.html", null ],
            [ "OrcNet.Graphics.Render.Textures.Texture3D", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html", null ],
            [ "OrcNet.Graphics.Render.Textures.TextureBuffer", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_buffer.html", null ],
            [ "OrcNet.Graphics.Render.Textures.TextureCube", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_cube.html", null ],
            [ "OrcNet.Graphics.Render.Textures.TextureCubeArray", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_cube_array.html", null ],
            [ "OrcNet.Graphics.Render.Textures.TextureRectangle", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_rectangle.html", null ]
          ] ]
        ] ],
        [ "OrcNet.Graphics.Text.Font", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html", null ]
      ] ],
      [ "OrcNet.Core.Resource.IResourceLoader", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_loader.html", [
        [ "OrcNet.Core.Resource.Loaders.XMLResourceLoader", "class_orc_net_1_1_core_1_1_resource_1_1_loaders_1_1_x_m_l_resource_loader.html", [
          [ "OrcTest.Helpers.TestResourceLoader", "class_orc_test_1_1_helpers_1_1_test_resource_loader.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Core.SceneGraph.SceneNodeMethod", "class_orc_net_1_1_core_1_1_scene_graph_1_1_scene_node_method.html", null ],
      [ "OrcNet.Core.Task.IScheduler", "interface_orc_net_1_1_core_1_1_task_1_1_i_scheduler.html", null ],
      [ "OrcNet.Core.Task.ITaskContainer", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_container.html", [
        [ "OrcNet.Core.Task.TaskSet", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html", null ]
      ] ],
      [ "OrcNet.Core.Task.ITaskFactory", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_factory.html", null ],
      [ "OrcNet.Diagnostics.IProfiler", "interface_orc_net_1_1_diagnostics_1_1_i_profiler.html", [
        [ "OrcNet.Diagnostics.AProfiler", "class_orc_net_1_1_diagnostics_1_1_a_profiler.html", null ]
      ] ],
      [ "OrcNet.Graphics.GPUTimer", "class_orc_net_1_1_graphics_1_1_g_p_u_timer.html", null ],
      [ "OrcNet.Graphics.Render.GPUQuery", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html", null ],
      [ "OrcNet.Graphics.Render.IBuffer", "interface_orc_net_1_1_graphics_1_1_render_1_1_i_buffer.html", [
        [ "OrcNet.Graphics.Render.CPUBuffer", "class_orc_net_1_1_graphics_1_1_render_1_1_c_p_u_buffer.html", null ],
        [ "OrcNet.Graphics.Render.GPUBuffer", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html", null ]
      ] ],
      [ "OrcNet.Graphics.Render.PipelineStage", "class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_stage.html", null ],
      [ "OrcNet.Graphics.Render.RenderBuffer", "class_orc_net_1_1_graphics_1_1_render_1_1_render_buffer.html", null ],
      [ "OrcNet.Graphics.Render.StencilDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_stencil_description.html", null ],
      [ "OrcNet.Graphics.Render.Textures.ATexture", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_a_texture.html", null ],
      [ "OrcNet.Graphics.Render.Textures.ISampler", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_sampler.html", [
        [ "OrcNet.Graphics.Render.Textures.Sampler", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_sampler.html", null ]
      ] ],
      [ "OrcNet.Graphics.Render.Textures.ITexture", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html", null ],
      [ "OrcNet.Graphics.Render.TransformFeedback", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixUniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_uniform.html", [
        [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix2Uniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix2_uniform.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix3Uniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix3_uniform.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix4Uniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix4_uniform.html", null ]
      ] ],
      [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.SubroutineUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.TextureUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.Values.TextureValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_texture_value.html", null ],
      [ "OrcNet.Graphics.Task.IRunnable", "interface_orc_net_1_1_graphics_1_1_task_1_1_i_runnable.html", [
        [ "OrcNet.Graphics.Task.Runnables.ARunnable", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_a_runnable.html", [
          [ "OrcNet.Graphics.Task.Runnables.SetBlend", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_blend.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetBlendColor", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_blend_color.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetBuffers", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_buffers.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetClearColor", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_clear_color.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetClearDepth", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_clear_depth.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetClearState", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_clear_state.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetClearStencil", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_clear_stencil.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetClipDistances", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_clip_distances.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetColorMask", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_color_mask.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetDepthMask", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_depth_mask.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetDepthRange", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_depth_range.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetDepthTest", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_depth_test.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetDither", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_dither.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetFrontFaceMode", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_front_face_mode.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetHasPointLowerLeftOrigin", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_has_point_lower_left_origin.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetLineSmooth", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_line_smooth.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetLineWidth", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_line_width.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetLogicOp", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_logic_op.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetMultisample", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_multisample.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetOcclusionTest", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_occlusion_test.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetPointFadeThresholdSize", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_point_fade_threshold_size.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetPointSize", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_point_size.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetPolygonMode", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_polygon_mode.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetPolygonOffset", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_polygon_offset.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetPolygonOffsets", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_polygon_offsets.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetPolygonSmooth", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_polygon_smooth.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetSampleAlpha", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_sample_alpha.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetSampleCoverage", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_sample_coverage.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetSampleMask", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_sample_mask.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetSampleShading", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_sample_shading.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetScissorTest", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_scissor_test.html", [
            [ "OrcNet.Graphics.Task.Runnables.SetScissorTestValue", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_scissor_test_value.html", null ]
          ] ],
          [ "OrcNet.Graphics.Task.Runnables.SetStencilMask", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_stencil_mask.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetStencilTest", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_stencil_test.html", null ],
          [ "OrcNet.Graphics.Task.Runnables.SetViewport", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_set_viewport.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Graphics.UI.Core.PresentationSource", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_presentation_source.html", null ],
      [ "OrcNet.Graphics.UI.Dispatcher", "class_orc_net_1_1_graphics_1_1_u_i_1_1_dispatcher.html", null ],
      [ "OrcNet.Graphics.UI.Render.ADrawingContext", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_drawing_context.html", null ],
      [ "OrcNet.UI.OpenTKContext", "class_orc_net_1_1_u_i_1_1_open_t_k_context.html", null ]
    ] ],
    [ "IEnumerable< KeyValuePair< TKey, HashSet< TValue >>>", null, [
      [ "OrcNet.Core.Datastructures.IMultimap< TKey, TValue >", "interface_orc_net_1_1_core_1_1_datastructures_1_1_i_multimap.html", null ]
    ] ],
    [ "IEquatable", null, [
      [ "OrcNet.Core.Math.Half", "struct_orc_net_1_1_core_1_1_math_1_1_half.html", null ]
    ] ],
    [ "IEquatable< IBox< T >>", null, [
      [ "OrcNet.Core.Math.IBox< T >", "interface_orc_net_1_1_core_1_1_math_1_1_i_box.html", null ]
    ] ],
    [ "IEquatable< IMatrix< T >>", null, [
      [ "OrcNet.Core.Math.IMatrix< T >", "interface_orc_net_1_1_core_1_1_math_1_1_i_matrix.html", null ]
    ] ],
    [ "IEquatable< IVector< T >>", null, [
      [ "OrcNet.Core.Math.IVector< T >", "interface_orc_net_1_1_core_1_1_math_1_1_i_vector.html", null ]
    ] ],
    [ "IEquatable< MatrixCache< T >>", null, [
      [ "OrcNet.Core.Math.MatrixCache< T >", "class_orc_net_1_1_core_1_1_math_1_1_matrix_cache.html", null ]
    ] ],
    [ "OrcNet.Graphics.UI.Core.IInputElement", "interface_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_i_input_element.html", [
      [ "OrcNet.Graphics.UI.Core.ContentElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_content_element.html", null ],
      [ "OrcNet.Graphics.UI.Core.UIElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_u_i_element.html", null ],
      [ "OrcNet.Graphics.UI.Framework.IFrameworkInputElement", "interface_orc_net_1_1_graphics_1_1_u_i_1_1_framework_1_1_i_framework_input_element.html", [
        [ "OrcNet.Graphics.UI.Framework.FrameworkContentElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_framework_1_1_framework_content_element.html", null ],
        [ "OrcNet.Graphics.UI.Framework.FrameworkElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_framework_1_1_framework_element.html", null ]
      ] ]
    ] ],
    [ "OrcNet.Core.Math.IMatrix< float >", "interface_orc_net_1_1_core_1_1_math_1_1_i_matrix.html", null ],
    [ "OrcNet.Core.IMemoryProfilable", "interface_orc_net_1_1_core_1_1_i_memory_profilable.html", [
      [ "OrcNet.Core.AMemoryProfilable", "class_orc_net_1_1_core_1_1_a_memory_profilable.html", [
        [ "OrcNet.Core.Math.ABox< T >", "class_orc_net_1_1_core_1_1_math_1_1_a_box.html", null ],
        [ "OrcNet.Core.Math.AMatrix< T >", "class_orc_net_1_1_core_1_1_math_1_1_a_matrix.html", null ],
        [ "OrcNet.Core.Math.AVector< T >", "class_orc_net_1_1_core_1_1_math_1_1_a_vector.html", null ],
        [ "OrcNet.Core.Resource.AResource< TClass >", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource.html", null ],
        [ "OrcNet.Core.Resource.AResourceDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource_descriptor.html", null ],
        [ "OrcNet.Core.SceneGraph.ATaskFactory", "class_orc_net_1_1_core_1_1_scene_graph_1_1_a_task_factory.html", null ],
        [ "OrcNet.Core.SceneGraph.MethodQualifier", "class_orc_net_1_1_core_1_1_scene_graph_1_1_method_qualifier.html", [
          [ "OrcNet.Graphics.SceneGraph.MeshQualifier", "class_orc_net_1_1_graphics_1_1_scene_graph_1_1_mesh_qualifier.html", null ],
          [ "OrcNet.Graphics.SceneGraph.PipelineQualifier", "class_orc_net_1_1_graphics_1_1_scene_graph_1_1_pipeline_qualifier.html", null ],
          [ "OrcNet.Graphics.SceneGraph.ScreenQualifier", "class_orc_net_1_1_graphics_1_1_scene_graph_1_1_screen_qualifier.html", null ],
          [ "OrcNet.Graphics.SceneGraph.UniformQualifier", "class_orc_net_1_1_graphics_1_1_scene_graph_1_1_uniform_qualifier.html", null ]
        ] ],
        [ "OrcNet.Core.SceneGraph.SceneNodeMethod", "class_orc_net_1_1_core_1_1_scene_graph_1_1_scene_node_method.html", null ],
        [ "OrcNet.Core.Service.AService", "class_orc_net_1_1_core_1_1_service_1_1_a_service.html", null ],
        [ "OrcNet.Graphics.Mesh.Mesh< V, I >", "class_orc_net_1_1_graphics_1_1_mesh_1_1_mesh.html", null ],
        [ "OrcNet.Graphics.Mesh.MeshBuffers", "class_orc_net_1_1_graphics_1_1_mesh_1_1_mesh_buffers.html", null ],
        [ "OrcNet.Graphics.Render.GPUBuffer", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_buffer.html", null ],
        [ "OrcNet.Graphics.Render.GPUQuery", "class_orc_net_1_1_graphics_1_1_render_1_1_g_p_u_query.html", null ],
        [ "OrcNet.Graphics.Render.PipelineAttributeDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_attribute_description.html", null ],
        [ "OrcNet.Graphics.Render.PipelineDescription", "class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_description.html", null ],
        [ "OrcNet.Graphics.Render.PipelinePass", "class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_pass.html", null ],
        [ "OrcNet.Graphics.Render.PipelineStage", "class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_stage.html", null ],
        [ "OrcNet.Graphics.Render.Textures.ATexture", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_a_texture.html", null ],
        [ "OrcNet.Graphics.Render.Textures.Sampler", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_sampler.html", null ],
        [ "OrcNet.Graphics.Render.Textures.SamplerParameters", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_sampler_parameters.html", null ],
        [ "OrcNet.Graphics.Render.TransformFeedback", "class_orc_net_1_1_graphics_1_1_render_1_1_transform_feedback.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixUniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_uniform.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixValue< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_value.html", [
          [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix2Value< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix2_value.html", null ],
          [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix3Value< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix3_value.html", null ],
          [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrix4Value< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix4_value.html", null ]
        ] ],
        [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.SubroutineUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.TextureUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.UniformBlock", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_uniform_block.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.Values.SubroutineValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_subroutine_value.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.Values.TextureValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_texture_value.html", null ],
        [ "OrcNet.Graphics.SceneGraph.SceneNode", "class_orc_net_1_1_graphics_1_1_scene_graph_1_1_scene_node.html", null ],
        [ "OrcNet.Graphics.Task.Runnables.ARunnable", "class_orc_net_1_1_graphics_1_1_task_1_1_runnables_1_1_a_runnable.html", null ],
        [ "OrcNet.Graphics.Text.Font", "class_orc_net_1_1_graphics_1_1_text_1_1_font.html", null ],
        [ "OrcNet.Graphics.UI.Threading.ADispatcherObject", "class_orc_net_1_1_graphics_1_1_u_i_1_1_threading_1_1_a_dispatcher_object.html", [
          [ "OrcNet.Graphics.UI.Core.DependencyObject", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_dependency_object.html", null ],
          [ "OrcNet.Graphics.UI.Render.ADrawingContext", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_drawing_context.html", null ],
          [ "OrcNet.Graphics.UI.Style.Style", "class_orc_net_1_1_graphics_1_1_u_i_1_1_style_1_1_style.html", null ]
        ] ]
      ] ],
      [ "OrcNet.Core.Render.IMesh", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh.html", null ],
      [ "OrcNet.Core.Render.IMeshBuffers", "interface_orc_net_1_1_core_1_1_render_1_1_i_mesh_buffers.html", null ],
      [ "OrcNet.Core.Render.IPipelineAttributeDescription", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_attribute_description.html", null ],
      [ "OrcNet.Core.Render.IPipelineDescription", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_description.html", null ],
      [ "OrcNet.Core.Render.IPipelinePass", "interface_orc_net_1_1_core_1_1_render_1_1_i_pipeline_pass.html", null ],
      [ "OrcNet.Core.SceneGraph.ISceneNode", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_node.html", null ],
      [ "OrcNet.Core.Task.ITaskFactory", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_factory.html", null ],
      [ "OrcNet.Graphics.Render.Textures.ISampler", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_sampler.html", null ],
      [ "OrcNet.Graphics.Render.Textures.ITexture", "interface_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_i_texture.html", null ],
      [ "OrcNet.Graphics.Render.Uniforms.IPipelineUniform", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_uniform.html", [
        [ "OrcNet.Graphics.Render.Uniforms.Generics.IPipelineUniform< T >", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_i_pipeline_uniform.html", [
          [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineUniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_uniform.html", null ],
          [ "OrcNet.Graphics.Render.Uniforms.Generics.IPipelineMatrixUniform< T >", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_i_pipeline_matrix_uniform.html", [
            [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixUniform< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_uniform.html", null ]
          ] ]
        ] ],
        [ "OrcNet.Graphics.Render.Uniforms.SubroutineUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_subroutine_uniform.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.TextureUniform", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_texture_uniform.html", null ]
      ] ],
      [ "OrcNet.Graphics.Render.Uniforms.IPipelineValue", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_i_pipeline_value.html", [
        [ "OrcNet.Graphics.Render.Uniforms.Generics.IPipelineValue< T >", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_i_pipeline_value.html", [
          [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineValue< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_value.html", null ],
          [ "OrcNet.Graphics.Render.Uniforms.Generics.IPipelineMatrixValue< T >", "interface_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_i_pipeline_matrix_value.html", [
            [ "OrcNet.Graphics.Render.Uniforms.Generics.APipelineMatrixValue< T >", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_generics_1_1_a_pipeline_matrix_value.html", null ]
          ] ]
        ] ],
        [ "OrcNet.Graphics.Render.Uniforms.Values.SubroutineValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_subroutine_value.html", null ],
        [ "OrcNet.Graphics.Render.Uniforms.Values.TextureValue", "class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_texture_value.html", null ]
      ] ],
      [ "OrcNet.Graphics.SceneGraph.AttachmentTarget", "struct_orc_net_1_1_graphics_1_1_scene_graph_1_1_attachment_target.html", null ],
      [ "OrcNet.Graphics.Task.IRunnable", "interface_orc_net_1_1_graphics_1_1_task_1_1_i_runnable.html", null ]
    ] ],
    [ "OrcNet.Core.IModule", "interface_orc_net_1_1_core_1_1_i_module.html", [
      [ "OrcNet.Core.AModule", "class_orc_net_1_1_core_1_1_a_module.html", null ]
    ] ],
    [ "OrcNet.Core.Render.IndexIndex", "struct_orc_net_1_1_core_1_1_render_1_1_index_index.html", null ],
    [ "OrcNet.Graphics.UI.Managers.InputManager", "class_orc_net_1_1_graphics_1_1_u_i_1_1_managers_1_1_input_manager.html", null ],
    [ "OrcNet.Graphics.UI.Helpers.IntersectionHelper", "class_orc_net_1_1_graphics_1_1_u_i_1_1_helpers_1_1_intersection_helper.html", null ],
    [ "OrcNet.Core.IObserver", "interface_orc_net_1_1_core_1_1_i_observer.html", [
      [ "OrcNet.Core.Render.IFrameBuffer", "interface_orc_net_1_1_core_1_1_render_1_1_i_frame_buffer.html", null ],
      [ "OrcNet.Core.Task.ITaskObserver", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_observer.html", [
        [ "OrcNet.Core.Task.TaskSet", "class_orc_net_1_1_core_1_1_task_1_1_task_set.html", null ]
      ] ],
      [ "OrcNet.Graphics.Render.FrameBufferParameters", "class_orc_net_1_1_graphics_1_1_render_1_1_frame_buffer_parameters.html", null ]
    ] ],
    [ "IOException", null, [
      [ "Application.Plugins.Exceptions.PluginDirectoryNotFoundException", "class_application_1_1_plugins_1_1_exceptions_1_1_plugin_directory_not_found_exception.html", null ],
      [ "Application.Plugins.Exceptions.PluginFileNotFoundException", "class_application_1_1_plugins_1_1_exceptions_1_1_plugin_file_not_found_exception.html", null ],
      [ "Application.Plugins.Exceptions.PluginFileNotInPluginsFolderException", "class_application_1_1_plugins_1_1_exceptions_1_1_plugin_file_not_in_plugins_folder_exception.html", null ]
    ] ],
    [ "OrcNet.Core.Resource.IResourceFactory", "interface_orc_net_1_1_core_1_1_resource_1_1_i_resource_factory.html", [
      [ "OrcNet.Core.Resource.AResourceDescriptor", "class_orc_net_1_1_core_1_1_resource_1_1_a_resource_descriptor.html", null ]
    ] ],
    [ "OrcNet.Core.Math.IVector< float >", "interface_orc_net_1_1_core_1_1_math_1_1_i_vector.html", null ],
    [ "IWindowInfo", null, [
      [ "OrcNet.UI.AvaloniaWindowInfo", "class_orc_net_1_1_u_i_1_1_avalonia_window_info.html", null ]
    ] ],
    [ "KeyboardDevice", null, [
      [ "OrcNet.UI.Input.OpenTKKeyboardDevice", "class_orc_net_1_1_u_i_1_1_input_1_1_open_t_k_keyboard_device.html", null ]
    ] ],
    [ "OrcNet.Core.Math.MathUtility", "class_orc_net_1_1_core_1_1_math_1_1_math_utility.html", null ],
    [ "OrcNet.Graphics.UI.Maths.Matrix", "struct_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_matrix.html", null ],
    [ "OrcNet.Core.Math.Matrix2< double >", "class_orc_net_1_1_core_1_1_math_1_1_matrix2.html", [
      [ "OrcNet.Core.Math.Matrix2D", "class_orc_net_1_1_core_1_1_math_1_1_matrix2_d.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix2< float >", "class_orc_net_1_1_core_1_1_math_1_1_matrix2.html", [
      [ "OrcNet.Core.Math.Matrix2F", "class_orc_net_1_1_core_1_1_math_1_1_matrix2_f.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix2< Half >", "class_orc_net_1_1_core_1_1_math_1_1_matrix2.html", [
      [ "OrcNet.Core.Math.Matrix2H", "class_orc_net_1_1_core_1_1_math_1_1_matrix2_h.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix2< int >", "class_orc_net_1_1_core_1_1_math_1_1_matrix2.html", [
      [ "OrcNet.Core.Math.Matrix2I", "class_orc_net_1_1_core_1_1_math_1_1_matrix2_i.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix2< uint >", "class_orc_net_1_1_core_1_1_math_1_1_matrix2.html", [
      [ "OrcNet.Core.Math.Matrix2UI", "class_orc_net_1_1_core_1_1_math_1_1_matrix2_u_i.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix3< double >", "class_orc_net_1_1_core_1_1_math_1_1_matrix3.html", [
      [ "OrcNet.Core.Math.Matrix3D", "class_orc_net_1_1_core_1_1_math_1_1_matrix3_d.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix3< float >", "class_orc_net_1_1_core_1_1_math_1_1_matrix3.html", [
      [ "OrcNet.Core.Math.Matrix3F", "class_orc_net_1_1_core_1_1_math_1_1_matrix3_f.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix3< Half >", "class_orc_net_1_1_core_1_1_math_1_1_matrix3.html", [
      [ "OrcNet.Core.Math.Matrix3H", "class_orc_net_1_1_core_1_1_math_1_1_matrix3_h.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix3< int >", "class_orc_net_1_1_core_1_1_math_1_1_matrix3.html", [
      [ "OrcNet.Core.Math.Matrix3I", "class_orc_net_1_1_core_1_1_math_1_1_matrix3_i.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix4< double >", "class_orc_net_1_1_core_1_1_math_1_1_matrix4.html", [
      [ "OrcNet.Core.Math.Matrix4D", "class_orc_net_1_1_core_1_1_math_1_1_matrix4_d.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix4< float >", "class_orc_net_1_1_core_1_1_math_1_1_matrix4.html", [
      [ "OrcNet.Core.Math.Matrix4F", "class_orc_net_1_1_core_1_1_math_1_1_matrix4_f.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix4< Half >", "class_orc_net_1_1_core_1_1_math_1_1_matrix4.html", [
      [ "OrcNet.Core.Math.Matrix4H", "class_orc_net_1_1_core_1_1_math_1_1_matrix4_h.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Matrix4< int >", "class_orc_net_1_1_core_1_1_math_1_1_matrix4.html", [
      [ "OrcNet.Core.Math.Matrix4I", "class_orc_net_1_1_core_1_1_math_1_1_matrix4_i.html", null ]
    ] ],
    [ "OrcNet.Graphics.Math.Extensions.Matrix4dExtensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_matrix4d_extensions.html", null ],
    [ "OrcNet.Graphics.Math.Extensions.Matrix4Extensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_matrix4_extensions.html", null ],
    [ "OrcNet.Diagnostics.Memory.MemoryStatistics", "class_orc_net_1_1_diagnostics_1_1_memory_1_1_memory_statistics.html", null ],
    [ "MouseDevice", null, [
      [ "OrcNet.UI.Input.OpenTKMouseDevice", "class_orc_net_1_1_u_i_1_1_input_1_1_open_t_k_mouse_device.html", null ]
    ] ],
    [ "OrcNet.Core.Datastructures.Multimap< string, SceneNode >", "class_orc_net_1_1_core_1_1_datastructures_1_1_multimap.html", null ],
    [ "ObservableCollection", null, [
      [ "OrcNet.Graphics.UI.DataStructures.TransformCollection", "class_orc_net_1_1_graphics_1_1_u_i_1_1_data_structures_1_1_transform_collection.html", null ]
    ] ],
    [ "OrcNet.Core.Render.PipelineDescriptionIndex", "struct_orc_net_1_1_core_1_1_render_1_1_pipeline_description_index.html", null ],
    [ "OrcNet.Core.Plugins.PluginConfiguration", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration.html", null ],
    [ "OrcNet.Graphics.UI.Maths.Point", "struct_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_point.html", null ],
    [ "OrcNet.Core.PropertyChangedEventArgs", "struct_orc_net_1_1_core_1_1_property_changed_event_args.html", null ],
    [ "OrcNet.Graphics.Render.QueryModeExtensions", "class_orc_net_1_1_graphics_1_1_render_1_1_query_mode_extensions.html", null ],
    [ "OrcNet.Graphics.UI.Maths.Rect", "struct_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_rect.html", null ],
    [ "OrcNet.Graphics.UI.Core.RoutedEvent", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_routed_event.html", null ],
    [ "OrcNet.Core.Math.RowIndex", "struct_orc_net_1_1_core_1_1_math_1_1_row_index.html", null ],
    [ "OrcNet.Core.SceneGraph.SceneNodeIndex", "struct_orc_net_1_1_core_1_1_scene_graph_1_1_scene_node_index.html", null ],
    [ "OrcNet.Graphics.Render.ScissorDescription.ScissorIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_scissor_description_1_1_scissor_index.html", null ],
    [ "OrcNet.Graphics.Render.ScissorDescription.ScissorStateIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_scissor_description_1_1_scissor_state_index.html", null ],
    [ "OrcNet.Core.Service.ServiceManager", "class_orc_net_1_1_core_1_1_service_1_1_service_manager.html", null ],
    [ "OrcNet.Graphics.UI.Maths.Size", "struct_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_size.html", null ],
    [ "OrcNet.Graphics.UI.Core.SizeChangedArguments", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments.html", null ],
    [ "OrcNet.UI.Extensions.SizeExtensions", "class_orc_net_1_1_u_i_1_1_extensions_1_1_size_extensions.html", null ],
    [ "OrcNet.Graphics.Render.StageTypeExtensions", "class_orc_net_1_1_graphics_1_1_render_1_1_stage_type_extensions.html", null ],
    [ "OrcNet.Core.Extensions.StringExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_string_extensions.html", null ],
    [ "OrcNet.Diagnostics.Task.TaskStatistics", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_statistics.html", null ],
    [ "OrcTest.Helpers.Test", "class_orc_test_1_1_helpers_1_1_test.html", null ],
    [ "OrcTest.TestFrameBuffer", "class_orc_test_1_1_test_frame_buffer.html", null ],
    [ "OrcTest.Helpers.TestHelpers", "class_orc_test_1_1_helpers_1_1_test_helpers.html", null ],
    [ "OrcTest.TestResource", "class_orc_test_1_1_test_resource.html", null ],
    [ "OrcTest.Helpers.TestSuite", "class_orc_test_1_1_helpers_1_1_test_suite.html", null ],
    [ "OrcTest.TestTexture", "class_orc_test_1_1_test_texture.html", null ],
    [ "OrcTest.TestUniform", "class_orc_test_1_1_test_uniform.html", null ],
    [ "OrcTest.TestUniformBlock", "class_orc_test_1_1_test_uniform_block.html", null ],
    [ "OrcNet.Graphics.Render.Textures.TextureUnit", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit.html", null ],
    [ "OrcNet.Graphics.Render.Textures.TextureUnitManager", "class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture_unit_manager.html", null ],
    [ "OrcNet.Core.Thread.ThreadFactory", "class_orc_net_1_1_core_1_1_thread_1_1_thread_factory.html", null ],
    [ "OrcNet.Core.Extensions.TypeExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_type_extensions.html", null ],
    [ "OrcNet.Graphics.Render.UniformBlockName", "struct_orc_net_1_1_graphics_1_1_render_1_1_uniform_block_name.html", null ],
    [ "OrcNet.Graphics.Render.UniformName", "struct_orc_net_1_1_graphics_1_1_render_1_1_uniform_name.html", null ],
    [ "OrcNet.Core.Helpers.Utilities", "class_orc_net_1_1_core_1_1_helpers_1_1_utilities.html", null ],
    [ "OrcNet.Core.Math.Vector2< double >", "class_orc_net_1_1_core_1_1_math_1_1_vector2.html", [
      [ "OrcNet.Core.Math.Vector2D", "class_orc_net_1_1_core_1_1_math_1_1_vector2_d.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector2< float >", "class_orc_net_1_1_core_1_1_math_1_1_vector2.html", [
      [ "OrcNet.Core.Math.Vector2F", "class_orc_net_1_1_core_1_1_math_1_1_vector2_f.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector2< Half >", "class_orc_net_1_1_core_1_1_math_1_1_vector2.html", [
      [ "OrcNet.Core.Math.Vector2H", "class_orc_net_1_1_core_1_1_math_1_1_vector2_h.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector2< int >", "class_orc_net_1_1_core_1_1_math_1_1_vector2.html", [
      [ "OrcNet.Core.Math.Vector2I", "class_orc_net_1_1_core_1_1_math_1_1_vector2_i.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector2< uint >", "class_orc_net_1_1_core_1_1_math_1_1_vector2.html", [
      [ "OrcNet.Core.Math.Vector2UI", "class_orc_net_1_1_core_1_1_math_1_1_vector2_u_i.html", null ]
    ] ],
    [ "OrcNet.Graphics.Math.Extensions.Vector2dExtensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector2d_extensions.html", null ],
    [ "OrcNet.Graphics.Math.Extensions.Vector2Extensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector2_extensions.html", null ],
    [ "OrcNet.Core.Math.Vector3< double >", "class_orc_net_1_1_core_1_1_math_1_1_vector3.html", [
      [ "OrcNet.Core.Math.Vector3D", "class_orc_net_1_1_core_1_1_math_1_1_vector3_d.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector3< float >", "class_orc_net_1_1_core_1_1_math_1_1_vector3.html", [
      [ "OrcNet.Core.Math.Vector3F", "class_orc_net_1_1_core_1_1_math_1_1_vector3_f.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector3< Half >", "class_orc_net_1_1_core_1_1_math_1_1_vector3.html", [
      [ "OrcNet.Core.Math.Vector3H", "class_orc_net_1_1_core_1_1_math_1_1_vector3_h.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector3< int >", "class_orc_net_1_1_core_1_1_math_1_1_vector3.html", [
      [ "OrcNet.Core.Math.Vector3I", "class_orc_net_1_1_core_1_1_math_1_1_vector3_i.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector3< uint >", "class_orc_net_1_1_core_1_1_math_1_1_vector3.html", [
      [ "OrcNet.Core.Math.Vector3UI", "class_orc_net_1_1_core_1_1_math_1_1_vector3_u_i.html", null ]
    ] ],
    [ "OrcNet.Graphics.Math.Extensions.Vector3dExtensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector3d_extensions.html", null ],
    [ "OrcNet.Core.Math.Vector4< double >", "class_orc_net_1_1_core_1_1_math_1_1_vector4.html", [
      [ "OrcNet.Core.Math.Vector4D", "class_orc_net_1_1_core_1_1_math_1_1_vector4_d.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector4< float >", "class_orc_net_1_1_core_1_1_math_1_1_vector4.html", [
      [ "OrcNet.Core.Math.Vector4F", "class_orc_net_1_1_core_1_1_math_1_1_vector4_f.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector4< Half >", "class_orc_net_1_1_core_1_1_math_1_1_vector4.html", [
      [ "OrcNet.Core.Math.Vector4H", "class_orc_net_1_1_core_1_1_math_1_1_vector4_h.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector4< int >", "class_orc_net_1_1_core_1_1_math_1_1_vector4.html", [
      [ "OrcNet.Core.Math.Vector4I", "class_orc_net_1_1_core_1_1_math_1_1_vector4_i.html", null ]
    ] ],
    [ "OrcNet.Core.Math.Vector4< uint >", "class_orc_net_1_1_core_1_1_math_1_1_vector4.html", [
      [ "OrcNet.Core.Math.Vector4UI", "class_orc_net_1_1_core_1_1_math_1_1_vector4_u_i.html", null ]
    ] ],
    [ "OrcNet.Graphics.Math.Extensions.Vector4dExtensions", "class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_vector4d_extensions.html", null ],
    [ "OrcNet.Graphics.Render.Vertex2", "struct_orc_net_1_1_graphics_1_1_render_1_1_vertex2.html", null ],
    [ "OrcNet.Graphics.Render.Vertex3", "struct_orc_net_1_1_graphics_1_1_render_1_1_vertex3.html", null ],
    [ "OrcNet.Graphics.Render.Vertex4", "struct_orc_net_1_1_graphics_1_1_render_1_1_vertex4.html", null ],
    [ "OrcNet.Core.Render.VertexIndex", "struct_orc_net_1_1_core_1_1_render_1_1_vertex_index.html", null ],
    [ "OrcNet.Graphics.Render.FrameBufferParameters.ViewportIndex", "struct_orc_net_1_1_graphics_1_1_render_1_1_frame_buffer_parameters_1_1_viewport_index.html", null ],
    [ "OrcNet.Core.Extensions.XElementExtensions", "class_orc_net_1_1_core_1_1_extensions_1_1_x_element_extensions.html", null ]
];