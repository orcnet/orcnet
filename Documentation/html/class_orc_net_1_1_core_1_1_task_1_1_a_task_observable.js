var class_orc_net_1_1_core_1_1_task_1_1_a_task_observable =
[
    [ "ATaskObservable", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html#aabaf7500961929d7eb1bbb9b4868fbaf", null ],
    [ "AddTaskObserver", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html#a09d69a962211e08a891007051b15b785", null ],
    [ "NotifyCompletionDateChanged", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html#aaf121e763d22529b947a74fb9e6563f7", null ],
    [ "NotifyStateChanged", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html#af303143630e2c36d42624f4191729f13", null ],
    [ "OnDispose", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html#ae73e293c0147f7741e5705f2d1931996", null ],
    [ "RemoveTaskObserver", "class_orc_net_1_1_core_1_1_task_1_1_a_task_observable.html#a14b71697289005db787fc6fc97d0947c", null ]
];