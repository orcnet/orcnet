var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_target_task_resource_descriptor =
[
    [ "SetTargetTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_target_task_resource_descriptor.html#ac050c81d180ef9b274c444e936e46111", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_target_task_resource_descriptor.html#a60ba82ac7e6e5156d9260455089daa70", null ],
    [ "Factory", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_target_task_resource_descriptor.html#ad160c30224d8f93ac07d4c53d1ab289e", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_target_task_resource_descriptor.html#a0eeb0719fbc4b426b145c9007d83a92e", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_set_target_task_resource_descriptor.html#ada8a186c0f4cd54ae22de870d1fe0cf5", null ]
];