var struct_orc_net_1_1_core_1_1_property_changed_event_args =
[
    [ "PropertyChangedEventArgs", "struct_orc_net_1_1_core_1_1_property_changed_event_args.html#ad46ec4036b282dd6cf929a3337309438", null ],
    [ "NewValue", "struct_orc_net_1_1_core_1_1_property_changed_event_args.html#a90a2e197f2117c7e8be010913b9633d9", null ],
    [ "OldValue", "struct_orc_net_1_1_core_1_1_property_changed_event_args.html#aa703266b38489c5a4b72fe985563d6e1", null ],
    [ "PropertyName", "struct_orc_net_1_1_core_1_1_property_changed_event_args.html#a550f304f8107f6e60f9a740b497e80ea", null ]
];