var class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry =
[
    [ "AGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a185ce8f4dfd617a981725ccb897bdf79", null ],
    [ "Clone", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a57d0c55e7c517c84984fec1e7f7d49e9", null ],
    [ "FillContains", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a8cd33e536f59d2de783ad4bbc9c21702", null ],
    [ "FillContains", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#ad4f7405dac7576c5a12a75140c4d960a", null ],
    [ "FillContainsWithDetail", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#ade98d527d975417c9cb84847b45ed1ff", null ],
    [ "GetRenderBounds", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#aed544edf2f3bb8a422a0dd1bcc8223c6", null ],
    [ "IsEmpty", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a9608716824ba369c4976ecfd038077c7", null ],
    [ "MakeEmptyGeometry", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a4cda890391b38efaba97c994b2f1282e", null ],
    [ "MayHaveCurves", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a3bc9920eb827857a9c6db58a2292e761", null ],
    [ "StrokeContains", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#af0755f752c844de9a4115b1186cc0181", null ],
    [ "StrokeContainsWithDetail", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a329a700dcac0e9314bf5b5fad74fcd5b", null ],
    [ "Bounds", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a6f08c8bc0253614e861451bb38a6fc25", null ],
    [ "Empty", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a87cd9557dcff60541b0c92871f912c41", null ],
    [ "Transform", "class_orc_net_1_1_graphics_1_1_u_i_1_1_render_1_1_a_geometry.html#a6459be8860d9e2ad72177c994b0c5ded", null ]
];