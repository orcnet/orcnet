var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor =
[
    [ "Texture2DDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html#abfe85ba69e89c4bc639d3d2ad584e05f", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html#a5101244c358bd81f38eb86f3761b9838", null ],
    [ "OnDispose", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html#aedbc6e61626bba6a4e44354fa3f829fd", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html#abc2020c26207e59a305a6cf13c4e6d70", null ],
    [ "Height", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html#a35d0d774ecd0ce6b5396c6aa1175ce59", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html#a9c4e2484250c55a4152296c42033076c", null ],
    [ "Width", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html#a44bba82146fe5ed6d1df873d84b6fac5", null ]
];