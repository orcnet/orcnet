var class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args =
[
    [ "KeyEventArgs", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args.html#a530ca1c900568d39722d86af2c284055", null ],
    [ "Alt", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args.html#a32a3528ec47f29dd364fec3837e2ee85", null ],
    [ "Ctrl", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args.html#a18e12d8497918ee29ea76a4217cf574b", null ],
    [ "IsDown", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args.html#affb339473719170100e1065bbd277daa", null ],
    [ "IsUp", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args.html#a19192d58b09e60c5d19da017b5b5a55f", null ],
    [ "KeyData", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args.html#a5405340afee8105a170e57ce5130b4ef", null ],
    [ "Shift", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_key_event_args.html#ad2acfa58405d428140c15eaa0f498dde", null ]
];