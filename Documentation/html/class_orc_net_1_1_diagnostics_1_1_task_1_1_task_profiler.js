var class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler =
[
    [ "TaskProfiler", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#a93430b2cce2e03455a773f2a6ec5b9f0", null ],
    [ "DisplaysDebug", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#ab402e12fdfe6c47541e235710b7687ea", null ],
    [ "OnDispose", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#a69e4168990f47046b44914f818baca58", null ],
    [ "PrefetchStatistics", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#a5b9adc70d682ec620e7f377d958837d6", null ],
    [ "ProfileTask", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#ab1fc2e8c242b7cfb6bb8b1f641efa253", null ],
    [ "ResetStatistics", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#a6492340d8230c3c90a6f2d324eb76841", null ],
    [ "UpdateFrameStatistics", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#a4030c4b08c7f4fcf8360455a51f34529", null ],
    [ "UpdateGlobalStatistics", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#a5bf9308cf07dd973274c1f52d57d76e0", null ],
    [ "IsProfiling", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#a90063a09e1dbd8470c4e8d56fc489111", null ],
    [ "MonitoredTasks", "class_orc_net_1_1_diagnostics_1_1_task_1_1_task_profiler.html#a61964707ba30e7056a8e52302931cc1e", null ]
];