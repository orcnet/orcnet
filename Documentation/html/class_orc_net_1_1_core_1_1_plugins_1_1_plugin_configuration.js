var class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration =
[
    [ "PluginConfiguration", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration.html#a820a0de5ca4e8627a8d5951037cde171", null ],
    [ "Cache", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration.html#a2e25d55f1b1f824ba2684734b83fa9ab", null ],
    [ "CreatePluginsFolder", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration.html#a41bb9d558d2df36379cc32942e0f8208", null ],
    [ "KeepFileHandle", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration.html#a891a3ebe7716e1f1a8a4f38298df9e1d", null ],
    [ "PluginsFolder", "class_orc_net_1_1_core_1_1_plugins_1_1_plugin_configuration.html#a9bc1eec4aaeb8730d60652f9f60519d2", null ]
];