var class_orc_net_1_1_core_1_1_a_observable =
[
    [ "AObservable", "class_orc_net_1_1_core_1_1_a_observable.html#a461e1b7f1b327219aa36782790a81732", null ],
    [ "AddPropertyObserver", "class_orc_net_1_1_core_1_1_a_observable.html#af8e9c82d46dc292f031e626a5b53df08", null ],
    [ "Dispose", "class_orc_net_1_1_core_1_1_a_observable.html#a04f294ce1094f351ce9ae5d1f60d4061", null ],
    [ "InformPropertyChangedListener< T >", "class_orc_net_1_1_core_1_1_a_observable.html#a5a0e85efbe6edb4ac74a76c4763ffdb2", null ],
    [ "NotifyPropertyChanged< T >", "class_orc_net_1_1_core_1_1_a_observable.html#a8d744d83f7a072d1461586eef2d00afa", null ],
    [ "OnDispose", "class_orc_net_1_1_core_1_1_a_observable.html#aca1c286d782fa8fc9ac5b1813d867b13", null ],
    [ "RemovePropertyObserver", "class_orc_net_1_1_core_1_1_a_observable.html#aa89c71859cfca39d7cf1354de2cf5f59", null ]
];