var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor =
[
    [ "TextureRectangleDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor.html#a7c05cdf296053122b321325dee3a2bd8", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor.html#a2a476897cde0884dd569858d40612895", null ],
    [ "OnDispose", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor.html#a55790de8dcf04774688c7d8849558cd8", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor.html#a2a646a76dc06748243adc148f4834567", null ],
    [ "Height", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor.html#ae96980d405737a3f0fcc04484c3f7773", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor.html#ada170c23b70202e56866d060e509e8f0", null ],
    [ "Width", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture_rectangle_descriptor.html#ab548d568be20f27d519ee51575a31e52", null ]
];