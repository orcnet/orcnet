var class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments =
[
    [ "SizeChangedArguments", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments.html#af6b657f606b9af47ebf8b7a5423f315d", null ],
    [ "EditedElement", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments.html#a88121ddcacfbb0fbfd355710a008b805", null ],
    [ "IsHeightChanged", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments.html#a8f544c602f6e171790ee96fb61735022", null ],
    [ "IsWidthChanged", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments.html#a4ab43acf953874c7e07bc83d9ff5d95d", null ],
    [ "NewSize", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments.html#af512f8ff456c0abe691d385d0a27f8e3", null ],
    [ "Next", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments.html#ac9c47c0ad399a2da4effbbf33d6cd08f", null ],
    [ "PreviousSize", "class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_size_changed_arguments.html#a8380660acbaa340249965b517772d633", null ]
];