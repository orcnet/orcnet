var interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service =
[
    [ "GetNodes", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a856fd2a526c984fb50a2273f23a29baa", null ],
    [ "IsVisible", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a430383c24689710d6b3aab773033a186", null ],
    [ "Render", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a233eb32963f8f72b101fe2209008d7ae", null ],
    [ "SetNodeAsCamera", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a1708502988f704899c9da8fcc3185059", null ],
    [ "Update", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#ada67381e4bcad01ce8faa67f49c8595a", null ],
    [ "AbsoluteTime", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a90a777f73c8f858f3754ad3ad7d5b033", null ],
    [ "Camera", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a68185217cb537327dffaccc6e198772a", null ],
    [ "CameraMethod", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a64fc0d4a42b68ff748e14e6ff5d99d28", null ],
    [ "CurrentFrameBuffer", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a1dd3294aa6368fb03c3a1c071dd77422", null ],
    [ "CurrentPass", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a9fe3236ce6772867387112e3fcb499e5", null ],
    [ "ElapsedTime", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a0e128f264ad15491aebe3101fd421bac", null ],
    [ "FrameCount", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a488fe6ccce7512515436ce41eb315ae3", null ],
    [ "Root", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a35e1dca63a71a4a79c112f141c576a8f", null ],
    [ "Scheduler", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a4c7380f7627fda77ecd11bcea0d03dcb", null ],
    [ "this[string pName]", "interface_orc_net_1_1_core_1_1_scene_graph_1_1_i_scene_service.html#a6db649bdcba7101eacebefdd6a3ee4a6", null ]
];