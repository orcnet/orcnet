var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_draw_mesh_task_resource_descriptor =
[
    [ "DrawMeshTaskResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_draw_mesh_task_resource_descriptor.html#a333837541e028a0516e71850a8a471e2", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_draw_mesh_task_resource_descriptor.html#adf5d80dfee30f33bd672f6e3ea9a5116", null ],
    [ "DrawCount", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_draw_mesh_task_resource_descriptor.html#ac290b41dcbd5db12cc3d3e50c88f03f3", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_draw_mesh_task_resource_descriptor.html#aa54ad6709574b60d5e8295ce9212aafb", null ],
    [ "Qualifier", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_draw_mesh_task_resource_descriptor.html#af605bf0a8815ac05d57b634d09e4ac8b", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_draw_mesh_task_resource_descriptor.html#a04033d13df96efe394163267e39eb23a", null ]
];