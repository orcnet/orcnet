var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvwxyz",
  1: "abcdefghiklmnopqrstuvx",
  2: "ao",
  3: "abcdefghiklmnoprstuvx",
  4: "bcgiklmprs",
  5: "abcdfghilmpqstuv",
  6: "abcdefghiklmnoprstuvwx",
  7: "abcdefghijklmnopqrstuvwxyz",
  8: "agiklmp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Events"
};

