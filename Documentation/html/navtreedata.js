var NAVTREE =
[
  [ "OrcNet", "index.html", [
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Properties", "functions_prop.html", "functions_prop" ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"class_orc_net_1_1_core_1_1_math_1_1_a_vector.html#a30a53f343ee9143dded02c11b551f190",
"class_orc_net_1_1_core_1_1_math_1_1_matrix2_h.html#a6011f8fc4bbf8978be4e4af8c440aabf",
"class_orc_net_1_1_core_1_1_math_1_1_matrix4_h.html#ad8de071e030e3dd6272c5ef86896515b",
"class_orc_net_1_1_core_1_1_math_1_1_vector3_i.html#a06a442509441ea4d087fd3e802ad6366",
"class_orc_net_1_1_core_1_1_resource_1_1_factories_1_1_loop_task_descriptor.html#a2d9e5e13e7e641a50fb4b41bd215a526",
"class_orc_net_1_1_graphics_1_1_math_1_1_extensions_1_1_matrix4d_extensions.html#a3cb5ad7cc1d648929496f604eccc972f",
"class_orc_net_1_1_graphics_1_1_render_1_1_pipeline_attribute_description.html#a69e1571f3db161520b441676030e9d6b",
"class_orc_net_1_1_graphics_1_1_render_1_1_textures_1_1_texture3_d.html#a643741c49dfa35a4caba08ecadc1ea6b",
"class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_matrix4_d_uniform.html#a54fc31cd796ef81793c74446a0a3fe61",
"class_orc_net_1_1_graphics_1_1_render_1_1_uniforms_1_1_values_1_1_vector4_d_value.html",
"class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_texture2_d_descriptor.html#aedbc6e61626bba6a4e44354fa3f829fd",
"class_orc_net_1_1_graphics_1_1_task_1_1_factories_1_1_set_state_task_factory.html#a7d5e3b14972ae4b8f2f8d3c14e63dd68",
"class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_a_hit_test_result.html",
"class_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_u_i_element.html#a74303fa7c64722f766f67eaa3cbfa078",
"class_orc_net_1_1_graphics_1_1_u_i_1_1_maths_1_1_skew_transform.html#aa0f9ae145f6d9b8562f4dabcd9e669cf",
"class_orc_test_1_1_test_resource.html#a1da8374b9f57c3754ac805709e8f96fb",
"interface_orc_net_1_1_core_1_1_plugins_1_1_i_plugin_manager.html#a2279e6599a7375e5d2721ea49b7b3c51",
"interface_orc_net_1_1_graphics_1_1_u_i_1_1_core_1_1_i_input_element.html#af4e6438194de71a4107fa2c9d8de6232",
"struct_orc_net_1_1_graphics_1_1_scene_graph_1_1_attachment_target.html#a9b8a6996602d80b803a96c73ae1fa35a"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';