var interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler =
[
    [ "DisplaysDebug", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html#a7bf59ad9a9f41faf283e7bf28968a666", null ],
    [ "PrefetchStatistics", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html#a0f1384773ba8f78d246e9a1ca6302465", null ],
    [ "ProfileTask", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html#a323a7686546f11e6749ad5b19a4ffd06", null ],
    [ "ResetStatistics", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html#a17a3b9c3fcaf6cab7e696aa72a4cbfd0", null ],
    [ "UpdateFrameStatistics", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html#a7d95b8111b2b54bccd7984be7df866a2", null ],
    [ "UpdateGlobalStatistics", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html#aaeb782ff67616f7777d0c10a830c44a8", null ],
    [ "IsProfiling", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html#a1f89b0f19039b7c0925d5cb7498ec5fc", null ],
    [ "MonitoredTasks", "interface_orc_net_1_1_core_1_1_task_1_1_i_task_profiler.html#a7b292f3fcd33c5f0bdc3d0403dec75b7", null ]
];