var class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor =
[
    [ "MeshBuffersResourceDescriptor", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#ada052c3fea6c538c31ac2bd245e9bc06", null ],
    [ "Create", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#adfffb495c6158d8dd54f6f5c242ae5f2", null ],
    [ "Attributes", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#ae2ba92831b4f45befb2bbfed284c6249", null ],
    [ "Bounds", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#a50d42fa732af7350772bd272cd8a99c6", null ],
    [ "FactoryType", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#a5ead90d5c53039db0369f234c967a9a6", null ],
    [ "IndexCount", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#a2976c9ae9cf1be8162fb27c1f5d4a4b4", null ],
    [ "IndicesBuffer", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#afd5fab3f67ac907d6f4dcd568ba750a1", null ],
    [ "Mode", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#a2b8f1b397dba686a8238873ac62bcdc7", null ],
    [ "Size", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#ad4f6a25af25eadb57cc7b9aa1e2fadf6", null ],
    [ "VertexCount", "class_orc_net_1_1_graphics_1_1_resource_1_1_factories_1_1_mesh_buffers_resource_descriptor.html#adc1566e80292e5fab7981570b6ea275f", null ]
];