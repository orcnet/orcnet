﻿using OrcNet.Graphics.UI.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrcNet.Graphics.UI.Framework
{
    /// <summary>
    /// Framework content element class definition.
    /// </summary>
    public class FrameworkContentElement : ContentElement, IFrameworkInputElement
    {
        #region Properties

        /// <summary>
        /// Gets or sets the element name.
        /// </summary>
        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameworkContentElement"/> class.
        /// </summary>
        public FrameworkContentElement()
        {

        }

        #endregion Constructor

        #region Methods


        #endregion Methods
    }
}
