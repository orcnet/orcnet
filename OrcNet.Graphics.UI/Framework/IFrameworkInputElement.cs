﻿using OrcNet.Graphics.UI.Core;

namespace OrcNet.Graphics.UI.Framework
{
    /// <summary>
    /// Base framework input element interface definition.
    /// </summary>
    public interface IFrameworkInputElement : IInputElement
    {
        #region Properties

        /// <summary>
        /// Gets or sets the element name.
        /// </summary>
        string Name
        {
            get;
            set;
        }

        #endregion Properties
    }
}
