﻿using OrcNet.Graphics.UI.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrcNet.Graphics.UI.Framework
{
    /// <summary>
    /// Base framework element class definition.
    /// </summary>
    public class FrameworkElement : UIElement, IFrameworkInputElement
    {
        #region Fields

        /// <summary>
        /// Stores the static type of FrameworkElement.
        /// </summary>
        private static readonly Type sThisType = typeof(FrameworkElement);

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the element name.
        /// </summary>
        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameworkElement"/> class.
        /// </summary>
        public FrameworkElement()
        {

        }
        
        #endregion Constructor
    }
}
