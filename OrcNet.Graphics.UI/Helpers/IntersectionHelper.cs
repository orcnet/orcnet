﻿using OrcNet.Core.Math;
using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Helpers
{
    /// <summary>
    /// Intersection helper class definition.
    /// </summary>
    public static class IntersectionHelper
    {
        #region Methods

        /// <summary>
        /// Checks whether the given point lies on the line between the two provided start and end points.
        /// </summary>
        /// <param name="pToCheck">The point to check on the line.</param>
        /// <param name="pStartPoint">The segment start point.</param>
        /// <param name="pEndPoint">The segment end point.</param>
        /// <returns>True if lies on the line between the two points, false otherwise.</returns>
        public static bool IsPointInLine(Point pToCheck, Point pStartPoint, Point pEndPoint)
        {
            double lDxc = pToCheck.X - pStartPoint.X;
            double lDyc = pToCheck.Y - pStartPoint.Y;

            double lDxl = pEndPoint.X - pStartPoint.X;
            double lDyl = pEndPoint.Y - pStartPoint.Y;

            double lCross = lDxc * lDyl - lDyc * lDxl;

            // If not at least on the line.
            if ( lCross != 0 )
            {
                return false;
            }

            // On the line, but need to check if on the segment drawn by the endpoints.
            if ( System.Math.Abs( lDxl ) >= System.Math.Abs( lDyl ) )
            {
                return lDxl > 0 ? pStartPoint.X <= pToCheck.X && pToCheck.X <= pEndPoint.X :
                                  pEndPoint.X <= pToCheck.X && pToCheck.X <= pStartPoint.X;
            }
            else
            {
                return lDyl > 0 ? pStartPoint.Y <= pToCheck.Y && pToCheck.Y <= pEndPoint.Y :
                                  pEndPoint.Y <= pToCheck.Y && pToCheck.Y <= pStartPoint.Y;
            }
        }

        /// <summary>
        /// Checks whether the given lines intersect together.
        /// </summary>
        /// <param name="pStartPoint1">The first line start point.</param>
        /// <param name="pEndPoint1">The first line end point.</param>
        /// <param name="pStartPoint2">The second line start point.</param>
        /// <param name="pEndPoint2">The second line end point.</param>
        /// <returns>The intersection point if any, null otherwise.</returns>
        public static Point? IsIntersectingLine(Point pStartPoint1, Point pEndPoint1, Point pStartPoint2, Point pEndPoint2)
        {
            double lDiff = (pStartPoint1.X - pEndPoint1.X) * (pStartPoint2.Y - pEndPoint2.Y) - (pStartPoint1.Y - pEndPoint1.Y) * (pStartPoint2.X - pEndPoint2.X);
            if ( lDiff == 0 )
            {
                return null;
            }

            double lPointX = ((pStartPoint2.X - pEndPoint2.X) * (pStartPoint1.X * pEndPoint1.Y - pStartPoint1.Y * pEndPoint1.X) - (pStartPoint1.X - pEndPoint1.X) * (pStartPoint2.X * pEndPoint2.Y - pStartPoint2.Y * pEndPoint2.X)) / lDiff;
            double lPointY = ((pStartPoint2.Y - pEndPoint2.Y) * (pStartPoint1.X * pEndPoint1.Y - pStartPoint1.Y * pEndPoint1.X) - (pStartPoint1.Y - pEndPoint1.Y) * (pStartPoint2.X * pEndPoint2.Y - pStartPoint2.Y * pEndPoint2.X)) / lDiff;

            return new Point( lPointX, lPointY );
        }

        /// <summary>
        /// Checks whether the given point is inside the supplied polygon.
        /// </summary>
        /// <param name="pToCheck">The point to check.</param>
        /// <param name="pPolygon">The polygon regarding to which the point must be checked.</param>
        /// <param name="pIsCCW">The flag indicating whether the polygon ordering is counter clock wise or not.</param>
        /// <returns>True if the given point is in the polygon, false otherwise.</returns>
        public static bool IsPointInPolygon(Point pToCheck, Point[] pPolygon, bool pIsCCW = true)
        {
            Vector2D lToCheck = new Vector2D( pToCheck.X, pToCheck.Y );
            for ( int lOther = 2, lCurr = 0; lCurr < pPolygon.Length; lOther = lCurr, lCurr++ )
            {
                Point lFrom = pPolygon[ lOther ];
                Point lTo   = pPolygon[ lCurr ];
                Vector2D lPoint1 = new Vector2D( lTo.X, lTo.Y );
                Vector2D lPoint2 = new Vector2D( lFrom.X, lFrom.Y );
                Vector2D lDirection = (lPoint1 - lPoint2) as Vector2D;
                Vector2D lNormal = new Vector2D( -lDirection.Y, lDirection.X ); // Normal of the edge obeying to the right hand rule.
                AVector<double> lD = lToCheck - lPoint2;

                bool lResult;
                double lSign = lD.DotProduct( lNormal, out lResult );
                if ( pIsCCW )
                {
                    if ( lSign > 0.0 )
                    {
                        return false;
                    }
                }
                else
                {
                    if ( lSign < 0.0 )
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Checks whether the given line defined by the start and end points intersects the supplied
        /// polygon.
        /// </summary>
        /// <param name="pPolygon">The polygon intersection must be tested against.</param>
        /// <param name="pStartPoint">The line start point.</param>
        /// <param name="pEndPoint">The line end point.</param>
        /// <param name="pNearIntersection">The near intersection along the line. (Must be initially initialized at 0.0)</param>
        /// <param name="pFarIntersection">The far intersection along the line. (Must be initially initialized at 1.0)</param>
        /// <returns>True if the line drawn by the start and end points intersects the polygon, false otherwise.</returns>
        public static bool IsIntersectingPolygon(Point[] pPolygon, Point pStartPoint, Point pEndPoint, ref double pNearIntersection, ref double pFarIntersection)
        {
            if ( pPolygon == null )
            {
                return false;
            }

            int lPolygonPointCount = pPolygon.Length;
            Vector2D lStart = new Vector2D( pStartPoint.X, pStartPoint.Y );
            Vector2D lEnd   = new Vector2D( pEndPoint.X, pEndPoint.Y );
            AVector<double> lDirection = lEnd - lStart;

            // Test separation axes of the polygon.
            for ( int lSecond = lPolygonPointCount - 1, lFirst = 0; 
                      lFirst < lPolygonPointCount; 
                      lSecond = lFirst, lFirst++ )
            {
                Point lPoint1 = pPolygon[ lSecond ];
                Point lPoint2 = pPolygon[ lFirst ];
                Vector2D lE0 = new Vector2D( lPoint1.X, lPoint1.Y );
                Vector2D lE1 = new Vector2D( lPoint2.X, lPoint2.Y );
                Vector2D lE = (lE1 - lE0) as Vector2D;
                Vector2D lENormal = new Vector2D( lE.Y, -lE.X );
                AVector<double> lD = lE0 - lStart;
                bool lResult;
                double lDenominator = lD.DotProduct( lENormal, out lResult );
                double lNumerator   = lDirection.DotProduct( lENormal, out lResult );

                // Ray parallel to plane.
                if ( System.Math.Abs( lNumerator ) < 1.0e-8 )
                {
                    // Origin outside the plane, no intersection.
                    if ( lDenominator < 0.0 )
                    {
                        return false;
                    }
                }
                else
                {
                    double lTClip = lDenominator / lNumerator;

                    // Near intersection
                    if ( lNumerator < 0.0 )
                    {
                        if ( lTClip > pFarIntersection )
                        {
                            return false;
                        }

                        if ( lTClip > pNearIntersection )
                        {
                            pNearIntersection = lTClip;
                        }
                    }
                    // Far intersection
                    else
                    {
                        if ( lTClip < pNearIntersection )
                        {
                            return false;
                        }

                        if ( lTClip < pFarIntersection )
                        {
                            pFarIntersection = lTClip;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Checks whether the given polygons intersect together.
        /// </summary>
        /// <param name="pPolygon1">The first polygon.</param>
        /// <param name="pPolygon2">The second polygon.</param>
        /// <returns>True if the polygons intersect, false otherwise.</returns>
        public static bool IsIntersectingPolygon(Point[] pPolygon1, Point[] pPolygon2)
        {
            // TO DO: Optimize by pre computing the edges direction if passing Polygon objects instead of points array.
            int lPolygon1PointCount = pPolygon1.Length;
            for ( int J = lPolygon1PointCount - 1, I = 0;
                      I < lPolygon1PointCount; 
                      J = I, I++ )
            {
                Point lPoint1 = pPolygon1[ I ];
                Point lPoint2 = pPolygon1[ J ];
                Vector2D lTo   = new Vector2D( lPoint1.X, lPoint1.Y );
                Vector2D lFrom = new Vector2D( lPoint2.X, lPoint2.Y );

                Vector2D lE = (lTo - lFrom) as Vector2D;
                AVector<double> lNormal = new Vector2D( -lE.Y, lE.X );

                if ( AxisSeparatePolygons( ref lNormal, pPolygon1, pPolygon2 ) )
                {
                    return false;
                }
            }

            int lPolygon2PointCount = pPolygon2.Length;
            for ( int J = lPolygon2PointCount - 1, I = 0; 
                      I < lPolygon2PointCount; 
                      J = I, I++ )
            {
                Point lPoint1 = pPolygon2[ I ];
                Point lPoint2 = pPolygon2[ J ];
                Vector2D lTo   = new Vector2D( lPoint1.X, lPoint1.Y );
                Vector2D lFrom = new Vector2D( lPoint2.X, lPoint2.Y );
                
                Vector2D lE = (lTo - lFrom) as Vector2D;
                AVector<double> lNormal = new Vector2D( -lE.Y, lE.X );

                if ( AxisSeparatePolygons( ref lNormal, pPolygon1, pPolygon2 ) )
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks along an axis whether two polygons intersect with each other.
        /// </summary>
        /// <param name="pAxis">The axis. (is a ref as if the user wants do apply a response, it will have to store all axes after the AxisSeparatePolygons pass to find the MTD)</param>
        /// <param name="pPolygon1">The first polygon.</param>
        /// <param name="pPolygon2">The second polygon.</param>
        /// <returns>True if separated, false otherwise.</returns>
        private static bool AxisSeparatePolygons(ref AVector<double> pAxis, Point[] pPolygon1, Point[] pPolygon2)
        {
            double lMinPolygon1Interval = 0;
            double lMaxPolygon1Interval = 0;
            double lMinPolygon2Interval = 0;
            double lMaxPolygon2Interval = 0;

            CalculateInterval( pAxis, pPolygon1, ref lMinPolygon1Interval, ref lMaxPolygon1Interval );
            CalculateInterval( pAxis, pPolygon2, ref lMinPolygon2Interval, ref lMaxPolygon2Interval );

            if 
                ( lMinPolygon1Interval > lMaxPolygon2Interval || 
                  lMinPolygon2Interval > lMaxPolygon1Interval )
            {
                return true;
            }

            // Find the interval overlap 
            double d0 = lMaxPolygon1Interval - lMinPolygon2Interval;
            double d1 = lMaxPolygon2Interval - lMinPolygon1Interval;
            double lDepth = (d0 < d1) ? d0 : d1;

            // Convert the separation axis into a push vector (re-normalise 
            // the axis and multiply by interval overlap) 
            bool lResult;
            double lAxisLengthSquared = pAxis.DotProduct( pAxis, out lResult );

            pAxis *= lDepth / lAxisLengthSquared;

            return false;
        }

        /// <summary>
        /// Computes the intervals of the polygon along a given axis being the separation plane normal.
        /// </summary>
        /// <param name="pAxis">The axis.</param>
        /// <param name="pPolygon">The polygon.</param>
        /// <param name="pMin">The minimum interval.</param>
        /// <param name="pMax">The maximum interval.</param>
        private static void CalculateInterval(AVector<double> pAxis, Point[] pPolygon, ref double pMin, ref double pMax)
        {
            Point lFirst = pPolygon[ 0 ];
            Vector2D lPoint1 = new Vector2D( lFirst.X, lFirst.Y );
            bool lResult;
            double lDot = pAxis.DotProduct( lPoint1, out lResult );
            pMin = pMax = lDot;
            for ( int I = 0; I < pPolygon.Length; I++ )
            {
                Point lCurr = pPolygon[ I ];
                Vector2D lPoint = new Vector2D( lCurr.X, lCurr.Y );
                lDot = lPoint.DotProduct( pAxis, out lResult );
                if ( lDot < pMin )
                {
                    pMin = lDot;
                }
                else if ( lDot > pMax )
                {
                    pMax = lDot;
                }
            }
        }

        #endregion Methods
    }
}
