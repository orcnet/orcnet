﻿using OpenTK.Input;
using OrcNet.Core;
using OrcNet.Core.Service;
using OrcNet.Graphics.UI.Core;
using OrcNet.Graphics.UI.Managers;
using OrcNet.Graphics.UI.Maths;
using OrcNet.Graphics.UI.Services;
using System;

namespace OrcNet.Graphics.UI.Helpers
{
    /// <summary>
    /// Helper class for working with IInputElement objects.
    /// </summary>
    internal static class InputElement
    {
        #region Fields

        /// <summary>
        /// Stores the ContentElement's Type
        /// </summary>
        private static Type ContentElementType = typeof(ContentElement);

        /// <summary>
        /// Stores the UIElement's Type.
        /// </summary>
        private static Type UIElementType = typeof(UIElement);

        #endregion Fields

        #region Methods

        /// <summary>
        /// Return whether the InputElement is a valid type or not.
        /// </summary>
        /// <param name="pElement">The element to check.</param>
        /// <returns>True if valid, false otherwise.</returns>
        internal static bool IsValid(IInputElement pElement)
        {
            DependencyObject lCast = pElement as DependencyObject;
            return IsValid( lCast );
        }

        /// <summary>
        /// Checks whether the dependency object is a valid type or not
        /// </summary>
        /// <param name="pObject">The object to check.</param>
        /// <returns>True if valid, false otherwise.</returns>
        internal static bool IsValid(DependencyObject pObject)
        {
            return IsUIElement( pObject ) || 
                   IsContentElement( pObject );

        }

        /// <summary>
        /// Returns whether the given Object is a UIElement or not.
        /// </summary>
        /// <param name="pObject">The object to check.</param>
        /// <returns>True if a UI element, false otherwise.</returns>
        internal static bool IsUIElement(DependencyObject pObject)
        {
            return UIElementType.IsAssignableFrom( pObject.GetType() );
        }

        /// <summary>
        /// Returns whether the given Object is a ContentElement or not.
        /// </summary>
        /// <param name="pObject">The object to check.</param>
        /// <returns>True if a Content element, false otherwise.</returns>
        internal static bool IsContentElement(DependencyObject pObject)
        {
            return ContentElementType.IsAssignableFrom( pObject.GetType() );
        }

        /// <summary>
        /// Returns the containing input element of the given DynamicObject.
        /// </summary>
        /// <param name="pObject">The object to check.</param>
        /// <returns>The containing UI element.</returns>
        internal static DependencyObject GetContainingUIElement(DependencyObject pObject)
        {
            DependencyObject lContainer = null;
            AVisual lVisual;
            if
                ( pObject != null )
            {
                if 
                    ( IsUIElement( pObject ) )
                {
                    lContainer = pObject;
                }
                else if 
                    ( IsContentElement( pObject ) )
                {
                    ContentElement lCast = pObject as ContentElement;
                    DependencyObject lParent = lCast.Parent;
                    if
                        ( lParent != null )
                    {
                        lContainer = GetContainingUIElement( lParent );
                    }
                    else
                    {
                        lParent = lCast.CustomGetUIParent();
                        if
                            ( lParent != null )
                        {
                            lContainer = GetContainingUIElement( lParent );
                        }
                    }
                }
                else if 
                    ( (lVisual = pObject as AVisual) != null )
                {
                    DependencyObject lParent = lVisual.VisualParent;
                    if 
                        ( lParent != null )
                    {
                        lContainer = GetContainingUIElement( lParent );
                    }
                }
            }

            return lContainer;
        }

        /// <summary>
        /// Returns the containing input element of the given Object.
        /// </summary>
        /// <param name="pObject">The object to check.</param>
        /// <returns>The containing Input element.</returns>
        internal static IInputElement GetContainingInputElement(DependencyObject pObject)
        {
            IInputElement lContainer = null;
            AVisual lVisual;
            if 
                ( pObject != null )
            {
                if 
                    ( IsUIElement( pObject ) )
                {
                    lContainer = pObject as UIElement;
                }
                else if 
                    ( IsContentElement( pObject ) )
                {
                    lContainer = pObject as ContentElement;
                }
                else if 
                    ( (lVisual = pObject as AVisual) != null )
                {
                    DependencyObject lParent = lVisual.VisualParent;
                    if 
                        ( lParent != null )
                    {
                        lContainer = GetContainingInputElement( lParent );
                    }
                }
            }

            return lContainer;
        }

        /// <summary>
        /// Returns the containing visual of the given Object.
        /// </summary>
        /// <param name="pObject">The object to check.</param>
        /// <returns>The containing visual element.</returns>
        internal static DependencyObject GetContainingVisual(DependencyObject pObject)
        {
            DependencyObject lElement = null;
            if 
                ( pObject != null )
            {
                if 
                    ( IsUIElement( pObject ) )
                {
                    lElement = pObject as AVisual;
                }
                else if 
                    ( IsContentElement( pObject ) )
                {
                    ContentElement lCast = pObject as ContentElement;
                    DependencyObject lParent = lCast.Parent;
                    if 
                        ( lParent != null )
                    {
                        lElement = GetContainingVisual( lParent );
                    }
                    else
                    {
                        lParent = lCast.CustomGetUIParent();
                        if 
                            ( lParent != null )
                        {
                            lElement = GetContainingVisual( lParent );
                        }
                    }
                }
                else
                {
                    lElement = pObject as AVisual;
                }
            }

            return lElement;
        }

        /// <summary>
        /// Returns the root visual of the given Object.
        /// </summary>
        /// <param name="pObject">The object to check.</param>
        /// <returns>The root visual.</returns>
        internal static DependencyObject GetRootVisual(DependencyObject pObject)
        {
            AVisual lRootVisual = GetContainingVisual( pObject ) as AVisual;
            DependencyObject lParentVisual;
            while
                ( lRootVisual != null && 
                  ((lParentVisual = lRootVisual.VisualParent) != null))
            {
                lRootVisual = lParentVisual as AVisual;
            }

            return lRootVisual;
        }

        /// <summary>
        /// Translates the given point from an object to another.
        /// </summary>
        /// <param name="pPoint">The point to translate.</param>
        /// <param name="pFrom">The starting object.</param>
        /// <param name="pTo">The ending object.</param>
        /// <returns>The translated point.</returns>
        internal static Point TranslatePoint(Point pPoint, DependencyObject pFrom, DependencyObject pTo)
        {
            bool lUnused = false;
            return TranslatePoint( pPoint, pFrom, pTo, out lUnused );
        }

        /// <summary>
        /// Translates the given point from an object to another.
        /// </summary>
        /// <param name="pPoint">The point to translate.</param>
        /// <param name="pFrom">The starting object.</param>
        /// <param name="pTo">The ending object.</param>
        /// <param name="pTranslated"></param>
        /// <returns>The translated point.</returns>
        internal static Point TranslatePoint(Point pPoint, DependencyObject pFrom, DependencyObject pTo, out bool pTranslated)
        {
            pTranslated = false;

            Point lPointToTranslate = pPoint;

            // Get the containing and root visuals we are coming pFrom.
            DependencyObject lFromAsDO = InputElement.GetContainingVisual( pFrom );
            AVisual lRootFrom = InputElement.GetRootVisual( pFrom ) as AVisual;

            AVisual lVisualFrom = lFromAsDO as AVisual;
            if 
                ( lVisualFrom != null && 
                  lRootFrom != null )
            {
                AGeneralTransform lGeneralTransformUp;
                Matrix lMatrixUp;

                bool lIsUpSimple = false;
                lIsUpSimple = lVisualFrom.TrySimpleTransformToAncestor( lRootFrom,
                                                                        false, /* do not apply inverse */
                                                                        out lGeneralTransformUp,
                                                                        out lMatrixUp );
                if 
                    ( lIsUpSimple )
                {
                    lPointToTranslate = lMatrixUp.Transform( lPointToTranslate);
                }
                else if 
                    ( lGeneralTransformUp.TryTransform( lPointToTranslate, out lPointToTranslate ) == false )
                {
                    // Error.  Out parameter has been set false.
                    return new Point();
                }

                // If no element was specified pTo translate pTo, we leave the coordinates
                // pTranslated pTo the root.
                if 
                    ( pTo != null )
                {
                    // Get the containing and root visuals we are going pTo.
                    DependencyObject lVisualTo = InputElement.GetContainingVisual( pTo );
                    AVisual lRootTo = InputElement.GetRootVisual( pTo ) as AVisual;

                    if 
                        ( lVisualTo != null && 
                          lRootTo != null )
                    {
                        // If both are under the same root visual, we can easily translate the point
                        // between them by translating up pTo the root, and then back down.
                        //
                        // However, if both are under different roots, we can only translate
                        // between them if we know how pTo relate the two root visuals.  Currently
                        // we only know how pTo do that if both roots are sourced in HwndSources.
                        if 
                            ( lRootFrom != lRootTo )
                        {
                            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
                            PresentationSource lSource = lUIService.PresentationSource;

                            if 
                                ( lSource != null && 
                                  lSource.Source != null )
                            {
                                // Translate the point into client coordinates.
                                lPointToTranslate = PointUtil.RootToClient( lPointToTranslate, lSource );

                                // Translate the point into screen coordinates.
                                Point ptScreen    = PointUtil.ClientToScreen( lPointToTranslate, lSource );

                                // Translate the point back the the client coordinates of the To window.
                                lPointToTranslate = PointUtil.ScreenToClient( ptScreen, lSource );

                                // Translate the point back pTo the root element.
                                lPointToTranslate = PointUtil.ClientToRoot( lPointToTranslate, lSource );

                            }
                            else
                            {
                                // Error.  Out parameter has been set false.
                                return new Point();
                            }
                        }

                        // Translate the point pFrom the root pTo the visual.
                        AGeneralTransform lGeneralTransformDown;
                        Matrix lMatrixDown;

                        AVisual lVisualCast = lVisualTo as AVisual;

                        bool lIsDownSimple = lVisualCast.TrySimpleTransformToAncestor( lRootTo,
                                                                                      true, /* apply inverse */
                                                                                      out lGeneralTransformDown,
                                                                                      out lMatrixDown );

                        if 
                            ( lIsDownSimple )
                        {
                            lPointToTranslate = lMatrixDown.Transform( lPointToTranslate );
                        }
                        else if 
                            ( lGeneralTransformDown != null )
                        {
                            if 
                                ( lGeneralTransformDown.TryTransform( lPointToTranslate, out lPointToTranslate ) == false )
                            {
                                // Error.  Out parameter has been set false.
                                return new Point();
                            }
                        }
                        else
                        {
                            // Error.  Out parameter has been set false.
                            return new Point();
                        }
                    }
                    else
                    {
                        // Error.  Out parameter has been set false.
                        return new Point();
                    }
                }
            }
            else
            {
                // Error.  Out parameter has been set false.
                return new Point();
            }

            pTranslated = true;
            return lPointToTranslate;
        }

        /// <summary>
        /// Recursive is mouse over property changed propagated through the element hierarchy.
        /// </summary>
        /// <param name="pElement">The element that must raise the event.</param>
        /// <param name="pOldValue">The old mouse over state.</param>
        internal static void FireMouseOverPropertyChangedInAncestry(DependencyObject pElement, bool pOldValue)
        {
            bool lShouldNotify = false;
            UIElement lUIElement = pElement as UIElement;
            if
                ( lUIElement != null )
            {
                lShouldNotify = (pOldValue == false && lUIElement.IsMouseOver) || (pOldValue && lUIElement.IsMouseOver == false);
            }

            ContentElement lContentElement = pElement as ContentElement;
            if
                ( lContentElement != null )
            {
                lShouldNotify = (pOldValue == false && lContentElement.IsMouseOver) || (pOldValue && lContentElement.IsMouseOver == false);
            }

            if
                ( lShouldNotify )
            {
                Core.MouseEventArgs lArguments = new Core.MouseEventArgs( Mouse.GetState(), Environment.TickCount );
                lArguments.RoutedEvent = pOldValue ? InputManager.MouseLeaveEvent : InputManager.MouseEnterEvent;
                if
                    ( lUIElement != null )
                {
                    lUIElement.RaiseEvent( lArguments );
                }
                else if
                    ( lContentElement != null )
                {
                    lContentElement.RaiseEvent( lArguments, false );
                }
            }

            // Propagate up.
            DependencyObject lCoreParent    = GetInputElementParent( pElement );
            DependencyObject lLogicalParent = GetLogicalParent( pElement );
            if
                ( lCoreParent != null )
            {
                FireMouseOverPropertyChangedInAncestry( pElement, pOldValue );
            }

            if
                ( lLogicalParent != null && 
                  lCoreParent != lLogicalParent )
            {
                FireMouseOverPropertyChangedInAncestry( pElement, pOldValue );
            }
        }

        /// <summary>
        /// Recursive is mouse capture within property changed propagated through the element hierarchy.
        /// </summary>
        /// <param name="pElement">The element that must raise the event.</param>
        /// <param name="pIsOldValue">The flag indicating whether the element is the old one the mouse captured within or not.</param>
        internal static void FireMouseCaptureWithinPropertyChangedInAncestry(DependencyObject pElement, bool pIsOldValue)
        {
            PropertyChangedEventArgs lArguments = new PropertyChangedEventArgs( pIsOldValue, !pIsOldValue, "IsMouseCaptureWithin" );
            UIElement lUIElement = pElement as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.RaiseIsMouseCaptureWithinChanged( lArguments );
            }

            ContentElement lContentElement = pElement as ContentElement;
            if 
                ( lContentElement != null )
            {
                lContentElement.RaiseIsMouseCaptureWithinChanged( lArguments );
            }

            // Propagate up.
            DependencyObject lCoreParent    = GetInputElementParent( pElement );
            DependencyObject lLogicalParent = GetLogicalParent( pElement );
            if
                ( lCoreParent != null )
            {
                FireMouseCaptureWithinPropertyChangedInAncestry( pElement, pIsOldValue );
            }

            if
                ( lLogicalParent != null && 
                  lCoreParent != lLogicalParent )
            {
                FireMouseCaptureWithinPropertyChangedInAncestry( pElement, pIsOldValue );
            }
        }

        /// <summary>
        /// Gets the input element parent of the given element.
        /// </summary>
        /// <param name="pElement">The element to get the parent element of.</param>
        /// <returns>THe parent element.</returns>
        public static DependencyObject GetInputElementParent(DependencyObject pElement)
        {
            DependencyObject lParent = pElement;
            while 
                ( true )
            {
                lParent = GetCoreParent( lParent );

                if 
                    ( lParent == null || 
                      InputElement.IsValid( lParent ) )
                {
                    break;
                }
            }

            return lParent;
        }

        /// <summary>
        /// Gets the logical parent of the given element.
        /// </summary>
        /// <param name="pElement">The element to get the parent element of.</param>
        /// <returns>The parent element.</returns>
        public static DependencyObject GetLogicalParent(DependencyObject pElement)
        {
            DependencyObject lParent = null;
            UIElement lUIElement = pElement as UIElement;
            if
                ( lUIElement != null )
            {
                lParent = lUIElement.CustomGetUIParent();
            }

            ContentElement lContentElement = pElement as ContentElement;
            if 
                ( lContentElement != null )
            {
                lParent = lContentElement.CustomGetUIParent();
            }

            return lParent;
        }

        /// <summary>
        /// Gets the core parent of a given element.
        /// </summary>
        /// <param name="pElement">The element to get the parent for.</param>
        /// <returns>THe core parent element.</returns>
        public static DependencyObject GetCoreParent(DependencyObject pElement)
        {
            DependencyObject lParent = null;
            AVisual lVisual = pElement as AVisual;
            if 
                ( lVisual != null )
            {
                lParent = lVisual.VisualParent;
            }
            else
            {
                ContentElement lContentElement = pElement as ContentElement;
                if
                    ( lContentElement != null )
                {
                    lParent = lContentElement.Parent;
                }
            }

            return lParent;
        }

        #endregion Methods
    }
}
