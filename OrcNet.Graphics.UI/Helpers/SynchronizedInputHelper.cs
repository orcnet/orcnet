﻿using OpenTK.Input;
using OrcNet.Graphics.UI.Core;
using OrcNet.Graphics.UI.Managers;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.UI.Helpers
{
    /// <summary>
    /// Helper class definition managing synchronized input(s)
    /// </summary>
    internal static class SynchronizedInputHelper
    {
        #region Methods

        /// <summary>
        /// Gets the UI parent of the given object.
        /// </summary>
        /// <param name="pObject">The </param>
        /// <returns>The UI parent.</returns>
        internal static DependencyObject CustomGetUIParent(DependencyObject pObject)
        {
            UIElement lElement = pObject as UIElement;
            if 
                ( lElement != null )
            {
                return lElement.CustomGetUIParent();
            }
            else
            {
                ContentElement lContentElement = pObject as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    return lContentElement.CustomGetUIParent();
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the given event arguments is aiming
        /// a mapped event or not.
        /// </summary>
        /// <param name="pEventArgs">The event arguments</param>
        /// <returns>True if mapped event, false otherwise.</returns>
        internal static bool IsMappedEvent(RoutedEventArgs pEventArgs)
        {
            RoutedEvent lEvent = pEventArgs.RoutedEvent;

            return lEvent == InputManager.KeyUpEvent ||
                   lEvent == InputManager.KeyDownEvent ||
                   lEvent == InputManager.MouseDownEvent ||
                   lEvent == InputManager.MouseUpEvent;
        }

        /// <summary>
        /// Gets the given input type opposite input type.
        /// </summary>
        /// <param name="pInputType">The input type to look the opposite input type for.</param>
        /// <returns>The opposite input type.</returns>
        internal static SynchronizedInputType GetOppositeInputType(SynchronizedInputType pInputType)
        {
            SynchronizedInputType lOpposite = SynchronizedInputType.KeyDown;
            switch 
                ( pInputType )
            {
                case SynchronizedInputType.KeyDown:
                    lOpposite = SynchronizedInputType.KeyUp;
                    break;

                case SynchronizedInputType.KeyUp:
                    lOpposite = SynchronizedInputType.KeyDown;
                    break;

                case SynchronizedInputType.MouseLeftButtonDown:
                    lOpposite = SynchronizedInputType.MouseLeftButtonUp;
                    break;

                case SynchronizedInputType.MouseLeftButtonUp:
                    lOpposite = SynchronizedInputType.MouseLeftButtonDown;
                    break;

                case SynchronizedInputType.MouseRightButtonDown:
                    lOpposite = SynchronizedInputType.MouseRightButtonUp;
                    break;

                case SynchronizedInputType.MouseRightButtonUp:
                    lOpposite = SynchronizedInputType.MouseRightButtonDown;
                    break;
            }

            return lOpposite;
        }

        /// <summary>
        /// Check whether the InputManager is listening for the input event contained in the given arguments.
        /// </summary>
        /// <param name="pArgs">The routed event arguments.</param>
        /// <returns>True if listening the routed event input, false otherwise.</returns>
        internal static bool IsListening(RoutedEventArgs pArgs)
        {
            if 
                ( Array.IndexOf( InputManager.Instance.SynchronizedInputEvents, pArgs.RoutedEvent ) >= 0 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the given element is listening for 
        /// the input event contained in the given arguments.
        /// </summary>
        /// <param name="pObject">The element.</param>
        /// <param name="pArgs">The routed event arguments.</param>
        /// <returns>True if the element is listening the routed event input, false otherwise.</returns>
        internal static bool IsListening(DependencyObject pObject, RoutedEventArgs pArgs)
        {
            if
                ( InputManager.Instance.ListeningElement == pObject &&
                  Array.IndexOf( InputManager.Instance.SynchronizedInputEvents, pArgs.RoutedEvent ) >= 0 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks whether the listener must keep listening or not.
        /// </summary>
        /// <param name="pArgs">The event arguments.</param>
        /// <returns>True if must keep listening, false otherwise.</returns>
        internal static bool MustKeepListening(RoutedEventArgs pArgs)
        {
            return pArgs.RoutedEvent == InputManager.KeyDownEvent;
        }

        /// <summary>
        /// Maps a Synchronized input type received from automation client to routed event
        /// </summary>
        /// <param name="pInputType"></param>
        /// <returns>The corresponding routed event(s).</returns>
        internal static RoutedEvent[] MapInputTypeToRoutedEvents(SynchronizedInputType pInputType)
        {
            RoutedEvent[] lEvents = null;
            switch 
                ( pInputType )
            {
                case SynchronizedInputType.KeyUp:
                    lEvents = new RoutedEvent[] { InputManager.KeyUpEvent };
                    break;
                case SynchronizedInputType.KeyDown:
                    lEvents = new RoutedEvent[] { InputManager.KeyDownEvent };
                    break;
                case SynchronizedInputType.MouseLeftButtonDown:
                case SynchronizedInputType.MouseRightButtonDown:
                    lEvents = new RoutedEvent[] { InputManager.MouseDownEvent };
                    break;
                case SynchronizedInputType.MouseLeftButtonUp:
                case SynchronizedInputType.MouseRightButtonUp:
                    lEvents = new RoutedEvent[] { InputManager.MouseUpEvent };
                    break;
                default:
                    Debug.Assert(false);
                    lEvents = null;
                    break;
            }

            return lEvents;
        }

        /// <summary>
        /// Adds a preopportunity handler for the logical parent in case of templated element.
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pRoute"></param>
        /// <param name="pEventArgs"></param>
        internal static void AddParentPreOpportunityHandler(DependencyObject pObject, EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            // If the logical parent is different from visual parent then add handler on behalf of the
            // parent into the pRoute. This is to cover the templated elements, where event could be
            // handled by one of the child visual element but we should consider it as if event is handled by
            // parent element ( logical parent).
            DependencyObject lVisualParent = null;
            if 
                ( pObject is AVisual )
            {
                lVisualParent = (pObject as AVisual).VisualParent;
            }

            DependencyObject lLogicalParent = CustomGetUIParent( pObject );
            if
                ( lLogicalParent != null && 
                  lLogicalParent != lVisualParent )
            {
                UIElement lElement = lLogicalParent as UIElement;
                if 
                    ( lElement != null )
                {
                    lElement.AddSynchronizedInputPreOpportunityHandler( pRoute, pEventArgs );
                }
                else
                {
                    ContentElement lContentElement = lLogicalParent as ContentElement;
                    if 
                        ( lContentElement != null )
                    {
                        lContentElement.AddSynchronizedInputPreOpportunityHandler( pRoute, pEventArgs );
                    }
                }
            }
        }

        /// <summary>
        /// Adds a synchronized input handler to the route.
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pRoute"></param>
        /// <param name="pDelegate"></param>
        /// <param name="pProcessHandledEvents"></param>
        internal static void AddHandlerToRoute(DependencyObject pObject, EventRoute pRoute, RoutedEventDelegate pDelegate, bool pProcessHandledEvents)
        {
            pRoute.Add( pObject, pDelegate, pProcessHandledEvents );
        }

        /// <summary>
        /// This method indicates the element had the opportunity to handle event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal static void PreOpportunityHandler(object pSender, RoutedEventArgs pEventArgs)
        {
            KeyboardEventArgs lKeyboardArgs = pEventArgs as KeyboardEventArgs;
            // if it's the keyboard event then we have 1:1 mapping between handlers & events,
            // so no remapping required.
            if 
                ( lKeyboardArgs != null )
            {
                InputManager.Instance.SynchronizedInputState = SynchronizedInputStates.HadOpportunity;
            }
            else
            {
                // If this is an mouse event then we have handlers only for generic MouseDown & MouseUp events,
                // so we need additional logic here to decide between Mouse left and right button events.
                Core.MouseButtonEventArgs lMouseArgs = pEventArgs as Core.MouseButtonEventArgs;
                if 
                    ( lMouseArgs != null )
                {
                    Debug.Assert(lMouseArgs != null);
                    switch 
                        ( lMouseArgs.Button )
                    {
                        case MouseButton.Left:
                            if (InputManager.Instance.SynchronizedInputType == SynchronizedInputType.MouseLeftButtonDown ||
                                InputManager.Instance.SynchronizedInputType == SynchronizedInputType.MouseLeftButtonUp)
                            {
                                InputManager.Instance.SynchronizedInputState = SynchronizedInputStates.HadOpportunity;
                            }
                            break;
                        case MouseButton.Right:
                            if (InputManager.Instance.SynchronizedInputType == SynchronizedInputType.MouseRightButtonDown ||
                                InputManager.Instance.SynchronizedInputType == SynchronizedInputType.MouseRightButtonUp)
                            {
                                InputManager.Instance.SynchronizedInputState = SynchronizedInputStates.HadOpportunity;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// This method will be called after all class and instance handlers be called and then
        /// decide whether the event is handled by this element or some other element.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal static void PostOpportunityHandler(object pSender, RoutedEventArgs pEventArgs)
        {
            KeyboardEventArgs lKeyboardArgs = pEventArgs as KeyboardEventArgs;
            // if it's the keyboard event then we have 1:1 mapping between handlers & events,
            // so no remapping required.
            if 
                ( lKeyboardArgs != null )
            {
                InputManager.Instance.SynchronizedInputState = SynchronizedInputStates.Handled;
            }
            else
            {
                // If this is an mouse event then we have handlers only for generic MouseDown & MouseUp events,
                // so we need additional logic here to decide between Mouse left and right button events.
                Core.MouseButtonEventArgs lMouseArgs = pEventArgs as Core.MouseButtonEventArgs;
                Debug.Assert(lMouseArgs != null);
                if 
                    ( lMouseArgs != null )
                {
                    switch 
                        ( lMouseArgs.Button )
                    {
                        case MouseButton.Left:
                            if (InputManager.Instance.SynchronizedInputType == SynchronizedInputType.MouseLeftButtonDown ||
                                InputManager.Instance.SynchronizedInputType == SynchronizedInputType.MouseLeftButtonUp)
                            {
                                InputManager.Instance.SynchronizedInputState = SynchronizedInputStates.Handled;
                            }
                            break;
                        case MouseButton.Right:
                            if (InputManager.Instance.SynchronizedInputType == SynchronizedInputType.MouseRightButtonDown ||
                                InputManager.Instance.SynchronizedInputType == SynchronizedInputType.MouseRightButtonUp)
                            {
                                InputManager.Instance.SynchronizedInputState = SynchronizedInputStates.Handled;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Notifies automatic events listeners.
        /// </summary>
        internal static void RaiseAutomaticEvents()
        {
            if 
                ( InputElement.IsUIElement( InputManager.Instance.ListeningElement ) )
            {
                UIElement lElement = InputManager.Instance.ListeningElement as UIElement;
                //Raise InputDiscarded automation event
                SynchronizedInputHelper.RaiseAutomationEvent( lElement );
            }
            else if 
                ( InputElement.IsContentElement( InputManager.Instance.ListeningElement ) )
            {
                ContentElement lContentElement = InputManager.Instance.ListeningElement as ContentElement;
                //Raise InputDiscarded automation event
                SynchronizedInputHelper.RaiseAutomationEvent( lContentElement );
            }
        }

        /// <summary>
        /// Raise the synchronized input automatic events.
        /// </summary>
        /// <param name="pElement">The element on which raising automatic event.</param>
        internal static void RaiseAutomationEvent(UIElement pElement)
        {
            if
                ( pElement != null )
            {
                switch 
                    ( InputManager.Instance.SynchronizedInputState )
                {
                    case SynchronizedInputStates.Handled:
                        pElement.RaiseAutomaticEvent( AutomaticEvents.InputReachedTarget );
                        break;
                    case SynchronizedInputStates.Discarded:
                        pElement.RaiseAutomaticEvent( AutomaticEvents.InputDiscarded );
                        break;
                    default:
                        pElement.RaiseAutomaticEvent( AutomaticEvents.InputReachedOtherElement );
                        break;
                }
            }

        }

        /// <summary>
        /// Raise the synchronized input automatic events.
        /// </summary>
        /// <param name="pElement">The element on which raising automatic event.</param>
        internal static void RaiseAutomationEvent(ContentElement pElement)
        {
            if
                ( pElement != null )
            {
                switch 
                    ( InputManager.Instance.SynchronizedInputState )
                {
                    case SynchronizedInputStates.Handled:
                        pElement.RaiseAutomaticEvent( AutomaticEvents.InputReachedTarget );
                        break;
                    case SynchronizedInputStates.Discarded:
                        pElement.RaiseAutomaticEvent( AutomaticEvents.InputDiscarded );
                        break;
                    default:
                        pElement.RaiseAutomaticEvent( AutomaticEvents.InputReachedOtherElement );
                        break;
                }
            }

        }

        #endregion Methods
    }
}
