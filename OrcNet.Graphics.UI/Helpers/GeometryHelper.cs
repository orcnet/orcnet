﻿using OrcNet.Graphics.UI.Maths;
using OrcNet.Graphics.UI.Render;

namespace OrcNet.Graphics.UI.Helpers
{
    /// <summary>
    /// Geometry helper class definition.
    /// </summary>
    public static class GeometryHelper
    {
        #region Fields

        /// <summary>
        /// Stores the constant segment count of a line shape.
        /// </summary>
        internal const uint cLineSegmentCount    = 1;

        /// <summary>
        /// Stores the constant point count of a line shape.
        /// </summary>
        internal const uint cLinePointCount      = 2;

        /// <summary>
        /// Stores the constant segment count if regular squared shape.
        /// </summary>
        internal const uint cSquaredSegmentCount = 4;

        /// <summary>
        /// Stores the constant point count if regular squared shape.
        /// </summary>
        internal const uint cSquaredPointCount   = 5;

        /// <summary>
        /// Stores the constant segment count if regular rounded shape.
        /// </summary>
        internal const uint cRoundedSegmentCount = 8;

        /// <summary>
        /// Stores the constant point count if regular rounded shape.
        /// </summary>
        internal const uint cRoundedPointCount   = 17;

        /// <summary>
        /// Stores the constant segment count of an ellipse shape.
        /// </summary>
        internal const uint cEllipseSegmentCount = 4;

        /// <summary>
        /// Stores the constant point count of an ellipse shape.
        /// </summary>
        internal const uint cEllipsePointCount   = 13;

        /// <summary>
        /// Stores the approximation of a 1/4 circle with a Bezier curve 
        /// </summary>
        internal const double cArcAsBezier = 0.5522847498307933984;

        #endregion Fields

        #region Methods

        /// <summary>
        /// Gets the world space bounds of a geometry given a pen, two points and transformation to apply.
        /// </summary>
        /// <param name="pPen"></param>
        /// <param name="pWorldMatrix"></param>
        /// <param name="pPoint1"></param>
        /// <param name="pPoint2"></param>
        /// <param name="pGeometryMatrix"></param>
        /// <returns></returns>
        internal static Rect GetBoundsHelper(Pen pPen, Matrix pWorldMatrix, Point pPoint1, Point pPoint2, Matrix pGeometryMatrix)
        {
            if 
                ( pPen == null && 
                  pWorldMatrix.IsIdentity && 
                  pGeometryMatrix.IsIdentity )
            {
                return new Rect( pPoint1, pPoint2 );
            }
            else
            {
                Point lPoint1GeometrySpace = pGeometryMatrix.Transform( pPoint1 );
                Point lPoint2GeometrySpace = pGeometryMatrix.Transform( pPoint2 );

                Rect lBoundsInGeometrySpace = new Rect( lPoint1GeometrySpace, lPoint2GeometrySpace );
                lBoundsInGeometrySpace.Transform( pWorldMatrix );
                return lBoundsInGeometrySpace;
            }
        }

        /// <summary>
        /// Gets the world bounds of a rectangle geometry given a pen, the rectangle shape and transformation to apply.
        /// </summary>
        /// <param name="pPen"></param>
        /// <param name="pWorldMatrix"></param>
        /// <param name="pRectangle"></param>
        /// <param name="pRadiusX"></param>
        /// <param name="pRadiusY"></param>
        /// <param name="pGeometryMatrix"></param>
        /// <returns></returns>
        internal static Rect GetBoundsHelper(Pen pPen, Matrix pWorldMatrix, Rect pRectangle, double pRadiusX, double pRadiusY, Matrix pGeometryMatrix)
        {
            Rect lResult;

            if 
                ( pRectangle.IsEmpty )
            {
                lResult = Rect.Empty;
            }
            else if 
                ( (pPen == null || pPen.DoesNotContainGaps) &&
                   pGeometryMatrix.IsIdentity && pWorldMatrix.IsIdentity )
            {
                double lStrokeThickness = 0.0;

                lResult = pRectangle;
                if 
                    ( Pen.ContributesToBounds( pPen ) )
                {
                    lStrokeThickness = System.Math.Abs( pPen.Thickness );

                    lResult.X      -= 0.5 * lStrokeThickness;
                    lResult.Y      -= 0.5 * lStrokeThickness;
                    lResult.Width  += lStrokeThickness;
                    lResult.Height += lStrokeThickness;
                }
            }
            else
            {
                Point lMin = new Point( double.MaxValue, double.MaxValue );
                Point lMax = new Point( double.MinValue, double.MinValue );
                Point[] lPoints = GetRectanglePointList( pRectangle, pRadiusX, pRadiusY );
                foreach
                    ( Point lCurr in lPoints )
                {
                    if
                        ( lCurr.X < lMin.X )
                    {
                        lMin.X = lCurr.X;
                    }
                    if
                        ( lCurr.Y < lMin.Y )
                    {
                        lMin.Y = lCurr.Y;
                    }
                    if
                        ( lCurr.X > lMax.X )
                    {
                        lMax.X = lCurr.X;
                    }
                    if
                        ( lCurr.Y > lMax.Y )
                    {
                        lMax.Y = lCurr.Y;
                    }
                }

                // Now uses the two start and end points in geometry space
                // to get the bounds.
                lResult = GetBoundsHelper( pPen, pWorldMatrix, lMin, lMax, pGeometryMatrix );
            }

            return lResult;
        }

        /// <summary>
        /// Gets the world bounds of an ellipse geometry given a pen, the ellipse center, its radius in both axes and transformation to apply.
        /// </summary>
        /// <param name="pPen"></param>
        /// <param name="pWorldMatrix"></param>
        /// <param name="pCenter"></param>
        /// <param name="pRadiusX"></param>
        /// <param name="pRadiusY"></param>
        /// <param name="pGeometryMatrix"></param>
        /// <returns></returns>
        internal static Rect GetBoundsHelper(Pen pPen, Matrix pWorldMatrix, Point pCenter, double pRadiusX, double pRadiusY, Matrix pGeometryMatrix)
        {
            Rect lResult;

            if 
                ( (pPen == null || pPen.DoesNotContainGaps) &&
                  pWorldMatrix.IsIdentity && pGeometryMatrix.IsIdentity )
            {
                double lStrokeThickness = 0.0;
                if 
                    ( Pen.ContributesToBounds( pPen ) )
                {
                    lStrokeThickness = System.Math.Abs( pPen.Thickness );
                }

                lResult = new Rect( pCenter.X - System.Math.Abs( pRadiusX ) - 0.5 * lStrokeThickness,
                                    pCenter.Y - System.Math.Abs( pRadiusY ) - 0.5 * lStrokeThickness,
                                    2.0 * System.Math.Abs( pRadiusX ) + lStrokeThickness,
                                    2.0 * System.Math.Abs( pRadiusY ) + lStrokeThickness );
            }
            else
            {
                Point lMin = new Point( double.MaxValue, double.MaxValue );
                Point lMax = new Point( double.MinValue, double.MinValue );
                Point[] lPoints = GetEllipsePointList( pCenter, pRadiusX, pRadiusY );
                foreach
                    ( Point lCurr in lPoints )
                {
                    if
                        ( lCurr.X < lMin.X )
                    {
                        lMin.X = lCurr.X;
                    }
                    if
                        ( lCurr.Y < lMin.Y )
                    {
                        lMin.Y = lCurr.Y;
                    }
                    if
                        ( lCurr.X > lMax.X )
                    {
                        lMax.X = lCurr.X;
                    }
                    if
                        ( lCurr.Y > lMax.Y )
                    {
                        lMax.Y = lCurr.Y;
                    }
                }

                // Now uses the two start and end points in geometry space
                // to get the bounds.
                lResult = GetBoundsHelper( pPen, pWorldMatrix, lMin, lMax, pGeometryMatrix );
            }

            return lResult;
        }

        /// <summary>
        /// Gets the set of points for the given rectangle.
        /// </summary>
        /// <param name="pRectangle">The rectangle points must be get for.</param>
        /// <param name="pRadiusX">The radius on the X axis.</param>
        /// <param name="pRadiusY">The radius on the Y axis.</param>
        /// <returns></returns>
        internal static Point[] GetRectanglePointList(Rect pRectangle, double pRadiusX, double pRadiusY)
        {
            Point[] lResult;
            if 
                ( IsRounded( pRadiusX, pRadiusY ) )
            {
                // It is a rounded rectangle
                lResult = new Point[ cRoundedPointCount ];

                pRadiusX = System.Math.Min( pRectangle.Width * (1.0 / 2.0), System.Math.Abs( pRadiusX ) );
                pRadiusY = System.Math.Min( pRectangle.Height * (1.0 / 2.0), System.Math.Abs( pRadiusY ) );

                double lBezierX = (1.0 - cArcAsBezier) * pRadiusX;
                double lBezierY = (1.0 - cArcAsBezier) * pRadiusY;

                lResult[1].X = lResult[0].X = lResult[15].X = lResult[14].X = pRectangle.X;
                lResult[2].X = lResult[13].X = pRectangle.X + lBezierX;
                lResult[3].X = lResult[12].X = pRectangle.X + pRadiusX;
                lResult[4].X = lResult[11].X = pRectangle.Right - pRadiusX;
                lResult[5].X = lResult[10].X = pRectangle.Right - lBezierX;
                lResult[6].X = lResult[7].X = lResult[8].X = lResult[9].X = pRectangle.Right;

                lResult[2].Y = lResult[3].Y = lResult[4].Y = lResult[5].Y = pRectangle.Y;
                lResult[1].Y = lResult[6].Y = pRectangle.Y + lBezierY;
                lResult[0].Y = lResult[7].Y = pRectangle.Y + pRadiusY;
                lResult[15].Y = lResult[8].Y = pRectangle.Bottom - pRadiusY;
                lResult[14].Y = lResult[9].Y = pRectangle.Bottom - lBezierY;
                lResult[13].Y = lResult[12].Y = lResult[11].Y = lResult[10].Y = pRectangle.Bottom;

                lResult[16] = lResult[0];
            }
            else
            {
                // The rectangle is not rounded
                lResult = new Point[ cSquaredPointCount ];

                lResult[0].X = lResult[3].X = lResult[4].X = pRectangle.X;
                lResult[1].X = lResult[2].X = pRectangle.Right;

                lResult[0].Y = lResult[1].Y = lResult[4].Y = pRectangle.Y;
                lResult[2].Y = lResult[3].Y = pRectangle.Bottom;
            }

            return lResult;
        }

        /// <summary>
        /// Gets the set of points for an ellipse described by the given center and radius components.
        /// </summary>
        /// <param name="pCenter"></param>
        /// <param name="pRadiusX"></param>
        /// <param name="pRadiusY"></param>
        /// <returns></returns>
        internal static Point[] GetEllipsePointList(Point pCenter, double pRadiusX, double pRadiusY)
        {
            Point[] lResult = new Point[ cEllipsePointCount ];
            pRadiusX = System.Math.Abs( pRadiusX );
            pRadiusY = System.Math.Abs( pRadiusY );

            // Set the X coordinates
            double lMiddle = pRadiusX * cArcAsBezier;

            lResult[0].X = lResult[1].X = lResult[11].X = lResult[12].X = pCenter.X + pRadiusX;
            lResult[2].X = lResult[10].X = pCenter.X + lMiddle;
            lResult[3].X = lResult[9].X = pCenter.X;
            lResult[4].X = lResult[8].X = pCenter.X - lMiddle;
            lResult[5].X = lResult[6].X = lResult[7].X = pCenter.X - pRadiusX;

            // Set the Y coordinates
            lMiddle = pRadiusY * cArcAsBezier;

            lResult[2].Y = lResult[3].Y = lResult[4].Y = pCenter.Y + pRadiusY;
            lResult[1].Y = lResult[5].Y = pCenter.Y + lMiddle;
            lResult[0].Y = lResult[6].Y = lResult[12].Y = pCenter.Y;
            lResult[7].Y = lResult[11].Y = pCenter.Y - lMiddle;
            lResult[8].Y = lResult[9].Y = lResult[10].Y = pCenter.Y - pRadiusY;

            return lResult;
        }

        /// <summary>
        /// Gets the given rectangle amount of points and segments.
        /// </summary>
        /// <param name="pRect">The rectangle those amount must be returned for.</param>
        /// <param name="pRadiusX">The radius on the X axis if rounded</param>
        /// <param name="pRadiusY">The radius on the Y axis if rounded</param>
        /// <param name="pPointCount">The amount of points.</param>
        /// <param name="pSegmentCount">The amount of segments.</param>
        private static void GetCounts(Rect pRect, double pRadiusX, double pRadiusY, out uint pPointCount, out uint pSegmentCount)
        {
            if 
                ( pRect.IsEmpty )
            {
                pPointCount = 0;
                pSegmentCount = 0;
            }
            else if 
                ( IsRounded( pRadiusX, pRadiusY ) )
            {
                // The rectangle is rounded
                pPointCount   = cRoundedPointCount;
                pSegmentCount = cRoundedSegmentCount;
            }
            else
            {
                pPointCount   = cSquaredPointCount;
                pSegmentCount = cSquaredSegmentCount;
            }
        }

        /// <summary>
        /// Checks whether the geometry is rounded given the X/Y radius components.
        /// </summary>
        /// <param name="pRadiusX">The radius on the X axis.</param>
        /// <param name="pRadiusY">The radius on the Y axis.</param>
        /// <returns></returns>
        internal static bool IsRounded(double pRadiusX, double pRadiusY)
        {
            return pRadiusX != 0.0 && 
                   pRadiusY != 0.0;
        }

        #endregion Methods
    }
}
