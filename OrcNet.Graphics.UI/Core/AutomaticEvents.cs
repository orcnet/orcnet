﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Automatic events enumeration.
    /// </summary>
    public enum AutomaticEvents
    {
        /// <summary>
        /// Focus has changed.
        /// </summary>
        FocusChanged,
        
        /// <summary>
        /// Property has changed.
        /// </summary>
        PropertyChanged,
        
        /// <summary>
        /// Input has reached its target.
        /// </summary>
        InputReachedTarget,

        /// <summary>
        /// Input reached another target.
        /// </summary>
        InputReachedOtherElement,

        /// <summary>
        /// Input has been discarded.
        /// </summary>
        InputDiscarded,
    }
}
