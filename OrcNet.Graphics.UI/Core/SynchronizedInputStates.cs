﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Enumeration of the different synchronized input states.
    /// </summary>
    internal enum SynchronizedInputStates
    {
        /// <summary>
        /// No opportunity.
        /// </summary>
        NoOpportunity = 0x01,

        /// <summary>
        /// Had an opportunity.
        /// </summary>
        HadOpportunity = 0x02,

        /// <summary>
        /// Is handled.
        /// </summary>
        Handled = 0x04,

        /// <summary>
        /// Is discarded.
        /// </summary>
        Discarded = 0x08
    }
}
