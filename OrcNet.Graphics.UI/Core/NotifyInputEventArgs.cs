﻿using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Notify input event arguments prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void NotifyInputEventDelegate(object pSender, NotifyInputEventArgs pEventArgs);

    /// <summary>
    /// Notify an input from the manager.
    /// </summary>
    public class NotifyInputEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the input to notify.
        /// </summary>
        private InputEventArgs mInput;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the input to notify.
        /// </summary>
        public InputEventArgs Input
        {
            get
            {
                return this.mInput;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="NotifyInputEventArgs"/> class.
        /// </summary>
        internal NotifyInputEventArgs()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Resets the input event notifier for object recycling purpose only.
        /// </summary>
        /// <param name="pInput"></param>
        internal virtual void Reset(InputEventArgs pInput)
        {
            this.mInput = pInput;
        }

        #endregion Methods
    }
}
