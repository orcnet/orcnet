﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Hit test result class definition owning the visual
    /// that was hit during a hit test pass.
    /// </summary>
    public abstract class AHitTestResult
    {
        #region Delegates

        /// <summary>
        /// Delegate for hit tester to control returning of hit information on visual.
        /// </summary>
        public delegate HitTestResultBehavior HitTestResultCallback(AHitTestResult pResult);

        #endregion Delegates

        #region Properties

        /// <summary>
        /// Stores the visual that was hit.
        /// </summary>
        private DependencyObject mHit;

        #endregion Properties

        #region Properties

        /// <summary>
        /// Gets the visual that was hit.
        /// </summary>
        public DependencyObject VisualHit
        {
            get
            {
                return mHit;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AHitTestResult"/> class.
        /// </summary>
        /// <param name="pHit"></param>
        internal AHitTestResult(DependencyObject pHit)
        {
            this.mHit = pHit;
        }

        #endregion Constructor
    }
}
