﻿using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Ancestor changed event argument class definition.
    /// </summary>
    internal class AncestorChangedEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the child the parent has changed.
        /// </summary>
        private DependencyObject mSubRoot;

        /// <summary>
        /// Stores the old parent.
        /// </summary>
        private DependencyObject mOldParent;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the child the parent has changed.
        /// </summary>
        public DependencyObject Ancestor
        {
            get
            {
                return this.mSubRoot;
            }
        }

        /// <summary>
        /// Gets the old parent.
        /// </summary>
        public DependencyObject OldParent
        {
            get
            {
                return this.mOldParent;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AncestorChangedEventArgs"/> class.
        /// </summary>
        /// <param name="pSubRoot">The child the parent has changed.</param>
        /// <param name="pOldParent">The old parent.</param>
        public AncestorChangedEventArgs(DependencyObject pSubRoot, DependencyObject pOldParent)
        {
            this.mSubRoot   = pSubRoot;
            this.mOldParent = pOldParent;
        }

        #endregion Constructor
    }
}
