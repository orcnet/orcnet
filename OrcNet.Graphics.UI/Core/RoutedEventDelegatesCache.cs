﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Routed event to delegates mappers cache class definition.
    /// </summary>
    internal class RoutedEventDelegatesCache
    {
        #region Fields

        /// <summary>
        /// Stores the set of RoutedEventDelegates keyed on RoutedEvent
        /// </summary>
        private List<RoutedEventDelegates> mEventDelegates;

        #endregion Fields

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="RoutedEventDelegatesCache"/> class.
        /// </summary>
        /// <param name="pSize">The default cache size.</param>
        internal RoutedEventDelegatesCache(int pSize)
        {
            this.mEventDelegates = new List<RoutedEventDelegates>( pSize );
        }

        #endregion Construction

        #region Methods
        
        /// <summary>
        /// Adds a routed event delegate at the given index of the cache
        /// </summary>
        /// <param name="pIndex">The cache index to set the delegate at.</param>
        /// <param name="pDelegate">The new delegate to add.</param>
        /// <param name="pProcessHandled">The flag indicating whether the handled events must be processed as well or not.</param>
        /// <returns>The updated set of routed event delegates.</returns>
        internal RoutedEventDelegateInfoContainer AddToExistingDelegates(int pIndex, Delegate pDelegate, bool pProcessHandled)
        {
            Debug.Assert( pIndex != -1, 
                          "Set of delegates already existing for the given routed event!!!");

            // Create a new RoutedEventHandler
            RoutedEventHandlerInfo lInfo = new RoutedEventHandlerInfo( pDelegate, pProcessHandled );

            // Check if we need to create a new node in the linked list
            RoutedEventDelegateInfoContainer lDelegatesContainer = this.mEventDelegates[ pIndex ].DelegatesContainer;
            if 
                ( lDelegatesContainer == null || 
                  this.mEventDelegates[ pIndex ].HasSelfHandlers == false )
            {
                // Create a new node in the linked list of class 
                // lDelegatesContainer for this type and routed event.
                lDelegatesContainer = new RoutedEventDelegateInfoContainer();
                lDelegatesContainer.Delegates = new RoutedEventHandlerInfo[ 1 ];
                lDelegatesContainer.Delegates[ 0 ] = lInfo;
                lDelegatesContainer.Next = this.mEventDelegates[ pIndex ].DelegatesContainer;
                this.mEventDelegates[ pIndex ].DelegatesContainer = lDelegatesContainer;
                this.mEventDelegates[ pIndex ].HasSelfHandlers = true;
            }
            else
            {
                // Add this pDelegate to the existing node in the linked list 
                // of class lDelegatesContainer for this type and routed event.
                int lDelegateCount = lDelegatesContainer.Delegates.Length;
                RoutedEventHandlerInfo[] lMergedDelegates = new RoutedEventHandlerInfo[ lDelegateCount + 1 ];
                Array.Copy( lDelegatesContainer.Delegates, 0, lMergedDelegates, 0, lDelegateCount );
                lMergedDelegates[ lDelegateCount ] = lInfo;
                lDelegatesContainer.Delegates = lMergedDelegates;
            }

            return lDelegatesContainer;
        }

        /// <summary>
        /// Gets the routed event delegates at the given cache index.
        /// </summary>
        /// <param name="pIndex">The index to get the routed event's delegates at.</param>
        /// <returns>The set of routed event delegates.</returns>
        internal RoutedEventDelegateInfoContainer GetExistingHandlers(int pIndex)
        {
            Debug.Assert( pIndex != -1, 
                          "Cannot provide delegates by negative index!!!");

            return this.mEventDelegates[ pIndex ].DelegatesContainer;
        }

        /// <summary>
        /// Creates a new routed event to delegates mapper object and return its cache index.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event the mapper is for.</param>
        /// <param name="pDelegates">The delegates container that must be mapped with the routed event.</param>
        /// <returns>The new mapper cache's index.</returns>
        internal int CreateHandlersLink(RoutedEvent pRoutedEvent, RoutedEventDelegateInfoContainer pDelegates)
        {
            Debug.Assert( this.GetHandlersIndex( pRoutedEvent ) == -1, 
                          "There should not exist a set of delegates for the given routed event" );

            RoutedEventDelegates lDelegatesContainer = new RoutedEventDelegates();
            lDelegatesContainer.RoutedEvent        = pRoutedEvent;
            lDelegatesContainer.DelegatesContainer = pDelegates;
            lDelegatesContainer.HasSelfHandlers    = false;
            this.mEventDelegates.Add( lDelegatesContainer );

            return this.mEventDelegates.Count - 1;
        }

        /// <summary>
        /// Updates the sub class delegates with the given parent class's delegates
        /// </summary>
        /// <param name="pRoutedEvent">The routed event the delegates are linked to.</param>
        /// <param name="pClassDelegates">The parent class's delegates to update by the routed event ones.</param>
        internal void UpdateSubClassHandlers(RoutedEvent pRoutedEvent, RoutedEventDelegateInfoContainer pClassDelegates)
        {
            Debug.Assert( pClassDelegates != null, 
                          "Cannot update an empty set of routed event delegates!!!");

            // Get the lDelegatesContainer lIndex corresponding to the given RoutedEvent
            int lIndex = this.GetHandlersIndex( pRoutedEvent );
            if 
                ( lIndex != -1 )
            {
                bool lHasSelfHandlers = this.mEventDelegates[ lIndex ].HasSelfHandlers;

                // Fetch the delegates for your base Type the current node knows of
                RoutedEventDelegateInfoContainer lDelegatesContainer = lHasSelfHandlers ? this.mEventDelegates[ lIndex ].DelegatesContainer.Next :
                                                                                          this.mEventDelegates[ lIndex ].DelegatesContainer;

                bool lMustUpdate = false;
                if 
                    ( lDelegatesContainer != null )
                {
                    if 
                        ( pClassDelegates.Next != null && 
                          pClassDelegates.Next.Contains( lDelegatesContainer ) )
                    {
                        lMustUpdate = true;
                    }
                }
                else
                {
                    lMustUpdate = true;
                }

                if 
                    ( lMustUpdate )
                {
                    if 
                        ( lHasSelfHandlers )
                    {
                        this.mEventDelegates[ lIndex ].DelegatesContainer.Next = pClassDelegates;
                    }
                    else
                    {
                        this.mEventDelegates[ lIndex ].DelegatesContainer = pClassDelegates;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the routed event delegates cache's index.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event delegates cache index must be found for.</param>
        /// <returns>The cache index of the given routed event delegates.</returns>
        internal int GetHandlersIndex(RoutedEvent pRoutedEvent)
        {
            for 
                ( int lCurr = 0; lCurr < this.mEventDelegates.Count; lCurr++ )
            {
                if 
                    ( this.mEventDelegates[ lCurr ].RoutedEvent == pRoutedEvent )
                {
                    return lCurr;
                }
            }

            return -1;
        }

        #endregion Operations
    }
}
