﻿using System.Collections;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Input event arguments class definition owning the 
    /// input device.
    /// </summary>
    public class InputEventArgs : RoutedEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the input device associated with the event.
        /// </summary>
        private object mDevice;

        /// <summary>
        /// Stores a set of event contextual data needed to process the input.
        /// </summary>
        private Hashtable mEventContextualData;

        /// <summary>
        /// Stores the time when the input occured.
        /// </summary>
        private static int sTimeStamp;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the input device associated with the event.
        /// </summary>
        public object Device
        {
            get
            {
                return this.mDevice;
            }
            internal set
            {
                this.mDevice = value;
            }
        }

        /// <summary>
        /// Gets the time when the input occured.
        /// </summary>
        public int TimeStamp
        {
            get
            {
                return sTimeStamp;
            }
            internal set
            {
                sTimeStamp = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InputEventArgs"/> class.
        /// </summary>
        /// <param name="pDevice">The input device associated with the event.</param>
        /// <param name="pTimeStamp">The time when the input occured.</param>
        public InputEventArgs(object pDevice, int pTimeStamp)
        {
            this.mDevice    = pDevice;
            this.mEventContextualData = new Hashtable();
            sTimeStamp = pTimeStamp;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the contextual event data corresponding to the given key.
        /// </summary>
        /// <param name="pKey">The key.</param>
        /// <returns>The contextual event data, null if key not found.</returns>
        public object GetData(object pKey)
        {
            return this.mEventContextualData[ pKey ];
        }

        /// <summary>
        /// Sets the contextual input event data for the given key.
        /// </summary>
        /// <param name="pKey">The key the data is attached to.</param>
        /// <param name="pData">The new contextual input event data.</param>
        public void SetData(object pKey, object pData)
        {
            this.mEventContextualData[ pKey ] = pData;
        }

        #endregion Methods
    }
}
