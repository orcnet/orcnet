﻿using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// The mouse input report class definition owning the input info provided from a mouse.
    /// </summary>
    internal class MouseInputReport : AInputReport
    {
        #region Fields

        /// <summary>
        /// Stores the mouse X position.
        /// </summary>
        private int mX;

        /// <summary>
        /// Stores the mouse Y position.
        /// </summary>
        private int mY;

        /// <summary>
        /// Stores the mouse wheel 
        /// </summary>
        private int mWheel;

        /// <summary>
        /// Stores the reported mouse actions
        /// </summary>
        private MouseActions mActions;

        /// <summary>
        /// Stores a potential extra object.
        /// </summary>
        private WeakReference mExtra;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the mouse X position.
        /// </summary>
        public int X
        {
            get
            {
                return this.mX;
            }
        }

        /// <summary>
        /// Gets the mouse Y position.
        /// </summary>
        public int Y
        {
            get
            {
                return this.mY;
            }
        }

        /// <summary>
        /// Gets the mouse wheel 
        /// </summary>
        public int Wheel
        {
            get
            {
                return this.mWheel;
            }
        }

        /// <summary>
        /// Gets the reported mouse actions
        /// </summary>
        public MouseActions Actions
        {
            get
            {
                return this.mActions;
            }
        }

        /// <summary>
        /// Gets a potential extra object.
        /// </summary>
        public object Extra
        {
            get
            {
                return this.mExtra.Target;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the mouse input report has been sync or not.
        /// </summary>
        internal bool IsSynchronized
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseInputReport"/> class.
        /// </summary>
        /// <param name="pMode">The input mode.</param>
        /// <param name="pTimeStamp">The time the input occured at.</param>
        /// <param name="pInputSource">The source input comes from.</param>
        /// <param name="pActions">The reported mouse actions</param>
        /// <param name="pX">The mouse X position.</param>
        /// <param name="pY">The mouse Y position.</param>
        /// <param name="pWheel">The mouse wheel </param>
        /// <param name="pExtra">The potential extra object.</param>
        public MouseInputReport(InputMode pMode, int pTimeStamp, PresentationSource pInputSource, MouseActions pActions, int pX, int pY, int pWheel, object pExtra) :
        base( pInputSource, InputType.Mouse, pMode, pTimeStamp )
        {
            this.mActions = pActions;
            this.mX = pX;
            this.mY = pY;
            this.mWheel = pWheel;
            this.mExtra = new WeakReference( pExtra );
        }

        #endregion Constructor
    }
}
