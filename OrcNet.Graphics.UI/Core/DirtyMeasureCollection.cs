﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Dirty measure collection owning UI elements that need to be
    /// measured.
    /// </summary>
    internal class DirtyMeasureCollection : ADirtyLayoutCollection
    {
        #region Methods

        /// <summary>
        /// Checks whether the given element is already added as dirty element to that collection.
        /// </summary>
        /// <param name="pElement">The element to look for.</param>
        /// <returns>True if already in, false otherwise.</returns>
        internal override bool Contains(UIElement pElement)
        {
            return pElement.HasRequestedMeasurement;
        }

        /// <summary>
        /// Gets the flag indicating whether the given parent is sufficient
        /// to assure its children will be valid without having to re compute them.
        /// </summary>
        /// <param name="pParent">The parent to check.</param>
        /// <returns>True if parent is sufficient to avoid children computation, false otherwise.</returns>
        protected override bool CanRelyOnParentValidation(UIElement pParent)
        {
            return pParent.IsMeasureDirty &&
                   pParent.IsMeasureInProgress == false;
        }

        /// <summary>
        /// Attaches a node and a UI element together for fast retrieval.
        /// </summary>
        /// <param name="pNode">The node to attach with the UI element. (Null means no attachment)</param>
        /// <param name="pToAttach">The UI element to attach to the given node.</param>
        protected override void AttachToNode(DirtyNode pNode, UIElement pToAttach)
        {
            pToAttach.HasRequestedMeasurement = pNode != null;
            pToAttach.MeasureNode = pNode;
        }

        /// <summary>
        /// Gets the node the given UI element is attached to.
        /// </summary>
        /// <param name="pElement">The UI element the node must be returned.</param>
        /// <returns>The UI element's node it is attached to, Null otherwise.</returns>
        protected override DirtyNode GetNode(UIElement pElement)
        {
            return pElement.MeasureNode;
        }

        /// <summary>
        /// Invalidates the given element to inform it needs to be re computed.
        /// </summary>
        /// <param name="pElement">The element to invalidate.</param>
        protected override void Invalidate(UIElement pElement)
        {
            pElement.InvalidateMeasure();
        }
        
        #endregion Methods
    }
}
