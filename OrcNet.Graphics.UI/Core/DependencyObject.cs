﻿using OrcNet.Core;
using OrcNet.Graphics.UI.Threading;
using System;
using System.Collections.Generic;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Object having dependency properties class definition.
    /// </summary>
    public class DependencyObject : ADispatcherObject, IObservable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the dependency objetc has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the set of property observer(s) to notify on property changes.
        /// </summary>
        private List<IObserver> mPropertyObservers;

        #endregion Fields

        #region Properties

        #region Properties AMemoryProfilable

        /// <summary>
        /// Gets the object size.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = sizeof(bool);
                lSize += sizeof(int) * (uint)this.mPropertyObservers.Count; // Just count the refs.
                return lSize;
            }
        }

        #endregion Properties AMemoryProfilable

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyObject"/> class.
        /// </summary>
        public DependencyObject()
        {
            this.mIsDisposed = false;
            this.mPropertyObservers = new List<IObserver>();
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        /// <summary>
        /// Notifies observer(s) that the observable had a property changed.
        /// </summary>
        /// <param name="pOldValue">The old property's value.</param>
        /// <param name="pNewValue">The new property's value.</param>
        /// <param name="pPropertyName">The changed property's name</param>
        /// <returns>True if notified, false otherwise.</returns>
        protected bool NotifyPropertyChanged<T>(ref T pOldValue, T pNewValue, string pPropertyName)
        {
            if
                ( EqualityComparer<T>.Default.Equals( pOldValue, pNewValue ) == false )
            {
                T lOldValue = pOldValue;
                pOldValue   = pNewValue;

                this.InformPropertyChangedListener( lOldValue, pNewValue, pPropertyName );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Informs listener(s) that the observable had a property changed
        /// whatever the old and new value are differnet or not.
        /// </summary>
        /// <param name="pOldValue">The old property's value.</param>
        /// <param name="pNewValue">The new property's value.</param>
        /// <param name="pPropertyName">The changed property's name</param>
        protected void InformPropertyChangedListener<T>(T pOldValue, T pNewValue, string pPropertyName)
        {
            foreach
                ( IObserver lObserver in this.mPropertyObservers )
            {
                lObserver.OnObservablePropertyChanged( this, new PropertyChangedEventArgs( pOldValue, pNewValue, pPropertyName ) );
            }

            // Allow class inheriting to be aware of the change.
            this.OnPropertyChanged( pPropertyName );
        }
        
        /// <summary>
        /// Internal delegate used to add extra process on property changes.
        /// </summary>
        /// <param name="pPropertyName">The property changed.</param>
        protected virtual void OnPropertyChanged(string pPropertyName)
        {
            // Override it.
        }

        #endregion Methods Internal

        #region Methods IObservable

        /// <summary>
        /// Add a new observer to this observable for being 
        /// notified it had property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        public void AddPropertyObserver(IObserver pObserver)
        {
            if
                ( this.mPropertyObservers.Contains( pObserver ) == false )
            {
                this.mPropertyObservers.Add( pObserver );
            }
        }

        /// <summary>
        /// Remove an observer to this observable to stop
        /// notifying it on property changes.
        /// </summary>
        /// <param name="pObserver">The observer</param>
        public void RemovePropertyObserver(IObserver pObserver)
        {
            if
                ( this.mPropertyObservers.Contains( pObserver ) )
            {
                this.mPropertyObservers.Remove( pObserver );
            }
        }

        #endregion Methods IObservable

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if
                ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            // Free observers ref(s)
            this.mPropertyObservers.Clear();
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
