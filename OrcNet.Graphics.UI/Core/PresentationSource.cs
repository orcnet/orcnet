﻿using OpenTK;
using OpenTK.Input;
using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using OrcNet.Graphics.UI.Maths;
using OrcNet.Graphics.UI.Services;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Stores all the window relevant info for the UI it is displayed on.
    /// </summary>
    public class PresentationSource : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the source has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the source.
        /// </summary>
        private INativeWindow mSource;

        /// <summary>
        /// Stores the root visual for this window.
        /// </summary>
        private AVisual mRoot;

        /// <summary>
        /// Stores the size to content mode.
        /// </summary>
        private SizeToContent mSizeToContentMode = SizeToContent.Manual;

        /// <summary>
        /// Stores the previous window size.
        /// </summary>
        private Size? mPreviousSize;

        #endregion Fiels

        #region Properties
        
        /// <summary>
        /// Gets the presentation source.
        /// </summary>
        public INativeWindow Source
        {
            get
            {
                return this.mSource;
            }
        }

        /// <summary>
        /// Gets or sets the Root Visual for this window. 
        /// If it is an UIElement ONLY.
        /// </summary>
        public AVisual RootVisual
        {
            get
            {
                if 
                    ( this.mIsDisposed )
                {
                    return null;
                }
                
                return this.mRoot;
            }
            set
            {
                if
                    ( this.mIsDisposed == false )
                {
                    this.RootVisualInternal = value;
                }
            }
        }

        /// <SecurityNote>
        /// Sets the new Root visual.
        /// </SecurityNote>
        private AVisual RootVisualInternal
        {
            set
            {
                if
                    ( this.mRoot != value )
                {
                    IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
                    AVisual lOldRoot = this.mRoot;
                    if 
                        ( value != null )
                    {
                        this.mRoot = value;

                        lUIService.LayoutUpdated += this.OnLayoutUpdated;
                        
                        UIElement.PropagateResumeLayout( null, value );
                    }
                    else
                    {
                        this.mRoot = null;
                    }

                    if 
                        ( lOldRoot != null )
                    {
                        lUIService.LayoutUpdated -= this.OnLayoutUpdated;

                        UIElement.PropagateSuspendLayout( lOldRoot );
                    }

                    this.OnRootChanged( lOldRoot, this.mRoot );

                    if 
                        ( this.IsLayoutActive() == true )
                    {
                        // Call the helper method SetLayoutSize to set Layout's size
                        this.SetLayoutSize();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the Size To Content mode on the source.
        /// </summary>
        /// <remarks>The default value is SizeToContent.Manual</remarks>
        public SizeToContent SizeToContentMode
        {
            get
            {
                return this.mSizeToContentMode;
            }
            set
            {
                if 
                    ( this.mSizeToContentMode == value )
                {
                    return;
                }

                this.mSizeToContentMode = value;

                // we only raise SizeToContentChanged when user interaction caused the change;
                // if a developer goes directly to HwndSource and sets SizeToContent, we will
                // not notify the wrapping Window
                if 
                    ( this.IsLayoutActive() == true )
                {
                    // Call the helper method SetLayoutSize to set Layout's size
                    this.SetLayoutSize();
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PresentationSource"/> class.
        /// </summary>
        /// <param name="pSource">The source.</param>
        public PresentationSource(INativeWindow pSource)
        {
            this.mSource = pSource;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Render the overall UI elements tree.
        /// </summary>
        internal void Render()
        {
            if
                ( this.mRoot != null )
            {
                LogManager.Instance.Log( "Starting rendering the UI tree!!!", LogType.INFO );

                // Prepare the UI elements.
                this.mRoot.Precompute();

                // Render the UI elements.
                this.mRoot.Render();

                LogManager.Instance.Log( "Ending rendering the UI tree!!!", LogType.INFO );
            }
        }

        /// <summary>
        /// Checks whether the root visual layout is active or not.
        /// </summary>
        /// <returns>True if active, false otherwise.</returns>
        private bool IsLayoutActive()
        {

            if 
                ( this.mRoot is UIElement &&
                  this.mSource != null &&
                  this.mIsDisposed == false )
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// This is the helper method that sets Layout's size basing it on
        /// the current value of SizeToContent.
        /// </summary>
        private void SetLayoutSize()
        {
            Debug.Assert( this.mSource != null, "Source is null");
            Debug.Assert( this.mIsDisposed == false, "Source is disposed");

            UIElement lRootUIElement = null;
            lRootUIElement = this.mRoot as UIElement;
            if 
                ( lRootUIElement == null )
            {
                return;
            }
            
            lRootUIElement.InvalidateMeasure();
            
            if 
                ( this.mSizeToContentMode == SizeToContent.WidthAndHeight )
            {
                // Setup constraints for measure-to-content
                Size lSize = new Size( double.PositiveInfinity, double.PositiveInfinity );
                
                lRootUIElement.Measure( lSize );
                
                lRootUIElement.Arrange( new Rect( new Point(), 
                                                  lRootUIElement.DesiredSize ) );
                
            }
            else
            {
                System.Drawing.Size lClientSize = this.mSource.ClientSize;
                Size lWindowSize = new Size( lClientSize.Width, 
                                             lClientSize.Height );
                Size lSize = new Size( ( this.mSizeToContentMode == SizeToContent.Width ? double.PositiveInfinity : lWindowSize.Width ),
                                       ( this.mSizeToContentMode == SizeToContent.Height ? double.PositiveInfinity : lWindowSize.Height ) );
                
                lRootUIElement.Measure( lSize );

                if 
                    ( this.mSizeToContentMode == SizeToContent.Width )
                {
                    lSize = new Size( lRootUIElement.DesiredSize.Width, 
                                      lWindowSize.Height );
                }
                else if 
                    ( this.mSizeToContentMode == SizeToContent.Height )
                {
                    lSize = new Size( lWindowSize.Width, 
                                      lRootUIElement.DesiredSize.Height );
                }
                else
                {
                    lSize = lWindowSize;
                }

                lRootUIElement.Arrange( new Rect( new Point(), 
                                                  lSize ) );
            }

            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            lUIService.UpdateLayout();
        }

        /// <summary>
        /// Delegate called on layout updated.
        /// </summary>
        /// <param name="pSender">The event sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnLayoutUpdated(object pSender, EventArgs pEventArgs)
        {
            UIElement lRoot = this.mRoot as UIElement;
            if 
                ( lRoot != null )
            {
                Size lNewSize = lRoot.RenderSize;
                if 
                    ( this.mPreviousSize == null || 
                      DoubleUtil.AreClose( this.mPreviousSize.Value.Width, lNewSize.Width ) == false || 
                      DoubleUtil.AreClose( this.mPreviousSize.Value.Height, lNewSize.Height ) == false )
                {
                    this.mPreviousSize = lNewSize;

                    if 
                        ( this.mSizeToContentMode != SizeToContent.Manual && 
                          this.mSource.WindowState != WindowState.Minimized )
                    {
                        //Resize( lNewSize );
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called on root changes.
        /// </summary>
        /// <param name="pOldRoot">The previous root.</param>
        /// <param name="pNewRoot">The new root.</param>
        protected void OnRootChanged(AVisual pOldRoot, AVisual pNewRoot)
        {
            if
                ( pOldRoot == pNewRoot )
            {
                return;
            }

            UIElement lOldUIRoot = pOldRoot as UIElement;
            if
                ( lOldUIRoot != null )
            {
                lOldUIRoot.UpdateIsVisibleCache();
            }

            UIElement lNewUIRoot = pNewRoot as UIElement;
            if
                ( lNewUIRoot != null )
            {
                lNewUIRoot.UpdateIsVisibleCache();
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if
                ( this.mIsDisposed == false )
            {
                this.RootVisualInternal = null;
                this.mSource = null;

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods
    }
}
