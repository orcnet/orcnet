﻿using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Automatic event arguments class definition.
    /// </summary>
    public class AutomaticEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the event automatic type.
        /// </summary>
        private AutomaticEvents mEvent;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the event automatic type.
        /// </summary>
        public AutomaticEvents Event
        {
            get
            {
                return this.mEvent;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AutomaticEventArgs"/> class.
        /// </summary>
        /// <param name="pEvent">The event type.</param>
        public AutomaticEventArgs(AutomaticEvents pEvent)
        {
            this.mEvent = pEvent;
        }

        #endregion Constructor
    }
}
