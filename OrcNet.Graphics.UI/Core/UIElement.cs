﻿using OpenTK;
using OrcNet.Core;
using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using OrcNet.Graphics.UI.Helpers;
using OrcNet.Graphics.UI.Managers;
using OrcNet.Graphics.UI.Maths;
using OrcNet.Graphics.UI.Render;
using OrcNet.Graphics.UI.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using static OrcNet.Graphics.UI.Core.AHitTestResult;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Automatic event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void AutomaticEventDelegate(object pSender, AutomaticEventArgs pEventArgs);

    /// <summary>
    /// Base UI element class definition.
    /// </summary>
    public class UIElement : AVisual, IInputElement
    {
        #region Constants

        /// <summary>
        /// Stores the constant determining the maximum amount of elements in a route.
        /// </summary>
        internal const int cMAX_ELEMENTS_IN_ROUTE = 4096;

        #endregion Constants

        #region Fields

        /// <summary>
        /// Stores the static type of UIElement.
        /// </summary>
        private static readonly Type sThisType = typeof(UIElement);

        /// <summary>
        /// Stores the coerce methods to run on property changes that require such a behavior.
        /// </summary>
        protected static readonly Dictionary<string, Action<DependencyObject>> sCoercers;
        
        /// <summary>
        /// Stores the IsEnabled changed event unique key identifier.
        /// </summary>
        internal static readonly int IsEnabledChangedKey = RoutedEvent.GetNextIdentifier();

        /// <summary>
        /// Stores the IsVisible changed event unique key identifier.
        /// </summary>
        internal static readonly int IsVisibleChangedKey = RoutedEvent.GetNextIdentifier();

        /// <summary>
        /// Stores the IsHitTestVisible changed event unique key identifier.
        /// </summary>
        internal static readonly int IsHitTestVisibleChangedKey = RoutedEvent.GetNextIdentifier();

        /// <summary>
        /// Stores the Focusable changed event unique key identifier.
        /// </summary>
        internal static readonly int FocusableChangedKey = RoutedEvent.GetNextIdentifier();

        /// <summary>
        /// Stores the IsMouseDirectlyOver changed event unique key identifier.
        /// </summary>
        internal static readonly int IsMouseDirectlyOverChangedKey = RoutedEvent.GetNextIdentifier();

        /// <summary>
        /// Stores the IsMouseCapturedChanged changed event unique key identifier.
        /// </summary>
        internal static readonly int IsMouseCapturedChangedKey = RoutedEvent.GetNextIdentifier();

        /// <summary>
        /// Stores the IsMouseCaptureWithin changed event unique key identifier.
        /// </summary>
        internal static readonly int IsMouseCaptureWithinChangedKey = RoutedEvent.GetNextIdentifier();

        /// <summary>
        /// Stores the IsKeyboardFocusWithin changed event unique key identifier.
        /// </summary>
        internal static readonly int IsKeyboardFocusWithinChangedKey = RoutedEvent.GetNextIdentifier();

        /// <summary>
        /// Stores the preview Mouse left button down routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseLeftButtonDownEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseLeftButtonDown", 
                                                                                                              typeof(MouseButtonEventDelegate), 
                                                                                                              sThisType );

        /// <summary>
        /// Stores the Mouse left button down routed event.
        /// </summary>
        public static readonly RoutedEvent MouseLeftButtonDownEvent = RoutedEvent.RegisterRoutedEvent( "MouseLeftButtonDown", 
                                                                                                       typeof(MouseButtonEventDelegate), 
                                                                                                       sThisType );

        /// <summary>
        /// Stores the preview Mouse left button up routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseLeftButtonUpEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseLeftButtonUp", 
                                                                                                            typeof(MouseButtonEventDelegate), 
                                                                                                            sThisType );

        /// <summary>
        /// Stores the Mouse left button up routed event.
        /// </summary>
        public static readonly RoutedEvent MouseLeftButtonUpEvent = RoutedEvent.RegisterRoutedEvent( "MouseLeftButtonUp", 
                                                                                                     typeof(MouseButtonEventDelegate), 
                                                                                                     sThisType );

        /// <summary>
        /// Stores the preview Mouse right button down routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseRightButtonDownEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseRightButtonDown", 
                                                                                                               typeof(MouseButtonEventDelegate), 
                                                                                                               sThisType );

        /// <summary>
        /// Stores the Mouse right button down routed event.
        /// </summary>
        public static readonly RoutedEvent MouseRightButtonDownEvent = RoutedEvent.RegisterRoutedEvent( "MouseRightButtonDown", 
                                                                                                        typeof(MouseButtonEventDelegate), 
                                                                                                        sThisType );

        /// <summary>
        /// Stores the preview Mouse right button up routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseRightButtonUpEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseRightButtonUp", 
                                                                                                             typeof(MouseButtonEventDelegate), 
                                                                                                             sThisType );

        /// <summary>
        /// Stores the Mouse right button up routed event.
        /// </summary>
        public static readonly RoutedEvent MouseRightButtonUpEvent = RoutedEvent.RegisterRoutedEvent( "MouseRightButtonUp", 
                                                                                                      typeof(MouseButtonEventDelegate), 
                                                                                                      sThisType );

        /// <summary>
        /// Stores the flag indicating whether the EventDelegatesCache exists or not.
        /// </summary>
        private bool mHasEventDelegatesCache;

        /// <summary>
        /// Stores the routed event delegates cache for this UI element.
        /// </summary>
        internal EventDelegatesCache mEventDelegatesCache;

        /// <summary>
        /// Stores the flag indicating whether the UI element is visible or not.
        /// </summary>
        private bool mIsVisible;

        /// <summary>
        /// Stores the flag indicating whether the UI element is enabled or not.
        /// </summary>
        private bool mIsEnabled;

        /// <summary>
        /// Stores the flag indicating whether the UI element is hit test visible or not.
        /// </summary>
        private bool mIsHitTestVisible;

        /// <summary>
        /// Stores the flag indicating whether the element can have the focus or not.
        /// </summary>
        private bool mFocusable;

        /// <summary>
        /// Stores the flag indicating whether the element has the keyboard focus or not.
        /// </summary>
        private bool mIsKeyboardFocused;

        /// <summary>
        /// Stores the flag indicating whether the keyboard focus is whithin the element or not.
        /// </summary>
        private bool mIsKeyboardFocusWithin;

        /// <summary>
        /// Stores the flag indicating whether the mouse is entered in the element bounds or not.
        /// </summary>
        private bool mIsMouseCaptured;

        /// <summary>
        /// Stores the flag indicating whether the mouse has that element as Top most regarding
        /// to those in the region the mouse is in.
        /// </summary>
        private bool mIsMouseDirectlyOver;

        /// <summary>
        /// Stores the flag indicating whether the mouse is on that element whatever it is a child
        /// or the top most of those in the region the mouse is in.
        /// </summary>
        private bool mIsMouseOver;

        /// <summary>
        /// Stores the flag indicating whether the mouse captured within that element or not.
        /// </summary>
        private bool mIsMouseCaptureWithin;

        /// <summary>
        /// Stores the drawing content for this UI element.
        /// </summary>
        private IDrawingContent mDrawingContent;

        /// <summary>
        /// Stores the UI element transform used for rendering it.
        /// </summary>
        private ATransform mRenderTransform;

        /// <summary>
        /// Stores the UI element render transform origin point.
        /// </summary>
        public Point mRenderTransformOrigin;

        /// <summary>
        /// STores the flag indicating whether the transforms of that UI element and 
        /// its children are valid or not.
        /// </summary>
        private bool mAreTransformsValid;

        /// <summary>
        /// Stores the flag indicating whether the UI element has been collapsed or not.
        /// </summary>
        private bool mIsCollapsed;

        /// <summary>
        /// Stores the flag indicating whether the opacity has been cut off or not.
        /// </summary>
        private bool mIsOpacityCutoff;

        /// <summary>
        /// Stores the opacity before it be cut off.
        /// </summary>
        private double mOpacityBeforeCutoff;

        /// <summary>
        /// Stores the flag indicating whether the UI element has to be measured or not.
        /// </summary>
        private bool mHasRequestedMeasurement;

        /// <summary>
        /// Stores the flag indicating whether the UI element is being measured or not.
        /// </summary>
        private bool mIsMeasureInProgress;

        /// <summary>
        /// Stores the flag indicating whether the UI element has a dirty measurement or not.
        /// </summary>
        private bool mIsMeasureDirty;

        /// <summary>
        /// Stores the flag indicating whether the UI element has never been measured or not.
        /// </summary>
        private bool mIsNeverMeasured;

        /// <summary>
        /// Stores the flag indicating whether the measure has been done while arranging as well or not.
        /// </summary>
        private bool mIsMeasuringDuringArrangement;

        /// <summary>
        /// Stores the flag indicating whether the UI element has to be arranged or not.
        /// </summary>
        private bool mHasRequestedArrangement;

        /// <summary>
        /// Stores the flag indicating whether the UI element is being arranged or not.
        /// </summary>
        private bool mIsArrangeInProgress;

        /// <summary>
        /// Stores the flag indicating whether the UI element has a dirty arrangement or not.
        /// </summary>
        private bool mIsArrangeDirty;

        /// <summary>
        /// Stores the flag indicating whether the UI element has never been arranged or not.
        /// </summary>
        private bool mIsNeverArranged;

        /// <summary>
        /// Stores the flag indicating whether the UI element must be re drawn or not.
        /// </summary>
        private bool mIsRenderDirty;

        /// <summary>
        /// Stores the UI element size used to render it.
        /// </summary>
        private Size mRenderSize;

        /// <summary>
        /// Stores the UI element desired size.
        /// </summary>
        private Size mDesiredSize;

        /// <summary>
        /// Stores the previous parent available size for that UI element.
        /// </summary>
        private Size mPreviousAvailableSize;

        /// <summary>
        /// Stores the previous UI element arrange rectangle.
        /// </summary>
        private Rect mPreviousArrangeRectangle;

        #region Fields Internal Layout Tree

        /// <summary>
        /// Stores the current size changed arguments on layout notifications.
        /// </summary>
        internal SizeChangedArguments SizeChangedArguments;

        /// <summary>
        /// Stores the Dirty node that UI element is attached to for measure update fast retrieval purpose ONLY.
        /// </summary>
        internal ADirtyLayoutCollection.DirtyNode MeasureNode;

        /// <summary>
        /// Stores the Dirty node that UI element is attached to for arrange update fast retrieval purpose ONLY.
        /// </summary>
        internal ADirtyLayoutCollection.DirtyNode ArrangeNode;

        #endregion Fields Internal Layout Tree

        #endregion Fields

        #region Events

        /// <summary>
        /// Event triggered on IsEnabled property changes.
        /// </summary>
        public event PropertyChangedEventDelegate IsEnabledChanged
        {
            add
            {
                this.AddEventDelegateToCache( IsEnabledChangedKey, value );
            }
            remove
            {
                this.RemoveEventDelegateFromCache( IsEnabledChangedKey, value );
            }
        }

        /// <summary>
        /// Event triggered on IsVisible property changes.
        /// </summary>
        public event PropertyChangedEventDelegate IsVisibleChanged
        {
            add
            {
                this.AddEventDelegateToCache( IsVisibleChangedKey, value );
            }
            remove
            {
                this.RemoveEventDelegateFromCache( IsVisibleChangedKey, value );
            }
        }

        /// <summary>
        /// Event triggered on IsHitTestVisible property changes.
        /// </summary>
        public event PropertyChangedEventDelegate IsHitTestVisibleChanged
        {
            add
            {
                this.AddEventDelegateToCache( IsHitTestVisibleChangedKey, value );
            }
            remove
            {
                this.RemoveEventDelegateFromCache( IsHitTestVisibleChangedKey, value );
            }
        }

        /// <summary>
        /// Event triggered on Focusable property changes.
        /// </summary>
        public event PropertyChangedEventDelegate FocusableChanged
        {
            add
            {
                this.AddEventDelegateToCache( FocusableChangedKey, value );
            }
            remove
            {
                this.RemoveEventDelegateFromCache( FocusableChangedKey, value );
            }
        }

        /// <summary>
        /// Event triggered on IsMouseCaptured property changed.
        /// </summary>
        public event PropertyChangedEventDelegate IsMouseCapturedChanged
        {
            add
            {
                this.AddEventDelegateToCache( IsMouseCapturedChangedKey, value );
            }
            remove
            {
                this.RemoveEventDelegateFromCache( IsMouseCapturedChangedKey, value );
            }
        }

        /// <summary>
        /// Event triggered on IsMouseCaptureWithin property changes.
        /// </summary>
        public event PropertyChangedEventDelegate IsMouseCaptureWithinChanged
        {
            add
            {
                this.AddEventDelegateToCache( IsMouseCaptureWithinChangedKey, value );
            }
            remove
            {
                this.RemoveEventDelegateFromCache( IsMouseCaptureWithinChangedKey, value );
            }
        }

        /// <summary>
        /// Event triggered on IsKeyboardFocusWithin property changes.
        /// </summary>
        public event PropertyChangedEventDelegate IsKeyboardFocusWithinChanged
        {
            add
            {
                this.AddEventDelegateToCache( IsKeyboardFocusWithinChangedKey, value );
            }
            remove
            {
                this.RemoveEventDelegateFromCache( IsKeyboardFocusWithinChangedKey, value );
            }
        }

        /// <summary>
        /// Event triggered automatically to notify different Element changes.
        /// </summary>
        public event AutomaticEventDelegate AutomaticEvent;

        /// <summary>
        /// Event triggered when the keyboard has the focus on that element.
        /// </summary>
        public event KeyboardFocusChangedEventDelegate GotKeyboardFocus
        {
            add
            {
                this.AddHandler( InputManager.GotKeyboardFocusEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.GotKeyboardFocusEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the element has the mouse in its region.
        /// </summary>
        public event MouseEventDelegate GotMouseCapture
        {
            add
            {
                this.AddHandler( InputManager.GotMouseCaptureEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.GotMouseCaptureEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the element has the keyboard focus and a key is pressed.
        /// </summary>
        public event KeyEventDelegate KeyDown
        {
            add
            {
                this.AddHandler( InputManager.KeyDownEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.KeyDownEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the element has the keyboard focus and a key is released.
        /// </summary>
        public event KeyEventDelegate KeyUp
        {
            add
            {
                this.AddHandler( InputManager.KeyUpEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.KeyUpEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the keyboard loses the focus on that element.
        /// </summary>
        public event KeyboardFocusChangedEventDelegate LostKeyboardFocus
        {
            add
            {
                this.AddHandler( InputManager.LostKeyboardFocusEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.LostKeyboardFocusEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the element has the mouse in its region no more.
        /// </summary>
        public event MouseEventDelegate LostMouseCapture
        {
            add
            {
                this.AddHandler( InputManager.LostMouseCaptureEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.LostMouseCaptureEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the mouse just entered the limit of that element.
        /// </summary>
        public event MouseEventDelegate MouseEnter
        {
            add
            {
                this.AddHandler( InputManager.MouseEnterEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.MouseEnterEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the mouse just exited the limit of that element.
        /// </summary>
        public event MouseEventDelegate MouseLeave
        {
            add
            {
                this.AddHandler( InputManager.MouseLeaveEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.MouseLeaveEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the mouse left button is pressed on that element.
        /// </summary>
        public event MouseButtonEventDelegate MouseLeftButtonDown
        {
            add
            {
                this.AddHandler( UIElement.MouseLeftButtonDownEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( UIElement.MouseLeftButtonDownEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the mouse left button is released on that element.
        /// </summary>
        public event MouseButtonEventDelegate MouseLeftButtonUp
        {
            add
            {
                this.AddHandler( UIElement.MouseLeftButtonUpEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( UIElement.MouseLeftButtonUpEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the mouse is moving on that element.
        /// </summary>
        public event MouseEventDelegate MouseMove
        {
            add
            {
                this.AddHandler( InputManager.MouseMoveEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.MouseMoveEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the mouse right button is pressed on that element.
        /// </summary>
        public event MouseButtonEventDelegate MouseRightButtonDown
        {
            add
            {
                this.AddHandler( UIElement.MouseRightButtonDownEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( UIElement.MouseRightButtonDownEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the mouse right button is released on that element.
        /// </summary>
        public event MouseButtonEventDelegate MouseRightButtonUp
        {
            add
            {
                this.AddHandler( UIElement.MouseRightButtonUpEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( UIElement.MouseRightButtonUpEvent, value );
            }
        }

        /// <summary>
        /// Event triggered when the mouse wheel is rolling on that element.
        /// </summary>
        public event MouseWheelEventDelegate MouseWheel
        {
            add
            {
                this.AddHandler( InputManager.MouseWheelEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.MouseWheelEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the keyboard has the focus on that element.
        /// </summary>
        public event KeyboardFocusChangedEventDelegate PreviewGotKeyboardFocus
        {
            add
            {
                this.AddHandler( InputManager.PreviewGotKeyboardFocusEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.PreviewGotKeyboardFocusEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the element has the keyboard focus and a key is pressed.
        /// </summary>
        public event KeyEventDelegate PreviewKeyDown
        {
            add
            {
                this.AddHandler( InputManager.PreviewKeyDownEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.PreviewKeyDownEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the element has the keyboard focus and a key is released.
        /// </summary>
        public event KeyEventDelegate PreviewKeyUp
        {
            add
            {
                this.AddHandler( InputManager.PreviewKeyUpEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.PreviewKeyUpEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the keyboard loses the focus on that element.
        /// </summary>
        public event KeyboardFocusChangedEventDelegate PreviewLostKeyboardFocus
        {
            add
            {
                this.AddHandler( InputManager.PreviewLostKeyboardFocusEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.PreviewLostKeyboardFocusEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the mouse left button is pressed on that element.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseLeftButtonDown
        {
            add
            {
                this.AddHandler( UIElement.PreviewMouseLeftButtonDownEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( UIElement.PreviewMouseLeftButtonDownEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the mouse left button is released on that element.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseLeftButtonUp
        {
            add
            {
                this.AddHandler( UIElement.PreviewMouseLeftButtonUpEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( UIElement.PreviewMouseLeftButtonUpEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the mouse is moving on that element.
        /// </summary>
        public event MouseEventDelegate PreviewMouseMove
        {
            add
            {
                this.AddHandler( InputManager.PreviewMouseMoveEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.PreviewMouseMoveEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the mouse right button is pressed on that element.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseRightButtonDown
        {
            add
            {
                this.AddHandler( UIElement.PreviewMouseRightButtonDownEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( UIElement.PreviewMouseRightButtonDownEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the mouse right button is released on that element.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseRightButtonUp
        {
            add
            {
                this.AddHandler( UIElement.PreviewMouseRightButtonUpEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( UIElement.PreviewMouseRightButtonUpEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the mouse wheel rolls on that element.
        /// </summary>
        public event MouseWheelEventDelegate PreviewMouseWheel
        {
            add
            {
                this.AddHandler( InputManager.PreviewMouseWheelEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.PreviewMouseWheelEvent, value );
            }
        }

        /// <summary>
        /// Event triggered on Mouse down.
        /// </summary>
        public event MouseButtonEventDelegate MouseDown
        {
            add
            {
                this.AddHandler( InputManager.MouseDownEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.MouseDownEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the on Mouse down.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseDown
        {
            add
            {
                this.AddHandler( InputManager.PreviewMouseDownEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.PreviewMouseDownEvent, value );
            }
        }

        /// <summary>
        /// Event triggered on Mouse up.
        /// </summary>
        public event MouseButtonEventDelegate MouseUp
        {
            add
            {
                this.AddHandler( InputManager.MouseUpEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.MouseUpEvent, value );
            }
        }

        /// <summary>
        /// Event triggered just before the on Mouse up.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseUp
        {
            add
            {
                this.AddHandler( InputManager.PreviewMouseUpEvent, value, false );
            }
            remove
            {
                this.RemoveHandler( InputManager.PreviewMouseUpEvent, value );
            }
        }

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the event delegates cache if any.
        /// </summary>
        internal EventDelegatesCache EventDelegatesCache
        {
            get
            {
                if
                    ( this.mHasEventDelegatesCache == false )
                {
                    return null;
                }

                return this.mEventDelegatesCache;
            }
        }

        /// <summary>
        /// Gets or sets the UI element transform used for rendering it.
        /// </summary>
        public ATransform RenderTransform
        {
            get
            {
                return this.mRenderTransform;
            }
            set
            {
                if
                    ( this.NotifyPropertyChanged( ref this.mRenderTransform, value, "RenderTransform" ) )
                {
                    // If never measured or arranged, no need to invalidate.
                    if
                        ( this.mIsNeverMeasured == false &&
                          this.mIsNeverArranged == false )
                    {
                        this.InvalidateArrange();
                        this.AreTransformsValid = false;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the UI element render transform origin point.
        /// </summary>
        public Point RenderTransformOrigin
        {
            get
            {
                return this.mRenderTransformOrigin;
            }
            set
            {
                if
                    ( IsRenderTransformOriginValid( value ) )
                {
                    if
                        ( this.NotifyPropertyChanged( ref this.mRenderTransformOrigin, value, "RenderTransformOrigin" ) )
                    {
                        // If never measured or arranged, no need to invalidate.
                        if
                            ( this.mIsNeverMeasured == false &&
                              this.mIsNeverArranged == false )
                        {
                            this.InvalidateArrange();
                            this.AreTransformsValid = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the transforms of that UI element and 
        /// its children are valid or not.
        /// </summary>
        internal bool AreTransformsValid
        {
            get
            {
                return this.mAreTransformsValid;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mAreTransformsValid, value, "AreTransformsValid" );
            }
        }

        /// <summary>
        /// Gets or sets the UI element visibility state.
        /// </summary>
        public Visibility Visibility
        {
            get
            {
                if 
                    ( this.CheckFlagsAnd( VisualFlags.VisibilityCache_Visible ) )
                {
                    return Visibility.Visible;
                }
                else if 
                    ( this.CheckFlagsAnd( VisualFlags.VisibilityCache_TakesSpace ) )
                {
                    return Visibility.Hidden;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            set
            {
                Debug.Assert( value == Visibility.Visible || 
                              value == Visibility.Hidden  || 
                              value == Visibility.Collapsed );

                switch 
                    ( value )
                {
                    case Visibility.Visible:
                        {
                            this.SetFlags( true, VisualFlags.VisibilityCache_Visible );
                            this.SetFlags( false, VisualFlags.VisibilityCache_TakesSpace );
                        }
                        break;
                    case Visibility.Hidden:
                        {
                            this.SetFlags( false, VisualFlags.VisibilityCache_Visible );
                            this.SetFlags( true, VisualFlags.VisibilityCache_TakesSpace );
                        }
                        break;
                    case Visibility.Collapsed:
                        {
                            this.SetFlags( false, VisualFlags.VisibilityCache_Visible );
                            this.SetFlags( false, VisualFlags.VisibilityCache_TakesSpace );
                        }
                        break;
                }

                this.InformPropertyChangedListener( value, value, "Visibility" );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the UI element is visible or not.
        /// </summary>
        public bool IsVisible
        {
            get
            {
                return this.mIsVisible;
            }
        }

        /// <summary>
        /// Gets the size the UI element computed during the last Measure pass.
        /// </summary>
        public Size DesiredSize
        {
            get
            {
                if 
                    ( this.Visibility == Visibility.Collapsed )
                {
                    return new Size( 0, 0 );
                }
                else
                {
                    return this.mDesiredSize;
                }
            }
        }

        /// <summary>
        /// Gets the UI element size used to render it.
        /// </summary>
        public Size RenderSize
        {
            get
            {
                if 
                    ( this.Visibility == Visibility.Collapsed )
                {
                    return new Size();
                }

                return this.mRenderSize;
            }
            set
            {
                if
                    ( this.NotifyPropertyChanged( ref this.mRenderSize, value, "RenderSize" ) )
                {
                    this.InvalidateHitTestBounds();
                }
            }
        }

        /// <summary>
        /// Gets the previous available size constraint.
        /// </summary>
        internal Size PreviousConstraint
        {
            get
            {
                return this.mPreviousAvailableSize;
            }
        }

        /// <summary>
        /// Gets the previous arrangement rectangle
        /// </summary>
        internal Rect PreviousArrangeRect
        {
            get
            {
                return this.mPreviousArrangeRectangle;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the UI element has to be measured.
        /// </summary>
        internal bool HasRequestedMeasurement
        {
            get
            {
                return this.mHasRequestedMeasurement;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mHasRequestedMeasurement, value, "HasRequestedMeasurement" );
            }
        }
        
        /// <summary>
        /// Gets or sets the flag indicating whether the UI element is being measured or not.
        /// </summary>
        internal bool IsMeasureInProgress
        {
            get
            {
                return this.mIsMeasureInProgress;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mIsMeasureInProgress, value, "IsMeasureInProgress" );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the UI element has a dirty measurement or not.
        /// </summary>
        internal bool IsMeasureDirty
        {
            get
            {
                return this.mIsMeasureDirty;
            }
            private set
            {
                this.NotifyPropertyChanged( ref this.mIsMeasureDirty, value, "IsMeasureDirty" );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the UI element has never been measured or not.
        /// </summary>
        internal bool IsNeverMeasured
        {
            get
            {
                return this.mIsNeverMeasured;
            }
            private set
            {
                this.NotifyPropertyChanged( ref this.mIsNeverMeasured, value, "IsNeverMeasured" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the UI element has to be arranged.
        /// </summary>
        internal bool HasRequestedArrangement
        {
            get
            {
                return this.mHasRequestedArrangement;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mHasRequestedArrangement, value, "HasRequestedArrangement" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the UI element is being arranged or not.
        /// </summary>
        internal bool IsArrangeInProgress
        {
            get
            {
                return this.mIsArrangeInProgress;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mIsArrangeInProgress, value, "IsArrangeInProgress" );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the UI element has a dirty arrangement or not.
        /// </summary>
        internal bool IsArrangeDirty
        {
            get
            {
                return this.mIsArrangeDirty;
            }
            private set
            {
                this.NotifyPropertyChanged( ref this.mIsArrangeDirty, value, "IsArrangeDirty" );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the UI element has never been arranged or not.
        /// </summary>
        internal bool IsNeverArranged
        {
            get
            {
                return this.mIsNeverArranged;
            }
            private set
            {
                this.NotifyPropertyChanged( ref this.mIsNeverArranged, value, "IsNeverArranged" );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the UI element is hit test visible or not.
        /// </summary>
        public bool IsHitTestVisible
        {
            get
            {
                return this.mIsHitTestVisible;
            }
            set
            {
                bool lOldValue = this.mIsHitTestVisible; // For OnIsEnabledChanged ONLY.
                if ( this.NotifyPropertyChanged( ref this.mIsHitTestVisible, value, "IsHitTestVisible" ) )
                {
                    OnIsHitTestVisibleChanged( this, new PropertyChangedEventArgs( lOldValue, value, "IsHitTestVisible" ) );
                }
            }
        }

        /// <summary>
        /// Internal IsHitTestVisible changed delegate notifying all registered listeners in cache.
        /// </summary>
        /// <param name="pSender">The element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnIsHitTestVisibleChanged(object pSender, PropertyChangedEventArgs pEventArgs)
        {
            UIElement lElement = pSender as UIElement;

            // Raise the public changed event.
            lElement.InternalNotifyPropertyChanged( IsHitTestVisibleChangedKey, pEventArgs );

            // Invalidate the children so that they will inherit the new value.
            InvalidateForceInheritPropertyOnChildren( lElement, pEventArgs.PropertyName );
            
            //InputManager.Instance.SafeCurrentNotifyHitTestInvalidated();
        }

        /// <summary>
        /// Coerces the IsHitTestVisible property value.
        /// </summary>
        /// <param name="pElement">The element it must be coerced for.</param>
        private static void CoerceIsHitTestVisible(DependencyObject pElement)
        {
            UIElement lElement = pElement as UIElement;
            if 
                ( lElement.mIsHitTestVisible )
            {
                DependencyObject lParent = InputElement.GetContainingUIElement( lElement.VisualParent );
                UIElement lParentElement = lParent as UIElement;
                if
                    ( lParentElement == null ||
                      lParentElement.mIsHitTestVisible )
                {
                    lElement.mIsHitTestVisible = true;
                }
                else
                {
                    lElement.mIsHitTestVisible = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the element can have the focus or not.
        /// </summary>
        public bool Focusable
        {
            get
            {
                return this.mFocusable;
            }
            set
            {
                bool lOldValue = this.mFocusable;
                if ( this.NotifyPropertyChanged( ref this.mFocusable, value, "Focusable" ) )
                {
                    OnFocusableChanged( this, new PropertyChangedEventArgs( lOldValue, value, "Focusable" ) );
                }
            }
        }

        /// <summary>
        /// Internal Focusable changed delegate notifying all registered listeners in cache.
        /// </summary>
        /// <param name="pSender">The element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnFocusableChanged(object pSender, PropertyChangedEventArgs pEventArgs)
        {
            UIElement lElement = pSender as UIElement;

            // Raise the public changed event.
            lElement.InternalNotifyPropertyChanged( FocusableChangedKey, pEventArgs );
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the element has been activated or not.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return this.mIsEnabled;
            }
            set
            {
                bool lOldValue = this.mIsEnabled; // For OnIsEnabledChanged ONLY.
                if ( this.NotifyPropertyChanged( ref this.mIsEnabled, value, "IsEnabled" ) )
                {
                    OnIsEnabledChanged( this, new PropertyChangedEventArgs( lOldValue, value, "IsEnabled" ) );
                }
            }
        }

        /// <summary>
        /// Internal IsEnabled changed delegate notifying all registered listeners in cache.
        /// </summary>
        /// <param name="pSender">The element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnIsEnabledChanged(object pSender, PropertyChangedEventArgs pEventArgs)
        {
            UIElement lElement = pSender as UIElement;

            // Raise the public changed event.
            lElement.InternalNotifyPropertyChanged( IsEnabledChangedKey, pEventArgs );

            // Invalidate the children so that they will inherit the new value.
            InvalidateForceInheritPropertyOnChildren( lElement, pEventArgs.PropertyName );
            
            //InputManager.Instance.SafeCurrentNotifyHitTestInvalidated();
        }

        /// <summary>
        /// Coerces the IsEnabled property value.
        /// </summary>
        /// <param name="pElement">The element it must be coerced for.</param>
        private static void CoerceIsEnabled(DependencyObject pElement)
        {
            UIElement lElement = pElement as UIElement;
            if 
                ( lElement.mIsEnabled )
            {
                DependencyObject lParent = lElement.CustomGetUIParent() as ContentElement;
                if 
                    ( lParent == null )
                {
                    lParent = InputElement.GetContainingUIElement( lElement.VisualParent );
                }

                UIElement lParentElement = lParent as UIElement;
                if 
                    ( lParentElement == null ||
                      lParentElement.mIsEnabled )
                {
                    lElement.mIsEnabled = lElement.CustomIsEnabled;
                }
                else
                {
                    lElement.mIsEnabled = false;
                }
            }
        }

        /// <summary>
        /// Allow to give a specific behavior on IsEnabled property changes.
        /// see CoerceIsEnabled.
        /// </summary>
        protected virtual bool CustomIsEnabled
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the element has the keyboard focus or not.
        /// </summary>
        public bool IsKeyboardFocused
        {
            get
            {
                return this.mIsKeyboardFocused;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the keyboard focus is whithin the element or not.
        /// </summary>
        public bool IsKeyboardFocusWithin
        {
            get
            {
                return this.mIsKeyboardFocusWithin;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse is entered in the element bounds or not.
        /// </summary>
        public bool IsMouseCaptured
        {
            get
            {
                return this.mIsMouseCaptured;
            }
            internal set
            {
                bool lOldValue = this.mIsMouseCaptured;
                if
                    ( this.NotifyPropertyChanged( ref this.mIsMouseCaptured, value, "IsMouseCaptured" ) )
                {
                    this.RaiseIsMouseCapturedChanged( new PropertyChangedEventArgs( lOldValue , value, "IsMouseCaptured" ) );
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse has that element as Top most regarding
        /// to those in the region the mouse is in.
        /// </summary>
        public bool IsMouseDirectlyOver
        {
            get
            {
                return this.mIsMouseDirectlyOver;
            }
            internal set
            {
                bool lOldValue = this.mIsMouseDirectlyOver; // For OnIsEnabledChanged ONLY.
                if
                    ( this.NotifyPropertyChanged( ref this.mIsMouseDirectlyOver, value, "IsMouseDirectlyOver" ) )
                {
                    OnIsMouseDirectlyOverChanged( this, new PropertyChangedEventArgs( lOldValue, value, "IsMouseDirectlyOver" ) );
                }
            }
        }

        /// <summary>
        /// Internal IsMouseDirectlyOver changed delegate notifying all registered listeners in cache.
        /// </summary>
        /// <param name="pSender">The element.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnIsMouseDirectlyOverChanged(object pSender, PropertyChangedEventArgs pEventArgs)
        {
            UIElement lElement = pSender as UIElement;

            // Process the overridable method first as pre process.
            lElement.OnIsMouseDirectlyOverChanged( pEventArgs );

            // Raise the public changed event.
            lElement.InternalNotifyPropertyChanged( IsMouseDirectlyOverChangedKey, pEventArgs );
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse is on that element whatever it is a child
        /// or the top most of those in the region the mouse is in.
        /// </summary>
        public bool IsMouseOver
        {
            get
            {
                return this.mIsMouseOver;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse captured within that element or not.
        /// </summary>
        internal bool IsMouseCaptureWithin
        {
            get
            {
                return this.mIsMouseCaptureWithin;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="UIElement"/> class.
        /// </summary>
        static UIElement()
        {
            sCoercers = new Dictionary<string, Action<DependencyObject>>();
            sCoercers.Add( "IsEnabled", CoerceIsEnabled );
            sCoercers.Add( "IsHitTestVisible", CoerceIsHitTestVisible );
            
            // Register this type of elemnt to the relevant routed events.
            RoutedEvent.AddOwner( InputManager.PreviewMouseDownEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.MouseDownEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.PreviewMouseUpEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.MouseUpEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.PreviewMouseMoveEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.MouseMoveEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.PreviewMouseWheelEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.MouseWheelEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.MouseEnterEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.MouseLeaveEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.GotMouseCaptureEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.LostMouseCaptureEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.PreviewKeyDownEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.KeyDownEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.PreviewKeyUpEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.KeyUpEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.PreviewGotKeyboardFocusEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.GotKeyboardFocusEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.PreviewLostKeyboardFocusEvent, sThisType );
            RoutedEvent.AddOwner( InputManager.LostKeyboardFocusEvent, sThisType );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIElement"/> class.
        /// </summary>
        public UIElement()
        {
            this.mDrawingContent = null;
            this.mIsKeyboardFocused = false;
            this.mIsKeyboardFocusWithin = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Updates the UI element visibility.
        /// </summary>
        internal void UpdateIsVisibleCache()
        {
            bool lIsVisible = this.Visibility == Visibility.Visible;

            // We must be false if our parent is false, but we can be
            // either true or false if our parent is true.
            if 
                ( lIsVisible )
            {
                bool lParentVisibility = false;

                // Our lParent can constrain us.
                UIElement lParent = this.VisualParent as UIElement;
                if 
                    ( lParent != null )
                {
                    lParentVisibility = lParent.IsVisible;
                }
                else
                {
                    // Else it could be the tree root.
                    IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
                    if 
                        ( lUIService.PresentationSource != null &&
                          lUIService.PresentationSource.RootVisual == this )
                    {
                        lParentVisibility = true;
                    }
                }

                if 
                    ( lParentVisibility == false )
                {
                    lIsVisible = false;
                }
            }

            this.NotifyPropertyChanged( ref this.mIsVisible, lIsVisible, "IsVisible" );
        }

        /// <summary>
        /// Recursively propagates IsLayoutSuspended flag down to the whole visual's sub tree.
        /// </summary>
        /// <param name="pVisual">The visual that require to suspend the layout.</param>
        internal static void PropagateSuspendLayout(AVisual pVisual)
        {
            if 
                ( pVisual.CheckFlagsAnd( VisualFlags.IsLayoutIslandRoot ) )
            {
                return;
            }

            //the subtree is already suspended - happens when already suspended tree is further disassembled
            //no need to walk down in this case
            if 
                ( pVisual.CheckFlagsAnd( VisualFlags.IsLayoutSuspended ) )
            {
                return;
            }

            //  (bug # 1623922) assert that a UIElement has not being
            //  removed from the visual tree while updating layout.
            if 
                ( pVisual.CheckFlagsAnd( VisualFlags.IsUIElement ) )
            {
                UIElement lElement = pVisual as UIElement;
                if( lElement.mIsMeasureInProgress &&
                    lElement.mIsArrangeInProgress )
                {
                    return;
                }
            }

            pVisual.SetFlags( true, VisualFlags.IsLayoutSuspended );

            int lCount = pVisual.VisualChildrenCount;
            for 
                ( int lCurr = 0; lCurr < lCount; lCurr++ )
            {
                AVisual lChild = pVisual.GetVisualChild( lCurr );
                if 
                    ( lChild != null )
                {
                    PropagateSuspendLayout( lChild );
                }
            }
        }

        /// <summary>
        /// Recursively resets IsLayoutSuspended flag on all visuals of the whole v's sub tree.
        /// For UIElements also re-inserts the UIElement into Measure and / or Arrange update queues
        /// if necessary.
        /// </summary>
        /// <param name="pParent">The visual parent</param>
        /// <param name="pVisual">The visual that required the layout resume.</param>
        internal static void PropagateResumeLayout(AVisual pParent, AVisual pVisual)
        {
            if 
                ( pVisual.CheckFlagsAnd( VisualFlags.IsLayoutIslandRoot ) )
            {
                return;
            }

            bool lIsParentIsSuspended = false;
            if
                ( pParent != null )
            {
                lIsParentIsSuspended = pParent.CheckFlagsAnd( VisualFlags.IsLayoutSuspended );
            }

            if 
                ( lIsParentIsSuspended )
            {
                return;
            }

            pVisual.SetFlags( false, VisualFlags.IsLayoutSuspended );
            if 
                ( pVisual.CheckFlagsAnd( VisualFlags.IsUIElement ) )
            {
                UIElement lElement = pVisual as UIElement;
                if
                    ( lElement.mIsMeasureInProgress && 
                      lElement.mIsArrangeInProgress )
                {
                    return;
                }
                
                bool lRequireMeasureUpdate = lElement.mIsMeasureDirty && lElement.mIsNeverMeasured == false && lElement.mHasRequestedMeasurement == false;
                bool lRequireArrangeUpdate = lElement.mIsArrangeDirty && lElement.mIsNeverArranged == false && lElement.mHasRequestedArrangement == false;

                UIService lUIService = ServiceManager.Instance.GetService<UIService>();
                if 
                    ( lRequireMeasureUpdate )
                {
                    lUIService.RequestMeasurement( lElement );
                }

                if 
                    ( lRequireArrangeUpdate )
                {
                    lUIService.RequestArrangement( lElement );
                }
            }

            int lCount = pVisual.VisualChildrenCount;
            for
                ( int lCurr = 0; lCurr < lCount; lCurr++ )
            {
                AVisual lChild = pVisual.GetVisualChild( lCurr );
                if 
                    ( lChild != null )
                {
                    PropagateResumeLayout( pVisual, lChild );
                }
            }
        }

        /// <summary>
        /// Updates the desired size of the UI element. 
        /// It is called by parents to form a recursive update and is first pass of layout update.
        /// </summary>
        /// <param name="pAvailableSize">The available size that parent can give to the child.</param>
        public void Measure(Size pAvailableSize)
        {
            UIService lUIService = ServiceManager.Instance.GetService<UIService>();
            try
            {
                //enforce that Measure can not receive NaN size .
                if 
                    ( DoubleUtil.IsNaN( pAvailableSize.Width ) || 
                      DoubleUtil.IsNaN( pAvailableSize.Height ) )
                {
                    throw new InvalidOperationException( "Layout measure produced a NaN value!!!" );
                }
                    
                bool lNeverMeasured = this.mIsNeverMeasured;
                if 
                    ( lNeverMeasured )
                {
                    this.InvalidateVisibility( this.Visibility );
                }

                bool lIsCloseToPreviousMeasure = DoubleUtil.AreClose( pAvailableSize, this.mPreviousAvailableSize );
                if 
                    ( this.Visibility == Visibility.Collapsed || 
                      this.CheckFlagsAnd( VisualFlags.IsLayoutSuspended ) )
                {
                    // Remove the measure request if any as it is needed no more.
                    if 
                        ( this.mHasRequestedMeasurement )
                    {
                        lUIService.StopMeasurement( this );
                    }
                    
                    if 
                        ( lIsCloseToPreviousMeasure == false )
                    {
                        this.mIsMeasureDirty = true;
                        this.mPreviousAvailableSize = pAvailableSize;
                    }

                    return;
                }
                
                // Avoid useless computation.
                if 
                    ( this.mIsMeasureDirty == false && 
                      lNeverMeasured == false && 
                      lIsCloseToPreviousMeasure )
                {
                    return;
                }

                this.mIsNeverMeasured = false;
                Size lPreviousSize = this.mDesiredSize;

                InvalidateArrange();
                
                // Set the measurement flag.
                this.mIsMeasureInProgress = true;

                Size lDesiredSize = new Size( 0, 0 );

                lUIService.PreMeasure();

                try
                {
                    lDesiredSize = this.CustomMeasure( pAvailableSize );
                }
                catch
                    ( Exception pEx )
                {
                    LogManager.Instance.Log( pEx );
                }

                // Reset the measurement flag.
                this.mIsMeasureInProgress = false;

                this.mPreviousAvailableSize = pAvailableSize;

                lUIService.PostMeasure();

                if 
                    ( double.IsPositiveInfinity( lDesiredSize.Width ) || 
                      double.IsPositiveInfinity( lDesiredSize.Height ) )
                {
                    throw new InvalidOperationException( "Infinity is not a valid layout size!!!" );
                }

                if 
                    ( DoubleUtil.IsNaN( lDesiredSize.Width ) || 
                      DoubleUtil.IsNaN( lDesiredSize.Height ) )
                {
                    throw new InvalidOperationException( "NaN layout measurement produced!!!" );
                }
                
                // Job done...
                this.mIsMeasureDirty = false;
                
                if 
                    ( this.mHasRequestedMeasurement )
                {
                    lUIService.StopMeasurement( this );
                }

                // Store the desired size
                this.mDesiredSize = lDesiredSize;

                // Informs the parent (if any) that the size has changed.
                if 
                    ( this.mIsMeasuringDuringArrangement == false && 
                      DoubleUtil.AreClose( lPreviousSize, lDesiredSize ) == false )
                {
                    UIElement lParent;
                    this.GetUIParent( out lParent );
                    if 
                        ( lParent != null && 
                          lParent.mIsMeasureInProgress == false )
                    {
                        lParent.OnChildDesiredSizeChanged( this );
                    }
                }
            }
            catch
                ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }
        }

        /// <summary>
        /// Arrange the UI element for the given area.
        /// </summary>
        /// <param name="pFinalRectangle">The final rectangle in which arrange the UI element.</param>
        public void Arrange(Rect pFinalRectangle)
        {
            try
            {
                UIService lUIService = ServiceManager.Instance.GetService<UIService>();

                // Assures that arrange cannot end with Infinity size or NaN
                if 
                    ( double.IsPositiveInfinity( pFinalRectangle.Width ) || 
                      double.IsPositiveInfinity( pFinalRectangle.Height ) || 
                      DoubleUtil.IsNaN( pFinalRectangle.Width ) || 
                      DoubleUtil.IsNaN( pFinalRectangle.Height ) )
                {
                    UIElement lParent = this.GetUIParent() as UIElement;
                    throw new InvalidOperationException( string.Format( "Infinity arrange occured between the {0} parent and its {1} child!!!", lParent == null ? "" : lParent.GetType().FullName, this.GetType().FullName ) );
                }

                // If Collapsed, it should not be arranged so
                // dirty bit remains set but it must stop requesting arrangement.
                if 
                    ( this.Visibility == Visibility.Collapsed || 
                      this.CheckFlagsAnd( VisualFlags.IsLayoutSuspended ) )
                {
                    // Stop requesting arrangement.
                    if 
                        ( this.HasRequestedArrangement )
                    {
                        lUIService.StopArrangement( this );
                    }
                    
                    // Keep a track of that 
                    this.mPreviousArrangeRectangle = pFinalRectangle;

                    return;
                }

                // If the parent did not call Measure on a child, call it now.
                if 
                    ( this.mIsMeasureDirty || 
                      this.mIsNeverMeasured )
                {
                    try
                    {
                        this.mIsMeasuringDuringArrangement = true;
                        if 
                            ( this.mIsNeverMeasured )
                        {
                            this.Measure( pFinalRectangle.Size );
                        }
                        else
                        {
                            this.Measure( this.PreviousConstraint );
                        }
                    }
                    finally
                    {
                        this.mIsMeasuringDuringArrangement = false;
                    }
                }

                // No need to re-arrange if the UI element is cleaned
                if 
                    ( this.mIsArrangeDirty || 
                      this.mIsNeverArranged || 
                      DoubleUtil.AreClose( pFinalRectangle, this.mPreviousArrangeRectangle ) == false )
                {
                    bool lIsFirstArrangement = this.mIsNeverArranged;
                    this.mIsNeverArranged = false;
                    this.mIsArrangeInProgress = true;

                    Size lOldSize = this.RenderSize;
                    bool lIsSizeChanged = false;

                    // If using layout rounding, round final size before calling ArrangeCore.
                    if 
                        ( this.CheckFlagsAnd( VisualFlags.UseLayoutRounding ) )
                    {
                        // No Dpi info for now.
                        //pFinalRectangle = RoundLayoutRect(pFinalRectangle, _dpiScaleX, _dpiScaleY);
                    }

                    try
                    {
                        lUIService.PreArrange();

                        // Updates the RenderSize according to the final region in which the 
                        // UI element must be rendered.
                        this.CustomArrange( pFinalRectangle );

                        // No clipping for now...
                        //EnsureClip(pFinalRectangle.Size);

                        //see if we need to call OnRenderSizeChanged on this element
                        lIsSizeChanged = this.CheckSizeChanged( lOldSize, this.RenderSize );
                    }
                    catch
                        ( Exception pEx )
                    {
                        LogManager.Instance.Log( pEx );
                    }

                    this.mIsArrangeInProgress = false;

                    lUIService.PostArrange();
                    
                    this.mPreviousArrangeRectangle = pFinalRectangle;

                    this.mIsArrangeDirty = false;

                    // Stop requesting arrangement.
                    if
                        ( this.mHasRequestedArrangement )
                    {
                        lUIService.StopArrangement( this );
                    }
                    
                    if 
                        ( (lIsSizeChanged || this.mIsRenderDirty || lIsFirstArrangement) && 
                          this.IsRenderable() )
                    {
                        ADrawingContext lContext = this.RenderOpen();
                        try
                        {
                            try
                            {
                                this.OnRender( lContext );
                            }
                            catch
                                ( Exception pEx )
                            {
                                LogManager.Instance.Log( pEx );
                            }
                        }
                        finally
                        {
                            lContext.Close();
                            this.mIsRenderDirty = false;
                        }
                    }
                }
            }
            catch
                ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }
        }

        /// <summary>
        /// Creates a drawing context used to render the element content.
        /// </summary>
        /// <returns></returns>
        internal ADrawingContext RenderOpen()
        {
            return new VisualDrawingContext( this );
        }

        /// <summary>
        /// Gets the visual's bounds for Hit test purpose.
        /// </summary>
        /// <returns>The rectangle being the bounds.</returns>
        internal override Rect GetHitTestBounds()
        {
            Rect lBounds = new Rect( this.mRenderSize );
            if 
                ( this.mDrawingContent != null )
            {
                BoundsDrawingContext lContext = new BoundsDrawingContext();

                Rect lResult = this.mDrawingContent.GetContentBounds( lContext );
                
                lBounds.Union( lResult );
            }

            return lBounds;
        }

        /// <summary>
        /// Gets the UI parent of this UI element when there is no visual parent.
        /// </summary>
        /// <returns>The UI parent.</returns>
        protected virtual internal DependencyObject CustomGetUIParent()
        {
            // To override...
            return null;
        }

        /// <summary>
        /// Retrieves the parent UI element
        /// </summary>
        /// <param name="pParent">The resulting parent UI element of that UI element.</param>
        /// <returns>True if parent found, false otherwise.</returns>
        internal bool GetUIParent(out UIElement pParent)
        {
            pParent = null;
            bool lNotFound   = true;
            AVisual lCurrent = this.VisualParent as AVisual;
            while 
                ( lCurrent != null &&
                  lNotFound )
            {
                if 
                    ( lCurrent.CheckFlagsAnd( VisualFlags.IsUIElement ) )
                {
                    pParent   = lCurrent as UIElement;
                    lNotFound = false;
                }

                lCurrent = lCurrent.VisualParent as AVisual;
            }

            return lNotFound == false;
        }

        /// <summary>
        /// Gets the UI element parent.
        /// </summary>
        /// <returns>The UI element's parent, null otherwise.</returns>
        internal DependencyObject GetUIParent()
        {
            return GetUIParent( this, false );
        }

        /// <summary>
        /// Gets the UI element parent.
        /// </summary>
        /// <param name="pKeepGoingPastVisualTree">The flag indicating whether the parent search must keep going through the logical tree if nothing found in the visual one.</param>
        /// <returns>The UI element's parent, null otherwise.</returns>
        internal DependencyObject GetUIParent(bool pKeepGoingPastVisualTree)
        {
            return GetUIParent( this, pKeepGoingPastVisualTree );
        }

        /// <summary>
        /// Gets the UI element parent of the given child.
        /// </summary>
        /// <param name="pChild">THe child the parent must be found for.</param>
        /// <param name="pKeepGoingPastVisualTree">The flag indicating whether the parent search must keep going through the logical tree if nothing found in the visual one.</param>
        /// <returns>The UI element's parent, null otherwise.</returns>
        internal static DependencyObject GetUIParent(DependencyObject pChild, bool pKeepGoingPastVisualTree)
        {
            DependencyObject lParent = null;
            DependencyObject lChildParentParent = null;

            // Try to find a UIElement parent in the visual ancestry.
            if 
                ( pChild is AVisual )
            {
                AVisual lChild = pChild as AVisual;
                lChildParentParent = lChild.VisualParent;
            }

            lParent = InputElement.GetContainingUIElement( lChildParentParent ) as DependencyObject;

            // If there was no UIElement parent in the visual ancestry,
            // check along the logical branch.
            if 
                ( lParent == null && 
                  pKeepGoingPastVisualTree )
            {
                UIElement lChildAsUIElement = pChild as UIElement;
                if 
                    ( lChildAsUIElement != null )
                {
                    lParent = InputElement.GetContainingInputElement( lChildAsUIElement.CustomGetUIParent() ) as DependencyObject;
                }
            }

            return lParent;
        }

        /// <summary>
        /// Looks for the parent UIElement within Element Layout Island, 
        /// and stops once the island's root is found
        /// </summary>
        /// <returns>The Found UI parent, Null otherwise.</returns>
        internal UIElement GetUIParentWithinLayoutIsland()
        {
            UIElement lParent = null;
            bool lNotFound    = true;
            AVisual lCurrent  = this.VisualParent as AVisual;
            while 
                ( lCurrent != null &&
                  lNotFound )
            {
                if 
                    ( lCurrent.CheckFlagsAnd( VisualFlags.IsLayoutIslandRoot ) )
                {
                    lNotFound = false;
                }

                if 
                    ( lCurrent.CheckFlagsAnd( VisualFlags.IsUIElement ) )
                {
                    lParent   = lCurrent as UIElement;
                    lNotFound = false;
                }

                lCurrent = lCurrent.VisualParent as AVisual;
            }

            return lParent;
        }

        /// <summary>
        /// Invalidates the measurement state for the element.
        /// This has the effect of also invalidating the arrangement state for the element.
        /// The element will be queued for an update layout that will occur asynchronously.
        /// </summary>
        public void InvalidateMeasure()
        {
            if 
                ( this.mIsMeasureDirty == false && 
                  this.mIsMeasureInProgress == false )
            {
                Debug.Assert( this.mHasRequestedMeasurement == false,
                              "Cannot have already an Measurement request!!!" );
                
                if 
                    ( this.mIsNeverMeasured == false )
                {
                    UIService lUIService = ServiceManager.Instance.GetService<UIService>();
                    lUIService.RequestMeasurement( this );
                }
                this.mIsMeasureDirty = true;
            }
        }

        /// <summary>
        /// Invalidates the arrangement state for the element.
        /// The element will be queued for an update layout that will occur asynchronously.
        /// MeasureCore will not be called unless InvalidateMeasure is also called - or that something
        /// else caused the measure state to be invalidated.
        /// </summary>
        public void InvalidateArrange()
        {
            if 
                ( this.mIsArrangeDirty == false && 
                  this.mIsArrangeInProgress == false )
            {
                Debug.Assert( this.mHasRequestedArrangement == false, 
                              "Cannot have already an Arrangement request!!!");
                
                if
                    ( this.mIsNeverArranged == false )
                {
                    UIService lUIService = ServiceManager.Instance.GetService<UIService>();
                    lUIService.RequestArrangement( this );
                }
                
                this.mIsArrangeDirty = true;
            }
        }

        /// <summary>
        /// Invalidates the rendering of the element.
        /// </summary>
        public void InvalidateVisual()
        {
            this.InvalidateArrange();
            this.mIsRenderDirty = true;
        }

        /// <summary>
        /// Returns the deepest enabled input element within this element that is
        /// at the specified coordinates relative to this element.
        /// </summary>
        public IInputElement InputHitTest(Point pPoint)
        {
            IInputElement lDeepestEnabled;
            IInputElement lDeepest;
            this.InputHitTest( pPoint, out lDeepestEnabled, out lDeepest );

            return lDeepestEnabled;
        }

        /// <summary>
        /// Returns the deepest input elements within this element that is
        /// at the specified coordinates relative to this element.
        /// </summary>
        /// <param name="pPoint">This is the coordinate, relative to this element, at which to look for elements within this one.</param>
        /// <param name="pDeepestEnabled">The deepest enabled input element that is at the specified coordinates.</param>
        /// <param name="pDeepest">The deepest input element (which could be disabled) that is at the specified coordinates.</param>
        internal void InputHitTest(Point pPoint, out IInputElement pDeepestEnabled, out IInputElement pDeepest)
        {
            AHitTestResult lResult;
            this.InputHitTest( pPoint, out pDeepestEnabled, out pDeepest, out lResult );
        }

        /// <summary>
        /// Returns the deepest input element within this element that is
        /// at the specified coordinates relative to this element.
        /// </summary>
        /// <param name="pPoint">This is the coordinate, relative to this element, at which to look for elements within this one.</param>
        /// <param name="pDeepestEnabled">The deepest enabled input element that is at the specified coordinates.</param>
        /// <param name="pDeepest">The deepest input element (which could be disabled) that is at the specified coordinates.</param>
        /// <param name="pResult">The hit test result owning the deepest UI element.</param>
        internal void InputHitTest(Point pPoint, out IInputElement pDeepestEnabled, out IInputElement pDeepest, out AHitTestResult pResult)
        {
            PointHitTestParameters lHitTestParameters = new PointHitTestParameters( pPoint );

            // We store the result of the hit testing. 
            // The HitTestResultCallback is an instance method on this class
            // so that it can store the lElement which is hit.
            InputHitTestResult lResult = new InputHitTestResult();
            this.HitTest( new HitTestFilterCallback( this.InputHitTestFilterCallback ),
                          new HitTestResultCallback( lResult.InputHitTestResultCallback ),
                          lHitTestParameters );

            DependencyObject lCandidate = lResult.Result;
            pDeepest = lCandidate as IInputElement;
            pResult = lResult.HitTestResult;
            pDeepestEnabled = null;
            while 
                ( lCandidate != null )
            {
                UIElement lElement = lCandidate as UIElement;
                if 
                    ( lElement != null )
                {
                    if 
                        ( pDeepest == null )
                    {
                        // Earlier we hit a non-IInputElement. This is the first one
                        // we've found, so use that as pDeepest.
                        pDeepest = lElement;
                        pResult  = null;
                    }

                    if 
                        ( lElement.IsEnabled )
                    {
                        pDeepestEnabled = lElement;
                        break;
                    }
                }
                
                if 
                    ( lCandidate == this )
                {
                    // Nothing found!!!
                    return;
                }

                AVisual lCandidateCast = lCandidate as AVisual;
                if
                    ( lCandidateCast != null )
                {
                    lCandidate = lCandidateCast.VisualParent;
                }
                else
                {
                    lCandidate = null;
                }
            }
        }

        /// <summary>
        /// Delegate used to filter node of the visual tree dependeing on their type and state.
        /// </summary>
        /// <param name="pNode">The node to test for whether it must be filtered or not.</param>
        /// <returns>How the callback user must behave on the supplied node.</returns>
        private HitTestFilterBehavior InputHitTestFilterCallback(DependencyObject pNode)
        {
            HitTestFilterBehavior lBehavior = HitTestFilterBehavior.Continue;
            if 
                ( pNode is UIElement )
            {
                UIElement lNode = pNode as UIElement;
                if 
                    ( lNode.IsVisible == false )
                {
                    // The element which is currently processing is not visible,
                    // so it does not allow hit testing to continue down this
                    // subtree.
                    lBehavior = HitTestFilterBehavior.ContinueSkipSelfAndChildren;
                }

                if 
                    ( lNode.IsHitTestVisible == false )
                {
                    // The element which is currently processing is not visible for hit testing,
                    // so it does not allow hit testing to continue down this
                    // subtree.
                    lBehavior = HitTestFilterBehavior.ContinueSkipSelfAndChildren;
                }
            }
            else
            {
                // It is a AVisual only that cannot receive inputs,
                // so allow the hit testing to continue through this visual.
                lBehavior = HitTestFilterBehavior.Continue;
            }

            return lBehavior;
        }

        /// <summary>
        /// Custom measurement of the UI element provided by sub classes.
        /// </summary>
        /// <param name="pAvailableSize">The available size that parent can give to the child.</param>
        /// <returns>The requested UI element size.</returns>
        protected virtual Size CustomMeasure(Size pAvailableSize)
        {
            // Override it.
            return new Size( 0, 0 );
        }

        /// <summary>
        /// Custom arrangement of the UI element provided by sub classes.
        /// </summary>
        /// <param name="pFinalRectangle"></param>
        protected virtual void CustomArrange(Rect pFinalRectangle)
        {
            // Set the element size.
            this.RenderSize = pFinalRectangle.Size;

            // Set transform to reflect the offset of pFinalRectangle.
            // Parents that have multiple children pass offset in the pFinalRectangle to communicate
            // the location of this child withing the parent.
            ATransform lRenderTransform = this.RenderTransform;
            if 
                ( lRenderTransform == ATransform.Identity )
            {
                lRenderTransform = null;
            }
            
            Vector2d lOldOffset = this.VisualOffset;
            if 
                ( DoubleUtil.AreClose( lOldOffset.X, pFinalRectangle.X ) == false ||
                  DoubleUtil.AreClose( lOldOffset.Y, pFinalRectangle.Y ) == false )
            {
                this.VisualOffset = new Vector2d( pFinalRectangle.X, pFinalRectangle.Y );
            }

            if 
                ( lRenderTransform != null )
            {
                //render transform + layout offset, create a collection
                TransformGroup lGroup = new TransformGroup();

                Point lOrigin   = this.RenderTransformOrigin;
                bool lHasOrigin = lOrigin.X != 0d || lOrigin.Y != 0d;
                if 
                    ( lHasOrigin )
                {
                    lGroup.Transformations.Add( new TranslateTransform( -(pFinalRectangle.Width * lOrigin.X), 
                                                                        -(pFinalRectangle.Height * lOrigin.Y) ) );
                }
                
                lGroup.Transformations.Add( lRenderTransform );

                if 
                    ( lHasOrigin )
                {
                    lGroup.Transformations.Add( new TranslateTransform( pFinalRectangle.Width * lOrigin.X,
                                                                        pFinalRectangle.Height * lOrigin.Y ) );
                }
                
                this.VisualTransform = lGroup;
            }
            else
            {
                this.VisualTransform = null;
            }
        }

        /// <summary>
        /// Invalidates the UI element visibility by passing the new one.
        /// </summary>
        /// <param name="pVisibility">The new visibility.</param>
        private void InvalidateVisibility(Visibility pVisibility)
        {
            if
                ( pVisibility == Visibility.Visible )
            {
                this.MakeVisible();
                return;
            }

            this.MakeInvisible( pVisibility == Visibility.Collapsed ? true : false );
        }

        /// <summary>
        /// Makes the UI element visible.
        /// </summary>
        private void MakeVisible()
        {
            if 
                ( this.mIsOpacityCutoff )
            {
                // Reset the Opacity before it be cut off.
                this.VisualOpacity = this.mOpacityBeforeCutoff;

                if 
                    ( this.mIsCollapsed )
                {
                    this.mIsCollapsed = false;

                    // Informs the parent.
                    this.NotifyDesiredSizeChange();

                    // Request a visual update as visibility has changed from collapsed.
                    this.InvalidateVisual();
                }

                this.mIsOpacityCutoff = false;
            }
        }

        /// <summary>
        /// Makes the UI element invisible.
        /// </summary>
        /// <param name="pIsCollapsed">The flag indicating whether the UI element is collapsed or not.</param>
        private void MakeInvisible(bool pIsCollapsed)
        {
            if 
                ( this.mIsOpacityCutoff == false )
            {
                this.mOpacityBeforeCutoff = this.VisualOpacity;
                this.VisualOpacity = 0;
                this.mIsOpacityCutoff = true;
            }

            // Manages the switch from Hidden to Collapsed.
            if 
                ( this.mIsCollapsed == false && 
                  pIsCollapsed)
            {
                this.mIsCollapsed = true;

                this.NotifyDesiredSizeChange();
            }
            // Manages the switch from Collapsed to Hidden.
            else if 
                ( this.mIsCollapsed && 
                  pIsCollapsed == false )
            {
                this.mIsCollapsed = false;
                
                this.NotifyDesiredSizeChange();
            }
        }

        /// <summary>
        /// Notifies the parent that the desired UI element size has changed.
        /// </summary>
        private void NotifyDesiredSizeChange()
        {
            UIElement lParent;
            this.GetUIParent( out lParent );
            if
                ( lParent != null )
            {
                lParent.OnChildDesiredSizeChanged( this );
            }
        }

        /// <summary>
        /// Delegate called by Measure of a child when
        /// it ends up with different desired size for the child.
        /// </summary>
        protected virtual void OnChildDesiredSizeChanged(UIElement pChild)
        {
            if 
                ( this.mIsMeasureDirty == false )
            {
                this.InvalidateMeasure();
            }
        }

        /// <summary>
        /// Internal delegate called when the parent has changed. 
        /// </summary>
        /// <param name="pOldParent">The old parent or null if the AVisual did not have a parent before.</param>
        protected internal override void OnVisualParentChanged(DependencyObject pOldParent)
        {
            // Synchronize ForceInherit properties
            if 
                ( this.VisualParent != null )
            {
                DependencyObject lParent = this.VisualParent;
                if 
                    ( InputElement.IsUIElement( lParent ) == false )
                {
                    AVisual lParentCast = lParent as AVisual;
                    if 
                        ( lParentCast != null )
                    {
                        // It is being plugged into a non-UIElement visual. This
                        // means that our lParent doesn't play by the same rules we
                        // do, so we need to track changes to our ancestors in
                        // order to bridge the gap.
                        lParentCast.VisualAncestorChanged += this.OnVisualAncestorChanged_ForceInherit;

                        // Try to find an UIElement ancestor to use for coersion.
                        lParent = InputElement.GetContainingUIElement( lParentCast );
                    }
                }

                if 
                    ( lParent != null )
                {
                    SynchronizeForceInheritProperties( this, lParent );
                }
            }
            else
            {
                DependencyObject lParent = pOldParent;
                if 
                    ( InputElement.IsUIElement( lParent ) == false )
                {
                    // It is being unplugged from a non-UIElement visual. This
                    // means that our parent did not play by the same rules that
                    // one does, so start to track changes to the ancestors in
                    // order to bridge the gap.
                    if 
                        ( pOldParent is AVisual )
                    {
                        (pOldParent as AVisual).VisualAncestorChanged -= this.OnVisualAncestorChanged_ForceInherit;
                    }

                    // Try to find a UIElement ancestor to use for coersion.
                    lParent = InputElement.GetContainingUIElement( pOldParent );
                }

                if 
                    ( lParent != null )
                {
                    SynchronizeForceInheritProperties( this, lParent );
                }
            }

            // TO DO... Check if useless.
            this.SynchronizeReverseInheritPropertyFlags( pOldParent, true );
        }

        /// <summary>
        /// Delegate called on visual parent ancestor changes.
        /// </summary>
        /// <param name="pSender">The sender</param>
        /// <param name="pEventArgs">The event arguments</param>
        private void OnVisualAncestorChanged_ForceInherit(object pSender, AncestorChangedEventArgs pEventArgs)
        {
            DependencyObject lParent = null;
            if 
                ( pEventArgs.OldParent == null )
            {
                // Find our nearest UIElement parent.
                lParent = InputElement.GetContainingUIElement( this.VisualParent );

                // See if this parent is a child of the ancestor who's parent changed.
                // If so, it does not care about changes that happen above us.
                if 
                    ( lParent != null && 
                      (pEventArgs.Ancestor as AVisual).IsAncestorOf( lParent ) )
                {
                    lParent = null;
                }
            }
            else
            {
                // Find our nearest UIElement lParent.
                lParent = InputElement.GetContainingUIElement( this.VisualParent );
                if 
                    ( lParent != null )
                {
                    // If we found a UIElement parent in the subtree, the
                    // break in the visual tree must have been above it,
                    // so it does not need to respond.
                    lParent = null;
                }
                else
                {
                    // There was no UIElement parent in the subtree, so it
                    // may be detaching from some UIElement parent above
                    // the break point in the tree.
                    lParent = InputElement.GetContainingUIElement( pEventArgs.OldParent );
                }
            }

            if
                ( lParent != null )
            {
                SynchronizeForceInheritProperties( this, lParent );
            }
        }

        /// <summary>
        /// OnRender is called by the base class when the rendering instructions of the UIElement are required.
        /// Note: the drawing instructions sent to DrawingContext are not rendered immediately on the screen
        /// but rather stored and later passed to the rendering engine at proper time.
        /// Derived classes override this method to draw the content of the UIElement.
        /// </summary>
        /// <param name="pContext">The drawing context for rendering content.</param>
        protected virtual void OnRender(ADrawingContext pContext)
        {
            // Nothing to do.
        }

        /// <summary>
        /// Delegate called once the layout update notified the UI element has changed in size.
        /// </summary>
        /// <param name="pArguments">The size changed arguments.</param>
        protected internal virtual void OnRenderSizeChanged(SizeChangedArguments pArguments)
        {
            // Nothing to do.
        }

        /// <summary>
        /// Checks whether the UI element is renderable or not.
        /// </summary>
        /// <returns>True if can be rendered, false otherwise.</returns>
        private bool IsRenderable()
        {
            if 
                ( this.mIsNeverMeasured || 
                  this.mIsNeverArranged )
            {
                return false;
            }

            if 
                ( this.mIsCollapsed )
            {
                return false;
            }

            return this.mIsMeasureDirty == false && 
                   this.mIsArrangeDirty == false;
        }

        /// <summary>
        /// Checks whether the UI element changed in size or not.
        /// </summary>
        /// <param name="pOldSize">The old size.</param>
        /// <param name="pNewSize">The new size.</param>
        /// <returns>True if has changed, false otherwise.</returns>
        private bool CheckSizeChanged(Size pOldSize, Size pNewSize)
        {
            //already marked for SizeChanged, simply update the pNewSize
            bool lHasWidthChanged  = DoubleUtil.AreClose( pOldSize.Width, pNewSize.Width ) == false;
            bool lHasHeightChanged = DoubleUtil.AreClose( pOldSize.Height, pNewSize.Height ) == false;

            SizeChangedArguments lCurrentArguments = this.SizeChangedArguments;
            if 
                ( lCurrentArguments != null )
            {
                lCurrentArguments.Accumulate( lHasWidthChanged, lHasHeightChanged );
                return true;
            }
            else if 
                ( lHasWidthChanged || 
                  lHasHeightChanged )
            {
                lCurrentArguments = new SizeChangedArguments( this, 
                                                              pOldSize, 
                                                              lHasWidthChanged, 
                                                              lHasHeightChanged );
                this.SizeChangedArguments = lCurrentArguments;

                UIService lUIService = ServiceManager.Instance.GetService<UIService>();
                lUIService.AddToSizeChangedChain( lCurrentArguments );
                
                // Notifies the Visual layer that hittest boundary potentially changed
                PropagateFlags( this,
                                VisualFlags.IsSubtreeDirtyForPrecompute | VisualFlags.IsSubtreeDirtyForRender );

                return true;
            }
            
            return false;
        }

        /// <summary>
        /// Checks whether the given point is a valid transform origin or not.
        /// </summary>
        /// <param name="pOrigin">The point to check.</param>
        /// <returns>True if valid, false otherwise.</returns>
        private static bool IsRenderTransformOriginValid(Point pOrigin)
        {
            return (DoubleUtil.IsNaN(pOrigin.X) == false && Double.IsPositiveInfinity(pOrigin.X) == false && Double.IsNegativeInfinity(pOrigin.X)) == false && 
                   (DoubleUtil.IsNaN(pOrigin.Y) == false && Double.IsPositiveInfinity(pOrigin.Y) == false && Double.IsNegativeInfinity(pOrigin.Y)) == false;
        }

        /// <summary>
        /// Trigger user input re evaluation on parent changes.
        /// </summary>
        /// <param name="pOldParent">The old parent.</param>
        /// <param name="pIsCoreParent"></param>
        internal void SynchronizeReverseInheritPropertyFlags(DependencyObject pOldParent, bool pIsCoreParent)
        {
            // TO DO...
            if
                ( this.IsKeyboardFocusWithin )
            {
                //Keyboard.PrimaryDevice.ReevaluateFocusAsync(this, pOldParent, pIsCoreParent);
            }
 
            if
                ( this.IsMouseOver )
            {
                //Mouse.PrimaryDevice.ReevaluateMouseOver(this, pOldParent, pIsCoreParent);
            }
        }

        /// <summary>
        /// Synchronize the UI element and its parent flags.
        /// </summary>
        /// <param name="pElement">The UI element.</param>
        /// <param name="pParent">The UI element's parent.</param>
        internal static void SynchronizeForceInheritProperties(UIElement pElement, DependencyObject pParent)
        {
            UIElement lParent = pParent as UIElement;
            if
                ( pElement != null )
            {
                bool lParentFlag = lParent.IsEnabled;
                if
                    ( lParentFlag == false )
                {
                    pElement.IsEnabled = lParentFlag;
                }
 
                lParentFlag = lParent.IsHitTestVisible;
                if
                    ( lParentFlag == false )
                {
                    pElement.IsHitTestVisible = lParentFlag;
                }
 
                lParentFlag = lParent.IsVisible;
                if
                    ( lParentFlag )
                {
                    pElement.UpdateIsVisibleCache();
                }
            }
        }

        /// <summary>
        /// Invalidates inherited property for children.
        /// </summary>
        /// <param name="pVisual">The current visual</param>
        /// <param name="pPropertyName">The property name that has changed.</param>
        internal static void InvalidateForceInheritPropertyOnChildren(AVisual pVisual, string pPropertyName)
        {
            int lChildrenCount = pVisual.VisualChildrenCount;
            for 
                ( int lCurrentChild = 0; lCurrentChild < lChildrenCount; lCurrentChild++ )
            {
                AVisual lVisualChild = pVisual.GetVisualChild( lCurrentChild );
                if 
                    ( lVisualChild != null )
                {
                    UIElement lElement = lVisualChild as UIElement;
                    if 
                        ( lElement != null )
                    {
                        if ( pPropertyName == "IsVisible" )
                        {
                            lElement.UpdateIsVisibleCache();
                        }
                        else
                        {
                            Action<DependencyObject> lCoercer;
                            if
                                ( sCoercers.TryGetValue( pPropertyName, out lCoercer ) )
                            {
                                lCoercer( lElement );
                            }
                        }
                    }
                    else
                    {
                        InvalidateForceInheritPropertyOnChildren( lVisualChild, pPropertyName);
                    }
                }
            }
        }

        /// <summary>
        /// Adds a  pre-opportunity handler for templated parent of this element in case parent is listening
        /// for synchronized input.
        /// </summary>
        /// <param name="pRoute">The event route.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void AddSynchronizedInputPreOpportunityHandler(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            if 
                ( InputManager.Instance.IsSynchronizedInput)
            {
                if 
                    ( SynchronizedInputHelper.IsListening( this, pEventArgs ) )
                {
                    RoutedEventDelegate lDelegate = new RoutedEventDelegate( this.SynchronizedInputPreOpportunityHandler );
                    SynchronizedInputHelper.AddHandlerToRoute( this, pRoute, lDelegate, false);
                }
                else
                {
                    this.CustomAddSynchronizedInputPreOpportunityDelegate( pRoute, pEventArgs );
                }
            }
        }

        /// <summary>
        /// Adds a  pre-opportunity handler for templated parent of this element in case parent is listening
        /// for synchronized input. (Overridable)
        /// </summary>
        /// <param name="pRoute">The event route.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void CustomAddSynchronizedInputPreOpportunityDelegate(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Adds a handler to post process the synchronized input if this UI element is currently listening to synchronized input(s),
        /// otherwise it adds a synchronized input pre-opportunity handler from parent if parent is listening.
        /// </summary>
        /// <param name="pRoute">The event route.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void AddSynchronizedInputPostOpportunityHandler(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            if 
                ( InputManager.Instance.IsSynchronizedInput )
            {
                if 
                    ( SynchronizedInputHelper.IsListening( this, pEventArgs ) )
                {
                    RoutedEventDelegate eventHandler = new RoutedEventDelegate( this.SynchronizedInputPostOpportunityHandler );
                    SynchronizedInputHelper.AddHandlerToRoute( this, pRoute, eventHandler, true );
                }
                else
                {
                    // Add a preview handler from the parent.
                    SynchronizedInputHelper.AddParentPreOpportunityHandler( this, pRoute, pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called before all the instance's handlers for this UI element.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void SynchronizedInputPreOpportunityHandler(object pSender, RoutedEventArgs pEventArgs)
        {
            SynchronizedInputHelper.PreOpportunityHandler( pSender, pEventArgs );
        }

        /// <summary>
        /// Delegate called after the instance's handlers for this UI element.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void SynchronizedInputPostOpportunityHandler(object pSender, RoutedEventArgs pEventArgs)
        {
            if  ( pEventArgs.IsHandled && 
                  InputManager.Instance.SynchronizedInputState == SynchronizedInputStates.HadOpportunity )
            {
                SynchronizedInputHelper.PostOpportunityHandler( pSender, pEventArgs );
            }
        }

        /// <summary>
        /// Notifies an automatic event.
        /// </summary>
        /// <param name="pEvent">The event type.</param>
        internal void RaiseAutomaticEvent(AutomaticEvents pEvent)
        {
            if
                ( this.AutomaticEvent != null )
            {
                this.AutomaticEvent( this, new AutomaticEventArgs( pEvent ) );
            }
        }

        /// <summary>
        /// Customizes the UIElement route
        /// </summary>
        /// <param name="pRoute">The route to modify.</param>
        /// <param name="pEventArgs">The route event arguments.</param>
        /// <returns>Whether or not the route should continue past the visual tree. If this is true, and there are no more visual parents, the route
        /// building code will call the GetUIParentCore method to find the next non-visual parent.</returns>
        internal virtual bool CustomBuildRoute(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            return false;
        }

        /// <summary>
        /// Builds the event route of this UI elemet.
        /// </summary>
        /// <param name="pRoute">The route to modify.</param>
        /// <param name="pEventArgs">The route event arguments.</param>
        internal void BuildRoute(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            UIElement.BuildRouteHelper( this, pRoute, pEventArgs );
        }

        /// <summary>
        /// Notifies the routed event listeners.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        public void RaiseEvent(RoutedEventArgs pEventArgs)
        {
            if 
                ( pEventArgs == null )
            {
                throw new ArgumentNullException("pEventArgs");
            }

            pEventArgs.IsInitiated = false;

            UIElement.RaiseEventImpl( this, pEventArgs );
        }

        /// <summary>
        /// Notifies the routed event listeners.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        /// <param name="pIsTrusted">The flag indicating whether the event must be raised safely or not.</param>
        internal void RaiseEvent(RoutedEventArgs pEventArgs, bool pIsTrusted)
        {
            if 
                ( pEventArgs == null )
            {
                throw new ArgumentNullException("args");
            }

            if
                ( pIsTrusted )
            {
                this.RaiseTrustedEvent( pEventArgs );
            }
            else
            {
                pEventArgs.IsInitiated = false;

                UIElement.RaiseEventImpl( this, pEventArgs );
            }
        }

        /// <summary>
        /// Safe routed event raise method.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void RaiseTrustedEvent(RoutedEventArgs pEventArgs)
        {
            if 
                ( pEventArgs == null )
            {
                throw new ArgumentNullException("pEventArgs");
            }

            // Try/finally to ensure that UserInitiated bit is cleared.
            pEventArgs.IsInitiated = true;

            try
            {
                UIElement.RaiseEventImpl( this, pEventArgs );
            }
            catch
                ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            pEventArgs.IsInitiated = false;
        }
        
        /// <summary>
        /// Adjusts the event source
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        /// <returns>The new source.</returns>
        internal virtual object AdjustEventSource(RoutedEventArgs pEventArgs)
        {
            return null;
        }

        /// <summary>
        /// Adds a new delegate to run for the given routed event.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event delegate to run must be added to.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public void AddHandler(RoutedEvent pRoutedEvent, Delegate pDelegate)
        {
            this.AddHandler( pRoutedEvent, pDelegate, false );
        }

        /// <summary>
        /// Adds a new delegate to run for the given routed event.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event delegate to run must be added to.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        /// <param name="pProcessHandled">The flag indicating whether the handled event must be notified as well or not.</param>
        public void AddHandler(RoutedEvent pRoutedEvent, Delegate pDelegate, bool pProcessHandled)
        {
            if 
                ( pRoutedEvent == null )
            {
                throw new ArgumentNullException("pRoutedEvent");
            }

            if 
                ( pDelegate == null )
            {
                throw new ArgumentNullException("pDelegate");
            }

            if 
                ( pRoutedEvent.IsLegalHandler( pDelegate ) == false )
            {
                throw new ArgumentException( "Unsupported handler type!!!" );
            }

            this.EnsureEventDelegatesCache();
            this.mEventDelegatesCache.AddRoutedEventHandler( pRoutedEvent, pDelegate, pProcessHandled );

            this.OnAddHandler( pRoutedEvent, pDelegate );
        }

        /// <summary>
        /// Delegate called on routed event handler addition.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event delegate comes from.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        internal virtual void OnAddHandler(RoutedEvent pRoutedEvent, Delegate pDelegate)
        {

        }

        /// <summary>
        /// Removes all instances of the specified routed event delegate for this UI element.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event delegate comes from.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public void RemoveHandler(RoutedEvent pRoutedEvent, Delegate pDelegate)
        {
            if
                ( pRoutedEvent == null )
            {
                throw new ArgumentNullException("pRoutedEvent");
            }

            if 
                ( pDelegate == null )
            {
                throw new ArgumentNullException("pDelegate");
            }

            if 
                ( pRoutedEvent.IsLegalHandler( pDelegate ) == false )
            {
                throw new ArgumentException( "Unsupported delegate type!!!" );
            }

            EventDelegatesCache lCache = this.EventDelegatesCache;
            if 
                ( lCache != null )
            {
                lCache.RemoveRoutedEventHandler( pRoutedEvent, pDelegate );

                this.OnRemoveHandler( pRoutedEvent, pDelegate );

                if 
                    ( lCache.Count == 0 )
                {
                    this.mEventDelegatesCache = null;
                    this.mHasEventDelegatesCache = false;
                }
            }
        }

        /// <summary>
        /// Delegate called on handler removal.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event delegate comes from.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        internal virtual void OnRemoveHandler(RoutedEvent pRoutedEvent, Delegate pDelegate)
        {

        }

        /// <summary>
        /// Adds an event delegate from cache.
        /// </summary>
        /// <param name="pKey">The event key.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        private void AddEventDelegateToCache(int pKey, Delegate pDelegate)
        {
            this.EnsureEventDelegatesCache();

            this.mEventDelegatesCache.Add( pKey, pDelegate );
        }

        /// <summary>
        /// Removes an event delegate from cache.
        /// </summary>
        /// <param name="pKey">The event key.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        private void RemoveEventDelegateFromCache(int pKey, Delegate pDelegate)
        {
            EventDelegatesCache lCache = this.EventDelegatesCache;
            if 
                ( lCache != null )
            {
                lCache.Remove( pKey, pDelegate );
                if 
                    ( lCache.Count == 0 )
                {
                    this.mEventDelegatesCache = null;
                    this.mHasEventDelegatesCache = false;
                }
            }
        }

        /// <summary>
        /// Helper building the UI element route for the routed event.
        /// </summary>
        /// <param name="pElement">The element the route must be built for.</param>
        /// <param name="pRoute">The event route.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal static void BuildRouteHelper(DependencyObject pElement, EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            if 
                ( pRoute == null )
            {
                throw new ArgumentNullException("pRoute");
            }

            if 
                ( pEventArgs == null )
            {
                throw new ArgumentNullException("pEventArgs");
            }

            if 
                ( pEventArgs.Source == null )
            {
                throw new ArgumentException( "No source for the event route!!!" );
            }

            if 
                ( pEventArgs.RoutedEvent != pRoute.RoutedEvent )
            {
                throw new ArgumentException( "Mismatched routed event arguments!!!" );
            }

            // Route via the visual tree
            if 
                ( pEventArgs.RoutedEvent.IsDirect )
            {
                UIElement lUIElement = pElement as UIElement;
                ContentElement lContentElement = null;

                if 
                    ( lUIElement == null )
                {
                    lContentElement = pElement as ContentElement;
                }

                // Add this element to pRoute
                if 
                    ( lUIElement != null )
                {
                    lUIElement.AddToEventRoute( pRoute, pEventArgs );
                }
                else if 
                    ( lContentElement != null )
                {
                    lContentElement.AddToEventRoute( pRoute, pEventArgs );
                }
            }
            else
            {
                int lElementCount = 0;
                while 
                    ( pElement != null )
                {
                    UIElement lUIElement = pElement as UIElement;
                    ContentElement lContentElement = null;

                    if 
                        ( lUIElement == null )
                    {
                        lContentElement = pElement as ContentElement;
                    }

                    // Protect against infinite loops by limiting the number of elements
                    // that we will process.
                    if 
                        ( lElementCount++ > cMAX_ELEMENTS_IN_ROUTE )
                    {
                        throw new InvalidOperationException( "Started an infinite loop!!!" );
                    }

                    // Allow the element to adjust source
                    object lNewSource = null;
                    if 
                        ( lUIElement != null )
                    {
                        lNewSource = lUIElement.AdjustEventSource( pEventArgs );
                    }
                    else if 
                        ( lContentElement != null )
                    {
                        lNewSource = lContentElement.AdjustEventSource( pEventArgs );
                    }

                    // Add changed source information to the pRoute
                    if 
                        ( lNewSource != null )
                    {
                        pRoute.AddSource( lNewSource );
                    }

                    // Invoke CustomBuildRoute
                    bool lKeepPastVisualTree = false;
                    if
                        ( lUIElement != null )
                    {
                        //Add a Synchronized input pre-opportunity handler just before the class and instance handlers
                        lUIElement.AddSynchronizedInputPreOpportunityHandler( pRoute, pEventArgs );

                        lKeepPastVisualTree = lUIElement.CustomBuildRoute( pRoute, pEventArgs );

                        // Add this element to pRoute
                        lUIElement.AddToEventRoute( pRoute, pEventArgs );

                        //Add a Synchronized input post-opportunity handler just after class and instance handlers
                        lUIElement.AddSynchronizedInputPostOpportunityHandler( pRoute, pEventArgs );

                        // Get element's visual parent
                        pElement = lUIElement.GetUIParent( lKeepPastVisualTree );
                    }
                    else if 
                        ( lContentElement != null )
                    {
                        //Add a Synchronized input pre-opportunity handler just before the class and instance handlers
                        lContentElement.AddSynchronizedInputPreOpportunityHandler( pRoute, pEventArgs );

                        lKeepPastVisualTree = lContentElement.CustomBuildRoute( pRoute, pEventArgs );

                        // Add this element to pRoute
                        lContentElement.AddToEventRoute( pRoute, pEventArgs );

                        // Add a Synchronized input post-opportunity handler just after the class and instance handlers
                        lContentElement.AddSynchronizedInputPostOpportunityHandler( pRoute, pEventArgs );

                        // Get element's visual parent
                        pElement = lContentElement.GetUIParent( lKeepPastVisualTree );
                    }

                    // If the CustomBuildRoute implementation changed the
                    // event source to the route parent, respect it in
                    // the actual route.
                    if 
                        ( pElement == pEventArgs.Source )
                    {
                        pRoute.AddSource( pElement );
                    }
                }
            }
        }

        /// <summary>
        /// Raises the routed event listeners of this UI element.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal static void RaiseEventImpl(DependencyObject pSender, RoutedEventArgs pEventArgs)
        {
            EventRoute lRoute = EventRouteFactory.FetchObject( pEventArgs.RoutedEvent );

            try
            {
                // Set Source
                pEventArgs.Source = pSender;

                UIElement.BuildRouteHelper( pSender, lRoute, pEventArgs );

                lRoute.InvokeHandlers( pSender, pEventArgs );

                // Reset Source to OriginalSource
                pEventArgs.Source = pEventArgs.OriginalSource;
            }
            catch
                ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            EventRouteFactory.RecycleObject( lRoute );
        }

        /// <summary>
        /// Adds the given event handlers for this UI element to the route.
        /// </summary>
        /// <param name="pRoute">The route delegate(s) must be added to.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        public void AddToEventRoute(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            if 
                ( pRoute == null )
            {
                throw new ArgumentNullException("pRoute");
            }

            if 
                ( pEventArgs == null )
            {
                throw new ArgumentNullException("pEventArgs");
            }

            // Get class delegates for this UI Element
            RoutedEventDelegateInfoContainer lClassDelegates = RoutedEvent.GetTypedClassListeners( this.GetType(), 
                                                                                                   pEventArgs.RoutedEvent );

            // Add all the class delegates for this UI Element
            while 
                ( lClassDelegates != null )
            {
                for 
                    ( int lCurr = 0; lCurr < lClassDelegates.Delegates.Length; lCurr++ )
                {
                    pRoute.Add( this, 
                                lClassDelegates.Delegates[ lCurr ].Handler, 
                                lClassDelegates.Delegates[ lCurr ].ProcessHandledEvents );
                }

                lClassDelegates = lClassDelegates.Next;
            }

            // Get instance listeners for this UIElement
            List<RoutedEventHandlerInfo> lThisDelegates = null;
            EventDelegatesCache lCache = this.EventDelegatesCache;
            if 
                ( lCache != null )
            {
                lThisDelegates = lCache[ pEventArgs.RoutedEvent ];

                // Add all instance listeners for this UIElement
                if 
                    ( lThisDelegates != null )
                {
                    for 
                        ( int lCurr = 0; lCurr < lThisDelegates.Count; lCurr++ )
                    {
                        pRoute.Add( this, 
                                    lThisDelegates[ lCurr ].Handler, 
                                    lThisDelegates[ lCurr ].ProcessHandledEvents );
                    }
                }
            }

            // Allow an override.
            this.CustomAddToEventRoute( pRoute, pEventArgs );
        }

        /// <summary>
        /// Overridable add to event route method.
        /// </summary>
        /// <param name="pRoute">The route delegate must be added to.</param>
        /// <param name="pEventArgs">THe event arguments.</param>
        internal virtual void CustomAddToEventRoute(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Ensures the event delegates cache exists.
        /// </summary>
        internal void EnsureEventDelegatesCache()
        {
            if
                ( this.mEventDelegatesCache == null )
            {
                this.mEventDelegatesCache = new EventDelegatesCache();
                this.mHasEventDelegatesCache = true;
            }
        }

        /// <summary>
        /// Notifies internal cache listeners about a property changed.
        /// </summary>
        /// <param name="pKey">The event key.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void InternalNotifyPropertyChanged(int pKey, PropertyChangedEventArgs pEventArgs)
        {
            EventDelegatesCache lCache = this.EventDelegatesCache;
            if 
                ( lCache != null )
            {
                Delegate lDelegate = lCache.Get( pKey );
                if
                    ( lDelegate != null )
                {
                    ((PropertyChangedEventDelegate)lDelegate)( this, pEventArgs );
                }
            }
        }
        
        /// <summary>
        /// Notifies a mouse over change.
        /// </summary>
        /// <param name="pOldElement">The old element.</param>
        /// <param name="pNewElement">The new element.</param>
        internal static void NotifyMouseOverValueChanged(UIElement pOldElement, UIElement pNewElement)
        {
            if
                ( pOldElement.NotifyPropertyChanged( ref pOldElement.mIsMouseOver, false, "IsMouseOver" ) )
            {
                InputElement.FireMouseOverPropertyChangedInAncestry( pOldElement, true );
            }
            
            if
                ( pNewElement.NotifyPropertyChanged( ref pNewElement.mIsMouseOver, true, "IsMouseOver" ) )
            {
                InputElement.FireMouseOverPropertyChangedInAncestry( pNewElement, false );
            }
        }

        /// <summary>
        /// Notifies a mouse capture within element.
        /// </summary>
        /// <param name="pOldElement">The old element.</param>
        /// <param name="pNewElement">The new element.</param>
        internal static void NotifyMouseCaptureWithinValueChanged(UIElement pOldElement, UIElement pNewElement)
        {
            if
                ( pOldElement.NotifyPropertyChanged( ref pOldElement.mIsMouseCaptureWithin, false, "IsMouseCaptureWithin" ) )
            {
                InputElement.FireMouseCaptureWithinPropertyChangedInAncestry( pOldElement, true );
            }
            
            if
                ( pNewElement.NotifyPropertyChanged( ref pNewElement.mIsMouseCaptureWithin, true, "IsMouseCaptureWithin" ) )
            {
                InputElement.FireMouseCaptureWithinPropertyChangedInAncestry( pNewElement, false );
            }
        }

        /// <summary>
        /// Delegate called on IsMouseCaptureWithin changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnIsMouseCaptureWithinChanged(PropertyChangedEventArgs pEventArgs)
        {
            // To override...
        }

        /// <summary>
        /// Internal raise of the IsMouseCaptureWithin event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void RaiseIsMouseCaptureWithinChanged(PropertyChangedEventArgs pEventArgs)
        {
            // Call the virtual method first.
            this.OnIsMouseCaptureWithinChanged( pEventArgs );

            // Raise the public event second.
            this.InternalNotifyPropertyChanged( UIElement.IsMouseCaptureWithinChangedKey, pEventArgs );
        }

        /// <summary>
        /// Delegate called on IsMouseCaptured changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnIsMouseCapturedChanged(PropertyChangedEventArgs pEventArgs)
        {
            // Override it...
        }

        /// <summary>
        /// Internal raise of the IsMouseCaptured event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        private void RaiseIsMouseCapturedChanged(PropertyChangedEventArgs pEventArgs)
        {
            // Call the virtual method first.
            this.OnIsMouseCapturedChanged( pEventArgs );

            // Raise the public event second.
            this.InternalNotifyPropertyChanged( IsMouseCapturedChangedKey, pEventArgs );
        }

        /// <summary>
        /// Delegate called on IsKeyboardFocusWithin changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnIsKeyboardFocusWithinChanged(PropertyChangedEventArgs pEventArgs)
        {
            // Override it...
        }

        /// <summary>
        /// Internal raise of the IsKeyboardFocusWithin event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void RaiseIsKeyboardFocusWithinChanged(PropertyChangedEventArgs pEventArgs)
        {
            // Call the virtual method first.
            this.OnIsKeyboardFocusWithinChanged( pEventArgs );

            // Raise the public event second.
            this.InternalNotifyPropertyChanged( IsKeyboardFocusWithinChangedKey, pEventArgs );
        }

        /// <summary>
        /// Delegate called on mouse directly over property changes. (Overridable)
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnIsMouseDirectlyOverChanged(PropertyChangedEventArgs pEventArgs)
        {
            // Nothing to do.
        }

        /// <summary>
        /// Registers the UIElement internal callbacks on input routed events of the given type.
        /// </summary>
        /// <param name="pType">The class type input routed event must be listened to.</param>
        internal static void RegisterEvents(Type pType)
        {
            RoutedEvent.RegisterClassHandler( pType, InputManager.PreviewMouseDownEvent, new MouseButtonEventDelegate( UIElement.OnPreviewMouseDownThunk ), true );
            RoutedEvent.RegisterClassHandler( pType, InputManager.MouseDownEvent, new MouseButtonEventDelegate( UIElement.OnMouseDownThunk ), true );
            RoutedEvent.RegisterClassHandler( pType, InputManager.PreviewMouseUpEvent, new MouseButtonEventDelegate( UIElement.OnPreviewMouseUpThunk ), true );
            RoutedEvent.RegisterClassHandler( pType, InputManager.MouseUpEvent, new MouseButtonEventDelegate( UIElement.OnMouseUpThunk ), true );
            RoutedEvent.RegisterClassHandler( pType, UIElement.PreviewMouseLeftButtonDownEvent, new MouseButtonEventDelegate(UIElement.OnPreviewMouseLeftButtonDownThunk), false);
            RoutedEvent.RegisterClassHandler( pType, UIElement.MouseLeftButtonDownEvent, new MouseButtonEventDelegate(UIElement.OnMouseLeftButtonDownThunk), false);
            RoutedEvent.RegisterClassHandler( pType, UIElement.PreviewMouseLeftButtonUpEvent, new MouseButtonEventDelegate(UIElement.OnPreviewMouseLeftButtonUpThunk), false);
            RoutedEvent.RegisterClassHandler( pType, UIElement.MouseLeftButtonUpEvent, new MouseButtonEventDelegate(UIElement.OnMouseLeftButtonUpThunk), false);
            RoutedEvent.RegisterClassHandler( pType, UIElement.PreviewMouseRightButtonDownEvent, new MouseButtonEventDelegate(UIElement.OnPreviewMouseRightButtonDownThunk), false);
            RoutedEvent.RegisterClassHandler( pType, UIElement.MouseRightButtonDownEvent, new MouseButtonEventDelegate(UIElement.OnMouseRightButtonDownThunk), false);
            RoutedEvent.RegisterClassHandler( pType, UIElement.PreviewMouseRightButtonUpEvent, new MouseButtonEventDelegate(UIElement.OnPreviewMouseRightButtonUpThunk), false);
            RoutedEvent.RegisterClassHandler( pType, UIElement.MouseRightButtonUpEvent, new MouseButtonEventDelegate(UIElement.OnMouseRightButtonUpThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.PreviewMouseMoveEvent, new MouseEventDelegate(UIElement.OnPreviewMouseMoveThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.MouseMoveEvent, new MouseEventDelegate(UIElement.OnMouseMoveThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.PreviewMouseWheelEvent, new MouseWheelEventDelegate(UIElement.OnPreviewMouseWheelThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.MouseWheelEvent, new MouseWheelEventDelegate(UIElement.OnMouseWheelThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.MouseEnterEvent, new MouseEventDelegate(UIElement.OnMouseEnterThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.MouseLeaveEvent, new MouseEventDelegate(UIElement.OnMouseLeaveThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.GotMouseCaptureEvent, new MouseEventDelegate(UIElement.OnGotMouseCaptureThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.LostMouseCaptureEvent, new MouseEventDelegate(UIElement.OnLostMouseCaptureThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.PreviewKeyDownEvent, new KeyEventDelegate(UIElement.OnPreviewKeyDownThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.KeyDownEvent, new KeyEventDelegate(UIElement.OnKeyDownThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.PreviewKeyUpEvent, new KeyEventDelegate(UIElement.OnPreviewKeyUpThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.KeyUpEvent, new KeyEventDelegate(UIElement.OnKeyUpThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.PreviewGotKeyboardFocusEvent, new KeyboardFocusChangedEventDelegate(UIElement.OnPreviewGotKeyboardFocusThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.GotKeyboardFocusEvent, new KeyboardFocusChangedEventDelegate(UIElement.OnGotKeyboardFocusThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.PreviewLostKeyboardFocusEvent, new KeyboardFocusChangedEventDelegate(UIElement.OnPreviewLostKeyboardFocusThunk), false);
            RoutedEvent.RegisterClassHandler( pType, InputManager.LostKeyboardFocusEvent, new KeyboardFocusChangedEventDelegate(UIElement.OnLostKeyboardFocusThunk), false);
        }

        /// <summary>
        /// Right and Left mouse button wrapping routine from UIelement input routed event and
        /// the corresponding Input manager ones.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        /// <returns>THe wrapper routed event.</returns>
        private static RoutedEvent WrapMouseButtonEvent(MouseButtonEventArgs pEventArgs)
        {
            RoutedEvent lWrapperEvent = null;
            switch 
                ( pEventArgs.Button )
            {
                case OpenTK.Input.MouseButton.Left:
                    {
                        if 
                            ( pEventArgs.RoutedEvent == InputManager.PreviewMouseDownEvent )
                        {
                            lWrapperEvent = UIElement.PreviewMouseLeftButtonDownEvent;
                        }
                        else if 
                            ( pEventArgs.RoutedEvent == InputManager.MouseDownEvent )
                        {
                            lWrapperEvent = UIElement.MouseLeftButtonDownEvent;
                        }
                        else if 
                            ( pEventArgs.RoutedEvent == InputManager.PreviewMouseUpEvent )
                        {
                            lWrapperEvent = UIElement.PreviewMouseLeftButtonUpEvent;
                        }
                        else
                        {
                            lWrapperEvent = UIElement.MouseLeftButtonUpEvent;
                        }
                    }
                    break;
                case OpenTK.Input.MouseButton.Right:
                    {
                        if 
                            ( pEventArgs.RoutedEvent == InputManager.PreviewMouseDownEvent )
                        {
                            lWrapperEvent = UIElement.PreviewMouseRightButtonDownEvent;
                        }
                        else if 
                            ( pEventArgs.RoutedEvent == InputManager.MouseDownEvent )
                        {
                            lWrapperEvent = UIElement.MouseRightButtonDownEvent;
                        }
                        else if 
                            ( pEventArgs.RoutedEvent == InputManager.PreviewMouseUpEvent )
                        {
                            lWrapperEvent = UIElement.PreviewMouseRightButtonUpEvent;
                        }
                        else
                        {
                            lWrapperEvent = UIElement.MouseRightButtonUpEvent;
                        }
                    }
                    break;
                default:
                    // No wrappers exposed for the other buttons.
                    break;
            }

            return (lWrapperEvent);
        }

        /// <summary>
        /// Wrap the event routed event and notify the wrapper event then.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void WrapMouseButtonEventAndNotifyEvent(DependencyObject pSender, MouseButtonEventArgs pEventArgs)
        {
            RoutedEvent lWrapperEvent = WrapMouseButtonEvent( pEventArgs );
            if 
                ( lWrapperEvent != null )
            {
                NotifyEventAs( pSender, pEventArgs, lWrapperEvent );
            }
        }

        /// <summary>
        /// Notifies the wrapper event as 
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pRoutedEventArgs">The routed event arguments.</param>
        /// <param name="pWrapperEvent">The wrapper routed event.</param>
        private static void NotifyEventAs(DependencyObject pSender, RoutedEventArgs pRoutedEventArgs, RoutedEvent pWrapperEvent)
        {
            // Preseve and change the RoutedEvent
            RoutedEvent lRoutedEvent = pRoutedEventArgs.RoutedEvent;
            pRoutedEventArgs.RoutedEvent = pWrapperEvent;
 
            // Preserve Source
            object lSource = pRoutedEventArgs.Source;
 
            EventRoute lRoute = EventRouteFactory.FetchObject( pRoutedEventArgs.RoutedEvent );
            try
            {
                // Build the route and invoke the handlers
                UIElement.BuildRouteHelper( pSender, lRoute, pRoutedEventArgs );

                lRoute.ReInvokeHandlers( pSender, pRoutedEventArgs );

                // Restore Source
                pRoutedEventArgs.Source = lSource;

                // Restore RoutedEvent
                pRoutedEventArgs.RoutedEvent = lRoutedEvent;
            }
            catch
                ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }
 
            // Recycle the route object
            EventRouteFactory.RecycleObject( lRoute );
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview mouse down event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewMouseDownThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if 
                ( pEventArgs.IsHandled == false )
            {
                UIElement lUIElement = pSender as UIElement;
                if 
                    ( lUIElement != null )
                {
                    lUIElement.OnPreviewMouseDown( pEventArgs );
                }
                else
                {
                    ContentElement lContentElement = pSender as ContentElement;
                    if 
                        ( lContentElement != null )
                    {
                        lContentElement.OnPreviewMouseDown( pEventArgs );
                    }
                }
            }

            WrapMouseButtonEventAndNotifyEvent( pSender as DependencyObject, pEventArgs );
        }

        /// <summary>
        /// Delegate called for on preview mouse down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewMouseDown(MouseButtonEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse down event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseDownThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if
                ( pEventArgs.IsHandled == false )
            {
                UIElement lUIElement = pSender as UIElement;
                if 
                    ( lUIElement != null )
                {
                    lUIElement.OnMouseDown( pEventArgs );
                }
                else
                {
                    ContentElement lContentElement = pSender as ContentElement;
                    if 
                        ( lContentElement != null )
                    {
                        lContentElement.OnMouseDown( pEventArgs );
                    }
                }
            }
 
            WrapMouseButtonEventAndNotifyEvent( pSender as DependencyObject, pEventArgs );
        }

        /// <summary>
        /// Delegate called for on mouse down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseDown(MouseButtonEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview mouse up event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewMouseUpThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if 
                ( pEventArgs.IsHandled == false )
            {
                UIElement lUIElement = pSender as UIElement;
                if 
                    ( lUIElement != null )
                {
                    lUIElement.OnPreviewMouseUp( pEventArgs );
                }
                else
                {
                    ContentElement lContentElement = pSender as ContentElement;
                    if 
                        ( lContentElement != null )
                    {
                        lContentElement.OnPreviewMouseUp( pEventArgs );
                    }
                }
            }

            WrapMouseButtonEventAndNotifyEvent( pSender as DependencyObject, pEventArgs );
        }

        /// <summary>
        /// Delegate called for on preview mouse up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewMouseUp(MouseButtonEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse up event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseUpThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if 
                ( pEventArgs.IsHandled == false )
            {
                UIElement lUIElement = pSender as UIElement;
                if 
                    ( lUIElement != null )
                {
                    lUIElement.OnMouseUp( pEventArgs );
                }
                else
                {
                    ContentElement lContentElement = pSender as ContentElement;
                    if 
                        ( lContentElement != null )
                    {
                        lContentElement.OnMouseUp( pEventArgs );
                    }
                }
            }

            WrapMouseButtonEventAndNotifyEvent( pSender as DependencyObject, pEventArgs );
        }

        /// <summary>
        /// Delegate called for on mouse up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseUp(MouseButtonEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview mouse left button down event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewMouseLeftButtonDownThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewMouseLeftButtonDown( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewMouseLeftButtonDown( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview mouse left button down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse left button down event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseLeftButtonDownThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnMouseLeftButtonDown( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnMouseLeftButtonDown( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on mouse left button down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseLeftButtonDown(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview mouse left button up event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewMouseLeftButtonUpThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewMouseLeftButtonUp( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewMouseLeftButtonUp( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview mouse left button up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse left button up event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseLeftButtonUpThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnMouseLeftButtonUp( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnMouseLeftButtonUp( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on mouse left button up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseLeftButtonUp(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do..
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview mouse right button down event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewMouseRightButtonDownThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewMouseRightButtonDown( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewMouseRightButtonDown( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview mouse right button down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewMouseRightButtonDown(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse right button down event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseRightButtonDownThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnMouseRightButtonDown( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnMouseRightButtonDown( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on mouse right button down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseRightButtonDown(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview mouse right button up event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewMouseRightButtonUpThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewMouseRightButtonUp( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewMouseRightButtonUp( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview mouse right button up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewMouseRightButtonUp(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse right button up event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseRightButtonUpThunk(object pSender, MouseButtonEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnMouseRightButtonUp( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnMouseRightButtonUp( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on mouse right button up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseRightButtonUp(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview mouse move event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewMouseMoveThunk(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewMouseMove( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewMouseMove( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview mouse move event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewMouseMove(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse move event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseMoveThunk(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnMouseMove( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnMouseMove( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on mouse move event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseMove(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview mouse wheel event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewMouseWheelThunk(object pSender, MouseWheelEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewMouseWheel( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewMouseWheel( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview mouse wheel event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewMouseWheel(MouseWheelEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse wheel event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseWheelThunk(object pSender, MouseWheelEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            //CommandManager.TranslateInput((IInputElement)sender, e);

            if ( pEventArgs.IsHandled == false )
            {
                UIElement lUIElement = pSender as UIElement;
                if 
                    ( lUIElement != null )
                {
                    lUIElement.OnMouseWheel( pEventArgs );
                }
                else
                {
                    ContentElement lContentElement = pSender as ContentElement;
                    if 
                        ( lContentElement != null )
                    {
                        lContentElement.OnMouseWheel( pEventArgs );
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called for on mouse wheel event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseWheel(MouseWheelEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse enter event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseEnterThunk(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnMouseEnter( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnMouseEnter( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on mouse enter event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseEnter(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on mouse leave event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnMouseLeaveThunk(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnMouseLeave( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnMouseLeave( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on mouse leave event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnMouseLeave(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on got mouse capture event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnGotMouseCaptureThunk(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnGotMouseCapture( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnGotMouseCapture( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on got mouse capture event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnGotMouseCapture(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on lost mouse capture event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnLostMouseCaptureThunk(object pSender, MouseEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnLostMouseCapture( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnLostMouseCapture( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on lost mouse capture event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnLostMouseCapture(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview key down event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewKeyDownThunk(object pSender, KeyEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewKeyDown( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewKeyDown( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview key down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewKeyDown(KeyEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on key down event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnKeyDownThunk(object pSender, KeyEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            //CommandManager.TranslateInput((IInputElement)pSender, pEventArgs);

            if
                ( pEventArgs.IsHandled == false )
            {
                UIElement lUIElement = pSender as UIElement;
                if 
                    ( lUIElement != null )
                {
                    lUIElement.OnKeyDown( pEventArgs );
                }
                else
                {
                    ContentElement lContentElement = pSender as ContentElement;
                    if 
                        ( lContentElement != null )
                    {
                        lContentElement.OnKeyDown( pEventArgs );
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called for on key down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnKeyDown(KeyEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview key up event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewKeyUpThunk(object pSender, KeyEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewKeyUp( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewKeyUp( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview key up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewKeyUp(KeyEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on key up event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnKeyUpThunk(object pSender, KeyEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnKeyUp( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnKeyUp( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on key up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnKeyUp(KeyEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview got keyboard focus event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewGotKeyboardFocusThunk(object pSender, KeyboardFocusChangedEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewGotKeyboardFocus( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewGotKeyboardFocus( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview Got keyboard focus event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewGotKeyboardFocus(KeyboardFocusChangedEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on got keyboard focus event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnGotKeyboardFocusThunk(object pSender, KeyboardFocusChangedEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnGotKeyboardFocus( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnGotKeyboardFocus( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on Got keyboard focus event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on preview lost keyboard focus event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnPreviewLostKeyboardFocusThunk(object pSender, KeyboardFocusChangedEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnPreviewLostKeyboardFocus( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnPreviewLostKeyboardFocus( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on preview Lost keyboard focus event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnPreviewLostKeyboardFocus(KeyboardFocusChangedEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for UIElement and ContentElement on lost keyboard focus event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private static void OnLostKeyboardFocusThunk(object pSender, KeyboardFocusChangedEventArgs pEventArgs)
        {
            if ( pEventArgs.IsHandled )
            {
                LogManager.Instance.Log( "Event has already been handled." );
            }

            UIElement lUIElement = pSender as UIElement;
            if 
                ( lUIElement != null )
            {
                lUIElement.OnLostKeyboardFocus( pEventArgs );
            }
            else
            {
                ContentElement lContentElement = pSender as ContentElement;
                if 
                    ( lContentElement != null )
                {
                    lContentElement.OnLostKeyboardFocus( pEventArgs );
                }
            }
        }

        /// <summary>
        /// Delegate called for on Lost keyboard focus event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Input based Hit test result class definition.
        /// </summary>
        private class InputHitTestResult
        {
            #region Fields

            /// <summary>
            /// Stores the hit test result.
            /// </summary>
            private AHitTestResult mResult;

            #endregion Fields

            #region Properties

            /// <summary>
            /// Gets the hit result object.
            /// </summary>
            public DependencyObject Result
            {
                get
                {
                    return this.mResult != null ? this.mResult.VisualHit : null;
                }
            }

            /// <summary>
            /// Gets the hit test result.
            /// </summary>
            public AHitTestResult HitTestResult
            {
                get
                {
                    return this.mResult;
                }
            }

            #endregion Properties

            #region Methods

            /// <summary>
            /// Hit result callback determining whether to stop or not.
            /// </summary>
            /// <param name="pResult">The hit test result.</param>
            /// <returns>The hit test behavior.</returns>
            public HitTestResultBehavior InputHitTestResultCallback(AHitTestResult pResult)
            {
                this.mResult = pResult;
                return HitTestResultBehavior.Stop;
            }

            #endregion Methods
        }

        #endregion Inner classes
    }
}
