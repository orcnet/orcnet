﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Synchronized input type enumeration definition.
    /// </summary>
    public enum SynchronizedInputType
    {
        /// <summary>
        /// Any key up.
        /// </summary>
        KeyUp = 1,

        /// <summary>
        /// Any key down.
        /// </summary>
        KeyDown = 2,

        /// <summary>
        /// Mouse left up.
        /// </summary>
        MouseLeftButtonUp = 4,

        /// <summary>
        /// Mouse left down.
        /// </summary>
        MouseLeftButtonDown = 8,

        /// <summary>
        /// Mouse right up.
        /// </summary>
        MouseRightButtonUp = 16,

        /// <summary>
        /// Mouse right down.
        /// </summary>
        MouseRightButtonDown = 32
    }
}
