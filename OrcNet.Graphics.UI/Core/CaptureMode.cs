﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Enumeration of the different mouse capture modes.
    /// </summary>
    public enum CaptureMode
    {
        /// <summary>
        /// No Capture
        /// </summary>
        None,

        /// <summary>
        /// Only a single element can be captured at a time.
        /// </summary>
        Element,

        /// <summary>
        /// An entire subtree of an element can be captured.
        /// </summary>
        SubTree
    }
}
