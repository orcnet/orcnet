﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Container storing the set of class's handlers for a given RoutedEvent
    /// </summary>
    internal class RoutedEventDelegates
    {
        #region Fields

        /// <summary>
        /// Stores the routed event the delegates are associated with.
        /// </summary>
        internal RoutedEvent RoutedEvent;

        /// <summary>
        /// Stores the set of delegates of this cntainer.
        /// </summary>
        internal RoutedEventDelegateInfoContainer DelegatesContainer;

        /// <summary>
        /// Stores the flag indicating whether the routed event has its own handlers or not.
        /// </summary>
        internal bool HasSelfHandlers;

        #endregion Fields

        #region Methods

        /// <summary>
        /// Checks whether the given routed event delegates mapper is equal to this one or not.
        /// </summary>
        /// <param name="pObject">The other to test against this.</param>
        public override bool Equals(object pObject)
        {
            return this.Equals((RoutedEventDelegates)pObject);
        }

        /// <summary>
        /// Checks whether the given routed event delegates mapper is equal to this one or not.
        /// </summary>
        /// <param name="pDelegates">The other to test against this.</param>
        public bool Equals(RoutedEventDelegates pDelegates)
        {
            return pDelegates.RoutedEvent == this.RoutedEvent &&
                   pDelegates.DelegatesContainer == this.DelegatesContainer;
        }

        /// <summary>
        /// Gets the hash code for that mapper being used as hasher in hash-containers.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Checks whether the two routed event delegates mapper are equal or not.
        /// </summary>
        /// <param name="pFirst">The first.</param>
        /// <param name="pSecond">The second.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public static bool operator ==(RoutedEventDelegates pFirst, RoutedEventDelegates pSecond)
        {
            return pFirst.Equals( pSecond );
        }

        /// <summary>
        /// Checks whether the two routed event delegates mapper are different or not.
        /// </summary>
        /// <param name="pFirst">The first.</param>
        /// <param name="pSecond">The second.</param>
        /// <returns>True if different, false otherwise.</returns>
        public static bool operator !=(RoutedEventDelegates pFirst, RoutedEventDelegates pSecond)
        {
            return pFirst.Equals( pSecond ) == false;
        }

        #endregion Methods
    }
}
