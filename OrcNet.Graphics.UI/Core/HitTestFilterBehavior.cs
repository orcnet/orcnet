﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Behavior for filtering visuals while hit tesitng
    /// </summary>
    public enum HitTestFilterBehavior
    {
        /// <summary>
        /// Hit test against current visual and not its children.
        /// </summary>
        ContinueSkipChildren = HTFBInterpreter.c_DoHitTest,

        /// <summary>
        /// Do not hit test against current visual or its children.
        /// </summary>
        ContinueSkipSelfAndChildren = 0,

        /// <summary>
        /// Do not hit test against current visual but hit test against children.
        /// </summary>
        ContinueSkipSelf = HTFBInterpreter.c_IncludeChidren,

        /// <summary>
        /// Hit test against current visual and children.
        /// </summary>
        Continue = HTFBInterpreter.c_DoHitTest | HTFBInterpreter.c_IncludeChidren,

        /// <summary>
        /// Stop any further hit testing and return.
        /// </summary>
        Stop = HTFBInterpreter.c_Stop
    }

    /// <summary>
    /// Delegate for hit tester to control whether to test against the
    /// current scene graph node.
    /// </summary>
    public delegate HitTestFilterBehavior HitTestFilterCallback(DependencyObject pPotentialHitTestTarget);

    /// <summary>
    /// Static helper class with methods for interpreting the HitTestFilterBehavior enum.
    /// </summary>
    internal static class HTFBInterpreter
    {
        /// <summary>
        /// 
        /// </summary>
        internal const int c_DoHitTest = (1 << 1);

        /// <summary>
        /// 
        /// </summary>
        internal const int c_IncludeChidren = (1 << 2);

        /// <summary>
        /// 
        /// </summary>
        internal const int c_Stop = (1 << 3);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pbehavior"></param>
        /// <returns></returns>
        internal static bool DoHitTest(HitTestFilterBehavior pbehavior)
        {
            return (((int)pbehavior) & c_DoHitTest) == c_DoHitTest;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pbehavior"></param>
        /// <returns></returns>
        internal static bool IncludeChildren(HitTestFilterBehavior pbehavior)
        {
            return (((int)pbehavior) & c_IncludeChidren) == c_IncludeChidren;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pbehavior"></param>
        /// <returns></returns>
        internal static bool Stop(HitTestFilterBehavior pbehavior)
        {
            return (((int)pbehavior) & c_Stop) == c_Stop;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pbehavior"></param>
        /// <returns></returns>
        internal static bool SkipSubgraph(HitTestFilterBehavior pbehavior)
        {
            return pbehavior == HitTestFilterBehavior.ContinueSkipSelfAndChildren;
        }
    }
}
