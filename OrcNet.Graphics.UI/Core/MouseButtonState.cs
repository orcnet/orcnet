﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Enumerate the mouse button states.
    /// </summary>
    public enum MouseButtonState
    {
        /// <summary>
        /// Button released
        /// </summary>
        Released = 0,

        /// <summary>
        /// Button pressed
        /// </summary>
        Pressed = 1
    }
}
