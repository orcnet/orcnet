﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    ///     Creates and recycles instance of EventRoute
    /// </summary>
    internal static class EventRouteFactory
    {
        #region Fields

        /// <summary>
        /// Stores the event route cache.
        /// </summary>
        private static EventRoute[] sEventRouteStack;

        /// <summary>
        /// Stores the event route current count.
        /// </summary>
        private static int sStackTop;

        /// <summary>
        /// Stores the factory cache sync object.
        /// </summary>
        private static object sSync = new object();

        #endregion Fields

        #region Methods

        /// <summary>
        /// Fetches a recycled event route if available else create a new instance
        /// </summary>
        /// <param name="pRoutedEvent">The routed event the route will be for.</param>
        /// <returns></returns>
        internal static EventRoute FetchObject(RoutedEvent pRoutedEvent)
        {
            EventRoute pEventRoute = Pop();
            if 
                ( pEventRoute == null )
            {
                pEventRoute = new EventRoute( pRoutedEvent );
            }
            else
            {
                pEventRoute.RoutedEvent = pRoutedEvent;
            }

            return pEventRoute;
        }

        /// <summary>
        /// Recycles the given EventRoute
        /// </summary>
        /// <param name="pEventRoute">The event route to recycle.</param>
        internal static void RecycleObject(EventRoute pEventRoute)
        {
            // Cleanup all references held
            pEventRoute.Clear();

            // Push instance on to the stack
            Push( pEventRoute );
        }
        
        /// <summary>
        /// Pushes the given EventRoute onto the stack
        /// </summary>
        /// <param name="pEventRoute">The event route.</param>
        private static void Push(EventRoute pEventRoute)
        {
            lock 
                ( sSync )
            {
                // In a normal scenario it is extremely rare to 
                // require more than 2 EventRoutes at the same time
                if 
                    ( sEventRouteStack == null )
                {
                    sEventRouteStack = new EventRoute[ 2 ];
                    sStackTop = 0;
                }

                if 
                    ( sStackTop < 2 )
                {
                    sEventRouteStack[ sStackTop++ ] = pEventRoute;
                }
            }
        }

        /// <summary>
        /// Pops off the last EventRoute in the stack
        /// </summary>
        /// <returns>The event route.</returns>
        private static EventRoute Pop()
        {
            lock 
                ( sSync )
            {
                if 
                    ( sStackTop > 0 )
                {
                    EventRoute lEventRoute = sEventRouteStack[ --sStackTop ];
                    sEventRouteStack[ sStackTop ] = null;
                    return lEventRoute;
                }
            }

            return null;
        }

        #endregion Methods
    }
}
