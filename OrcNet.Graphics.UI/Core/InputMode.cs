﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Enumerate the mode of input processing when an input was provided.
    /// </summary>
    public enum InputMode
    {
        /// <summary>
        /// The input was provided while the application was in the foreground.
        /// </summary>
        Foreground = 0,

        /// <summary>
        /// The input was provided while the application was not in the foreground.
        /// </summary>
        Sink
    }
}
