﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Route item class definition describing an event route.
    /// </summary>
    internal struct RouteItem
    {
        #region Fields

        /// <summary>
        /// Stores the route item target.
        /// </summary>
        private object mTarget;

        /// <summary>
        /// Stores the route item handler info to run the delegate.
        /// </summary>
        private RoutedEventHandlerInfo mHandlerInfo;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the route item target.
        /// </summary>
        internal object Target
        {
            get
            {
                return this.mTarget;
            }
        }

        #endregion Properties

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="RouteItem"/> class.
        /// </summary>
        /// <param name="pTarget">The route item target.</param>
        /// <param name="pHandlerInfo">The route item handler info to run the delegate.</param>
        internal RouteItem(object pTarget, RoutedEventHandlerInfo pHandlerInfo)
        {
            this.mTarget = pTarget;
            this.mHandlerInfo = pHandlerInfo;
        }

        #endregion Construction

        #region Operations
        
        /// <summary>
        /// Runs the delegate of the route item.
        /// </summary>
        /// <param name="pEventArgs">The routed event arguments.</param>
        internal void RunDelegate(RoutedEventArgs pEventArgs)
        {
            this.mHandlerInfo.RunDelegate( this.mTarget, pEventArgs );
        }
        
        /// <summary>
        /// Checks whether two route item are equal or not.
        /// </summary>
        /// <param name="pObject"></param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pObject)
        {
            return this.Equals( (RouteItem)pObject );
        }

        /// <summary>
        /// Checks whether two route item are equal or not.
        /// </summary>
        /// <param name="pRouteItem"></param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(RouteItem pRouteItem)
        {
            return pRouteItem.mTarget == this.mTarget &&
                   pRouteItem.mHandlerInfo == this.mHandlerInfo;
        }

        /// <summary>
        /// Gets the hascode.
        /// </summary>
        /// <returns>The hashcode.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Checks whether two route item are equal or not.
        /// </summary>
        /// <param name="pRouteItem1"></param>
        /// <param name="pRouteItem2"></param>
        /// <returns>True if equal, false otherwise.</returns>
        public static bool operator ==(RouteItem pRouteItem1, RouteItem pRouteItem2)
        {
            return pRouteItem1.Equals( pRouteItem2 );
        }

        /// <summary>
        /// Checks whether two route item are different or not.
        /// </summary>
        /// <param name="pRouteItem1"></param>
        /// <param name="pRouteItem2"></param>
        /// <returns>True if different, false otherwise.</returns>
        public static bool operator !=(RouteItem pRouteItem1, RouteItem pRouteItem2)
        {
            return pRouteItem1.Equals( pRouteItem2 ) == false;
        }

        #endregion Operations
    }
}
