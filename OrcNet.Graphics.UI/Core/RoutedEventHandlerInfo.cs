﻿using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Container for handler instance and other
    /// invocation preferences for this handler instance
    /// </summary>
    internal class RoutedEventHandlerInfo
    {
        #region Fields

        /// <summary>
        /// Stores the delegate to run.
        /// </summary>
        private Delegate mDelegate;

        /// <summary>
        /// Stores the flag indicating whether already handled events must have their
        /// delegate invocated as well or not.
        /// </summary>
        private bool     mProcessHandledEvents;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the associated delegate to run.
        /// </summary>
        internal Delegate Handler
        {
            get
            {
                return this.mDelegate;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether already handled events must have their
        /// delegate invocated as well or not.
        /// </summary>
        internal bool ProcessHandledEvents
        {
            get
            {
                return this.mProcessHandledEvents;
            }
        }

        #endregion Properties

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="RoutedEventHandlerInfo"/> class.
        /// </summary>
        /// <param name="pDelegate">The delegate to run.</param>
        /// <param name="pProcessHandledEvents">he flag indicating whether already handled events must have their delegate invocated as well or not.</param>
        internal RoutedEventHandlerInfo(Delegate pDelegate, bool pProcessHandledEvents)
        {
            this.mDelegate = pDelegate;
            this.mProcessHandledEvents = pProcessHandledEvents;
        }

        #endregion Construction      

        #region Methods
        
        /// <summary>
        /// Triggers the associated delegate to run.
        /// </summary>
        /// <param name="pTarget">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void RunDelegate(object pTarget, RoutedEventArgs pEventArgs)
        {
            if 
                ( pEventArgs.IsHandled == false || 
                  this.mProcessHandledEvents )
            {
                if 
                    ( this.mDelegate is RoutedEventDelegate )
                {
                    ((RoutedEventDelegate)this.mDelegate)( pTarget, pEventArgs );
                }
                else
                {
                    pEventArgs.RunDelegate( this.mDelegate, pTarget );
                }
            }
        }

        /// <summary>
        /// Checks whether the given object is equal to this one.
        /// </summary>
        /// <param name="pObj">The object this one must be tested with.</param>
        /// <returns>True of both are equal, false otherwise.</returns>
        public override bool Equals(object pObj)
        {
            if 
                ( pObj == null || 
                  (pObj is RoutedEventHandlerInfo) == false )
            {
                return false;
            }

            return this.Equals( (RoutedEventHandlerInfo)pObj );
        }

        /// <summary>
        /// Checks whether the given routed event handler info is equal to this one.
        /// </summary>
        /// <param name="pHandlerInfo">The handler info this one must be tested with.</param>
        /// <returns>True of both are equal, false otherwise.</returns>
        public bool Equals(RoutedEventHandlerInfo pHandlerInfo)
        {
            return this.mDelegate == pHandlerInfo.mDelegate && 
                   this.mProcessHandledEvents == pHandlerInfo.mProcessHandledEvents;
        }

        /// <summary>
        /// Gets the hash code.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Checks whether two handler info objects are equal or not.
        /// </summary>
        /// <param name="pHandlerInfo1"></param>
        /// <param name="pHandlerInfo2"></param>
        /// <returns>True if equal, false otherwise.</returns>
        public static bool operator == (RoutedEventHandlerInfo pHandlerInfo1, RoutedEventHandlerInfo pHandlerInfo2)
        {
            return pHandlerInfo1.Equals( pHandlerInfo2 );
        }

        /// <summary>
        /// Checks whether two handler info objects are different or not.
        /// </summary>
        /// <param name="pHandlerInfo1"></param>
        /// <param name="pHandlerInfo2"></param>
        /// <returns>True is different, false otherwise.</returns>
        public static bool operator != (RoutedEventHandlerInfo pHandlerInfo1, RoutedEventHandlerInfo pHandlerInfo2)
        {
            return pHandlerInfo1.Equals( pHandlerInfo2 ) == false;
        }

        #endregion Methods
    }
}
