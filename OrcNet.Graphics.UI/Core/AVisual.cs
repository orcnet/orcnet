﻿using OpenTK;
using OrcNet.Core.Service;
using OrcNet.Graphics.UI.Maths;
using OrcNet.Graphics.UI.Render;
using OrcNet.Graphics.UI.Services;
using System;
using System.Diagnostics;
using static OrcNet.Graphics.UI.Core.AHitTestResult;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Base renderable visual abstract class definition
    /// providing coordinate transformations and bounding
    /// box computation.
    /// </summary>
    public abstract class AVisual : DependencyObject
    {
        #region Constants

        /// <summary>
        /// Stores the constant visual tree level limit of node.
        /// </summary>
        private const uint cTreeLevelLimit = 0x7FF;

        /// <summary>
        /// Stores the dirty mask for a visual at start up and reset by the end of the render pass.
        /// </summary>
        private const VisualFlags c_FlagsDirtyMask = VisualFlags.IsSubtreeDirtyForRender | 
                                                     VisualFlags.IsContentDirty | 
                                                     VisualFlags.IsTransformDirty | 
                                                     VisualFlags.IsClipDirty | 
                                                     VisualFlags.IsOpacityDirty | 
                                                     VisualFlags.IsOffsetDirty | 
                                                     VisualFlags.IsScrollableAreaClipDirty;

        #endregion Constants

        #region Delegates

        /// <summary>
        /// Ancestor changed event handler prototype definition.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal delegate void AncestorChangedEventHandler(object pSender, AncestorChangedEventArgs pEventArgs);

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the ancestor changed event handler.
        /// </summary>
        private AncestorChangedEventHandler mAncestorChangedEventHandler;

        /// <summary>
        /// Stores the bbox in inner coordinate space of this node including its children.
        /// </summary>
        private Rect mBBoxSubgraph = Rect.Empty;

        /// <summary>
        /// Stores the scrollable area clip for the AVisual.
        /// </summary>
        private Rect? mVisualScrollableAreaClip;
        
        /// <summary>
        /// Stores the visual parent
        /// </summary>
        private DependencyObject mParent;

        /// <summary>
        /// Stores the visual transformation.
        /// </summary>
        private ATransform mTransform;

        /// <summary>
        /// Stores the visual offset.
        /// </summary>
        private Vector2d    mOffset;

        /// <summary>
        /// Stores the visual opacity.
        /// </summary>
        private double      mOpacity;

        /// <summary>
        /// Stores the visual state pFlags.
        /// </summary>
        private VisualFlags mFlags;

        #endregion Fields

        #region Events

        /// <summary>
        /// Adds or removes delegates to the VisualAncenstorChanged Event.
        /// </summary>
        /// <remarks>This also sets/clears the tree-searching bit up the tree.</remarks>
        internal event AncestorChangedEventHandler VisualAncestorChanged
        {
            add
            {
                if 
                    ( this.mAncestorChangedEventHandler == null )
                {
                    this.mAncestorChangedEventHandler = value;
                }
                else
                {
                    this.mAncestorChangedEventHandler += value;
                }
                
                SetTreeBits( this,
                             VisualFlags.SubTreeHoldsAncestorChanged,
                             VisualFlags.RegisteredForAncestorChanged );
            }
            remove
            {
                // check that we are Disabling a node that was previously Enabled
                if 
                    ( this.CheckFlagsAnd( VisualFlags.SubTreeHoldsAncestorChanged ) )
                {
                    ClearTreeBits( this,
                                   VisualFlags.SubTreeHoldsAncestorChanged,
                                   VisualFlags.RegisteredForAncestorChanged );
                }

                // if we are Disabling a Visual that was not Enabled then this
                // search should fail.  But it is safe to check.
                if 
                    ( this.mAncestorChangedEventHandler != null )
                {
                    this.mAncestorChangedEventHandler -= value;
                }
            }
        }
        
        #endregion Events

        #region Properties

        ///<Summary>
        ///Flag to check if this visual has any children
        ///</Summary>
        internal bool HasVisualChildren
        {
            get
            {
                return ((mFlags & VisualFlags.HasChildren) != 0);
            }
        }

        /// <summary>
        /// Gets the children lCount.
        /// </summary>
        protected internal virtual int VisualChildrenCount
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the visual parent.
        /// </summary>
        protected internal DependencyObject VisualParent
        {
            get
            {
                return this.mParent;
            }
        }
        
        /// <summary>
        /// Gets the visual transformation.
        /// </summary>
        protected internal ATransform VisualTransform
        {
            get
            {
                return this.mTransform;
            }
            protected set
            {
                if
                    ( this.mTransform != value )
                {
                    this.mTransform = value;

                    this.SetFlags( true, VisualFlags.IsTransformDirty );

                    this.PropagateChangedFlags();
                }
            }
        }

        /// <summary>
        /// Gets the visual tree element's level.
        /// </summary>
        internal uint TreeLevel
        {
            get
            {
                return ((uint)this.mFlags & 0xFFE00000) >> 21;
            }
            set
            {
                if 
                    ( value > cTreeLevelLimit )
                {
                    throw new InvalidOperationException( "Visual element exceeding the maximum tree node count!!!" );
                }

                this.mFlags = (VisualFlags)(((uint)this.mFlags & 0x001FFFFF) | (value << 21));
            }
        }

        /// <summary>
        /// Gets the bounding box for the contents of the current visual. 
        /// </summary> 
        internal Rect VisualContentBounds
        {
            get
            {
                return GetContentBounds();
            }
        }
        
        /// <summary>
        /// Gets the union of all of the content bounding boxes for all of the descendants of the current visual,
        /// and also including the contents of the current visual. 
        /// So we end up with the bounds of the whole sub-graph in inner space. 
        /// </summary> 
        internal Rect VisualDescendantBounds
        {
            get
            {
                Rect lBBoxSubGraph = this.CalculateSubgraphBoundsInnerSpace();

                // If bounding box has NaN, then we set the bounding box to infinity.
                if 
                    ( DoubleUtil.RectHasNaN( lBBoxSubGraph ) )
                {
                    lBBoxSubGraph.X = Double.NegativeInfinity;
                    lBBoxSubGraph.Y = Double.NegativeInfinity;
                    lBBoxSubGraph.Width  = Double.PositiveInfinity;
                    lBBoxSubGraph.Height = Double.PositiveInfinity;
                }

                return lBBoxSubGraph;
            }
        }
        
        /// <summary>
        /// Gets or sets whether the visual is the root element or not.
        /// The root element is in charge of triggering the rendering.
        /// </summary>
        internal bool IsRootElement
        {
            get
            {
                return this.CheckFlagsAnd( VisualFlags.ShouldPostRender );
            }
            set
            {
                this.SetFlags( value, VisualFlags.ShouldPostRender );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether it is iterating through the visual children or not.
        /// This flag is set during a descendents walk, for property invalidation.
        /// </summary>
        internal bool IsVisualChildrenIterationInProgress
        {
            get
            {
                return this.CheckFlagsAnd( VisualFlags.IsVisualChildrenIterationInProgress );
            }
            set
            {
                this.SetFlags( value, VisualFlags.IsVisualChildrenIterationInProgress );
            }
        }

        /// <summary>
        /// Gets the scrollable area clip for the AVisual.
        /// </summary>
        protected internal Rect? VisualScrollableAreaClip
        {
            get
            {
                return this.mVisualScrollableAreaClip;
            }
            protected set
            {
                if 
                    ( this.mVisualScrollableAreaClip != value )
                {
                    this.mVisualScrollableAreaClip = value;
                    
                    this.SetFlags( true, VisualFlags.IsScrollableAreaClipDirty );

                    this.PropagateChangedFlags();
                }
            }
        }

        /// <summary>
        /// Gets the offset of the visual.
        /// </summary>
        protected internal Vector2d VisualOffset
        {
            get
            {
                return this.mOffset;
            }
            protected set
            {
                if 
                    ( this.mOffset != value )
                {
                    this.mOffset = value;

                    this.SetFlags( true, VisualFlags.IsOffsetDirty );

                    PropagateFlags( this,
                                    VisualFlags.IsSubtreeDirtyForPrecompute | VisualFlags.IsSubtreeDirtyForRender );
                }
            }
        }

        /// <summary>
        /// Gets the opacity of the Visual.
        /// </summary>
        protected internal double VisualOpacity
        {
            get
            {
                return this.mOpacity;
            }
            protected set
            {
                if 
                    ( this.mOpacity != value )
                {
                    this.mOpacity = value;

                    SetFlags( true, VisualFlags.IsOpacityDirty );

                    PropagateFlags( this,
                                    VisualFlags.IsSubtreeDirtyForRender );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AVisual"/> class.
        /// </summary>
        protected AVisual()
        {
            this.mParent = null;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Returns true if the specified ancestor (this) is really the ancestor of the
        /// given descendant (argument).
        /// </summary>
        /// <param name="pDescendant">The descendant object.</param>
        /// <returns>True if this is ancestor of the supplied descendant, false otherwise.</returns>
        public bool IsAncestorOf(DependencyObject pDescendant)
        {
            AVisual lVisual = pDescendant as AVisual;
            if 
                ( lVisual != null )
            {
                return lVisual.IsDescendantOf( this );
            }

            return false;
        }

        /// <summary>
        /// Returns true if the refernece AVisual (this) is a descendant of the argument AVisual. 
        /// </summary>
        /// <param name="pAncestor">The ancestor object.</param>
        /// <returns>True if this is descendant of the supplied ancestor, false otherwise.</returns>
        public bool IsDescendantOf(DependencyObject pAncestor)
        {
            if 
                ( pAncestor == null )
            {
                throw new ArgumentNullException( "pAncestor" );
            }
            
            // Walk up the parent chain of the descendant until we run out 
            // of 2D parents or we find the ancestor. 
            DependencyObject lCurrent = this;
            while 
                ( lCurrent != null && 
                  lCurrent != pAncestor )
            {
                AVisual lCurrentAsVisual = lCurrent as AVisual;
                if 
                    ( lCurrentAsVisual != null )
                {
                    lCurrent = lCurrentAsVisual.mParent;
                }
                else
                {
                    lCurrent = null;
                }
            }

            return lCurrent == pAncestor;
        }

        /// <summary>
        /// Finds the common ancestor of two Visuals.
        /// </summary>
        /// <param name="pOtherVisual">The other visual to look for a common visual ancestor with that one.</param>
        /// <returns>Returns the common ancestor if the Visuals have one or otherwise null.</returns>
        /// <exception cref="ArgumentNullException">If the argument is null.</exception>
        public DependencyObject FindCommonVisualAncestor(DependencyObject pOtherVisual)
        {
            AVisual lOtherVisual = pOtherVisual as AVisual;
            if
                ( lOtherVisual == null )
            {
                throw new ArgumentNullException(" OtherVisual is not a AVisual ");
            }

            // Since we can't rely on code running in the CLR, we need to first make sure
            // that the FindCommonAncestor flag is not set. It is enought to ensure this
            // on one path to the root Visual.
            
            this.SetFlagsToRoot( false, VisualFlags.FindCommonAncestor );

            // Walk up the other visual's parent chain and set the FindCommonAncestor flag.
            lOtherVisual.SetFlagsToRoot( true, VisualFlags.FindCommonAncestor );

            // Now see if the other Visual's parent chain crosses our parent chain.
            return FindFirstAncestorWithFlagsAnd( VisualFlags.FindCommonAncestor );
        }

        /// <summary>
        /// Returns a transform that can be used to transform coordinate from this
        /// node to the specified ancestor.  It allows 3D to be between the 2D nodes.
        /// </summary>
        /// <param name="pAncestor">The ancestor regarding to which the transform must be supplied.</param>
        /// <exception cref="ArgumentNullException">If ancestor is null.</exception>
        /// <exception cref="ArgumentException">If the ancestor Visual is not a ancestor of Visual.</exception>
        /// <exception cref="InvalidOperationException">If the Visuals are not connected.</exception>
        public AGeneralTransform TransformToAncestor(AVisual pAncestor)
        {
            if 
                ( pAncestor == null )
            {
                throw new ArgumentNullException("pAncestor");
            }
            
            return InternalTransformToAncestor( pAncestor, false );
        }

        /// <summary>
        /// Returns a transform that can be used to transform coordinates from this
        /// node to the specified descendant, or null if the transform from descendant to "this"
        /// is non-invertible.  It allows 3D to be between the 2D nodes.
        /// </summary>
        /// <param name="pDescendant">The descendant regarding to which the transform must be supplied.</param>
        /// <exception cref="ArgumentException">If the reference Visual is not a ancestor of the descendant Visual.</exception>
        /// <exception cref="ArgumentNullException">If the descendant argument is null.</exception>
        /// <exception cref="InvalidOperationException">If the Visuals are not connected.</exception>
        public AGeneralTransform TransformToDescendant(AVisual pDescendant)
        {
            if 
                ( pDescendant == null )
            {
                throw new ArgumentNullException("pDescendant");
            }
 
            return pDescendant.InternalTransformToAncestor( this, true );
        }

        /// <summary>
        /// This method converts a point in the current Visual's coordinate
        /// system into a point in screen coordinates.
        /// </summary>
        /// <param name="pPoint">The point to transform.</param>
        public Point PointToScreen(Point pPoint)
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            PresentationSource lSource = lUIService.PresentationSource;
            if 
                ( lSource == null )
            {
                throw new InvalidOperationException( "No presentation source!!!" );
            }

            // Translate the point from the visual to the root.
            AGeneralTransform lGeneralTransformUp = this.TransformToAncestor( lSource.RootVisual );
            if 
                ( lGeneralTransformUp == null ||
                  lGeneralTransformUp.TryTransform( pPoint, out pPoint ) == false )
            {
                throw new InvalidOperationException( "Cannot transform point!!!" );
            }

            pPoint = PointUtil.RootToClient( pPoint, lSource );
            pPoint = PointUtil.ClientToScreen( pPoint, lSource );

            return pPoint;
        }

        /// <summary>
        /// This method converts a point in screen coordinates into a point
        /// in the current Visual's coordinate system.
        /// </summary>
        /// <param name="pPoint">The point to transform.</param>
        public Point PointFromScreen(Point pPoint)
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            PresentationSource lSource = lUIService.PresentationSource;
            if 
                ( lSource == null )
            {
                throw new InvalidOperationException( "No presentation source!!!" );
            }

            // Translate the point from the screen to the root
            pPoint = PointUtil.ScreenToClient( pPoint, lSource );
            pPoint = PointUtil.ClientToRoot( pPoint, lSource );

            // Translate the point from the root to the visual.
            AGeneralTransform lGeneralTransformDown = lSource.RootVisual.TransformToDescendant( this );
            if 
                ( lGeneralTransformDown == null || 
                  lGeneralTransformDown.TryTransform( pPoint, out pPoint ) == false )
            {
                throw new InvalidOperationException( "Cannot transform point!!!" );
            }

            return pPoint;
        }

        #region Methods Internal

        /// <summary>
        /// Derived class must implement to support AVisual children. The method must return 
        /// the lChild at the specified index. Index must be between 0 and GetVisualChildrenCount-1. 
        /// By default a AVisual does not have any children.
        /// </summary>
        protected internal virtual AVisual GetVisualChild(int pIndex)
        {
            throw new ArgumentOutOfRangeException("pIndex", pIndex, "Out of range index!!!");
        }
        
        /// <summary>
        /// Derived classes must override this method and return the bounding 
        /// box of their content.
        /// </summary> 
        internal virtual Rect GetContentBounds()
        {
            return Rect.Empty;
        }
        
        /// <summary> 
        /// RenderContent is implemented by derived classes to hook up their
        /// content.
        /// </summary> 
        internal virtual void RenderContent()
        {
            /* do nothing */
        }

        /// <summary> 
        /// This method is overrided on Visuals that can instantiate IDrawingContext 
        /// like UIElement.
        /// </summary> 
        internal virtual void RenderClose(IDrawingContent pContent)
        {

        }

        /// <summary>
        /// Computes the union of all content bounding boxes of this AVisual's sub-graph 
        /// in inner space. Note that the result includes the root AVisual's content.
        /// 
        /// Definition: Outer/Inner Space: 
        ///
        ///      A AVisual has a set of properties which include clip, lTransform, offset 
        ///      and bitmap effect. Those properties affect in which space (coordinate
        ///      clip space) a AVisual's vector graphics and sub-graph is interpreted.
        ///      Inner space is the space before applying any of the properties. Outer
        ///      space is the space where all the properties have been taken into account. 
        ///      For example if the AVisual draws a rectangle {0, 0, 100, 100} and the
        ///      Offset property is set to {20, 20} and the clip is set to {40, 40, 10, 10} 
        ///      then the bounding box of the AVisual in inner space is {0, 0, 100, 100} and 
        ///      in outer space {60, 60, 10, 10} (start out with the bbox of {0, 0, 100, 100}
        ///      then apply the clip {40, 40, 10, 10} which leaves us with a bbox of 
        ///      {40, 40, 10, 10} and finally apply the offset and we end up with a bbox
        ///      of {60, 60, 10, 10}
        /// </summary>
        internal Rect CalculateSubgraphBoundsInnerSpace()
        {
            return this.CalculateSubgraphBoundsInnerSpace( false );
        }

        /// <summary> 
        /// Computes the union of all rendering bounding boxes of this AVisual's sub-graph
        /// in inner space. Note that the result includes the root AVisual's content.
        /// </summary>
        internal Rect CalculateSubgraphRenderBoundsInnerSpace()
        {
            return this.CalculateSubgraphBoundsInnerSpace( true );
        }

        /// <summary> 
        /// Same as the parameterless CalculateSubgraphBoundsInnerSpace except it takes a
        /// boolean indicating whether or not to calculate the rendering bounds.
        /// If the pRenderBounds parameter is set to true then the render bounds are returned.
        /// The render bounds differ in that they treat zero area bounds as emtpy rectangles.
        /// </summary>
        internal virtual Rect CalculateSubgraphBoundsInnerSpace(bool pRenderBounds)
        {
            Rect lBBoxSubGraph = Rect.Empty;

            // Recursively calculate sub-graph bounds of children of this node. We get 
            // the bounds of each Child AVisual in outer space which is this AVisual's
            // inner space and union them together.
            int lCount = this.VisualChildrenCount;
            for 
                ( int lCurrent = 0; lCurrent < lCount; lCurrent++ )
            {
                AVisual lChild = this.GetVisualChild( lCurrent );
                if
                    ( lChild != null )
                {
                    Rect lBBoxSubgraphChild = lChild.CalculateSubgraphBoundsOuterSpace( pRenderBounds );
                    lBBoxSubGraph.Union( lBBoxSubgraphChild );
                }
            }

            // Get the content bounds of the AVisual. 
            // In the case that we're interested in rendering bounds (lCurrent.pVisual. what MIL will consider the size of the object), we set 0 area rects 
            // to be empty so that they don't union to create larger sized rects.
            Rect lContentBounds = this.GetContentBounds();
            if 
                ( pRenderBounds && IsEmptyRenderBounds( ref lContentBounds /* ref for perf - not modified */ ) )
            {
                lContentBounds = Rect.Empty;
            }

            // Union the content bounds to the sub-graph bounds so that we end up with the 
            // bounds of the whole sub-graph in inner space and return it. 
            lBBoxSubGraph.Union( lContentBounds );

            return lBBoxSubGraph;
        }
        
        /// <summary> 
        /// Computes the union of all content bounding boxes of this AVisual's sub-graph 
        /// in outer space. Note that the result includes the root AVisual's content.
        /// For a definition of outer/inner space see CalculateSubgraphBoundsInnerSpace. 
        /// </summary>
        internal Rect CalculateSubgraphBoundsOuterSpace()
        {
            return this.CalculateSubgraphBoundsOuterSpace( false /* pRenderBounds */ );
        }

        /// <summary> 
        /// Computes the union of all rendering bounding boxes of this AVisual's sub-graph
        /// in outer space. Note that the result includes the root AVisual's content. 
        /// For a definition of outer/inner space see CalculateSubgraphBoundsInnerSpace.
        /// </summary>
        internal Rect CalculateSubgraphRenderBoundsOuterSpace()
        {
            return this.CalculateSubgraphBoundsOuterSpace( true /* pRenderBounds */ );
        }

        /// <summary>
        /// Same as the parameterless CalculateSubgraphBoundsOuterSpace except it takes a 
        /// boolean indicating whether or not to calculate the rendering bounds.
        /// If the renderBounds parameter is set to true then the render bounds are returned.
        /// The render bounds differ in that they treat zero area bounds as emtpy rectangles.
        /// </summary>
        /// <param name="pRenderBounds">The flag indicating whether or not to involve the rendering bounds.</param>
        private Rect CalculateSubgraphBoundsOuterSpace(bool pRenderBounds)
        {
            Rect lBBoxSubGraph = Rect.Empty;

            // Get the inner space bounding box of this node and then lTransform it into outer 
            // space.
            lBBoxSubGraph = this.CalculateSubgraphBoundsInnerSpace( pRenderBounds );
            
            // Apply Transform. 
            ATransform lTransform = this.mTransform;
            if 
                ( lTransform != null && 
                  lTransform.IsIdentity == false )
            {
                Matrix lMatrix = lTransform.Value;
                MatrixUtil.TransformRect( ref lBBoxSubGraph, ref lMatrix );
            }

            // Apply Offset. 
            if 
                ( lBBoxSubGraph.IsEmpty == false )
            {
                lBBoxSubGraph.X += mOffset.X;
                lBBoxSubGraph.Y += mOffset.Y;
            }

            // Apply scrollable-area clip.
            Rect? lScrollClip = this.mVisualScrollableAreaClip;
            if 
                ( lScrollClip.HasValue )
            {
                lBBoxSubGraph.Intersect( lScrollClip.Value );
            }

            // If bounding box has NaN, then we set the bounding box to infinity. 
            if 
                ( DoubleUtil.RectHasNaN( lBBoxSubGraph ) )
            {
                lBBoxSubGraph.X      = double.NegativeInfinity;
                lBBoxSubGraph.Y      = double.NegativeInfinity;
                lBBoxSubGraph.Width  = double.PositiveInfinity;
                lBBoxSubGraph.Height = double.PositiveInfinity;
            }

            return lBBoxSubGraph;
        }

        /// <summary>
        /// This method returns true if the given WPF bounds will be considered 
        /// empty in terms of rendering.  This is the case when the bounds describe
        /// a zero-area space.  bounds are passed by ref for speed but are not modified 
        /// 
        /// NTRAID#Longhorn-111639-2007/07/05-kurtb:
        /// See above CalculateSubgraphBounds* methods for more detail.  This helper method 
        /// goes with them.
        /// </summary>
        private bool IsEmptyRenderBounds(ref Rect pBounds)
        {
            return (pBounds.Width <= 0 || pBounds.Height <= 0);
        }
        
        /// <summary>
        /// Internal delegate called when the parent has changed. 
        /// </summary>
        /// <param name="pOldParent">The old parent or null if the AVisual did not have a parent before.</param>
        protected internal virtual void OnVisualParentChanged(DependencyObject pOldParent)
        {
            // Override it...
        }

        /// <summary>
        /// Internal delegate called when the VisualCollection has been edited.
        /// </summary>
        /// <param name="pChildAdded">The lChild added if new addition, null otherwise.</param>
        /// <param name="pChildRemoved">The lChild removed if new removal, null otherwise.</param>
        protected internal virtual void OnVisualChildrenChanged(DependencyObject pChildAdded, DependencyObject pChildRemoved)
        {
            // Override it...
        }

        /// <summary>
        /// Used by derived classes to invalidate their hit-test bounds.
        /// </summary>
        internal void InvalidateHitTestBounds()
        {
            PropagateFlags( this,
                            VisualFlags.IsSubtreeDirtyForPrecompute );
        }

        /// <summary>
        /// Gets the visual's bounds for Hit test purpose.
        /// </summary>
        /// <returns>The rectangle being the bounds.</returns>
        internal virtual Rect GetHitTestBounds()
        {
            return this.GetContentBounds();
        }
        
        /// <summary>
        /// Gets the top most visual of a hit test.
        /// </summary>
        /// <param name="pPoint">The point origin of the hit pass.</param>
        internal AHitTestResult HitTest(Point pPoint)
        {
            TopMostHitResult lResult = new TopMostHitResult();

            this.HitTest( new HitTestFilterCallback( lResult.NoNested2DFilter ),
                          new HitTestResultCallback( lResult.HitTestResult ),
                          new PointHitTestParameters( pPoint ) );

            return lResult.HitResult;
        }

        /// <summary>
        /// Initiate a hit test using delegates.
        /// </summary>
        /// <param name="pFilterCallback">The filtering hit test callback.</param>
        /// <param name="pResultCallback">The hit test result callback.</param>
        /// <param name="pHitTestParameters">The hit test parameters.</param>
        internal void HitTest(HitTestFilterCallback pFilterCallback,
                              HitTestResultCallback pResultCallback,
                              AHitTestParameters pHitTestParameters)
        {
            if 
                ( pResultCallback == null )
            {
                throw new ArgumentNullException("pResultCallback");
            }

            if 
                ( pHitTestParameters == null )
            {
                throw new ArgumentNullException("pHitTestParameters");
            }
            
            this.Precompute();

            PointHitTestParameters lPointParams = pHitTestParameters as PointHitTestParameters;
            if 
                ( lPointParams != null )
            {
                // Because we call dynamic code during the hit testing walk we need to back up
                // the original hit point in case the user's delegate throws an exception so that
                // we can restore it.
                Point lBackupHitPoint = lPointParams.HitPoint;
                try
                {
                    HitTestPoint( pFilterCallback, pResultCallback, lPointParams );
                }
                catch
                {
                    // If an exception occured, restore the user's hit point and rethrow.
                    lPointParams.HitPoint = lBackupHitPoint;

                    throw;
                }
                finally
                {
                    Debug.Assert( Point.Equals(lPointParams.HitPoint, lBackupHitPoint), 
                                  "Failed to restore user's hit point back to the original coordinate system.");
                }
            }
//            else
//            {
//                GeometryHitTestParameters lGeometryParams = pHitTestParameters as GeometryHitTestParameters;
//                if
//                    ( lGeometryParams != null )
//                {
//                    // Because we call dynamic code during the hit testing walk we need to ensure
//                    // that if the user's delegate throws an exception we restore the original
//                    // transform on the hit test geometry.
//#if DEBUG
//                    // Internally we replace the hit geometry with a copy which is guaranteed to have
//                    // a MatrixTransform so we do not need to worry about null dereferences here.
//                    Matrix lOriginalMatrix = lGeometryParams.InternalHitGeometry.Transform.Value;
//#endif // DEBUG
//                    try
//                    {
//                        HitTestGeometry( pFilterCallback, pResultCallback, lGeometryParams );
//                    }
//                    catch
//                    {
//                        lGeometryParams.EmergencyRestoreOriginalTransform();

//                        throw;
//                    }
//#if DEBUG
//                    finally
//                    {
//                        Debug.Assert( Matrix.Equals( lGeometryParams.InternalHitGeometry.Transform.Value, lOriginalMatrix ),
//                                      "Failed to restore user's hit geometry back to the original coordinate system." );
//                    }
//#endif // DEBUG
//                }
//                else
//                {
//                    // This should never happen, users can not extend the abstract HitTestParameters class.
//                    LogManager.Instance.Log( string.Format( "'{0}' HitTestParameters are not supported on {1}.", pHitTestParameters.GetType().Name, this.GetType().Name ) );
//                }
//            }
        }

        /// <summary>
        /// Internal point hit test.
        /// </summary>
        /// <param name="pFilterCallback">The filtering hit test callback.</param>
        /// <param name="pResultCallback">The hit test result callback.</param>
        /// <param name="pPointParams">The point hit test parameters.</param>
        /// <returns>The hit test result behavior state.</returns>
        internal HitTestResultBehavior HitTestPoint(HitTestFilterCallback pFilterCallback,
                                                    HitTestResultCallback pResultCallback,
                                                    PointHitTestParameters pPointParams)
        {
            // Before we continue hit-testing we check against the hit-test bounds for the sub-graph.
            // If the point is not with-in the hit-test bounds, the sub-graph can be skipped.
            if 
                ( this.mBBoxSubgraph.Contains(pPointParams.HitPoint) ) // Check that the hit-point is with-in the clip.
            {
                //
                // Determine if there is a special lFilter behavior defined for this
                // AVisual.
                //
 
                HitTestFilterBehavior lFilter = HitTestFilterBehavior.Continue;
                if 
                    ( pFilterCallback != null )
                {
                    lFilter = pFilterCallback( this );
 
                    if 
                        ( lFilter == HitTestFilterBehavior.ContinueSkipSelfAndChildren )
                    {
                        return HitTestResultBehavior.Continue;
                    }
 
                    if 
                        ( lFilter == HitTestFilterBehavior.Stop )
                    {
                        return HitTestResultBehavior.Stop;
                    }
                }
 
                // if there is a bitmap effect transform the point
                // Backup the hit point so that we can restore it later on.
                Point lOriginalHitPoint = pPointParams.HitPoint;
                Point lHitPoint = lOriginalHitPoint;
 
                //
                // Hit test against the children.
                //
                if 
                    ( lFilter != HitTestFilterBehavior.ContinueSkipChildren )
                {
                    int lChildCount = this.VisualChildrenCount;
                    for 
                        ( int lCurr = lChildCount-1; lCurr >= 0; lCurr-- )
                    {
                        AVisual lChild = this.GetVisualChild( lCurr );
                        if 
                            ( lChild != null )
                        {
                            // Hit the scollClip bounds first, which are in the lChild's outer-space.
                            Rect? lScrollClip = lChild.mVisualScrollableAreaClip;
                            if 
                                ( lScrollClip.HasValue && 
                                  lScrollClip.Value.Contains( lHitPoint ) == false )
                            {
                                // Skip lChild if the point is not within the ScrollableClip.
                                continue;
                            }
 
                            //
                            // Transform the hit-test point below offset and transform.
                            //
                            Point lNewHitPoint = lHitPoint;
 
                            // Apply the offset.
                            lNewHitPoint = lNewHitPoint - lChild.mOffset;
 
                            // If we have a transform, apply the transform.
                            ATransform lChildTransform = lChild.mTransform;
                            if 
                                ( lChildTransform != null )
                            {
                                Matrix lInverse = lChildTransform.Value;
 
                                // If we can't invert the transform, the lChild is not hitable. This makes sense since
                                // the node's rendered content is degenerate, lCurr.e. does not really take up any space.
                                // Skip the lChild by continuing in the loop.
                                if 
                                    ( lInverse.HasInverse == false )
                                {
                                    continue;
                                }
 
                                lInverse.Invert();
 
                                lNewHitPoint = lNewHitPoint * lInverse;
                            }
 
                            // Set the new hittesting point into the hittest params.
                            pPointParams.HitPoint = lNewHitPoint;
 
                            // Perform the hit-test against the lChild.
                            HitTestResultBehavior lResult = lChild.HitTestPoint( pFilterCallback, pResultCallback, pPointParams );
 
                            // Restore the hit-test point.
                            pPointParams.HitPoint = lOriginalHitPoint;

                            if 
                                ( lResult == HitTestResultBehavior.Stop )
                            {
                                return HitTestResultBehavior.Stop;
                            }
                        }
                    }
                }
 
                //
                // Hit test against the content of this AVisual.
                //
                if 
                    ( lFilter != HitTestFilterBehavior.ContinueSkipSelf )
                {
                    // set the transformed hit point
                    pPointParams.HitPoint = lHitPoint;
 
                    HitTestResultBehavior lResult = this.HitTestPointInternal( pFilterCallback, pResultCallback, pPointParams );
 
                    // restore the hit point back to its original
                    pPointParams.HitPoint = lOriginalHitPoint;
 
                    if 
                        ( lResult == HitTestResultBehavior.Stop )
                    {
                        return HitTestResultBehavior.Stop;
                    }
                }
            }
 
            return HitTestResultBehavior.Continue;
        }

        /// <summary>
        /// Internal point hit test. (Overridable)
        /// </summary>
        /// <param name="pFilterCallback">The filtering hit test callback.</param>
        /// <param name="pResultCallback">The hit test result callback.</param>
        /// <param name="pPointParams">The point hit test parameters.</param>
        /// <returns>The hit test result behavior state.</returns>
        internal virtual HitTestResultBehavior HitTestPointInternal(HitTestFilterCallback pFilterCallback,
                                                                    HitTestResultCallback pResultCallback,
                                                                    PointHitTestParameters pPointParams)
        {
            AHitTestResult lHitResult = this.HitTestCore( pPointParams );
            if 
                ( lHitResult != null )
            {
                return pResultCallback( lHitResult );
            }

            return HitTestResultBehavior.Continue;
        }

        /// <summary>
        /// HitTestCore implements whether we have hit the bounds of this visual.
        /// </summary>
        /// <param name="pPointParams">The point hit test parameters.</param>
        /// <returns>The hit result data.</returns>
        protected virtual AHitTestResult HitTestCore(PointHitTestParameters pPointParams)
        {
            if 
                ( pPointParams == null )
            {
                throw new ArgumentNullException( "pPointParams" );
            }

            // If we don't have a clip, or if the clip contains the point, keep going.
            if
                ( this.GetHitTestBounds().Contains( pPointParams.HitPoint ) )
            {
                return new PointHitTestResult( this, pPointParams.HitPoint );
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Precomputes the visual's bbox.
        /// </summary>
        internal void Precompute()
        {
            if 
                ( this.CheckFlagsAnd( VisualFlags.IsSubtreeDirtyForPrecompute ) )
            {
                Rect lBBoxSubgraph;
                this.PrecomputeRecursive( out lBBoxSubgraph );
            }
        }

        /// <summary>
        /// Derived class can do precomputations on their content by overriding this method.
        /// Derived classes must call the base class.
        /// </summary>
        internal virtual void PrecomputeContent()
        {
            this.mBBoxSubgraph = GetHitTestBounds();

            // If bounding box has NaN, then we set the bounding box to infinity.
            if 
                ( DoubleUtil.RectHasNaN( this.mBBoxSubgraph ) )
            {
                this.mBBoxSubgraph.X      = double.NegativeInfinity;
                this.mBBoxSubgraph.Y      = double.NegativeInfinity;
                this.mBBoxSubgraph.Width  = double.PositiveInfinity;
                this.mBBoxSubgraph.Height = double.PositiveInfinity;
            }
        }

        /// <summary>
        /// Recursive function in charge of pre computing the visual's and its children BBox.
        /// </summary>
        /// <param name="pBBoxSubgraph">The resulting bbox.</param>
        internal void PrecomputeRecursive(out Rect pBBoxSubgraph)
        {
            // Simple loop detection to avoid stack overflow in cyclic AVisual
            // scenarios. This fix is only aimed at mitigating a very common
            // VisualBrush scenario.
            bool lCanEnter = this.Enter();
            if 
                ( lCanEnter )
            {
                try
                {
                    if 
                        ( this.CheckFlagsAnd( VisualFlags.IsSubtreeDirtyForPrecompute ) )
                    {
                        this.PrecomputeContent();

                        int lChildCount = this.VisualChildrenCount;
                        for 
                            ( int lCurr = 0; lCurr < lChildCount; lCurr++ )
                        {
                            AVisual lChild = this.GetVisualChild( lCurr );
                            if 
                                ( lChild != null )
                            {
                                Rect lBBoxSubgraphChild;
                                lChild.PrecomputeRecursive( out lBBoxSubgraphChild );
                                mBBoxSubgraph.Union( lBBoxSubgraphChild );
                            }
                        }

                        this.SetFlags( false, VisualFlags.IsSubtreeDirtyForPrecompute );
                    }

                    // Bounding boxes are cached in inner space (below offset, Transform, and clip).
                    // Before returning them we need
                    // to Transform them into outer space.

                    pBBoxSubgraph = mBBoxSubgraph;
                    
                    ATransform lTransform = this.mTransform;
                    if 
                        ( lTransform != null && 
                          lTransform.IsIdentity == false )
                    {
                        Matrix lMatrix = lTransform.Value;
                        MatrixUtil.TransformRect(ref pBBoxSubgraph, ref lMatrix);
                    }

                    if 
                        ( pBBoxSubgraph.IsEmpty == false )
                    {
                        pBBoxSubgraph.X += mOffset.X;
                        pBBoxSubgraph.Y += mOffset.Y;
                    }

                    Rect? lScrollClip = this.mVisualScrollableAreaClip;
                    if 
                        ( lScrollClip.HasValue )
                    {
                        pBBoxSubgraph.Intersect( lScrollClip.Value );
                    }

                    // If lChild's bounding box has NaN, then we set the bounding box to infinity.
                    if 
                        ( DoubleUtil.RectHasNaN( pBBoxSubgraph ) )
                    {
                        pBBoxSubgraph.X      = double.NegativeInfinity;
                        pBBoxSubgraph.Y      = double.NegativeInfinity;
                        pBBoxSubgraph.Width  = double.PositiveInfinity;
                        pBBoxSubgraph.Height = double.PositiveInfinity;
                    }
                }
                finally
                {
                    this.Exit();
                }
            }
            else
            {
                pBBoxSubgraph = new Rect();
            }
        }

        /// <summary>
        /// Renders the visual and its children.
        /// </summary>
        internal void Render()
        {
            // Currently everything is sent to the compositor. IsSubtreeDirtyForRender
            // indicates that something in the sub-graph of this AVisual needs to have an update
            // sent to the compositor. Hence traverse if this bit is set. Also traverse when the
            // sub-graph has not yet been sent to the compositor.
            if 
                ( this.CheckFlagsAnd( VisualFlags.IsSubtreeDirtyForRender ) )
            {
                this.RenderRecursive();
            }
        }

        /// <summary>
        /// Recursive overridable render function.
        /// </summary>
        internal virtual void RenderRecursive()
        {
            // Simple loop detection to avoid stack overflow in cyclic AVisual
            // scenarios. This fix is only aimed at mitigating a very common
            // VisualBrush scenario.
            bool lCanEnter = this.Enter();
            if
                ( lCanEnter )
            {
                try
                {
                    this.UpdateContent( this.mFlags );

                    this.UpdateChildren();
                    
                    // Finally, reset the dirty pFlags for this visual (at this point,
                    // we have handled them all).
                    this.SetFlags( false, VisualFlags.IsSubtreeDirtyForRender );
                }
                finally
                {
                    this.Exit();
                }
            }
        }

        /// <summary>
        /// Enter is used for simple cycle detection in AVisual. If the method returns false
        /// the AVisual has already been entered and cannot be entered again. Matching invocation of Exit
        /// must be skipped if Enter returns false.
        /// </summary>
        internal bool Enter()
        {
            if 
                ( this.CheckFlagsAnd( VisualFlags.ReentrancyFlag ) )
            {
                return false;
            }
            else
            {
                this.SetFlags( true, VisualFlags.ReentrancyFlag );
                return true;
            }
        }

        /// <summary>
        /// Exits the AVisual. For more details see Enter method.
        /// </summary>
        internal void Exit()
        {
            Debug.Assert( CheckFlagsAnd( VisualFlags.ReentrancyFlag ) ); // Exit must be matched with Enter. See Enter comments.
            this.SetFlags( false, VisualFlags.ReentrancyFlag );
        }

        /// <summary>
        /// SetFlags is used to set or unset one or multiple node pFlags on the node.
        /// </summary>
        /// <param name="pValue">The flag indicating whether new bits must be activated or deactivated.</param>
        /// <param name="pFlags">The set of flags to set or unset.</param>
        internal void SetFlags(bool pValue, VisualFlags pFlags)
        {
            mFlags = pValue ? (mFlags | pFlags) : (mFlags & (~pFlags));
        }
        
        /// <summary>
        /// CheckFlagsAnd returns true if all pFlags in the bitmask pFlags are set on the node.
        /// </summary>
        /// <remarks>If there aren't any bits set on the specified pFlags the method
        /// returns true</remarks>
        internal bool CheckFlagsAnd(VisualFlags pFlags)
        {
            return (mFlags & pFlags) == pFlags;
        }
        
        /// <summary>
        /// Checks if any of the specified pFlags is set on the node.
        /// </summary>
        /// <remarks>If there aren't any bits set on the specified pFlags the method
        /// returns true</remarks>
        internal bool CheckFlagsOr(VisualFlags pFlags)
        {
            return (pFlags == 0) || ((mFlags & pFlags) > 0);
        }
        
        /// <summary>
        /// Set a bit in a AVisual node and in all its direct ancestors.
        /// </summary>
        /// <param name="pVisual">The AVisual Element</param>
        /// <param name="pTreeFlag">The Flag that marks a sub tree to search</param>
        /// <param name="pNodeFlag">The Flag that marks the node to search for.</param>
        internal static void SetTreeBits(DependencyObject pVisual,
                                         VisualFlags pTreeFlag,
                                         VisualFlags pNodeFlag)
        {
            AVisual eAsVisual;

            if (pVisual != null)
            {
                eAsVisual = pVisual as AVisual;
                if (eAsVisual != null)
                {
                    eAsVisual.SetFlags(true, pNodeFlag);
                }
            }

            while (null != pVisual)
            {
                eAsVisual = pVisual as AVisual;
                if (eAsVisual != null)
                {
                    // if the bit is already set, then we're done.
                    if (eAsVisual.CheckFlagsAnd(pTreeFlag))
                        return;

                    eAsVisual.SetFlags(true, pTreeFlag);

                    pVisual = eAsVisual.mParent;
                }
            }
        }


        /// <summary>
        /// Clean a bit in a AVisual node and in all its direct ancestors;
        /// unless the ancestor also has
        /// </summary>
        /// <param name="pVisual">The AVisual Element</param>
        /// <param name="pTreeFlag">The Flag that marks a sub tree to search</param>
        /// <param name="pNodeFlag">The Flag that marks the node to search for.</param>
        internal static void ClearTreeBits(DependencyObject pVisual,
                                           VisualFlags pTreeFlag,
                                           VisualFlags pNodeFlag)
        {
            AVisual lVisual;
            // This bit might not be set, but checking costs as much as setting
            // So it is faster to just clear it everytime.
            if 
                ( pVisual != null )
            {
                lVisual = pVisual as AVisual;
                if
                    ( lVisual != null )
                {
                    lVisual.SetFlags( false, pNodeFlag );
                }
            }

            while 
                ( pVisual != null )
            {
                lVisual = pVisual as AVisual;
                if 
                    ( lVisual != null )
                {
                    if 
                        ( lVisual.CheckFlagsAnd( pNodeFlag ) )
                    {
                        return;  // Done;   if a parent also has the Node bit set.
                    }

                    if 
                        ( DoAnyChildrenHaveABitSet( lVisual, pTreeFlag ) )
                    {
                        return;  // Done;   if a other subtrees are set.
                    }

                    lVisual.SetFlags( false, pTreeFlag );

                    pVisual = lVisual.mParent;
                }
            }
        }

        /// <summary>
        /// Check all the children for a bit.
        /// </summary>
        /// <param name="pVisual">The visual element children will be checked.</param>
        /// <param name="pFlag">The bit to look for.</param>
        /// <returns>True if has any children with the supplied bit set.</returns>
        private static bool DoAnyChildrenHaveABitSet(AVisual pVisual,
                                                     VisualFlags pFlag)
        {
            int lCount = pVisual.VisualChildrenCount;
            for
                ( int lCurr = 0; lCurr < lCount; lCurr++ )
            {
                AVisual lChild = pVisual.GetVisualChild( lCurr );
                if 
                    ( lChild != null && 
                      lChild.CheckFlagsAnd( pFlag ) )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Propagates the pFlags up to the root.
        /// </summary>
        /// <remarks>The walk stops on a node with all of the required pFlags set.</remarks>
        /// <param name="pVisual">The visual to modify the flag for.</param>
        /// <param name="pFlags">The new flags to set.</param>
        internal static void PropagateFlags(AVisual pVisual,
                                            VisualFlags pFlags)
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            while 
                ( pVisual != null &&
                  pVisual.CheckFlagsAnd( pFlags ) == false )
            {
                if 
                    ( pVisual.CheckFlagsOr( VisualFlags.ShouldPostRender ) )
                {
                    lUIService.Render();
                }

                pVisual.SetFlags( true, pFlags );

                if 
                    ( pVisual.mParent == null )
                {
                    // Stop propagating.
                    // We are at the root of the 2D subtree.
                    return;
                }
                
                pVisual = pVisual.mParent as AVisual;
            }
        }

        /// <summary>
        /// Propagates the dirty Flags up to the root.
        /// </summary>
        /// <remarks>The walk stops on a node with all of the required pFlags set.</remarks>
        internal void PropagateChangedFlags()
        {
            PropagateFlags( this,
                            VisualFlags.IsSubtreeDirtyForPrecompute | VisualFlags.IsSubtreeDirtyForRender );
        }

        /// <summary>
        /// Provides a transform that goes between the AVisual's coordinate space
        /// and that after applying the transforms that bring it to outer space.
        /// </summary>
        /// <returns>The resulting transformation.</returns>
        internal AGeneralTransform TransformToOuterSpace()
        {
            Matrix lMatrix = Matrix.Identity;
            GeneralTransformGroup lGroup = null;
            AGeneralTransform lResult = null;
            
            ATransform lTransform = this.mTransform;
            if 
                ( lTransform != null )
            {
                Matrix lThisMatrix = lTransform.Value;
                MatrixUtil.MultiplyMatrix( ref lMatrix, ref lThisMatrix );
            }

            lMatrix.Translate( this.mOffset.X, this.mOffset.Y ); // Consider having a bit that indicates that we have a non-null offset.

            if 
                ( lGroup == null )
            {
                lResult = new MatrixTransform( lMatrix );
            }
            else
            {
                lGroup.Children.Add( new MatrixTransform( lMatrix ) );
                lResult = lGroup;
            }
            
            return lResult;
        }

        /// <summary>
        /// Helper method to provide access to AddVisualChild for the VisualCollection.
        /// </summary>
        /// <param name="pChild">The new child to add.</param>
        internal void InternalAddVisualChild(AVisual pChild)
        {
            this.AddVisualChild(pChild);
        }

        /// <summary>
        /// Helper method to provide access to RemoveVisualChild for the VisualCollection.
        /// </summary>
        /// <param name="pChild">The child to remove.</param>
        internal void InternalRemoveVisualChild(AVisual pChild)
        {
            this.RemoveVisualChild(pChild);
        }

        /// <summary>
        /// Attaches a new child to that visual.
        /// Derived classes must call this method to notify the AVisual layer that a new
        /// child appeard in the children collection. The AVisual layer will then call the GetVisualChild
        /// method to find out where the child was added.
        /// </summary>
        /// <param name="pChild">The new child.</param>
        protected void AddVisualChild(AVisual pChild)
        {
            if 
                ( pChild == null )
            {
                return;
            }

            if 
                ( pChild.mParent != null )
            {
                throw new ArgumentException("The visual has already a parent!!!");
            }

            this.SetFlags( true, VisualFlags.HasChildren );

            // Set the parent pointer.
            pChild.mParent = this;

            //
            // The child might be dirty. Hence we need to propagate dirty information
            // from the parent and from the pChild.
            //
            AVisual.PropagateFlags( this,
                                    VisualFlags.IsSubtreeDirtyForPrecompute | VisualFlags.IsSubtreeDirtyForRender );

            AVisual.PropagateFlags( pChild,
                                    VisualFlags.IsSubtreeDirtyForPrecompute | VisualFlags.IsSubtreeDirtyForRender );

            //
            // Resume layout.
            //
            UIElement.PropagateResumeLayout( this, pChild );

            // Fire notifications
            this.OnVisualChildrenChanged( pChild, null /* no removed pChild */ );
            pChild.FireOnVisualParentChanged( null );
        }

        /// <summary>
        /// Detaches a child from that visual.
        /// Derived classes must call this method to notify the AVisual layer that a
        /// child was removed from the children collection. The AVisual layer will then call
        /// GetChildren to find out which child has been removed.
        /// </summary>
        /// <param name="pChild">The child to remove.</param>
        protected void RemoveVisualChild(AVisual pChild)
        {
            if 
                ( pChild == null || 
                  pChild.mParent == null )
            {
                return;
            }

            if 
                ( pChild.mParent != this )
            {
                throw new ArgumentException( "The visual is not a child of that one!!!" );
            }
            
            if 
                ( this.VisualChildrenCount == 0 )
            {
                this.SetFlags( false, VisualFlags.HasChildren );
            }
            
            // Set the parent pointer to null.
            pChild.mParent = null;

            AVisual.PropagateFlags( this,
                                    VisualFlags.IsSubtreeDirtyForPrecompute | VisualFlags.IsSubtreeDirtyForRender );

            // Informs the layout.
            UIElement.PropagateSuspendLayout( pChild );

            // Fire notifications
            pChild.FireOnVisualParentChanged( this );
            this.OnVisualChildrenChanged( null /* no pChild added */, pChild );
        }

        /// <summary>
        /// InvalidateZOrder
        /// Note: must do invalidation without removing / adding
        /// to avoid loosing focused element by input system
        /// </summary>
        internal void InvalidateZOrder()
        {
            //  if we don't have any children, there is nothing to do
            if 
                ( this.VisualChildrenCount == 0 )
            {
                return;
            }

            AVisual.PropagateFlags( this,
                                    VisualFlags.IsSubtreeDirtyForPrecompute | VisualFlags.IsSubtreeDirtyForRender );

            this.SetFlags( true, VisualFlags.IsChildrenZOrderDirty );
            
            //InputManager.Instance.SafeCurrentNotifyHitTestInvalidated();
        }

        /// <summary>
        /// This is called when the parent link of the Visual is changed.
        /// This method executes important base functionality before calling the
        /// overridable virtual.
        /// </summary>
        /// <param name="pOldParent">Old parent or null if the Visual did not have a parent before.</param>
        internal virtual void FireOnVisualParentChanged(DependencyObject pOldParent)
        {
            // Call the ParentChanged virtual before firing the Ancestor Changed Event
            this.OnVisualParentChanged( pOldParent );

            // Clean up bits when the tree is Cut or Pasted.

            // If we are attaching to a tree then
            // send the bit up if we need to.
            if 
                ( pOldParent == null )
            {
                Debug.Assert( this.mParent != null, "If pOldParent is null, current parent should != null." );

                if 
                    ( this.CheckFlagsAnd( VisualFlags.SubTreeHoldsAncestorChanged ) )
                {
                    SetTreeBits( this.mParent,
                                 VisualFlags.SubTreeHoldsAncestorChanged,
                                 VisualFlags.RegisteredForAncestorChanged );
                }
            }
            // If we are cutting a sub tree off then
            // clear the bit in the main tree above if we need to.
            else
            {
                if 
                    ( this.CheckFlagsAnd( VisualFlags.SubTreeHoldsAncestorChanged ) )
                {
                    ClearTreeBits( pOldParent,
                                   VisualFlags.SubTreeHoldsAncestorChanged,
                                   VisualFlags.RegisteredForAncestorChanged );
                }
            }

            // Fire the Ancestor changed Event on the nodes.
            AncestorChangedEventArgs lArgs = new AncestorChangedEventArgs( this, pOldParent );
            ProcessAncestorChangedNotificationRecursive( this, lArgs );
        }

        /// <summary>
        /// Walks down in the tree for nodes that have AncestorChanged Handlers
        /// registered and calls them.
        /// It uses Flag bits that help it prune the walk.  This should go
        /// straight to the relevent nodes.
        /// </summary>
        /// <param name="pSender">The event sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal static void ProcessAncestorChangedNotificationRecursive(DependencyObject pSender, AncestorChangedEventArgs pEventArgs)
        {
            AVisual lVisual = pSender as AVisual;
            // If the flag is not set, then we are Done.
            if 
                ( lVisual.CheckFlagsAnd( VisualFlags.SubTreeHoldsAncestorChanged ) == false )
            {
                return;
            }

            // If there is a handler on this node, then fire it.
            lVisual.mAncestorChangedEventHandler?.Invoke( lVisual, pEventArgs );

            // Decend into the children.
            int lCount = lVisual.VisualChildrenCount;
            for 
                ( int lCurr = 0; lCurr < lCount; lCurr++ )
            {
                DependencyObject lChild = lVisual.GetVisualChild( lCurr );
                if 
                    ( lChild != null )
                {
                    ProcessAncestorChangedNotificationRecursive( lChild, pEventArgs );
                }
            }
        }

        /// <summary>
        /// Walks up the Visual tree setting or clearing the given flags.
        /// Unlike PropagateFlags, this does not terminate when it reaches node with
        /// the flags already set.
        /// It always walks all the way to the root.
        /// </summary>
        /// <param name="pValue">The flag indicating whether the supplied bit must be set or unset.</param>
        /// <param name="pFlag">The bit flag to set or unset.</param>
        internal void SetFlagsToRoot(bool pValue, VisualFlags pFlag)
        {
            AVisual lCurrent = this;
            do
            {
                lCurrent.SetFlags( pValue, pFlag );
                
                lCurrent = lCurrent.mParent as AVisual;
            }
            while 
                ( lCurrent != null );
        }

        /// <summary>
        /// Finds the first ancestor of the given element which has the given
        /// flags set.
        /// </summary>
        /// <param name="pFlag">The bit flag to look for.</param>
        internal DependencyObject FindFirstAncestorWithFlagsAnd(VisualFlags pFlag)
        {
            AVisual lCurrent = this;
            do
            {
                if 
                    ( lCurrent.CheckFlagsAnd( pFlag ) )
                {
                    // The other AVisual crossed through this AVisual's lParent chain. Hence this is our
                    // common ancestor.
                    return lCurrent;
                }
                
                lCurrent = lCurrent.mParent as AVisual;
            }
            while 
                ( lCurrent != null );

            return null;
        }

        /// <summary>
        /// The returned matrix can be used to transform coordinates from this Visual to
        /// the specified Visual.
        /// Returns null if no such transform exists due to a non-invertible Transform.
        /// </summary>
        /// <param name="pVisual">The visual regarding to which the transform must transform to.</param>
        /// <exception cref="ArgumentNullException">If visual is null.</exception>
        /// <exception cref="InvalidOperationException">If the Visuals are not connected.</exception>
        public AGeneralTransform TransformToVisual(AVisual pVisual)
        {
            DependencyObject lAncestor = this.FindCommonVisualAncestor( pVisual );
            AVisual lCast = lAncestor as AVisual;
            if 
                ( lCast == null )
            {
                throw new InvalidOperationException( "No common ancestor!!!" );
            }

            AGeneralTransform lTransform0;
            Matrix lMatrix0;
            bool lIsSimple0 = this.TrySimpleTransformToAncestor( lCast,
                                                                 false,
                                                                 out lTransform0,
                                                                 out lMatrix0 );

            AGeneralTransform lTransform1;
            Matrix lMatrix1;
            bool lIsSimple1 = pVisual.TrySimpleTransformToAncestor( lCast,
                                                                    true,
                                                                    out lTransform1,
                                                                    out lMatrix1 );

            // combine the transforms
            // if both transforms are simple Matrix transforms, just multiply them and
            // return the result.
            if 
                ( lIsSimple0 && 
                  lIsSimple1 )
            {
                MatrixUtil.MultiplyMatrix( ref lMatrix0, ref lMatrix1 );
                MatrixTransform lCombined = new MatrixTransform( lMatrix0 );
                return lCombined;
            }

            // Handle the case where 0 is simple and 1 is complex.
            if 
                ( lIsSimple0 )
            {
                lTransform0 = new MatrixTransform( lMatrix0 );
            }
            else if 
                ( lIsSimple1 )
            {
                lTransform1 = new MatrixTransform( lMatrix1 );
            }

            // If inverse was requested, TrySimpleTransformToAncestor can return null
            // add the transform only if it is not null
            if 
                ( lTransform1 != null )
            {
                GeneralTransformGroup lGroup = new GeneralTransformGroup();
                lGroup.Children.Add( lTransform0 );
                lGroup.Children.Add( lTransform1 );
                return lGroup;
            }

            return lTransform0;
        }

        /// <summary>
        /// Returns the transform or the inverse transform between this visual and the specified ancestor.
        /// If inverse is requested but does not exist (if the transform is not invertible), null is returned.
        /// </summary>
        /// <param name="pAncestor">Ancestor visual.</param>
        /// <param name="pInverse">Returns inverse if this argument is true.</param>
        private AGeneralTransform InternalTransformToAncestor(AVisual pAncestor, bool pInverse)
        {
            AGeneralTransform lGeneralTransform;
            Matrix lSimpleTransform;

            bool lIsSimple = TrySimpleTransformToAncestor( pAncestor,
                                                           pInverse,
                                                           out lGeneralTransform,
                                                           out lSimpleTransform );

            if 
                ( lIsSimple )
            {
                MatrixTransform lResult = new MatrixTransform( lSimpleTransform );
                return lResult;
            }
            else
            {
                return lGeneralTransform;
            }
        }

        /// <summary>
        /// Provides the transform or the inverse transform between this visual and the specified ancestor.
        /// Returns true if the transform is "simple" - in which case the GeneralTransform is null
        /// and the caller should use the Matrix.
        /// Otherwise, returns false - use the GeneralTransform and ignore the Matrix.
        /// If inverse is requested but not available (if the transform is not invertible), false is
        /// returned and the GeneralTransform is null.
        /// </summary>
        /// <param name="pAncestor">Ancestor visual.</param>
        /// <param name="pInverse">Returns inverse if this argument is true.</param>
        /// <param name="pGeneralTransform">The GeneralTransform if this method returns false.</param>
        /// <param name="pSimpleTransform">The Matrix if this method returns true.</param>
        internal bool TrySimpleTransformToAncestor(AVisual pAncestor,
                                                   bool pInverse,
                                                   out AGeneralTransform pGeneralTransform,
                                                   out Matrix pSimpleTransform)
        {
            Debug.Assert( pAncestor != null );
            
            AVisual lCurrent = this;
            Matrix lMatrix = Matrix.Identity;

            // Keep this null until it's needed
            GeneralTransformGroup lGroup = null;

            // This while loop will walk up the visual tree until we encounter the pAncestor.
            // As it does so, it will accumulate the descendent->pAncestor lTransform.
            // In most cases, this is simply a matrix, though if we encounter a bitmap effect we
            // will need to use a general lTransform lGroup to store the lTransform.
            // We will accumulate the current lTransform in a matrix until we encounter a bitmap effect,
            // at which point we will add the matrix's current value and the bitmap effect's transforms
            // to the GeneralTransformGroup and continue to accumulate further transforms in the matrix again.
            // At the end of this loop, we will have 0 or more transforms in the GeneralTransformGroup
            // and the matrix which, if not identity, should be appended to the GeneralTransformGroup.
            // If, as is commonly the case, this loop terminates without encountering a bitmap effect
            // we will simply use the Matrix.
            while 
                ( lCurrent.mParent != null && 
                  lCurrent != pAncestor )
            {
                ATransform lTransform = lCurrent.mTransform;
                if 
                    ( lTransform != null )
                {
                    Matrix cm = lTransform.Value;
                    MatrixUtil.MultiplyMatrix( ref lMatrix, ref cm );
                }
                lMatrix.Translate( lCurrent.mOffset.X, lCurrent.mOffset.Y ); // Consider having a bit that indicates that we have a non-null offset.
                lCurrent = lCurrent.mParent as AVisual;
            }

            if 
                ( lCurrent != pAncestor )
            {
                throw new InvalidOperationException( pInverse ? "Not a descendant!!!" : "Not an ancestor!!!" );
            }

            // At this point, we will have 0 or more transforms in the GeneralTransformGroup
            // and the matrix which, if not identity, should be appended to the GeneralTransformGroup.
            // If, as is commonly the case, this loop terminates without encountering a bitmap effect
            // we will simply use the Matrix.

            // Assert that a non-null lGroup implies at least one child
            Debug.Assert( lGroup == null || lGroup.Children.Count > 0 );

            // Do we have a lGroup?
            if 
                ( lGroup != null )
            {
                if 
                    ( lMatrix.IsIdentity == false )
                {
                    lGroup.Children.Add( new MatrixTransform( lMatrix ) );
                }

                if 
                    ( pInverse )
                {
                    lGroup = (GeneralTransformGroup)lGroup.Inverse;
                }
                
                // Initialize out params
                pGeneralTransform = lGroup;
                pSimpleTransform  = new Matrix();
                return false; // simple lTransform failed
            }
            // If not, the entire lTransform is stored in the matrix
            else
            {
                // Initialize out params
                pGeneralTransform = null;

                if 
                    ( pInverse )
                {
                    if 
                        ( lMatrix.HasInverse == false )
                    {
                        pSimpleTransform = new Matrix();
                        return false; // inversion failed, so simple lTransform failed.
                    }

                    lMatrix.Invert();
                }

                pSimpleTransform = lMatrix;
                return true; // simple lTransform succeeded
            }
        }

        /// <summary>
        /// Notifies a content changes.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        internal virtual void ContentsChanged(object pSender, EventArgs pEventArgs)
        {
            this.PropagateChangedFlags();
        }

        /// <summary>
        /// Update the visual content.
        /// </summary>
        /// <param name="pFlags">The visual flags.</param>
        private void UpdateContent(VisualFlags pFlags)
        {
            if 
                ( (pFlags & VisualFlags.IsContentDirty) != 0 )
            {
                this.RenderContent();
                this.SetFlags( false, VisualFlags.IsContentDirty );
            }
        }

        /// <summary>
        /// Updates the children
        /// </summary>
        private void UpdateChildren()
        {
            // Visit children of this visual.
            bool lIsZOrderDirty = this.CheckFlagsAnd( VisualFlags.IsChildrenZOrderDirty );
            int lChildCOunt = this.VisualChildrenCount;

            //
            // If the visual children have been re-ordered, enqueue a packet to RemoveAllChildren,
            // then reinsert all the children.  The parent visual will release the children when
            // the RemoveAllChildren packet, but the managed visuals will still have references
            // to them so that they won't be destructed and recreated.
            //
            if 
                ( lIsZOrderDirty )
            {
                this.ReorderChildren();
            }

            for 
                ( int lCurr = 0; lCurr < lChildCOunt; lCurr++ )
            {
                AVisual lChild = GetVisualChild( lCurr );
                if 
                    ( lChild != null )
                {
                    //
                    // Recurse if the lChild visual is dirty
                    // or it has not been marshalled yet.
                    //
                    if 
                        ( lChild.CheckFlagsAnd( VisualFlags.IsSubtreeDirtyForRender ) )
                    {
                        lChild.RenderRecursive();
                    }
                }
            }

            this.SetFlags( false, VisualFlags.IsChildrenZOrderDirty );
        }

        /// <summary>
        /// Reorder children as the way it need to be.
        /// </summary>
        protected virtual void ReorderChildren()
        {
            // Nothing to do.
        }

        #endregion Methods Internal

        #endregion Methods
    }
}
