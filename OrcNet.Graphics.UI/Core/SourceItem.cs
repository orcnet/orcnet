﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// A source item in the event route to follow.
    /// </summary>
    internal struct SourceItem
    {
        #region Fields

        /// <summary>
        /// Stores the index to start at.
        /// </summary>
        private int    mStartIndex;

        /// <summary>
        /// Stores the source object.
        /// </summary>
        private object mSource;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the index to start at.
        /// </summary>
        internal int StartIndex
        {
            get
            {
                return this.mStartIndex;
            }
        }

        /// <summary>
        /// Gets the source object.
        /// </summary>
        internal object Source
        {
            get
            {
                return this.mSource;
            }
        }

        #endregion Properties

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceItem"/> class.
        /// </summary>
        /// <param name="pStartIndex">The index to start at.</param>
        /// <param name="pSource">The source object.</param>
        internal SourceItem(int pStartIndex, object pSource)
        {
            this.mStartIndex = pStartIndex;
            this.mSource = pSource;
        }

        #endregion Construction

        #region Operations
        
        /// <summary>
        /// Checks whether two source item are equal or not.
        /// </summary>
        /// <param name="pObject">The other source item to compare with.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pObject)
        {
            return this.Equals( (SourceItem)pObject );
        }

        /// <summary>
        /// Checks whether two source item are equal or not.
        /// </summary>
        /// <param name="pSourceItem">The other source item to compare with.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public bool Equals(SourceItem pSourceItem)
        {
            return pSourceItem.mStartIndex == this.mStartIndex &&
                   pSourceItem.mSource == this.mSource;
        }

        /// <summary>
        /// Gets the hashcode.
        /// </summary>
        /// <returns>The hashcode.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Checks whether two source item are equal or not.
        /// </summary>
        /// <param name="pSourceItem1"></param>
        /// <param name="pSourceItem2"></param>
        /// <returns>True if equal, false otherwise.</returns>
        public static bool operator == (SourceItem pSourceItem1, SourceItem pSourceItem2)
        {
            return pSourceItem1.Equals( pSourceItem2 );
        }

        /// <summary>
        /// Checks whether two source item are different or not.
        /// </summary>
        /// <param name="pSourceItem1"></param>
        /// <param name="pSourceItem2"></param>
        /// <returns>True if different, false otherwise.</returns>
        public static bool operator != (SourceItem pSourceItem1, SourceItem pSourceItem2)
        {
            return pSourceItem1.Equals( pSourceItem2 ) == false;
        }

        #endregion Operations
    }
}
