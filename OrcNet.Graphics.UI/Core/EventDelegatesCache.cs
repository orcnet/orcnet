﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Container for the event delegates of an Element class definition.
    /// </summary>
    internal class EventDelegatesCache
    {
        #region Fields

        /// <summary>
        /// Stores the set of delegates map.
        /// </summary>
        private Dictionary<int, object> mEntries;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the delegates for the given key.
        /// </summary>
        /// <param name="pKey">The key.</param>
        /// <returns>The delegates.</returns>
        internal List<RoutedEventHandlerInfo> this[RoutedEvent pKey]
        {
            get
            {
                Debug.Assert( pKey != null, 
                              "The key cannot be null");

                object lDelegates = this.mEntries[ pKey.Identifier ];
                return lDelegates as List<RoutedEventHandlerInfo>;
            }
        }

        /// <summary>
        /// Gets the delegates for the given key.
        /// </summary>
        /// <param name="pKey">The key.</param>
        /// <returns>The delegates.</returns>
        internal Delegate this[int pKey]
        {
            get
            {
                Debug.Assert( pKey != -1, 
                              "The key cannot be negative!!!");

                object lExistingDelegate = this.mEntries[ pKey ];
                return lExistingDelegate as Delegate;
            }
        }

        /// <summary>
        /// Gets the event cache count.
        /// </summary>
        internal int Count
        {
            get
            {
                return this.mEntries.Count;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EventDelegatesCache"/> class.
        /// </summary>
        internal EventDelegatesCache()
        {
            this.mEntries = new Dictionary<int, object>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds an event delegate for the given Key into cache.
        /// </summary>
        /// <param name="pKey">The delegate's key in cache.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public void Add(int pKey, Delegate pDelegate)
        {
            if 
                ( pKey == -1 )
            {
                throw new ArgumentNullException("pKey");
            }

            if 
                ( pDelegate == null )
            {
                throw new ArgumentNullException("pDelegate");
            }

            // Get the entry corresponding to the given key
            Delegate lExistingDelegate = (Delegate)this[ pKey ];
            if 
                ( lExistingDelegate == null )
            {
                this.mEntries[ pKey ] = pDelegate;
            }
            else
            {
                this.mEntries[ pKey ] = Delegate.Combine( lExistingDelegate, pDelegate );
            }
        }

        /// <summary>
        /// Removes an instance of the specified delegate for the given Key from cache.
        /// </summary>
        /// <param name="pKey">The delegate's key in cache.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public void Remove(int pKey, Delegate pDelegate)
        {
            if
                ( pKey == -1 )
            {
                throw new ArgumentNullException("pKey");
            }

            if 
                ( pDelegate == null )
            {
                throw new ArgumentNullException("pDelegate");
            }

            // Get the entry corresponding to the given key
            Delegate lExistingDelegate = (Delegate)this[ pKey ];
            if
                ( lExistingDelegate != null )
            {
                lExistingDelegate = Delegate.Remove( lExistingDelegate, pDelegate );
                if
                    ( lExistingDelegate == null )
                {
                    this.mEntries[ pKey ] = null;
                }
                else
                {
                    this.mEntries[ pKey ] = lExistingDelegate;
                }
            }
        }

        /// <summary>
        /// Gets all the delegates for the given key.
        /// </summary>
        /// <param name="pKey"></param>
        /// <returns></returns>
        public Delegate Get(int pKey)
        {
            if 
                ( pKey == -1 )
            {
                throw new ArgumentNullException("pKey");
            }

            // Return the delegates corresponding to the given key
            return (Delegate)this[ pKey ];
        }

        /// <summary>
        /// Get all the event delegates in this cache for the given routed event
        /// </summary>
        /// <param name="pRoutedEvent">The routed event the delegates must be retrieve for.</param>
        /// <returns>The set of delegates, null otherwise.</returns>
        public RoutedEventHandlerInfo[] GetRoutedEventDelegates(RoutedEvent pRoutedEvent)
        {
            if 
                ( pRoutedEvent == null )
            {
                throw new ArgumentNullException("pRoutedEvent");
            }

            List<RoutedEventHandlerInfo> lDelegates = this[ pRoutedEvent ];
            if 
                ( lDelegates != null )
            {
                return lDelegates.ToArray();
            }

            return null;
        }

        /// <summary>
        /// Checks whether the given routed event exists in cache or not.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event to look for.</param>
        /// <returns>True if contained, false otherwise.</returns>

        public bool Contains(RoutedEvent pRoutedEvent)
        {
            if 
                ( pRoutedEvent == null )
            {
                throw new ArgumentNullException("pRoutedEvent");
            }

            List<RoutedEventHandlerInfo> lDelegates = this[ pRoutedEvent ] as List<RoutedEventHandlerInfo>;
            return lDelegates != null && lDelegates.Count != 0;
        }

        /// <summary>
        /// Adds a routed event delegate for the given routed event into cache.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event to add the delegate for.</param>
        /// <param name="pDelegate">The new delegate to add.</param>
        /// <param name="pProcessHandled">THe flag indicating whether to process handled event or not.</param>
        public void AddRoutedEventHandler(RoutedEvent pRoutedEvent, Delegate pDelegate, bool pProcessHandled)
        {
            if 
                ( pRoutedEvent == null )
            {
                throw new ArgumentNullException("pRoutedEvent");
            }

            if 
                ( pDelegate == null )
            {
                throw new ArgumentNullException("pDelegate");
            }

            if 
                ( pRoutedEvent.IsLegalHandler( pDelegate ) == false )
            {
                throw new ArgumentException( "Illegal handler!!!" );
            }

            // Create a new RoutedEventHandler
            RoutedEventHandlerInfo lRoutedEventHandlerInfo = new RoutedEventHandlerInfo( pDelegate, pProcessHandled );

            // Get the entry corresponding to the given RoutedEvent
            List<RoutedEventHandlerInfo> lDelegates = this[ pRoutedEvent ] as List<RoutedEventHandlerInfo>;
            if 
                ( lDelegates == null )
            {
                this.mEntries[ pRoutedEvent.Identifier ] = lDelegates = new List<RoutedEventHandlerInfo>( 1 );
            }

            // Add the RoutedEventHandlerInfo to the list
            lDelegates.Add( lRoutedEventHandlerInfo );
        }

        /// <summary>
        /// Removes an instance of the specified routed event delegate for the given routed event from cache.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event from which removing the delegate.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public void RemoveRoutedEventHandler(RoutedEvent pRoutedEvent, Delegate pDelegate)
        {
            if 
                ( pRoutedEvent == null )
            {
                throw new ArgumentNullException("pRoutedEvent");
            }

            if 
                ( pDelegate == null )
            {
                throw new ArgumentNullException("pDelegate");
            }

            if 
                ( pRoutedEvent.IsLegalHandler( pDelegate ) == false )
            {
                throw new ArgumentException( "Illegal delegate!!!" );
            }

            // Get the entry corresponding to the given routed event
            List<RoutedEventHandlerInfo> lDelegates = this[ pRoutedEvent ] as List<RoutedEventHandlerInfo>;
            if
                ( lDelegates != null && 
                  lDelegates.Count > 0 )
            {
                if 
                    ( lDelegates.Count == 1 && 
                      lDelegates[0].Handler == pDelegate )
                {
                    this.mEntries[ pRoutedEvent.Identifier ] = null;
                }
                else
                {
                    // When a matching instance is found remove it
                    for 
                        ( int lCurr = 0; lCurr < lDelegates.Count; lCurr++ )
                    {
                        if 
                            ( lDelegates[ lCurr ].Handler == pDelegate )
                        {
                            lDelegates.RemoveAt( lCurr );
                            break;
                        }
                    }
                }
            }
        }

        #endregion Methods
    }
}
