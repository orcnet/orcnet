﻿using OpenTK.Input;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Keyboard event arguments class definition.
    /// </summary>
    public class KeyboardEventArgs : InputEventArgs
    {
        #region Properties

        /// <summary>
        /// Gets the keyboard state associated with the event.
        /// </summary>
        public KeyboardState KeyboardDevice
        {
            get
            {
                return (KeyboardState)this.Device;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyboardEventArgs"/> class.
        /// </summary>
        /// <param name="pKeyboard">The keyboard state associated with the event.</param>
        /// <param name="pTimeStamp">The time when the input occured.</param>
        public KeyboardEventArgs(KeyboardState pKeyboard, int pTimeStamp) :
        base( pKeyboard, pTimeStamp )
        {
            
        }

        #endregion Constructor
    }
}
