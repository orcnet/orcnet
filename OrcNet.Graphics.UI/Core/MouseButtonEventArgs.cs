﻿using OpenTK.Input;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Mouse button event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void MouseButtonEventDelegate(object pSender, MouseButtonEventArgs pEventArgs);

    /// <summary>
    /// Mouse button event arguments class definition.
    /// </summary>
    public class MouseButtonEventArgs : MouseEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the mouse button whose state is being described.
        /// </summary>
        private MouseButton mButton;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the mouse button whose state is being described.
        /// </summary>
        public MouseButton Button
        {
            get
            {
                return this.mButton;
            }
        }

        /// <summary>
        /// Gets the mouse button state 
        /// </summary>
        public MouseButtonState ButtonState
        {
            get
            {
                return this.MouseDevice[ this.mButton ] ? MouseButtonState.Pressed : MouseButtonState.Released;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseButtonEventArgs"/> class.
        /// </summary>
        /// <param name="pDevice">The mouse device associated with the event.</param>
        /// <param name="pTimeStamp">The time when the input occured.</param>
        /// <param name="pButton">The mouse button whose state is being described.</param>
        public MouseButtonEventArgs(MouseState pDevice, int pTimeStamp, MouseButton pButton) :
        base( pDevice, pTimeStamp )
        {
            this.mButton = pButton;
        }

        #endregion Constructor
    }
}
