﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Routed event class definition.
    /// </summary>
    public sealed class RoutedEvent
    {
        #region Fields

        /// <summary>
        /// Stores the routed event manager locker.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the static index counter for routed events.
        /// </summary>
        private static int sIdentifierCounter = 1;

        /// <summary>
        /// Stores the base type this routed event is at least for.
        /// </summary>
        private static Type sBaseType = typeof(DependencyObject);

        /// <summary>
        /// Stores the set of routed events by owner types.
        /// </summary>
        private static Dictionary<Type, List<RoutedEvent>> sRoutedEventsByOwnerType = new Dictionary<Type, List<RoutedEvent>>( 10 );

        /// <summary>
        /// Stores the set of routed event delegates by class types.
        /// </summary>
        private static Dictionary<Type, RoutedEventDelegatesCache> sRoutedEventDelegatesByType = new Dictionary<Type, RoutedEventDelegatesCache>( 100 );

        /// <summary>
        /// Stores the overall routed event count.
        /// </summary>
        private static int sOverallEventCount = 0;

        /// <summary>
        /// Stores the flag indicating whether the last handlers added will be invoked at the end or at the beginning.
        /// </summary>
        private bool mIsDirect;

        /// <summary>
        /// Stores the unique identifier of that routed event.
        /// </summary>
        private int mIdentifier;

        /// <summary>
        /// Stores the routed event name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the type of handler for that routed event.
        /// </summary>
        private Type mHandlerType;

        /// <summary>
        /// Stores the type of owner for that routed event.
        /// </summary>
        private Type mOwnerType;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the routed event name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the last handlers added will be invoked at the end or at the beginning.
        /// </summary>
        internal bool IsDirect
        {
            get
            {
                return this.mIsDirect;
            }
            set
            {
                this.mIsDirect = value;
            }
        }

        /// <summary>
        /// Gets the type of handler for that routed event.
        /// </summary>
        public Type HandlerType
        {
            get
            {
                return this.mHandlerType;
            }
        }

        /// <summary>
        /// Gets the type of owner for that routed event.
        /// </summary>
        public Type OwnerType
        {
            get
            {
                return this.mOwnerType;
            }
        }

        /// <summary>
        /// Gets the unique identifier of that routed event.
        /// </summary>
        internal int Identifier
        {
            get
            {
                return this.mIdentifier;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RoutedEvent"/> class.
        /// </summary>
        /// <param name="pName">The event name.</param>
        /// <param name="pHandlerType">The event handler type.</param>
        /// <param name="pOwnerType">The event owner type.</param>
        private RoutedEvent(string pName, Type pHandlerType, Type pOwnerType)
        {
            this.mName = pName;
            this.mHandlerType = pHandlerType;
            this.mOwnerType = pOwnerType;

            this.mIdentifier = GetNextIdentifier();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the given delegate is supported by this routed event.
        /// </summary>
        /// <param name="pDelegate">The delegate to test</param>
        /// <returns>True if supported, false otherwise.</returns>
        internal bool IsLegalHandler(Delegate pDelegate)
        {
            Type lDelegateType = pDelegate.GetType();

            return lDelegateType == this.mHandlerType ||
                   lDelegateType == typeof(RoutedEventDelegate);
        }

        /// <summary>
        /// Gets an unique identifier among routed event and delegates.
        /// </summary>
        /// <returns>The unique identifier.</returns>
        internal static int GetNextIdentifier()
        {
            return sIdentifierCounter++;
        }

        /// <summary>
        /// Registers a RoutedEvent with the given details
        /// </summary>
        /// <remarks>The Name must be unique within the given OwnerType</remarks>
        /// <param name="pName">The event name.</param>
        /// <param name="pHandlerType">The event handler type.</param>
        /// <param name="pOwnerType">The event owner type.</param>
        /// <returns>The new routed event.</returns>
        internal static RoutedEvent RegisterRoutedEvent(string pName, Type pHandlerType, Type pOwnerType)
        {
            RoutedEvent lAlreadyIn = GetRoutedEventFromName( pName, pOwnerType, false );
            if
                ( lAlreadyIn != null )
            {
                return lAlreadyIn;
            }

            lock 
                ( sSyncRoot )
            {
                // Create a new RoutedEvent
                RoutedEvent lRoutedEvent = new RoutedEvent( pName, pHandlerType, pOwnerType );
                
                sOverallEventCount++;

                AddOwner( lRoutedEvent, pOwnerType );

                return lRoutedEvent;
            }
        }

        /// <summary>
        /// Registers a new delegate for the given type and routed event.
        /// </summary>
        /// <param name="pType">The type the delegate must added for.</param>
        /// <param name="pRoutedEvent">The routed event of the given type the delegate must be added for.</param>
        /// <param name="pDelegate">The delegate to register.</param>
        /// <param name="pProcessHandled">The flag indicating whether the handled event must be processed as well or not.</param>
        internal static void RegisterClassHandler(Type pType, RoutedEvent pRoutedEvent, Delegate pDelegate, bool pProcessHandled)
        {
            Debug.Assert( typeof(UIElement).IsAssignableFrom(pType) ||
                          typeof(ContentElement).IsAssignableFrom(pType),
                          "Class handler can be registered only for UIElement, ContentElement and their sub types");
            Debug.Assert( pRoutedEvent.IsLegalHandler( pDelegate ),
                          "Handler Type mismatch" );

            RoutedEventDelegatesCache lClassListeners;
            int lIndex;
            
            // Get the updated EventHandlersStore for the given DType
            GetTypedClassListeners( pType, pRoutedEvent, out lClassListeners, out lIndex );

            // Reuired to update storage
            lock 
                ( sSyncRoot )
            {
                // Add new routed event pDelegate and get the updated set of handlers
                RoutedEventDelegateInfoContainer updatedClassListeners = lClassListeners.AddToExistingDelegates( lIndex, pDelegate, pProcessHandled );

                // Update Sub Classes
                Type[] lKeys = sRoutedEventDelegatesByType.Keys.ToArray();
                for 
                    ( int lCurr = 0; lCurr < lKeys.Length; lCurr++ )
                {
                    if 
                        ( lKeys[ lCurr ].IsSubclassOf( pType ) == true)
                    {
                        lClassListeners = sRoutedEventDelegatesByType[ lKeys[ lCurr ] ];
                        lClassListeners.UpdateSubClassHandlers( pRoutedEvent, updatedClassListeners );
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new routed event to the global routed event cache manager.
        /// </summary>
        /// <param name="pRoutedEvent">The new routed event.</param>
        /// <param name="pOwnerType">The owner type.</param>
        internal static void AddOwner(RoutedEvent pRoutedEvent, Type pOwnerType)
        {
            List<RoutedEvent> lEvents = null;
            if
                ( sRoutedEventsByOwnerType.TryGetValue( pOwnerType, out lEvents ) == false )
            {
                lEvents = new List<RoutedEvent>( 1 );
                sRoutedEventsByOwnerType[ pOwnerType ] = lEvents;
            }

            // Add the newly created routed event.
            if 
                ( lEvents.Contains( pRoutedEvent ) == false )
            {
                lEvents.Add( pRoutedEvent );
            }
        }

        /// <summary>
        /// Retrieves the requested routed event having the given name over the supplied type
        /// and its parent type if allowed.
        /// </summary>
        /// <param name="pName">The event name to look for.</param>
        /// <param name="pOwnerType">The event owner type.</param>
        /// <param name="pCheckParentTypes">The flag indicating whether to use parent owner types to retrieve the event or not.</param>
        /// <returns>The routed event, null otherwise.</returns>
        internal static RoutedEvent GetRoutedEventFromName(string pName, Type pOwnerType, bool pCheckParentTypes)
        {
            // Search in the event map.
            while 
                ( pOwnerType != null )
            {
                List<RoutedEvent> lEvents = null;
                if 
                    ( sRoutedEventsByOwnerType.TryGetValue( pOwnerType, out lEvents ) )
                {
                    // Check for the routed event having the requested name.
                    for
                        ( int lCurr = 0; lCurr < lEvents.Count; lCurr++ )
                    {
                        RoutedEvent lRoutedEvent = lEvents[ lCurr ];
                        if 
                            ( lRoutedEvent.mName.Equals( pName ) )
                        {
                            // Return if found
                            return lRoutedEvent;
                        }
                    }
                }
                
                if
                    ( pCheckParentTypes )
                {
                    pOwnerType = pOwnerType.BaseType;
                }
                else
                {
                    pOwnerType = null;
                }
            }

            // No match found
            return null;
        }

        /// <summary>
        /// Gets the set of routed events that match the given owner Type.
        /// </summary>
        /// <param name="pOwnerType">The owner type routed event list must be found for.</param>
        /// <returns>The set or routed events, null otherwise.</returns>
        internal static RoutedEvent[] GetRoutedEventsForOwner(Type pOwnerType)
        {
            List<RoutedEvent> lEvents = null;
            if
                ( sRoutedEventsByOwnerType.TryGetValue( pOwnerType, out lEvents ) )
            {
                lEvents.ToArray();
            }
            
            return null;
        }

        /// <summary>
        /// Gets the list of class listeners for the given type and routed event
        /// </summary>
        /// <param name="pType">The class type listeners must be provided for.</param>
        /// <param name="pRoutedEvent">The routed event listeners are for.</param>
        /// <returns>The list of delegates, null otherwise.</returns>
        internal static RoutedEventDelegateInfoContainer GetTypedClassListeners(Type pType, RoutedEvent pRoutedEvent)
        {
            RoutedEventDelegatesCache lClassListeners;
            int lIndex;
            
            return GetTypedClassListeners( pType, pRoutedEvent, out lClassListeners, out lIndex );
        }

        /// <summary>
        /// Gets the list of class listeners for the given type and routed event
        /// </summary>
        /// <param name="pType">The class type listeners must be provided for.</param>
        /// <param name="pRoutedEvent">The routed event listeners are for.</param>
        /// <param name="pClassListeners">The list of delegates</param>
        /// <param name="pIndex">The index of the routed event delegates list in the delegates set for the given type.</param>
        /// <returns>The list of delegates, null otherwise.</returns>
        internal static RoutedEventDelegateInfoContainer GetTypedClassListeners(Type pType, RoutedEvent pRoutedEvent, out RoutedEventDelegatesCache pClassListeners, out int pIndex)
        {
            // Get the RoutedEventDelegatesCache for the given DType
            pClassListeners = sRoutedEventDelegatesByType[ pType ];
            RoutedEventDelegateInfoContainer lDelegatesInfo;
            if 
                ( pClassListeners != null )
            {
                // Get the delegates Info for the given type and routed event
                pIndex = pClassListeners.GetHandlersIndex( pRoutedEvent );
                if 
                    ( pIndex != -1 )
                {
                    lDelegatesInfo = pClassListeners.GetExistingHandlers( pIndex );
                    return lDelegatesInfo;
                }
            }

            lock 
                ( sSyncRoot )
            {
                // Search in the map for the list of matching RoutedEventDelegatesInfo
                lDelegatesInfo = GetUpdatedDTypedClassListeners( pType, pRoutedEvent, out pClassListeners, out pIndex );
            }

            return lDelegatesInfo;
        }

        /// <summary>
        /// Gets the updated set of class's delegates for the given type and routed event
        /// </summary>
        /// <param name="pType">The type delegates are for.</param>
        /// <param name="pRoutedEvent">The routed event that must run delegates.</param>
        /// <param name="pClassDelegates">The base class's delegates.</param>
        /// <param name="pIndex">The cache index where delegates are stored.</param>
        /// <returns>The delegates container, null otherwise.</returns>
        private static RoutedEventDelegateInfoContainer GetUpdatedDTypedClassListeners(Type pType, RoutedEvent pRoutedEvent, out RoutedEventDelegatesCache pClassDelegates, out int pIndex)
        {
            // Get the RoutedEventDelegatesCache for the given type
            pClassDelegates = sRoutedEventDelegatesByType[ pType ];
            RoutedEventDelegateInfoContainer lDelegatesContainer;
            if 
                ( pClassDelegates != null )
            {
                // Get the delegates container for the given type and routed event
                pIndex = pClassDelegates.GetHandlersIndex( pRoutedEvent );
                if 
                    ( pIndex != -1 )
                {
                    lDelegatesContainer = pClassDelegates.GetExistingHandlers( pIndex );
                    return lDelegatesContainer;
                }
            }

            // Since the matching delegates container has not been found at this level 
            // keep going through base classes to check for registered class's delegates container.
            Type lTempType = pType;
            RoutedEventDelegatesCache lTempRoutedEventDelegatesCache = null;
            RoutedEventDelegateInfoContainer lTempDelegatesContainer = null;
            int lTempIndex = -1;
            while 
                ( lTempIndex == -1 &&
                  lTempType != sBaseType )
            {
                lTempType = lTempType.BaseType;
                lTempRoutedEventDelegatesCache = (RoutedEventDelegatesCache)sRoutedEventDelegatesByType[ lTempType ];
                if 
                    ( lTempRoutedEventDelegatesCache != null )
                {
                    // Get the delegates container for the new type and routed Event
                    lTempIndex = lTempRoutedEventDelegatesCache.GetHandlersIndex( pRoutedEvent );
                    if 
                        ( lTempIndex != -1 )
                    {
                        lTempDelegatesContainer = lTempRoutedEventDelegatesCache.GetExistingHandlers( lTempIndex );
                    }
                }
            }

            if
                ( pClassDelegates == null )
            {
                if 
                    ( pType == typeof(UIElement) || 
                      pType == typeof(ContentElement) )
                {
                    pClassDelegates = new RoutedEventDelegatesCache( 80 ); // Based on the number of class lDelegatesContainer for these classes
                }
                else
                {
                    pClassDelegates = new RoutedEventDelegatesCache( 1 );
                }

                sRoutedEventDelegatesByType[ pType ] = pClassDelegates;
            }

            pIndex = pClassDelegates.CreateHandlersLink( pRoutedEvent, lTempDelegatesContainer );

            return lTempDelegatesContainer;
        }

        #endregion Methods
    }
}
