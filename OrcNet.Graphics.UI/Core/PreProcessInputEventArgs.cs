﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Pre-process input event arguments prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void PreProcessInputEventDelegate(object pSender, PreProcessInputEventArgs pEventArgs);

    /// <summary>
    /// Pre-process input event arguments class definition.
    /// </summary>
    public sealed class PreProcessInputEventArgs : ProcessInputEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the event input has been canceled or not.
        /// </summary>
        private bool mIsCanceled;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the event input has been canceled or not.
        /// </summary>
        public bool IsCanceled
        {
            get
            {
                return this.mIsCanceled;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="PreProcessInputEventArgs"/> class.
        /// </summary>
        internal PreProcessInputEventArgs()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Cancels the input event processing.
        /// </summary>
        public void Cancel()
        {
            this.mIsCanceled = true;
        }
        /// <summary>
        /// Resets the input event notifier for object recycling purpose only.
        /// </summary>
        /// <param name="pInput">The input that replace the old one.</param>
        internal override void Reset(InputEventArgs pInput)
        {
            this.mIsCanceled = false;
            base.Reset( pInput );
        }

        #endregion Methods
    }
}
