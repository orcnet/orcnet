﻿
namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Base input element interface definition
    /// exposing the different 
    /// </summary>
    public interface IInputElement
    {
        #region Events
        
        /// <summary>
        /// Event triggered when the keyboard has the focus on that element.
        /// </summary>
        event KeyboardFocusChangedEventDelegate GotKeyboardFocus;

        /// <summary>
        /// Event triggered when the element has the mouse in its region.
        /// </summary>
        event MouseEventDelegate GotMouseCapture;
        
        /// <summary>
        /// Event triggered when the element has the keyboard focus and a key is pressed.
        /// </summary>
        event KeyEventDelegate KeyDown;

        /// <summary>
        /// Event triggered when the element has the keyboard focus and a key is released.
        /// </summary>
        event KeyEventDelegate KeyUp;

        /// <summary>
        /// Event triggered when the keyboard loses the focus on that element.
        /// </summary>
        event KeyboardFocusChangedEventDelegate LostKeyboardFocus;

        /// <summary>
        /// Event triggered when the element has the mouse in its region no more.
        /// </summary>
        event MouseEventDelegate LostMouseCapture;
        
        /// <summary>
        /// Event triggered when the mouse just entered the limit of that element.
        /// </summary>
        event MouseEventDelegate MouseEnter;

        /// <summary>
        /// Event triggered when the mouse just exited the limit of that element.
        /// </summary>
        event MouseEventDelegate MouseLeave;

        /// <summary>
        /// Event triggered when the mouse left button is pressed on that element.
        /// </summary>
        event MouseButtonEventDelegate MouseLeftButtonDown;

        /// <summary>
        /// Event triggered when the mouse left button is released on that element.
        /// </summary>
        event MouseButtonEventDelegate MouseLeftButtonUp;

        /// <summary>
        /// Event triggered when the mouse is moving on that element.
        /// </summary>
        event MouseEventDelegate MouseMove;

        /// <summary>
        /// Event triggered when the mouse right button is pressed on that element.
        /// </summary>
        event MouseButtonEventDelegate MouseRightButtonDown;

        /// <summary>
        /// Event triggered when the mouse right button is released on that element.
        /// </summary>
        event MouseButtonEventDelegate MouseRightButtonUp;

        /// <summary>
        /// Event triggered when the mouse wheel is rolling on that element.
        /// </summary>
        event MouseWheelEventDelegate MouseWheel;

        /// <summary>
        /// Event triggered just before the keyboard has the focus on that element.
        /// </summary>
        event KeyboardFocusChangedEventDelegate PreviewGotKeyboardFocus;

        /// <summary>
        /// Event triggered just before the element has the keyboard focus and a key is pressed.
        /// </summary>
        event KeyEventDelegate PreviewKeyDown;

        /// <summary>
        /// Event triggered just before the element has the keyboard focus and a key is released.
        /// </summary>
        event KeyEventDelegate PreviewKeyUp;

        /// <summary>
        /// Event triggered just before the keyboard loses the focus on that element.
        /// </summary>
        event KeyboardFocusChangedEventDelegate PreviewLostKeyboardFocus;

        /// <summary>
        /// Event triggered just before the mouse left button is pressed on that element.
        /// </summary>
        event MouseButtonEventDelegate PreviewMouseLeftButtonDown;

        /// <summary>
        /// Event triggered just before the mouse left button is released on that element.
        /// </summary>
        event MouseButtonEventDelegate PreviewMouseLeftButtonUp;

        /// <summary>
        /// Event triggered just before the mouse is moving on that element.
        /// </summary>
        event MouseEventDelegate PreviewMouseMove;

        /// <summary>
        /// Event triggered just before the mouse right button is pressed on that element.
        /// </summary>
        event MouseButtonEventDelegate PreviewMouseRightButtonDown;

        /// <summary>
        /// Event triggered just before the mouse right button is released on that element.
        /// </summary>
        event MouseButtonEventDelegate PreviewMouseRightButtonUp;

        /// <summary>
        /// Event triggered just before the mouse wheel rolls on that element.
        /// </summary>
        event MouseWheelEventDelegate PreviewMouseWheel;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the element can have the focus or not.
        /// </summary>
        bool Focusable
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets the flag indicating whether the element has been activated or not.
        /// </summary>
        bool IsEnabled
        {
            get;
        }
        
        /// <summary>
        /// Gets the flag indicating whether the element has the keyboard focus or not.
        /// </summary>
        bool IsKeyboardFocused
        {
            get;
        }
        
        /// <summary>
        /// Gets the flag indicating whether the keyboard focus is whithin the element or not.
        /// </summary>
        bool IsKeyboardFocusWithin
        {
            get;
        }
        
        /// <summary>
        /// Gets the flag indicating whether the mouse is entered in the element bounds or not.
        /// </summary>
        bool IsMouseCaptured
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse has that element as Top most regarding
        /// to those in the region the mouse is in.
        /// </summary>
        bool IsMouseDirectlyOver
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse is on that element whatever it is a child
        /// or the top most of those in the region the mouse is in.
        /// </summary>
        bool IsMouseOver
        {
            get;
        }
        
        #endregion Properties
    }
}
