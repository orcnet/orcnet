﻿using OpenTK.Input;
using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Keyboard input report class definition containing the info
    /// provided from a keyboard.
    /// </summary>
    internal class KeyboardInputReport : AInputReport
    {
        #region Fields

        /// <summary>
        /// Stores the keyboard key data.
        /// </summary>
        private Key mKey;

        /// <summary>
        /// Stores a potential extra object.
        /// </summary>
        private WeakReference mExtra;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the keyboard key data.
        /// </summary>
        public Key Key
        {
            get
            {
                return this.mKey;
            }
        }

        /// <summary>
        /// Gets a potential extra object.
        /// </summary>
        public object Extra
        {
            get
            {
                return this.mExtra.Target;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyboardInputReport"/> class.
        /// </summary>
        /// <param name="pInputSource">The source input comes from.</param>
        /// <param name="pMode">The input mode.</param>
        /// <param name="pTimeStamp">The time the input occured at.</param>
        /// <param name="pKey">The keyboard key.</param>
        /// <param name="pExtra">The potential extra object.</param>
        public KeyboardInputReport(PresentationSource pInputSource, InputMode pMode, int pTimeStamp, Key pKey, object pExtra) :
        base( pInputSource, InputType.Keyboard, pMode, pTimeStamp )
        {
            this.mKey = pKey;
            this.mExtra = new WeakReference( pExtra );
        }

        #endregion Constructor
    }
}
