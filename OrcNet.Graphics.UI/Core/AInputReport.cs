﻿using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Base input report abstract class definition.
    /// </summary>
    public abstract class AInputReport
    {
        #region Fields

        /// <summary>
        /// Stores the input source weak reference.
        /// </summary>
        private WeakReference mInputSource;

        /// <summary>
        /// Stores the input type.
        /// </summary>
        private InputType mType;

        /// <summary>
        /// Stores the input mode.
        /// </summary>
        private InputMode mMode;

        /// <summary>
        /// Stores the time the input occured at.
        /// </summary>
        private int mTimestamp;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the input source weak reference.
        /// </summary>
        public PresentationSource InputSource
        {
            get
            {
                return this.mInputSource.Target as PresentationSource;
            }
        }

        /// <summary>
        /// Gets the input type.
        /// </summary>
        public InputType Type
        {
            get
            {
                return this.mType;
            }
        }

        /// <summary>
        /// Gets the input mode.
        /// </summary>
        public InputMode Mode
        {
            get
            {
                return this.mMode;
            }
        }

        /// <summary>
        /// Gets the time the input occured at.
        /// </summary>
        public int TimeStamp
        {
            get
            {
                return this.mTimestamp;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AInputReport"/> class.
        /// </summary>
        /// <param name="pInputSource">The source input comes from.</param>
        /// <param name="pType">The input type.</param>
        /// <param name="pMode">The input mode.</param>
        /// <param name="pTimestamp">The time the input occured at.</param>
        protected AInputReport(PresentationSource pInputSource, InputType pType, InputMode pMode, int pTimestamp)
        {
            this.mInputSource = new WeakReference( pInputSource );
            this.mType = pType;
            this.mMode = pMode;
            this.mTimestamp = pTimestamp;
        }

        #endregion Constructor
    }
}
