﻿using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// This class returns the point and visual hit during a hit test pass.
    /// </summary>
    public class PointHitTestResult : AHitTestResult
    {
        #region Fields

        /// <summary>
        /// Stores the point in local space of the hit visual.
        /// </summary>
        private Point mHitPoint;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the point in local space of the hit visual.
        /// </summary>
        public Point PointHit
        {
            get
            {
                return mHitPoint;
            }
        }

        /// <summary>
        /// Re-expose Visual property strongly typed to 2D Visual.
        /// </summary>
        public new AVisual VisualHit
        {
            get
            {
                return (AVisual)base.VisualHit;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PointHitTestResult"/> class.
        /// </summary>
        /// <param name="pVisualHit">The visual hit.</param>
        /// <param name="pHitPoint">The point of the hit.</param>
        public PointHitTestResult(AVisual pVisualHit, Point pHitPoint) : 
        base(pVisualHit)
        {
            mHitPoint = pHitPoint;
        }

        #endregion Constructor
    }
}
