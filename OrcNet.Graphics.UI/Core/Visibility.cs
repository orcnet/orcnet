﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// UI element visibility state enumeration definition.
    /// </summary>
    public enum Visibility : byte
    {
        /// <summary>
        /// Visible.
        /// </summary>
        Visible = 0,

        /// <summary>
        /// Takes the requested space into the layout but not visible.
        /// </summary>
        Hidden,

        /// <summary>
        /// Takes no space into the layout and is not visible.
        /// </summary>
        Collapsed
    }
}
