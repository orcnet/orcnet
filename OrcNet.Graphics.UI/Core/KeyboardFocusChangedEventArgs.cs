﻿using OpenTK.Input;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Keyboard focus changed event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void KeyboardFocusChangedEventDelegate(object pSender, KeyboardFocusChangedEventArgs pEventArgs);

    /// <summary>
    /// Specific keyboard focus changes event arguments class definition.
    /// </summary>
    public class KeyboardFocusChangedEventArgs : KeyboardEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the new element that received the keyboard focus.
        /// </summary>
        private IInputElement mNewFocus;

        /// <summary>
        /// Stores the old element that lost the keyboard focus.
        /// </summary>
        private IInputElement mOldFocus;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the new element that received the keyboard focus.
        /// </summary>
        public IInputElement NewFocus
        {
            get
            {
                return this.mNewFocus;
            }
        }

        /// <summary>
        /// Gets the old element that lost the keyboard focus.
        /// </summary>
        public IInputElement OldFocus
        {
            get
            {
                return this.mOldFocus;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyboardFocusChangedEventArgs"/> class.
        /// </summary>
        /// <param name="pKeyboard">The keyboard device associated with the event.</param>
        /// <param name="pTimeStamp">The time when the input occured.</param>
        /// <param name="pOldFocus">The new element that received the keyboard focus.</param>
        /// <param name="pNewFocus">The old element that lost the keyboard focus.</param>
        public KeyboardFocusChangedEventArgs(KeyboardState pKeyboard, int pTimeStamp, IInputElement pOldFocus, IInputElement pNewFocus) :
        base( pKeyboard, pTimeStamp )
        {
            this.mNewFocus = pNewFocus;
            this.mOldFocus = pOldFocus;
        }

        #endregion Constructor
    }
}
