﻿using System;
using System.Collections.Generic;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Route a routed event must follow whan raised.
    /// </summary>
    public sealed class EventRoute
    {
        #region Fields
        
        /// <summary>
        /// Stores the routed event this route is for.
        /// </summary>
        private RoutedEvent mRoutedEvent;

        /// <summary>
        /// Stores the route to follow by the routed event.
        /// </summary>
        private List<RouteItem> mRouteItems;

        /// <summary>
        /// Stores the branching nodes in the route that need to be backtracked while 
        /// augmenting the route
        /// </summary>
        private Stack<BranchNode> mBranchNodes;

        /// <summary>
        /// Stores the source items for separated trees
        /// </summary>
        private List<SourceItem> mSourceItems;

        #endregion Fields

        #region Properties
        
        /// <summary>
        /// Gets or sets the associated routed event.
        /// </summary>
        internal RoutedEvent RoutedEvent
        {
            get
            {
                return this.mRoutedEvent;
            }
            set
            {
                this.mRoutedEvent = value;
            }
        }

        /// <summary>
        /// Gets the branch node set.
        /// </summary>
        private Stack<BranchNode> BranchNodeStack
        {
            get
            {
                if 
                    ( this.mBranchNodes == null )
                {
                    this.mBranchNodes = new Stack<BranchNode>( 1 );
                }

                return this.mBranchNodes;
            }
        }

        #endregion Properties

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="EventRoute"/> class.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event this route is for.</param>
        public EventRoute(RoutedEvent pRoutedEvent)
        {
            if 
                ( pRoutedEvent == null )
            {
                throw new ArgumentNullException("pRoutedEvent");
            }

            this.mRoutedEvent = pRoutedEvent;
            this.mRouteItems  = new List<RouteItem>( 16 );
            this.mSourceItems = new List<SourceItem>( 16 );
        }

        #endregion Construction

        #region Methods
        
        /// <summary>
        /// Adds the given delegate to run for the specified target to the route
        /// </summary>
        /// <param name="pSender">The event sender.</param>
        /// <param name="pDelegate">The delegate to run.</param>
        /// <param name="pProcessHandledEvents">The flag indicating whether the handled events must follow suit or not.</param>
        public void Add(object pSender, Delegate pDelegate, bool pProcessHandledEvents)
        {
            if 
                ( pSender == null )
            {
                throw new ArgumentNullException("pSender");
            }

            if 
                ( pDelegate == null )
            {
                throw new ArgumentNullException("pDelegate");
            }

            RouteItem lRouteItem = new RouteItem( pSender, new RoutedEventHandlerInfo( pDelegate, pProcessHandledEvents ) );

            this.mRouteItems.Add( lRouteItem );
        }

        /// <summary>
        /// Invokes all the handlers that have been added to the route
        /// </summary>
        /// <param name="pSender">The event sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void InvokeHandlers(object pSender, RoutedEventArgs pEventArgs)
        {
            this.InvokeHandlers( pSender, pEventArgs, false );
        }

        /// <summary>
        /// Invokes again all the handlers that have been added to the route
        /// </summary>
        /// <param name="pSender">The event sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void ReInvokeHandlers(object pSender, RoutedEventArgs pEventArgs)
        {
            this.InvokeHandlers( pSender, pEventArgs, true );
        }

        /// <summary>
        /// Invokes all the handlers that have been added to the route
        /// </summary>
        /// <param name="pSender">The event sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        /// <param name="pWasAlreadyRaised">The flag indicating whether the event has been already raised or not.</param>
        private void InvokeHandlers(object pSender, RoutedEventArgs pEventArgs, bool pWasAlreadyRaised)
        {
            if 
                ( pSender == null )
            {
                throw new ArgumentNullException("pSender");
            }

            if
                ( pEventArgs == null )
            {
                throw new ArgumentNullException("pEventArgs");
            }

            if
                ( pEventArgs.Source == null )
            {
                throw new ArgumentException( "No routed event source!!!" );
            }

            if 
                ( pEventArgs.RoutedEvent != this.mRoutedEvent )
            {
                throw new ArgumentException( "Invoking handlers for the wrong routed event!!!" );
            }

            if 
                ( pEventArgs.RoutedEvent.IsDirect )
            {
                int lEndSourceChangeIndex = 0;
                for 
                    ( int lCurr = 0; lCurr < this.mRouteItems.Count; lCurr++ )
                {
                    // Query for new pSender only if we are 
                    // past the range of the previous pSender change
                    if 
                        ( lCurr >= lEndSourceChangeIndex )
                    {
                        // Get the source at this point in the bubble route and also 
                        // the index at which this pSender change seizes to apply
                        object lNewSource = this.GetBubbleSource( lCurr, out lEndSourceChangeIndex );

                        // Set the appropriate sender as source.
                        if 
                            ( pWasAlreadyRaised == false )
                        {
                            if 
                                ( lNewSource == null )
                            {
                                pEventArgs.Source = pSender;
                            }
                            else
                            {
                                pEventArgs.Source = lNewSource;
                            }
                        }
                    }

                    // Invoke listeners
                    this.mRouteItems[ lCurr ].RunDelegate( pEventArgs );
                }
            }
            else
            {
                int lStartSourceChangeIndex = this.mRouteItems.Count;
                int lEndTargetIndex = this.mRouteItems.Count - 1;
                int lStartTargetIndex;
                while 
                    ( lEndTargetIndex >= 0 )
                {
                    // For tunnel events, handlers must be invoked for the last target first. 
                    // However the handlers for that individual target must be fired in the right order. 
                    // Eg. Class Handlers must be fired before Instance Handlers.
                    object lCurrTarget = this.mRouteItems[ lEndTargetIndex ].Target;
                    for 
                        ( lStartTargetIndex = lEndTargetIndex; lStartTargetIndex >= 0; lStartTargetIndex-- )
                    {
                        if 
                            ( this.mRouteItems[ lStartTargetIndex ].Target != lCurrTarget )
                        {
                            break;
                        }
                    }

                    for 
                        ( int lCurr = lStartTargetIndex + 1; lCurr <= lEndTargetIndex; lCurr++ )
                    {
                        // Query for new pSender only if we are 
                        // past the range of the previous pSender change
                        if 
                            ( lCurr < lStartSourceChangeIndex )
                        {
                            // Get the source at this point in the tunnel route and also 
                            // the index at which this pSender change seizes to apply
                            object lNewSource = this.GetTunnelSource( lCurr, out lStartSourceChangeIndex );

                            // Set the appropriate source
                            if 
                                ( lNewSource == null )
                            {
                                pEventArgs.Source = pSender;
                            }
                            else
                            {
                                pEventArgs.Source = lNewSource;
                            }
                        }
                        
                        // Invoke listeners
                        this.mRouteItems[ lCurr ].RunDelegate( pEventArgs );
                    }

                    lEndTargetIndex = lStartTargetIndex;
                }
            }
        }
        
        /// <summary>
        /// Pushes the given node at the top of the stack of branches.
        /// </summary>
        /// <param name="pNode">The node where the visual parent is different from the logical parent.</param>
        /// <param name="pSource">The source that is currently being used, and which should be restored when this branch is popped off the stack.</param>
        public void PushBranchNode(object pNode, object pSource)
        {
            BranchNode lBranchNode = new BranchNode();
            lBranchNode.Node   = pNode;
            lBranchNode.Source = pSource;

            this.BranchNodeStack.Push( lBranchNode );
        }
        
        /// <summary>
        /// Pops the given node from the top of the stack of branches.
        /// </summary>
        /// <returns>The node where the visual parent was different from the logical parent.</returns>
        public object PopBranchNode()
        {
            if
                ( this.BranchNodeStack.Count == 0 )
            {
                return null;
            }

            BranchNode lBranchNode = this.BranchNodeStack.Pop();
            return lBranchNode.Node;
        }
        
        /// <summary>
        /// Peeks the given node from the top of the stack of branches.
        /// </summary>
        /// <returns>The node where the visual parent was different from the logical parent.</returns>
        public object PeekBranchNode()
        {
            if 
                ( this.BranchNodeStack.Count == 0 )
            {
                return null;
            }

            BranchNode lBranchNode = this.BranchNodeStack.Peek();
            return lBranchNode.Node;
        }
        
        /// <summary>
        /// Peeks the given source from the top of the stack of branches.
        /// </summary>
        /// <returns>The source that was stored along with the node where the visual parent was different from the logical parent.</returns>
        public object PeekBranchSource()
        {
            if 
                ( this.BranchNodeStack.Count == 0 )
            {
                return null;
            }

            BranchNode lBranchNode = this.BranchNodeStack.Peek();
            return lBranchNode.Source;
        }

        /// <summary>
        /// Adds the given source to the source item list indicating what the source will be this point 
        /// onwards in the route
        /// </summary>
        /// <param name="pSource">The source to add.</param>
        internal void AddSource(object pSource)
        {
            int lStartIndex = this.mRouteItems.Count;
            this.mSourceItems.Add( new SourceItem( lStartIndex, pSource ) );
        }

        /// <summary>
        /// Determine what the RoutedEventArgs.Source should be, at this
        /// point in the bubble. Also the endIndex output parameter tells 
        /// you the exact index of the handlersList at which this source 
        /// change ceases to apply
        /// </summary>
        /// <param name="pIndex">The index of the source to look for.</param>
        /// <param name="pEndIndex">The resulting ending index.</param>
        /// <returns></returns>
        private object GetBubbleSource(int pIndex, out int pEndIndex)
        {
            // If the Source never changes during the route execution,
            // then we're done (just return null).
            if
                ( this.mSourceItems.Count == 0 )
            {
                pEndIndex = this.mRouteItems.Count;
                return null;
            }

            // Similarly, if we're not to the point of the route of the first Source
            // change, simply return null.
            int lFirstStartIndex = this.mSourceItems[ 0 ].StartIndex;
            if 
                ( pIndex < lFirstStartIndex )
            {
                pEndIndex = lFirstStartIndex;
                return null;
            }

            // See if we should be using one of the intermediate
            // sources
            for 
                ( int lCurr = 0; lCurr < this.mSourceItems.Count - 1; lCurr++ )
            {
                if 
                    ( pIndex >= this.mSourceItems[ lCurr ].StartIndex &&
                      pIndex < this.mSourceItems[ lCurr + 1 ].StartIndex )
                {
                    pEndIndex = this.mSourceItems[ lCurr + 1 ].StartIndex;
                    return this.mSourceItems[ lCurr ].Source;
                }
            }

            // If we get here, we're on the last one,
            // so return that.            
            pEndIndex = this.mRouteItems.Count;
            return this.mSourceItems[ this.mSourceItems.Count - 1 ].Source;
        }

        /// <summary>
        /// Determine what the RoutedEventArgs.Source should be, at this
        /// point in the tunnel. Also the startIndex output parameter tells 
        /// you the exact index of the handlersList at which this source 
        /// change starts to apply
        /// </summary>
        /// <param name="pIndex">The index of the source to look for.</param>
        /// <param name="pStartIndex">The resulting starting index.</param>
        /// <returns></returns>
        private object GetTunnelSource(int pIndex, out int pStartIndex)
        {
            // If the Source never changes during the route execution,
            // then we're done (just return null).
            if
                ( this.mSourceItems.Count == 0 )
            {
                pStartIndex = 0;
                return null;
            }

            // Similarly, if we're past the point of the route of the first Source
            // change, simply return null.
            if 
                ( pIndex < this.mSourceItems[ 0 ].StartIndex )
            {
                pStartIndex = 0;
                return null;
            }

            // See if we should be using one of the intermediate
            // sources
            for 
                ( int lCurr = 0; lCurr < this.mSourceItems.Count - 1; lCurr++ )
            {
                if 
                    ( pIndex >= this.mSourceItems[ lCurr ].StartIndex &&
                      pIndex < this.mSourceItems[ lCurr + 1 ].StartIndex )
                {
                    pStartIndex = this.mSourceItems[ lCurr ].StartIndex;
                    return this.mSourceItems[ lCurr ].Source;
                }
            }

            // If we get here, we're on the last one, so return that.            
            pStartIndex = this.mSourceItems[ this.mSourceItems.Count - 1 ].StartIndex;
            return this.mSourceItems[ this.mSourceItems.Count - 1 ].Source;
        }

        /// <summary>
        /// Clear all route's items.
        /// </summary>
        internal void Clear()
        {
            this.mRoutedEvent = null;

            this.mRouteItems.Clear();
            this.mSourceItems.Clear();

            if 
                ( this.mBranchNodes != null )
            {
                this.mBranchNodes.Clear();
            }
        }

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// A BranchNode indicates a point in the tree where the logical and
        /// visual structure might diverge.  When building a route, we store
        /// this branch node for every logical link we find.  Along with the
        /// node where the possible divergence occurred, we store the source
        /// that the event should use.  This is so that the source of an
        /// event will always be in the logical tree of the element handling
        /// the event.
        /// </summary>
        private struct BranchNode
        {
            public object Node;
            public object Source;
        }

        #endregion Inner classes
    }
}
