﻿using OrcNet.Graphics.UI.Managers;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Process input event arguments prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void ProcessInputEventDelegate(object pSender, ProcessInputEventArgs pEventArgs);

    /// <summary>
    /// Process input event arguments class definition.
    /// </summary>
    public class ProcessInputEventArgs : NotifyInputEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the input can be accessed or not.
        /// </summary>
        private bool mAllowAccessToInput;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="ProcessInputEventArgs"/> class.
        /// </summary>
        internal ProcessInputEventArgs()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Pushes a new input event arguments to the manager stack.
        /// </summary>
        /// <param name="pInput">The new input to process.</param>
        /// <returns>The new input stacked.</returns>
        public InputEventArgs PushInput(InputEventArgs pInput)
        {
            if
                ( this.mAllowAccessToInput == false )
            {
                return null;
            }

            return InputManager.Instance.PushInput( pInput );
        }

        /// <summary>
        /// Peeks an input from the manager stack without removing it.
        /// </summary>
        /// <returns>The peeked input.</returns>
        public InputEventArgs PeekInput()
        {
            if
                ( this.mAllowAccessToInput == false )
            {
                return null;
            }

            return InputManager.Instance.PeekInput();
        }

        /// <summary>
        /// Pops an input from the manager stack.
        /// </summary>
        /// <returns>The popped input.</returns>
        public InputEventArgs PopInput()
        {
            if
                ( this.mAllowAccessToInput == false )
            {
                return null;
            }

            return InputManager.Instance.PopInput();
        }

        /// <summary>
        /// Resets the input event notifier for object recycling purpose only.
        /// </summary>
        /// <param name="pInput">The input that replace the old one.</param>
        internal override void Reset(InputEventArgs pInput)
        {
            this.mAllowAccessToInput = true;
            base.Reset( pInput );
        }

        #endregion Methods
    }
}
