﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Input report event arguments class definition containing information about 
    /// an input report that is being processed.
    /// </summary>
    internal class InputReportEventArgs : InputEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the input report.
        /// </summary>
        private AInputReport mReport;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the input report.
        /// </summary>
        public AInputReport Report
        {
            get
            {
                return this.mReport;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InputReportEventArgs"/> class.
        /// </summary>
        /// <param name="pDevice">The input device associated with the event.</param>
        /// <param name="pReport">The input report</param>
        public InputReportEventArgs(object pDevice, AInputReport pReport) :
        base( pDevice, pReport != null ? pReport.TimeStamp : -1 )
        {
            this.mReport = pReport;
        }

        #endregion Constructor
    }
}
