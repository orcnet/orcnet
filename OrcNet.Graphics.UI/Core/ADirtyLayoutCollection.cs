﻿using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using OrcNet.Graphics.UI.Services;
using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Base dirty layout UI element collection abstract class definition.
    /// </summary>
    internal abstract class ADirtyLayoutCollection
    {
        #region Fields

        /// <summary>
        /// Stores the size of the pre-allocated node slots.
        /// </summary>
        private const int cSlotCount = 153;

        /// <summary>
        /// Stores the amount of reserved slots in the collection
        /// as when this many elements remain in the collection,
        /// collection will switch to invalidating up and adding only the root
        /// </summary>
        private const int cReservedSlots = 8;

        /// <summary>
        /// Stores the current dirty collection size.
        /// </summary>
        private int       mCurrentSize;

        /// <summary>
        /// Stores the collection head node.
        /// </summary>
        private DirtyNode mHead;

        /// <summary>
        /// Stores the current collection node.
        /// </summary>
        private DirtyNode mCurrent;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the collection is empty or not.
        /// </summary>
        internal bool IsEmpty
        {
            get
            {
                return this.mHead == null;
            }
        }

        /// <summary>
        /// Gets the top most dirty element from the collection.
        /// </summary>
        internal UIElement Top
        {
            get
            {
                UIElement lToReturn = null;
                ulong treeLevel = ulong.MaxValue;

                for
                    ( DirtyNode lCurrent = this.mHead;
                                lCurrent != null;
                                lCurrent = lCurrent.Next )
                {
                    UIElement lElement = lCurrent.DirtyElement;
                    ulong lLevel = lElement.TreeLevel;
                    if
                        ( lLevel < treeLevel )
                    {
                        treeLevel = lLevel;
                        lToReturn = lCurrent.DirtyElement;
                    }
                }

                return lToReturn;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ADirtyLayoutCollection"/> class.
        /// </summary>
        protected ADirtyLayoutCollection()
        {
            DirtyNode lNode;
            for 
                ( int i = 0; i < cSlotCount; i++ )
            {
                lNode = new DirtyNode();
                lNode.Next = this.mCurrent;
                this.mCurrent = lNode;
            }

            this.mCurrentSize = cSlotCount;
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Checks whether the given element is already added as dirty element to that collection.
        /// </summary>
        /// <param name="pElement">The element to look for.</param>
        /// <returns>True if already in, false otherwise.</returns>
        internal abstract bool Contains(UIElement pElement);

        /// <summary>
        /// Adds a new dirty layout UI element 
        /// </summary>
        /// <param name="pElement">The dirty element.</param>
        internal void Add(UIElement pElement)
        {
            if 
                ( this.Contains( pElement ) )
            {
                return;
            }

            if
                ( pElement.CheckFlagsAnd( VisualFlags.IsLayoutSuspended ) )
            {
                return;
            }

            // Free the collection of orphans if any.
            this.RemoveOrphans( pElement );

            UIElement lParent = pElement.GetUIParentWithinLayoutIsland();
            if 
                ( lParent != null && 
                  this.CanRelyOnParentValidation( lParent ) )
            {
                return;
            }
            
            //10 is arbitrary number here, simply indicates the queue is
            //about to be filled. If not queue is not almost full, simply add
            //the element to it. If it is almost full, start conserve entries
            //by escalating invalidation to all the ancestors until the top of
            //the visual tree, and only add root of visula tree to the queue.
            if
                ( this.mCurrentSize > cReservedSlots )
            {
                this.AddDirtyNode( pElement );
            }
            else
            {
                //walk up until we are the topmost UIElement in the tree.
                //on each step, mark the lParent dirty and remove it from the queues
                //only leave a single node in the queue - the root of visual tree
                while 
                    ( pElement != null )
                {
                    UIElement lCurrentParent = pElement.GetUIParentWithinLayoutIsland();

                    this.Invalidate( pElement );

                    if
                        ( lCurrentParent != null && 
                          lCurrentParent.Visibility != Visibility.Collapsed )
                    {
                        this.Remove( pElement );
                    }
                    // Else it is either a root or a collapsed node
                    else
                    {
                        if 
                            ( this.Contains( pElement ) == false )
                        {
                            this.RemoveOrphans( pElement );
                            this.AddDirtyNode( pElement );
                        }
                    }

                    pElement = lCurrentParent;
                }
            }

            UIService lUIService = ServiceManager.Instance.GetService<UIService>();
            if
                ( lUIService != null )
            {
                lUIService.RequestLayoutUpdate();
            }
        }

        /// <summary>
        /// Removes the given UI element from the dirty collection.
        /// </summary>
        /// <param name="pElement">The UI element to remove.</param>
        internal void Remove(UIElement pElement)
        {
            DirtyNode lToRemove = this.GetNode( pElement );
            if 
                ( lToRemove == null )
            {
                return;
            }

            this.RemoveDirtyNode( lToRemove );
            this.DetachFromNode( pElement );
        }

        /// <summary>
        /// Removes orphans elements if any.
        /// </summary>
        /// <param name="pParent">The UI element search must start at.</param>
        internal void RemoveOrphans(UIElement pParent)
        {
            DirtyNode lCurrent = this.mHead;
            while 
                ( lCurrent != null )
            {
                UIElement lChild = lCurrent.DirtyElement;
                DirtyNode lNext  = lCurrent.Next;
                ulong lParentTreeLevel = pParent.TreeLevel;
                if 
                    ( lChild.TreeLevel == lParentTreeLevel + 1 && 
                      lChild.GetUIParentWithinLayoutIsland() == pParent )
                {
                    this.RemoveDirtyNode( this.GetNode( lChild ) );
                    this.DetachFromNode( lChild );
                }

                lCurrent = lNext;
            }
        }

        /// <summary>
        /// Gets the node the given UI element is attached to.
        /// </summary>
        /// <param name="pElement">The UI element the node must be returned.</param>
        /// <returns>The UI element's node it is attached to, Null otherwise.</returns>
        protected abstract DirtyNode GetNode(UIElement pElement);

        /// <summary>
        /// Attaches a node and a UI element together for fast retrieval.
        /// </summary>
        /// <param name="pNode">The node to attach with the UI element. (Null means no attachment)</param>
        /// <param name="pToAttach">The UI element to attach to the given node.</param>
        protected abstract void AttachToNode(DirtyNode pNode, UIElement pToAttach);

        /// <summary>
        /// Detaches the given UI element from its Node.
        /// </summary>
        /// <param name="pElement">The element that must be detached from its node.</param>
        protected void DetachFromNode(UIElement pElement)
        {
            this.AttachToNode( null, pElement );
        }

        /// <summary>
        /// Gets the flag indicating whether the given parent is sufficient
        /// to assure its children will be valid without having to re compute them.
        /// </summary>
        /// <param name="pParent">The parent to check.</param>
        /// <returns>True if parent is sufficient to avoid children computation, false otherwise.</returns>
        protected abstract bool CanRelyOnParentValidation(UIElement pParent);

        /// <summary>
        /// Invalidates the given element to inform it needs to be re computed.
        /// </summary>
        /// <param name="pElement">The element to invalidate.</param>
        protected abstract void Invalidate(UIElement pElement);

        /// <summary>
        /// Adds a new dirty node.
        /// </summary>
        /// <param name="pElement">The owned UI element of the new dirty node.</param>
        private void AddDirtyNode(UIElement pElement)
        {
            DirtyNode lNewNode = this.CreateNode( pElement );
            if 
                ( lNewNode != null )
            {
                lNewNode.Next = this.mHead;
                if 
                    ( this.mHead != null )
                {
                    this.mHead.Previous = lNewNode;
                }

                this.mHead = lNewNode;

                this.AttachToNode( lNewNode, pElement );
            }
        }

        /// <summary>
        /// Removes a dirty node from the collection and 
        /// prepare it to be reused another time.
        /// </summary>
        /// <param name="pToRemove">The node to remove.</param>
        private void RemoveDirtyNode(DirtyNode pToRemove)
        {
            if 
                ( pToRemove.Previous == null )
            {
                this.mHead = pToRemove.Next;
            }
            else
            {
                pToRemove.Previous.Next = pToRemove.Next;
            }

            if 
                ( pToRemove.Next != null )
            {
                pToRemove.Next.Previous = pToRemove.Previous;
            }

            this.PrepareToReuse( pToRemove );
        }

        /// <summary>
        /// Creates a new dirty node in the collection.
        /// </summary>
        /// <param name="pElement">The owned UI element of the new dirty node.</param>
        /// <returns>The newly created dirty node containing the UI element.</returns>
        private DirtyNode CreateNode(UIElement pElement)
        {
            DirtyNode lNewNode = null;
            if 
                ( this.mCurrent != null)
            {
                lNewNode = this.mCurrent;
                this.mCurrent = lNewNode.Next;
                this.mCurrentSize--;
                lNewNode.Next = lNewNode.Previous = null;
            }
            else
            {
                try
                {
                    lNewNode = new DirtyNode();
                }
                catch 
                    ( OutOfMemoryException pEx )
                {
                    LogManager.Instance.Log( pEx );
                    return null;
                }
            }

            lNewNode.DirtyElement = pElement;
            return lNewNode;
        }

        /// <summary>
        /// Prepares a dirty node to be reused by freeing and 
        /// replacing it in the collection. 
        /// </summary>
        /// <param name="pToReuse">The node to reuse</param>
        private void PrepareToReuse(DirtyNode pToReuse)
        {
            pToReuse.DirtyElement = null;

            if 
                ( this.mCurrentSize < cSlotCount )
            {
                pToReuse.Next = this.mCurrent;
                this.mCurrent = pToReuse;
                this.mCurrentSize++;
            }
        }

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Collection node class definition.
        /// </summary>
        protected internal class DirtyNode
        {
            /// <summary>
            /// Stores the dirty element.
            /// </summary>
            public UIElement DirtyElement;

            /// <summary>
            /// Stores the next Dirty node.
            /// </summary>
            public DirtyNode Next;

            /// <summary>
            /// Stores the previous Dirty node.
            /// </summary>
            public DirtyNode Previous;
        }

        #endregion Inner classes
    }
}
