﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Marker input event arguments class definition in charge of
    /// delimiting a set of inputs.
    /// </summary>
    public class MarkerInputEventArgs : InputEventArgs
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MarkerInputEventArgs"/> class.
        /// </summary>
        public MarkerInputEventArgs() :
        base( null, 0 )
        {

        }

        #endregion Constructor
    }
}
