﻿using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// This is the class for specifying parameters hit testing with a point.
    /// </summary>
    public class PointHitTestParameters : AHitTestParameters
    {
        #region Fields

        /// <summary>
        /// Stores the point to hit test against.
        /// </summary>
        private Point mHitPoint;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the point to hit test against.
        /// </summary>
        public Point HitPoint
        {
            get
            {
                return this.mHitPoint;
            }
            internal set
            {
                this.mHitPoint = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PointHitTestParameters"/> class.
        /// </summary>
        public PointHitTestParameters(Point pPoint) : base()
        {
            this.mHitPoint = pPoint;
        }

        #endregion Constructor
    }
}
