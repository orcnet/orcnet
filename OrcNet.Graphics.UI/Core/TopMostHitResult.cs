﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Hit result containing the top most hit visual.
    /// </summary>
    internal class TopMostHitResult
    {
        #region Fields

        /// <summary>
        /// Stores the hit result.
        /// </summary>
        internal AHitTestResult HitResult = null;

        #endregion Fields

        #region Methods

        /// <summary>
        /// Delegate in charge of stopping or not the hit test by checking
        /// the hit result each time it goes through a new visual.
        /// </summary>
        /// <param name="pResult">The hit result.</param>
        /// <returns>The behavior while going through the hit pass.</returns>
        internal HitTestResultBehavior HitTestResult(AHitTestResult pResult)
        {
            this.HitResult = pResult;

            return HitTestResultBehavior.Stop;
        }

        /// <summary>
        /// Delegate informing about the filtering behavior during the hit pass.
        /// </summary>
        /// <param name="pPotentialHitTestTarget"></param>
        /// <returns></returns>
        internal HitTestFilterBehavior NoNested2DFilter(DependencyObject pPotentialHitTestTarget)
        {
            return HitTestFilterBehavior.Continue;
        }

        #endregion Methods
    }
}
