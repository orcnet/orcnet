﻿using OpenTK.Input;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Key event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void KeyEventDelegate(object pSender, KeyEventArgs pEventArgs);

    /// <summary>
    /// Key event arguments class definition.
    /// </summary>
    public class KeyEventArgs : KeyboardEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the key.
        /// </summary>
        private Key mKeyData;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the event key is up or not.
        /// </summary>
        public bool IsUp
        {
            get
            {
                return this.KeyboardDevice.IsKeyUp( this.mKeyData );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the event key is down or not.
        /// </summary>
        public bool IsDown
        {
            get
            {
                return this.KeyboardDevice.IsKeyDown( this.mKeyData );
            }
        }

        /// <summary>
        /// Gets the key.
        /// </summary>
        public Key KeyData
        {
            get
            {
                return this.mKeyData;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Alt key has been pressed or not.
        /// </summary>
        public bool Alt
        {
            get
            {
                KeyboardState lState = this.KeyboardDevice;
                return lState.IsKeyDown( Key.AltLeft ) ||
                       lState.IsKeyDown( Key.AltRight );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Ctrl key has been pressed or not.
        /// </summary>
        public bool Ctrl
        {
            get
            {
                KeyboardState lState = this.KeyboardDevice;
                return lState.IsKeyDown( Key.ControlLeft ) ||
                       lState.IsKeyDown( Key.ControlRight );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Shift key has been pressed or not.
        /// </summary>
        public bool Shift
        {
            get
            {
                KeyboardState lState = this.KeyboardDevice;
                return lState.IsKeyDown( Key.ShiftLeft ) ||
                       lState.IsKeyDown( Key.ShiftRight );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyEventArgs"/> class.
        /// </summary>
        /// <param name="pKeyboard">The keyboard device.</param>
        /// <param name="pTimeStamp">The key timestamp.</param>
        /// <param name="pKey">The involved key.</param>
        public KeyEventArgs(KeyboardState pKeyboard, int pTimeStamp, Key pKey) :
        base( pKeyboard, pTimeStamp )
        {
            this.mKeyData = pKey;
        }

        #endregion Constructor
    }
}
