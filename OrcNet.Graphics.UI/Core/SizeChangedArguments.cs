﻿using OrcNet.Graphics.UI.Maths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// UI element size changed arguments class definition.
    /// </summary>
    public class SizeChangedArguments
    {
        #region Fields

        /// <summary>
        /// Stores the next element the size has changed 
        /// </summary>
        private SizeChangedArguments mNext;

        /// <summary>
        /// Stores the UI element involved in the size change.
        /// </summary>
        private UIElement mEditedElement;

        /// <summary>
        /// Stores the previous edited UI element size.
        /// </summary>
        private Size      mPreviousSize;

        /// <summary>
        /// Stores the flag indicating whether the width has changed or not.
        /// </summary>
        private bool      mIsWidthChanged;

        /// <summary>
        /// Stores the flag indicating whether the height has changed or not.
        /// </summary>
        private bool      mIsHeightChanged;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the next element the size has changed 
        /// </summary>
        internal SizeChangedArguments Next
        {
            get
            {
                return this.mNext;
            }
            set
            {
                this.mNext = value;
            }
        }

        /// <summary>
        /// Gets the UI element involved in the size change.
        /// </summary>
        internal UIElement EditedElement
        {
            get
            {
                return this.mEditedElement;
            }
        }

        /// <summary>
        /// Gets the edited UI element new size.
        /// </summary>
        public Size NewSize
        {
            get
            {
                return this.mEditedElement.RenderSize;
            }
        }

        /// <summary>
        /// Gets the previous edited UI element size.
        /// </summary>
        public Size PreviousSize
        {
            get
            {
                return this.mPreviousSize;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the width has changed or not.
        /// </summary>
        public bool IsWidthChanged
        {
            get
            {
                return this.mIsWidthChanged;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the height has changed or not.
        /// </summary>
        public bool IsHeightChanged
        {
            get
            {
                return this.mIsHeightChanged;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SizeChangedArguments"/> class.
        /// </summary>
        /// <param name="pEditedElement">The UI element involved in the size change.</param>
        /// <param name="pPreviousSize">The previous edited UI element size.</param>
        /// <param name="pIsWidthChanged">The flag indicating whether the width has changed or not.</param>
        /// <param name="pIsHeightChanged">The flag indicating whether the height has changed or not.</param>
        public SizeChangedArguments(UIElement pEditedElement, Size pPreviousSize, bool pIsWidthChanged, bool pIsHeightChanged)
        {
            this.mEditedElement   = pEditedElement;
            this.mPreviousSize    = pPreviousSize;
            this.mIsWidthChanged  = pIsWidthChanged;
            this.mIsHeightChanged = pIsHeightChanged;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Accumulates the changes flags.
        /// </summary>
        /// <param name="pIsWidthChanged">The flag indicating whether the width has changed or not.</param>
        /// <param name="pIsHeightChanged">The flag indicating whether the height has changed or not.</param>
        internal void Accumulate(bool pIsWidthChanged, bool pIsHeightChanged)
        {
            this.mIsWidthChanged  |= pIsWidthChanged;
            this.mIsHeightChanged |= pIsHeightChanged;
        }

        #endregion Methods
    }
}
