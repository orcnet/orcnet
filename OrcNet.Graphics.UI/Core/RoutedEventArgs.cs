﻿using OrcNet.Core.Logger;
using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Routed event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void RoutedEventDelegate(object pSender, RoutedEventArgs pEventArgs);

    /// <summary>
    /// Routed event arguments class definition owning the
    /// info relative to a routed event.
    /// </summary>
    public class RoutedEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the routed event is handled or not.
        /// </summary>
        private bool mIsHandled;

        /// <summary>
        /// Stores the flag indicating whether the user initiated the routed event or not.
        /// </summary>
        private bool mIsInitiated;

        /// <summary>
        /// Stores the flag indicating whether the routed event delegate is being invoked or not.
        /// </summary>
        private bool mIsInvokingHandler;

        /// <summary>
        /// Stores the routed event associated with the event arguments
        /// </summary>
        private RoutedEvent mRoutedEvent;

        /// <summary>
        /// Stores the routed event source object.
        /// </summary>
        private object  mSource;

        /// <summary>
        /// Stores the routed event original source object which can differ from
        /// the source object.
        /// </summary>
        private object  mOriginalSource;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the flag indicating whether the routed event delegate is being invoked or not.
        /// </summary>
        public bool IsInvokingHandler
        {
            get
            {
                return this.mIsInvokingHandler;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the event has been handled or not.
        /// </summary>
        public bool IsHandled
        {
            get
            {
                return this.mIsHandled;
            }
            set
            {
                if
                    ( this.mRoutedEvent == null )
                {
                    throw new InvalidOperationException( "No routed event!!!" );
                }

                this.mIsHandled = value;
            }
        }

        /// <summary>
        /// Gets or sets the routed event source.
        /// </summary>
        public object Source
        {
            get
            {
                return this.mSource;
            }
            set
            {
                object lSource = value;
                if 
                    ( this.mSource == null && 
                      this.mOriginalSource == null )
                {
                    // First time, init both.
                    this.mSource = this.mOriginalSource = lSource;
                    this.OnSourceChanged( null, lSource );
                }
                else if 
                    ( this.mSource != lSource )
                {
                    object lOld  = this.mSource;
                    this.mSource = lSource;
                    this.OnSourceChanged( lOld, lSource );
                }
            }
        }

        /// <summary>
        /// Gets the routed event original source object which can differ from
        /// the source object.
        /// </summary>
        public object OriginalSource
        {
            get
            {
                return this.mOriginalSource;
            }
        }

        /// <summary>
        /// Gets or sets the routed event associated with the event arguments
        /// </summary>
        public RoutedEvent RoutedEvent
        {
            get
            {
                return this.mRoutedEvent;
            }
            set
            {
                this.mRoutedEvent = value;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the user initiated the routed event or not.
        /// </summary>
        internal bool IsInitiated
        {
            get
            {
                return this.mIsInitiated;
            }
            set
            {
                this.mIsInitiated = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RoutedEventArgs"/> class.
        /// </summary>
        public RoutedEventArgs() :
        this( null, null )
        {

        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RoutedEventArgs"/> class.
        /// </summary>
        /// <param name="pRoutedEvent">The new routed event.</param>
        public RoutedEventArgs(RoutedEvent pRoutedEvent) :
        this( pRoutedEvent, null )
        {
            
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RoutedEventArgs"/> class.
        /// </summary>
        /// <param name="pRoutedEvent">The new routed event.</param>
        /// <param name="pSource">The routed event source trigger object.</param>
        public RoutedEventArgs(RoutedEvent pRoutedEvent, object pSource)
        {
            this.mIsInvokingHandler = false;
            this.mOriginalSource = pSource;
            this.mSource = pSource;
            this.mRoutedEvent = pRoutedEvent;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Triggers the routed event by running the given delegate.
        /// </summary>
        /// <param name="pDelegate">The delegate to run</param>
        /// <param name="pTarget">The delegate sender.</param>
        internal void RunDelegate(Delegate pDelegate, object pTarget)
        {
            this.mIsInvokingHandler = true;

            try
            {
                this.NotifyRoutedEvent( pDelegate, pTarget );
            }
            catch
                ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            this.mIsInvokingHandler = false;
        }

        /// <summary>
        /// Notifies the routed event by running the given delegate.
        /// </summary>
        /// <param name="pDelegate">The delegate to run</param>
        /// <param name="pTarget">The delegate sender.</param>
        protected virtual void NotifyRoutedEvent(Delegate pDelegate, object pTarget)
        {
            if 
                ( pDelegate == null )
            {
                throw new ArgumentNullException("pDelegate");
            }

            if 
                ( pTarget == null )
            {
                throw new ArgumentNullException("pTarget");
            }

            if
                ( this.mRoutedEvent == null )
            {
                throw new InvalidOperationException("No routed event!!!");
            }

            this.mIsInvokingHandler = true;

            try
            {
                if 
                    ( pDelegate is RoutedEventDelegate )
                {
                    ((RoutedEventDelegate)pDelegate)( pTarget, this );
                }
                else
                {
                    // Restricted Action - reflection permission required
                    pDelegate.DynamicInvoke( new object[] { pTarget, this } );
                }
            }
            catch
                ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            this.mIsInvokingHandler = false;
        }

        /// <summary>
        /// Delegate called on event source changes.
        /// </summary>
        /// <param name="pOldSource">THe old source.</param>
        /// <param name="pNewSource">The new source.</param>
        protected virtual void OnSourceChanged(object pOldSource, object pNewSource)
        {
            // To override.
        }

        #endregion Methods
    }
}
