﻿using OpenTK.Input;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Mouse event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void MouseEventDelegate(object pSender, MouseEventArgs pEventArgs);

    /// <summary>
    /// Mouse event arguments class definition.
    /// </summary>
    public class MouseEventArgs : InputEventArgs
    {
        #region Properties

        /// <summary>
        /// Gets the mouse device associated with the event.
        /// </summary>
        public MouseState MouseDevice
        {
            get
            {
                return (MouseState)this.Device;
            }
        }

        /// <summary>
        /// Gets the state of the left button.
        /// </summary>
        public MouseButtonState LeftButton
        {
            get
            {
                return this.MouseDevice[ MouseButton.Left ] ? MouseButtonState.Pressed : MouseButtonState.Released;
            }
        }

        /// <summary>
        /// Gets the state of the right button.
        /// </summary>
        public MouseButtonState RightButton
        {
            get
            {
                return this.MouseDevice[ MouseButton.Right ] ? MouseButtonState.Pressed : MouseButtonState.Released;
            }
        }

        /// <summary>
        /// Gets the state of the middle button.
        /// </summary>
        public MouseButtonState MiddleButton
        {
            get
            {
                return this.MouseDevice[ MouseButton.Middle ] ? MouseButtonState.Pressed : MouseButtonState.Released;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseEventArgs"/> class.
        /// </summary>
        /// <param name="pDevice">The mouse device associated with the event.</param>
        /// <param name="pTimeStamp">The time when the input occured.</param>
        public MouseEventArgs(MouseState pDevice, int pTimeStamp) :
        base( pDevice, pTimeStamp )
        {
            
        }

        #endregion Constructor
    }
}
