﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Data-structure representing a linked list of all a class's 
    /// handlers for a type and its base types.
    /// </summary>
    internal class RoutedEventDelegateInfoContainer
    {
        #region Fields

        /// <summary>
        /// Stores the set of delegates.
        /// </summary>
        internal RoutedEventHandlerInfo[] Delegates;

        /// <summary>
        /// Stores the next linked list delegates container.
        /// </summary>
        internal RoutedEventDelegateInfoContainer Next;

        #endregion Fields

        #region Methods

        /// <summary>
        /// Checks whether the given delegates are contained in that delegates info container or not.
        /// </summary>
        /// <param name="pDelegates">The set of delegates to look for.</param>
        /// <returns>True if contained in that container node, false otherwise.</returns>
        internal bool Contains(RoutedEventDelegateInfoContainer pDelegates)
        {
            RoutedEventDelegateInfoContainer lHandlers = this;
            while 
                ( lHandlers != null )
            {
                if 
                    ( lHandlers == pDelegates )
                {
                    return true;
                }

                lHandlers = lHandlers.Next;
            }

            return false;
        }

        #endregion Methods
    }
}
