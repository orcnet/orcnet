﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Enumeration of the type(s) of input being reported.
    /// </summary>
    /// <remarks>HID indicates that the input was provided by a Human Interface Device that was not a keyboard, a mouse, or a stylus.</remarks>
    public enum InputType
    {
        /// <remarks>
        /// The input comes from a keyboard.
        /// </remarks>
        Keyboard = 0,

        /// <remarks>
        /// The input comes from a mouse.
        /// </remarks>
        Mouse,
        
        /// <remarks>
        /// The input comes from a HID device that is not a keyboard or a mouse.
        /// </remarks>
        Hid
    }
}
