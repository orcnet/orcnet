﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Enum controls behavior when a positive hit occurs during hit testing.
    /// </summary>
    public enum HitTestResultBehavior
    {
        /// <summary>
        /// Stop any further hit testing and return.
        /// </summary>
        Stop,

        /// <summary>
        /// Continue hit testing against next visual.
        /// </summary>
        Continue
    };
}
