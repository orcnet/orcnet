﻿using OpenTK.Input;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Mouse wheel event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    public delegate void MouseWheelEventDelegate(object pSender, MouseWheelEventArgs pEventArgs);

    /// <summary>
    /// Mouse wheel event arguments class definition.
    /// </summary>
    public class MouseWheelEventArgs : MouseEventArgs
    {
        #region Fields

        /// <summary>
        /// Stores the mouse wheel amount of tick it turned.
        /// </summary>
        private int mDelta;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the mouse wheel amount of tick it turned.
        /// </summary>
        public int Delta
        {
            get
            {
                return this.mDelta;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseWheelEventArgs"/> class.
        /// </summary>
        /// <param name="pDevice">The mouse device associated with the event.</param>
        /// <param name="pTimeStamp">The time when the input occured.</param>
        /// <param name="pDelta">The amount of wheel tick it turned.</param>
        public MouseWheelEventArgs(MouseState pDevice, int pTimeStamp, int pDelta) :
        base( pDevice, pTimeStamp )
        {
            this.mDelta = pDelta;
        }

        #endregion Constructor
    }
}
