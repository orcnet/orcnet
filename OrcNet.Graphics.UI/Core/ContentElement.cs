﻿using OrcNet.Core;
using OrcNet.Graphics.UI.Helpers;
using System;

namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// NOT USED FOR NOW.
    /// </summary>
    public class ContentElement : DependencyObject, IInputElement
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the EventDelegatesCache exists or not.
        /// </summary>
        private bool mHasEventDelegatesCache;

        /// <summary>
        /// Stores the routed event delegates cache for this UI element.
        /// </summary>
        internal EventDelegatesCache mEventDelegatesCache;

        /// <summary>
        /// Stores the Element parent.
        /// </summary>
        internal DependencyObject Parent;

        /// <summary>
        /// Stores the flag indicating whether the mouse os over that content element or not.
        /// </summary>
        private bool mIsMouseOver;

        /// <summary>
        /// Stores the flag indicating whether the mouse is entered in the element bounds or not.
        /// </summary>
        private bool mIsMouseCaptured;

        /// <summary>
        /// Stores the flag indicating whether the mouse captured within that element or not.
        /// </summary>
        private bool mIsMouseCaptureWithin;

        /// <summary>
        /// Stores the flag indicating whether the mouse has that element as Top most regarding
        /// to those in the region the mouse is in.
        /// </summary>
        private bool mIsMouseDirectlyOver;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event triggered on IsEnabled property changes.
        /// </summary>
        public event PropertyChangedEventDelegate IsEnabledChanged;

        /// <summary>
        /// Event triggered automatically to notify different Element changes.
        /// </summary>
        public event AutomaticEventDelegate AutomaticEvent;

        /// <summary>
        /// Event triggered when the keyboard has the focus on that element.
        /// </summary>
        public event KeyboardFocusChangedEventDelegate GotKeyboardFocus;

        /// <summary>
        /// Event triggered when the element has the mouse in its region.
        /// </summary>
        public event MouseEventDelegate GotMouseCapture;

        /// <summary>
        /// Event triggered when the element has the keyboard focus and a key is pressed.
        /// </summary>
        public event KeyEventDelegate KeyDown;

        /// <summary>
        /// Event triggered when the element has the keyboard focus and a key is released.
        /// </summary>
        public event KeyEventDelegate KeyUp;

        /// <summary>
        /// Event triggered when the keyboard loses the focus on that element.
        /// </summary>
        public event KeyboardFocusChangedEventDelegate LostKeyboardFocus;

        /// <summary>
        /// Event triggered when the element has the mouse in its region no more.
        /// </summary>
        public event MouseEventDelegate LostMouseCapture;

        /// <summary>
        /// Event triggered when the mouse just entered the limit of that element.
        /// </summary>
        public event MouseEventDelegate MouseEnter;

        /// <summary>
        /// Event triggered when the mouse just exited the limit of that element.
        /// </summary>
        public event MouseEventDelegate MouseLeave;

        /// <summary>
        /// Event triggered when the mouse left button is pressed on that element.
        /// </summary>
        public event MouseButtonEventDelegate MouseLeftButtonDown;

        /// <summary>
        /// Event triggered when the mouse left button is released on that element.
        /// </summary>
        public event MouseButtonEventDelegate MouseLeftButtonUp;

        /// <summary>
        /// Event triggered when the mouse is moving on that element.
        /// </summary>
        public event MouseEventDelegate MouseMove;

        /// <summary>
        /// Event triggered when the mouse right button is pressed on that element.
        /// </summary>
        public event MouseButtonEventDelegate MouseRightButtonDown;

        /// <summary>
        /// Event triggered when the mouse right button is released on that element.
        /// </summary>
        public event MouseButtonEventDelegate MouseRightButtonUp;

        /// <summary>
        /// Event triggered when the mouse wheel is rolling on that element.
        /// </summary>
        public event MouseWheelEventDelegate MouseWheel;

        /// <summary>
        /// Event triggered just before the keyboard has the focus on that element.
        /// </summary>
        public event KeyboardFocusChangedEventDelegate PreviewGotKeyboardFocus;

        /// <summary>
        /// Event triggered just before the element has the keyboard focus and a key is pressed.
        /// </summary>
        public event KeyEventDelegate PreviewKeyDown;

        /// <summary>
        /// Event triggered just before the element has the keyboard focus and a key is released.
        /// </summary>
        public event KeyEventDelegate PreviewKeyUp;

        /// <summary>
        /// Event triggered just before the keyboard loses the focus on that element.
        /// </summary>
        public event KeyboardFocusChangedEventDelegate PreviewLostKeyboardFocus;

        /// <summary>
        /// Event triggered just before the mouse left button is pressed on that element.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseLeftButtonDown;

        /// <summary>
        /// Event triggered just before the mouse left button is released on that element.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseLeftButtonUp;

        /// <summary>
        /// Event triggered just before the mouse is moving on that element.
        /// </summary>
        public event MouseEventDelegate PreviewMouseMove;

        /// <summary>
        /// Event triggered just before the mouse right button is pressed on that element.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseRightButtonDown;

        /// <summary>
        /// Event triggered just before the mouse right button is released on that element.
        /// </summary>
        public event MouseButtonEventDelegate PreviewMouseRightButtonUp;

        /// <summary>
        /// Event triggered just before the mouse wheel rolls on that element.
        /// </summary>
        public event MouseWheelEventDelegate PreviewMouseWheel;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the event delegates cache if any.
        /// </summary>
        internal EventDelegatesCache EventDelegatesCache
        {
            get
            {
                if
                    ( this.mHasEventDelegatesCache == false )
                {
                    return null;
                }

                return this.mEventDelegatesCache;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the element can have the focus or not.
        /// </summary>
        public bool Focusable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the flag indicating whether the element has been activated or not.
        /// </summary>
        public bool IsEnabled
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the element has the keyboard focus or not.
        /// </summary>
        public bool IsKeyboardFocused
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the keyboard focus is whithin the element or not.
        /// </summary>
        public bool IsKeyboardFocusWithin
        {
            get;
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse is entered in the element bounds or not.
        /// </summary>
        public bool IsMouseCaptured
        {
            get
            {
                return this.mIsMouseCaptured;
            }
            internal set
            {
                bool lOldValue = this.mIsMouseCaptured;
                if
                    ( this.NotifyPropertyChanged( ref this.mIsMouseCaptured, value, "IsMouseCaptured" ) )
                {
                    
                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse has that element as Top most regarding
        /// to those in the region the mouse is in.
        /// </summary>
        public bool IsMouseDirectlyOver
        {
            get
            {
                return this.mIsMouseDirectlyOver;
            }
            internal set
            {
                if
                    ( this.NotifyPropertyChanged( ref this.mIsMouseDirectlyOver, value, "IsMouseDirectlyOver" ) )
                {

                }
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse is on that element whatever it is a child
        /// or the top most of those in the region the mouse is in.
        /// </summary>
        public bool IsMouseOver
        {
            get
            {
                return this.mIsMouseOver;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the mouse captured within that element or not.
        /// </summary>
        internal bool IsMouseCaptureWithin
        {
            get
            {
                return this.mIsMouseCaptureWithin;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentElement"/> class.
        /// </summary>
        public ContentElement()
        {
            this.mIsMouseCaptureWithin = false;
            this.mIsMouseOver = false;
            this.Parent = null;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a  pre-opportunity handler for templated parent of this element in case parent is listening
        /// for synchronized input.
        /// </summary>
        /// <param name="pRoute">The event route.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void AddSynchronizedInputPreOpportunityHandler(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            
        }

        /// <summary>
        /// Adds a handler to post process the synchronized input if this UI element is currently listening to synchronized input(s),
        /// otherwise it adds a synchronized input pre-opportunity handler from parent if parent is listening.
        /// </summary>
        /// <param name="pRoute">The event route.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void AddSynchronizedInputPostOpportunityHandler(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Notifies the routed event listeners.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        /// <param name="pIsTrusted">The flag indicating whether the event must be raised safely or not.</param>
        internal void RaiseEvent(RoutedEventArgs pEventArgs, bool pIsTrusted)
        {

        }

        /// <summary>
        /// Notifies an automatic event.
        /// </summary>
        /// <param name="pEvent">The event type.</param>
        internal void RaiseAutomaticEvent(AutomaticEvents pEvent)
        {
            if
                ( this.AutomaticEvent != null )
            {
                this.AutomaticEvent( this, new AutomaticEventArgs( pEvent ) );
            }
        }

        /// <summary>
        /// Customizes the UIElement route
        /// </summary>
        /// <param name="pRoute">The route to modify.</param>
        /// <param name="pEventArgs">The route event arguments.</param>
        /// <returns>Whether or not the route should continue past the visual tree. If this is true, and there are no more visual parents, the route
        /// building code will call the GetUIParentCore method to find the next non-visual parent.</returns>
        internal virtual bool CustomBuildRoute(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {
            return false;
        }

        /// <summary>
        /// Adds the given event handlers for this UI element to the route.
        /// </summary>
        /// <param name="pRoute">The route delegate(s) must be added to.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        public void AddToEventRoute(EventRoute pRoute, RoutedEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Adjusts the event source
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        /// <returns>The new source.</returns>
        internal virtual object AdjustEventSource(RoutedEventArgs pEventArgs)
        {
            return null;
        }

        /// <summary>
        /// Gets the Content element parent.
        /// </summary>
        /// <param name="pKeepGoingPastVisualTree">The flag indicating whether the parent search must keep going through the logical tree if nothing found in the visual one.</param>
        /// <returns>The Element parent, null otherwise.</returns>
        internal DependencyObject GetUIParent(bool pKeepGoingPastVisualTree)
        {
            DependencyObject lParent = null;

            // Try to find a UIElement parent in the visual ancestry.
            lParent = InputElement.GetContainingInputElement( this.Parent ) as DependencyObject;

            // If there was no InputElement parent in the visual ancestry,
            // check along the logical branch.
            if 
                ( lParent == null && 
                  pKeepGoingPastVisualTree )
            {
                DependencyObject lLogicalParent = this.CustomGetUIParent();
                lParent = InputElement.GetContainingInputElement( lLogicalParent ) as DependencyObject;
            }

            return lParent;
        }

        /// <summary>
        /// Gets the UI parent.
        /// </summary>
        /// <returns>The parent.</returns>
        protected virtual internal DependencyObject CustomGetUIParent()
        {
            return null;
        }

        /// <summary>
        /// Adds a new delegate to run for the given routed event.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event delegate to run must be added to.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public void AddHandler(RoutedEvent pRoutedEvent, Delegate pDelegate)
        {

        }

        /// <summary>
        /// Removes all instances of the specified routed event delegate for this UI element.
        /// </summary>
        /// <param name="pRoutedEvent">The routed event delegate comes from.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public void RemoveHandler(RoutedEvent pRoutedEvent, Delegate pDelegate)
        {

        }

        /// <summary>
        /// Delegate called on IsMouseCaptureWithin changes.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected virtual void OnIsMouseCaptureWithinChanged(PropertyChangedEventArgs pEventArgs)
        {
            // To override...
        }

        /// <summary>
        /// Internal raise of the IsMouseCaptureWithin event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        internal void RaiseIsMouseCaptureWithinChanged(PropertyChangedEventArgs pEventArgs)
        {
            // Call the virtual method first.
            this.OnIsMouseCaptureWithinChanged( pEventArgs );

            // Raise the public event second.
            this.InternalNotifyPropertyChanged( UIElement.IsMouseCaptureWithinChangedKey, pEventArgs );
        }

        /// <summary>
        /// Notifies a mouse over change.
        /// </summary>
        /// <param name="pOldElement">The old element.</param>
        /// <param name="pNewElement">The new element.</param>
        internal static void NotifyMouseOverValueChanged(ContentElement pOldElement, ContentElement pNewElement)
        {
            if
                ( pOldElement.NotifyPropertyChanged( ref pOldElement.mIsMouseOver, false, "IsMouseOver" ) )
            {
                InputElement.FireMouseOverPropertyChangedInAncestry( pOldElement, true );
            }
            
            if
                ( pNewElement.NotifyPropertyChanged( ref pNewElement.mIsMouseOver, true, "IsMouseOver" ) )
            {
                InputElement.FireMouseOverPropertyChangedInAncestry( pNewElement, false );
            }
        }

        /// <summary>
        /// Notifies a mouse capture within element.
        /// </summary>
        /// <param name="pOldElement">The old element.</param>
        /// <param name="pNewElement">The new element.</param>
        internal static void NotifyMouseCaptureWithinValueChanged(ContentElement pOldElement, ContentElement pNewElement)
        {
            if
                ( pOldElement.NotifyPropertyChanged( ref pOldElement.mIsMouseCaptureWithin, false, "IsMouseCaptureWithin" ) )
            {
                InputElement.FireMouseCaptureWithinPropertyChangedInAncestry( pOldElement, true );
            }
            
            if
                ( pNewElement.NotifyPropertyChanged( ref pNewElement.mIsMouseCaptureWithin, true, "IsMouseCaptureWithin" ) )
            {
                InputElement.FireMouseCaptureWithinPropertyChangedInAncestry( pNewElement, false );
            }
        }

        /// <summary>
        /// Delegate called for on preview mouse down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewMouseDown(MouseButtonEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called for on mouse down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseDown(MouseButtonEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called for on preview mouse up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewMouseUp(MouseButtonEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called for on mouse up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseUp(MouseButtonEventArgs pEventArgs)
        {

        }

        /// <summary>
        /// Delegate called for on preview mouse left button down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on mouse left button down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseLeftButtonDown(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on preview mouse left button up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on mouse left button up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseLeftButtonUp(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do..
        }

        /// <summary>
        /// Delegate called for on preview mouse right button down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewMouseRightButtonDown(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on mouse right button down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseRightButtonDown(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on preview mouse right button up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewMouseRightButtonUp(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on mouse right button up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseRightButtonUp(MouseButtonEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on preview mouse move event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewMouseMove(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on mouse move event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseMove(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on preview mouse wheel event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewMouseWheel(MouseWheelEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on mouse wheel event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseWheel(MouseWheelEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on mouse enter event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseEnter(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on mouse leave event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnMouseLeave(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on got mouse capture event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnGotMouseCapture(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on lost mouse capture event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnLostMouseCapture(MouseEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on preview key down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewKeyDown(KeyEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on key down event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnKeyDown(KeyEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on preview key up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewKeyUp(KeyEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on key up event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnKeyUp(KeyEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on preview Got keyboard focus event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewGotKeyboardFocus(KeyboardFocusChangedEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on Got keyboard focus event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on preview Lost keyboard focus event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnPreviewLostKeyboardFocus(KeyboardFocusChangedEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Delegate called for on Lost keyboard focus event.
        /// </summary>
        /// <param name="pEventArgs">The event arguments.</param>
        protected internal virtual void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs pEventArgs)
        {
            // Nothing to do...
        }

        /// <summary>
        /// Ensures the event delegates cache exists.
        /// </summary>
        internal void EnsureEventDelegatesCache()
        {
            if
                ( this.mEventDelegatesCache == null )
            {
                this.mEventDelegatesCache = new EventDelegatesCache();
                this.mHasEventDelegatesCache = true;
            }
        }

        /// <summary>
        /// Notifies internal cache listeners about a property changed.
        /// </summary>
        /// <param name="pKey">The event key.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void InternalNotifyPropertyChanged(int pKey, PropertyChangedEventArgs pEventArgs)
        {
            EventDelegatesCache lCache = this.EventDelegatesCache;
            if 
                ( lCache != null )
            {
                Delegate lDelegate = lCache.Get( pKey );
                if
                    ( lDelegate != null )
                {
                    ((PropertyChangedEventDelegate)lDelegate)( this, pEventArgs );
                }
            }
        }

        #endregion Methods
    }
}
