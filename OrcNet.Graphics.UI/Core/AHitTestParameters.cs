﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// This is the base class for packing together parameters for a hit test pass.
    /// </summary>
    public abstract class AHitTestParameters
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AHitTestParameters"/> class.
        /// </summary>
        internal AHitTestParameters()
        {

        }

        #endregion Constructors
    }
}
