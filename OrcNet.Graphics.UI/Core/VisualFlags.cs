﻿namespace OrcNet.Graphics.UI.Core
{
    /// <summary>
    /// Visual flags for Bounds and hierarchy states enumeration definition.
    /// </summary>
    internal enum VisualFlags : uint
    {
        /// <summary>
        /// No flags are set for this visual.
        /// </summary>
        None                                = 0x00000000,

        /// <summary>
        /// IsSubtreeDirtyForPrecompute indicates that at least one Visual in the sub-graph of this Visual needs
        /// a bounding box update.
        /// </summary>
        IsSubtreeDirtyForPrecompute         = 0x00000001,

        /// <summary>
        /// IsSubtreeDirtyForRender indicates that at least one Visual 
        /// in the sub-graph of this Visual needs to be re-rendered.
        /// </summary>
        IsSubtreeDirtyForRender             = 0x00000002,

        /// <summary>
        /// Should post render indicates that this is a root visual and therefore we need to indicate that this
        /// visual tree needs to be re-rendered. Today we are doing this by posting a render queue item.
        /// </summary>
        ShouldPostRender                    = 0x00000004,

        /// <summary>
        /// This flag must be set to true when the transform property 
        /// of a Visual is set. It ensures that the new transform is 
        /// attached to the visual resource.
        /// </summary>
        IsTransformDirty                    = 0x00000008,

        /// <summary>
        /// This flag must be set to true when the clip property of a Visual is set. It ensures that the
        /// new clip is attached to the visual resource.
        /// </summary>
        IsClipDirty                         = 0x00000010,

        /// <summary>
        /// This flag indicates that the content of the visual resource needs to
        /// be updated. This is done by calling the virtual method RenderContent.
        /// When this flag is set usually the IsSubtreeDirtyForRender is propagated.
        /// </summary>
        IsContentDirty                      = 0x00000020,

        /// <summary>
        /// This flag indicates that the opacity needs to be updated on the visual
        /// resource. When this flag is set, IsSubtreeDirtyForRender is propagated.
        /// </summary>
        IsOpacityDirty                      = 0x00000040,

        /// <summary>
        /// Indicates that the offset has changed and we need to update the visual resource.
        /// </summary>
        IsOffsetDirty                       = 0x00000080,

        /// <summary>
        /// Flag indicating that the visual is a UI element.
        /// </summary>
        IsUIElement                         = 0x00000100,

        /// <summary>
        /// For UIElement -- It's in VisualFlags so that it can be propagated through the
        /// Visual subtree without casting.
        /// </summary>
        IsLayoutSuspended                   = 0x00000200,

        /// <summary>
        /// Are we in the process of iterating the visual children. 
        /// This flag is set during a descendents walk, for property invalidation.
        /// </summary>
        IsVisualChildrenIterationInProgress = 0x00000400,
        
        /// <summary>
        /// FindCommonAncestor is used to find the common ancestor of a Visual.
        /// </summary>
        FindCommonAncestor                  = 0x00000800,

        /// <summary>
        /// IsLayoutIslandRoot indicates that this Visual is a root of Element Layout Island
        /// </summary>
        IsLayoutIslandRoot                  = 0x00001000,

        /// <summary>
        /// UseLayoutRounding indicates that layout rounding should be applied during Measure/Arrange for this UIElement.
        /// </summary>
        UseLayoutRounding                   = 0x00002000,

        /// <summary>
        /// UI element is visible.
        /// </summary>
        VisibilityCache_Visible             = 0x00004000,

        /// <summary>
        /// UI element is hidden but keep taking space in memory.
        /// If Neither visible, nor hidden, it is collapsed.
        /// </summary>
        VisibilityCache_TakesSpace          = 0x00008000,

        /// <summary>
        /// Indicates that a given node is registered for AncestorChanged.
        /// </summary>
        RegisteredForAncestorChanged        = 0x00010000,

        /// <summary>
        /// Indicates that a node below this node is registered for AncestorChanged.
        /// </summary>
        SubTreeHoldsAncestorChanged         = 0x00020000,
        
        /// <summary>
        /// Indicates that the user entered again in an already in Visual.
        /// </summary>
        ReentrancyFlag                      = 0x00040000,

        /// <summary>
        /// Indicates if the visual has any children. Avoids calls to visualchildrencount while checking for presence of children.
        /// </summary>
        HasChildren                         = 0x00080000,

        /// <summary>
        /// This flag indicates that the visual's children have been reordered and we need to
        /// update the visual resource.
        /// </summary>
        IsChildrenZOrderDirty               = 0x00100000,

        /// <summary>
        /// Indicates when the ScrollableAreaClip needs to be updated on the visual
        /// resource. When this flag is set, IsSubtreeDirtyForRender is propagated.
        /// </summary>
        IsScrollableAreaClipDirty           = 0x00200000,
    }
}
