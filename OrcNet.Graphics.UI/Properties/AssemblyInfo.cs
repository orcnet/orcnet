﻿using OrcNet.Constants;
using System.Reflection;
using System.Runtime.InteropServices;

// Les informations générales relatives à un assembly dépendent de 
// l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
// associées à un assembly.
[assembly: AssemblyTitle("OrcNet.Graphics.UI")]
[assembly: AssemblyDescription("The User Interface core module.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(Constants.COMPANY)]
[assembly: AssemblyProduct("OrcNet.Graphics.UI")]
[assembly: AssemblyCopyright(Constants.COPYRIGHT)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// L'affectation de la valeur false à ComVisible rend les types invisibles dans cet assembly 
// aux composants COM.  Si vous devez accéder à un type dans cet assembly à partir de 
// COM, affectez la valeur true à l'attribut ComVisible sur ce type.
[assembly: ComVisible(false)]

// Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
[assembly: Guid("8ea69e7c-a456-4a6e-9d64-a9c2c23d2da4")]

// Les informations de version pour un assembly se composent des quatre valeurs suivantes :
//
//      Version principale
//      Version secondaire 
//      Numéro de build
//      Révision
//
// Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
// en utilisant '*', comme indiqué ci-dessous :
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(Constants.MAJOR + Constants.MINOR + Constants.BUILD + Constants.REVISION)]
[assembly: AssemblyFileVersion(Constants.MAJOR + Constants.MINOR + Constants.BUILD + Constants.REVISION)]
