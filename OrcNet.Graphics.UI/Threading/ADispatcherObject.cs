﻿using OrcNet.Core;
using OrcNet.Core.Service;
using OrcNet.Graphics.UI.Services;
using System.Threading;

namespace OrcNet.Graphics.UI.Threading
{
    /// <summary>
    /// Base Dispatcher object abstract class definition
    /// which expose the Dispatcher the object is associated with.
    /// </summary>
    public abstract class ADispatcherObject : AMemoryProfilable
    {
        #region Properties

        /// <summary>
        /// Gets the UI dispatcher.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
                if
                    ( lUIService != null )
                {
                    return lUIService.Dispatcher;
                }

                return null;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ADispatcherObject"/> class.
        /// </summary>
        protected ADispatcherObject()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether the current thread have access to that object.
        /// </summary>
        /// <returns>True if the calling thread is the object's owner, false otherwise.</returns>
        public bool CheckAccess()
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            if
                ( lUIService != null )
            {
                return Thread.CurrentThread.ManagedThreadId == lUIService.Dispatcher.ThreadId;
            }

            return false;
        }

        #endregion Methods
    }
}
