﻿using OpenTK.Input;
using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using OrcNet.Graphics.UI.Core;
using OrcNet.Graphics.UI.Helpers;
using OrcNet.Graphics.UI.Maths;
using OrcNet.Graphics.UI.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace OrcNet.Graphics.UI.Managers
{
    /// <summary>
    /// Input report event delegate prototype definition.
    /// </summary>
    /// <param name="pSender">The sender.</param>
    /// <param name="pEventArgs">The event arguments.</param>
    internal delegate void InputReportEventDelegate(object pSender, InputReportEventArgs pEventArgs);

    /// <summary>
    /// Input manager singleton class definition
    /// Thread-safe
    /// </summary>
    public sealed class InputManager
    {
        #region Fields

        /// <summary>
        /// Stores the routed event indicating that a preview input report arrived.
        /// </summary>
        internal static readonly RoutedEvent PreviewInputReportEvent = RoutedEvent.RegisterRoutedEvent( "PreviewInputReport", 
                                                                                                        typeof(InputReportEventDelegate), 
                                                                                                        typeof(InputManager) );

        /// <summary>
        /// Stores the routed event indicating that an input report arrived.
        /// </summary>
        internal static readonly RoutedEvent InputReportEvent = RoutedEvent.RegisterRoutedEvent( "InputReport", 
                                                                                                 typeof(InputReportEventDelegate), 
                                                                                                 typeof(InputManager) );

        /// <summary>
        /// Stores the preview Key up routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewKeyUpEvent = RoutedEvent.RegisterRoutedEvent( "PreviewKeyUp", 
                                                                                                typeof(KeyEventDelegate), 
                                                                                                typeof(InputManager) );

        /// <summary>
        /// Stores the Key up routed event.
        /// </summary>
        public static readonly RoutedEvent KeyUpEvent     = RoutedEvent.RegisterRoutedEvent( "KeyUp", 
                                                                                             typeof(KeyEventDelegate), 
                                                                                             typeof(InputManager) );

        /// <summary>
        /// Stores the preview Key down routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewKeyDownEvent = RoutedEvent.RegisterRoutedEvent( "PreviewKeyDown", 
                                                                                                  typeof(KeyEventDelegate), 
                                                                                                  typeof(InputManager) );

        /// <summary>
        /// Stores the Key down routed event.
        /// </summary>
        public static readonly RoutedEvent KeyDownEvent   = RoutedEvent.RegisterRoutedEvent( "KeyDown", 
                                                                                             typeof(KeyEventDelegate), 
                                                                                             typeof(InputManager) );

        /// <summary>
        /// Stores the preview mouse move routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseMoveEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseMove", 
                                                                                                    typeof(MouseEventDelegate), 
                                                                                                    typeof(InputManager) );

        /// <summary>
        /// Stores the mouse move routed event.
        /// </summary>
        public static readonly RoutedEvent MouseMoveEvent = RoutedEvent.RegisterRoutedEvent( "MouseMove", 
                                                                                             typeof(MouseEventDelegate), 
                                                                                             typeof(InputManager) );

        /// <summary>
        /// Stores the preview mouse down outside captured element routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseDownOutsideCapturedElementEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseDownOutsideCapturedElement", 
                                                                                                                          typeof(MouseButtonEventDelegate), 
                                                                                                                          typeof(InputManager) );

        /// <summary>
        /// Stores the preview mouse up outside captured element routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseUpOutsideCapturedElementEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseUpOutsideCapturedElement", 
                                                                                                                        typeof(MouseButtonEventDelegate), 
                                                                                                                        typeof(InputManager) );

        /// <summary>
        /// Stores the preview Mouse down routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseDownEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseDown", 
                                                                                                    typeof(MouseButtonEventDelegate), 
                                                                                                    typeof(InputManager) );

        /// <summary>
        /// Stores the Mouse down routed event.
        /// </summary>
        public static readonly RoutedEvent MouseDownEvent = RoutedEvent.RegisterRoutedEvent( "MouseDown", 
                                                                                             typeof(MouseButtonEventDelegate), 
                                                                                             typeof(InputManager) );

        /// <summary>
        /// Stores the preview Mouse up routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseUpEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseUp", 
                                                                                                  typeof(MouseButtonEventDelegate), 
                                                                                                  typeof(InputManager) );

        /// <summary>
        /// Stores the Mouse up routed event.
        /// </summary>
        public static readonly RoutedEvent MouseUpEvent   = RoutedEvent.RegisterRoutedEvent( "MouseUp", 
                                                                                             typeof(MouseButtonEventDelegate), 
                                                                                             typeof(InputManager) );

        /// <summary>
        /// Stores the preview Mouse wheel routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewMouseWheelEvent = RoutedEvent.RegisterRoutedEvent( "PreviewMouseWheel", 
                                                                                                     typeof(MouseWheelEventDelegate), 
                                                                                                     typeof(InputManager) );

        /// <summary>
        /// Stores the Mouse wheel routed event.
        /// </summary>
        public static readonly RoutedEvent MouseWheelEvent = RoutedEvent.RegisterRoutedEvent( "MouseWheel", 
                                                                                              typeof(MouseWheelEventDelegate), 
                                                                                              typeof(InputManager) );

        /// <summary>
        /// Stores the Mouse enter routed event.
        /// </summary>
        public static readonly RoutedEvent MouseEnterEvent = RoutedEvent.RegisterRoutedEvent( "MouseEnter", 
                                                                                              typeof(MouseEventDelegate), 
                                                                                              typeof(InputManager) );

        /// <summary>
        /// Stores the Mouse leave routed event.
        /// </summary>
        public static readonly RoutedEvent MouseLeaveEvent = RoutedEvent.RegisterRoutedEvent( "MouseLeave", 
                                                                                              typeof(MouseEventDelegate), 
                                                                                              typeof(InputManager) );

        /// <summary>
        /// Stores the Lost mouse capture routed event.
        /// </summary>
        public static readonly RoutedEvent LostMouseCaptureEvent = RoutedEvent.RegisterRoutedEvent( "LostMouseCapture", 
                                                                                                    typeof(MouseEventDelegate), 
                                                                                                    typeof(InputManager) );

        /// <summary>
        /// Stores the Got mouse capture routed event.
        /// </summary>
        public static readonly RoutedEvent GotMouseCaptureEvent = RoutedEvent.RegisterRoutedEvent( "GotMouseCapture", 
                                                                                                   typeof(MouseEventDelegate), 
                                                                                                   typeof(InputManager) );

        /// <summary>
        /// Stores the preview Got keyboard focus routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewGotKeyboardFocusEvent = RoutedEvent.RegisterRoutedEvent( "PreviewGotKeyboardFocus", 
                                                                                                           typeof(KeyboardFocusChangedEventDelegate), 
                                                                                                           typeof(InputManager) );

        /// <summary>
        /// Stores the Got keyboard focus routed event.
        /// </summary>
        public static readonly RoutedEvent GotKeyboardFocusEvent = RoutedEvent.RegisterRoutedEvent( "GotKeyboardFocus", 
                                                                                                    typeof(KeyboardFocusChangedEventDelegate), 
                                                                                                    typeof(InputManager) );

        /// <summary>
        /// Stores the lost keyboard focus routed event.
        /// </summary>
        public static readonly RoutedEvent LostKeyboardFocusEvent = RoutedEvent.RegisterRoutedEvent( "LostKeyboardFocus", 
                                                                                                     typeof(KeyboardFocusChangedEventDelegate), 
                                                                                                     typeof(InputManager) );

        /// <summary>
        /// Stores the preview lost keyboard focus routed event.
        /// </summary>
        public static readonly RoutedEvent PreviewLostKeyboardFocusEvent = RoutedEvent.RegisterRoutedEvent( "PreviewLostKeyboardFocus", 
                                                                                                            typeof(KeyboardFocusChangedEventDelegate), 
                                                                                                            typeof(InputManager) );

        /// <summary>
        /// Stores the input event(s) the element is listening on.
        /// </summary>
        private RoutedEvent[] mSynchronizedInputEvents;

        /// <summary>
        /// Stores the complementary pair of input event(s) the element is listening on.
        /// </summary>
        private RoutedEvent[] mSynchronizedOppositeInputEvents;

        ///<summary>
        /// Stores the synchronized input type, set by the client. 
        ///</summary> 
        private SynchronizedInputType mSynchronizedInputType;

        ///<summary>
        /// Stores the input state of the event during the synchronized processing.
        ///</summary> 
        private SynchronizedInputStates mSynchronizedInputState;

        /// <summary>
        /// Stores the element listening for synchronized input.
        /// </summary>
        private DependencyObject      mListeningElement;

        /// <summary>
        /// Stores the flag indicating whether any element is currently listening for synchronized input or not.
        /// </summary>
        private bool mIsSynchronizedInput;

        /// <summary>
        /// Stores the flag indicating whether the mouse is physically over an element or not.
        /// </summary>
        private bool mIsMousePhysicallyOver;

        /// <summary>
        /// Stores the last known client position.
        /// </summary>
        private Point mLastPosition = new Point();

        /// <summary>
        /// Stores tha flag indicating whether the last position must be updated or not.
        /// </summary>
        private bool mMustUpdateLastPosition = false;

        /// <summary>
        /// Stores the mouse position relative to the element it is over.
        /// </summary>
        private Point mPositionRelativeToOver = new Point();

        /// <summary>
        /// Stores the non redundant actions contextual input event data key.
        /// </summary>
        private object mNonRedundantActionsTag = new object();

        /// <summary>
        /// Stores the root point contextual input event data key.
        /// </summary>
        private object mRootPointTag = new object();

        /// <summary>
        /// Stores the last position of a mouse click.
        /// </summary>
        private Point mLastClick = new Point();

        /// <summary>
        /// Stores the last mouse button clicked.
        /// </summary>
        private MouseButton mLastButton;

        /// <summary>
        /// Stores the same button click count.
        /// </summary>
        private int mClickCount;

        /// <summary>
        /// Stores the time elapsed since the last click.
        /// </summary>
        private int mLastClickTime;

        /// <summary>
        /// Stores the click delta time before a new click can be determined as a new one.
        /// </summary>
        private int mDoubleClickDeltaTime;

        /// <summary>
        /// Stores the click threshold on X before a new click can be determined as at another position.
        /// </summary>
        private int mDoubleClickDeltaX;

        /// <summary>
        /// Stores the click threshold on Y before a new click can be determined as at another position.
        /// </summary>
        private int mDoubleClickDeltaY;

        /// <summary>
        /// Stores the current element under the mouse.
        /// </summary>
        private IInputElement mMouseOver;

        /// <summary>
        /// Stores the current weak reference of the element the mouse is over whatever it is enabled or not.
        /// </summary>
        private WeakReference mRawMouseOver;

        /// <summary>
        /// Stores the current element captured by the mouse if any.
        /// </summary>
        private IInputElement mMouseCapture;
        
        /// <summary>
        /// Stores the current capture mode.
        /// </summary>
        private CaptureMode   mCaptureMode;

        /// <summary>
        /// Stores the notify input event arguments instance that will be recycle each time.
        /// </summary>
        private NotifyInputEventArgs     mNotifyInputEventArgs;

        /// <summary>
        /// Stores the process input event arguments instance that will be recycle each time.
        /// </summary>
        private ProcessInputEventArgs    mProcessInputEventArgs;

        /// <summary>
        /// Stores the pre-process input event arguments instance that will be recycle each time.
        /// </summary>
        private PreProcessInputEventArgs mPreProcessInputEventArgs;
        
        /// <summary>
        /// Stores the set of input to notify to the UI.
        /// </summary>
        private Stack<InputEventArgs> mInputs;

        /// <summary>
        /// Stores the flag indicating whether the inputs must be still processed by the manager or not.
        /// </summary>
        private bool mKeepProcessingInputs;

        /// <summary>
        /// Stores the input manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile InputManager sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the lock used to serialize access to synchronized input related fields.
        /// </summary>
        private static object sSynchronizedInputLock = new object();

        #endregion Fields

        #region Events

        /// <summary>
        /// Event triggered before to process an input.
        /// </summary>
        public event PreProcessInputEventDelegate PreProcessInput;

        /// <summary>
        /// Event triggered before an input be taken into account.
        /// </summary>
        public event NotifyInputEventDelegate PreNotifyInput;

        /// <summary>
        /// Event triggered after an input be taken into account.
        /// </summary>
        public event NotifyInputEventDelegate PostNotifyInput;

        /// <summary>
        /// Event triggered once an input is processed.
        /// </summary>
        public event ProcessInputEventDelegate PostProcessInput;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether any element is currently listening for synchronized input or not.
        /// </summary>
        internal bool IsSynchronizedInput
        {
            get
            {
                return this.mIsSynchronizedInput;
            }
        }

        /// <summary>
        /// Gets the input event(s) the element is listening on.
        /// </summary>
        internal RoutedEvent[] SynchronizedInputEvents
        {
            get
            {
                return this.mSynchronizedInputEvents;
            }
        }

        /// <summary>
        /// Gets the complementary pair of input event(s) the element is listening on.
        /// </summary>
        internal RoutedEvent[] SynchronizedOppositeInputEvents
        {
            get
            {
                return this.mSynchronizedOppositeInputEvents;
            }
        }

        ///<summary>
        /// Gets the synchronized input type, set by the client. 
        ///</summary> 
        internal SynchronizedInputType SynchronizedInputType
        {
            get
            {
                return this.mSynchronizedInputType;
            }
        }

        ///<summary>
        /// Gets the element on which StartListening was called.
        ///</summary> 
        internal DependencyObject ListeningElement
        {
            get
            {
                return this.mListeningElement;
            }
        }

        ///<summary>
        /// Gets or sets the input state of the event during the synchronized processing.
        ///</summary> 
        internal SynchronizedInputStates SynchronizedInputState
        {
            get
            {
                return this.mSynchronizedInputState;
            }
            set
            {
                this.mSynchronizedInputState = value;
            }
        }

        /// <summary>
        /// Gets the mouse device state.
        /// </summary>
        public MouseState Mouse
        {
            get
            {
                return OpenTK.Input.Mouse.GetCursorState();
            }
        }

        /// <summary>
        /// Gets the keyboard device state.
        /// </summary>
        public KeyboardState Keyboard
        {
            get
            {
                return OpenTK.Input.Keyboard.GetState();
            }
        }

        /// <summary>
        /// Gets the current mouse target, that is, the element being under the mouse cursor.
        /// </summary>
        public IInputElement MouseTarget
        {
            get
            {
                return this.mMouseOver;
            }
        }

        /// <summary>
        /// Gets the element the mouse is considered directly over if it has been captured.
        /// </summary>
        public IInputElement DirectlyOver
        {
            get
            {
                return this.mMouseOver;
            }
        }

        /// <summary>
        /// Gets the element the mouse is over whatever the element is enabled or not.
        /// </summary>
        internal IInputElement RawDirectlyOver
        {
            get
            {
                if 
                    ( this.mRawMouseOver != null )
                {
                    IInputElement lRawMouseOver = this.mRawMouseOver.Target as IInputElement;
                    if
                        ( lRawMouseOver != null )
                    {
                        return lRawMouseOver;
                    }
                }

                return this.DirectlyOver;
            }
        }

        /// <summary>
        /// Gets the element that has captured the mouse.
        /// </summary>
        public IInputElement Captured
        {
            get
            {
                return this.mMouseCapture;
            }
        }

        /// <summary>
        /// Gets the capture mode.
        /// </summary>
        internal CaptureMode CapturedMode
        {
            get
            {
                return this.mCaptureMode;
            }
        }

        /// <summary>
        /// Gets the input manager handle.
        /// </summary>
        public static InputManager Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if
                    ( sInstance == null )
                {
                    // Lock on
                    lock
                        ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if
                            ( sInstance == null )
                        {
                            sInstance = new InputManager();
                        }
                    }
                }

                return sInstance;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="InputManager"/> class.
        /// </summary>
        static InputManager()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InputManager"/> class.
        /// </summary>
        private InputManager()
        {
            this.mInputs = new Stack<InputEventArgs>();
            this.mDoubleClickDeltaX = 1;
            this.mDoubleClickDeltaY = 1;
            this.mDoubleClickDeltaTime = 1000; // 1 Secs

            // Listen layout changes.
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            lUIService.LayoutUpdated += this.OnLayoutUpdated;

            // Internal delegates.
            this.PreProcessInput  += this.OnPreProcessInput;
            this.PreNotifyInput   += this.OnPreNotifyInput;
            this.PostProcessInput += this.OnPostProcessInput;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Computes the mouse position relative to the given element.
        /// </summary>
        /// <param name="pRelativeTo">The relative UI element.</param>
        /// <returns>The relative position.</returns>
        public Point GetPosition(IInputElement pRelativeTo)
        {
            if 
                ( pRelativeTo != null && 
                  InputElement.IsValid( pRelativeTo ) == false )
            {
                throw new InvalidOperationException( string.Format( "Invalid element {0}!!!", pRelativeTo.GetType() ) );
            }

            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            PresentationSource lSource = lUIService.PresentationSource;
            if
                ( lSource == null )
            {
                return new Point( 0, 0 );
            }

            bool lSuccess;
            Point lClient = this.GetClientPosition( lSource );
            Point lRoot   = PointUtil.TryClientToRoot( lClient, lSource, false, out lSuccess );
            if
                ( lSuccess == false )
            {
                return new Point( 0, 0 );
            }

            Point lRelativePoint = InputElement.TranslatePoint( lRoot, lSource.RootVisual, pRelativeTo as DependencyObject );
            return lRelativePoint;
        }

        /// <summary>
        /// Captures the mouse to a particular element.
        /// </summary>
        public bool Capture(IInputElement pElement)
        {
            return this.Capture( pElement, CaptureMode.Element );
        }

        /// <summary>
        /// Captures the mouse to a particular element given a capture mode for allowing
        /// input event to be recieved for the window the element is in.
        /// </summary>
        /// <param name="pElement">THe element the source must be captured.</param>
        /// <param name="pCaptureMode">The capture mode.</param>
        public bool Capture(IInputElement pElement, CaptureMode pCaptureMode)
        {
            int lTimeStamp = Environment.TickCount;

            if
                ( (pCaptureMode == CaptureMode.None || pCaptureMode == CaptureMode.Element || pCaptureMode == CaptureMode.SubTree) == false )
            {
                throw new InvalidEnumArgumentException("pCaptureMode", (int)pCaptureMode, typeof(CaptureMode));
            }

            if 
                ( pElement == null )
            {
                pCaptureMode = CaptureMode.None;
            }

            if 
                ( pCaptureMode == CaptureMode.None )
            {
                pElement = null;
            }

            // Validate that elt is either a UIElement or a ContentElement
            DependencyObject lElement = pElement as DependencyObject;
            if 
                ( lElement != null && 
                  InputElement.IsValid( pElement ) == false )
            {
                throw new InvalidOperationException( string.Format( "Invalid input element {0}" , lElement.GetType() ) );
            }

            bool lSuccess = false;

            // The pElement we are capturing to must be both enabled and visible.
            if 
                ( pElement is UIElement )
            {
                UIElement lCast = pElement as UIElement;

#pragma warning disable 6506 // e is obviously not null
                if 
                    ( lCast.IsVisible &&
                      lCast.IsEnabled )
                {
                    lSuccess = true;
                }
            }
            else if 
                ( pElement is ContentElement )
            {
                ContentElement lContentElement = pElement as ContentElement;

#pragma warning restore 6506 // ce is obviosuly not null
                if 
                    ( lContentElement.IsEnabled ) // There is no IsVisible property for ContentElement
                {
                    lSuccess = true;
                }
            }
            else
            {
                // Setting capture to null.
                lSuccess = true;
            }

            if 
                ( lSuccess )
            {
                IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
                lSuccess = false;

                if
                    ( lUIService.PresentationSource != null )
                {
                    lSuccess = true;
                    // For now only one source, one window support, so no need to test for capture among different sources.
                    if
                        ( pElement != null )
                    {
                        this.ChangeMouseCapture( pElement, pCaptureMode, lTimeStamp );
                    }
                }
                // Find a mouse input provider that provides input for either
                // the new pElement (if taking capture) or the existing capture
                // pElement (if releasing capture).
                //MouseState? lMouseInputProvider = null;
                //if 
                //    ( pElement != null )
                //{
                //    DependencyObject containingVisual = InputElement.GetContainingVisual(lElement);
                //    if (containingVisual != null)
                //    {
                //        PresentationSource captureSource = PresentationSource.CriticalFromVisual(containingVisual);
                //        if (captureSource != null)
                //        {
                //            mouseInputProvider = captureSource.GetInputProvider(typeof(MouseDevice)) as IMouseInputProvider;
                //        }
                //    }
                //}
                //else if 
                //    ( this.mMouseCapture != null )
                //{
                //    lMouseInputProvider = this.mProviderCapture;
                //}

                //// If we found a mouse input provider, ask it to either capture
                //// or release the mouse for us.
                //if 
                //    ( lMouseInputProvider != null )
                //{
                //    if 
                //        ( pElement != null )
                //    {
                //        lSuccess = mouseInputProvider.CaptureMouse();

                //        if (lSuccess)
                //        {
                //            ChangeMouseCapture(pElement, mouseInputProvider, pCaptureMode, lTimeStamp);
                //        }
                //    }
                //    else
                //    {
                //        mouseInputProvider.ReleaseMouseCapture();

                //        // If we had capture, the input provider will release it.  That will
                //        // cause a RawMouseAction.CancelCapture to be processed, which will
                //        // update our internal states.
                //        lSuccess = true;
                //    }
                //}
            }

            return lSuccess;
        }

        /// <summary>
        /// Processes a new input.
        /// </summary>
        /// <param name="pInput">The new input.</param>
        /// <returns>True if handled, false otherwise.</returns>
        public bool ProcessInput(InputEventArgs pInput)
        {
            if
                ( pInput == null )
            {
                throw new ArgumentNullException("Null input argument!!!");
            }

            // Push a marker indicating the amount of inputs
            // that need to be processed.
            this.PushMarker();

            // Push the input to be processed onto the manager stack.
            this.PushInput( pInput );

            // Post a new job in the UI thread to process Inputs.
            this.RequestKeepProcessingInputs();

            // Now drain the inputs up to the marker we pushed.
            bool lHandled = this.ProcessInputs();
            return lHandled;
        }

        /// <summary>
        /// Pushes a new marker to delimit a set of Inputs
        /// the manager has to process before to stop.
        /// </summary>
        /// <returns>The new event arguments.</returns>
        internal InputEventArgs PushMarker()
        {
            return this.PushInput( new MarkerInputEventArgs() );
        }

        /// <summary>
        /// Pushes a new input event arguments to the manager stack.
        /// </summary>
        /// <param name="pInput">The new input to process.</param>
        /// <returns>The new input stacked.</returns>
        internal InputEventArgs PushInput(InputEventArgs pInput)
        {
            this.mInputs.Push( pInput );
            return pInput;
        }

        /// <summary>
        /// Pops an input from the manager stack.
        /// </summary>
        /// <returns>The popped input.</returns>
        internal InputEventArgs PopInput()
        {
            if 
                ( this.mInputs.Count > 0 )
            {
                return this.mInputs.Pop();
            }

            return null;
        }

        /// <summary>
        /// Peeks an input from the manager stack without removing it.
        /// </summary>
        /// <returns>The peeked input.</returns>
        internal InputEventArgs PeekInput()
        {
            if 
                ( this.mInputs.Count > 0 )
            {
                return this.mInputs.Peek();
            }

            return null;
        }

        /// <summary>
        /// Delegate dispatched on new RequestKeepProcessingInputs calls.
        /// </summary>
        internal void ContinueProcessingInputs()
        {
            this.mKeepProcessingInputs = false;

            if 
                ( this.mInputs.Count > 0 )
            {
                // Post another work later on.
                this.RequestKeepProcessingInputs();

                // Now synchronously drain the inputs stack.
                this.ProcessInputs();
            }
        }

        /// <summary>
        /// Notifies the hit test is valid no more.
        /// </summary>
        internal void NotifyHitTestInvalidated()
        {
            // TO DO...
            // Re evaluate the hit test.
        }

        /// <summary>
        /// Requests the manager be keeping processing inputs.
        /// </summary>
        private void RequestKeepProcessingInputs()
        {
            if 
                ( this.mKeepProcessingInputs == false )
            {
                // Dispatches.
                IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
                lUIService.Dispatcher.Invoke( this.ContinueProcessingInputs );
                this.mKeepProcessingInputs = true;
            }
        }

        /// <summary>
        /// Processes inputs until the next marker input.
        /// </summary>
        /// <returns>True if handled, false otherwise.</returns>
        private bool ProcessInputs()
        {
            bool lHandled = false;

            // OPT: Recycle event arguments instances.
            NotifyInputEventArgs lNotifyInputEventArgs         = this.mNotifyInputEventArgs != null ? this.mNotifyInputEventArgs : new NotifyInputEventArgs();
            ProcessInputEventArgs lProcessInputEventArgs       = this.mProcessInputEventArgs != null ? this.mProcessInputEventArgs : new ProcessInputEventArgs();
            PreProcessInputEventArgs lPreProcessInputEventArgs = this.mPreProcessInputEventArgs != null ? this.mPreProcessInputEventArgs : new PreProcessInputEventArgs();
            this.mNotifyInputEventArgs     = null;
            this.mProcessInputEventArgs    = null;
            this.mPreProcessInputEventArgs = null;

            InputEventArgs lCurrent = this.PopInput();
            while 
                ( lCurrent != null &&
                  (lCurrent is MarkerInputEventArgs) == false ) // If any marker event, it is the end of an lCurrent package to process.
            {
                // Pre-Process the lCurrent. This could modify the lCurrent set
                // being processed and cancel the processing of this
                // lCurrent event.
                if
                    ( this.PreProcessInput != null )
                {
                    lPreProcessInputEventArgs.Reset( lCurrent );

                    Delegate[] lToRuns = this.PreProcessInput.GetInvocationList();

                    // Invoke in reverse order so that users delegates are ran before internal ones.
                    for
                        ( int lCurr = lToRuns.Length - 1; lCurr >= 0; lCurr-- )
                    {
                        PreProcessInputEventDelegate lDelegate = (PreProcessInputEventDelegate)lToRuns[ lCurr ];
                        lDelegate( this, lPreProcessInputEventArgs );
                    }
                }

                // Checks whether there is any cancelation??
                if
                    ( lPreProcessInputEventArgs.IsCanceled == false )
                {
                    // Pre-Notify the lCurrent.
                    if 
                        ( this.PreNotifyInput != null )
                    {
                        lNotifyInputEventArgs.Reset( lCurrent );

                        // Invoke in reverse order so that users delegates are ran before internal ones.
                        Delegate[] lToRuns = this.PreNotifyInput.GetInvocationList();
                        for
                            ( int lCurr = lToRuns.Length - 1; lCurr >= 0; lCurr-- )
                        {
                            NotifyInputEventDelegate lDelegate = (NotifyInputEventDelegate)lToRuns[ lCurr ];
                            lDelegate( this, lNotifyInputEventArgs );
                        }
                    }

                    // Raise the lCurrent event being processed.
                    DependencyObject lEventSource = lCurrent.Source as DependencyObject;
                    if
                        ( lEventSource == null ||
                          InputElement.IsValid( lEventSource as IInputElement ) == false )
                    {
                        if
                            ( lCurrent.Device != null )
                        {
                            lEventSource = this.MouseTarget as DependencyObject;
                        }
                    }

                    // During synchronized lCurrent processing, event should be discarded if not listening for this lCurrent type.
                    if 
                        ( this.mIsSynchronizedInput &&
                          SynchronizedInputHelper.IsMappedEvent(lCurrent) &&
                          Array.IndexOf( this.mSynchronizedInputEvents, lCurrent.RoutedEvent) < 0 &&
                          Array.IndexOf( this.mSynchronizedOppositeInputEvents, lCurrent.RoutedEvent) < 0)
                    {
                        if 
                            ( SynchronizedInputHelper.MustKeepListening( lCurrent ) == false )
                        {
                            // Discard the event
                            this.mSynchronizedInputState = SynchronizedInputStates.Discarded;
                            SynchronizedInputHelper.RaiseAutomaticEvents();
                            this.CancelSynchronizedInput();
                        }
                        else
                        {
                            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
                            lUIService.Dispatcher.Invoke( (Action)delegate
                            {
                                // Discard the event
                                this.mSynchronizedInputState = SynchronizedInputStates.Discarded;
                                SynchronizedInputHelper.RaiseAutomaticEvents();
                                this.CancelSynchronizedInput();
                            } );
                        }
                    }
                    else
                    {
                        if 
                            ( lEventSource != null )
                        {
                            if 
                                ( InputElement.IsUIElement( lEventSource ) )
                            {
                                UIElement lElement = lEventSource as UIElement;

                                lElement.RaiseEvent( lCurrent, true ); // Call the "trusted" flavor of RaiseEvent. 
                            }
                            else if 
                                ( InputElement.IsContentElement( lEventSource ) )
                            {
                                ContentElement lContentElement = lEventSource as ContentElement;

                                lContentElement.RaiseEvent( lCurrent, true );// Call the "trusted" flavor of RaiseEvent.
                            }

                            // If synchronized lCurrent raise appropriate automation event.
                            if 
                                ( this.mIsSynchronizedInput &&
                                  SynchronizedInputHelper.IsListening( this.mListeningElement, lCurrent ) )
                            {
                                if 
                                    ( SynchronizedInputHelper.MustKeepListening( lCurrent ) == false )
                                {
                                    SynchronizedInputHelper.RaiseAutomaticEvents();
                                    this.CancelSynchronizedInput();
                                }
                                else
                                {
                                    IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
                                    lUIService.Dispatcher.Invoke( (Action)delegate
                                    {
                                        SynchronizedInputHelper.RaiseAutomaticEvents();
                                        this.CancelSynchronizedInput();
                                    } );
                                }
                            }
                        }
                    }

                    // Post-Notify the input.
                    if 
                        ( this.PostNotifyInput != null )
                    {
                        this.mNotifyInputEventArgs.Reset( lCurrent );

                        // Invoke in reverse order so that users delegates are ran before internal ones.
                        Delegate[] lToRuns = this.PostNotifyInput.GetInvocationList();
                        for
                            ( int lCurr = lToRuns.Length - 1; lCurr >= 0; lCurr-- )
                        {
                            NotifyInputEventDelegate lDelegate = (NotifyInputEventDelegate)lToRuns[ lCurr ];
                            lDelegate( this, lNotifyInputEventArgs );
                        }
                    }

                    // Post-Process the input.
                    if 
                        ( this.PostProcessInput != null )
                    {
                        this.mProcessInputEventArgs.Reset( lCurrent );

                        this.NotifyProcessInputEventListeners( this.PostProcessInput, this.mProcessInputEventArgs );

                        // PreviewInputReport --> InputReport
                        if 
                            ( lCurrent.RoutedEvent == InputManager.PreviewInputReportEvent )
                        {
                            if 
                                ( lCurrent.IsHandled == false )
                            {
                                InputReportEventArgs lPreviewInputReport = (InputReportEventArgs)lCurrent;

                                InputReportEventArgs lInputReport = new InputReportEventArgs( lPreviewInputReport.Device, lPreviewInputReport.Report );
                                lInputReport.RoutedEvent = InputManager.InputReportEvent;
                                this.PushInput( lInputReport );
                            }
                        }
                    }

                    if 
                        ( lCurrent.IsHandled )
                    {
                        lHandled = true;
                    }
                }
            }

            // Store the lCurrent event args so that we can use them again, and
            // avoid having to allocate more.
            this.mNotifyInputEventArgs     = lNotifyInputEventArgs;
            this.mProcessInputEventArgs    = lProcessInputEventArgs;
            this.mPreProcessInputEventArgs = lPreProcessInputEventArgs;

            // Make sure to throw away the contents of the event args so
            // we don't keep refs around to things we don't mean to.
            this.mNotifyInputEventArgs.Reset( null );
            this.mProcessInputEventArgs.Reset( null );
            this.mPreProcessInputEventArgs.Reset( null );

            return lHandled;
        }

        /// <summary>
        /// Starts listening the given input type for the supplied element.
        /// </summary>
        /// <param name="pObject">The element listening.</param>
        /// <param name="pInputType">The input type to listen.</param>
        /// <returns>True if able to start listening, false otherwise.</returns>
        internal bool StartListeningSynchronizedInput(DependencyObject pObject, SynchronizedInputType pInputType)
        {
            lock 
                ( sSynchronizedInputLock )
            {
                if
                    ( this.mIsSynchronizedInput )
                {
                    return false;
                }
                else
                {
                    this.mIsSynchronizedInput = true;
                    this.mSynchronizedInputState = SynchronizedInputStates.NoOpportunity;
                    this.mListeningElement = pObject;
                    this.mSynchronizedInputType = pInputType;
                    this.mSynchronizedInputEvents = SynchronizedInputHelper.MapInputTypeToRoutedEvents( pInputType );
                    this.mSynchronizedOppositeInputEvents = SynchronizedInputHelper.MapInputTypeToRoutedEvents( SynchronizedInputHelper.GetOppositeInputType( pInputType ) );
                    return true;
                }
            }
        }

        /// <summary>
        /// Stop listening the last synchronized input event for the last input element.
        /// </summary>
        internal void CancelSynchronizedInput()
        {
            lock 
                ( sSynchronizedInputLock )
            {
                this.mIsSynchronizedInput = false;
                this.mSynchronizedInputState = SynchronizedInputStates.NoOpportunity;
                this.mListeningElement = null;
                this.mSynchronizedInputEvents = null;
                this.mSynchronizedOppositeInputEvents = null;
            }
        }

        /// <summary>
        /// Notifies the set of process input event listeners.
        /// </summary>
        /// <param name="pDelegate">The event delegate to run.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void NotifyProcessInputEventListeners(ProcessInputEventDelegate pDelegate, ProcessInputEventArgs pEventArgs)
        {
            pEventArgs.Input.IsInitiated = true;

            try
            {
                // Invoke in reverse order so that users delegates are ran before internal ones.
                Delegate[] lToRuns = this.PostProcessInput.GetInvocationList();
                for 
                    ( int lCurr = (lToRuns.Length - 1); lCurr >= 0; lCurr-- )
                {
                    ProcessInputEventDelegate lDelegate = (ProcessInputEventDelegate)lToRuns[ lCurr ];
                    lDelegate( this, pEventArgs );
                }
            }
            catch
                ( Exception pEx )
            {
                LogManager.Instance.Log( pEx );
            }

            pEventArgs.Input.IsInitiated = false;
        }

        /// <summary>
        /// Delegate called on layout updates.
        /// </summary>
        /// <param name="pSender">The service.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnLayoutUpdated(object pSender, EventArgs pEventArgs)
        {
            this.NotifyHitTestInvalidated();
        }

        /// <summary>
        /// Gets the current screen position.
        /// </summary>
        private Point GetScreenPosition()
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            PresentationSource lSource = lUIService.PresentationSource;
            if
                ( lSource != null )
            {
                return PointUtil.ClientToScreen( this.mLastPosition, lSource );
            }

            return new Point( 0, 0 );
        }

        /// <summary>
        /// Gets the current client space position.
        /// </summary>
        /// <param name="pSource">The window in which the client position is required.</param>
        private Point GetClientPosition(PresentationSource pSource)
        {
            Point lScreen = this.GetScreenPosition();
            Point lClient = PointUtil.ScreenToClient( lScreen, pSource );
            return lClient;
        }

        /// <summary>
        /// Changes the current element the mouse is over.
        /// </summary>
        /// <param name="pMouseOver">The new potential element the mouse is over.</param>
        /// <param name="pTimestamp">The time stamp.</param>
        private void ChangeMouseOver(IInputElement pMouseOver, int pTimestamp)
        {
            DependencyObject lMouseOver = null;
            if 
                ( this.mMouseOver != pMouseOver )
            {
                // Update the critical piece of data.
                IInputElement lPreviousMouseOver = this.mMouseOver;
                this.mMouseOver = pMouseOver;
                
                // Releases the old one resources.
                if 
                    ( lPreviousMouseOver != null )
                {
                    lMouseOver = lPreviousMouseOver as DependencyObject;
                    if 
                        ( InputElement.IsUIElement( lMouseOver ) )
                    {
                        UIElement lCast = lMouseOver as UIElement;
                        lCast.IsEnabledChanged -= this.OnOverIsEnabledChanged;
                        lCast.IsVisibleChanged -= this.OnOverIsVisibleChanged;
                        lCast.IsHitTestVisibleChanged -= this.OnOverIsHitTestVisibleChanged;
                    }
                    else if 
                        ( InputElement.IsContentElement( lMouseOver ) )
                    {
                        ContentElement lCast = lMouseOver as ContentElement;
                        lCast.IsEnabledChanged -= this.OnOverIsEnabledChanged;
                    }
                }

                // Listen to the new one.
                if 
                    ( this.mMouseOver != null )
                {
                    lMouseOver = this.mMouseOver as DependencyObject;
                    if 
                        ( InputElement.IsUIElement( lMouseOver ) )
                    {
                        UIElement lCast = lMouseOver as UIElement;
                        lCast.IsEnabledChanged += this.OnOverIsEnabledChanged;
                        lCast.IsVisibleChanged += this.OnOverIsVisibleChanged;
                        lCast.IsHitTestVisibleChanged += this.OnOverIsHitTestVisibleChanged;

                        UIElement.NotifyMouseOverValueChanged( lPreviousMouseOver as UIElement, 
                                                               lCast );
                    }
                    else if
                        ( InputElement.IsContentElement( lMouseOver ) )
                    {
                        ContentElement lCast = lMouseOver as ContentElement;
                        lCast.IsEnabledChanged += this.OnOverIsEnabledChanged;

                        ContentElement.NotifyMouseOverValueChanged( lPreviousMouseOver as ContentElement, 
                                                                    lCast );
                    }
                }
                
                // Invalidate the IsMouseDirectlyOver property.
                if ( lPreviousMouseOver != null )
                {
                    lMouseOver = lPreviousMouseOver as DependencyObject;

                    UIElement lCast = lPreviousMouseOver as UIElement;
                    if
                        ( lCast != null )
                    {
                        lCast.IsMouseDirectlyOver = false;
                    }
                    else
                    {
                        ContentElement lContent = lPreviousMouseOver as ContentElement;
                        if
                            ( lContent != null )
                        {
                            lContent.IsMouseDirectlyOver = false;
                        }
                    }
                }

                if ( this.mMouseOver != null )
                {
                    lMouseOver = this.mMouseOver as DependencyObject;

                    UIElement lCast = this.mMouseOver as UIElement;
                    if
                        ( lCast != null )
                    {
                        lCast.IsMouseDirectlyOver = true;
                    }
                    else
                    {
                        ContentElement lContent = this.mMouseOver as ContentElement;
                        if
                            ( lContent != null )
                        {
                            lContent.IsMouseDirectlyOver = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Changes the current element the source is captured for.
        /// </summary>
        /// <param name="pMouseCapture">The element captured element within the current captured source.</param>
        /// <param name="pCaptureMode">The capture mode.</param>
        /// <param name="pTimestamp">The time stamp.</param>
        private void ChangeMouseCapture(IInputElement pMouseCapture, CaptureMode pCaptureMode, int pTimestamp)
        {
            DependencyObject lObject = null;
            if 
                ( pMouseCapture != this.mMouseCapture )
            {
                // Update the critical pieces of data.
                IInputElement lOldMouseCapture = this.mMouseCapture;
                this.mMouseCapture = pMouseCapture;
                this.mCaptureMode = pCaptureMode;

                // Adjust the handlers we use to track everything.
                if 
                    ( lOldMouseCapture != null )
                {
                    lObject = lOldMouseCapture as DependencyObject;
                    if 
                        ( InputElement.IsUIElement( lObject ) )
                    {
                        UIElement lCast = lObject as UIElement;
                        lCast.IsEnabledChanged -= this.OnCaptureIsEnabledChanged;
                        lCast.IsVisibleChanged -= this.OnCaptureIsVisibleChanged;
                        lCast.IsHitTestVisibleChanged -= this.OnCaptureIsHitTestVisibleChanged;
                    }
                    else if 
                        ( InputElement.IsContentElement( lObject ) )
                    {
                        ContentElement lCast = lObject as ContentElement;
                        lCast.IsEnabledChanged -= this.OnCaptureIsEnabledChanged;
                    }
                }

                if 
                    ( this.mMouseCapture != null )
                {
                    lObject = this.mMouseCapture as DependencyObject;
                    if
                        ( InputElement.IsUIElement( lObject ) )
                    {
                        UIElement lCast = lObject as UIElement;
                        lCast.IsEnabledChanged += this.OnCaptureIsEnabledChanged;
                        lCast.IsVisibleChanged += this.OnCaptureIsVisibleChanged;
                        lCast.IsHitTestVisibleChanged += this.OnCaptureIsHitTestVisibleChanged;

                        UIElement.NotifyMouseCaptureWithinValueChanged( lOldMouseCapture as UIElement,
                                                                        lCast );
                    }
                    else if
                        ( InputElement.IsContentElement( lObject ) )
                    {
                        ContentElement lCast = lObject as ContentElement;
                        lCast.IsEnabledChanged += this.OnCaptureIsEnabledChanged;

                        ContentElement.NotifyMouseCaptureWithinValueChanged( lOldMouseCapture as ContentElement,
                                                                             lCast );
                    }
                }
                
                // Invalidate the IsMouseCaptured properties.
                if 
                    ( lOldMouseCapture != null )
                {
                    lObject = lOldMouseCapture as DependencyObject;

                    UIElement lCast = lOldMouseCapture as UIElement;
                    if
                        ( lCast != null )
                    {
                        lCast.IsMouseCaptured = false;
                    }
                    else
                    {
                        ContentElement lContent = lOldMouseCapture as ContentElement;
                        if
                            ( lContent != null )
                        {
                            lContent.IsMouseCaptured = false;
                        }
                    }
                }

                if 
                    ( this.mMouseCapture != null )
                {
                    lObject = this.mMouseCapture as DependencyObject;

                    UIElement lCast = this.mMouseCapture as UIElement;
                    if
                        ( lCast != null )
                    {
                        lCast.IsMouseCaptured = true;
                    }
                    else
                    {
                        ContentElement lContent = this.mMouseCapture as ContentElement;
                        if
                            ( lContent != null )
                        {
                            lContent.IsMouseCaptured = true;
                        }
                    }
                }

                // Send the LostMouseCapture and GotMouseCapture events.
                if 
                    ( lOldMouseCapture != null )
                {
                    Core.MouseEventArgs lLostCapture = new Core.MouseEventArgs( this.Mouse, pTimestamp );
                    lLostCapture.RoutedEvent = InputManager.LostMouseCaptureEvent;
                    lLostCapture.Source = lOldMouseCapture;
                    this.ProcessInput(lLostCapture);
                }
                if 
                    ( this.mMouseCapture != null )
                {
                    Core.MouseEventArgs lGotCapture = new Core.MouseEventArgs( this.Mouse, pTimestamp );
                    lGotCapture.RoutedEvent = InputManager.GotMouseCaptureEvent;
                    lGotCapture.Source = this.mMouseCapture;
                    this.ProcessInput( lGotCapture );
                }

                // Force a mouse move so we can update the mouse over.
                this.Synchronize();
            }
        }

        /// <summary>
        /// Delegate called on element IsEnabled changes.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnOverIsEnabledChanged(object pSender, OrcNet.Core.PropertyChangedEventArgs pEventArgs)
        {
            // Need to resynchronize the mouse so that we can figure out who the mouse is over now.
            this.ReevaluateMouseOver( null, null );
        }

        /// <summary>
        /// Delegate called on element IsVisible changes.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnOverIsVisibleChanged(object pSender, OrcNet.Core.PropertyChangedEventArgs pEventArgs)
        {
            // Need to resynchronize the mouse so that we can figure out who the mouse is over now.
            this.ReevaluateMouseOver( null, null );
        }

        /// <summary>
        /// Delegate called on element IsHitTestVisible changes.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnOverIsHitTestVisibleChanged(object pSender, OrcNet.Core.PropertyChangedEventArgs pEventArgs)
        {
            // Need to resynchronize the mouse so that we can figure out who the mouse is over now.
            this.ReevaluateMouseOver( null, null );
        }

        /// <summary>
        /// Re evaluate the mouse over element.
        /// </summary>
        /// <param name="pElement">The element for mouse over re evaluation if any.</param>
        /// <param name="pOldParent">The element parent for mouse over re evaluation if any.</param>
        internal void ReevaluateMouseOver(DependencyObject pElement, DependencyObject pOldParent)
        {
            this.Synchronize();
        }

        /// <summary>
        /// Delegate called on element IsEnabled changes for capture purpose ONLY.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnCaptureIsEnabledChanged(object pSender, OrcNet.Core.PropertyChangedEventArgs pEventArgs)
        {
            // Need to re-evaluate the element that has mouse capture since we can't allow the mouse to remain captured by a disabled element.
            this.ReevaluateCapture( null, null );
        }

        /// <summary>
        /// Delegate called on element IsVisible changes for capture purpose ONLY.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnCaptureIsVisibleChanged(object pSender, OrcNet.Core.PropertyChangedEventArgs pEventArgs)
        {
            // Need to re-evaluate the element that has mouse capture since we can't allow the mouse to remain captured by a non-visible element.
            this.ReevaluateCapture( null, null );
        }

        /// <summary>
        /// Delegate called on element IsHitTestVisible changes for capture purpose ONLY.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnCaptureIsHitTestVisibleChanged(object pSender, OrcNet.Core.PropertyChangedEventArgs pEventArgs)
        {
            // Need to re-evaluate the element that has mouse capture since we can't allow the mouse to remain captured by a non-hittest-visible element.
            this.ReevaluateCapture( null, null );
        }

        /// <summary>
        /// Re evaluate the element capture.
        /// </summary>
        /// <param name="pElement">The element for mouse over re evaluation if any.</param>
        /// <param name="pOldParent">The element parent for mouse over re evaluation if any.</param>
        internal void ReevaluateCapture(DependencyObject pElement, DependencyObject pOldParent)
        {
            if 
                ( this.mMouseCapture == null )
            {
                return;
            }

            bool lMustResetCapture = false;
            DependencyObject lCurrentCapture = this.mMouseCapture as DependencyObject;

            if 
                ( InputElement.IsUIElement( lCurrentCapture ) )
            {
                lMustResetCapture = this.ValidateUIElementForCapture( lCurrentCapture as UIElement ) == false;
            }
            else if 
                ( InputElement.IsContentElement( lCurrentCapture ) )
            {
                lMustResetCapture = this.ValidateContentElementForCapture( lCurrentCapture as ContentElement ) == false;
            }

            // If no way to kill capture with UI Elements, try on a visual basis.
            if 
                ( lMustResetCapture == false )
            {
                DependencyObject lContainingVisual = InputElement.GetContainingVisual( lCurrentCapture );
                lMustResetCapture = this.ValidateVisualForCapture( lContainingVisual ) == false;
            }

            // If must kill capture, do it.
            if
                ( lMustResetCapture )
            {
                this.Capture( null );
            }
        }

        /// <summary>
        /// Synchronize current cached values to inputs.
        /// </summary>
        public void Synchronize()
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();

            // Simulate a mouse move
            PresentationSource lSource = lUIService.PresentationSource;
            if ( lSource != null )
            {
                int lTimeStamp = Environment.TickCount;
                Point lClientPoint = this.GetClientPosition( lSource );

                MouseInputReport lReport = new MouseInputReport( InputMode.Foreground,
                                                                 lTimeStamp,
                                                                 lSource,
                                                                 MouseActions.AbsoluteMove,
                                                                 (int)lClientPoint.X,
                                                                 (int)lClientPoint.Y,
                                                                 0,
                                                                 IntPtr.Zero );
                lReport.IsSynchronized = true;

                InputReportEventArgs lInputReportEventArgs = new InputReportEventArgs( this.Mouse, 
                                                                                       lReport );
                lInputReportEventArgs.RoutedEvent = InputManager.PreviewInputReportEvent;

                // Process the input.
                this.ProcessInput( lInputReportEventArgs );
            }
        }

        /// <summary>
        /// Delegate called on pre process input event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnPreProcessInput(object pSender, PreProcessInputEventArgs pEventArgs)
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            PresentationSource lInputSource = lUIService.PresentationSource;
            if 
                ( pEventArgs.Input.RoutedEvent == InputManager.PreviewInputReportEvent )
            {
                InputReportEventArgs lInputReportEventArgs = pEventArgs.Input as InputReportEventArgs;

                if 
                    ( lInputReportEventArgs.IsHandled == false && 
                      lInputReportEventArgs.Report.Type == InputType.Mouse )
                {
                    MouseInputReport lMouseInputReport = lInputReportEventArgs.Report as MouseInputReport;
                    
                    // Normally we only process mouse input that is from our
                    // active visual manager.  The only exception to this is
                    // the activate report, which is how we change the visual
                    // manager that is active.
                    if 
                        ( (lMouseInputReport.Actions & MouseActions.Activate) == MouseActions.Activate )
                    {
                        // If other actions are being reported besides the
                        // activate, separate them into different events.
                        if 
                            ( (lMouseInputReport.Actions & ~MouseActions.Activate) != 0 )
                        {
                            // Cancel this event.  We'll push a new event for the activate.
                            pEventArgs.Cancel();

                            // Push a new MouseInputReport for the non-activate actions.
                            MouseInputReport lActionsReport = new MouseInputReport( lMouseInputReport.Mode,
                                                                                    lMouseInputReport.TimeStamp,
                                                                                    lMouseInputReport.InputSource,
                                                                                    lMouseInputReport.Actions & ~MouseActions.Activate,
                                                                                    lMouseInputReport.X,
                                                                                    lMouseInputReport.Y,
                                                                                    lMouseInputReport.Wheel,
                                                                                    lMouseInputReport.Extra );
                            InputReportEventArgs actionsArgs = new InputReportEventArgs( lInputReportEventArgs.Device, lActionsReport );
                            actionsArgs.RoutedEvent = InputManager.PreviewInputReportEvent;
                            this.PushInput( actionsArgs );

                            // Create a new MouseInputReport for the activate.
                            MouseInputReport lActivationReport = new MouseInputReport( lMouseInputReport.Mode,
                                                                                       lMouseInputReport.TimeStamp,
                                                                                       lMouseInputReport.InputSource,
                                                                                       MouseActions.Activate,
                                                                                       lMouseInputReport.X,
                                                                                       lMouseInputReport.Y,
                                                                                       lMouseInputReport.Wheel,
                                                                                       lMouseInputReport.Extra );

                            // Push a new MouseInputReport for the activate.
                            InputReportEventArgs activateArgs = new InputReportEventArgs(lInputReportEventArgs.Device, lActivationReport);
                            activateArgs.RoutedEvent = InputManager.PreviewInputReportEvent;
                            this.PushInput( activateArgs );
                        }
                    }
                    // Only process mouse input that is from our active PresentationSource.
                    else if 
                        ( lInputSource != null && 
                          lMouseInputReport.InputSource == lInputSource )
                    {
                        // Claim the input for the mouse.
                        lInputReportEventArgs.Device = this.Mouse;

                        // If the input is reporting mouse deactivation, we need
                        // to ensure that the element receives a final leave.
                        // Note that activation could have been moved to another
                        // visual manager in our app, which means that the leave
                        // was already sent.  So only do this if the deactivate
                        // event is from the visual manager that we think is active.
                        if 
                            ( (lMouseInputReport.Actions & MouseActions.Deactivate) == MouseActions.Deactivate )
                        {
                            if 
                                ( this.mMouseOver != null )
                            {
                                // Push back this event, and cancel the current processing.
                                this.PushInput( pEventArgs.Input );
                                pEventArgs.Cancel();
                                this.mIsMousePhysicallyOver = false;
                                this.ChangeMouseOver( null, pEventArgs.Input.TimeStamp );
                            }
                        }

                        // If the input is reporting mouse movement, we need to check
                        // if we need to update our sense of "mouse over".
                        // 
                        if 
                            ( (lMouseInputReport.Actions & MouseActions.AbsoluteMove) == MouseActions.AbsoluteMove )
                        {
                            // If other actions are being reported besides the
                            // move, separate them into different events.
                            if 
                                ( (lMouseInputReport.Actions & ~MouseActions.AbsoluteMove) != 0 )
                            {
                                // Cancel this event.  We'll push a new event for the move.
                                pEventArgs.Cancel();

                                // Push a new MouseInputReport for the non-move actions.
                                MouseInputReport lActionsReport = new MouseInputReport( lMouseInputReport.Mode,
                                                                                        lMouseInputReport.TimeStamp,
                                                                                        lMouseInputReport.InputSource,
                                                                                        lMouseInputReport.Actions & ~MouseActions.AbsoluteMove,
                                                                                        0,
                                                                                        0,
                                                                                        lMouseInputReport.Wheel,
                                                                                        lMouseInputReport.Extra );
                                InputReportEventArgs actionsArgs = new InputReportEventArgs( this.Mouse, lActionsReport );
                                actionsArgs.RoutedEvent = InputManager.PreviewInputReportEvent;
                                pEventArgs.PushInput( actionsArgs );

                                // Push a new MouseInputReport for the AbsoluteMove.
                                MouseInputReport lMoveReport = new MouseInputReport( lMouseInputReport.Mode,
                                                                                     lMouseInputReport.TimeStamp,
                                                                                     lMouseInputReport.InputSource,
                                                                                     lMouseInputReport.Actions & MouseActions.AbsoluteMove,
                                                                                     lMouseInputReport.X,
                                                                                     lMouseInputReport.Y,
                                                                                     0,
                                                                                     IntPtr.Zero );
                                InputReportEventArgs moveArgs = new InputReportEventArgs( this.Mouse, lMoveReport );
                                moveArgs.RoutedEvent = InputManager.PreviewInputReportEvent;
                                pEventArgs.PushInput( moveArgs );
                            }
                            else
                            {
                                // Convert the point from client coordinates into "root" coordinates.
                                // We do this in the pre-process stage because it is possible that
                                // this conversion will fail, in which case we want to cancel the
                                // mouse move event.
                                bool lSuccess = true;
                                Point lClientPoint = new Point( lMouseInputReport.X, lMouseInputReport.Y );
                                Point lPointInRootSpace = PointUtil.TryClientToRoot( lClientPoint, lMouseInputReport.InputSource, false, out lSuccess );
                                if 
                                    ( lSuccess )
                                {
                                    pEventArgs.Input.SetData( this.mRootPointTag, lPointInRootSpace );
                                }
                                else
                                {
                                    pEventArgs.Cancel();
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // All mouse event processing should only happen if we still have an active input source.
                if 
                    ( lInputSource != null )
                {
                    if 
                        ( pEventArgs.Input.RoutedEvent == InputManager.PreviewMouseDownEvent )
                    {
                        Core.MouseButtonEventArgs lMouseButtonEventArgs = pEventArgs.Input as Core.MouseButtonEventArgs;
                        if ( this.mMouseCapture != null && 
                             this.mIsMousePhysicallyOver == false )
                        {
                            // The mouse is not physically over the capture point (or
                            // subtree), so raise the PreviewMouseDownOutsideCapturedElement
                            // event first.
                            Core.MouseButtonEventArgs lClickThrough = new Core.MouseButtonEventArgs( this.Mouse, lMouseButtonEventArgs.TimeStamp, lMouseButtonEventArgs.Button );
                            lClickThrough.RoutedEvent = InputManager.PreviewMouseDownOutsideCapturedElementEvent;
                            this.ProcessInput( lClickThrough );
                        }
                    }
                    else if 
                        ( pEventArgs.Input.RoutedEvent == InputManager.PreviewMouseUpEvent )
                    {
                        Core.MouseButtonEventArgs lMouseButtonEventArgs = pEventArgs.Input as Core.MouseButtonEventArgs;
                        if ( this.mMouseCapture != null && 
                             this.mIsMousePhysicallyOver == false )
                        {
                            // The mouse is not physically over the capture point (or
                            // subtree), so raise the PreviewMouseUpOutsideCapturedElement
                            // event first.
                            Core.MouseButtonEventArgs lClickThrough = new Core.MouseButtonEventArgs( this.Mouse, lMouseButtonEventArgs.TimeStamp, lMouseButtonEventArgs.Button );
                            lClickThrough.RoutedEvent = InputManager.PreviewMouseUpOutsideCapturedElementEvent;
                            this.ProcessInput( lClickThrough );
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called on pre notify input event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnPreNotifyInput(object pSender, NotifyInputEventArgs pEventArgs)
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            PresentationSource lInputSource = lUIService.PresentationSource;
            if
                ( pEventArgs.Input.RoutedEvent == InputManager.PreviewInputReportEvent )
            {
                InputReportEventArgs lInputReportEventArgs = pEventArgs.Input as InputReportEventArgs;
                if 
                    ( lInputReportEventArgs.IsHandled == false && 
                      lInputReportEventArgs.Report.Type == InputType.Mouse )
                {
                    MouseInputReport lMouseInputReport = lInputReportEventArgs.Report as MouseInputReport;

                    // Generally, we need to check against redundant actions.
                    // We never prevent the raw event from going through, but we
                    // will only generate the high-level events for non-redundant
                    // lActions.  We store the set of non-redundant lActions in
                    // the dictionary of this event.

                    // Get the current Non-Redundant Actions for this event and
                    // make a copy.  We will compare the original value against the copy
                    // at the end of this function and write it back in if changed.
                    MouseActions lActions = this.GetNonRedundantActions( pEventArgs );
                    MouseActions lOriginalActions = lActions;
                    
                    // Normally we only process mouse input that is from our
                    // active presentation source.  The only exception to this is
                    // the activate report, which is how we change the visual
                    // manager that is active.
                    if ((lMouseInputReport.Actions & MouseActions.Activate) == MouseActions.Activate)
                    {
                        lActions |= MouseActions.Activate;

                        this.mPositionRelativeToOver.X = 0;
                        this.mPositionRelativeToOver.Y = 0;

                        this.mLastPosition.X = lMouseInputReport.X;
                        this.mLastPosition.Y = lMouseInputReport.Y;
                        this.mMustUpdateLastPosition = true;
                        
                        // if the existing source is null, no need to do any special-case handling
                        if 
                            ( lInputSource == null )
                        {
                            lInputSource = lMouseInputReport.InputSource;
                        }
                        // if the new source is the same as the old source, don't bother doing anything
                        else if 
                            ( lInputSource != lMouseInputReport.InputSource )
                        {
                            // All mouse information is now restricted to this presentation source.
                            lInputSource = lMouseInputReport.InputSource;
                        }
                    }

                    // Only process mouse input that is from our active presentation source.
                    if 
                        ( lInputSource != null && 
                          lMouseInputReport.InputSource == lInputSource )
                    {
                        // If the input is reporting mouse deactivation, we need
                        // to break any capture we may have.  Note that we only do
                        // this if the presentation source associated with this event
                        // is the same presentation source we are already over.
                        if 
                            ( (lMouseInputReport.Actions & MouseActions.Deactivate) == MouseActions.Deactivate )
                        {
                            Debug.Assert( this.mMouseOver == null, "mMouseOver should be null because we have called ChangeMouseOver(null) already.");
                            lInputSource = null;

                            this.ChangeMouseCapture( null, CaptureMode.None, pEventArgs.Input.TimeStamp );
                        }

                        if 
                            ( (lMouseInputReport.Actions & MouseActions.CancelCapture) == MouseActions.CancelCapture )
                        {
                            this.ChangeMouseCapture( null, CaptureMode.None, pEventArgs.Input.TimeStamp );
                        }

                        // If the input is reporting mouse movement, only update the
                        // set of non-redundant lActions if the position changed.
                        if 
                            ( (lMouseInputReport.Actions & MouseActions.AbsoluteMove) == MouseActions.AbsoluteMove )
                        {
                            // Translate the mouse coordinates to both root relative and "lMouseOver" relate.
                            // - Note: "lMouseOver" in this case is the element the mouse "was" over before this move.
                            bool lIsMouseOverAvailable = false;
                            Point lClientPoint = new Point( lMouseInputReport.X, lMouseInputReport.Y );
                            Point lPointInRootSpace = (Point)pEventArgs.Input.GetData( this.mRootPointTag );
                            Point lPointRelativeToOver = InputElement.TranslatePoint( lPointInRootSpace, lMouseInputReport.InputSource.RootVisual, this.mMouseOver as DependencyObject, out lIsMouseOverAvailable );

                            IInputElement lMouseOver = this.mMouseOver;
                            IInputElement lRawMouseOver = this.mRawMouseOver != null ? (IInputElement)this.mRawMouseOver.Target : null;
                            bool lIsPhysicallyOver = this.mIsMousePhysicallyOver;
                            bool lIsGlobalChange = ArePointsClose( lClientPoint, this.mLastPosition ) == false;  // determine if the mouse actually physically moved

                            // Invoke Hit Test logic to determine what element the mouse will be over AFTER the move is processed.
                            // - Only do this if:
                            //      - The mouse physcially moved (lIsGlobalChange)
                            //      - We are simulating a mouse move (_isSynchronize)
                            //      - lMouseOver isn't availabe (lIsMouseOverAvailable == false)  Could be caused by a degenerate transform.
                            // - This is to mitigate the redundant AbsoluteMove notifications associated with QueryCursor
                            if 
                                ( lIsGlobalChange || 
                                  lMouseInputReport.IsSynchronized || 
                                  lIsMouseOverAvailable == false )
                            {
                                lIsPhysicallyOver = true;  // Assume mouse is physical over element, we'll set it false if it's due to capture

                                switch 
                                    ( this.mCaptureMode )
                                {
                                    // In this case there is no capture, so a simple hit test will determine which element becomes "lMouseOver"
                                    case CaptureMode.None:
                                        {
                                            if
                                                ( lMouseInputReport.IsSynchronized )
                                            {
                                                GlobalHitTest( true, lClientPoint, lInputSource, out lMouseOver, out lRawMouseOver );
                                            }
                                            else
                                            {
                                                LocalHitTest( true, lClientPoint, lInputSource, out lMouseOver, out lRawMouseOver );
                                            }

                                            if 
                                                ( lMouseOver == lRawMouseOver )
                                            {
                                                // Since they are the same, there is no reason to process lRawMouseOver
                                                lRawMouseOver = null;
                                            }

                                            // We understand UIElements and ContentElements.
                                            // If we are over something else (like a raw visual)
                                            // find the containing element.
                                            if 
                                                ( InputElement.IsValid( lMouseOver ) == false )
                                            {
                                                lMouseOver = InputElement.GetContainingInputElement( lMouseOver as DependencyObject );
                                            }
                                            
                                            if 
                                                ( lRawMouseOver != null && 
                                                  InputElement.IsValid( lRawMouseOver ) == false )
                                            {
                                                lRawMouseOver = InputElement.GetContainingInputElement( lRawMouseOver as DependencyObject );
                                            }
                                            
                                        }
                                        break;

                                    // In this case, capture is to a specific element, so it will ALWAYS become "lMouseOver"
                                    // - however, we do a hit test to see if the mouse is actually physically over the element,
                                    // - if it is not, we toggle lIsPhysicallyOver
                                    case CaptureMode.Element:
                                        { 
                                            if 
                                                ( lMouseInputReport.IsSynchronized )
                                            {
                                                lMouseOver = GlobalHitTest( true, lClientPoint, lInputSource );
                                            }
                                            else
                                            {
                                                lMouseOver = LocalHitTest( true, lClientPoint, lInputSource );
                                            }

                                            // There is no reason to process lRawMouseOver when
                                            // the element should always be the one with mouse capture.
                                            lRawMouseOver = null;

                                            if
                                                ( lMouseOver != this.mMouseCapture )
                                            {
                                                // Always consider the mouse over the capture point.
                                                lMouseOver = this.mMouseCapture;
                                                lIsPhysicallyOver = false;
                                            }
                                        }
                                        break;

                                    // In this case, capture is set to an entire subtree.  We use simple hit testing to determine
                                    // which, if any element in the subtree it is over, and set "lMouseOver to that element
                                    // If it is not over any specific subtree element, "lMouseOver" is set to the root of the subtree.
                                    // - Note: a subtree can span multiple HWNDs
                                    case CaptureMode.SubTree:
                                        {
                                            IInputElement lMouseCapture = InputElement.GetContainingInputElement( this.mMouseCapture as DependencyObject );
                                            if 
                                                ( lMouseCapture != null )
                                            {
                                                // We need to re-hit-test to get the "real" UIElement we are over.
                                                // This allows us to have our capture-to-subtree span multiple windows.

                                                // GlobalHitTest always returns an IInputElement, so we are sure to have one.
                                                GlobalHitTest( true, lClientPoint, lInputSource, out lMouseOver, out lRawMouseOver );
                                            }

                                            if
                                                ( lMouseOver != null && 
                                                  InputElement.IsValid( lMouseOver ) == false )
                                            {
                                                lMouseOver = InputElement.GetContainingInputElement( lMouseOver as DependencyObject );
                                            }
                                            
                                            // Make sure that the element we hit is acutally underneath
                                            // our captured element.  Because we did a global hit test, we
                                            // could have hit an element in a completely different window.
                                            //
                                            // Note that we support the child being in a completely different window.
                                            // So we use the GetUIParent method instead of just looking at
                                            // visual/content parents.
                                            if 
                                                ( lMouseOver != null )
                                            {
                                                IInputElement lInputElementToTest = lMouseOver;
                                                UIElement lUIElementToTest = null;
                                                ContentElement lContentElementToTest = null;

                                                while 
                                                    ( lInputElementToTest != null && 
                                                      lInputElementToTest != lMouseCapture )
                                                {
                                                    lUIElementToTest = lInputElementToTest as UIElement;
                                                    if 
                                                        ( lUIElementToTest != null )
                                                    {
                                                        lInputElementToTest = InputElement.GetContainingInputElement( lUIElementToTest.GetUIParent( true ) );
                                                    }
                                                    else
                                                    {
                                                        lContentElementToTest = lInputElementToTest as ContentElement;
                                                        if 
                                                            ( lContentElementToTest != null )
                                                        {
                                                            lInputElementToTest = InputElement.GetContainingInputElement( lContentElementToTest.GetUIParent( true ) );
                                                        }
                                                    }
                                                }

                                                // If we missed the capture point, we didn't hit anything.
                                                if 
                                                    ( lInputElementToTest != lMouseCapture )
                                                {
                                                    lMouseOver = this.mMouseCapture;
                                                    lIsPhysicallyOver = false;

                                                    // Since they are the same, there is no reason to process lRawMouseOver
                                                    lRawMouseOver = null;
                                                }
                                            }
                                            else
                                            {
                                                // We didn't hit anything.  Consider the mouse over the capture point.
                                                lMouseOver = this.mMouseCapture;
                                                lIsPhysicallyOver = false;

                                                // Since they are the same, there is no reason to process lRawMouseOver
                                                lRawMouseOver = null;
                                            }

                                            if 
                                                ( lRawMouseOver != null )
                                            {
                                                if 
                                                    ( lMouseOver == lRawMouseOver )
                                                {
                                                    // Since they are the same, there is no reason to process lRawMouseOver
                                                    lRawMouseOver = null;
                                                }
                                                else if 
                                                    ( InputElement.IsValid( lRawMouseOver ) == false )
                                                {
                                                    lRawMouseOver = InputElement.GetContainingInputElement( lRawMouseOver as DependencyObject );
                                                }
                                            }
                                        }
                                        break;
                                }
                            }

                            this.mIsMousePhysicallyOver = lMouseOver == null ? false : lIsPhysicallyOver;

                            // Now that we've determine what element the mouse is over now (lMouseOver)
                            // - we need to check if it's changed

                            bool lIsMouseOverChanged = lMouseOver != this.mMouseOver;

                            // If lMouseOver changed, we need to recalculate the lPointRelativeToOver, because "Over" changed!
                            if 
                                ( lIsMouseOverChanged )
                            {
                                lPointRelativeToOver = InputElement.TranslatePoint( lPointInRootSpace, lMouseInputReport.InputSource.RootVisual, (DependencyObject)lMouseOver );
                            }
                            
                            // Check to see if the local mouse position changed.  This can be
                            // caused by a change to the geometry of the
                            // element we are over or a change in which element
                            // we are over.
                            //
                            bool lIsLocalChange = lIsMouseOverChanged || ArePointsClose( lPointRelativeToOver, this.mPositionRelativeToOver ) == false;
                            if 
                                ( lIsGlobalChange || 
                                  lIsLocalChange || 
                                  this.mMustUpdateLastPosition )
                            {
                                this.mMustUpdateLastPosition = false;

                                this.mLastPosition = lClientPoint;
                                this.mPositionRelativeToOver = lPointRelativeToOver;

                                if (lIsMouseOverChanged)
                                {
                                    ChangeMouseOver(lMouseOver, pEventArgs.Input.TimeStamp);
                                }

                                if 
                                    ( this.mRawMouseOver == null && 
                                      lRawMouseOver != null )
                                {
                                    this.mRawMouseOver = new WeakReference( lRawMouseOver );
                                }
                                else if 
                                    ( this.mRawMouseOver != null )
                                {
                                    this.mRawMouseOver.Target = lRawMouseOver;
                                }

                                lActions |= MouseActions.AbsoluteMove;
                            }
                        }

                        // Mouse wheel rotate events are never considered redundant.
                        if 
                            ( (lMouseInputReport.Actions & MouseActions.VerticalWheelRotate) == MouseActions.VerticalWheelRotate )
                        {
                            lActions |= MouseActions.VerticalWheelRotate;
                        }
                        
                        MouseActions[] lButtonPressActions =
                        {
                            MouseActions.Button1Press,
                            MouseActions.Button2Press,
                            MouseActions.Button3Press,
                            MouseActions.Button4Press,
                            MouseActions.Button5Press
                        };

                        MouseActions[] lButtonReleaseActions =
                        {
                            MouseActions.Button1Release,
                            MouseActions.Button2Release,
                            MouseActions.Button3Release,
                            MouseActions.Button4Release,
                            MouseActions.Button5Release
                        };

                        for 
                            ( int lButton = 0; lButton < 5; lButton++ )
                        {
                            if 
                                ( (lMouseInputReport.Actions & lButtonPressActions[ lButton ]) == lButtonPressActions[ lButton ] )
                            {
                                lActions |= lButtonPressActions[ lButton ];
                            }

                            if 
                                ( (lMouseInputReport.Actions & lButtonReleaseActions[ lButton ]) == lButtonReleaseActions[ lButton ] )
                            {
                                lActions |= lButtonReleaseActions[ lButton ];
                            }
                        }
                    }

                    if 
                        ( lActions != lOriginalActions )
                    {
                        pEventArgs.Input.SetData( this.mNonRedundantActionsTag, lActions );
                    }
                }
            }
            else
            {
                // All mouse event processing should only happen if we still have an active input source.
                if
                    ( lInputSource != null )
                {
                    // During the PreviewMouseDown event, we update the click count, if there are
                    // multiple "quick" clicks in approximately the "same" location (as defined
                    // by the hosting environment, aka the registry).
                    if 
                        ( pEventArgs.Input.RoutedEvent == InputManager.PreviewMouseDownEvent )
                    {
                        Core.MouseButtonEventArgs lMouseButtonArgs = pEventArgs.Input as Core.MouseButtonEventArgs;
                        Point lClientPoint = this.GetClientPosition( lInputSource );

                        this.mClickCount = this.CalculateClickCount( lMouseButtonArgs.Button, lMouseButtonArgs.TimeStamp, lClientPoint );
                        if 
                            ( this.mClickCount == 1 )
                        {
                            // we need to reset out data, since this is the start of the click count process...
                            this.mLastClick  = lClientPoint;
                            this.mLastButton = lMouseButtonArgs.Button;
                            this.mLastClickTime = lMouseButtonArgs.TimeStamp;
                        }

                        // Put the updated count into the args.
                        //lMouseButtonArgs.ClickCount = this.mClickCount;
                    }
                }
            }
        }

        /// <summary>
        /// Delegate called on post process input event.
        /// </summary>
        /// <param name="pSender">The sender.</param>
        /// <param name="pEventArgs">The event arguments.</param>
        private void OnPostProcessInput(object pSender, ProcessInputEventArgs pEventArgs)
        {
            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            PresentationSource lInputSource = lUIService.PresentationSource;

            // PreviewMouseWheel --> MouseWheel
            if 
                ( pEventArgs.Input.RoutedEvent == InputManager.PreviewMouseWheelEvent )
            {
                if 
                    ( pEventArgs.Input.IsHandled == false )
                {
                    Core.MouseWheelEventArgs lPreviewWheel = pEventArgs.Input as Core.MouseWheelEventArgs;
                    Core.MouseWheelEventArgs lWheel = new Core.MouseWheelEventArgs( this.Mouse, lPreviewWheel.TimeStamp, lPreviewWheel.Delta );
                    lWheel.RoutedEvent = InputManager.MouseWheelEvent;

                    this.PushInput( lWheel );
                }
            }

            // PreviewMouseDown --> MouseDown
            if 
                ( pEventArgs.Input.RoutedEvent == InputManager.PreviewMouseDownEvent )
            {
                if 
                    ( pEventArgs.Input.IsHandled == false )
                {
                    Core.MouseButtonEventArgs lPreviewDown = pEventArgs.Input as Core.MouseButtonEventArgs;
                    Core.MouseButtonEventArgs lDown = new Core.MouseButtonEventArgs( this.Mouse, lPreviewDown.TimeStamp, lPreviewDown.Button );
                    //lDown.ClickCount = lPreviewDown.ClickCount;
                    lDown.RoutedEvent = InputManager.MouseDownEvent;
                    this.PushInput( lDown );
                }
            }

            // PreviewMouseUp --> MouseUp
            if 
                ( pEventArgs.Input.RoutedEvent == InputManager.PreviewMouseUpEvent )
            {
                if 
                    ( pEventArgs.Input.IsHandled == false )
                {
                    Core.MouseButtonEventArgs lPreviewUp = pEventArgs.Input as Core.MouseButtonEventArgs;
                    Core.MouseButtonEventArgs lUp = new Core.MouseButtonEventArgs( this.Mouse, lPreviewUp.TimeStamp, lPreviewUp.Button );
                    lUp.RoutedEvent = InputManager.MouseUpEvent;
                    this.PushInput( lUp );
                }
            }

            // PreviewMouseMove --> MouseMove
            if 
                ( pEventArgs.Input.RoutedEvent == InputManager.PreviewMouseMoveEvent )
            {
                if 
                    ( pEventArgs.Input.IsHandled == false )
                {
                    Core.MouseEventArgs lPreviewMove = (Core.MouseEventArgs)pEventArgs.Input;
                    Core.MouseEventArgs lMove = new Core.MouseEventArgs( this.Mouse, lPreviewMove.TimeStamp );
                    lMove.RoutedEvent = InputManager.MouseMoveEvent;
                    this.PushInput( lMove );
                }
            }
            
            if 
                ( pEventArgs.Input.RoutedEvent == InputManager.InputReportEvent )
            {
                InputReportEventArgs lInputReportEventArgs = pEventArgs.Input as InputReportEventArgs;
                if 
                    ( lInputReportEventArgs.IsHandled == false && 
                      lInputReportEventArgs.Report.Type == InputType.Mouse )
                {
                    MouseInputReport lMouseInputReport = lInputReportEventArgs.Report as MouseInputReport;

                    // Only process mouse input that is from our active visual manager.
                    if 
                        ( lInputSource != null && 
                          lMouseInputReport.InputSource == lInputSource )
                    {
                        // In general, this is where we promote the non-redundant
                        // reported lActions to our premier events.
                        MouseActions lActions = this.GetNonRedundantActions( pEventArgs );

                        // Raw Activate --> Raw MouseMove
                        // Whenever the mouse device is activated we need to
                        // cause a mouse lMove so that elements realize that
                        // the mouse is over them again.  In most cases, the
                        // action that caused the mouse to activate is a lMove,
                        // but this is to guard against any other cases.
                        if 
                            ( (lActions & MouseActions.Activate) == MouseActions.Activate )
                        {
                            this.Synchronize();
                        }

                        // Raw --> PreviewMouseWheel
                        if 
                            ( (lActions & MouseActions.VerticalWheelRotate) == MouseActions.VerticalWheelRotate) // 
                        {
                            Core.MouseWheelEventArgs lPreviewWheel = new Core.MouseWheelEventArgs( this.Mouse, lMouseInputReport.TimeStamp, lMouseInputReport.Wheel );

                            lPreviewWheel.RoutedEvent = InputManager.PreviewMouseWheelEvent;

                            this.PushInput( lPreviewWheel );
                        }

                        // Raw --> PreviewMouseDown
                        if 
                            ( (lActions & MouseActions.Button1Press) == MouseActions.Button1Press )
                        {
                            Core.MouseButtonEventArgs lPreviewDown = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Left );

                            lPreviewDown.RoutedEvent = InputManager.PreviewMouseDownEvent;
                            this.PushInput( lPreviewDown );
                        }

                        // Raw --> PreviewMouseUp
                        if 
                            ( (lActions & MouseActions.Button1Release) == MouseActions.Button1Release )
                        {
                            Core.MouseButtonEventArgs lPreviewUp = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Left );

                            lPreviewUp.RoutedEvent = InputManager.PreviewMouseUpEvent;
                            this.PushInput( lPreviewUp );
                        }

                        // Raw --> PreviewMouseDown
                        if 
                            ( (lActions & MouseActions.Button2Press) == MouseActions.Button2Press )
                        {
                            Core.MouseButtonEventArgs lPreviewDown = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Right );

                            lPreviewDown.RoutedEvent = InputManager.PreviewMouseDownEvent;
                            this.PushInput( lPreviewDown );
                        }

                        // Raw --> PreviewMouseUp
                        if 
                            ( (lActions & MouseActions.Button2Release) == MouseActions.Button2Release )
                        {
                            Core.MouseButtonEventArgs lPreviewUp = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Right );

                            lPreviewUp.RoutedEvent = InputManager.PreviewMouseUpEvent;
                            this.PushInput( lPreviewUp );
                        }

                        // Raw --> PreviewMouseDown
                        if 
                            ( (lActions & MouseActions.Button3Press) == MouseActions.Button3Press )
                        {
                            Core.MouseButtonEventArgs lPreviewDown = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Middle );

                            lPreviewDown.RoutedEvent = InputManager.PreviewMouseDownEvent;
                            this.PushInput( lPreviewDown );
                        }

                        // Raw --> PreviewMouseUp
                        if 
                            ( (lActions & MouseActions.Button3Release) == MouseActions.Button3Release )
                        {
                            Core.MouseButtonEventArgs lPreviewUp = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Middle );

                            lPreviewUp.RoutedEvent = InputManager.PreviewMouseUpEvent;
                            this.PushInput( lPreviewUp );
                        }

                        // Raw --> PreviewMouseDown
                        if 
                            ( (lActions & MouseActions.Button4Press) == MouseActions.Button4Press )
                        {
                            Core.MouseButtonEventArgs lPreviewDown = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Button1 );

                            lPreviewDown.RoutedEvent = InputManager.PreviewMouseDownEvent;
                            this.PushInput( lPreviewDown );
                        }

                        // Raw --> PreviewMouseUp
                        if 
                            ( (lActions & MouseActions.Button4Release) == MouseActions.Button4Release )
                        {
                            Core.MouseButtonEventArgs lPreviewUp = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Button1 );

                            lPreviewUp.RoutedEvent = InputManager.PreviewMouseUpEvent;
                            this.PushInput( lPreviewUp );
                        }

                        // Raw --> PreviewMouseDown
                        if 
                            ( (lActions & MouseActions.Button5Press) == MouseActions.Button5Press )
                        {
                            Core.MouseButtonEventArgs lPreviewDown = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Button2 );

                            lPreviewDown.RoutedEvent = InputManager.PreviewMouseDownEvent;
                            this.PushInput( lPreviewDown );
                        }

                        // Raw --> PreviewMouseUp
                        if 
                            ( (lActions & MouseActions.Button5Release) == MouseActions.Button5Release )
                        {
                            Core.MouseButtonEventArgs lPreviewUp = new Core.MouseButtonEventArgs( this.Mouse, lMouseInputReport.TimeStamp, MouseButton.Button2 );

                            lPreviewUp.RoutedEvent = InputManager.PreviewMouseUpEvent;
                            this.PushInput( lPreviewUp );
                        }

                        // Raw --> PreviewMouseMove
                        if 
                            ( (lActions & MouseActions.AbsoluteMove) == MouseActions.AbsoluteMove )
                        {
                            Core.MouseEventArgs lPreviewMove = new Core.MouseEventArgs( this.Mouse, lMouseInputReport.TimeStamp );

                            lPreviewMove.RoutedEvent = InputManager.PreviewMouseMoveEvent;
                            this.PushInput( lPreviewMove );
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the non redundant actions for the input.
        /// </summary>
        /// <param name="pEventArgs">THe input event arguments.</param>
        /// <returns>The set of mouse actions.</returns>
        private MouseActions GetNonRedundantActions(NotifyInputEventArgs pEventArgs)
        {
            MouseActions lActions = MouseActions.None;
            
            object lData = pEventArgs.Input.GetData( this.mNonRedundantActionsTag );
            if 
                ( lData != null )
            {
                lActions = (MouseActions)lData;
            }

            return lActions;
        }

        /// <summary>
        /// Global hit test, first hit testing to a window and then down to an element.
        /// </summary>
        /// <param name="pClientPoint">The mouse point in client space.</param>
        /// <param name="pInputSource">The presentation source the input is for.</param>
        /// <returns>The hit element if any, null otherwise.</returns>
        internal static IInputElement GlobalHitTest(Point pClientPoint, PresentationSource pInputSource)
        {
            return GlobalHitTest( true, pClientPoint, pInputSource );
        }

        /// <summary>
        /// Global hit test, first hit testing to a window and then down to an element.
        /// </summary>
        /// <param name="pClientUnits">The flag indicating whether the point is in client space or not.</param>
        /// <param name="pPoint">The mouse point.</param>
        /// <param name="pInputSource">The presentation source the input is for.</param>
        /// <returns>The hit element if any, null otherwise.</returns>
        internal static IInputElement GlobalHitTest(bool pClientUnits, Point pPoint, PresentationSource pInputSource)
        {
            IInputElement lEnabledHit;
            IInputElement lOriginalHit;
            GlobalHitTest( pClientUnits, pPoint, pInputSource, out lEnabledHit, out lOriginalHit );

            return lEnabledHit;
        }

        /// <summary>
        /// Global hit test, first hit testing to a window and then down to an element.
        /// </summary>
        /// <param name="pClientUnits">The flag indicating whether the point is in client space or not.</param>
        /// <param name="pPoint">The mouse point.</param>
        /// <param name="pInputSource">The presentation source the input is for.</param>
        /// <param name="pEnabledHit">The first enabled element that is hit.</param>
        /// <param name="pOriginalHit">The first element that is hit whatever it is enabled or not.</param>
        private static void GlobalHitTest(bool pClientUnits, Point pPoint, PresentationSource pInputSource, out IInputElement pEnabledHit, out IInputElement pOriginalHit)
        {
            pEnabledHit = pOriginalHit = null;

            Point lClientPoint = pClientUnits ? pPoint : PointUtil.RootToClient( pPoint, pInputSource );
            if 
                ( pInputSource.Source != null )
            {
                // A window manager will have to be done to support multiple windows hit test but for now only one exist and test at least if in bounds.
                Point lScreenPoint = PointUtil.ClientToScreen( lClientPoint, pInputSource );
                if ( pInputSource.Source.Bounds.Contains( (int)lScreenPoint.X, (int)lScreenPoint.Y ) )
                {
                    // Perform a local hit-test within this visual manager.
                    LocalHitTest( true, lClientPoint, pInputSource, out pEnabledHit, out pOriginalHit );
                }
            }
        }

        /// <summary>
        /// Local hit test just hit testing down to an element.
        /// </summary>
        /// <param name="pClientPoint">THe point in client space.</param>
        /// <param name="pInputSource">The input source.</param>
        /// <returns>The hit element if any, null otherwise.</returns>
        internal static IInputElement LocalHitTest(Point pClientPoint, PresentationSource pInputSource)
        {
            return LocalHitTest( true, pClientPoint, pInputSource );
        }

        /// <summary>
        /// Local hit test just hit testing down to an element.
        /// </summary>
        /// <param name="pClientUnits">The flag indicating whether the point is in client space or not.</param>
        /// <param name="pPoint">The mouse point.</param>
        /// <param name="pInputSource">The input source.</param>
        /// <returns>The hit element if any, null otherwise.</returns>
        internal static IInputElement LocalHitTest(bool pClientUnits, Point pPoint, PresentationSource pInputSource)
        {
            IInputElement lEnabledHit;
            IInputElement lOriginalHit;
            LocalHitTest( pClientUnits, pPoint, pInputSource, out lEnabledHit, out lOriginalHit );

            return lEnabledHit;
        }

        /// <summary>
        /// Local hit test just hit testing down to an element.
        /// </summary>
        /// <param name="pClientUnits">The flag indicating whether the point is in client space or not.</param>
        /// <param name="pPoint">The mouse point.</param>
        /// <param name="pInputSource">The input source.</param>
        /// <param name="pEnabledHit">The first enabled element that is hit.</param>
        /// <param name="pOriginalHit">The first element that is hit whatever it is enabled or not.</param>
        private static void LocalHitTest(bool pClientUnits, Point pPoint, PresentationSource pInputSource, out IInputElement pEnabledHit, out IInputElement pOriginalHit)
        {
            pEnabledHit = pOriginalHit = null;

            // Hit-test starting from the root UIElement.
            if 
                ( pInputSource != null )
            {
                UIElement lRoot = pInputSource.RootVisual as UIElement;
                if 
                    ( lRoot != null )
                {
                    Point lPointInRootSpace = pClientUnits ? PointUtil.ClientToRoot( pPoint, pInputSource ) : pPoint;
                    lRoot.InputHitTest( lPointInRootSpace, out pEnabledHit, out pOriginalHit );
                }
            }
        }

        /// <summary>
        /// Checks whether two points are close enough to be assumed equal.
        /// </summary>
        /// <param name="pFirst">The first point to test.</param>
        /// <param name="pSecond">The second to test against the first.</param>
        /// <returns>True if close, false otherwise.</returns>
        private bool ArePointsClose(Point pFirst, Point pSecond)
        {
            return DoubleUtil.AreClose( pFirst.X, pSecond.X ) &&
                   DoubleUtil.AreClose( pFirst.Y, pSecond.Y );
        }

        /// <summary>
        /// Checks whethetr the given point is equivalent to a previously clicked one or not.
        /// </summary>
        /// <param name="pNewPoint">The new point to test.</param>
        /// <returns>True if points are equivalent, false otherwise.</returns>
        internal bool IsSameSpot(Point pNewPoint)
        {
            int lDoubleClickDeltaX = this.mDoubleClickDeltaX;
            int lDoubleClickDeltaY = this.mDoubleClickDeltaY;

            // Is the delta coordinates of this click close enough to the last click?
            return System.Math.Abs( pNewPoint.X - this.mLastClick.X ) < lDoubleClickDeltaX &&
                   System.Math.Abs( pNewPoint.Y - this.mLastClick.Y ) < lDoubleClickDeltaY;
        }

        /// <summary>
        /// Computes the new click count regarding to a given button and timestamp at a supplied position.
        /// </summary>
        /// <param name="pButton">The involved button.</param>
        /// <param name="pTimeStamp">The click timestamp.</param>
        /// <param name="pDownPoint">The new mouse position to test.</param>
        /// <returns>The click count.</returns>
        internal int CalculateClickCount(MouseButton pButton, int pTimeStamp, Point pDownPoint)
        {
            // How long since the last click?
            int lTimeSpan = pTimeStamp - this.mLastClickTime;

            int lDoubleClickDeltaTime = this.mDoubleClickDeltaTime;

            // Is the delta coordinates of this click close enough to the last click?
            bool lIsSameSpot = this.IsSameSpot( pDownPoint );

            // Is this the same mouse pButton as the last click?
            bool lIsSameButton = this.mLastButton == pButton;

            // Now check everything to see if this is a multi-click.
            if ( lTimeSpan < lDoubleClickDeltaTime && 
                 lIsSameSpot && 
                 lIsSameButton )
            {
                // Yes, increment the count
                return this.mClickCount + 1;
            }
            else
            {
                // No, not a multi-click.
                return 1;
            }
        }

        /// <summary>
        /// Validates for capture a Content Element.
        /// </summary>
        /// <param name="pElement">The element to validate.</param>
        /// <returns>True if validated, false otherwise.</returns>
        private bool ValidateUIElementForCapture(UIElement pElement)
        {
            if 
                ( pElement.IsEnabled == false )
            {
                return false;
            }

            if 
                ( pElement.IsVisible == false )
            {
                return false;
            }

            if 
                ( pElement.IsHitTestVisible == false )
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validates for capture a Content Element.
        /// </summary>
        /// <param name="pElement">The element to validate.</param>
        /// <returns>True if validated, false otherwise.</returns>
        private bool ValidateContentElementForCapture(ContentElement pElement)
        {
            if 
                ( pElement.IsEnabled == false )
            {
                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Validates for capture a Content Element.
        /// </summary>
        /// <param name="pVisual">The visual to validate.</param>
        /// <returns>True if validated, false otherwise.</returns>
        private bool ValidateVisualForCapture(DependencyObject pVisual)
        {
            if 
                ( pVisual == null )
            {
                return false;
            }

            IUIService lUIService = ServiceManager.Instance.GetService<IUIService>();
            
            if 
                ( lUIService.PresentationSource == null )
            {
                return false;
            }

            DependencyObject lRoot = InputElement.GetRootVisual( pVisual );
            if 
                ( lRoot != lUIService.PresentationSource.RootVisual )
            {
                return false;
            }

            return true;
        }

        #region Methods Helpers

        /// <summary>
        /// Adds a delegate on the PreviewKeyDown attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewKeyDownHandler(DependencyObject pElement, KeyEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewKeyDownEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewKeyDownEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the PreviewKeyDown attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewKeyDownHandler(DependencyObject pElement, KeyEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewKeyDownEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewKeyDownEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the KeyDown attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddKeyDownHandler(DependencyObject pElement, KeyEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( KeyDownEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( KeyDownEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the KeyDown attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveKeyDownHandler(DependencyObject pElement, KeyEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( KeyDownEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( KeyDownEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the PreviewKeyUp attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewKeyUpHandler(DependencyObject pElement, KeyEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewKeyUpEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewKeyUpEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the PreviewKeyUp attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewKeyUpHandler(DependencyObject pElement, KeyEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewKeyUpEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewKeyUpEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the KeyUp attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddKeyUpHandler(DependencyObject pElement, KeyEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( KeyUpEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( KeyUpEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the KeyUp attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveKeyUpHandler(DependencyObject pElement, KeyEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( KeyUpEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( KeyUpEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the PreviewMouseMove attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewMouseMoveHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewMouseMoveEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewMouseMoveEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the PreviewMouseMove attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewMouseMoveHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewMouseMoveEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewMouseMoveEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the MouseMove attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddMouseMoveHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( MouseMoveEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( MouseMoveEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the MouseMove attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveMouseMoveHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( MouseMoveEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( MouseMoveEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the PreviewMouseDownOutsideCapturedElement attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewMouseDownOutsideCapturedElementHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewMouseDownOutsideCapturedElementEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewMouseDownOutsideCapturedElementEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the MouseDownOutsideCapturedElement attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewMouseDownOutsideCapturedElementHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewMouseDownOutsideCapturedElementEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewMouseDownOutsideCapturedElementEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the MouseUpOutsideCapturedElement attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewMouseUpOutsideCapturedElementHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewMouseUpOutsideCapturedElementEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewMouseUpOutsideCapturedElementEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the MouseUpOutsideCapturedElement attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewMouseUpOutsideCapturedElementHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewMouseUpOutsideCapturedElementEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewMouseUpOutsideCapturedElementEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the PreviewMouseDown attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewMouseDownHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewMouseDownEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewMouseDownEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the PreviewMouseDown attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewMouseDownHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewMouseDownEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewMouseDownEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the MouseDown attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddMouseDownHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( MouseDownEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( MouseDownEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the MouseDown attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveMouseDownHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( MouseDownEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( MouseDownEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the PreviewMouseUp attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewMouseUpHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewMouseUpEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewMouseUpEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the PreviewMouseUp attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewMouseUpHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewMouseUpEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewMouseUpEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the MouseUp attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddMouseUpHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( MouseUpEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( MouseUpEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the MouseUp attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveMouseUpHandler(DependencyObject pElement, MouseButtonEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( MouseUpEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( MouseUpEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the PreviewMouseWheel attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewMouseWheelHandler(DependencyObject pElement, MouseWheelEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewMouseWheelEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewMouseWheelEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the PreviewMouseWheel attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewMouseWheelHandler(DependencyObject pElement, MouseWheelEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewMouseWheelEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewMouseWheelEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the MouseWheel attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddMouseWheelHandler(DependencyObject pElement, MouseWheelEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( MouseWheelEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( MouseWheelEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the MouseWheel attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveMouseWheelHandler(DependencyObject pElement, MouseWheelEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( MouseWheelEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( MouseWheelEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the MouseEnter attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddMouseEnterHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( MouseEnterEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( MouseEnterEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the MouseEnter attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveMouseEnterHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( MouseEnterEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( MouseEnterEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the MouseLeave attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddMouseLeaveHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( MouseLeaveEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( MouseLeaveEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the MouseLeave attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveMouseLeaveHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( MouseLeaveEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( MouseLeaveEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the LostMouseCapture attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddLostMouseCaptureHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( LostMouseCaptureEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( LostMouseCaptureEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the LostMouseCapture attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveLostMouseCaptureHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( LostMouseCaptureEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( LostMouseCaptureEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the GotMouseCapture attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddGotMouseCaptureHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( GotMouseCaptureEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( GotMouseCaptureEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the GotMouseCapture attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveGotMouseCaptureHandler(DependencyObject pElement, MouseEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( GotMouseCaptureEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( GotMouseCaptureEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the PreviewGotKeyboardFocus attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewGotKeyboardFocusHandler(DependencyObject pElement, KeyboardFocusChangedEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewGotKeyboardFocusEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewGotKeyboardFocusEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the PreviewGotKeyboardFocus attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewGotKeyboardFocusHandler(DependencyObject pElement, KeyboardFocusChangedEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewGotKeyboardFocusEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewGotKeyboardFocusEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the GotKeyboardFocus attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddGotKeyboardFocusHandler(DependencyObject pElement, KeyboardFocusChangedEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( GotKeyboardFocusEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( GotKeyboardFocusEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the GotKeyboardFocus attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveGotKeyboardFocusHandler(DependencyObject pElement, KeyboardFocusChangedEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( GotKeyboardFocusEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( GotKeyboardFocusEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the PreviewLostKeyboardFocus attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddPreviewLostKeyboardFocusHandler(DependencyObject pElement, KeyboardFocusChangedEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( PreviewLostKeyboardFocusEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( PreviewLostKeyboardFocusEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the PreviewLostKeyboardFocus attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemovePreviewLostKeyboardFocusHandler(DependencyObject pElement, KeyboardFocusChangedEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( PreviewLostKeyboardFocusEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( PreviewLostKeyboardFocusEvent, pDelegate );
            }
        }

        /// <summary>
        /// Adds a delegate on the LostKeyboardFocus attached event
        /// </summary>
        /// <param name="pElement">The Element to add a delegate for.</param>
        /// <param name="pDelegate">The delegate to add.</param>
        public static void AddLostKeyboardFocusHandler(DependencyObject pElement, KeyboardFocusChangedEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).AddHandler( LostKeyboardFocusEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).AddHandler( LostKeyboardFocusEvent, pDelegate );
            }
        }

        /// <summary>
        /// Removes a delegate on the LostKeyboardFocus attached event
        /// </summary>
        /// <param name="pElement">The Element to remove a delegate for.</param>
        /// <param name="pDelegate">The delegate to remove.</param>
        public static void RemoveLostKeyboardFocusHandler(DependencyObject pElement, KeyboardFocusChangedEventDelegate pDelegate)
        {
            if
                ( pElement is UIElement )
            {
                (pElement as UIElement).RemoveHandler( LostKeyboardFocusEvent, pDelegate );
            }
            else if
                ( pElement is ContentElement )
            {
                (pElement as ContentElement).RemoveHandler( LostKeyboardFocusEvent, pDelegate );
            }
        }

        #endregion Methods Helpers

        #endregion Methods
    }
}
