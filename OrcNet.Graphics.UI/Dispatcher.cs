﻿using OrcNet.Core.Thread;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace OrcNet.Graphics.UI
{
    /// <summary>
    /// Dispatcher class definition in charge of pushing
    /// new work into the UI thread.
    /// </summary>
    public sealed class Dispatcher : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the dispatcher has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the flag indicating whether the thread that dispatcher is linked with must be stopped or not.
        /// </summary>
        private bool mStop;

        /// <summary>
        /// Stores the thread that dispatcher is associated with.
        /// </summary>
        private Thread mThread;

        /// <summary>
        /// Stores the set of pending actions to execute.
        /// </summary>
        private ConcurrentBag<Action> mPendingActions;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the dispatcher's name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mThread.Name;
            }
        }

        /// <summary>
        /// Gets the dispatcher's thread unique Identifier.
        /// </summary>
        internal int ThreadId
        {
            get
            {
                return this.mThread.ManagedThreadId;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Dispatcher"/> class.
        /// </summary>
        /// <param name="pName">The dispatcher's name</param>
        public Dispatcher(string pName)
        {
            this.mIsDisposed = false;
            this.mStop = false;
            this.mThread = ThreadFactory.CreateThread( pName, this.Run );
            this.mPendingActions = new ConcurrentBag<Action>();
            this.mThread.Start();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new command to execut by that dispatcher.
        /// </summary>
        /// <param name="pAction">The new work to do.</param>
        public void Invoke(Action pAction)
        {
            if
                ( this.mStop )
            {
                return;
            }

            this.mPendingActions.Add( pAction );
        }

        /// <summary>
        /// Thread callback.
        /// </summary>
        private void Run()
        {
            while
                ( this.mStop == false )
            {
                if
                    ( this.mPendingActions.IsEmpty )
                {
                    Thread.Sleep( 100 );
                }
                else
                {
                    Action lAction;
                    while
                        ( this.mPendingActions.TryTake( out lAction ) )
                    {
                        lAction();
                    }

                    Thread.Sleep( 100 );
                }
            }
        }

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if
                ( this.mIsDisposed == false )
            {
                this.mStop = true;
                this.mThread.Join();
                this.mThread = null;

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
