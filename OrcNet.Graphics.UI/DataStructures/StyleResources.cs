﻿using OrcNet.Graphics.UI.Core;
using OrcNet.Graphics.UI.Xoml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace OrcNet.Graphics.UI.DataStructures
{
    /// <summary>
    /// Style resource(s) cache class definition.
    /// </summary>
    public class StyleResources : IDictionary
    {
        #region Fields

        /// <summary>
        /// Stores the style resource(s) flags.
        /// </summary>
        private ResourceFlags mFlags;

        /// <summary>
        /// Stores the source Uri.
        /// </summary>
        private Uri mSource;

        /// <summary>
        /// Stores the contextual object of the style resource cache.
        /// </summary>
        private WeakReference mContext;

        /// <summary>
        /// Stores the style resource(s) cache.
        /// </summary>
        private Hashtable mInnerCache;

        /// <summary>
        /// Stores the set of merged style resources.
        /// </summary>
        private ObservableCollection<StyleResources> mMergedStyleResources;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the contextual resource cache object. (usually the owner)
        /// </summary>
        private DependencyObject Context
        {
            get
            {
                if ( this.mContext != null )
                {
                    return this.mContext.Target as DependencyObject;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the resource cache is initialized or not.
        /// </summary>
        private bool IsInitialized
        {
            get
            {
                return ReadFlag(ResourceFlags.IsInitialized);
            }
            set
            {
                WriteFlag(ResourceFlags.IsInitialized, value);
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the resource cache needs to be initialized or not.
        /// </summary>
        private bool IsInitializePending
        {
            get
            {
                return ReadFlag(ResourceFlags.IsInitializePending);
            }
            set
            {
                WriteFlag(ResourceFlags.IsInitializePending, value);
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the resource cache is the theme or not.
        /// </summary>
        private bool IsThemeDictionary
        {
            get
            {
                return ReadFlag(ResourceFlags.IsThemeDictionary);
            }
            set
            {
                if 
                    ( this.IsThemeDictionary != value )
                {
                    WriteFlag(ResourceFlags.IsThemeDictionary, value);
                    if 
                        ( value )
                    {
                        //SealValues();
                    }

                    if ( this.mMergedStyleResources != null )
                    {
                        int lMergedCount = this.mMergedStyleResources.Count;
                        for ( int lCurr = 0; lCurr < lMergedCount; lCurr++ )
                        {
                            this.mMergedStyleResources[ lCurr ].IsThemeDictionary = value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the resource cache contains implicit styles or not.
        /// </summary>
        internal bool HasImplicitStyles
        {
            get
            {
                return ReadFlag(ResourceFlags.HasImplicitStyles);
            }
            set
            {
                WriteFlag(ResourceFlags.HasImplicitStyles, value);
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the resource cache can be accessed across threads or not.
        /// </summary>
        internal bool CanBeAccessedAcrossThreads
        {
            get
            {
                return ReadFlag( ResourceFlags.CanBeAccessedAcrossThreads );
            }
            set
            {
                WriteFlag( ResourceFlags.CanBeAccessedAcrossThreads, value );
            }
        }

        /// <summary>
        /// Gets the set of merged dictionaries of style resources.
        /// </summary>
        public Collection<StyleResources> MergedDictionaries
        {
            get
            {
                if ( this.mMergedStyleResources == null )
                {
                    this.mMergedStyleResources = new ResourceDictionaryCollection(this);
                    this.mMergedStyleResources.CollectionChanged += OnMergedDictionariesChanged;
                }

                return this.mMergedStyleResources;
            }
        }

        #region Properties IDictionary

        public object this[object key]
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int Count
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsFixedSize
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsSynchronized
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public ICollection Keys
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public object SyncRoot
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public ICollection Values
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion Properties IDictionary

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StyleResources"/> class.
        /// </summary>
        public StyleResources()
        {
            this.mInnerCache = new Hashtable();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Copies the resource cache into the supplied array.
        /// </summary>
        /// <param name="pArray">The array to fill with resource(s)</param>
        /// <param name="pStartingIndex">THe array to fill starting index.</param>
        public void CopyTo(DictionaryEntry[] pArray, int pStartingIndex)
        {
            if 
                ( this.CanBeAccessedAcrossThreads )
            {
                lock 
                    ( (this as ICollection).SyncRoot )
                {
                    this.CopyToWithoutLock( pArray, pStartingIndex );
                }
            }
            else
            {
                this.CopyToWithoutLock( pArray, pStartingIndex );
            }
        }

        /// <summary>
        /// Copies the resource cache into the supplied array.
        /// </summary>
        /// <param name="pArray">The array to fill with resource(s)</param>
        /// <param name="pStartingIndex">THe array to fill starting index.</param>
        private void CopyToWithoutLock(DictionaryEntry[] pArray, int pStartingIndex)
        {
            if ( pArray == null )
            {
                throw new ArgumentNullException("Null array!!!");
            }

            this.mInnerCache.CopyTo( pArray, pStartingIndex );

            int lCount = pStartingIndex + this.Count;
            for ( int lCurr = pStartingIndex; lCurr < lCount; lCurr++ )
            {
                DictionaryEntry lEntry = pArray[ lCurr ];
                object lValue = lEntry.Value;
                bool lCanCache;
                OnGettingValuePrivate( lEntry.Key, ref lValue, out lCanCache );
                lEntry.Value = lValue; // refresh the entry value in case it was changed in the previous call
            }
        }

        #region Methods IDictionary

        public void Add(object key, object value)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(object key)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public IDictionaryEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void Remove(object key)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion Methods IDictionary

        /// <summary>
        /// Modify the given style cache flag.
        /// </summary>
        /// <param name="pFlag">The flag to modify.</param>
        /// <param name="pEnable">The flag indicating whether the resource flag must be enabled or disabled.</param>
        private void WriteFlag(ResourceFlags pFlag, bool pEnable)
        {
            if 
                ( pEnable )
            {
                this.mFlags |= pFlag;
            }
            else
            {
                this.mFlags &= ~pFlag;
            }
        }

        /// <summary>
        /// Reads the given style cache flag.
        /// </summary>
        /// <param name="pFlag">The flag to look for.</param>
        /// <returns>True if the flag is enabled, false otherwise.</returns>
        private bool ReadFlag(ResourceFlags pFlag)
        {
            return (this.mFlags & pFlag) != 0;
        }

        private bool CanCache(KeyRecord keyRecord, object value)
        {
            if (keyRecord.SharedSet)
            {
                return keyRecord.Shared;
            }
            else
            {
                return true;
            }
        }

        private void OnGettingValuePrivate(object key, ref object value, out bool canCache)
        {
            OnGettingValue(key, ref value, out canCache);

            if (key != null && canCache)
            {
                if ( object.Equals( this.mInnerCache[key], value) == false )
                {
                    // cache the revised value, after setting its InheritanceContext
                    if ( this.Context != null )
                    {
                        AddInheritanceContext( this.Context, value);
                    }

                    this.mInnerCache[key] = value;
                }
            }
        }

        protected virtual void OnGettingValue(object key, ref object value, out bool canCache)
        {
            KeyRecord keyRecord = value as KeyRecord;

            // If the value is not a key record then
            // it has already been realized, is not deferred and is a "ready to go" value.
            if (keyRecord == null)
            {
                canCache = true;
                return;   /* Not deferred content */
            }

            Debug.Assert(_numDefer > 0, "The stream was closed before all deferred content was loaded.");

            // We want to return null if a resource asks for itself. It should return null
            //  <Style x:Key={x:Type Button} BasedOn={StaticResource {x:Type Button}}/> should not find itself
            if (_deferredLocationList.Contains(keyRecord))
            {
                canCache = false;
                value = null;
                return; /* Not defered content */
            }

            _deferredLocationList.Add(keyRecord);

            try
            {
                value = CreateObject(keyRecord);

            }
            finally
            {
                
            }

            _deferredLocationList.Remove(keyRecord);

            if (key != null)
            {
                canCache = CanCache(keyRecord, value);
                if (canCache)
                {
                    // Seal styles and templates within App and Theme dictionary
                    SealValue(value);

                    _numDefer--;

                    if (_numDefer == 0)
                    {
                        CloseReader();
                    }
                }
            }
            else
            {
                canCache = true;
            }
        }

        private void OnMergedDictionariesChanged(object pSender, NotifyCollectionChangedEventArgs e)
        {
            List<StyleResources> lOldResources = null;
            List<StyleResources> lNewResources = null;
            ResourceDictionary mergedDictionary;
            ResourcesChangeInfo info;

            if (e.Action != NotifyCollectionChangedAction.Reset)
            {
                Invariant.Assert(
                    (e.NewItems != null && e.NewItems.Count > 0) ||
                    (e.OldItems != null && e.OldItems.Count > 0),
                    "The NotifyCollectionChanged event fired when no dictionaries were added or removed");


                // If one or more resource dictionaries were removed we
                // need to remove the owners they were given by their
                // parent ResourceDictionary.

                if (e.Action == NotifyCollectionChangedAction.Remove
                    || e.Action == NotifyCollectionChangedAction.Replace)
                {
                    lOldResources = new List<ResourceDictionary>(e.OldItems.Count);

                    for (int i = 0; i < e.OldItems.Count; i++)
                    {
                        mergedDictionary = (ResourceDictionary)e.OldItems[i];
                        lOldResources.Add(mergedDictionary);

                        RemoveParentOwners(mergedDictionary);
                    }
                }

                // If one or more resource dictionaries were added to the merged
                // dictionaries collection we need to send down the parent
                // ResourceDictionary's owners.

                if (e.Action == NotifyCollectionChangedAction.Add
                    || e.Action == NotifyCollectionChangedAction.Replace)
                {
                    lNewResources = new List<ResourceDictionary>(e.NewItems.Count);

                    for (int i = 0; i < e.NewItems.Count; i++)
                    {
                        mergedDictionary = (ResourceDictionary)e.NewItems[i];
                        lNewResources.Add(mergedDictionary);

                        // If the merged dictionary HasImplicitStyle mark the outer dictionary the same.
                        if (!HasImplicitStyles && mergedDictionary.HasImplicitStyles)
                        {
                            HasImplicitStyles = true;
                        }

                        // If the parent dictionary is a theme dictionary mark the merged dictionary the same.
                        if (IsThemeDictionary)
                        {
                            mergedDictionary.IsThemeDictionary = true;
                        }

                        PropagateParentOwners(mergedDictionary);
                    }
                }

                info = new ResourcesChangeInfo(lOldResources, lNewResources, false, false, null);
            }
            else
            {
                // Case when MergedDictionary collection is cleared
                info = ResourcesChangeInfo.CatastrophicDictionaryChangeInfo;
            }

            // Notify the owners of the change and fire
            // invalidation if already initialized

            NotifyOwners(info);
        }

        #endregion Methods

        #region Inner structs

        /// <summary>
        /// Style resource flags enumeration.
        /// </summary>
        private enum ResourceFlags : byte
        {
            /// <summary>
            /// Resource(s) initialized.
            /// </summary>
            IsInitialized = 0x01,

            /// <summary>
            /// Resource(s) waiting for initialization.
            /// </summary>
            IsInitializePending = 0x02,

            /// <summary>
            /// Resource(s) read only.
            /// </summary>
            IsReadOnly = 0x04,

            /// <summary>
            /// Resource(s) intended for the theme.
            /// </summary>
            IsThemeDictionary = 0x08,

            /// <summary>
            /// Resource(s) contain(s) implicit styles.
            /// </summary>
            HasImplicitStyles = 0x10,

            /// <summary>
            /// Resource(s) can be accessed across multiple threads.
            /// </summary>
            CanBeAccessedAcrossThreads = 0x20
        }

        #endregion Inner structs
    }
}
