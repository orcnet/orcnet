﻿using OrcNet.Graphics.UI.Maths;
using System.Collections.ObjectModel;

namespace OrcNet.Graphics.UI.DataStructures
{
    /// <summary>
    /// Collection of transformations class definition.
    /// </summary>
    public sealed class TransformCollection : ObservableCollection<ATransform>
    {
        #region Fields

        /// <summary>
        /// Stores the transform collection empty.
        /// </summary>
        private static TransformCollection sEmpty;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the transform collection empty.
        /// </summary>
        internal static TransformCollection Empty
        {
            get
            {
                if 
                    ( sEmpty == null )
                {
                    sEmpty = new TransformCollection();
                }

                return sEmpty;
            }
        }

        #endregion Properties
    }
}
