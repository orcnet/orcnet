﻿using OrcNet.Graphics.UI.Framework;
using OrcNet.Graphics.UI.Threading;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace OrcNet.Graphics.UI.Style
{
    /// <summary>
    /// Base styling class definition. Simplified regarding to WPF one.
    /// </summary>
    public class Style : ADispatcherObject
    {
        #region Constants

        /// <summary>
        /// Stores the target type identifier.
        /// </summary>
        private const int cTargetTypeID = 0x01;

        /// <summary>
        /// Stores the style identifier that one is based on.
        /// </summary>
        internal const int cBasedOnID = 0x02;

        #endregion Constants

        #region Fields

        /// <summary>
        /// Stores the style instance count.
        /// </summary>
        private static int sStyleInstanceCount = 0;

        /// <summary>
        /// Stores the style modification flags.
        /// </summary>
        private int mModificationFlag = 0;

        /// <summary>
        /// Stores the flag indicating whether the style can be modified or not.
        /// </summary>
        private bool mCanChanged = true;

        ///// <summary>
        ///// STores the Xaml description.
        ///// </summary>
        //private XElement mXamlDescription;

        /// <summary>
        /// Stores the style components by name (extracted from the description).
        /// </summary>
        private Dictionary<string, object> mStyleComponentsByName;

        /// <summary>
        /// Stores the target type the style is applied to.
        /// </summary>
        private Type mTargetType;

        /// <summary>
        /// Stores the style that one is based on.
        /// </summary>
        private Style mBasedOn;

        /// <summary>
        /// Stores the style global index.
        /// </summary>
        internal int GlobalIndex;

        /// <summary>
        /// Stores the default style's target type. (that is all framework elements).
        /// </summary>
        internal static readonly Type sDefaultTargetType = typeof(IFrameworkInputElement);

        /// <summary>
        /// Synchronizer.
        /// </summary>
        internal static object sSyncRoot = new object();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicating whether the style can be modified or not.
        /// </summary>
        public bool IsSealed
        {
            get
            {
                return this.mCanChanged == false;
            }
        }

        /// <summary>
        /// Gets the target type the style is applied to.
        /// </summary>
        public Type TargetType
        {
            get
            {
                return this.mTargetType;
            }

            set
            {
                if ( this.mCanChanged == false )
                {
                    throw new InvalidOperationException("The style is sealed so cannot change...");
                }

                if ( value == null )
                {
                    throw new ArgumentNullException("value");
                }

                if ( typeof(FrameworkElement).IsAssignableFrom(value) == false &&
                     typeof(FrameworkContentElement).IsAssignableFrom(value) == false &&
                     sDefaultTargetType != value )
                {
                    throw new ArgumentException( string.Format( "The element {0} must be at least a framework elemnt...", value.Name ) );
                }

                this.mTargetType = value;

                this.SetModified(cTargetTypeID);
            }
        }

        /// <summary>
        /// Gets the style that one is based on.
        /// </summary>
        public Style BasedOn
        {
            get
            {
                return this.mBasedOn;
            }
            set
            {
                if ( this.mCanChanged == false )
                {
                    throw new InvalidOperationException( "The style is sealed so cannot change..." );
                }

                if ( value == this )
                {
                    throw new ArgumentException( "The style cannot be based on itself..." );
                }

                this.mBasedOn = value;

                this.SetModified( cBasedOnID );
            }
        }

        /// <summary>
        /// Gets or sets the style components by name (extracted from the description).
        /// </summary>
        public Dictionary<string, object> Resources
        {
            get
            {
                return this.mStyleComponentsByName;
            }
            set
            {
                this.mStyleComponentsByName = value;
            }
        }

        /// <summary>
        /// Gets the object name.
        /// NOTE: Size cannot be determined the easy way using SizeOf in .Net
        /// for managed objects, so the user will have to provide this info himself.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;

                return lSize;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="Style"/> class.
        /// </summary>
        static Style()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Style"/> class.
        /// </summary>
        public Style() :
        this( sDefaultTargetType )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Style"/> class.
        /// </summary>
        /// <param name="pTarget">The target type this style is for.</param>
        public Style(Type pTarget) :
        this( pTarget, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Style"/> class.
        /// </summary>
        /// <param name="pTarget">The target type this style is for.</param>
        /// <param name="pBasedOn">The style that one is based on.</param>
        public Style(Type pTarget, Style pBasedOn)
        {
            this.mTargetType = pTarget;
            this.mBasedOn    = pBasedOn;

            this.AssignNextIndex();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the style hash code.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.GlobalIndex;
        }

        /// <summary>
        /// Gets the resource corresponding to the specified key.
        /// </summary>
        /// <param name="pResourceKey">THe resource key.</param>
        /// <returns>The resource.</returns>
        internal object FindResource(string pResourceKey)
        {
            object lResource;
            if ( this.mStyleComponentsByName.TryGetValue( pResourceKey, out lResource ) )
            {
                return lResource;
            }
            else
            {
                // Load it on demand by looking for it in the Xelement having the description.
                                
            }

            return null;
        }

        /// <summary>
        /// Look for the resource container the given resource key is for.
        /// </summary>
        /// <param name="pResourceKey">The resource key to look for its resource container.</param>
        /// <returns>The resource container the resource key is from.</returns>
        internal Dictionary<string, object> FindResourceDictionary(string pResourceKey)
        {
            Debug.Assert( pResourceKey != null, "Argument cannot be null" );

            object lResource;
            if ( this.mStyleComponentsByName.TryGetValue( pResourceKey, out lResource ) )
            {
                return this.mStyleComponentsByName;
            }

            // Could be in the based on style resource dictionary...
            if ( this.mBasedOn != null )
            {
                return this.mBasedOn.FindResourceDictionary( pResourceKey );
            }

            return null;
        }

        /// <summary>
        /// Checks whether the target type this style must be applied to is a valid one or not.
        /// </summary>
        /// <param name="pElement">The element to check.</param>
        internal void CheckTargetType(object pElement)
        {
            // In the most common case TargetType is Default
            // and we can avoid a call to IsAssignableFrom() who's performance is unknown.
            if ( sDefaultTargetType == this.TargetType )
            {
                return;
            }

            Type lElementType = pElement.GetType();
            if ( this.TargetType.IsAssignableFrom( lElementType ) == false )
            {
                throw new InvalidOperationException( string.Format( "Wrong elemnt type this style must be applied to!!!",
                                                     this.TargetType.Name,
                                                     lElementType.Name ) );
            }
        }

        /// <summary>
        /// Lock the style to make it read only.
        /// </summary>
        public void Seal()
        {
            if ( this.mCanChanged == false )
            {
                return;
            }

            if ( this.mTargetType == null )
            {
                throw new InvalidOperationException( "No target type for that style!!!" );
            }

            if ( this.mBasedOn != null )
            {
                if ( sDefaultTargetType != this.mBasedOn.TargetType &&
                     this.mBasedOn.TargetType.IsAssignableFrom( this.mTargetType ) == false )
                {
                    throw new InvalidOperationException( string.Format( "Only base type(s) of the target type of this style can provide a BasedOn style!!! ", this.mTargetType.Name ) );
                }
            }

            this.CheckForCircularBasedOnReferences();

            if ( this.mBasedOn != null )
            {
                this.mBasedOn.Seal();
            }

            this.mCanChanged = false;
        }

        /// <summary>
        /// Checks whether the given flag has been set informing about a style modification.
        /// </summary>
        /// <param name="pFlag">The modification flag.</param>
        /// <returns>True if the flag is set, false otherwise.</returns>
        internal bool IsModified(int pFlag)
        {
            return (pFlag & this.mModificationFlag) != 0;
        }

        /// <summary>
        /// Sets the given flag to inform about a style change.
        /// </summary>
        /// <param name="pFlag">The flag to set.</param>
        private void SetModified(int pFlag)
        {
            this.mModificationFlag |= pFlag;
        }

        /// <summary>
        /// Checks whether the style has circular dependencies with the one(s) it is based on.
        /// </summary>
        private void CheckForCircularBasedOnReferences()
        {
            Stack lBasedOnHierarchy = new Stack(10);
            Style lLatestBasedOn    = this;

            while ( lLatestBasedOn != null )
            {
                if ( lBasedOnHierarchy.Contains( lLatestBasedOn ) )
                {
                    throw new InvalidOperationException( "Circular style dependencies found!!!" );
                }

                // Haven't seen it, push on stack and go to next level.
                lBasedOnHierarchy.Push( lLatestBasedOn );
                lLatestBasedOn = lLatestBasedOn.BasedOn;
            }

            return;
        }

        /// <summary>
        /// Provides the next global index available for that style.
        /// </summary>
        private void AssignNextIndex()
        {
            lock ( sSyncRoot )
            {
                // Setup unqiue global index
                this.GlobalIndex = ++sStyleInstanceCount;
            }
        }

        #endregion Methods
    }
}
