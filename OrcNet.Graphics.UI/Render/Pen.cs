﻿using OrcNet.Core;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Pen class definition used to describe how UI shapes
    /// are striked.
    /// </summary>
    public sealed class Pen : AObservable
    {
        #region Fields

        /// <summary>
        /// Stores the brush used to fill plane.
        /// </summary>
        private ABrush mBrush;

        /// <summary>
        /// Stores the pen dashed style (0 offset means full line).
        /// </summary>
        private DashStyle mStyle;

        /// <summary>
        /// Stores the pen thickness.
        /// </summary>
        private double mThickness;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the brush used to fill plane.
        /// </summary>
        public ABrush Brush
        {
            get
            {
                return this.mBrush;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mBrush, value, "Brush" );
            }
        }

        /// <summary>
        /// Gets or sets the pen dashed style (0 offset means full line).
        /// </summary>
        public DashStyle Style
        {
            get
            {
                return this.mStyle;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mStyle, value, "Style" );
            }
        }

        /// <summary>
        /// Gets or sets the pen thickness.
        /// </summary>
        public double Thickness
        {
            get
            {
                return this.mThickness;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mThickness, value, "Thickness" );
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the Pen style contains Gaps or not.
        /// </summary>
        internal bool DoesNotContainGaps
        {
            get
            {
                if 
                    ( this.Style != null )
                {
                    ushort[] lDashes = this.Style.Dashes;
                    if 
                        ( lDashes != null &&
                          lDashes.Length > 0 )
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion Porperties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Pen"/> class.
        /// </summary>
        public Pen() :
        this( null, 1.0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pen"/> class.
        /// </summary>
        /// <param name="pBrush">The brush used to fill plane.</param>
        /// <param name="pThickness">The pen thickness.</param>
        public Pen(ABrush pBrush, double pThickness) :
        this( pBrush, pThickness, DashStyles.Solid )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pen"/> class.
        /// </summary>
        /// <param name="pBrush">The brush used to fill plane.</param>
        /// <param name="pThickness">The pen thickness.</param>
        /// <param name="pStyle">The pen dashed style (0 offset means full line).</param>
        public Pen(ABrush pBrush, double pThickness, DashStyle pStyle)
        {
            this.mBrush     = pBrush;
            this.mThickness = pThickness;
            this.mStyle     = pStyle;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the flag indicating whether the given Pen is involved in drawn bounds or not.
        /// </summary>
        /// <param name="pPen">The Pen to check.</param>
        /// <returns>True if is involved in drawn bounds, false otherwise.</returns>
        internal static bool ContributesToBounds(Pen pPen)
        {
            return pPen != null && 
                   pPen.Brush != null;
        }

        #endregion Methods
    }
}
