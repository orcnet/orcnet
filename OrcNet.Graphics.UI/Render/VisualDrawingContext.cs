﻿using OrcNet.Core.Logger;
using OrcNet.Graphics.UI.Core;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Drawing context in charge of drawing visual contents.
    /// </summary>
    internal class VisualDrawingContext : RenderDataDrawingContext
    {
        #region Fields

        /// <summary>
        /// Stores the visual that created the context to draw its content.
        /// </summary>
        private AVisual mOwner;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualDrawingContext"/> class.
        /// </summary>
        /// <param name="pOwner">The visual that created the context to draw its content.</param>
        internal VisualDrawingContext(AVisual pOwner)
        {
            this.mOwner = pOwner;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Custom drawing context close.
        /// </summary>
        /// <param name="pData">The render data.</param>
        protected override void CustomClose(RenderData pData)
        {
            if
                ( this.mOwner != null )
            {
                this.mOwner.RenderClose( pData );
            }
            else
            {
                LogManager.Instance.Log( "Cannot render as there is no Visual owner!!!", LogType.ERROR );
            }
        }

        #endregion Methods
    }
}
