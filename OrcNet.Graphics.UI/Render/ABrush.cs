﻿using OrcNet.Core;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Base brush abstract class definition representing how
    /// to fill plane(s).
    /// </summary>
    public abstract class ABrush : AObservable
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ABrush"/> class.
        /// </summary>
        protected ABrush()
        {

        }

        #endregion Constructor
    }
}
