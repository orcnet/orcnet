﻿using System.Collections.Generic;
using OrcNet.Graphics.UI.Maths;
using OrcNet.Core.Logger;
using ImageMagick;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Render data owning Visual element contents to draw.
    /// </summary>
    internal class RenderData : IDrawingContent
    {
        #region Fields

        /// <summary>
        /// Stores the data to render.
        /// </summary>
        private List<KeyValuePair<DrawCommand, object[]>> mBuffer;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderData"/> class.
        /// </summary>
        internal RenderData()
        {
            this.mBuffer = new List<KeyValuePair<DrawCommand, object[]>>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new draw command to the render data set.
        /// </summary>
        /// <param name="pCommand">The draw command.</param>
        /// <param name="pParams">The command parameters.</param>
        public void AddDataToDraw(DrawCommand pCommand, params object[] pParams)
        {
            this.mBuffer.Add( new KeyValuePair<DrawCommand, object[]>( pCommand, pParams ) );
        }

        /// <summary>
        /// Gets the bounding box occupied by the content
        /// </summary>
        /// <returns>The bounding box occupied by the content</returns>
        public Rect GetContentBounds(BoundsDrawingContext pContext)
        {
            // Walks through the data to render to know
            // the exact BBox of the content to draw.
            this.DrawingContextWalk( pContext );

            return pContext.Bounds;
        }

        /// <summary>
        /// Forward the current value of the content to the DrawingContext
        /// methods.
        /// </summary>      
        /// <param name="pContext">the context to forward content to.</param>        
        public void ForwardContent(ABaseDrawingContext pContext)
        {
            this.DrawingContextWalk( pContext );
        }

        /// <summary>
        /// Determines whether or not a point exists within the content
        /// </summary>    
        /// <param name="pPoint">The point to hit-test for.</param>                
        /// <returns>True if the point exists within the content, False otherwise.</returns>        
        public bool HitTestPoint(Point pPoint)
        {
            HitTestDrawingContext lContext = new PointHitTestDrawingContext( pPoint );
            this.DrawingContextWalk( lContext );
            return lContext.IsHit;
        }

        /// <summary>
        /// Walks over the render data and call the appropriate
        /// drawing method.
        /// </summary>
        /// <param name="pContext">The drawing context.</param>
        public void DrawingContextWalk(ABaseDrawingContext pContext)
        {
            int lDrawCount = 0;
            int lDataCount = this.mBuffer.Count;
            while
                ( lDrawCount < lDataCount &&
                  pContext.MustStopWalking == false )
            {
                KeyValuePair<DrawCommand, object[]> lData = this.mBuffer[ lDrawCount ];
                switch 
                    ( lData.Key )
                {
                    case DrawCommand.Line:
                        {
                            object[] lLineParameters = lData.Value;
                            pContext.DrawLine( lLineParameters[ 0 ] as Pen, 
                                               (Point)lLineParameters[ 1 ], 
                                               (Point)lLineParameters[ 2 ] );
                        }
                        break;
                    case DrawCommand.Rectangle:
                        {
                            object[] lRectangleParameters = lData.Value;
                            pContext.DrawRectangle( lRectangleParameters[ 0 ] as ABrush,
                                                    lRectangleParameters[ 1 ] as Pen,
                                                    (Rect)lRectangleParameters[ 2 ] );
                        }
                        break;
                    case DrawCommand.RoundedRectangle:
                        {
                            object[] lRoundedRectangleParameters = lData.Value;
                            pContext.DrawRoundedRectangle( lRoundedRectangleParameters[ 0 ] as ABrush,
                                                           lRoundedRectangleParameters[ 1 ] as Pen,
                                                           (Rect)lRoundedRectangleParameters[ 2 ],
                                                           (double)lRoundedRectangleParameters[ 3 ],
                                                           (double)lRoundedRectangleParameters[ 4 ] );
                        }
                        break;
                    case DrawCommand.Ellipse:
                        {
                            object[] lEllipseParameters = lData.Value;
                            pContext.DrawEllipse( lEllipseParameters[ 0 ] as ABrush,
                                                  lEllipseParameters[ 1 ] as Pen,
                                                  (Point)lEllipseParameters[ 2 ],
                                                  (double)lEllipseParameters[ 3 ],
                                                  (double)lEllipseParameters[ 4 ] );
                        }
                        break;
                    case DrawCommand.Geometry:
                        {
                            object[] lGeometryParameters = lData.Value;
                            pContext.DrawGeometry( lGeometryParameters[ 0 ] as ABrush,
                                                   lGeometryParameters[ 1 ] as Pen,
                                                   lGeometryParameters[ 2 ] as AGeometry );
                        }
                        break;
                    case DrawCommand.Image:
                        {
                            object[] lImageParameters = lData.Value;
                            pContext.DrawImage( lImageParameters[ 0 ] as MagickImage,
                                                (Rect)lImageParameters[ 1 ] );
                        }
                        break;
                    case DrawCommand.PushOpacity:
                        {
                            object[] lOpacityParameters = lData.Value;
                            pContext.PushOpacity( (double)lOpacityParameters[ 0 ] );
                        }
                        break;
                    case DrawCommand.PushTransform:
                        {
                            object[] lTransformParameters = lData.Value;
                            pContext.PushTransform( lTransformParameters[ 0 ] as ATransform );
                        }
                        break;
                    case DrawCommand.Pop:
                        {
                            pContext.Pop();
                        }
                        break;
                    default:
                        {
                            LogManager.Instance.Log( "Unknown draw command!!!", LogType.ERROR );
                        }
                        break;
                }
                lDrawCount++;
            }
        }

        /// <summary>
        /// Clone the render data.
        /// </summary>
        /// <returns>The clone.</returns>
        public object Clone()
        {
            return new RenderData();
        }

        #endregion Methods
    }
}
