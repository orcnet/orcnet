﻿using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Tile brush describing the way to fill a region by tiling.
    /// </summary>
    public abstract class ATileBrush : ABrush
    {
        #region Fields

        /// <summary>
        /// Stores the viewport brush mode.
        /// </summary>
        private BrushMappingMode mViewportBrushMode;

        /// <summary>
        /// Stores the viewbox brush mode.
        /// </summary>
        private BrushMappingMode mViewboxBrushMode;

        /// <summary>
        /// Stores the viewport.
        /// </summary>
        private Rect mViewport;

        /// <summary>
        /// Stores the viewbox.
        /// </summary>
        private Rect mViewbox;

        /// <summary>
        /// Stores the stretch mode.
        /// </summary>
        private Stretch mStretch;

        /// <summary>
        /// Stores the horizontal alignment of the content.
        /// </summary>
        private HorizontalAlignment mHorizontalAlignment;

        /// <summary>
        /// Stores the vertical alignment of the content.
        /// </summary>
        private VerticalAlignment mVerticalAlignment;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the viewport brush mode.
        /// </summary>
        public BrushMappingMode ViewportBrushMode
        {
            get
            {
                return this.mViewportBrushMode;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mViewportBrushMode, value, "ViewportBrushMode" );
            }
        }

        /// <summary>
        /// Gets or sets the viewbox brush mode.
        /// </summary>
        public BrushMappingMode ViewboxBrushMode
        {
            get
            {
                return this.mViewboxBrushMode;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mViewboxBrushMode, value, "ViewboxBrushMode" );
            }
        }

        /// <summary>
        /// Gets or sets the viewport.
        /// </summary>
        public Rect Viewport
        {
            get
            {
                return this.mViewport;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mViewport, value, "Viewport" );
            }
        }

        /// <summary>
        /// Gets or sets the viewbox.
        /// </summary>
        public Rect Viewbox
        {
            get
            {
                return this.mViewbox;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mViewbox, value, "Viewbox" );
            }
        }

        /// <summary>
        /// Gets or sets the stretch mode.
        /// </summary>
        public Stretch Stretch
        {
            get
            {
                return this.mStretch;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mStretch, value, "Stretch" );
            }
        }
        
        /// <summary>
        /// Gets or sets the horizontal alignment of the content.
        /// </summary>
        public HorizontalAlignment HorizontalAlignment
        {
            get
            {
                return this.mHorizontalAlignment;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mHorizontalAlignment, value, "HorizontalAlignment" );
            }
        }

        /// <summary>
        /// Gets or sets the vertical alignment of the content.
        /// </summary>
        public VerticalAlignment VerticalAlignment
        {
            get
            {
                return this.mVerticalAlignment;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mVerticalAlignment, value, "VerticalAlignment" );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ATileBrush"/> class.
        /// </summary>
        protected ATileBrush()
        {
            this.mViewportBrushMode = BrushMappingMode.RelativeToBoundingBox;
            this.mViewboxBrushMode  = BrushMappingMode.RelativeToBoundingBox;
            this.mViewport = new Rect( 0, 0, 1, 1 );
            this.mViewbox  = new Rect( 0, 0, 1, 1 );
            this.mStretch  = Stretch.Fill;
            this.mHorizontalAlignment = HorizontalAlignment.Center;
            this.mVerticalAlignment   = VerticalAlignment.Center;
        }
        
        #endregion Constructor

        #region Methods

        /// <summary>
        /// Computes the bounds for the content of the tile.
        /// </summary>
        /// <param name="pContentCounds"></param>
        protected abstract void GetContentBounds(out Rect pContentCounds);

        #endregion Methods
    }
}
