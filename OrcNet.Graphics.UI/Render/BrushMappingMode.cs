﻿namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Enumeration of the brush mapping mode being whether absolute local coordinates or
    /// multiple of a BBox's size must be considered.
    /// </summary>
    public enum BrushMappingMode
    {
        /// <summary>
        /// The values in question will be interpreted directly in local space.
        /// </summary>
        Absolute = 0,

        /// <summary>
        /// The values will be interpreted as a multiples of a bounding box,
        /// where 1.0 is considered 100% of the bounding box measure.
        /// </summary>
        RelativeToBoundingBox = 1,
    }
}
