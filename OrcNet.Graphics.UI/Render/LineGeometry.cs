﻿using OrcNet.Graphics.UI.Helpers;
using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Line based geometry class definition.
    /// </summary>
    public sealed class LineGeometry : AGeometry
    {
        #region Fields

        /// <summary>
        /// Stores the starting point of the line.
        /// </summary>
        private Point mStartPoint;

        /// <summary>
        /// Stores the ending point of the line.
        /// </summary>
        private Point mEndPoint;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the starting point of the line.
        /// </summary>
        public Point StartPoint
        {
            get
            {
                return this.mStartPoint;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mStartPoint, value, "StartPoint" );
            }
        }

        /// <summary>
        /// Gets or sets the ending point of the line.
        /// </summary>
        public Point EndPoint
        {
            get
            {
                return this.mEndPoint;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mEndPoint, value, "EndPoint" );
            }
        }

        /// <summary>
        /// Gets the bounds of this Geometry as an axis-aligned bounding box
        /// </summary>
        public override Rect Bounds
        {
            get
            {
                Rect lBounds = new Rect( this.mStartPoint, this.mEndPoint );

                ATransform lThisTransform = this.Transform;
                if 
                    ( lThisTransform != null && 
                      lThisTransform.IsIdentity == false )
                {
                    lThisTransform.TransformRect( ref lBounds );
                }

                return lBounds;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of <see cref="LineGeometry"/> class.
        /// </summary>
        public LineGeometry() :
        this( new Point(), new Point(), ATransform.Identity )
        {

        }

        /// <summary>
        /// Initializes a new instance of <see cref="LineGeometry"/> class.
        /// </summary>
        /// <param name="pStartPoint">The starting point of the line.</param>
        /// <param name="pEndPoint">The ending point of the line.</param>
        public LineGeometry(Point pStartPoint, Point pEndPoint) :
        this( pStartPoint, pEndPoint, ATransform.Identity )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of <see cref="LineGeometry"/> class.
        /// </summary>
        /// <param name="pStartPoint">The starting point of the line.</param>
        /// <param name="pEndPoint">The ending point of the line.</param>
        /// <param name="pTransform">The line geometry transformation.</param>
        public LineGeometry(Point pStartPoint, Point pEndPoint, ATransform pTransform)
        {
            this.mStartPoint = pStartPoint;
            this.mEndPoint   = pEndPoint;
            this.Transform   = pTransform;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Returns true if this geometry is empty
        /// </summary>
        public override bool IsEmpty()
        {
            return false;
        }

        /// <summary>
        /// Returns if geometry is inside this geometry.
        /// </summary>
        /// <param name="pGeometry">The geometry to check.</param>
        public override HitResultDetail FillContainsWithDetail(AGeometry pGeometry)
        {
            // Line to Line.
            LineGeometry lLineCast = pGeometry as LineGeometry;
            if
                ( lLineCast != null )
            {
                Point? lIntersection = IntersectionHelper.IsIntersectingLine( this.mStartPoint, this.mEndPoint,
                                                                              lLineCast.mStartPoint, lLineCast.mEndPoint );
                if
                    ( lIntersection.HasValue )
                {
                    return HitResultDetail.Intersects;
                }
                else
                {
                    return HitResultDetail.Empty;
                }
            }

            // Line to Rectangle.
            RectangleGeometry lRectangleCast = pGeometry as RectangleGeometry;
            if
                ( lRectangleCast != null )
            {
                Point[] lPolygon = GeometryHelper.GetRectanglePointList( lRectangleCast.Rectangle, lRectangleCast.RadiusX, lRectangleCast.RadiusY );
                bool lIsStartPointInPolygon = IntersectionHelper.IsPointInPolygon( this.mStartPoint, lPolygon );
                bool lIsEndPointInPolygon   = IntersectionHelper.IsPointInPolygon( this.mEndPoint, lPolygon );
                if
                    ( lIsStartPointInPolygon && 
                      lIsEndPointInPolygon )
                {
                    return HitResultDetail.FullyInside;
                }
                else if
                    ( lIsStartPointInPolygon || 
                      lIsEndPointInPolygon )
                {
                    return HitResultDetail.Intersects;
                }

                // If none is inside, check for simple intersection.
                double lNearIntersection = 0.0;
                double lFarIntersection  = 1.0;
                if
                    ( IntersectionHelper.IsIntersectingPolygon( lPolygon, this.mStartPoint, this.mEndPoint, ref lNearIntersection, ref lFarIntersection ) )
                {
                    return HitResultDetail.Intersects;
                }
            }

            // Line to Ellipse.
            EllipseGeometry lEllipseCast = pGeometry as EllipseGeometry;
            if
                ( lEllipseCast != null )
            {
                Point[] lPolygon = GeometryHelper.GetEllipsePointList( lEllipseCast.Center, lEllipseCast.RadiusX, lEllipseCast.RadiusY );
                bool lIsStartPointInPolygon = IntersectionHelper.IsPointInPolygon( this.mStartPoint, lPolygon );
                bool lIsEndPointInPolygon   = IntersectionHelper.IsPointInPolygon( this.mEndPoint, lPolygon );
                if
                    ( lIsStartPointInPolygon && 
                      lIsEndPointInPolygon )
                {
                    return HitResultDetail.FullyInside;
                }
                else if
                    ( lIsStartPointInPolygon || 
                      lIsEndPointInPolygon )
                {
                    return HitResultDetail.Intersects;
                }

                // If none is inside, check for simple intersection.
                double lNearIntersection = 0.0;
                double lFarIntersection  = 1.0;
                if
                    ( IntersectionHelper.IsIntersectingPolygon( lPolygon, this.mStartPoint, this.mEndPoint, ref lNearIntersection, ref lFarIntersection ) )
                {
                    return HitResultDetail.Intersects;
                }
            }

            return HitResultDetail.NotCalculated;
        }

        /// <summary>
        /// Returns if a given geometry is inside the stroke defined by a given pen on this geometry.
        /// </summary>
        /// <param name="pPen">The pen used to stroke.</param>
        /// <param name="pGeometry">The geometry to check.</param>
        public override HitResultDetail StrokeContainsWithDetail(Pen pPen, AGeometry pGeometry)
        {
            // For now, same as FillContainsWithDetail
            return FillContainsWithDetail( pGeometry );
        }

        /// <summary>
        /// Returns the axis-aligned bounding rectangle when stroked with a pen, after applying
        /// the supplied transform (if non-null).
        /// </summary>
        /// <param name="pPen">The pen used to stroke.</param>
        /// <param name="pWorldMatrix">The world transformation to apply.</param>
        internal override Rect GetBoundsInternal(Pen pPen, Matrix pWorldMatrix)
        {
            Matrix lGeometryMatrix;
            ATransform.GetTransformValue( Transform, out lGeometryMatrix );

            return GeometryHelper.GetBoundsHelper( pPen, 
                                                   pWorldMatrix,
                                                   this.mStartPoint,
                                                   this.mEndPoint,
                                                   lGeometryMatrix );
        }

        /// <summary>
        /// Returns true if point is inside the stroke of a pen on this geometry.
        /// </summary>
        /// <param name="pPen"></param>
        /// <param name="pHitPoint"></param>
        /// <returns>True of contains the point, false otherwise.</returns>
        internal override bool ContainsInternal(Pen pPen, Point pHitPoint)
        {
            return IntersectionHelper.IsPointInLine( pHitPoint, this.mStartPoint, this.mEndPoint );
        }

        /// <summary>
        /// Clone the geometry.
        /// </summary>
        /// <returns>The cloned geometry</returns>
        public override AGeometry Clone()
        {
            return new LineGeometry( this.mStartPoint, this.mEndPoint, this.Transform.Clone() as ATransform );
        }

        /// <summary>
        /// Creates an empty geometry model.
        /// </summary>
        /// <returns>The empty geometry.</returns>
        protected override AGeometry MakeEmptyGeometry()
        {
            return new LineGeometry();
        }

        #endregion Methods
    }
}
