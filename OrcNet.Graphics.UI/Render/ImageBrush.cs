﻿using System;
using OrcNet.Graphics.UI.Maths;
using ImageMagick;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Brush defining its content with an image.
    /// </summary>
    public sealed class ImageBrush : ATileBrush
    {
        #region Fields

        /// <summary>
        /// Stores the image weak reference.
        /// </summary>
        private WeakReference mImageReference;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the image that brush must use as content.
        /// </summary>
        public MagickImage Image
        {
            get
            {
                if
                    ( this.mImageReference.IsAlive )
                {
                    return this.mImageReference.Target as MagickImage;
                }

                return null;
            }
            set
            {
                this.mImageReference.Target = value;

                this.InformPropertyChangedListener( value, value, "Image" );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageBrush"/> class.
        /// </summary>
        public ImageBrush() :
        this( null )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageBrush"/> class.
        /// </summary>
        /// <param name="pImage">The content image.</param>
        public ImageBrush(MagickImage pImage)
        {
            this.mImageReference = new WeakReference( pImage );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Computes the bounds for the content of the tile.
        /// </summary>
        /// <param name="pContentCounds"></param>
        protected override void GetContentBounds(out Rect pContentCounds)
        {
            MagickImage lImage = this.Image;
            if
                ( lImage != null )
            {
                pContentCounds = new Rect( 0, 0, lImage.Width, lImage.Height );
            }
            else
            {
                pContentCounds = Rect.Empty;
            }
        }

        #endregion Methods
    }
}
