﻿using ImageMagick;
using OrcNet.Graphics.UI.Helpers;
using OrcNet.Graphics.UI.Maths;
using System.Collections.Generic;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Drawing context in charge of computing the bounds of the contents to
    /// render.
    /// </summary>
    internal class BoundsDrawingContext : ABaseDrawingContext
    {
        #region Fields

        /// <summary>
        /// Stores the accumulated bounds, in world space.
        /// </summary>
        private Rect mBounds;

        /// <summary>
        /// Stores the current local to world transform as a matrix.
        /// </summary>
        private Matrix mLocalToWordTransform;

        /// <summary>
        /// Stores the stacked pushed type for the Push/Pop calls.
        /// This tells whether a given Pop corresponds to a Transform or an Opacity.
        /// </summary>
        private Stack<PushType> mTypeStack;

        /// <summary>
        /// Stores the stacked pushed Matricies encountered during the tree walk.
        /// The current transform is stored in mLocalToWordTransform and not in that Stack.
        /// </summary>
        private Stack<Matrix>   mTransformStack;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the context object size if memory.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;

                return lSize;
            }
        }

        /// <summary>
        /// Gets the resulting bounds
        /// </summary>
        public Rect Bounds
        {
            get
            {
                return this.mBounds;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BoundsDrawingContext"/> class.
        /// </summary>
        public BoundsDrawingContext()
        {
            this.mBounds = Rect.Empty;
            this.mLocalToWordTransform = Matrix.Identity;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Draws a line with the specified pen.
        /// </summary>
        /// <param name="pPen"> The Pen with which to stroke the line. </param>
        /// <param name="pStart"> The start Point for the line. </param>
        /// <param name="pEnd"> The end Point for the line. </param>
        public override void DrawLine(Pen pPen, Point pStart, Point pEnd)
        {
            if
                ( Pen.ContributesToBounds( pPen ) )
            {
                Rect lGeometryBounds = GeometryHelper.GetBoundsHelper( pPen,
                                                                       this.mLocalToWordTransform, // world transform
                                                                       pStart,
                                                                       pEnd,
                                                                       Matrix.Identity ); // geometry transform 

                this.AddTransformedBounds( ref lGeometryBounds );
            }
        }
        
        /// <summary>
        /// Draw a rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle">The Rect to fill and/or stroke.</param>
        public override void DrawRectangle(ABrush pBrush, Pen pPen, Rect pRectangle)
        {
            if 
                ( pBrush != null || 
                  Pen.ContributesToBounds( pPen ) )
            {
                Rect lGeometryBounds = GeometryHelper.GetBoundsHelper( pPen,
                                                                       this.mLocalToWordTransform, // world transform
                                                                       pRectangle,
                                                                       0.0,
                                                                       0.0,
                                                                       Matrix.Identity ); // geometry transform

                this.AddTransformedBounds( ref lGeometryBounds );
            }
        }
        
        /// <summary>
        /// Draw a rounded rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle"> The Rect to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Width/2].</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Height/2].</param>
        public override void DrawRoundedRectangle(ABrush pBrush, Pen pPen, Rect pRectangle, double pRadiusX, double pRadiusY)
        {
            if 
                ( pBrush != null || 
                  Pen.ContributesToBounds( pPen ) )
            {
                Rect lGeometryBounds = GeometryHelper.GetBoundsHelper( pPen,
                                                                       this.mLocalToWordTransform, // world transform
                                                                       pRectangle,
                                                                       pRadiusX,
                                                                       pRadiusY,
                                                                       Matrix.Identity ); // geometry transform

                this.AddTransformedBounds( ref lGeometryBounds );
            }
        }
        
        /// <summary>
        /// Draw an ellipse with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the ellipse.</param>
        /// <param name="pPen">The Pen with which to stroke the ellipse.</param>
        /// <param name="pCenter">The center of the ellipse to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        public override void DrawEllipse(ABrush pBrush, Pen pPen, Point pCenter, double pRadiusX, double pRadiusY)
        {
            if 
                ( pBrush != null || 
                  Pen.ContributesToBounds( pPen ) )
            {
                Rect lGeometryBounds = GeometryHelper.GetBoundsHelper( pPen,
                                                                       this.mLocalToWordTransform, // world transform
                                                                       pCenter,
                                                                       pRadiusX,
                                                                       pRadiusY,
                                                                       Matrix.Identity ); // geometry transform

                this.AddTransformedBounds( ref lGeometryBounds );
            }
        }
        
        /// <summary>
        /// Draw a Geometry with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the Geometry.</param>
        /// <param name="pPen">The Pen with which to stroke the Geometry.</param>
        /// <param name="pGeometry"> The Geometry to fill and/or stroke. </param>
        public override void DrawGeometry(ABrush pBrush, Pen pPen, AGeometry pGeometry)
        {
            if 
                ( pGeometry != null && 
                  ( pBrush != null || Pen.ContributesToBounds( pPen ) ) )
            {
                Rect lGeometryBounds = pGeometry.GetBoundsInternal( pPen, this.mLocalToWordTransform );

                this.AddTransformedBounds( ref lGeometryBounds );
            }
        }

        /// <summary>
        /// Draw an Image into the region specified by the Rect.
        /// The Image will potentially be stretched and distorted to fit the Rect.
        /// For more fine grained control, consider filling a Rect with an ImageBrush via DrawRectangle.
        /// </summary>
        /// <param name="pImage">The image to draw.</param>
        /// <param name="pRectangle">The Rect into which the image will be fit.</param>
        public override void DrawImage(MagickImage pImage, Rect pRectangle)
        {
            if 
                ( pImage != null )
            {
                this.AddBounds( ref pRectangle );
            }
        }
        
        /// <summary>
        /// Push an opacity which will blend the composite of all drawing primitives added 
        /// until the corresponding Pop call.
        /// </summary>
        /// <param name="pOpacity">The opacity with which to blend - 0 is transparent, 1 is opaque.</param>
        public override void PushOpacity(double pOpacity)
        {
            // Push the opacity type
            this.PushTypeStack( PushType.Opacity );
        }
        
        /// <summary>
        /// Push a Transform which will apply to all drawing operations until the corresponding Pop.
        /// </summary>
        /// <param name="pTransform">The Transform to push.</param>
        public override void PushTransform(ATransform pTransform)
        {
            if 
                ( this.mTransformStack == null )
            {
                this.mTransformStack = new Stack<Matrix>( 2 );
            }

            // Push the old transform.
            this.mTransformStack.Push( this.mLocalToWordTransform );

            // Push the pTransform type
            this.PushTypeStack( PushType.Transform );

            Matrix lNewTransform = Matrix.Identity;
            // Gets the new transform as a matrix if it exists
            if 
                ( pTransform != null && 
                  pTransform.IsIdentity == false )
            {
                lNewTransform = pTransform.Value;
            }

            // Update the current local to world transform
            this.mLocalToWordTransform = lNewTransform * this.mLocalToWordTransform;
        }
        
        /// <summary>
        /// Pop the previously pushed drawing context elements.
        /// </summary>
        public override void Pop()
        {
            // Retrieve the PushType to figure out what what this Pop is.
            PushType lPushType = this.mTypeStack.Pop();
            switch 
                ( lPushType )
            {
                case PushType.Transform:
                    {
                        if
                            ( this.mTransformStack != null &&
                              this.mTransformStack.Count > 0 )
                        {
                            // Restore the transform
                            this.mLocalToWordTransform = this.mTransformStack.Pop();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        
        /// <summary>
        /// Ensure that the state is clear and is good for next use.
        /// </summary>   
        internal void ClearState()
        {
            this.mBounds = Rect.Empty;
            this.mLocalToWordTransform = new Matrix();
            this.mTypeStack      = null;
            this.mTransformStack = null;
        }

        /// <summary>
        /// Unions the non-transformed bounds which are local to the current 
        /// Drawing operation with the aggregated bounds of other Drawing operations 
        /// encountered during the tree walk.
        /// </summary>
        /// <param name="pBounds"> 
        ///     In:  The bounds of the geometry to union in the coordinate
        ///          space of the current Drawing operations
        ///     Out: The transformed bounds in the coordinate space of the top-level Drawing
        ///          operation.
        /// </param>
        private void AddBounds(ref Rect pBounds)
        {
            // _bounds is always in "world" space
            // So, we need to transform the Rect to world to bound it
            if 
                ( this.mLocalToWordTransform.IsIdentity == false )
            {
                MatrixUtil.TransformRect( ref pBounds, ref this.mLocalToWordTransform );
            }
 
            this.AddTransformedBounds( ref pBounds );
        }
 
        /// <summary>
        /// Unions bounds which have been transformed into the top-level Drawing operation
        /// with the aggregated bounds of other Drawing operations encountered during the tree walk.
        /// </summary>
        /// <param name="pBounds"> 
        ///     In:  The bounds of the geometry to union in the coordinate
        ///          space of the current Drawing operations
        ///     Out: The transformed bounds in the coordinate space of the top-level Drawing
        ///          operation.
        /// </param>        
        private void AddTransformedBounds(ref Rect pBounds)
        {
            if 
                ( DoubleUtil.RectHasNaN( pBounds ) )
            {
                // We set the bounds to infinity if it has NaN
                pBounds.X      = double.NegativeInfinity;
                pBounds.Y      = double.NegativeInfinity;
                pBounds.Width  = double.PositiveInfinity;
                pBounds.Height = double.PositiveInfinity;
            }
 
            this.mBounds.Union( pBounds );
        }
 
        /// <summary>
        /// Ensure the type stack exists, and store given push type there.
        /// </summary>
        /// <param name="pPushType">The push type to store</param>
        private void PushTypeStack(PushType pPushType)
        {
            if 
                ( this.mTypeStack == null )
            {
                this.mTypeStack = new Stack<PushType>( 2 );
            }
 
            this.mTypeStack.Push( pPushType );
        }

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Enumerates the type able to be pushed into the context.
        /// </summary>
        private enum PushType
        {
            /// <summary>
            /// Pushed a transform.
            /// </summary>
            Transform,

            /// <summary>
            /// Pushed an opacity.
            /// </summary>
            Opacity
        }

        #endregion Inner classes
    }
}
