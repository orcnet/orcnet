﻿using OrcNet.Graphics.UI.Maths;
using System.Collections.Generic;
using System.Diagnostics;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Point based hit test drawing context class definition.
    /// </summary>
    internal class PointHitTestDrawingContext : HitTestDrawingContext
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether all instructions should perform no logic 
        /// until the layer is exited via a Pop()
        /// </summary>
        private bool mIsCurrentLayerNoOp;

        /// <summary>
        /// Stores the number of Pop() calls until IsCurrentLayerNoOp should be reset.
        /// </summary>
        private int  mNoOpLayerDepth;

        /// <summary>
        /// Stores the point involved in the Hit test.
        /// </summary>
        private Point mPoint;

        /// <summary>
        /// Stores the hit test point transformed to target geometry's 
        /// original coordinates.
        /// </summary>
        private Stack<Point> mPointStack;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the drawing context memory size.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = base.Size;
                lSize += sizeof(double) * 2;
                return lSize;
            }
        }

        /// <summary>
        /// Gets the hit result detail.
        /// </summary>
        internal override HitResultDetail IntersectionDetail
        {
            get
            {
                return this.mIsFullyContained ? HitResultDetail.FullyInside : HitResultDetail.Empty;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether there was a hit result or not.
        /// </summary>
        internal override bool IsHit
        {
            get
            {
                return this.mIsFullyContained;
            }
        }

        /// <summary>
        /// Gets or sets whether the current subgraph layer is a no-op or not.
        /// Currently, all subsequent instructions are skipped (no-op'd) when a non-invertible
        /// transform is pushed (because we have to invert the matrix to perform
        /// a hit-test).
        /// </summary>   
        private bool IsCurrentLayerNoOp
        {
            get
            {
                return this.mIsCurrentLayerNoOp;
            }
            set
            {
                if (value == true)
                {
                    // Guard that we aren't already in a no-op layer
                    //
                    // Instructions that can cause the layer to be no-op'd should be
                    // no-op'd themselves, and thus can't call this method,  if we 
                    // are already in a no-op layer
                    Debug.Assert( this.mIsCurrentLayerNoOp == false );
                    Debug.Assert( this.mNoOpLayerDepth == 0 );

                    // Set the no-op status & initial depth
                    this.mIsCurrentLayerNoOp = true;
                    this.mNoOpLayerDepth++;
                }
                else
                {
                    // Guard that we are in a no-op layer, and that the correct corresponding 
                    // Pop has been called.
                    Debug.Assert( this.mIsCurrentLayerNoOp );
                    Debug.Assert( this.mNoOpLayerDepth == 0);

                    // Reset the no-op status
                    this.mIsCurrentLayerNoOp = false;
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PointHitTestDrawingContext"/> class.
        /// </summary>
        /// <param name="pPoint">The point involved in the Hit test.</param>
        internal PointHitTestDrawingContext(Point pPoint)
        {
            this.mPoint = pPoint;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Draw a Geometry with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the Geometry.</param>
        /// <param name="pPen">The Pen with which to stroke the Geometry.</param>
        /// <param name="pGeometry"> The Geometry to fill and/or stroke. </param>
        public override void DrawGeometry(ABrush pBrush, Pen pPen, AGeometry pGeometry)
        {
            if 
                ( this.IsCurrentLayerNoOp || 
                  pGeometry == null || 
                  pGeometry.IsEmpty() )
            {
                return;
            }

            if 
                ( pBrush != null )
            {
                this.mIsFullyContained |= pGeometry.FillContains( this.mPoint );
            }

            // If we have a pen and we haven't yet hit, 
            // try the widened geometry.
            if 
                ( pPen != null && 
                  this.mIsFullyContained == false )
            {
                this.mIsFullyContained |= pGeometry.StrokeContains( pPen, this.mPoint );
            }

            // If we've hit, stop walking.
            if 
                ( this.mIsFullyContained )
            {
                this.StopWalking();
            }
        }

        /// <summary>
        /// Push an opacity which will blend the composite of all drawing primitives added 
        /// until the corresponding Pop call.
        /// </summary>
        /// <param name="pOpacity">The opacity with which to blend - 0 is transparent, 1 is opaque.</param>
        public override void PushOpacity(double pOpacity)
        {
            if 
                ( this.IsPushNoOp() == false )
            {
                // This Push doesn't affect the hit test, 
                // so just push the current point
                this.PushPointStack( this.mPoint );
            }
        }

        /// <summary>
        /// Push a Transform which will apply to all drawing operations until the corresponding Pop.
        /// </summary>
        /// <param name="pTransform">The Transform to push.</param>
        public override void PushTransform(ATransform pTransform)
        {
            if 
                ( this.IsPushNoOp() == false )
            {
                if 
                    ( pTransform == null || 
                      pTransform.IsIdentity )
                {
                    this.PushPointStack( this.mPoint );
                }
                else
                {
                    Matrix lMatrix = pTransform.Value;
                    if 
                        ( lMatrix.HasInverse )
                    {
                        // The inverse will be applied to the point so that hit testing 
                        // is done in the original geometry's coordinates
                        lMatrix.Invert();

                        // Push the transformed point on the stack.
                        // This also updates the cached point.
                        this.PushPointStack( this.mPoint * lMatrix );
                    }
                    else
                    {
                        // If this transform doesn't have an inverse, 
                        // then we don't need to consider any of the subsequent
                        // Drawing operations in this layer.
                        this.IsCurrentLayerNoOp = true;
                    }
                }
            }
        }

        /// <summary>
        /// Pop the previously pushed drawing context elements.
        /// </summary>
        public override void Pop()
        {
            if 
                ( this.IsPopNoOp() == false )
            {
                this.PopPointStack();
            }
        }

        /// <summary>
        /// Pushes a point onto the stack.
        /// </summary>
        /// <param name="pPoint">The new Point to push.</param>
        private void PushPointStack(Point pPoint)
        {
            if 
                ( this.mPointStack == null )
            {
                this.mPointStack = new Stack<Point>( 2 );
            }

            // Push the old point.
            this.mPointStack.Push( this.mPoint );

            // update current point
            this.mPoint = pPoint;
        }

        /// <summary>
        /// Pops a point off the stack.
        /// </summary>
        private void PopPointStack()
        {
            Debug.Assert( this.mPointStack != null );
            Debug.Assert( this.mPointStack.Count > 0 );

            // Retrieve the previous point from the stack.
            this.mPoint = this.mPointStack.Pop();
        }

        /// <summary>
        /// Checks whether the operation should be a no-op or not.
        /// If the current subgraph layer is being no-op'd, it also increments the no-op depth.
        /// </summary>        
        private bool IsPushNoOp()
        {
            if 
                ( this.IsCurrentLayerNoOp )
            {
                // Increment the depth so that the no-op status isn't reset
                // when this layer's cooresponding Pop is called.
                this.mNoOpLayerDepth++;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks whether a Pop should be a no-op'd.
        /// If the current subgraph layer is being no-op'd, it also decrements the no-op depth and
        /// then reset's the no-op status if it is the last Pop in the no-op layer.
        /// </summary>        
        private bool IsPopNoOp()
        {
            if 
                ( this.IsCurrentLayerNoOp )
            {
                Debug.Assert( this.mNoOpLayerDepth >= 1);

                this.mNoOpLayerDepth--;

                // If this Pop cooresponds to the Push that created
                // the no-op layer, then reset the no-op status.
                if
                    ( this.mNoOpLayerDepth == 0 )
                {
                    this.IsCurrentLayerNoOp = false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion Methods
    }
}
