﻿namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Enumeration descibing how a source rectangle should be stretched 
    /// to fit a destination rectangle.
    /// </summary>
    public enum Stretch
    {
        /// <summary>
        /// Preserves the original size
        /// </summary>
        None = 0,

        /// <summary>
        /// The aspect ratio is not preserved and the source rectangle 
        /// fully fills the destination rectangle.
        /// </summary>
        Fill = 1,

        /// <summary>
        /// The aspect ratio is preserved and the source rectangle is uniformly scaled 
        /// as large as possible such that both width and height fit within the destination 
        /// rectangle.
        /// </summary>
        Uniform = 2,

        /// <summary>
        /// The aspect ratio is preserved and the source rectangle is uniformly scaled 
        /// as small as possible such that the entire destination rectangle is filled.
        /// </summary>
        UniformToFill = 3,
    }
}
