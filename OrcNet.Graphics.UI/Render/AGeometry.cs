﻿using OrcNet.Core;
using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Base geometry abstract class definition.
    /// </summary>
    public abstract class AGeometry : AObservable
    {
        #region Fields

        /// <summary>
        /// Stores the constant empty geometry model.
        /// </summary>
        private static AGeometry sEmpty = null;

        /// <summary>
        /// Stores the geometry transform.
        /// </summary>
        private ATransform mTransform;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the constant empty model.
        /// </summary>
        public AGeometry Empty
        {
            get
            {
                if
                    ( sEmpty == null )
                {
                    sEmpty = this.MakeEmptyGeometry();
                }

                return sEmpty;
            }
        }
        
        /// <summary>
        /// Gets the bounds of this Geometry as an axis-aligned bounding box
        /// </summary>
        public abstract Rect Bounds
        {
            get;
        }

        /// <summary>
        /// Gets or sets the geometry transform.
        /// </summary>
        public ATransform Transform
        {
            get
            {
                return this.mTransform;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mTransform, value, "Transform" );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AGeometry"/> class.
        /// </summary>
        protected AGeometry()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Returns true if this geometry may have curved segments
        /// </summary>
        public virtual bool MayHaveCurves()
        {
            return false;
        }

        /// <summary>
        /// Returns the axis-aligned bounding rectangle when stroked with a pen.
        /// </summary>
        /// <param name="pPen">The pen</param>
        public virtual Rect GetRenderBounds(Pen pPen)
        {
            Matrix lMatrix = Matrix.Identity;
            return GetBoundsInternal( pPen, lMatrix );
        }

        /// <summary>
        /// This is not meant to be used generically since not all geometries implement
        /// the method (currently only RectangleGeometry is implemented).
        /// </summary>
        /// <param name="pGeometry">The other geometry to test against.</param>
        internal virtual bool AreClose(AGeometry pGeometry)
        {
            return false;
        }
        
        /// <summary>
        /// Returns true if this geometry is empty
        /// </summary>
        public abstract bool IsEmpty();

        /// <summary>
        /// Returns true if point is inside the fill region defined by this geometry.
        /// </summary>
        /// <param name="pHitPoint">The point to check</param>
        public bool FillContains(Point pHitPoint)
        {
            return ContainsInternal( null, pHitPoint );
        }

        /// <summary>
        /// Returns true if a given geometry is contained inside this geometry.
        /// </summary>
        /// <param name="pGeometry">The geometry to check.</param>
        public bool FillContains(AGeometry pGeometry)
        {
            HitResultDetail lDetail = this.FillContainsWithDetail( pGeometry );

            return lDetail == HitResultDetail.FullyContains;
        }

        /// <summary>
        /// Returns if geometry is inside this geometry.
        /// </summary>
        /// <param name="pGeometry">The geometry to check.</param>
        public abstract HitResultDetail FillContainsWithDetail(AGeometry pGeometry);

        /// <summary>
        /// Returns true if point is inside the stroke of a pen on this geometry.
        /// </summary>
        /// <param name="pPen">The pen used to define the stroke</param>
        /// <param name="pHitPoint">The point to check</param>
        public bool StrokeContains(Pen pPen, Point pHitPoint)
        {
            if 
                ( pPen == null )
            {
                return false;
            }

            return ContainsInternal( pPen, pHitPoint );
        }

        /// <summary>
        /// Returns if a given geometry is inside the stroke defined by a given pen on this geometry.
        /// </summary>
        /// <param name="pPen">The pen used to stroke.</param>
        /// <param name="pGeometry">The geometry to check.</param>
        public abstract HitResultDetail StrokeContainsWithDetail(Pen pPen, AGeometry pGeometry);

        /// <summary>
        /// Returns the axis-aligned bounding rectangle when stroked with a pen, after applying
        /// the supplied transform (if non-null).
        /// </summary>
        /// <param name="pPen">The pen used to stroke.</param>
        /// <param name="pMatrix">The transformation to apply.</param>
        internal abstract Rect GetBoundsInternal(Pen pPen, Matrix pMatrix);

        /// <summary>
        /// Returns true if point is inside the stroke of a pen on this geometry.
        /// </summary>
        /// <param name="pPen"></param>
        /// <param name="pHitPoint"></param>
        /// <returns>True of contains the point, false otherwise.</returns>
        internal abstract bool ContainsInternal(Pen pPen, Point pHitPoint);

        /// <summary>
        /// Gets a copy if this geometry transformed by the given transformation.
        /// </summary>
        /// <param name="pTransform">The transformation to apply to the cloned geometry.</param>
        /// <returns>The transformed cloned geometry.</returns>
        internal AGeometry GetTransformedCopy(ATransform pTransform)
        {
            AGeometry lCopy = this.Clone();
            ATransform lThisTransform = this.Transform;
            if 
                ( pTransform != null && 
                  pTransform.IsIdentity == false )
            {
                if 
                    ( lThisTransform == null || 
                      lThisTransform.IsIdentity )
                {
                    lCopy.Transform = pTransform;
                }
                else
                {
                    lCopy.Transform = new MatrixTransform( lThisTransform.Value * pTransform.Value );
                }
            }

            return lCopy;
        }

        /// <summary>
        /// Clone the geometry.
        /// </summary>
        /// <returns>The cloned geometry</returns>
        public abstract AGeometry Clone();
        
        /// <summary>
        /// Creates an empty geometry model.
        /// </summary>
        /// <returns>The empty geometry.</returns>
        protected abstract AGeometry MakeEmptyGeometry();

        /// <summary>
        /// Get the combination of the internal transform with a given transform.
        /// </summary>
        /// <param name="pTransform">The transform to combine with.</param>
        /// <returns>The combined transform.</returns>
        internal Matrix GetCombinedMatrix(ATransform pTransform)
        {
            Matrix lMatrix = Matrix.Identity;
            ATransform lThisTransform = Transform;

            if 
                ( lThisTransform != null && 
                  lThisTransform.IsIdentity == false )
            {
                lMatrix = lThisTransform.Value;

                if 
                    ( pTransform != null && 
                      pTransform.IsIdentity == false )
                {
                    lMatrix *= pTransform.Value;
                }
            }
            else if 
                ( pTransform != null && 
                  pTransform.IsIdentity == false )
            {
                lMatrix = pTransform.Value;
            }

            return lMatrix;
        }

        #endregion Methods
    }
}
