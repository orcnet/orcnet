﻿using ImageMagick;
using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Base Hit test drawing context class definition in charge
    /// of walking through the data to render to know whether there is
    /// a hit result or not.
    /// </summary>
    internal abstract class HitTestDrawingContext : ABaseDrawingContext
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the visual element fully contains 
        /// the hit test point or geometry.
        /// </summary>
        protected bool mIsFullyContained;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the drawing context memory size.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                lSize += sizeof(bool);
                return lSize;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether there was a hit result or not.
        /// </summary>
        internal abstract bool IsHit
        {
            get;
        }

        /// <summary>
        /// Gets the hit result detail.
        /// </summary>
        internal abstract HitResultDetail IntersectionDetail
        {
            get;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="HitTestDrawingContext"/> class.
        /// </summary>
        protected HitTestDrawingContext()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Draws a line with the specified pen.
        /// </summary>
        /// <param name="pPen"> The Pen with which to stroke the line. </param>
        /// <param name="pStart"> The start Point for the line. </param>
        /// <param name="pEnd"> The end Point for the line. </param>
        public override void DrawLine(Pen pPen, Point pStart, Point pEnd)
        {
            this.DrawGeometry( null, pPen, new LineGeometry( pStart, pEnd ) );
        }
        
        /// <summary>
        /// Draw a rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle">The Rect to fill and/or stroke.</param>
        public override void DrawRectangle(ABrush pBrush, Pen pPen, Rect pRectangle)
        {
            this.DrawGeometry( pBrush, pPen, new RectangleGeometry( pRectangle ) );
        }
        
        /// <summary>
        /// Draw a rounded rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle"> The Rect to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Width/2].</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Height/2].</param>
        public override void DrawRoundedRectangle(ABrush pBrush, Pen pPen, Rect pRectangle, double pRadiusX, double pRadiusY)
        {
            this.DrawGeometry( pBrush, pPen, new RectangleGeometry( pRectangle, pRadiusX, pRadiusY ) );
        }
        
        /// <summary>
        /// Draw an ellipse with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the ellipse.</param>
        /// <param name="pPen">The Pen with which to stroke the ellipse.</param>
        /// <param name="pCenter">The center of the ellipse to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        public override void DrawEllipse(ABrush pBrush, Pen pPen, Point pCenter, double pRadiusX, double pRadiusY)
        {
            this.DrawGeometry( pBrush, pPen, new EllipseGeometry( pCenter, pRadiusX, pRadiusY ) );
        }

        /// <summary>
        /// Draw an Image into the region specified by the Rect.
        /// The Image will potentially be stretched and distorted to fit the Rect.
        /// For more fine grained control, consider filling a Rect with an ImageBrush via DrawRectangle.
        /// </summary>
        /// <param name="pImage">The image to draw.</param>
        /// <param name="pRectangle">The Rect into which the image will be fit.</param>
        public override void DrawImage(MagickImage pImage, Rect pRectangle)
        {
            ImageBrush lBrush = new ImageBrush( pImage );
            this.DrawGeometry( lBrush, null, new RectangleGeometry( pRectangle ) );
        }

        #endregion Methods
    }
}
