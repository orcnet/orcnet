﻿namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Enumerate the available draw command.
    /// Relative to what drawing context are capable of.
    /// </summary>
    public enum DrawCommand
    {
        /// <summary>
        /// Draw a line.
        /// </summary>
        Line = 0,

        /// <summary>
        /// Draw a rectangle.
        /// </summary>
        Rectangle,

        /// <summary>
        /// Draw a rounded rectangle.
        /// </summary>
        RoundedRectangle,

        /// <summary>
        /// Draw an ellipse.
        /// </summary>
        Ellipse,

        /// <summary>
        /// Draw a custom geometry.
        /// </summary>
        Geometry,

        /// <summary>
        /// Draw an Image.
        /// </summary>
        Image,

        /// <summary>
        /// Push of an opacity.
        /// </summary>
        PushOpacity,

        /// <summary>
        /// Push of a transform.
        /// </summary>
        PushTransform,

        /// <summary>
        /// Pop the last pushed elements.
        /// </summary>
        Pop
    }
}
