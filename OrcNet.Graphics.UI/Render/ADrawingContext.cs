﻿using ImageMagick;
using OrcNet.Graphics.UI.Maths;
using OrcNet.Graphics.UI.Threading;
using System;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Base UI Drawing context abstract class definition.
    /// </summary>
    public abstract class ADrawingContext : ADispatcherObject, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the context has been disposed or not.
        /// </summary>
        private bool mIsDisposed;
        
        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ADrawingContext"/> class.
        /// </summary>
        protected ADrawingContext()
        {
            this.mIsDisposed = false;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Draws a line with the specified pen.
        /// </summary>
        /// <param name="pPen">The Pen with which to stroke the line.</param>
        /// <param name="pStart">The start Point for the line.</param>
        /// <param name="pEnd">The end Point for the line.</param>
        public abstract void DrawLine(Pen pPen, Point pStart, Point pEnd);
        
        /// <summary>
        /// Draw a rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle">The Rect to fill and/or stroke.</param>
        public abstract void DrawRectangle(ABrush pBrush, Pen pPen, Rect pRectangle);
        
        /// <summary>
        /// Draw a rounded rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle"> The Rect to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Width/2].</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Height/2].</param>
        public abstract void DrawRoundedRectangle(ABrush pBrush, Pen pPen, Rect pRectangle, double pRadiusX, double pRadiusY);
        
        /// <summary>
        /// Draw an ellipse with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the ellipse.</param>
        /// <param name="pPen">The Pen with which to stroke the ellipse.</param>
        /// <param name="pCenter">The center of the ellipse to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        public abstract void DrawEllipse(ABrush pBrush, Pen pPen, Point pCenter, double pRadiusX, double pRadiusY);

        /// <summary>
        /// Draw a Geometry with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the Geometry.</param>
        /// <param name="pPen">The Pen with which to stroke the Geometry.</param>
        /// <param name="pGeometry"> The Geometry to fill and/or stroke. </param>
        public abstract void DrawGeometry(ABrush pBrush, Pen pPen, AGeometry pGeometry);

        /// <summary>
        /// Draw an Image into the region specified by the Rect.
        /// The Image will potentially be stretched and distorted to fit the Rect.
        /// For more fine grained control, consider filling a Rect with an ImageBrush via DrawRectangle.
        /// </summary>
        /// <param name="pImage">The image to draw.</param>
        /// <param name="pRectangle">The Rect into which the image will be fit.</param>
        public abstract void DrawImage(MagickImage pImage, Rect pRectangle);
        
        /// <summary>
        /// Push an opacity which will blend the composite of all drawing primitives added 
        /// until the corresponding Pop call.
        /// </summary>
        /// <param name="pOpacity">The opacity with which to blend - 0 is transparent, 1 is opaque.</param>
        public abstract void PushOpacity(double pOpacity);
        
        /// <summary>
        /// Push a Transform which will apply to all drawing operations until the corresponding Pop.
        /// </summary>
        /// <param name="pTransform">The Transform to push.</param>
        public abstract void PushTransform(ATransform pTransform);
        
        /// <summary>
        /// Pop the previously pushed drawing context elements.
        /// </summary>
        public abstract void Pop();

        /// <summary>
        /// Closes the context.
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if
                ( this.mIsDisposed == false )
            {
                this.OnDispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected virtual void OnDispose()
        {
            // Nothing to do.
        }

        #endregion Methods
    }
}
