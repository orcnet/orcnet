﻿namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Definition of the <see cref="DashStyles"/> class.
    /// </summary>
    public static class DashStyles
    {
        #region Fields

        /// <summary>
        /// Stores the Solid dash lStyle.
        /// </summary>
        private static DashStyle sSolid;

        /// <summary>
        /// Stores the simple dashed dash lStyle.
        /// </summary>
        private static DashStyle sDash;

        /// <summary>
        /// Stores the dotted dash lStyle.
        /// </summary>
        private static DashStyle sDot;

        /// <summary>
        /// Stores the dashed/dotted dash lStyle.
        /// </summary>
        private static DashStyle sDashDot;

        /// <summary>
        /// Stores the dashed/dotted/dotted dash lStyle.
        /// </summary>
        private static DashStyle sDashDotDot;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Solid - A solid DashArray (no dashes).
        /// </summary>
        public static DashStyle Solid
        {
            get
            {
                if (sSolid == null)
                {
                    DashStyle solid = new DashStyle();
                    sSolid = solid;
                }

                return sSolid;
            }
        }

        /// <summary>
        /// Dash - A DashArray which is 2 on, 2 off
        /// </summary>
        public static DashStyle Dash
        {
            get
            {
                if (sDash == null)
                {
                    DashStyle lStyle = new DashStyle(new ushort[] { 2, 2 }, 1);
                    sDash = lStyle;
                }

                return sDash;
            }
        }

        /// <summary>
        /// Dot - A DashArray which is 0 on, 2 off
        /// </summary>
        public static DashStyle Dot
        {
            get
            {
                if (sDot == null)
                {
                    DashStyle lStyle = new DashStyle(new ushort[] { 0, 2 }, 0);
                    sDot = lStyle;
                }

                return sDot;
            }
        }

        /// <summary>
        /// DashDot - A DashArray which is 2 on, 2 off, 0 on, 2 off
        /// </summary>
        public static DashStyle DashDot
        {
            get
            {
                if (sDashDot == null)
                {
                    DashStyle lStyle = new DashStyle(new ushort[] { 2, 2, 0, 2 }, 1);
                    sDashDot = lStyle;
                }

                return sDashDot;
            }
        }

        /// <summary>
        /// DashDot - A DashArray which is 2 on, 2 off, 0 on, 2 off, 0 on, 2 off
        /// </summary>
        public static DashStyle DashDotDot
        {
            get
            {
                if (sDashDotDot == null)
                {
                    DashStyle lStyle = new DashStyle(new ushort[] { 2, 2, 0, 2, 0, 2 }, 1);
                    sDashDotDot = lStyle;
                }

                return sDashDotDot;
            }
        }

        #endregion Properties
    }
}
