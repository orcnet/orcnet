﻿using OrcNet.Graphics.UI.Helpers;
using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Rectangle based geometry class definition.
    /// </summary>
    public sealed class RectangleGeometry : AGeometry
    {
        #region Fields

        /// <summary>
        /// Stores the corner radius on the X axis.
        /// </summary>
        private double mRadiusX;

        /// <summary>
        /// Stores the corner radius on the Y axis.
        /// </summary>
        private double mRadiusY;

        /// <summary>
        /// Stores the geometry rectangle.
        /// </summary>
        private Rect mRectangle;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the corner radius on the X axis.
        /// </summary>
        public double RadiusX
        {
            get
            {
                return this.mRadiusX;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mRadiusX, value, "RadiusX" );
            }
        }

        /// <summary>
        /// Gets or sets the corner radius on the Y axis.
        /// </summary>
        public double RadiusY
        {
            get
            {
                return this.mRadiusY;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mRadiusY, value, "RadiusY" );
            }
        }

        /// <summary>
        /// Gets or sets the geometry rectangle.
        /// </summary>
        public Rect Rectangle
        {
            get
            {
                return this.mRectangle;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mRectangle, value, "Rectangle" );
            }
        }

        /// <summary>
        /// Gets the bounds of this Geometry as an axis-aligned bounding box
        /// </summary>
        public override Rect Bounds
        {
            get
            {
                Rect lBounds;
                
                ATransform lThisTransform = this.Transform;
                if 
                    ( this.mRectangle.IsEmpty )
                {
                    lBounds = Rect.Empty;
                }
                else if 
                    ( lThisTransform == null || lThisTransform.IsIdentity )
                {
                    lBounds = this.mRectangle;
                }
                else
                {
                    double lRadiusX = this.mRadiusX;
                    double lRadiusY = this.mRadiusY;

                    if 
                        ( lRadiusX == 0 && 
                          lRadiusY == 0 )
                    {
                        lBounds = this.mRectangle;
                        lThisTransform.TransformRect( ref lBounds );
                    }
                    else
                    {
                        // Transformed rounded rectangles are more tricky.
                        Matrix lGeometryMatrix;

                        ATransform.GetTransformValue( lThisTransform, out lGeometryMatrix );

                        lBounds = GeometryHelper.GetBoundsHelper( null /* no pen */,
                                                                  Matrix.Identity,
                                                                  this.mRectangle,
                                                                  lRadiusX,
                                                                  lRadiusY,
                                                                  lGeometryMatrix );
                    }
                }

                return lBounds;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of <see cref="RectangleGeometry"/> class.
        /// </summary>
        public RectangleGeometry() :
        this( Rect.Empty, 0.0, 0.0, ATransform.Identity )
        {

        }

        /// <summary>
        /// Initializes a new instance of <see cref="RectangleGeometry"/> class.
        /// </summary>
        /// <param name="pRectangle">The rectangle region.</param>
        public RectangleGeometry(Rect pRectangle) :
        this( pRectangle, 0.0, 0.0, ATransform.Identity )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of <see cref="RectangleGeometry"/> class.
        /// </summary>
        /// <param name="pRectangle">The rectangle region.</param>
        /// <param name="pRadiusX">The corner radius on the X axis.</param>
        /// <param name="pRadiusY">The corner radius on the Y axis.</param>
        public RectangleGeometry(Rect pRectangle, double pRadiusX, double pRadiusY) :
        this( pRectangle, pRadiusX, pRadiusY, ATransform.Identity )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of <see cref="RectangleGeometry"/> class.
        /// </summary>
        /// <param name="pRectangle">The rectangle region.</param>
        /// <param name="pRadiusX">The corner radius on the X axis.</param>
        /// <param name="pRadiusY">The corner radius on the Y axis.</param>
        /// <param name="pTransform">The geometry transform.</param>
        public RectangleGeometry(Rect pRectangle, double pRadiusX, double pRadiusY, ATransform pTransform)
        {
            this.mRectangle = pRectangle;
            this.mRadiusX   = pRadiusX;
            this.mRadiusY   = pRadiusY;
            this.Transform  = pTransform;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Returns true if this geometry may have curved segments
        /// </summary>
        public override bool MayHaveCurves()
        {
            return this.IsRounded();
        }

        /// <summary>
        /// This is not meant to be used generically since not all geometries implement
        /// the method (currently only RectangleGeometry is implemented).
        /// </summary>
        /// <param name="pGeometry">The other geometry to test against.</param>
        internal override bool AreClose(AGeometry pGeometry)
        {
            RectangleGeometry lOtherGeometry = pGeometry as RectangleGeometry;
            if 
                ( lOtherGeometry != null )
            {
                RectangleGeometry lThisGeometry = this;
                Rect lThisRectangle  = lThisGeometry.mRectangle;
                Rect lOtherRectangle = lOtherGeometry.mRectangle;

                return (DoubleUtil.AreClose( lThisRectangle.X, lOtherRectangle.X ) &&
                        DoubleUtil.AreClose( lThisRectangle.Y, lOtherRectangle.Y ) &&
                        DoubleUtil.AreClose( lThisRectangle.Width, lOtherRectangle.Width ) &&
                        DoubleUtil.AreClose( lThisRectangle.Height, lOtherRectangle.Height ) &&
                        DoubleUtil.AreClose( lThisGeometry.RadiusX, lOtherGeometry.RadiusX ) &&
                        DoubleUtil.AreClose( lThisGeometry.RadiusY, lOtherGeometry.RadiusY ) &&
                        (lThisGeometry.Transform == lOtherGeometry.Transform) );
            }

            return base.AreClose( pGeometry );
        }

        /// <summary>
        /// Returns true if this geometry is empty
        /// </summary>
        public override bool IsEmpty()
        {
            return this.mRectangle.IsEmpty;
        }

        /// <summary>
        /// Returns if geometry is inside this geometry.
        /// </summary>
        /// <param name="pGeometry">The geometry to check.</param>
        public override HitResultDetail FillContainsWithDetail(AGeometry pGeometry)
        {
            // Rectangle to Line.
            LineGeometry lLineCast = pGeometry as LineGeometry;
            if
                ( lLineCast != null )
            {
                Point[] lThisPolygon = GeometryHelper.GetRectanglePointList( this.mRectangle, this.mRadiusX, this.mRadiusY );
                bool lIsStartPointInPolygon = IntersectionHelper.IsPointInPolygon( lLineCast.StartPoint, lThisPolygon );
                bool lIsEndPointInPolygon   = IntersectionHelper.IsPointInPolygon( lLineCast.EndPoint, lThisPolygon );
                if
                    ( lIsStartPointInPolygon && 
                      lIsEndPointInPolygon )
                {
                    return HitResultDetail.FullyContains;
                }
                else if
                    ( lIsStartPointInPolygon || 
                      lIsEndPointInPolygon )
                {
                    return HitResultDetail.Intersects;
                }

                // If none is inside, check for simple intersection.
                double lNearIntersection = 0.0;
                double lFarIntersection  = 1.0;
                if
                    ( IntersectionHelper.IsIntersectingPolygon( lThisPolygon, lLineCast.StartPoint, lLineCast.EndPoint, ref lNearIntersection, ref lFarIntersection ) )
                {
                    return HitResultDetail.Intersects;
                }
            }

            // Rectangle to Rectangle.
            RectangleGeometry lRectangleCast = pGeometry as RectangleGeometry;
            if
                ( lRectangleCast != null )
            {
                Point[] lThisPolygon  = GeometryHelper.GetRectanglePointList( this.mRectangle, this.mRadiusX, this.mRadiusY );
                Point[] lOtherPolygon = GeometryHelper.GetRectanglePointList( lRectangleCast.mRectangle, lRectangleCast.mRadiusX, lRectangleCast.mRadiusY );
                if
                    ( IntersectionHelper.IsIntersectingPolygon( lThisPolygon, lOtherPolygon ) )
                {
                    return HitResultDetail.Intersects;
                }
            }

            // Rectangle to Ellipse.
            EllipseGeometry lEllipseCast = pGeometry as EllipseGeometry;
            if
                ( lEllipseCast != null )
            {
                Point[] lThisPolygon  = GeometryHelper.GetRectanglePointList( this.mRectangle, this.mRadiusX, this.mRadiusY );
                Point[] lOtherPolygon = GeometryHelper.GetEllipsePointList( lEllipseCast.Center, lEllipseCast.RadiusX, lEllipseCast.RadiusY );
                if
                    ( IntersectionHelper.IsIntersectingPolygon( lThisPolygon, lOtherPolygon ) )
                {
                    return HitResultDetail.Intersects;
                }
            }

            return HitResultDetail.NotCalculated;
        }

        /// <summary>
        /// Returns if a given geometry is inside the stroke defined by a given pen on this geometry.
        /// </summary>
        /// <param name="pPen">The pen used to stroke.</param>
        /// <param name="pGeometry">The geometry to check.</param>
        public override HitResultDetail StrokeContainsWithDetail(Pen pPen, AGeometry pGeometry)
        {
            // For now same as FillContainsWithDetail.
            return this.FillContainsWithDetail( pGeometry );
        }

        /// <summary>
        /// Returns the axis-aligned bounding rectangle when stroked with a pen, after applying
        /// the supplied transform (if non-null).
        /// </summary>
        /// <param name="pPen">The pen used to stroke.</param>
        /// <param name="pWorldMatrix">The world transformation to apply.</param>
        internal override Rect GetBoundsInternal(Pen pPen, Matrix pWorldMatrix)
        {
            Matrix lGeometryMatrix;

            ATransform.GetTransformValue( this.Transform, out lGeometryMatrix );

            return GeometryHelper.GetBoundsHelper( pPen,
                                                   pWorldMatrix,
                                                   this.mRectangle,
                                                   this.mRadiusX,
                                                   this.mRadiusY,
                                                   lGeometryMatrix );
        }

        /// <summary>
        /// Returns true if point is inside this geometry.
        /// </summary>
        /// <param name="pPen"></param>
        /// <param name="pHitPoint"></param>
        /// <returns>True of contains the point, false otherwise.</returns>
        internal override bool ContainsInternal(Pen pPen, Point pHitPoint)
        {
            if 
                ( this.IsEmpty() )
            {
                return false;
            }

            Point[] lPolygon = GeometryHelper.GetRectanglePointList( this.mRectangle, this.mRadiusX, this.mRadiusY );
            return IntersectionHelper.IsPointInPolygon( pHitPoint, lPolygon );
        }

        /// <summary>
        /// Clone the geometry.
        /// </summary>
        /// <returns>The cloned geometry</returns>
        public override AGeometry Clone()
        {
            return new RectangleGeometry( this.mRectangle, this.mRadiusX, this.mRadiusY, this.Transform.Clone() as ATransform );
        }

        /// <summary>
        /// Creates an empty geometry model.
        /// </summary>
        /// <returns>The empty geometry.</returns>
        protected override AGeometry MakeEmptyGeometry()
        {
            return new RectangleGeometry();
        }

        /// <summary>
        /// Checks whether the passed radius induces the shape is rounded.
        /// </summary>
        /// <param name="pRadiusX">The corner radius on the X axis.</param>
        /// <param name="pRadiusY">The corner radius on the Y axis.</param>
        /// <returns>True if rounded, false otherwise.</returns>
        internal static bool IsRounded(double pRadiusX, double pRadiusY)
        {
            return pRadiusX != 0.0 && 
                   pRadiusY != 0.0;
        }

        /// <summary>
        /// Checks whether the passed radius induces the shape is rounded.
        /// </summary>
        /// <returns>True if rounded, false otherwise.</returns>
        internal bool IsRounded()
        {
            return this.mRadiusX != 0.0 && 
                   this.mRadiusY != 0.0;
        }

        #endregion Methods
    }
}
