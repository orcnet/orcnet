﻿using ImageMagick;
using OrcNet.Core.Logger;
using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Base drawing context specific to that UI module.
    /// </summary>
    internal abstract class ABaseDrawingContext : ADrawingContext
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the drawing context must stop walking through the tree or not.
        /// </summary>
        private bool mMustStopWalking;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets  the flag indicating whether the drawing context must stop walking through the tree or not.
        /// </summary>
        internal bool MustStopWalking
        {
            get
            {
                return this.mMustStopWalking;
            }
            set
            {
                this.mMustStopWalking = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ABaseDrawingContext"/> class.
        /// </summary>
        protected ABaseDrawingContext()
        {
            
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Draws a line with the specified pen.
        /// </summary>
        /// <param name="pPen"> The Pen with which to stroke the line. </param>
        /// <param name="pStart"> The start Point for the line. </param>
        /// <param name="pEnd"> The end Point for the line. </param>
        public override void DrawLine(Pen pPen, Point pStart, Point pEnd)
        {
            LogManager.Instance.Log( "DrawLine not implemented!!!" );
        }
        
        /// <summary>
        /// Draw a rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle">The Rect to fill and/or stroke.</param>
        public override void DrawRectangle(ABrush pBrush, Pen pPen, Rect pRectangle)
        {
            LogManager.Instance.Log( "DrawRectangle not implemented!!!" );
        }
        
        /// <summary>
        /// Draw a rounded rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle"> The Rect to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Width/2].</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Height/2].</param>
        public override void DrawRoundedRectangle(ABrush pBrush, Pen pPen, Rect pRectangle, double pRadiusX, double pRadiusY)
        {
            LogManager.Instance.Log( "DrawRoundedRectangle not implemented!!!" );
        }
        
        /// <summary>
        /// Draw an ellipse with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the ellipse.</param>
        /// <param name="pPen">The Pen with which to stroke the ellipse.</param>
        /// <param name="pCenter">The center of the ellipse to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        public override void DrawEllipse(ABrush pBrush, Pen pPen, Point pCenter, double pRadiusX, double pRadiusY)
        {
            LogManager.Instance.Log( "DrawEllipse not implemented!!!" );
        }

        /// <summary>
        /// Draw a Geometry with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the Geometry.</param>
        /// <param name="pPen">The Pen with which to stroke the Geometry.</param>
        /// <param name="pGeometry"> The Geometry to fill and/or stroke. </param>
        public override void DrawGeometry(ABrush pBrush, Pen pPen, AGeometry pGeometry)
        {
            LogManager.Instance.Log( "DrawGeometry not implemented!!!" );
        }

        /// <summary>
        /// Draw an Image into the region specified by the Rect.
        /// The Image will potentially be stretched and distorted to fit the Rect.
        /// For more fine grained control, consider filling a Rect with an ImageBrush via DrawRectangle.
        /// </summary>
        /// <param name="pImage">The image to draw.</param>
        /// <param name="pRectangle">The Rect into which the image will be fit.</param>
        public override void DrawImage(MagickImage pImage, Rect pRectangle)
        {
            LogManager.Instance.Log( "DrawImage not implemented!!!" );
        }
        
        /// <summary>
        /// Push an opacity which will blend the composite of all drawing primitives added 
        /// until the corresponding Pop call.
        /// </summary>
        /// <param name="pOpacity">The opacity with which to blend - 0 is transparent, 1 is opaque.</param>
        public override void PushOpacity(double pOpacity)
        {
            LogManager.Instance.Log( "PushOpacity not implemented!!!" );
        }
        
        /// <summary>
        /// Push a Transform which will apply to all drawing operations until the corresponding Pop.
        /// </summary>
        /// <param name="pTransform">The Transform to push.</param>
        public override void PushTransform(ATransform pTransform)
        {
            LogManager.Instance.Log( "PushTransform not implemented!!!" );
        }
        
        /// <summary>
        /// Pop the previously pushed drawing context elements.
        /// </summary>
        public override void Pop()
        {
            LogManager.Instance.Log( "Pop not implemented!!!" );
        }

        /// <summary>
        /// Close the context.
        /// </summary>
        public sealed override void Close()
        {
             LogManager.Instance.Log( "Closing drawing context!!!" );
        }

        /// <summary>
        /// Stop the drawing context visual tree walk.
        /// </summary>
        protected void StopWalking()
        {
            this.mMustStopWalking = true;
        }

        #endregion Methods
    }
}
