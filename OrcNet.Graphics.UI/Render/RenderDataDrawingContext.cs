﻿using ImageMagick;
using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// The drawing context in charge of drawing data to render.
    /// </summary>
    internal class RenderDataDrawingContext : ADrawingContext
    {
        #region Fields

        /// <summary>
        /// Stores the amount of stacked elements.
        /// </summary>
        private int mStackDepth;

        /// <summary>
        /// Stores the data to render.
        /// </summary>
        private RenderData mRenderData;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the drawing context memory size.
        /// </summary>
        public override uint Size
        {
            get
            {
                uint lSize = 0;
                return lSize;
            }
        }

        /// <summary>
        /// Gets the data the context has to render.
        /// </summary>
        internal RenderData RenderData
        {
            get
            {
                return this.mRenderData;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderDataDrawingContext"/> class.
        /// </summary>
        internal RenderDataDrawingContext()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Draws a line with the specified pen.
        /// </summary>
        /// <param name="pPen">The Pen with which to stroke the line.</param>
        /// <param name="pStart">The start Point for the line.</param>
        /// <param name="pEnd">The end Point for the line.</param>
        public override void DrawLine(Pen pPen, Point pStart, Point pEnd)
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.Line, 
                                            pPen, 
                                            pStart, 
                                            pEnd );
        }

        /// <summary>
        /// Draw a rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle">The Rect to fill and/or stroke.</param>
        public override void DrawRectangle(ABrush pBrush, Pen pPen, Rect pRectangle)
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.Rectangle, 
                                            pBrush, 
                                            pPen, 
                                            pRectangle );
        }

        /// <summary>
        /// Draw a rounded rectangle with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the rectangle.</param>
        /// <param name="pPen">The Pen with which to stroke the rectangle.</param>
        /// <param name="pRectangle"> The Rect to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Width/2].</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the rounded corners of this rounded Rect.  This value will be clamped to the range [0..rectangle.Height/2].</param>
        public override void DrawRoundedRectangle(ABrush pBrush, Pen pPen, Rect pRectangle, double pRadiusX, double pRadiusY)
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.RoundedRectangle, 
                                            pBrush, 
                                            pPen, 
                                            pRectangle,
                                            pRadiusX,
                                            pRadiusY );
        }

        /// <summary>
        /// Draw an ellipse with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the ellipse.</param>
        /// <param name="pPen">The Pen with which to stroke the ellipse.</param>
        /// <param name="pCenter">The center of the ellipse to fill and/or stroke.</param>
        /// <param name="pRadiusX">The radius in the X dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        /// <param name="pRadiusY">The radius in the Y dimension of the ellipse. The absolute value of the radius provided will be used.</param>
        public override void DrawEllipse(ABrush pBrush, Pen pPen, Point pCenter, double pRadiusX, double pRadiusY)
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.Ellipse, 
                                            pBrush, 
                                            pPen, 
                                            pCenter,
                                            pRadiusX,
                                            pRadiusY );
        }

        /// <summary>
        /// Draw a Geometry with the provided Brush and/or Pen.
        /// </summary>
        /// <param name="pBrush">The Brush with which to fill the Geometry.</param>
        /// <param name="pPen">The Pen with which to stroke the Geometry.</param>
        /// <param name="pGeometry"> The Geometry to fill and/or stroke. </param>
        public override void DrawGeometry(ABrush pBrush, Pen pPen, AGeometry pGeometry)
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.Geometry, 
                                            pBrush, 
                                            pPen, 
                                            pGeometry );
        }

        /// <summary>
        /// Draw an Image into the region specified by the Rect.
        /// The Image will potentially be stretched and distorted to fit the Rect.
        /// For more fine grained control, consider filling a Rect with an ImageBrush via DrawRectangle.
        /// </summary>
        /// <param name="pImage">The image to draw.</param>
        /// <param name="pRectangle">The Rect into which the image will be fit.</param>
        public override void DrawImage(MagickImage pImage, Rect pRectangle)
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.Image, 
                                            pImage, 
                                            pRectangle );
        }

        /// <summary>
        /// Push an opacity which will blend the composite of all drawing primitives added 
        /// until the corresponding Pop call.
        /// </summary>
        /// <param name="pOpacity">The opacity with which to blend - 0 is transparent, 1 is opaque.</param>
        public override void PushOpacity(double pOpacity)
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.PushOpacity, 
                                            pOpacity );

            this.mStackDepth++;
        }

        /// <summary>
        /// Push a Transform which will apply to all drawing operations until the corresponding Pop.
        /// </summary>
        /// <param name="pTransform">The Transform to push.</param>
        public override void PushTransform(ATransform pTransform)
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.PushTransform, 
                                            pTransform );

            this.mStackDepth++;
        }

        /// <summary>
        /// Pop the previously pushed drawing context elements.
        /// </summary>
        public override void Pop()
        {
            this.EnsureRenderData();

            this.mRenderData.AddDataToDraw( DrawCommand.Pop, null );

            this.mStackDepth--;
        }

        /// <summary>
        /// Closes the context.
        /// </summary>
        public override void Close()
        {
            this.Dispose();
        }

        /// <summary>
        /// Custom drawing context close.
        /// </summary>
        /// <param name="pData">The render data.</param>
        protected virtual void CustomClose(RenderData pData)
        {
            // To override.
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDispose()
        {
            this.EnsureCorrectNesting();

            this.CustomClose( this.mRenderData );

            base.OnDispose();
        }

        /// <summary>
        /// Checks whether the render data cache exists. 
        /// </summary>
        private void EnsureRenderData()
        {
            if 
                ( this.mRenderData == null )
            {
                this.mRenderData = new RenderData();
            }
        }

        /// <summary>
        /// Checks if there were any Push calls that were not matched by
        /// the corresponding Pop. If this is the case, this method adds corresponding
        /// number of Pop instructions to the render data.
        /// </summary>
        private void EnsureCorrectNesting()
        {
            if 
                ( this.mRenderData != null && 
                  this.mStackDepth > 0 )
            {
                int lStackDepth = this.mStackDepth;
                for 
                    ( int i = 0; i < lStackDepth; i++ )
                {
                    this.Pop();
                }
            }

            this.mStackDepth = 0;
        }

        #endregion Methods
    }
}
