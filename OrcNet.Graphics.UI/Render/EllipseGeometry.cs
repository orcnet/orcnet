﻿using OrcNet.Graphics.UI.Helpers;
using OrcNet.Graphics.UI.Maths;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Ellipse based geometry class definition.
    /// </summary>
    public sealed class EllipseGeometry : AGeometry
    {
        #region Fields

        /// <summary>
        /// Stores the corner radius on the X axis.
        /// </summary>
        private double mRadiusX;

        /// <summary>
        /// Stores the corner radius on the Y axis.
        /// </summary>
        private double mRadiusY;

        /// <summary>
        /// Stores the ellipse center point.
        /// </summary>
        private Point  mCenter;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the corner radius on the X axis.
        /// </summary>
        public double RadiusX
        {
            get
            {
                return this.mRadiusX;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mRadiusX, value, "RadiusX" );
            }
        }

        /// <summary>
        /// Gets or sets the corner radius on the Y axis.
        /// </summary>
        public double RadiusY
        {
            get
            {
                return this.mRadiusY;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mRadiusY, value, "RadiusY" );
            }
        }

        /// <summary>
        /// Gets or sets the ellipse center point.
        /// </summary>
        public Point Center
        {
            get
            {
                return this.mCenter;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCenter, value, "Center" );
            }
        }

        /// <summary>
        /// Gets the bounds of this Geometry as an axis-aligned bounding box
        /// </summary>
        public override Rect Bounds
        {
            get
            {
                Rect lBounds;

                ATransform lThisTransform = this.Transform;
                if 
                    ( lThisTransform == null || 
                      lThisTransform.IsIdentity )
                {
                    Point lCurrentCenter   = this.mCenter;
                    double lCurrentRadiusX = this.mRadiusX;
                    double lCurrentRadiusY = this.mRadiusY;

                    lBounds = new Rect( lCurrentCenter.X - System.Math.Abs( lCurrentRadiusX ),
                                        lCurrentCenter.Y - System.Math.Abs( lCurrentRadiusY ),
                                        2.0 * System.Math.Abs( lCurrentRadiusX ),
                                        2.0 * System.Math.Abs( lCurrentRadiusY ) );
                }
                else
                {
                    Matrix lGeometryMatrix;

                    ATransform.GetTransformValue( lThisTransform, out lGeometryMatrix );

                    lBounds = GeometryHelper.GetBoundsHelper( null /* no pen */,
                                                              Matrix.Identity,
                                                              this.mCenter,
                                                              this.mRadiusX,
                                                              this.mRadiusY,
                                                              lGeometryMatrix );
                }

                return lBounds;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of <see cref="EllipseGeometry"/> class.
        /// </summary>
        public EllipseGeometry() :
        this( new Point(), 0.0, 0.0, ATransform.Identity )
        {

        }

        /// <summary>
        /// Initializes a new instance of <see cref="EllipseGeometry"/> class.
        /// </summary>
        /// <param name="pRectangle">The rectangle region.</param>
        public EllipseGeometry(Rect pRectangle)
        {
            if
                ( pRectangle.IsEmpty == false )
            {
                this.mRadiusX = (pRectangle.Right - pRectangle.X) * (1.0 / 2.0);
                this.mRadiusY = (pRectangle.Bottom - pRectangle.Y) * (1.0 / 2.0);
                this.mCenter = new Point( pRectangle.X + this.mRadiusX, pRectangle.Y + this.mRadiusY );
            }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="EllipseGeometry"/> class.
        /// </summary>
        /// <param name="pCenter">The ellipse center point.</param>
        /// <param name="pRadiusX">The corner radius on the X axis.</param>
        /// <param name="pRadiusY">The corner radius on the Y axis.</param>
        public EllipseGeometry(Point pCenter, double pRadiusX, double pRadiusY) :
        this( pCenter, pRadiusX, pRadiusY, ATransform.Identity )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of <see cref="EllipseGeometry"/> class.
        /// </summary>
        /// <param name="pCenter">The ellipse center point.</param>
        /// <param name="pRadiusX">The corner radius on the X axis.</param>
        /// <param name="pRadiusY">The corner radius on the Y axis.</param>
        /// <param name="pTransform">The geometry transform.</param>
        public EllipseGeometry(Point pCenter, double pRadiusX, double pRadiusY, ATransform pTransform)
        {
            this.mCenter   = pCenter;
            this.mRadiusX  = pRadiusX;
            this.mRadiusY  = pRadiusY;
            this.Transform = pTransform;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Returns true if this geometry may have curved segments
        /// </summary>
        public override bool MayHaveCurves()
        {
            return true;
        }
        
        /// <summary>
        /// Returns true if this geometry is empty
        /// </summary>
        public override bool IsEmpty()
        {
            return false;
        }

        /// <summary>
        /// Returns if geometry is inside this geometry.
        /// </summary>
        /// <param name="pGeometry">The geometry to check.</param>
        public override HitResultDetail FillContainsWithDetail(AGeometry pGeometry)
        {
            // Ellipse to Line.
            LineGeometry lLineCast = pGeometry as LineGeometry;
            if
                ( lLineCast != null )
            {
                Point[] lThisPolygon = GeometryHelper.GetEllipsePointList( this.mCenter, this.mRadiusX, this.mRadiusY );
                bool lIsStartPointInPolygon = IntersectionHelper.IsPointInPolygon( lLineCast.StartPoint, lThisPolygon );
                bool lIsEndPointInPolygon   = IntersectionHelper.IsPointInPolygon( lLineCast.EndPoint, lThisPolygon );
                if
                    ( lIsStartPointInPolygon && 
                      lIsEndPointInPolygon )
                {
                    return HitResultDetail.FullyContains;
                }
                else if
                    ( lIsStartPointInPolygon || 
                      lIsEndPointInPolygon )
                {
                    return HitResultDetail.Intersects;
                }

                // If none is inside, check for simple intersection.
                double lNearIntersection = 0.0;
                double lFarIntersection  = 1.0;
                if
                    ( IntersectionHelper.IsIntersectingPolygon( lThisPolygon, lLineCast.StartPoint, lLineCast.EndPoint, ref lNearIntersection, ref lFarIntersection ) )
                {
                    return HitResultDetail.Intersects;
                }
            }

            // Ellipse to Rectangle.
            RectangleGeometry lRectangleCast = pGeometry as RectangleGeometry;
            if
                ( lRectangleCast != null )
            {
                Point[] lThisPolygon  = GeometryHelper.GetEllipsePointList( this.mCenter, this.mRadiusX, this.mRadiusY );
                Point[] lOtherPolygon = GeometryHelper.GetRectanglePointList( lRectangleCast.Rectangle, lRectangleCast.RadiusX, lRectangleCast.RadiusY );
                if
                    ( IntersectionHelper.IsIntersectingPolygon( lThisPolygon, lOtherPolygon ) )
                {
                    return HitResultDetail.Intersects;
                }
            }

            // Ellipse to Ellipse.
            EllipseGeometry lEllipseCast = pGeometry as EllipseGeometry;
            if
                ( lEllipseCast != null )
            {
                Point[] lThisPolygon  = GeometryHelper.GetEllipsePointList( this.mCenter, this.mRadiusX, this.mRadiusY );
                Point[] lOtherPolygon = GeometryHelper.GetEllipsePointList( lEllipseCast.mCenter, lEllipseCast.mRadiusX, lEllipseCast.mRadiusY );
                if
                    ( IntersectionHelper.IsIntersectingPolygon( lThisPolygon, lOtherPolygon ) )
                {
                    return HitResultDetail.Intersects;
                }
            }

            return HitResultDetail.NotCalculated;
        }

        /// <summary>
        /// Returns if a given geometry is inside the stroke defined by a given pen on this geometry.
        /// </summary>
        /// <param name="pPen">The pen used to stroke.</param>
        /// <param name="pGeometry">The geometry to check.</param>
        public override HitResultDetail StrokeContainsWithDetail(Pen pPen, AGeometry pGeometry)
        {
            // For now same as FillContainsWithDetail.
            return this.FillContainsWithDetail( pGeometry );
        }

        /// <summary>
        /// Returns the axis-aligned bounding rectangle when stroked with a pen, after applying
        /// the supplied transform (if non-null).
        /// </summary>
        /// <param name="pPen">The pen used to stroke.</param>
        /// <param name="pWorldMatrix">The world transformation to apply.</param>
        internal override Rect GetBoundsInternal(Pen pPen, Matrix pWorldMatrix)
        {
            Matrix lGeometryMatrix;

            ATransform.GetTransformValue( this.Transform, out lGeometryMatrix );

            return GeometryHelper.GetBoundsHelper( pPen,
                                                   pWorldMatrix,
                                                   this.mCenter,
                                                   this.mRadiusX,
                                                   this.mRadiusY,
                                                   lGeometryMatrix );
        }

        /// <summary>
        /// Returns true if point is inside this geometry.
        /// </summary>
        /// <param name="pPen"></param>
        /// <param name="pHitPoint"></param>
        /// <returns>True of contains the point, false otherwise.</returns>
        internal override bool ContainsInternal(Pen pPen, Point pHitPoint)
        {
            Point[] lPolygon = GeometryHelper.GetEllipsePointList( this.mCenter, this.mRadiusX, this.mRadiusY );
            return IntersectionHelper.IsPointInPolygon( pHitPoint, lPolygon );
        }

        /// <summary>
        /// Clone the geometry.
        /// </summary>
        /// <returns>The cloned geometry</returns>
        public override AGeometry Clone()
        {
            return new EllipseGeometry( this.mCenter, this.mRadiusX, this.mRadiusY, this.Transform.Clone() as ATransform );
        }

        /// <summary>
        /// Creates an empty geometry model.
        /// </summary>
        /// <returns>The empty geometry.</returns>
        protected override AGeometry MakeEmptyGeometry()
        {
            return new EllipseGeometry();
        }
        
        #endregion Methods
    }
}
