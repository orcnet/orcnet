﻿using OrcNet.Core;
using OrcNet.Core.Helpers;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Dash style class definition.
    /// </summary>
    public class DashStyle : AObservable
    {
        #region Fields

        /// <summary>
        /// Stores the dashed factor.
        /// </summary>
        private uint mFactor;

        /// <summary>
        /// Stores the array of dashes and gaps for that style.
        /// </summary>
        private ushort[] mDashes;

        /// <summary>
        /// Stores the dash pattern resulting from the supplied Dashes and Gaps supplied.
        /// </summary>
        private ushort mDashPattern;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the dashed factor.
        /// </summary>
        public uint Factor
        {
            get
            {
                return this.mFactor;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mFactor, value, "Factor" );
            }
        }

        /// <summary>
        /// Gets or sets the array of dashes and gaps for that style.
        /// </summary>
        public ushort[] Dashes
        {
            get
            {
                return this.mDashes;
            }
            set
            {
                if
                    ( this.NotifyPropertyChanged( ref this.mDashes, value, "Dashes" ) )
                {
                    this.UpdatePattern( this.mDashes );

                    this.InformPropertyChangedListener( this.mDashPattern, this.mDashPattern, "DashPattern" );
                }
            }
        }

        /// <summary>
        /// Gets the dash pattern resulting from the supplied Dashes and Gaps supplied.
        /// </summary>
        internal ushort DashPattern
        {
            get
            {
                return this.mDashPattern;
            }
        }

        #endregion Porperties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DashStyle"/> class.
        /// </summary>
        public DashStyle() :
        this( null, 1 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DashStyle"/> class.
        /// </summary>
        /// <param name="pDashes">The array of dashes and gaps for that style.</param>
        /// <param name="pFactor">The dashed factor.</param>
        public DashStyle(ushort[] pDashes, uint pFactor)
        {
            this.mDashes = pDashes;
            this.mFactor = pFactor;

            this.UpdatePattern( this.mDashes );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Turns the dashes and gaps info into the internal dash pattern.
        /// </summary>
        /// <param name="pDashes">The dash description.</param>
        /// <returns>The dash pattern.</returns>
        private void UpdatePattern(ushort[] pDashes)
        {
            if
                ( pDashes == null )
            {
                this.mDashPattern = 0xFFFF;
                return;
            }

            int lBitWeight = 0;
            ushort lResult = 0;
            for
                ( int lCurr = 0; lCurr < pDashes.Length; lCurr++ )
            {
                int lFactor  = Utilities.IsOdd( lCurr ) ? 0 : 1;
                ushort lTemp = pDashes[ lCurr ];
                for
                    ( int lBit = 0; lBit < lTemp; lBit++ )
                {
                    lResult += (ushort)(System.Math.Pow( 2, lBitWeight ) * lFactor);
                    lBitWeight++;
                }
            }

            this.mDashPattern = lResult;
        }

        #endregion Methods
    }
}
