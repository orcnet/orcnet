﻿namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Enumerates how the content is vertically placed within a container.
    /// </summary>
    public enum VerticalAlignment
    {
        /// <summary>
        /// Aligns the contents towards the top of the container.
        /// </summary>
        Top = 0,

        /// <summary>
        /// Centers the contents vertically.
        /// </summary>
        Center = 1,

        /// <summary>
        /// Aligns the contents towards the bottom of the container.
        /// </summary>
        Bottom = 2,

        /// <summary>
        /// Stretch the contents vertically whithin the parent's layout.
        /// </summary>
        Stretch = 3
    }
}
