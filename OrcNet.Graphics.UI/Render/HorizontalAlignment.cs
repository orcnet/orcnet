﻿namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Enumerates how the content is horizontally placed within a container.
    /// </summary>
    public enum HorizontalAlignment
    {
        /// <summary>
        /// Aligns the contents towards the left of the container.
        /// </summary>
        Left = 0,

        /// <summary>
        /// Centers the contents horizontally.
        /// </summary>
        Center = 1,

        /// <summary>
        /// Aligns the contents towards the right of the container.
        /// </summary>
        Right = 2,

        /// <summary>
        /// Stretch the contents horizontally whithin the parent's layout.
        /// </summary>
        Stretch = 3
    }
}
