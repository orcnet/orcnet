﻿using OrcNet.Graphics.UI.Maths;
using System;

namespace OrcNet.Graphics.UI.Render
{
    /// <summary>
    /// Base drawing context interface definition.
    /// </summary>
    internal interface IDrawingContent : ICloneable
    {
        #region Methods

        /// <summary>
        /// Gets the bounding box occupied by the content
        /// </summary>
        /// <returns>The bounding box occupied by the content</returns>
        Rect GetContentBounds(BoundsDrawingContext pContext);

        /// <summary>
        /// Forward the current value of the content to the DrawingContext
        /// methods.
        /// </summary>      
        /// <param name="pContext">the context to forward content to.</param>        
        void ForwardContent(ABaseDrawingContext pContext);

        /// <summary>
        /// Determines whether or not a point exists within the content
        /// </summary>    
        /// <param name="pPoint">The point to hit-test for.</param>                
        /// <returns>True if the point exists within the content, False otherwise.</returns>        
        bool HitTestPoint(Point pPoint);

        #endregion Methods
    }
}
