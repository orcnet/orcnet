﻿using OrcNet.Core;
using OrcNet.Graphics.UI.Core;
using System;

namespace OrcNet.Graphics.UI.Services
{
    /// <summary>
    /// Base User Interface interface definition.
    /// </summary>
    public interface IUIService : IService
    {
        #region Events

        /// <summary>
        /// Event triggered each time layout updates occur.
        /// </summary>
        event Action<object, EventArgs> LayoutUpdated;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the UI dispatcher.
        /// </summary>
        Dispatcher Dispatcher
        {
            get;
        }

        /// <summary>
        /// Gets the presentation source.
        /// </summary>
        PresentationSource PresentationSource
        {
            get;
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Updates the layout.
        /// </summary>
        void UpdateLayout();

        /// <summary>
        /// Renders the UI on the screen.
        /// </summary>
        void Render();

        #endregion Methods
    }
}
