﻿using OpenTK;
using OrcNet.Core.Logger;
using OrcNet.Core.Service;
using OrcNet.Graphics.UI.Core;
using OrcNet.Graphics.UI.Maths;
using System;
using System.Collections.Generic;

namespace OrcNet.Graphics.UI.Services
{
    /// <summary>
    /// User interface service class definition
    /// </summary>
    public class UIService : AService, IUIService
    {
        #region Fields

        /// <summary>
        /// Stores the flag indicating whether the service has been disposed or not.
        /// </summary>
        private bool mIsDisposed;

        /// <summary>
        /// Stores the UI dispatcher.
        /// </summary>
        private Dispatcher mDispatcher;

        /// <summary>
        /// Stores the presentation source.
        /// </summary>
        private PresentationSource mPresentationSource;

        /// <summary>
        /// Stores the collection with UI elements to measure.
        /// </summary>
        private ADirtyLayoutCollection mMeasureCollection;

        /// <summary>
        /// Stores the collection with UI elements to arrange.
        /// </summary>
        private ADirtyLayoutCollection mArrangeCollection;

        /// <summary>
        /// Stores the flag indicating whether the layout is being updated or not.
        /// </summary>
        private bool mIsUpdatingLayout;

        /// <summary>
        /// Stores the flag indicating whether the layout update is locked 
        /// to prevent from looping if another call to the very same UpdateLayout method.
        /// </summary>
        private bool mIsUpdateLayoutLocked;
        
        /// <summary>
        /// Stores the amount of element to measure.
        /// </summary>
        private int  mToMeasureCount;

        /// <summary>
        /// Stores the amount of element to arrange.
        /// </summary>
        private int  mToArrangeCount;

        /// <summary>
        /// Stores the current size changed arguments to use on notifications.
        /// </summary>
        private SizeChangedArguments mArguments;

        /// <summary>
        /// Stores the flag indicating whether the layout update event is being notified or not.
        /// </summary>
        private bool mIsNotifyingLayoutUpdated;

        /// <summary>
        /// Stores the flag indicating whether the size changed event is being notified or not.
        /// </summary>
        private bool mIsNotifyingSizeChanged;

        /// <summary>
        /// Stores the flag indicating whether the a Layout update has been requested
        /// see RequestLayoutUpdate().
        /// </summary>
        private bool mHasRequestedLayoutUpdate;

        /// <summary>
        /// Stores the layout listeners.
        /// </summary>
        private List<Action<object, EventArgs>> mLayoutListeners;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event triggered each time layout updates occur.
        /// </summary>
        public event Action<object, EventArgs> LayoutUpdated
        {
            add
            {
                this.mLayoutListeners.Add( value );
            }
            remove
            {
                this.mLayoutListeners.Remove( value );
            }
        }

        #endregion Events

        #region Properties

        #region Properties IService

        /// <summary>
        /// Gets the service's name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "UI Service (Do not use)";
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the service is a core service or not.
        /// </summary>
        public override bool IsCore
        {
            get
            {
                return true;
            }
        }

        #endregion Properties IService

        /// <summary>
        /// Gets the flag indicating whether the layout must be re evaluated or not.
        /// </summary>
        private bool IsLayoutDirty
        {
            get
            {
                return this.mMeasureCollection.IsEmpty == false &&
                       this.mArrangeCollection.IsEmpty == false;
            }
        }

        /// <summary>
        /// Gets the UI dispatcher.
        /// </summary>
        public Dispatcher Dispatcher
        {
            get
            {
                return this.mDispatcher;
            }
        }

        /// <summary>
        /// Gets the presentation source.
        /// </summary>
        public PresentationSource PresentationSource
        {
            get
            {
                return this.mPresentationSource;
            }
        }

        /// <summary>
        /// Sets the UI presentation source
        /// NOTE: Support a single window for NOW.
        /// </summary>
        internal INativeWindow Source
        {
            set
            {
                if
                    ( this.mPresentationSource == null )
                {
                    this.mPresentationSource = new PresentationSource( value );
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UIService"/> class.
        /// </summary>
        public UIService()
        {
            this.mIsDisposed = false;
            this.mDispatcher = new Dispatcher( "UI Dispatcher" );

            this.mMeasureCollection = new DirtyMeasureCollection();
            this.mArrangeCollection = new DirtyArrangeCollection();

            this.mLayoutListeners = new List<Action<object, EventArgs>>();
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal
        
        /// <summary>
        /// Invalidate the element and overall hierarchy for measurement.
        /// </summary>
        /// <param name="pElement">The element the tree must be re evaluated.</param>
        private void InvalidateElementHierarchy(UIElement pElement)
        {
            // Get the top most parent.
            UIElement lParent;
            UIElement lElement = pElement;
            lElement.GetUIParent( out lParent );
            while 
                ( lParent != null )
            {
                lElement.GetUIParent( out lParent );
                if
                    ( lParent != null )
                {
                    lElement = lParent;
                }
            }

            this.InvalidateTree( lElement );
            this.mMeasureCollection.Add( lElement );
            this.mArrangeCollection.Add( lElement );
        }

        /// <summary>
        /// Invalidate the overall visual tree for re evaluation.
        /// </summary>
        /// <param name="pVisual">The top most visual element the tree must be invalidated.</param>
        private void InvalidateTree(AVisual pVisual)
        {
            if 
                ( pVisual != null )
            {
                if 
                    ( pVisual.CheckFlagsAnd( VisualFlags.IsUIElement ) )
                {
                    UIElement lUICast = pVisual as UIElement;
                    lUICast.InvalidateMeasure();
                    lUICast.InvalidateArrange();
                }

                int lCount = pVisual.VisualChildrenCount;
                for
                    ( int lCurrent = 0; lCurrent < lCount; lCurrent++ )
                {
                    AVisual lChild = pVisual.GetVisualChild( lCurrent );
                    if 
                        ( lChild != null )
                    {
                        this.InvalidateTree( lChild );
                    }
                }
            }
        }

        /// <summary>
        /// Pre process of the measurement.
        /// </summary>
        internal void PreMeasure()
        {
            this.mToMeasureCount++;
        }

        /// <summary>
        /// Pre process of the measurement.
        /// </summary>
        internal void PostMeasure()
        {
            this.mToMeasureCount--;
        }

        /// <summary>
        /// Adds a new measure request for the supplied UI element.
        /// </summary>
        /// <param name="pToMeasure">The UI element to measure.</param>
        internal void RequestMeasurement(UIElement pToMeasure)
        {
            this.mMeasureCollection.Add( pToMeasure );
        }

        /// <summary>
        /// Stop requesting a UI element measurement.
        /// </summary>
        /// <param name="pToStopMeasuring">The UI element measurement must be stopped for.</param>
        internal void StopMeasurement(UIElement pToStopMeasuring)
        {
            this.mMeasureCollection.Remove( pToStopMeasuring );
        }

        /// <summary>
        /// Adds a new arrange request for the supplied UI element.
        /// </summary>
        /// <param name="pToArrange">The UI element to arrange.</param>
        internal void RequestArrangement(UIElement pToArrange)
        {
            this.mArrangeCollection.Add( pToArrange );
        }

        /// <summary>
        /// Stop requesting a UI element arrangement.
        /// </summary>
        /// <param name="pToStopArranging">The UI element arrangement must be stopped for.</param>
        internal void StopArrangement(UIElement pToStopArranging)
        {
            this.mArrangeCollection.Remove( pToStopArranging );
        }

        /// <summary>
        /// Pre process of the arrangement.
        /// </summary>
        internal void PreArrange()
        {
            this.mToArrangeCount++;
        }

        /// <summary>
        /// Pre process of the arrangement.
        /// </summary>
        internal void PostArrange()
        {
            this.mToArrangeCount--;
        }

        /// <summary>
        /// Requests a layout update by dispatching it to the UI thread.
        /// </summary>
        internal void RequestLayoutUpdate()
        {
            if
                ( this.mHasRequestedLayoutUpdate ||
                  this.mIsUpdatingLayout ) // Skip if already doing it.
            {
                return;
            }

            this.Dispatcher.Invoke( this.UpdateLayout );
            this.mHasRequestedLayoutUpdate = true;
        }

        /// <summary>
        /// Updates the layout.
        /// </summary>
        public void UpdateLayout()
        {
            if 
                ( this.mIsUpdateLayoutLocked || 
                  this.mToMeasureCount > 0 ||
                  this.mToArrangeCount > 0 )
            {
                return;
            }
            
            int lProceesedNodeCount = 0;
            bool lHasException = true;
            UIElement lCurrentElement = null;
            try
            {
                while 
                    ( this.IsLayoutDirty )
                {
                    if 
                        ( ++lProceesedNodeCount > 153 )
                    {
                        // If going over a particular count, stack it to be processed later on.
                        this.Dispatcher.Invoke( this.UpdateLayout );
                        lCurrentElement = null;
                        lHasException = false;
                        return;
                    }
                    
                    this.mIsUpdatingLayout = true;
                    this.mIsUpdateLayoutLocked = true;
                    
                    //loop for Measure
                    //We limit the number of loops here by time - normally, all layout
                    //calculations should be done by this time, this limit is here for
                    //emergency, "infinite loop" scenarios - yielding in this case will
                    //provide user with ability to continue to interact with the app, even though
                    //it will be sluggish. If we don't yield here, the loop is goign to be a deadly one
                    //and it will be impossible to save results or even close the window.
                    int loopCounter = 0;
                    DateTime loopStartTime = new DateTime(0);
                    while 
                        ( true )
                    {
                        if 
                            ( ++loopCounter > 153 )
                        {
                            loopCounter = 0;
                            if 
                                ( loopStartTime.Ticks == 0 )
                            {
                                loopStartTime = DateTime.UtcNow;
                            }
                            else
                            {
                                TimeSpan loopDuration = (DateTime.UtcNow - loopStartTime);
                                if 
                                    ( loopDuration.Milliseconds > 153 * 2 )
                                {
                                    //loop detected. Lets go over to background to let input work.
                                    this.Dispatcher.Invoke(  this.UpdateLayout );
                                    lCurrentElement = null;
                                    lHasException = false;
                                    return;
                                }
                            }
                        }

                        lCurrentElement = this.mMeasureCollection.Top;

                        if 
                            ( lCurrentElement == null )
                        {
                            break; //exit if no more Measure candidates
                        }

                        lCurrentElement.Measure( lCurrentElement.PreviousConstraint );
                    }
  
                    // Process element(s) to arrange
                    loopCounter = 0;
                    loopStartTime = new DateTime(0);
                    while 
                        ( this.mMeasureCollection.IsEmpty )
                    {
                        if 
                            ( ++loopCounter > 153 )
                        {
                            loopCounter = 0;
                            if 
                                ( loopStartTime.Ticks == 0 )
                            {
                                loopStartTime = DateTime.UtcNow;
                            }
                            else
                            {
                                // If going over a time limit, stack it to be processed later on.
                                TimeSpan loopDuration = DateTime.UtcNow - loopStartTime;
                                if 
                                    ( loopDuration.Milliseconds > 153 * 2 )
                                {
                                    this.Dispatcher.Invoke( this.UpdateLayout );
                                    lCurrentElement = null;
                                    lHasException = false;
                                    return;
                                }
                            }
                        }

                        lCurrentElement = this.mArrangeCollection.Top;

                        if 
                            ( lCurrentElement == null )
                        {
                            break; //exit if no more Measure candidates
                        }

                        Rect lFinalRect = this.CorrectArrangeRect( lCurrentElement );
                        lCurrentElement.Arrange( lFinalRect );
                    }

                    
                    if 
                        ( this.mMeasureCollection.IsEmpty == false )
                    {
                        continue;
                    }

                    this.mIsUpdateLayoutLocked = false;

                    this.NotifySizeChanged();
                    if 
                        ( this.IsLayoutDirty )
                    {
                        continue;
                    }

                    this.NotifyLayoutUpdated();
                    if 
                        ( this.IsLayoutDirty )
                    {
                        continue;
                    }

                    // In the case nothing is dirty, one last chance for any size changes to announce.
                    this.NotifySizeChanged();
                }

                lCurrentElement = null;
                lHasException = false;
            }
            finally
            {
                this.mIsUpdatingLayout = false;
                this.mHasRequestedLayoutUpdate = false;
                this.mIsUpdateLayoutLocked = false;

                if 
                    ( lHasException )
                {
                    LogManager.Instance.Log( "Layout update not successfully done!!!" );
                }
            }
        }

        /// <summary>
        /// Renders the UI on the screen.
        /// </summary>
        public void Render()
        {
            if
                ( this.mPresentationSource != null )
            {
                this.Dispatcher.Invoke( this.mPresentationSource.Render );
            }
            else
            {
                LogManager.Instance.Log( "UI Render impossible, No presentation source!!!", LogType.ERROR );
            }
        }

        /// <summary>
        /// Gets the corrected arrange rectangle of the given UI element.
        /// </summary>
        /// <param name="pElement">The UI element the arrange rectangle must be corrected for.</param>
        /// <returns>The corrected arrange rectangle.</returns>
        private Rect CorrectArrangeRect(UIElement pElement)
        {
            Rect lPreviousArrangeRectangle = pElement.PreviousArrangeRect;

            // If the elements has no parent (top level).
            UIElement lUnused = null;
            if 
                ( pElement.GetUIParent( out lUnused ) == false )
            {
                lPreviousArrangeRectangle.X = lPreviousArrangeRectangle.Y = 0;

                if 
                    ( double.IsPositiveInfinity( pElement.PreviousConstraint.Width ) )
                {
                    lPreviousArrangeRectangle.Width = pElement.DesiredSize.Width;
                }

                if 
                    ( double.IsPositiveInfinity( pElement.PreviousConstraint.Height ) )
                {
                    lPreviousArrangeRectangle.Height = pElement.DesiredSize.Height;
                }
            }

            return lPreviousArrangeRectangle;
        }

        /// <summary>
        /// Add size changed arguments to the chain drawn by the currently updated tree of UI element
        /// for notification times.
        /// </summary>
        /// <param name="pArguments">The arguments of a UI element whose size has changed.</param>
        internal void AddToSizeChangedChain(SizeChangedArguments pArguments)
        {
            pArguments.Next = this.mArguments;
            this.mArguments = pArguments;
        }

        /// <summary>
        /// Notifies the size has changed.
        /// </summary>
        private void NotifySizeChanged()
        {
            // Reentrancy may happen if one of handlers calls 
            // UpdateLayout again inducing a potential loop.
            if 
                ( this.mIsNotifyingSizeChanged )
            {
                return;
            }

            try
            {
                this.mIsNotifyingSizeChanged = true;

                //loop for SizeChanged
                while 
                    ( this.mArguments != null )
                {
                    SizeChangedArguments lCurrentArguments = this.mArguments;
                    this.mArguments = lCurrentArguments.Next;

                    lCurrentArguments.EditedElement.SizeChangedArguments = null;

                    lCurrentArguments.EditedElement.OnRenderSizeChanged( lCurrentArguments );

                    // If notification make the tree dirty
                    // get back to layout update.
                    if 
                        ( this.IsLayoutDirty )
                    {
                        break;
                    }
                }
            }
            finally
            {
                this.mIsNotifyingSizeChanged = false;
            }
        }

        /// <summary>
        /// Notifies the layout has been updated.
        /// </summary>
        private void NotifyLayoutUpdated()
        {
            // Reentrancy may happen if one of handlers calls 
            // UpdateLayout again inducing a potential loop.
            if
                ( this.mIsNotifyingLayoutUpdated )
            {
                return;
            }

            try
            {
                this.mIsNotifyingLayoutUpdated = true;

                Action<object, EventArgs>[] lListeners = this.mLayoutListeners.ToArray();
                for
                    ( int lCurr = 0; lCurr < lListeners.Length; lCurr++ )
                {
                    Action<object, EventArgs> lListener = lListeners[ lCurr ];
                    try
                    {
                        lListener( null, EventArgs.Empty );

                        // if the handler dirtied the tree, 
                        // go clean it again before calling another handler
                        if 
                            ( this.IsLayoutDirty )
                        {
                            break;
                        }
                    }
                    catch 
                        ( Exception pEx )
                    {
                        LogManager.Instance.Log( pEx );
                    }
                }
            }
            finally
            {
                this.mIsNotifyingLayoutUpdated = false;
            }
        }

        #endregion Methods Internal

        #region Methods IDisposable

        /// <summary>
        /// Releases resources.
        /// </summary>
        protected override void OnDispose()
        {
            if ( this.mIsDisposed == false )
            {
                this.mDispatcher.Dispose();

                GC.SuppressFinalize( this );

                this.mIsDisposed = true;
            }
        }

        #endregion Methods IDisposable

        #endregion Methods
    }
}
