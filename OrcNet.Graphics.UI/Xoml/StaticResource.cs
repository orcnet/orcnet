﻿using System;
using System.Xml.Linq;

namespace OrcNet.Graphics.UI.Xoml
{
    /// <summary>
    /// Static resource class definition.
    /// </summary>
    internal class StaticResource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StaticResource"/> class.
        /// </summary>
        /// <param name="pType"></param>
        public StaticResource(Type pType)
        {
            ResourceNodeList = new XElement(pType.Name);
            // ResourceNodeList.Writer.WriteStartObject(type);
        }

        public XElement ResourceNodeList { get; private set; }
    }
}
