﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;

namespace OrcNet.Graphics.UI.Xoml
{
    /// <summary>
    /// Definition of the <see cref="KeyRecord"/> class.
    /// </summary>
    internal class KeyRecord
    {
        #region Fields

        private List<Object> _resources;
        private object _data;
        bool _shared;
        bool _sharedSet;

        #endregion Fields

        #region Properties

        public bool IsShared
        {
            get
            {
                return _shared;
            }
        }

        public bool IsSharedSet
        {
            get
            {
                return _sharedSet;
            }
        }

        /// <summary>
        /// Gets or sets the element value position.
        /// </summary>
        public long ValuePosition
        {
            get;
            set;
        }

        public int ValueSize
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the set of flags for the static resources of the record.
        /// </summary>
        public byte Flags
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the set of static resources of the record.
        /// </summary>
        public List<Object> StaticResources
        {
            get
            {
                if (_resources == null)
                {
                    _resources = new List<Object>();
                }

                return _resources;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether the record has any static resources or not.
        /// </summary>
        public bool HasStaticResources
        {
            get
            {
                return _resources != null && 
                       _resources.Count > 0;
            }
        }

        public StaticResource LastStaticResource
        {
            get
            {
                Debug.Assert(StaticResources[StaticResources.Count - 1] is StaticResource);
                return StaticResources[StaticResources.Count - 1] as StaticResource;
            }
        }

        public string KeyString
        {
            get
            {
                return _data as String;
            }
        }

        public Type KeyType
        {
            get
            {
                return _data as Type;
            }
        }

        public XElement KeyNodeList
        {
            get
            {
                return _data as XElement;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyRecord"/> class.
        /// </summary>
        /// <param name="shared"></param>
        /// <param name="sharedSet"></param>
        /// <param name="valuePosition"></param>
        /// <param name="keyType"></param>
        public KeyRecord(bool shared, bool sharedSet, int valuePosition, Type keyType) :
            this(shared, sharedSet, valuePosition)
        {
            _data = keyType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyRecord"/> class.
        /// </summary>
        /// <param name="shared"></param>
        /// <param name="sharedSet"></param>
        /// <param name="valuePosition"></param>
        /// <param name="keyString"></param>
        public KeyRecord(bool shared, bool sharedSet, int valuePosition, string keyString) :
            this(shared, sharedSet, valuePosition)
        {
            _data = keyString;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyRecord"/> class.
        /// </summary>
        /// <param name="shared"></param>
        /// <param name="sharedSet"></param>
        /// <param name="valuePosition"></param>
        /// <param name="pElementName"></param>
        public KeyRecord(bool shared, bool sharedSet, int valuePosition, XName pElementName) :
            this(shared, sharedSet, valuePosition)
        {
            _data = new XElement( pElementName );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyRecord"/> class.
        /// </summary>
        /// <param name="pIsShared"></param>
        /// <param name="pIsSharedSet"></param>
        /// <param name="pValuePosition"></param>
        private KeyRecord(bool pIsShared, bool pIsSharedSet, int pValuePosition)
        {
            _shared = pIsShared;
            _sharedSet = pIsSharedSet;
            ValuePosition = pValuePosition;
        }

        #endregion Constructor
    }
}
