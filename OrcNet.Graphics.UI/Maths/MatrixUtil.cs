﻿using System.Diagnostics;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// UI Matrix utilities.
    /// </summary>
    internal static class MatrixUtil
    {
        #region Methods

        /// <summary>
        /// TransformRect - Internal helper for perf 
        /// </summary>
        /// <param name="pRectangle">The Rect to transform.</param>
        /// <param name="pMatrix">The Matrix with which to transform the Rect.</param>
        internal static void TransformRect(ref Rect pRectangle, ref Matrix pMatrix)
        {
            if (pRectangle.IsEmpty)
            {
                return;
            }

            MatrixState lMatrixState = pMatrix.mState;

            // If the pMatrix is identity, don't worry.
            if (lMatrixState == MatrixState.TRANSFORM_IS_IDENTITY)
            {
                return;
            }

            // Scaling
            if (0 != (lMatrixState & MatrixState.TRANSFORM_IS_SCALING))
            {
                pRectangle.X *= pMatrix.M11;
                pRectangle.Y *= pMatrix.M22;
                pRectangle.Width *= pMatrix.M11;
                pRectangle.Height *= pMatrix.M22;

                // Ensure the width is always positive.  For example, if there was a reflection about the 
                // y axis followed by a translation into the visual area, the width could be negative.
                if (pRectangle.Width < 0.0)
                {
                    pRectangle.X += pRectangle.Width;
                    pRectangle.Width = -pRectangle.Width;
                }

                // Ensure the height is always positive.  For example, if there was a reflection about the
                // x axis followed by a translation into the visual area, the height could be negative. 
                if (pRectangle.Height < 0.0)
                {
                    pRectangle.Y += pRectangle.Height;
                    pRectangle.Height = -pRectangle.Height;
                }
            }

            // Translation
            if (0 != (lMatrixState & MatrixState.TRANSFORM_IS_TRANSLATION))
            {
                // X 
                pRectangle.X += pMatrix.OffsetX;

                // Y 
                pRectangle.Y += pMatrix.OffsetY;
            }

            if (lMatrixState == MatrixState.TRANSFORM_IS_UNKNOWN)
            {
                // Al Bunny implementation. 
                Point lPoint0 = pMatrix.Transform(pRectangle.TopLeft);
                Point lPoint1 = pMatrix.Transform(pRectangle.TopRight);
                Point lPoint2 = pMatrix.Transform(pRectangle.BottomRight);
                Point lPoint3 = pMatrix.Transform(pRectangle.BottomLeft);

                // Width and height is always positive here.
                pRectangle.X = System.Math.Min(System.Math.Min(lPoint0.X, lPoint1.X), System.Math.Min(lPoint2.X, lPoint3.X));
                pRectangle.Y = System.Math.Min(System.Math.Min(lPoint0.Y, lPoint1.Y), System.Math.Min(lPoint2.Y, lPoint3.Y));

                pRectangle.Width = System.Math.Max(System.Math.Max(lPoint0.X, lPoint1.X), System.Math.Max(lPoint2.X, lPoint3.X)) - pRectangle.X;
                pRectangle.Height = System.Math.Max(System.Math.Max(lPoint0.Y, lPoint1.Y), System.Math.Max(lPoint2.Y, lPoint3.Y)) - pRectangle.Y;
            }
        }

        /// <summary>
        /// Multiplies two transformations, where the behavior is pMatrix1 *= pMatrix2. 
        /// This code exists so that we can efficient combine matrices without copying
        /// the data around, since each pMatrix is 52 bytes. 
        /// To reduce duplication and to ensure consistent behavior, this is the 
        /// method which is used to implement Matrix * Matrix as well.
        /// </summary> 
        internal static void MultiplyMatrix(ref Matrix pMatrix1, ref Matrix pMatrix2)
        {
            MatrixState lState1 = pMatrix1.mState;
            MatrixState lState2 = pMatrix2.mState;

            // Check for idents 

            // If the second is ident, we can just return
            if (lState2 == MatrixState.TRANSFORM_IS_IDENTITY)
            {
                return;
            }

            // If the first is ident, we can just copy the memory across.
            if (lState1 == MatrixState.TRANSFORM_IS_IDENTITY)
            {
                pMatrix1 = pMatrix2;
                return;
            }

            // Optimize for translate case, where the second is a translate
            if (lState2 == MatrixState.TRANSFORM_IS_TRANSLATION)
            {
                // 2 additions 
                pMatrix1.OffsetX += pMatrix2.OffsetX;
                pMatrix1.OffsetY += pMatrix2.OffsetY;

                // If pMatrix 1 wasn't unknown we added a translation
                if (lState1 != MatrixState.TRANSFORM_IS_UNKNOWN)
                {
                    pMatrix1.mState |= MatrixState.TRANSFORM_IS_TRANSLATION;
                }

                return;
            }

            // Check for the first value being a translate
            if (lState1 == MatrixState.TRANSFORM_IS_TRANSLATION)
            {
                // Save off the old offsets 
                double offsetX = pMatrix1.OffsetX;
                double offsetY = pMatrix1.OffsetY;

                // Copy the pMatrix
                pMatrix1 = pMatrix2;

                pMatrix1.OffsetX = offsetX * pMatrix2.M11 + offsetY * pMatrix2.M21 + pMatrix2.OffsetX;
                pMatrix1.OffsetY = offsetX * pMatrix2.M12 + offsetY * pMatrix2.M22 + pMatrix2.OffsetY;

                if (lState2 == MatrixState.TRANSFORM_IS_UNKNOWN)
                {
                    pMatrix1.mState = MatrixState.TRANSFORM_IS_UNKNOWN;
                }
                else
                {
                    pMatrix1.mState = MatrixState.TRANSFORM_IS_SCALING | MatrixState.TRANSFORM_IS_TRANSLATION;
                }
                return;
            }

            // The following code combines the type of the transformations so that the high nibble 
            // is "this"'s type, and the low nibble is mat's type.  This allows for a switch rather
            // than nested switches. 

            // trans1.mState |  trans2.mState
            //  7  6  5  4   |  3  2  1  0
            int combinedType = ((int)lState1 << 4) | (int)lState2;

            switch (combinedType)
            {
                case 34:  // S * S
                    // 2 multiplications 
                    pMatrix1.M11 *= pMatrix2.M11;
                    pMatrix1.M22 *= pMatrix2.M22;
                    return;

                case 35:  // S * S|T
                    pMatrix1.M11 *= pMatrix2.M11;
                    pMatrix1.M22 *= pMatrix2.M22;
                    pMatrix1.OffsetX = pMatrix2.OffsetX;
                    pMatrix1.OffsetY = pMatrix2.OffsetY;

                    // Transform set to Translate and Scale
                    pMatrix1.mState = MatrixState.TRANSFORM_IS_TRANSLATION | MatrixState.TRANSFORM_IS_SCALING;
                    return;

                case 50: // S|T * S 
                    pMatrix1.M11 *= pMatrix2.M11;
                    pMatrix1.M22 *= pMatrix2.M22;
                    pMatrix1.OffsetX *= pMatrix2.M11;
                    pMatrix1.OffsetY *= pMatrix2.M22;
                    return;

                case 51: // S|T * S|T 
                    pMatrix1.M11 *= pMatrix2.M11;
                    pMatrix1.M22 *= pMatrix2.M22;
                    pMatrix1.OffsetX = pMatrix2.M11 * pMatrix1.OffsetX + pMatrix2.OffsetX;
                    pMatrix1.OffsetY = pMatrix2.M22 * pMatrix1.OffsetY + pMatrix2.OffsetY;
                    return;
                case 36: // S * U
                case 52: // S|T * U
                case 66: // U * S
                case 67: // U * S|T 
                case 68: // U * U
                    pMatrix1 = new Matrix(
                        pMatrix1.M11 * pMatrix2.M11 + pMatrix1.M12 * pMatrix2.M21,
                        pMatrix1.M11 * pMatrix2.M12 + pMatrix1.M12 * pMatrix2.M22,

                        pMatrix1.M21 * pMatrix2.M11 + pMatrix1.M22 * pMatrix2.M21,
                        pMatrix1.M21 * pMatrix2.M12 + pMatrix1.M22 * pMatrix2.M22,

                        pMatrix1.OffsetX * pMatrix2.M11 + pMatrix1.OffsetY * pMatrix2.M21 + pMatrix2.OffsetX,
                        pMatrix1.OffsetX * pMatrix2.M12 + pMatrix1.OffsetY * pMatrix2.M22 + pMatrix2.OffsetY);
                    return;
#if DEBUG
            default:
                Debug.Fail("Matrix multiply hit an invalid case: " + combinedType); 
                break;
#endif
            }
        }

        /// <summary> 
        /// Applies an offset to the specified pMatrix in place. 
        /// </summary>
        internal static void PrependOffset( ref Matrix pMatrix,
                                            double offsetX,
                                            double offsetY)
        {
            if (pMatrix.mState == MatrixState.TRANSFORM_IS_IDENTITY)
            {
                pMatrix = new Matrix(1, 0, 0, 1, offsetX, offsetY);
                pMatrix.mState = MatrixState.TRANSFORM_IS_TRANSLATION;
            }
            else
            {
                //
                //  / 1   0   0 \       / m11   m12   0 \ 
                //  | 0   1   0 |   *   | m21   m22   0 |
                //  \ tx  ty  1 /       \  ox    oy   1 / 
                // 
                //       /   m11                  m12                     0 \
                //  =    |   m21                  m22                     0 | 
                //       \   m11*tx+m21*ty+ox     m12*tx + m22*ty + oy    1 /
                //

                pMatrix.OffsetX += pMatrix.M11 * offsetX + pMatrix.M21 * offsetY;
                pMatrix.OffsetY += pMatrix.M12 * offsetX + pMatrix.M22 * offsetY;

                // It just gained a translate if was a scale transform. Identity transform is handled above. 
                Debug.Assert(pMatrix.mState != MatrixState.TRANSFORM_IS_IDENTITY);
                if (pMatrix.mState != MatrixState.TRANSFORM_IS_UNKNOWN)
                {
                    pMatrix.mState |= MatrixState.TRANSFORM_IS_TRANSLATION;
                }
            }
        }

        #endregion Methods
    }
}
