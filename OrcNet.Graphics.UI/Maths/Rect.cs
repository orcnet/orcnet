﻿using OpenTK;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary> 
    /// The primitive which represents a rectangle.
    /// Rects are stored as X, Y (Location) and Width and Height (Size).
    /// As a result, Rects cannot have negative Width or Height. 
    /// </summary>
    public struct Rect
    {
        #region Fields

        /// <summary>
        /// Stores the constant Empty rectangle.
        /// </summary>
        private readonly static Rect sEmpty = CreateEmptyRect();

        /// <summary>
        /// Stores the X coordinate of the Location.
        /// </summary>
        private double mX;

        /// <summary>
        /// Stores the Y coordinate of the Location.
        /// </summary>
        private double mY;

        /// <summary>
        /// Stores the Width component of the Size.
        /// </summary>
        private double mWidth;

        /// <summary>
        /// Stores the Height component of the Size.
        /// </summary>
        private double mHeight;

        #endregion Fields

        #region Properties

        /// <summary> 
        /// Gets an Empty rectangle.
        /// X and Y are positive-infinity and Width and Height are negative infinity.
        /// This is the only situation where Width or Height can be negative.
        /// </summary> 
        public static Rect Empty
        {
            get
            {
                return sEmpty;
            }
        }

        /// <summary>
        /// Returns true if this Rectangle is the Empty rectangle. 
        /// Note: If pWidth or pHeight are 0 this Rectangle still contains a 0 or 1 dimensional set
        /// of points, so this method should not be used to check for 0 area.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                // The funny pWidth and pHeight tests are to handle NaNs
                Debug.Assert((!(this.mWidth < 0) && !(this.mHeight < 0)) || (this == Empty));

                return this.mWidth < 0;
            }
        }

        /// <summary> 
        /// Gets the Point representing the origin of the Rectangle 
        /// </summary>
        public Point Location
        {
            get
            {
                return new Point(this.mX, this.mY);
            }
            set
            {
                if (IsEmpty)
                {
                    throw new System.InvalidOperationException("Cannot modify empty rectangle.");
                }

                this.mX = value.X;
                this.mY = value.Y;
            }
        }

        /// <summary> 
        /// Gets the Size representing the area of the Rectangle
        /// </summary>
        public Size Size
        {
            get
            {
                if (IsEmpty)
                {
                    return Size.Empty;
                }
                
                return new Size(this.mWidth, this.mHeight);
            }
            set
            {
                if (value.IsEmpty)
                {
                    this = sEmpty;
                }
                else
                {
                    if (IsEmpty)
                    {
                        throw new System.InvalidOperationException("Cannot modify an empty rectangle.");
                    }

                    this.mWidth = value.Width;
                    this.mHeight = value.Height;
                }
            }
        }

        /// <summary>
        /// Gets the X coordinate of the Location. 
        /// If this is the empty rectangle, the value will be positive infinity.
        /// If this lRectangle is Empty, setting this property is illegal. 
        /// </summary> 
        public double X
        {
            get
            {
                return this.mX;
            }
            set
            {
                if (IsEmpty)
                {
                    throw new System.InvalidOperationException("Cannot modify empty rectangle.");
                }

                this.mX = value;
            }

        }

        /// <summary>
        /// Gets the Y coordinate of the Location 
        /// If this is the empty rectangle, the value will be positive infinity.
        /// If this lRectangle is Empty, setting this property is illegal.
        /// </summary>
        public double Y
        {
            get
            {
                return this.mY;
            }
            set
            {
                if (IsEmpty)
                {
                    throw new System.InvalidOperationException("Cannot modify empty rectangle.");
                }

                this.mY = value;
            }
        }

        /// <summary>
        /// Gets the Width component of the Size. 
        /// This cannot be set to negative, and will only 
        /// be negative if this is the empty rectangle, in which case it will be negative infinity.
        /// If this lRectangle is Empty, setting this property is illegal. 
        /// </summary> 
        public double Width
        {
            get
            {
                return this.mWidth;
            }
            set
            {
                if (IsEmpty)
                {
                    throw new System.InvalidOperationException("Cannot modify empty rectangle.");
                }

                if (value < 0)
                {
                    throw new System.ArgumentException("Width cannot be negative.");
                }

                this.mWidth = value;
            }
        }

        /// <summary>
        /// Gets the Height component of the Size. 
        /// This cannot be set to negative, and will only 
        /// be negative if this is the empty rectangle, in which case it will be negative infinity.
        /// If this lRectangle is Empty, setting this property is illegal. 
        /// </summary> 
        public double Height
        {
            get
            {
                return this.mHeight;
            }
            set
            {
                if (IsEmpty)
                {
                    throw new System.InvalidOperationException("Cannot modify empty rectangle.");
                }

                if (value < 0)
                {
                    throw new System.ArgumentException("Height cannot be negative.");
                }

                this.mHeight = value;
            }
        }

        /// <summary>
        /// Gets the Left point.
        /// This is a read-only alias for X 
        /// If this is the empty rectangle, the value will be positive infinity.
        /// </summary> 
        public double Left
        {
            get
            {
                return this.mX;
            }
        }

        /// <summary> 
        /// Gets the Top point.
        /// This is a read-only alias for Y 
        /// If this is the empty rectangle, the value will be positive infinity.
        /// </summary> 
        public double Top
        {
            get
            {
                return this.mY;
            }
        }

        /// <summary> 
        /// Gets the Right point.
        /// This is a read-only alias for X + Width
        /// If this is the empty rectangle, the value will be negative infinity.
        /// </summary>
        public double Right
        {
            get
            {
                if (IsEmpty)
                {
                    return Double.NegativeInfinity;
                }

                return this.mX + this.mWidth;
            }
        }

        /// <summary>
        /// Gets the Bottom point.
        /// This is a read-only alias for Y + Height 
        /// If this is the empty rectangle, the value will be negative infinity.
        /// </summary>
        public double Bottom
        {
            get
            {
                if (IsEmpty)
                {
                    return Double.NegativeInfinity;
                }

                return this.mY + this.mHeight;
            }
        }

        /// <summary> 
        /// Gets the TopLeft point.
        /// This is a read-only alias for the Point which is at X, Y
        /// If this is the empty rectangle, the value will be positive infinity, positive infinity. 
        /// </summary>
        public Point TopLeft
        {
            get
            {
                return new Point(Left, Top);
            }
        }

        /// <summary>
        /// Gets the TopRight point.
        /// This is a read-only alias for the Point which is at X + Width, Y
        /// If this is the empty rectangle, the value will be negative infinity, positive infinity.
        /// </summary> 
        public Point TopRight
        {
            get
            {
                return new Point(Right, Top);
            }
        }

        /// <summary> 
        /// Gets the BottomLeft point.
        /// This is a read-only alias for the Point which is at X, Y + Height
        /// If this is the empty rectangle, the value will be positive infinity, negative infinity. 
        /// </summary> 
        public Point BottomLeft
        {
            get
            {
                return new Point(Left, Bottom);
            }
        }

        /// <summary> 
        /// Gets the BottomRight point.
        /// This is a read-only alias for the Point which is at X + Width, Y + Height
        /// If this is the empty rectangle, the value will be negative infinity, negative infinity. 
        /// </summary>
        public Point BottomRight
        {
            get
            {
                return new Point(Right, Bottom);
            }
        }

        #endregion Public Properties 

        #region Constructors 

        /// <summary>
        /// Initializes a new instance of the <see cref="Rect"/> class.
        /// </summary>
        /// <param name="pLocation">The location.</param>
        /// <param name="pSize">The size.</param>
        public Rect(Point pLocation,
                    Size pSize)
        {
            if (pSize.IsEmpty)
            {
                this = sEmpty;
            }
            else
            {
                this.mX = pLocation.X;
                this.mY = pLocation.Y;
                this.mWidth = pSize.Width;
                this.mHeight = pSize.Height;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rect"/> class.
        /// </summary>
        /// <param name="pX">The X component of the location.</param>
        /// <param name="pY">The Y component of the location.</param>
        /// <param name="pWidth">The width that must be non-negative </param>
        /// <param name="pHeight">The height that must be non-negative </param>
        public Rect(double pX,
                    double pY,
                    double pWidth,
                    double pHeight)
        {
            if (pWidth < 0 || pHeight < 0)
            {
                throw new System.ArgumentException("Width and Height cannot be negative.");
            }

            this.mX = pX;
            this.mY = pY;
            this.mWidth = pWidth;
            this.mHeight = pHeight;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rect"/> class.
        /// </summary>
        /// <param name="pPoint1">The first point to encapsulate.</param>
        /// <param name="pPoint2">The second point to encapsulate.</param>
        public Rect(Point pPoint1,
                    Point pPoint2)
        {
            this.mX = System.Math.Min(pPoint1.X, pPoint2.X);
            this.mY = System.Math.Min(pPoint1.Y, pPoint2.Y);

            //  Max with 0 to prevent double weirdness from causing us to be (-epsilon..0) 
            this.mWidth = System.Math.Max(System.Math.Max(pPoint1.X, pPoint2.X) - this.mX, 0);
            this.mHeight = System.Math.Max(System.Math.Max(pPoint1.Y, pPoint2.Y) - this.mY, 0);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rect"/> class.
        /// </summary>
        /// <param name="pPoint">The starting point.</param>
        /// <param name="pVector">The endpoint resulting from start point translated by the vector.</param>
        public Rect(Point pPoint,
                    Vector2d pVector) : 
        this(pPoint, pPoint + pVector)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rect"/> class.
        /// </summary>
        /// <param name="pSize">The rectangle size from the location (0, 0).</param>
        public Rect(Size pSize)
        {
            if (pSize.IsEmpty)
            {
                this = sEmpty;
            }
            else
            {
                this.mX = this.mY = 0;
                this.mWidth = pSize.Width;
                this.mHeight = pSize.Height;
            }
        }

        #endregion Constructors

        #region Methods

        /// <summary> 
        /// Returns true if the Point is within the rectangle, inclusive of the edges.
        /// </summary> 
        /// <param name="pPoint">The pPoint which is being tested.</param>
        /// <returns>True if the Point is within the rectangle, false otherwise.</returns>
        public bool Contains(Point pPoint)
        {
            return Contains(pPoint.X, pPoint.Y);
        }

        /// <summary> 
        /// Returns true if the Point represented by pX,pY is within the rectangle inclusive of the edges.
        /// </summary>
        /// <param name="pX">The X coordinate of the pPoint which is being tested.</param>
        /// <param name="pY">The Y coordinate of the pPoint which is being tested.</param>
        /// <returns>True if the Point represented by X/Y is within the rectangle, false otherwise.</returns>
        public bool Contains(double pX, double pY)
        {
            if (IsEmpty)
            {
                return false;
            }

            return ContainsInternal(pX, pY);
        }

        /// <summary>
        /// Returns true if the Rect non-Empty and is entirely contained within the
        /// rectangle, inclusive of the edges.
        /// </summary>
        /// <param name="pRectangle">The rectangle to test.</param>
        /// <returns>True of the supplied rectangle is contained in this, false otherwise.</returns>
        public bool Contains(Rect pRectangle)
        {
            if (IsEmpty || pRectangle.IsEmpty)
            {
                return false;
            }

            return (this.mX <= pRectangle.X &&
                    this.mY <= pRectangle.Y &&
                    this.mX + this.mWidth >= pRectangle.X + pRectangle.mWidth &&
                    this.mY + this.mHeight >= pRectangle.Y + pRectangle.mHeight);
        }

        /// <summary>
        /// Returns true if the Rect intersects with this rectangle
        /// Note that if one edge is coincident, this is considered an intersection.
        /// </summary>
        /// <param name="pRectangle">The rectangle to test for intersection.</param>
        /// <returns>True if the Rect intersects with this rectangle, false otherwise.</returns>
        public bool IntersectsWith(Rect pRectangle)
        {
            if (IsEmpty || pRectangle.IsEmpty)
            {
                return false;
            }

            return (pRectangle.Left <= Right) &&
                   (pRectangle.Right >= Left) &&
                   (pRectangle.Top <= Bottom) &&
                   (pRectangle.Bottom >= Top);
        }

        /// <summary>
        /// Updates this rectangle to be the intersection of this and another Rectangle 
        /// If either this or lRectangle are Empty, the result is Empty as well.
        /// </summary>
        /// <param name="pRectangle">The lRectangle to intersect with this.</param>
        public void Intersect(Rect pRectangle)
        {
            if ( this.IntersectsWith(pRectangle) == false )
            {
                this = Empty;
            }
            else
            {
                double lLeft = System.Math.Max(Left, pRectangle.Left);
                double lTop = System.Math.Max(Top, pRectangle.Top);

                //  Max with 0 to prevent double weirdness from causing us to be (-epsilon..0) 
                this.mWidth = System.Math.Max(System.Math.Min(Right, pRectangle.Right) - lLeft, 0);
                this.mHeight = System.Math.Max(System.Math.Min(Bottom, pRectangle.Bottom) - lTop, 0);

                this.mX = lLeft;
                this.mY = lTop;
            }
        }

        /// <summary>
        /// Returns the result of the intersection of pRectangle1 and pRectangle2. 
        /// If either this or lRectangle are Empty, the result is Empty as well.
        /// </summary>
        /// <param name="pRectangle1">The first rectangle.</param>
        /// <param name="pRectangle2">The second rectangle.</param>
        /// <returns>The intersection area.</returns>
        public static Rect Intersect(Rect pRectangle1, Rect pRectangle2)
        {
            pRectangle1.Intersect(pRectangle2);
            return pRectangle1;
        }

        /// <summary>
        /// Update this rectangle to be the union of this and the other Rectangle.
        /// </summary>
        /// <param name="pRectangle">The other rectangle</param>
        public void Union(Rect pRectangle)
        {
            if (IsEmpty)
            {
                this = pRectangle;
            }
            else if (!pRectangle.IsEmpty)
            {
                double lLeft = System.Math.Min(Left, pRectangle.Left);
                double lTop = System.Math.Min(Top, pRectangle.Top);


                // We need this check so that the math does not result in NaN 
                if ((pRectangle.Width == Double.PositiveInfinity) || (Width == Double.PositiveInfinity))
                {
                    this.mWidth = Double.PositiveInfinity;
                }
                else
                {
                    //  Max with 0 to prevent double weirdness from causing us to be (-epsilon..0)
                    double maxRight = System.Math.Max(Right, pRectangle.Right);
                    this.mWidth = System.Math.Max(maxRight - lLeft, 0);
                }

                // We need this check so that the math does not result in NaN 
                if ((pRectangle.Height == Double.PositiveInfinity) || (Height == Double.PositiveInfinity))
                {
                    this.mHeight = Double.PositiveInfinity;
                }
                else
                {
                    //  Max with 0 to prevent double weirdness from causing us to be (-epsilon..0)
                    double maxBottom = System.Math.Max(Bottom, pRectangle.Bottom);
                    this.mHeight = System.Math.Max(maxBottom - lTop, 0);
                }

                this.mX = lLeft;
                this.mY = lTop;
            }
        }

        /// <summary>
        /// Return the result of the union of pRectangle1 and pRectangle2.
        /// </summary>
        /// <param name="pRectangle1">The first rectangle.</param>
        /// <param name="pRectangle2">The second rectangle.</param>
        /// <returns></returns>
        public static Rect Union(Rect pRectangle1, Rect pRectangle2)
        {
            pRectangle1.Union(pRectangle2);
            return pRectangle1;
        }

        /// <summary>
        /// Update this rectangle to be the union of this and pPoint. 
        /// </summary>
        /// <param name="pPoint">The point to union with that instance.</param>
        public void Union(Point pPoint)
        {
            Union(new Rect(pPoint, pPoint));
        }

        /// <summary>
        /// Return the result of the union of pRectangle and pPoint. 
        /// </summary>
        /// <param name="pRectangle">The rectangle to union with a point.</param>
        /// <param name="pPoint">The point to union with the rectangle.</param>
        /// <returns></returns>
        public static Rect Union(Rect pRectangle, Point pPoint)
        {
            pRectangle.Union(new Rect(pPoint, pPoint));
            return pRectangle;
        }

        /// <summary>
        /// Translates the Location by the offset provided. 
        /// If this is Empty, this method is illegal. 
        /// </summary>
        /// <param name="pOffsetVector">The translation to apply.</param>
        public void Offset(Vector2d pOffsetVector)
        {
            if (IsEmpty)
            {
                throw new System.InvalidOperationException("Empty rectangle cannot call such a method.");
            }

            this.mX += pOffsetVector.X;
            this.mY += pOffsetVector.Y;
        }

        /// <summary>
        /// Translates the Location by the offset provided
        /// If this is Empty, this method is illegal.
        /// </summary>
        /// <param name="pOffsetX">The X translation.</param>
        /// <param name="pOffsetY">The Y translation.</param>
        public void Offset(double pOffsetX, double pOffsetY)
        {
            if (IsEmpty)
            {
                throw new System.InvalidOperationException("Empty rectangle cannot call such a method.");
            }

            this.mX += pOffsetX;
            this.mY += pOffsetY;
        }

        /// <summary>
        /// Offset - return the result of offsetting lRectangle by the offset provided 
        /// If this is Empty, this method is illegal.
        /// </summary>
        public static Rect Offset(Rect pRectangle, Vector2d pOffsetVector)
        {
            pRectangle.Offset(pOffsetVector.X, pOffsetVector.Y);
            return pRectangle;
        }

        /// <summary> 
        /// Offset - return the result of offsetting lRectangle by the offset provided
        /// If this is Empty, this method is illegal.
        /// </summary>
        public static Rect Offset(Rect pRectangle, double pOffsetX, double pOffsetY)
        {
            pRectangle.Offset(pOffsetX, pOffsetY);
            return pRectangle;
        }

        /// <summary>
        /// Inflates the bounds by the pSize provided, in all directions
        /// If this is Empty, this method is illegal.
        /// </summary>
        /// <param name="pSize">The width and height the rectangle must be inflated of.</param>
        public void Inflate(Size pSize)
        {
            Inflate(pSize.Width, pSize.Height);
        }

        /// <summary>
        /// Inflates the bounds by the pSize provided, in all directions.
        /// If -pWidth is > Width / 2 or -pHeight is > Height / 2, this Rect becomes Empty
        /// If this is Empty, this method is illegal. 
        /// </summary>
        /// <param name="pWidth">The width the rectangle must be inflated of.</param>
        /// <param name="pHeight">The height the rectangle must be inflated of.</param>
        public void Inflate(double pWidth, double pHeight)
        {
            if (IsEmpty)
            {
                throw new System.InvalidOperationException("Empty rectangle cannot call such a method.");
            }

            this.mX -= pWidth;
            this.mY -= pHeight;

            // Do two additions rather than multiplication by 2 to avoid spurious overflow 
            // That is: (A + 2 * B) != ((A + B) + B) if 2*B overflows.
            // Note that multiplication by 2 might work in this case because A should start 
            // positive & be "clamped" to positive after, but consider A = Inf & B = -MAX.
            this.mWidth += pWidth;
            this.mWidth += pWidth;
            this.mHeight += pHeight;
            this.mHeight += pHeight;

            // We catch the case of inflation by less than -pWidth/2 or -pHeight/2 here.  This also 
            // maintains the invariant that either the Rect is Empty or this.mWidth and this.mHeight are
            // non-negative, even if the user parameters were NaN, though this isn't strictly maintained 
            // by other methods.
            if (!(this.mWidth >= 0 && this.mHeight >= 0))
            {
                this = sEmpty;
            }
        }

        /// <summary>
        /// Returns the result of inflating lRectangle by the pSize provided, in all directions 
        /// If this is Empty, this method is illegal.
        /// </summary>
        /// <param name="pRectangle">The rectangle to inflate.</param>
        /// <param name="pSize">The width and height the rectangle must be inflated of.</param>
        /// <returns></returns>
        public static Rect Inflate(Rect pRectangle, Size pSize)
        {
            pRectangle.Inflate(pSize.Width, pSize.Height);
            return pRectangle;
        }

        /// <summary>
        /// Returns the result of inflating Rectangle by the Size provided, in all directions
        /// If this is Empty, this method is illegal.
        /// </summary>
        /// <param name="pRectangle">The rectangle to inflate.</param>
        /// <param name="pWidth">The width the rectangle must be inflated of.</param>
        /// <param name="pHeight">The height the rectangle must be inflated of.</param>
        /// <returns>The inflated rectangle.</returns>
        public static Rect Inflate(Rect pRectangle, double pWidth, double pHeight)
        {
            pRectangle.Inflate(pWidth, pHeight);
            return pRectangle;
        }

        /// <summary>
        /// Returns the bounds of the transformed rectangle.
        /// The Empty Rect is not affected by this call.
        /// </summary>
        /// <param name="pRectangle">The Rect to transform.</param>
        /// <param name="pMatrix">The Matrix by which to transform.</param>
        /// <returns>The Rectangle which results from the transformation.</returns>
        public static Rect Transform(Rect pRectangle, Matrix pMatrix)
        {
            MatrixUtil.TransformRect(ref pRectangle, ref pMatrix);
            return pRectangle;
        }

        /// <summary> 
        /// Updates rectangle to be the bounds of the original value transformed
        /// by the pMatrix. 
        /// The Empty Rect is not affected by this call.
        /// </summary>
        /// <param name="pMatrix">The transformation.</param>
        public void Transform(Matrix pMatrix)
        {
            MatrixUtil.TransformRect(ref this, ref pMatrix);
        }

        /// <summary> 
        /// Scale the rectangle in the X and Y directions
        /// </summary>
        /// <param name="pScaleX">The scale in X.</param>
        /// <param name="pScaleY">The scale in Y.</param>
        public void Scale(double pScaleX, double pScaleY)
        {
            if (IsEmpty)
            {
                return;
            }

            this.mX *= pScaleX;
            this.mY *= pScaleY;
            this.mWidth *= pScaleX;
            this.mHeight *= pScaleY;

            // If the scale in the X dimension is negative, we need to normalize X and Width
            if (pScaleX < 0)
            {
                // Make X the lLeft-most edge again
                this.mX += this.mWidth;

                // and make Width positive
                this.mWidth *= -1;
            }

            // Do the same for the Y dimension 
            if (pScaleY < 0)
            {
                // Make Y the lTop-most edge again
                this.mY += this.mHeight;

                // and make Height positive 
                this.mHeight *= -1;
            }
        }

        /// <summary> 
        /// ContainsInternal - Performs just the "pPoint inside" logic 
        /// </summary>
        /// <param name="pX">The X-coord of the pPoint to test.</param>
        /// <param name="pY">The Y-coord of the pPoint to test.</param>
        /// <returns>True if the pPoint is inside the Rectangle, false otherwise.</returns>
        private bool ContainsInternal(double pX, double pY)
        {
            // We include points on the edge as "contained". 
            // We do "pX - this.mWidth <= this.mX" instead of "pX <= this.mX + this.mWidth"
            // so that this check works when this.mWidth is PositiveInfinity 
            // and this.mX is NegativeInfinity.
            return ((pX >= this.mX) && (pX - this.mWidth <= this.mX) &&
                    (pY >= this.mY) && (pY - this.mHeight <= this.mY));
        }

        /// <summary>
        /// Creates an empty rectangle.
        /// </summary>
        /// <returns>The new rectangle.</returns>
        static private Rect CreateEmptyRect()
        {
            Rect lRectangle = new Rect();
            // We can't set these via the property setters because negatives widths 
            // are rejected in those APIs.
            lRectangle.X = Double.PositiveInfinity;
            lRectangle.Y = Double.PositiveInfinity;
            lRectangle.Width = Double.NegativeInfinity;
            lRectangle.Height = Double.NegativeInfinity;
            return lRectangle;
        }

        /// <summary>
        /// Equality operator definition.
        /// </summary>
        /// <param name="pRectangle1">The first rectangle.</param>
        /// <param name="pRectangle2">The second rectangle.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public static bool operator ==(Rect pRectangle1, Rect pRectangle2)
        {
            return pRectangle1.mWidth  == pRectangle2.mWidth &&
                   pRectangle1.mHeight == pRectangle2.mHeight;
        }

        /// <summary>
        /// Difference operator definition.
        /// </summary>
        /// <param name="pRectangle1">The first rectangle.</param>
        /// <param name="pRectangle2">The second rectangle.</param>
        /// <returns>True if different, false otherwise.</returns>
        public static bool operator !=(Rect pRectangle1, Rect pRectangle2)
        {
            return !(pRectangle1 == pRectangle2);
        }

        /// <summary>
        /// Compares this rectangle with the passed in object.
        /// </summary>
        /// <param name="pOther">The object to compare to "this".</param>
        /// <returns>True if the object is an instance of rectangle and if it's equal to "this", false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            if ((null == pOther) || !(pOther is Rect))
            {
                return false;
            }

            Rect pValue = (Rect)pOther;
            return Rect.Equals(this, pValue);
        }

        /// <summary> 
        /// Returns the HashCode for this Size
        /// </summary>
        /// <returns>The HashCode for this Size.</returns>
        public override int GetHashCode()
        {
            if (IsEmpty)
            {
                return 0;
            }
            else
            {
                // Perform field-by-field XOR of HashCodes
                return Width.GetHashCode() ^
                       Height.GetHashCode() ^
                       X.GetHashCode() ^
                       Y.GetHashCode();
            }
        }

        #endregion Methods
    }
}
