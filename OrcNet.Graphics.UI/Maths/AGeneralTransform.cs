﻿using OrcNet.Core;
using System;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Generalized transform class definition.
    /// </summary>
    public abstract class AGeneralTransform : AObservable
    {
        #region Properties

        /// <summary>
        /// Returns the inverse transform if it has an inverse, null otherwise
        /// </summary> 
        public abstract AGeneralTransform Inverse
        {
            get;
        }

        /// <summary> 
        /// Returns a best effort affine transform
        /// </summary> 
        internal abstract ATransform AffineTransform
        {
            get;
        }

        #endregion Properties

        #region Constructor

        /// <summary> 
        /// Initializes a new instance of the <see cref="AGeneralTransform"/> class.
        /// </summary>
        internal AGeneralTransform()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary> 
        /// Transform a pPoint
        /// </summary> 
        /// <param name="pPoint">Input Point</param>
        /// <param name="pResult">Output Point</param>
        /// <returns>True if the pPoint was transformed successfuly, false otherwise</returns>
        public abstract bool TryTransform(Point pPoint, out Point pResult);

        /// <summary> 
        /// Transform a Point.
        /// If the transformation does not succeed, this will throw an InvalidOperationException. 
        /// If you don't want to try/catch, call TryTransform instead and check the boolean it
        /// returns.
        /// Note that this method will always succeed when called on a subclass of Transform 
        /// </summary>
        /// <param name="pPoint">Input Point</param>
        /// <returns>The transformed pPoint</returns> 
        public Point Transform(Point pPoint)
        {
            Point lTransformedPoint;
            if 
                ( this.TryTransform(pPoint, out lTransformedPoint) == false )
            {
                throw new InvalidOperationException("General transformation failed!!!");
            }

            return lTransformedPoint;
        }

        /// <summary>
        /// Transforms the bounding box to the smallest axis aligned bounding box
        /// that contains all the points in the original bounding box 
        /// </summary>
        /// <param name="pRectangle">The Bounding box</param>
        /// <returns>The transformed bounding box</returns> 
        public abstract Rect TransformBounds(Rect pRectangle);
        
        #endregion Methods
    }
}
