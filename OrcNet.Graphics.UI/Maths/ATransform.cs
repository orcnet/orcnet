﻿using System;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Base abstract 2D transformation class definition.
    /// </summary>
    public abstract class ATransform : AGeneralTransform, ICloneable
    {
        #region Fields

        /// <summary>
        /// Stores the default pTransform lIdentity transformation.
        /// </summary>
        private static ATransform sIdentity = MakeIdentityTransform();

        #endregion Fields

        #region Properties

        ///<summary> 
        /// Gets the Identity transformation. 
        ///</summary>
        public static ATransform Identity
        {
            get
            {
                return sIdentity;
            }
        }

        ///<summary>
        /// Return the current transformation value. 
        ///</summary>
        public abstract Matrix Value
        {
            get;
        }

        ///<summary> 
        /// Returns true if transformation if the transformation is definitely an lIdentity.  There are cases where it will
        /// return false because of computational error or presence of animations (And we're interpolating through a 
        /// transient lIdentity) -- this is intentional.  This property is used internally only.  If you need to check the 
        /// current lMatrix value for lIdentity, use Transform.Value.Identity.
        ///</summary> 
        internal abstract bool IsIdentity
        {
            get;
        }

        /// <summary> 
        /// Returns the inverse pTransform if it has an inverse, null otherwise
        /// </summary> 
        public override AGeneralTransform Inverse
        {
            get
            {
                Matrix lMatrix = Value;
                if (!lMatrix.HasInverse)
                {
                    return null;
                }

                lMatrix.Invert();
                return new MatrixTransform(lMatrix);
            }
        }

        /// <summary>
        /// Returns a best effort affine pTransform
        /// </summary>
        internal override ATransform AffineTransform
        {
            get
            {
                return this;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ATransform"/> class.
        /// </summary>
        internal ATransform()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Creates a new identity pTransform.
        /// </summary>
        /// <returns>The new identity pTransform.</returns>
        private static ATransform MakeIdentityTransform()
        {
            ATransform lIdentity = new MatrixTransform( Matrix.Identity );
            //lIdentity.Freeze();
            return lIdentity;
        }
        
        /// <summary>
        /// Gets the flag indicating whether the pTransform can be serialized to a string or not.
        /// </summary>
        /// <returns>True if it can, false otherwise.</returns>
        internal virtual bool CanSerializeToString()
        {
            return false;
        }
        
        /// <summary>
        /// Transforms a rectangle using this transformation.
        /// </summary>
        /// <param name="pRectangle">The rectangle to transform.</param>
        internal virtual void TransformRect(ref Rect pRectangle)
        {
            Matrix lMatrix = this.Value;
            MatrixUtil.TransformRect(ref pRectangle, ref lMatrix);
        }

        /// <summary> 
        /// Returns "this" * pToMultiplyBy.
        /// </summary> 
        /// <param name="pResult">The Result is stored here.</param>
        /// <param name="pToMultiplyBy">The multiplicand.</param>
        internal virtual void MultiplyValueByMatrix(ref Matrix pResult, ref Matrix pToMultiplyBy)
        {
            pResult = this.Value;
            MatrixUtil.MultiplyMatrix(ref pResult, ref pToMultiplyBy);
        }
        
        /// <summary>
        /// Consolidates the common logic of obtain the value of a
        /// Transform, after checking the pTransform for null. 
        /// </summary>
        /// <param name="pTransform">Transform to obtain value of.</param>
        /// <param name="pCurrentTransformValue">The Current value of 'pTransform'. Matrix.Identity if the 'pTransform' parameter is null.</param>
        internal static void GetTransformValue(ATransform pTransform, out Matrix pCurrentTransformValue)
        {
            if (pTransform != null)
            {
                pCurrentTransformValue = pTransform.Value;
            }
            else
            {
                pCurrentTransformValue = Matrix.Identity;
            }
        }

        /// <summary>
        /// Transforms a point 
        /// </summary>
        /// <param name="pPoint">Input point</param>
        /// <param name="pResult">Output point</param>
        /// <returns>True if the point was successfully transformed</returns> 
        public override bool TryTransform(Point pPoint, out Point pResult)
        {
            Matrix lMatrix = this.Value;
            pResult = lMatrix.Transform(pPoint);
            return true;
        }

        /// <summary>
        /// Transforms the bounding box to the smallest axis aligned bounding box 
        /// that contains all the points in the original bounding box
        /// </summary> 
        /// <param name="pRectangle">Bounding box</param>
        /// <returns>The transformed bounding box</returns>
        public override Rect TransformBounds(Rect pRectangle)
        {
            this.TransformRect(ref pRectangle);
            return pRectangle;
        }

        /// <summary>
        /// Clone the transform.
        /// </summary>
        /// <returns></returns>
        public abstract object Clone();

        #endregion Methods
    }
}
