﻿using OpenTK;
using System;
using System.Diagnostics;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// UI specific pMatrix structure definition.
    /// </summary>
    public struct Matrix
    {
        #region Fields

        /// <summary>
        /// Stores the transform that is identity by default
        /// Actually fill in the fields - some (internal) code uses the fields directly for perf.
        /// </summary>
        private static Matrix s_identity = CreateIdentity();

        /// <summary>
        /// The hash code for a pMatrix is the xor of its element's hashes.
        /// Since the identity pMatrix has 2 1's and 4 0's its hash is 0. 
        /// </summary>
        private const int cIdentityHashCode = 0;

        #region Internal Fields

        /// <summary>
        /// Stores the inner 2x2 matrix.
        /// </summary>
        internal Matrix2d mInnerMatrix;

        /// <summary>
        /// Stores the matrix offset on X
        /// </summary>
        internal double   mOffsetX;

        /// <summary>
        /// Stores the matrix offset on Y.
        /// </summary>
        internal double   mOffsetY;

        /// <summary>
        /// Stores the matrix transform state.
        /// </summary>
        internal MatrixState mState;

        // This field is only used by unmanaged code which isn't detected by the compiler. 
#pragma warning disable 0414
        // Matrix in blt'd to unmanaged code, so this is padding 
        // to align structure. 
        //
        // ToDo: [....], Validate that this blt will work on 64-bit 
        //
        internal Int32 _padding;
#pragma warning restore 0414

        #endregion 

        #endregion Fields

        #region Properties

        #region Private Properties

        /// <summary> 
        /// Efficient but conservative test for identity.  Returns
        /// true if the the pMatrix is identity.  If it returns false 
        /// the pMatrix may still be identity. 
        /// </summary>
        private bool IsDistinguishedIdentity
        {
            get
            {
                return mState == MatrixState.TRANSFORM_IS_IDENTITY;
            }
        }

        #endregion Private Properties

        /// <summary> 
        /// Identity
        /// </summary> 
        public static Matrix Identity
        {
            get
            {
                return s_identity;
            }
        }

        /// <summary>
        /// Tests whether or not a given transform is an identity transform 
        /// </summary>
        public bool IsIdentity
        {
            get
            {
                return (mState == MatrixState.TRANSFORM_IS_IDENTITY ||
                        (this.mInnerMatrix.M11 == 1 && this.mInnerMatrix.M12 == 0 && this.mInnerMatrix.M21 == 0 && this.mInnerMatrix.M22 == 1 && mOffsetX == 0 && mOffsetY == 0));
            }
        }

        /// <summary> 
        /// The determinant of this pMatrix
        /// </summary>
        public double Determinant
        {
            get
            {
                switch (mState)
                {
                    case MatrixState.TRANSFORM_IS_IDENTITY:
                    case MatrixState.TRANSFORM_IS_TRANSLATION:
                        return 1.0;
                    case MatrixState.TRANSFORM_IS_SCALING:
                    case MatrixState.TRANSFORM_IS_SCALING | MatrixState.TRANSFORM_IS_TRANSLATION:
                        return (this.mInnerMatrix.M11 * this.mInnerMatrix.M22);
                    default:
                        return (this.mInnerMatrix.M11 * this.mInnerMatrix.M22) - (this.mInnerMatrix.M12 * this.mInnerMatrix.M21);
                }
            }
        }

        /// <summary>
        /// HasInverse Property - returns true if this pMatrix is invertable, false otherwise. 
        /// </summary>
        public bool HasInverse
        {
            get
            {
                return !DoubleUtil.IsZero(Determinant);
            }
        }


        /// <summary> 
        /// Gets or sets the M11 value.
        /// </summary>
        public double M11
        {
            get
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    return 1.0;
                }
                else
                {
                    return this.mInnerMatrix.M11;
                }
            }
            set
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    SetMatrix(value, 0,
                              0, 1,
                              0, 0,
                              MatrixState.TRANSFORM_IS_SCALING);
                }
                else
                {
                    this.mInnerMatrix.M11 = value;
                    if (mState != MatrixState.TRANSFORM_IS_UNKNOWN)
                    {
                        mState |= MatrixState.TRANSFORM_IS_SCALING;
                    }
                }
            }
        }

        /// <summary> 
        /// Gets or sets the M12 value.
        /// </summary>
        public double M12
        {
            get
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    return 0;
                }
                else
                {
                    return this.mInnerMatrix.M12;
                }
            }
            set
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    SetMatrix(1, value,
                              0, 1,
                              0, 0,
                              MatrixState.TRANSFORM_IS_UNKNOWN);
                }
                else
                {
                    this.mInnerMatrix.M12 = value;
                    mState = MatrixState.TRANSFORM_IS_UNKNOWN;
                }
            }
        }

        /// <summary> 
        /// Gets or sets the M22 value.
        /// </summary>
        public double M21
        {
            get
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    return 0;
                }
                else
                {
                    return this.mInnerMatrix.M21;
                }
            }
            set
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    SetMatrix(1, 0,
                              value, 1,
                              0, 0,
                              MatrixState.TRANSFORM_IS_UNKNOWN);
                }
                else
                {
                    this.mInnerMatrix.M21 = value;
                    mState = MatrixState.TRANSFORM_IS_UNKNOWN;
                }
            }
        }

        /// <summary>
        /// Gets or sets the M22 value.
        /// </summary>
        public double M22
        {
            get
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    return 1.0;
                }
                else
                {
                    return this.mInnerMatrix.M22;
                }
            }
            set
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    SetMatrix(1, 0,
                              0, value,
                              0, 0,
                              MatrixState.TRANSFORM_IS_SCALING);
                }
                else
                {
                    this.mInnerMatrix.M22 = value;
                    if (mState != MatrixState.TRANSFORM_IS_UNKNOWN)
                    {
                        mState |= MatrixState.TRANSFORM_IS_SCALING;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the OffsetX value.
        /// </summary>
        public double OffsetX
        {
            get
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    return 0;
                }
                else
                {
                    return mOffsetX;
                }
            }
            set
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    SetMatrix(1, 0,
                              0, 1,
                              value, 0,
                              MatrixState.TRANSFORM_IS_TRANSLATION);
                }
                else
                {
                    mOffsetX = value;
                    if (mState != MatrixState.TRANSFORM_IS_UNKNOWN)
                    {
                        mState |= MatrixState.TRANSFORM_IS_TRANSLATION;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the OffsetY value.
        /// </summary>
        public double OffsetY
        {
            get
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    return 0;
                }
                else
                {
                    return mOffsetY;
                }
            }
            set
            {
                if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
                {
                    SetMatrix(1, 0,
                              0, 1,
                              0, value,
                              MatrixState.TRANSFORM_IS_TRANSLATION);
                }
                else
                {
                    mOffsetY = value;
                    if (mState != MatrixState.TRANSFORM_IS_UNKNOWN)
                    {
                        mState |= MatrixState.TRANSFORM_IS_TRANSLATION;
                    }
                }
            }
        }
        
        #endregion Properties

        #region Constructor 

        /// <summary> 
        /// Creates a Matrix of the form 
        ///             / p11, p12, 0 \
        ///             | p21, p22, 0 | 
        ///             \ pOffsetX, pOffsetY, 1 /
        /// </summary>
        public Matrix(double p11, double p12,
                      double p21, double p22,
                      double pOffsetX, double pOffsetY)
        {
            this.mInnerMatrix = new Matrix2d();
            this.mInnerMatrix.M11 = p11;
            this.mInnerMatrix.M12 = p12;
            this.mInnerMatrix.M21 = p21;
            this.mInnerMatrix.M22 = p22;
            this.mOffsetX = pOffsetX;
            this.mOffsetY = pOffsetY;
            mState = MatrixState.TRANSFORM_IS_UNKNOWN;
            _padding = 0;

            // We will detect EXACT identity, scale, translation or 
            // scale+translation and use special case algorithms.
            DeriveMatrixType();
        }

        #endregion Constructor

        #region Methods
        
        /// <summary> 
        /// Sets the pMatrix to identity.
        /// </summary>
        public void SetIdentity()
        {
            mState = MatrixState.TRANSFORM_IS_IDENTITY;
        }
        
        /// <summary>
        /// Multiplies two transformations. 
        /// </summary> 
        public static Matrix operator *(Matrix pTransform1, Matrix pTransform2)
        {
            MatrixUtil.MultiplyMatrix(ref pTransform1, ref pTransform2);
            pTransform1.Debug_CheckType();
            return pTransform1;
        }

        /// <summary> 
        /// Multiply 
        /// </summary>
        public static Matrix Multiply(Matrix pTransform1, Matrix pTransform2)
        {
            MatrixUtil.MultiplyMatrix(ref pTransform1, ref pTransform2);
            pTransform1.Debug_CheckType();
            return pTransform1;
        }
        
        /// <summary>
        /// Append - "this" becomes this * pMatrix, the same as this *= pMatrix.
        /// </summary> 
        /// <param name="pMatrix">The Matrix to prepend to this Matrix</param>
        public void Append(Matrix pMatrix)
        {
            this *= pMatrix;
        }

        /// <summary>
        /// Prepend - "this" becomes pMatrix * this, the same as this = pMatrix * this.
        /// </summary> 
        /// <param name="pMatrix">The Matrix to prepend to this Matrix</param>
        public void Prepend(Matrix pMatrix)
        {
            this = pMatrix * this;
        }

        /// <summary>
        /// Rotates this pMatrix about the origin
        /// </summary> 
        /// <param name="pAngle">The angle to rotate specifed in degrees</param>
        public void Rotate(double pAngle)
        {
            pAngle %= 360.0; // Doing the modulo before converting to radians reduces total error
            this *= CreateRotationRadians(pAngle * (System.Math.PI / 180.0));
        }

        /// <summary>
        /// Prepends a rotation about the origin to "this" 
        /// </summary>
        /// <param name="pAngle">The angle to rotate specifed in degrees</param>
        public void RotatePrepend(double pAngle)
        {
            pAngle %= 360.0; // Doing the modulo before converting to radians reduces total error 
            this = CreateRotationRadians(pAngle * (System.Math.PI / 180.0)) * this;
        }

        /// <summary> 
        /// Rotates this pMatrix about the given pPoint
        /// </summary> 
        /// <param name="pAngle">The angle to rotate specifed in degrees</param>
        /// <param name="pCenterX">The centerX of rotation</param>
        /// <param name="pCenterY">The centerY of rotation</param>
        public void RotateAt(double pAngle, double pCenterX, double pCenterY)
        {
            pAngle %= 360.0; // Doing the modulo before converting to radians reduces total error
            this *= CreateRotationRadians(pAngle * (System.Math.PI / 180.0), pCenterX, pCenterY);
        }

        /// <summary> 
        /// Prepends a rotation about the given pPoint to "this"
        /// </summary> 
        /// <param name="pAngle">The angle to rotate specifed in degrees</param>
        /// <param name="pCenterX">The centerX of rotation</param>
        /// <param name="pCenterY">The centerY of rotation</param>
        public void RotateAtPrepend(double pAngle, double pCenterX, double pCenterY)
        {
            pAngle %= 360.0; // Doing the modulo before converting to radians reduces total error 
            this = CreateRotationRadians(pAngle * (System.Math.PI / 180.0), pCenterX, pCenterY) * this;
        }

        /// <summary>
        /// Scales this pMatrix around the origin
        /// </summary>
        /// <param name="pScaleX">The scale factor in the pX dimension</param>
        /// <param name="pScaleY">The scale factor in the pY dimension</param>
        public void Scale(double pScaleX, double pScaleY)
        {
            this *= CreateScaling(pScaleX, pScaleY);
        }

        /// <summary>
        /// Prepends a scale around the origin to "this"
        /// </summary> 
        /// <param name="pScaleX">The scale factor in the pX dimension</param>
        /// <param name="pScaleY">The scale factor in the pY dimension</param>
        public void ScalePrepend(double pScaleX, double pScaleY)
        {
            this = CreateScaling(pScaleX, pScaleY) * this;
        }

        /// <summary>
        /// Scales this pMatrix around the center provided 
        /// </summary>
        /// <param name="pScaleX">The scale factor in the pX dimension</param>
        /// <param name="pScaleY">The scale factor in the pY dimension</param>
        /// <param name="pCenterX">The centerX about which to scale</param>
        /// <param name="pCenterY">The centerY about which to scale</param>
        public void ScaleAt(double pScaleX, double pScaleY, double pCenterX, double pCenterY)
        {
            this *= CreateScaling(pScaleX, pScaleY, pCenterX, pCenterY);
        }

        /// <summary> 
        /// Prepends a scale around the center provided to "this" 
        /// </summary>
        /// <param name="pScaleX">The scale factor in the pX dimension</param>
        /// <param name="pScaleY">The scale factor in the pY dimension</param>
        /// <param name="pCenterX">The centerX about which to scale</param>
        /// <param name="pCenterY">The centerY about which to scale</param>
        public void ScaleAtPrepend(double pScaleX, double pScaleY, double pCenterX, double pCenterY)
        {
            this = CreateScaling(pScaleX, pScaleY, pCenterX, pCenterY) * this;
        }

        /// <summary> 
        /// Skews this pMatrix
        /// </summary>
        /// <param name="pSkewX">The skew angle in the pX dimension in degrees</param>
        /// <param name="pSkewY">The skew angle in the pY dimension in degrees</param>
        public void Skew(double pSkewX, double pSkewY)
        {
            pSkewX %= 360;
            pSkewY %= 360;
            this *= CreateSkewRadians(pSkewX * (System.Math.PI / 180.0),
                                      pSkewY * (System.Math.PI / 180.0));
        }

        /// <summary> 
        /// Prepends a skew to this pMatrix
        /// </summary> 
        /// <param name="pSkewX">The skew angle in the pX dimension in degrees</param>
        /// <param name="pSkewY">The skew angle in the pY dimension in degrees</param>
        public void SkewPrepend(double pSkewX, double pSkewY)
        {
            pSkewX %= 360;
            pSkewY %= 360;
            this = CreateSkewRadians(pSkewX * (System.Math.PI / 180.0),
                                     pSkewY * (System.Math.PI / 180.0)) * this;
        }

        /// <summary>
        /// Translates this pMatrix 
        /// </summary>
        /// <param name="pOffsetX">The offset in the pX dimension</param>
        /// <param name="pOffsetY">The offset in the pY dimension</param>
        public void Translate(double pOffsetX, double pOffsetY)
        {
            // 
            // / a b 0 \   / 1 0 0 \    / a      b       0 \ 
            // | c d 0 | * | 0 1 0 | = |  c      d       0 |
            // \ e f 1 /   \ pX pY 1 /    \ e+pX    f+pY     1 / 
            //
            // (where e = mOffsetX and f == mOffsetY)
            //

            if (mState == MatrixState.TRANSFORM_IS_IDENTITY)
            {
                // Values would be incorrect if pMatrix was created using default constructor. 
                // or if SetIdentity was called on a pMatrix which had values.
                // 
                SetMatrix(1, 0,
                          0, 1,
                          pOffsetX, pOffsetY,
                          MatrixState.TRANSFORM_IS_TRANSLATION);
            }
            else if (mState == MatrixState.TRANSFORM_IS_UNKNOWN)
            {
                mOffsetX += pOffsetX;
                mOffsetY += pOffsetY;
            }
            else
            {
                mOffsetX += pOffsetX;
                mOffsetY += pOffsetY;

                // If pMatrix wasn't unknown we added a translation 
                mState |= MatrixState.TRANSFORM_IS_TRANSLATION;
            }

            Debug_CheckType();
        }

        /// <summary>
        /// Prepends a translation to this Matrix 
        /// </summary> 
        /// <param name="pOffsetX">The offset in the pX dimension</param>
        /// <param name="pOffsetY">The offset in the pY dimension</param>
        public void TranslatePrepend(double pOffsetX, double pOffsetY)
        {
            this = CreateTranslation(pOffsetX, pOffsetY) * this;
        }

        /// <summary>
        /// Returns the result of transforming the pPoint by this pMatrix
        /// </summary>
        /// <param name="pPoint">The Point to transform.</param>
        /// <returns>The transformed Point</returns>
        public Point Transform(Point pPoint)
        {
            double lX = pPoint.X;
            double lY = pPoint.Y;
            this.MultiplyPoint( ref lX, ref lY );
            return new Point( lX, lY );
        }

        /// <summary> 
        /// Transform - Transforms each pPoint in the array by this pMatrix 
        /// </summary>
        /// <param name="pPoints">The Point array to transform.</param>
        public void Transform(Point[] pPoints)
        {
            if 
                ( pPoints != null )
            {
                for 
                    ( int lCurr = 0; lCurr < pPoints.Length; lCurr++ )
                {
                    double lX = pPoints[ lCurr ].X;
                    double lY = pPoints[ lCurr ].Y;
                    this.MultiplyPoint( ref lX, ref lY );
                    pPoints[ lCurr ] = new Point( lX, lY );
                }
            }
        }

        /// <summary>
        /// Returns the result of transforming the Vector by this pMatrix. 
        /// </summary>
        /// <param name="pVector">The Vector to transform.</param>
        /// <returns>The transformed Vector</returns>
        public Vector2d Transform(Vector2d pVector)
        {
            Vector2d lNewVector = pVector;
            this.MultiplyVector( ref lNewVector.X, ref lNewVector.Y );
            return lNewVector;
        }

        /// <summary>
        /// Transforms each Vector in the array by this pMatrix. 
        /// </summary>
        /// <param name="pVectors">The Vector array to transform.</param>
        public void Transform(Vector2d[] pVectors)
        {
            if 
                ( pVectors != null )
            {
                for
                    ( int lCurr = 0; lCurr < pVectors.Length; lCurr++ )
                {
                    MultiplyVector( ref pVectors[lCurr].X, ref pVectors[lCurr].Y );
                }
            }
        }
        
        /// <summary>
        /// Replaces pMatrix with the inverse of the transformation.
        /// This will throw an InvalidOperationException if HasInverse == false 
        /// </summary>
        /// <exception cref="InvalidOperationException"> 
        /// This will throw an InvalidOperationException if the pMatrix is non-invertable
        /// </exception>
        public void Invert()
        {
            double determinant = Determinant;

            if (DoubleUtil.IsZero(determinant))
            {
                throw new InvalidOperationException("Cannot invert due to zero determinant!!!");
            }

            // Inversion does not change the pState of a pMatrix.
            switch (mState)
            {
                case MatrixState.TRANSFORM_IS_IDENTITY:
                    break;
                case MatrixState.TRANSFORM_IS_SCALING:
                    {
                        this.mInnerMatrix.M11 = 1.0 / this.mInnerMatrix.M11;
                        this.mInnerMatrix.M22 = 1.0 / this.mInnerMatrix.M22;
                    }
                    break;
                case MatrixState.TRANSFORM_IS_TRANSLATION:
                    mOffsetX = -mOffsetX;
                    mOffsetY = -mOffsetY;
                    break;
                case MatrixState.TRANSFORM_IS_SCALING | MatrixState.TRANSFORM_IS_TRANSLATION:
                    {
                        this.mInnerMatrix.M11 = 1.0 / this.mInnerMatrix.M11;
                        this.mInnerMatrix.M22 = 1.0 / this.mInnerMatrix.M22;
                        mOffsetX = -mOffsetX * this.mInnerMatrix.M11;
                        mOffsetY = -mOffsetY * this.mInnerMatrix.M22;
                    }
                    break;
                default:
                    {
                        double invdet = 1.0 / determinant;
                        SetMatrix(this.mInnerMatrix.M22 * invdet,
                                  -this.mInnerMatrix.M12 * invdet,
                                  -this.mInnerMatrix.M21 * invdet,
                                  this.mInnerMatrix.M11 * invdet,
                                  (this.mInnerMatrix.M21 * mOffsetY - mOffsetX * this.mInnerMatrix.M22) * invdet,
                                  (mOffsetX * this.mInnerMatrix.M12 - this.mInnerMatrix.M11 * mOffsetY) * invdet,
                                  MatrixState.TRANSFORM_IS_UNKNOWN);
                    }
                    break;
            }
        }
        
        /// <summary> 
        /// MultiplyVector 
        /// </summary>
        internal void MultiplyVector(ref double pX, ref double pY)
        {
            switch (mState)
            {
                case MatrixState.TRANSFORM_IS_IDENTITY:
                case MatrixState.TRANSFORM_IS_TRANSLATION:
                    return;
                case MatrixState.TRANSFORM_IS_SCALING:
                case MatrixState.TRANSFORM_IS_SCALING | MatrixState.TRANSFORM_IS_TRANSLATION:
                    pX *= this.mInnerMatrix.M11;
                    pY *= this.mInnerMatrix.M22;
                    break;
                default:
                    double xadd = pY * this.mInnerMatrix.M21;
                    double yadd = pX * this.mInnerMatrix.M12;
                    pX *= this.mInnerMatrix.M11;
                    pX += xadd;
                    pY *= this.mInnerMatrix.M22;
                    pY += yadd;
                    break;
            }
        }

        /// <summary>
        /// MultiplyPoint 
        /// </summary> 
        internal void MultiplyPoint(ref double pX, ref double pY)
        {
            switch (mState)
            {
                case MatrixState.TRANSFORM_IS_IDENTITY:
                    return;
                case MatrixState.TRANSFORM_IS_TRANSLATION:
                    pX += mOffsetX;
                    pY += mOffsetY;
                    return;
                case MatrixState.TRANSFORM_IS_SCALING:
                    pX *= this.mInnerMatrix.M11;
                    pY *= this.mInnerMatrix.M22;
                    return;
                case MatrixState.TRANSFORM_IS_SCALING | MatrixState.TRANSFORM_IS_TRANSLATION:
                    pX *= this.mInnerMatrix.M11;
                    pX += mOffsetX;
                    pY *= this.mInnerMatrix.M22;
                    pY += mOffsetY;
                    break;
                default:
                    double xadd = pY * this.mInnerMatrix.M21 + mOffsetX;
                    double yadd = pX * this.mInnerMatrix.M12 + mOffsetY;
                    pX *= this.mInnerMatrix.M11;
                    pX += xadd;
                    pY *= this.mInnerMatrix.M22;
                    pY += yadd;
                    break;
            }
        }

        /// <summary>
        /// Creates a rotation transformation about the given pPoint 
        /// </summary>
        /// <param name="pAngle">The angle to rotate specifed in radians</param>
        internal static Matrix CreateRotationRadians(double pAngle)
        {
            return CreateRotationRadians(pAngle, /* pCenterX = */ 0, /* pCenterY = */ 0);
        }

        /// <summary>
        /// Creates a rotation transformation about the given pPoint 
        /// </summary>
        /// <param name="pAngle">The angle to rotate specifed in radians</param>
        /// <param name="pCenterX">The pCenterX of rotation</param>
        /// <param name="pCenterY">The pCenterY of rotation</param>
        internal static Matrix CreateRotationRadians(double pAngle, double pCenterX, double pCenterY)
        {
            Matrix pMatrix = new Matrix();

            double sin = System.Math.Sin(pAngle);
            double cos = System.Math.Cos(pAngle);
            double dx = (pCenterX * (1.0 - cos)) + (pCenterY * sin);
            double dy = (pCenterY * (1.0 - cos)) - (pCenterX * sin);

            pMatrix.SetMatrix(cos, sin,
                              -sin, cos,
                              dx, dy,
                              MatrixState.TRANSFORM_IS_UNKNOWN);

            return pMatrix;
        }

        /// <summary>
        /// Creates a scaling transform around the given pPoint 
        /// </summary>
        /// <param name="pScaleX">The scale factor in the pX dimension</param>
        /// <param name="pScaleY">The scale factor in the pY dimension</param>
        /// <param name="pCenterX">The pCenterX of scaling</param>
        /// <param name="pCenterY">The pCenterY of scaling</param>
        internal static Matrix CreateScaling(double pScaleX, double pScaleY, double pCenterX, double pCenterY)
        {
            Matrix pMatrix = new Matrix();

            pMatrix.SetMatrix(pScaleX, 0,
                             0, pScaleY,
                             pCenterX - pScaleX * pCenterX, pCenterY - pScaleY * pCenterY,
                             MatrixState.TRANSFORM_IS_SCALING | MatrixState.TRANSFORM_IS_TRANSLATION);

            return pMatrix;
        }

        /// <summary> 
        /// Creates a scaling transform around the origin
        /// </summary>
        /// <param name="pScaleX">The scale factor in the pX dimension</param>
        /// <param name="pScaleY">The scale factor in the pY dimension</param>
        internal static Matrix CreateScaling(double pScaleX, double pScaleY)
        {
            Matrix pMatrix = new Matrix();
            pMatrix.SetMatrix(pScaleX, 0,
                             0, pScaleY,
                             0, 0,
                             MatrixState.TRANSFORM_IS_SCALING);
            return pMatrix;
        }

        /// <summary> 
        /// Creates a skew transform 
        /// </summary>
        /// <param name="pSkewX">The skew pAngle in the pX dimension in degrees</param>
        /// <param name="pSkewY">The skew pAngle in the pY dimension in degrees</param>
        internal static Matrix CreateSkewRadians(double pSkewX, double pSkewY)
        {
            Matrix pMatrix = new Matrix();

            pMatrix.SetMatrix(1.0, System.Math.Tan(pSkewY),
                             System.Math.Tan(pSkewX), 1.0,
                             0.0, 0.0,
                             MatrixState.TRANSFORM_IS_UNKNOWN);

            return pMatrix;
        }

        /// <summary>
        /// Sets the transformation to the given translation specified by the offset pVector. 
        /// </summary> 
        /// <param name="pOffsetX">The offset in X</param>
        /// <param name="pOffsetY">The offset in Y</param>
        internal static Matrix CreateTranslation(double pOffsetX, double pOffsetY)
        {
            Matrix pMatrix = new Matrix();

            pMatrix.SetMatrix(1, 0,
                              0, 1,
                              pOffsetX, pOffsetY,
                              MatrixState.TRANSFORM_IS_TRANSLATION );

            return pMatrix;
        }
        
        /// <summary> 
        /// Sets the transformation to the identity.
        /// </summary> 
        private static Matrix CreateIdentity()
        {
            Matrix pMatrix = new Matrix();
            pMatrix.SetMatrix(1, 0,
                             0, 1,
                             0, 0,
                             MatrixState.TRANSFORM_IS_IDENTITY);
            return pMatrix;
        }

        ///<summary>
        /// Sets the transform to
        ///             / p11, p12, 0 \ 
        ///             | p21, p22, 0 |
        ///             \ pOffsetX, pOffsetY, 1 / 
        /// where pOffsetX, pOffsetY is the translation. 
        ///</summary>
        private void SetMatrix(double p11, double p12,
                               double p21, double p22,
                               double pOffsetX, double pOffsetY,
                               MatrixState pState)
        {
            this.mInnerMatrix.M11 = p11;
            this.mInnerMatrix.M12 = p12;
            this.mInnerMatrix.M21 = p21;
            this.mInnerMatrix.M22 = p22;
            this.mOffsetX = pOffsetX;
            this.mOffsetY = pOffsetY;
            this.mState = pState;
        }

        /// <summary>
        /// Set the State of the Matrix based on its current contents 
        /// </summary> 
        private void DeriveMatrixType()
        {
            mState = 0;

            // Now classify our pMatrix.
            if (!(this.mInnerMatrix.M21 == 0 && this.mInnerMatrix.M12 == 0))
            {
                mState = MatrixState.TRANSFORM_IS_UNKNOWN;
                return;
            }

            if (!(this.mInnerMatrix.M11 == 1 && this.mInnerMatrix.M22 == 1))
            {
                mState = MatrixState.TRANSFORM_IS_SCALING;
            }

            if (!(mOffsetX == 0 && mOffsetY == 0))
            {
                mState |= MatrixState.TRANSFORM_IS_TRANSLATION;
            }

            if (0 == (mState & (MatrixState.TRANSFORM_IS_TRANSLATION | MatrixState.TRANSFORM_IS_SCALING)))
            {
                // We have an identity pMatrix. 
                mState = MatrixState.TRANSFORM_IS_IDENTITY;
            }
            return;
        }

        /// <summary>
        /// Asserts that the pMatrix tag is one of the valid options and
        /// that coefficients are correct.
        /// </summary> 
        [Conditional("DEBUG")]
        private void Debug_CheckType()
        {
            switch (mState)
            {
                case MatrixState.TRANSFORM_IS_IDENTITY:
                    return;
                case MatrixState.TRANSFORM_IS_UNKNOWN:
                    return;
                case MatrixState.TRANSFORM_IS_SCALING:
                    Debug.Assert(this.mInnerMatrix.M21 == 0);
                    Debug.Assert(this.mInnerMatrix.M12 == 0);
                    Debug.Assert(mOffsetX == 0);
                    Debug.Assert(mOffsetY == 0);
                    return;
                case MatrixState.TRANSFORM_IS_TRANSLATION:
                    Debug.Assert(this.mInnerMatrix.M21 == 0);
                    Debug.Assert(this.mInnerMatrix.M12 == 0);
                    Debug.Assert(this.mInnerMatrix.M11 == 1);
                    Debug.Assert(this.mInnerMatrix.M22 == 1);
                    return;
                case MatrixState.TRANSFORM_IS_SCALING | MatrixState.TRANSFORM_IS_TRANSLATION:
                    Debug.Assert(this.mInnerMatrix.M21 == 0);
                    Debug.Assert(this.mInnerMatrix.M12 == 0);
                    return;
                default:
                    Debug.Assert(false);
                    return;
            }
        }
        
        #endregion Methods
    }
}
