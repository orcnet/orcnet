﻿namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Size class definition made of a width and a height.
    /// </summary>
    public struct Size
    {
        #region Fields

        /// <summary>
        /// Stores the constant empty size.
        /// </summary>
        private readonly static Size sEmpty = new Size( 0.0, 0.0 );

        /// <summary>
        /// Stores the size width component.
        /// </summary>
        internal double Width;

        /// <summary>
        /// Stores the size height component.
        /// </summary>
        internal double Height;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the flag indicatng whether the size is an empty size or not.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this == Empty;
            }
        }

        /// <summary>
        /// Gets an Empty size.
        /// </summary>
        public static Size Empty
        {
            get
            {
                return sEmpty;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Size"/> class.
        /// </summary>
        /// <param name="pWidth">The width.</param>
        /// <param name="pHeight">The height.</param>
        public Size(double pWidth, double pHeight)
        {
            this.Width  = pWidth;
            this.Height = pHeight;
        }

        #endregion Constructor

        #region Methods 

        /// <summary>
        /// Compares two Size instances for exact equality. 
        /// Note that double values can acquire error when operated upon, such that 
        /// an exact comparison between two values which are logically equal may fail.
        /// Furthermore, using this equality operator, Double.NaN is not equal to itself. 
        /// </summary>
        /// <param name="pSize1">The first Size to compare.</param>
        /// <param name="pSize2">The second Size to compare.</param>
        /// <returns>True if the two Size instances are exactly equal, false otherwise.</returns>
        public static bool operator ==(Size pSize1, Size pSize2)
        {
            return pSize1.Width  == pSize2.Width &&
                   pSize1.Height == pSize2.Height;
        }

        /// <summary> 
        /// Compares two Size instances for exact inequality.
        /// Note that double values can acquire error when operated upon, such that 
        /// an exact comparison between two values which are logically equal may fail. 
        /// Furthermore, using this equality operator, Double.NaN is not equal to itself.
        /// </summary>
        /// <param name="pSize1">The first Size to compare.</param>
        /// <param name="pSize2">The second Size to compare.</param>
        /// <returns>True if the two Size instances are exactly unequal, false otherwise.</returns>
        public static bool operator !=(Size pSize1, Size pSize2)
        {
            return !(pSize1 == pSize2);
        }

        /// <summary>
        /// Compares two Size instances for object equality.
        /// In this equality Double.NaN is equal to itself, unlike in numeric equality.
        /// Note that double values can acquire error when operated upon, such that 
        /// an exact comparison between two values which
        /// are logically equal may fail. 
        /// </summary>
        /// <param name="pSize1">The first Size to compare.</param>
        /// <param name="pSize2">The second Size to compare.</param>
        /// <returns>True if the two Size instances are exactly equal, false otherwise.</returns>
        public static bool Equals(Size pSize1, Size pSize2)
        {
            if (pSize1.IsEmpty)
            {
                return pSize2.IsEmpty;
            }
            else
            {
                return pSize1.Width.Equals(pSize2.Width) &&
                       pSize1.Height.Equals(pSize2.Height);
            }
        }

        /// <summary>
        /// Compares this Size with the passed in object.
        /// In this equality Double.NaN is equal to itself, unlike in numeric equality.
        /// Note that double values can acquire error when operated upon, such that
        /// an exact comparison between two values which
        /// are logically equal may fail. 
        /// </summary>
        /// <param name="pOther">The object to compare to "this".</param>
        /// <returns>True if the object is an instance of Size and if it's equal to "this", false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            if 
                ( pOther == null || 
                  (pOther is Size) == false )
            {
                return false;
            }

            Size lValue = (Size)pOther;
            return Size.Equals( this, lValue );
        }

        /// <summary>
        /// Compares this Size with the passed in object.
        /// In this equality Double.NaN is equal to itself, unlike in numeric equality.
        /// Note that double values can acquire error when operated upon, such that 
        /// an exact comparison between two values which 
        /// are logically equal may fail.
        /// </summary>
        /// <param name="pValue">The Size to compare to "this".</param>
        /// <returns>True if "pValue" is equal to "this", false otherwise.</returns>
        public bool Equals(Size pValue)
        {
            return Size.Equals(this, pValue);
        }

        /// <summary> 
        /// Returns the HashCode for this Size
        /// </summary>
        /// <returns>The HashCode for this Size.</returns>
        public override int GetHashCode()
        {
            if (IsEmpty)
            {
                return 0;
            }
            else
            {
                // Perform field-by-field XOR of HashCodes
                return Width.GetHashCode() ^
                       Height.GetHashCode();
            }
        }
        
        #endregion Methods
    }
}
