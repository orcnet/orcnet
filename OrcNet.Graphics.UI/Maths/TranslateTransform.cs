﻿namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Translation transformation class definition.
    /// </summary>
    public sealed class TranslateTransform : ATransform
    {
        #region Fields

        /// <summary>
        /// Stores the translation on the X axis.
        /// </summary>
        private double mX;

        /// <summary>
        /// Stores the translation on the Y axis.
        /// </summary>
        private double mY;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the translation on the X axis.
        /// </summary>
        public double X
        {
            get
            {
                return this.mX;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mX, value, "X" );
            }
        }

        /// <summary>
        /// Gets or sets the translation on the Y axis.
        /// </summary>
        public double Y
        {
            get
            {
                return this.mY;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mY, value, "Y" );
            }
        }

        ///<summary>
        /// Return the current transformation value. 
        ///</summary>
        public override Matrix Value
        {
            get
            {
                Matrix lTranslation = Matrix.Identity;

                lTranslation.Translate( this.mX, this.mY );

                return lTranslation;
            }
        }

        ///<summary> 
        /// Returns true if transformation if the transformation is definitely an Identity.  There are cases where it will
        /// return false because of computational error or presence of animations (And we're interpolating through a 
        /// transient Identity) -- this is intentional.  This property is used internally only.  If you need to check the 
        /// current Matrix value for Identity, use Transform.Value.Identity.
        ///</summary> 
        internal override bool IsIdentity
        {
            get
            {
                return this.mX == 0 &&
                       this.mY == 0;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslateTransform"/> class.
        /// </summary>
        public TranslateTransform() :
        this( 0.0, 0.0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslateTransform"/> class.
        /// </summary>
        /// <param name="pOffsetX">The translation in the X axis.</param>
        /// <param name="pOffsetY">The translation in the Y axis.</param>
        public TranslateTransform(double pOffsetX, double pOffsetY)
        {
            this.mX = pOffsetX;
            this.mY = pOffsetY;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Transforms a rectangle using this transformation.
        /// </summary>
        /// <param name="pRectangle">The rectangle to transform.</param>
        internal override void TransformRect(ref Rect pRectangle)
        {
            if 
                ( pRectangle.IsEmpty == false )
            {
                pRectangle.Offset( this.mX, this.mY );
            }
        }

        /// <summary> 
        /// Returns "this" * pToMultiplyBy.
        /// </summary> 
        /// <param name="pResult">The Result is stored here.</param>
        /// <param name="pToMultiplyBy">The multiplicand.</param>
        internal override void MultiplyValueByMatrix(ref Matrix pResult, ref Matrix pToMultiplyBy)
        {
            pResult = Matrix.Identity;

            // Set the translation and state
            pResult.OffsetX = this.mX;
            pResult.OffsetY = this.mY;
            pResult.mState  = MatrixState.TRANSFORM_IS_TRANSLATION;

            MatrixUtil.MultiplyMatrix( ref pResult, ref pToMultiplyBy );
        }

        /// <summary>
        /// Clone the transform.
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new TranslateTransform( this.mX, this.mY );
        }

        #endregion Methods
    }
}
