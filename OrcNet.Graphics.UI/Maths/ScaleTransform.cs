﻿namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Scaling transformation class definition.
    /// </summary>
    public sealed class ScaleTransform : ATransform
    {
        #region Fields

        /// <summary>
        /// Stores the scaling on the X axis.
        /// </summary>
        private double mScaleX;

        /// <summary>
        /// Stores the scaling on the Y axis.
        /// </summary>
        private double mScaleY;

        /// <summary>
        /// Stores the scale center X component.
        /// </summary>
        private double mCenterX;

        /// <summary>
        /// Stores the scale center Y component.
        /// </summary>
        private double mCenterY;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the scaling on the X axis.
        /// </summary>
        public double ScaleX
        {
            get
            {
                return this.mScaleX;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mScaleX, value, "ScaleX" );
            }
        }

        /// <summary>
        /// Gets or sets the scaling on the Y axis.
        /// </summary>
        public double ScaleY
        {
            get
            {
                return this.mScaleY;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mScaleY, value, "ScaleY" );
            }
        }

        /// <summary>
        /// Gets or sets the scale center X component.
        /// </summary>
        public double CenterX
        {
            get
            {
                return this.mCenterX;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCenterX, value, "CenterX" );
            }
        }

        /// <summary>
        /// Gets or sets the scale center Y component.
        /// </summary>
        public double CenterY
        {
            get
            {
                return this.mCenterY;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCenterY, value, "CenterY" );
            }
        }

        ///<summary>
        /// Return the current transformation value. 
        ///</summary>
        public override Matrix Value
        {
            get
            {
                Matrix lScaling = new Matrix();

                lScaling.ScaleAt( this.mScaleX, this.mScaleY, this.mCenterX, this.mCenterY );

                return lScaling;
            }
        }

        ///<summary> 
        /// Returns true if transformation if the transformation is definitely an lIdentity.  There are cases where it will
        /// return false because of computational error or presence of animations (And we're interpolating through a 
        /// transient lIdentity) -- this is intentional.  This property is used internally only.  If you need to check the 
        /// current lMatrix value for lIdentity, use Transform.Value.Identity.
        ///</summary> 
        internal override bool IsIdentity
        {
            get
            {
                return this.mScaleX == 1 &&
                       this.mScaleY == 1;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScaleTransform"/> class.
        /// </summary>
        public ScaleTransform() :
        this( 1, 1, 0, 0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScaleTransform"/> class.
        /// </summary>
        /// <param name="pScaleX">The scaling on the X axis.</param>
        /// <param name="pScaleY">The scaling on the Y axis.</param>
        public ScaleTransform(double pScaleX, double pScaleY) :
        this( pScaleX, pScaleY, 0, 0 )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScaleTransform"/> class.
        /// </summary>
        /// <param name="pScaleX">The scaling on the X axis.</param>
        /// <param name="pScaleY">The scaling on the Y axis.</param>
        /// <param name="pCenterX">The scale center X component.</param>
        /// <param name="pCenterY">The scale center Y component.</param>
        public ScaleTransform(double pScaleX, double pScaleY, double pCenterX, double pCenterY)
        {
            this.mScaleX  = pScaleX;
            this.mScaleY  = pScaleY;
            this.mCenterX = pCenterX;
            this.mCenterY = pCenterY;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Transforms a rectangle using this transformation.
        /// </summary>
        /// <param name="pRectangle">The rectangle to transform.</param>
        internal override void TransformRect(ref Rect pRectangle)
        {
            if 
                ( pRectangle.IsEmpty )
            {
                return;
            }

            double lScaleX  = this.mScaleX;
            double lScaleY  = this.mScaleY;
            double lCenterX = this.mCenterX;
            double lCenterY = this.mCenterY;

            bool lHasCenter = lCenterX != 0 || lCenterY != 0;

            if 
                ( lHasCenter )
            {
                pRectangle.X -= lCenterX;
                pRectangle.Y -= lCenterY;
            }

            pRectangle.Scale( lScaleX, lScaleY );

            if 
                ( lHasCenter )
            {
                pRectangle.X += lCenterX;
                pRectangle.Y += lCenterY;
            }
        }

        /// <summary> 
        /// Returns "this" * pToMultiplyBy.
        /// </summary> 
        /// <param name="pResult">The Result is stored here.</param>
        /// <param name="pToMultiplyBy">The multiplicand.</param>
        internal override void MultiplyValueByMatrix(ref Matrix pResult, ref Matrix pToMultiplyBy)
        {
            pResult = Matrix.Identity;

            pResult.M11 = ScaleX;
            pResult.M22 = ScaleY;
            double lCenterX = this.mCenterX;
            double lCenterY = this.mCenterY;

            pResult.mState = MatrixState.TRANSFORM_IS_SCALING;

            if 
                ( lCenterX != 0 || lCenterY != 0 )
            {
                pResult.OffsetX = lCenterX - lCenterX * pResult.M11;
                pResult.OffsetY = lCenterY - lCenterY * pResult.M22;
                pResult.mState |= MatrixState.TRANSFORM_IS_TRANSLATION;
            }

            MatrixUtil.MultiplyMatrix( ref pResult, ref pToMultiplyBy );
        }
        
        /// <summary>
        /// Clone the transform.
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new ScaleTransform( this.mScaleX, this.mScaleY, this.mCenterX, this.mCenterY );
        }
        
        #endregion Methods
    }
}
