﻿using OpenTK;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// 2D X/Y coordinates pair.
    /// </summary>
    public struct Point
    {
        #region Fields

        /// <summary>
        /// Stores the X element of the coordinate.
        /// </summary>
        private double mX;

        /// <summary>
        /// Stores the Y element of the coordinate.
        /// </summary>
        private double mY;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the X element of the coordinate.
        /// </summary>
        public double X
        {
            get
            {
                return this.mX;
            }
            set
            {
                this.mX = value;
            }
        }

        /// <summary>
        /// Gets or sets the Y element of the coordinate.
        /// </summary>
        public double Y
        {
            get
            {
                return this.mY;
            }
            set
            {
                this.mY = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Point"/> class.
        /// </summary>
        /// <param name="pX">The X element of the coordinate.</param>
        /// <param name="pY">The Y element of the coordinate.</param>
        public Point(double pX, double pY)
        {
            this.mX = pX;
            this.mY = pY;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Checks whether two point are equal or not.
        /// </summary>
        /// <param name="pPoint1">The first point.</param>
        /// <param name="pPoint2">The second point.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public static bool operator == (Point pPoint1, Point pPoint2)
        {
            return pPoint1.mX == pPoint2.mX &&
                   pPoint1.mY == pPoint2.mY;
        }

        /// <summary>
        /// Checks whether two point are equal or not.
        /// </summary>
        /// <param name="pPoint1">The first point.</param>
        /// <param name="pPoint2">The second point.</param>
        /// <returns>True if equal, false otherwise.</returns>
        public static bool operator !=(Point pPoint1, Point pPoint2)
        {
            return !(pPoint1 == pPoint2);
        }

        /// <summary>
        /// Compares two objects together
        /// </summary>
        /// <param name="pOther">The other object</param>
        /// <returns>True if equal, false otherwise.</returns>
        public override bool Equals(object pOther)
        {
            if
                ( pOther == null || 
                  (pOther is Point) == false  )
            {
                return false;
            }

            Point lOther = (Point)pOther;
            return Point.Equals( this, lOther );
        }
        
        /// <summary>
        /// Gets the Point's hash code.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.mX.GetHashCode() ^
                   this.mY.GetHashCode();
        }

        /// <summary>
        /// Compares two Point instances for object equality.
        /// </summary>
        /// <param name="pPoint1">The first point.</param>
        /// <param name="pPoint2">The second point.</param>
        /// <returns>True if the two point instances are exactly equal, false otherwise.</returns>
        public static bool Equals(Point pPoint1, Point pPoint2)
        {
            if ( pPoint1 == null )
            {
                return pPoint2 == null;
            }
            else if
                ( pPoint2 == null )
            {
                return false;
            }
            else
            {
                return pPoint1.mX.Equals( pPoint2.mX ) &&
                       pPoint1.mY.Equals( pPoint2.mY );
            }
        }

        /// <summary>
        /// Adds an offset to the point on each axis.
        /// </summary> 
        /// <param name="pOffsetX">The offset in the x dimension</param>
        /// <param name="pOffsetY">The offset in the y dimension </param>
        public void Offset(double pOffsetX, double pOffsetY)
        {
            this.mX += pOffsetX;
            this.mY += pOffsetY;
        }

        /// <summary> 
        /// Translate a point in the positive direction.
        /// </summary>
        /// <param name="pPoint">The Point to translate</param>
        /// <param name="pVector">The translation double based.</param>
        /// <returns>The translated point.</returns>
        public static Point operator +(Point pPoint, Vector2d pVector)
        {
            return new Point( pPoint.mX + pVector.X, pPoint.mY + pVector.Y );
        }

        /// <summary> 
        /// Translate a point in the positive direction.
        /// </summary>
        /// <param name="pPoint">The Point to translate</param>
        /// <param name="pVector">The translation float based.</param>
        /// <returns>The translated point.</returns>
        public static Point operator +(Point pPoint, Vector2 pVector)
        {
            return new Point( pPoint.mX + pVector.X, pPoint.mY + pVector.Y );
        }

        /// <summary> 
        /// Translate a point in the positive direction.
        /// </summary>
        /// <param name="pPoint">The Point to translate</param>
        /// <param name="pVector">The translation double based.</param>
        /// <returns>The translated point.</returns>
        public static Point Add(Point pPoint, Vector2d pVector)
        {
            return new Point( pPoint.mX + pVector.X, pPoint.mY + pVector.Y );
        }

        /// <summary> 
        /// Translate a point in the positive direction.
        /// </summary>
        /// <param name="pPoint">The Point to translate</param>
        /// <param name="pVector">The translation float based.</param>
        /// <returns>The translated point.</returns>
        public static Point Add(Point pPoint, Vector2 pVector)
        {
            return new Point( pPoint.mX + pVector.X, pPoint.mY + pVector.Y );
        }

        /// <summary> 
        /// Translate a point in the negative direction.
        /// </summary>
        /// <param name="pPoint">The Point to translate</param>
        /// <param name="pVector">The translation double based.</param>
        /// <returns>The translated point.</returns>
        public static Point operator -(Point pPoint, Vector2d pVector)
        {
            return new Point( pPoint.mX - pVector.X, pPoint.mY - pVector.Y );
        }

        /// <summary> 
        /// Translate a point in the negative direction.
        /// </summary>
        /// <param name="pPoint">The Point to translate</param>
        /// <param name="pVector">The translation float based.</param>
        /// <returns>The translated point.</returns>
        public static Point operator -(Point pPoint, Vector2 pVector)
        {
            return new Point( pPoint.mX - pVector.X, pPoint.mY - pVector.Y );
        }

        /// <summary> 
        /// Translate a point in the negative direction.
        /// </summary>
        /// <param name="pPoint">The Point to translate</param>
        /// <param name="pVector">The translation double based.</param>
        /// <returns>The translated point.</returns>
        public static Point Subtract(Point pPoint, Vector2d pVector)
        {
            return new Point( pPoint.mX - pVector.X, pPoint.mY - pVector.Y );
        }

        /// <summary> 
        /// Translate a point in the negative direction.
        /// </summary>
        /// <param name="pPoint">The Point to translate</param>
        /// <param name="pVector">The translation float based.</param>
        /// <returns>The translated point.</returns>
        public static Point Subtract(Point pPoint, Vector2 pVector)
        {
            return new Point( pPoint.mX - pVector.X, pPoint.mY - pVector.Y );
        }

        /// <summary>
        /// Subtracts two points.
        /// </summary>
        /// <param name="pPoint1">The first point.</param>
        /// <param name="pPoint2">The second point.</param>
        /// <returns>The Vector2d between the two points.</returns>
        public static Vector2d operator -(Point pPoint1, Point pPoint2)
        {
            return new Vector2d( pPoint1.mX - pPoint2.mX, pPoint1.mY - pPoint2.mY );
        }

        /// <summary>
        /// Subtracts two points.
        /// </summary>
        /// <param name="pPoint1">The first point.</param>
        /// <param name="pPoint2">The second point.</param>
        /// <returns>The Vector2d between the two points.</returns>
        public static Vector2d Subtract(Point pPoint1, Point pPoint2)
        {
            return new Vector2d( pPoint1.mX - pPoint2.mX, pPoint1.mY - pPoint2.mY );
        }

        /// <summary>
        /// Transform a point using the supplied transform.
        /// </summary>
        /// <param name="pPoint">The point to transform</param>
        /// <param name="pMatrix">The transformation.</param>
        /// <returns>The transformed point.</returns>
        public static Point operator *(Point pPoint, Matrix pMatrix)
        {
            return pMatrix.Transform( pPoint );
        }

        /// <summary>
        /// Transform a point using the supplied transform.
        /// </summary>
        /// <param name="pPoint">The point to transform</param>
        /// <param name="pMatrix">The transformation.</param>
        /// <returns>The transformed point.</returns>
        public static Point Multiply(Point pPoint, Matrix pMatrix)
        {
            return pMatrix.Transform( pPoint );
        }
 
        /// <summary>
        /// Explicit conversion to Size.
        /// Note that since Size cannot contain negative values, the resulting size will contains the absolute values of X and Y 
        /// </summary>
        /// <param name="pPoint">The Point to convert to a Size</param>
        /// <returns>A Size equal to this Point</returns>
        public static explicit operator Size(Point pPoint)
        {
            return new Size( System.Math.Abs( pPoint.X ), System.Math.Abs( pPoint.Y ) );
        }

        /// <summary>
        /// Explicit Point to Vector2d convertion
        /// </summary>
        /// <param name="pPoint">The point to turn into a Vector2d</param>
        public static explicit operator Vector2d(Point pPoint)
        {
            return new Vector2d( pPoint.mX, pPoint.mY );
        }

        /// <summary>
        /// Explicit Point to Vector2d convertion
        /// </summary>
        /// <param name="pPoint">The point to turn into a Vector2d</param>
        public static explicit operator Vector2(Point pPoint)
        {
            return new Vector2( (float)pPoint.mX, (float)pPoint.mY );
        }

        #endregion Methods
    }
}
