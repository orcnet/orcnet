﻿namespace OrcNet.Graphics.UI.Maths
{

    /// <summary>
    /// Obliquity transformation class definition.
    /// </summary>
    public sealed class SkewTransform : ATransform
    {
        #region Fields

        /// <summary>
        /// Stores the obliquity angle in degrees around the X axis.
        /// </summary>
        private double mAngleX;

        /// <summary>
        /// Stores the obliquity angle in degrees around the Y axis.
        /// </summary>
        private double mAngleY;

        /// <summary>
        /// Stores the skew center X component.
        /// </summary>
        private double mCenterX;

        /// <summary>
        /// Stores the skew center Y component.
        /// </summary>
        private double mCenterY;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets  the obliquity angle in degrees around the X axis.
        /// </summary>
        public double AngleX
        {
            get
            {
                return this.mAngleX;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mAngleX, value, "AngleX" );
            }
        }

        /// <summary>
        /// Gets or sets the obliquity angle in degrees around the Y axis.
        /// </summary>
        public double AngleY
        {
            get
            {
                return this.mAngleY;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mAngleY, value, "AngleY" );
            }
        }
        
        /// <summary>
        /// Gets or sets the skew center X component.
        /// </summary>
        public double CenterX
        {
            get
            {
                return this.mCenterX;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCenterX, value, "CenterX" );
            }
        }

        /// <summary>
        /// Gets or sets the skew center Y component.
        /// </summary>
        public double CenterY
        {
            get
            {
                return this.mCenterY;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCenterY, value, "CenterY" );
            }
        }

        ///<summary>
        /// Return the current transformation value. 
        ///</summary>
        public override Matrix Value
        {
            get
            {
                Matrix lSkew = new Matrix();

                double lAngleX  = this.mAngleX;
                double lAngleY  = this.mAngleY;
                double lCenterX = this.mCenterX;
                double lCenterY = this.mCenterY;

                bool lHasCenter = lCenterX != 0 || lCenterY != 0;
                if 
                    ( lHasCenter )
                {
                    lSkew.Translate( -lCenterX, -lCenterY );
                }

                lSkew.Skew( lAngleX, lAngleY );

                if 
                    ( lHasCenter )
                {
                    lSkew.Translate( lCenterX, lCenterY );
                }

                return lSkew;
            }
        }

        ///<summary> 
        /// Returns true if transformation if the transformation is definitely an lIdentity.  There are cases where it will
        /// return false because of computational error or presence of animations (And we're interpolating through a 
        /// transient lIdentity) -- this is intentional.  This property is used internally only.  If you need to check the 
        /// current lMatrix value for lIdentity, use Transform.Value.Identity.
        ///</summary> 
        internal override bool IsIdentity
        {
            get
            {
                return this.mAngleX == 0 &&
                       this.mAngleY == 0;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SkewTransform"/> class.
        /// </summary>
        public SkewTransform() :
        this( 0, 0, 0, 0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SkewTransform"/> class.
        /// </summary>
        /// <param name="pAngleX">The obliquity angle in degrees around the X axis.</param>
        /// <param name="pAngleY">The obliquity angle in degrees around the Y axis.</param>
        public SkewTransform(double pAngleX, double pAngleY) :
        this( pAngleX, pAngleY, 0, 0 )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SkewTransform"/> class.
        /// </summary>
        /// <param name="pAngleX">The obliquity angle in degrees around the X axis.</param>
        /// <param name="pAngleY">The obliquity angle in degrees around the Y axis.</param>
        /// <param name="pCenterX">The skew center X component.</param>
        /// <param name="pCenterY">The skew center Y component.</param>
        public SkewTransform(double pAngleX, double pAngleY, double pCenterX, double pCenterY)
        {
            this.mAngleX  = pAngleX;
            this.mAngleY  = pAngleY;
            this.mCenterX = pCenterX;
            this.mCenterY = pCenterY;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Clone the transform.
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new SkewTransform( this.mAngleX, this.mAngleY, this.mCenterX, this.mCenterY );
        }

        #endregion Methods
    }
}
