﻿namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Arbitrary UI pMatrix factory class definition.
    /// </summary>
    public sealed class MatrixTransform : ATransform
    {
        #region Fields

        /// <summary>
        /// Stores the inner transform pMatrix.
        /// </summary>
        private Matrix mInnerMatrix;

        #endregion Fields

        #region Properties

        ///<summary> 
        /// Gets the current transformation value.
        ///</summary> 
        public override Matrix Value
        {
            get
            {
                return this.mInnerMatrix;
            }
        }
        
        ///<summary>
        /// Gets the flag indicating whether the transformation matches 
        /// the identity transform or not.
        ///</summary>
        internal override bool IsIdentity
        {
            get
            {
                return this.mInnerMatrix.IsIdentity;
            }
        }

        #endregion Properties

        #region Constructor

        ///<summary>
        /// Initializes a new instance of the <see cref="MatrixTransform"/> class.
        ///</summary>
        public MatrixTransform()
        {
        }

        ///<summary>
        /// Initializes a new instance of the <see cref="MatrixTransform"/> class.
        ///</summary>
        ///<param name="pM11">Matrix value at position 1,1</param>
        ///<param name="pM12">Matrix value at position 1,2</param>
        ///<param name="pM21">Matrix value at position 2,1</param>
        ///<param name="pM22">Matrix value at position 2,2</param>
        ///<param name="pOffsetX">Matrix value at position 3,1</param>
        ///<param name="pOffsetY">Matrix value at position 3,2</param>
        public MatrixTransform(double pM11,
                               double pM12,
                               double pM21,
                               double pM22,
                               double pOffsetX,
                               double pOffsetY)
        {
            this.mInnerMatrix = new Matrix( pM11, pM12, pM21, pM22, pOffsetX, pOffsetY );
        }

        ///<summary>
        /// Initializes a new instance of the <see cref="MatrixTransform"/> class.
        ///</summary> 
        ///<param name="pMatrix">The constant pMatrix transformation.</param>
        public MatrixTransform(Matrix pMatrix)
        {
            this.mInnerMatrix = pMatrix;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Transforms a rectangle using this transformation.
        /// </summary>
        /// <param name="pRectangle">The rectangle to transform.</param>
        internal override void TransformRect(ref Rect pRectangle)
        {
            Matrix lMatrix = this.mInnerMatrix;
            MatrixUtil.TransformRect(ref pRectangle, ref lMatrix);
        }

        /// <summary> 
        /// Returns "this" * pToMultiplyBy.
        /// </summary> 
        /// <param name="pResult">The Result is stored here.</param>
        /// <param name="pToMultiplyBy">The multiplicand.</param>
        internal override void MultiplyValueByMatrix(ref Matrix pResult, ref Matrix pToMultiplyBy)
        {
            pResult = this.mInnerMatrix;
            MatrixUtil.MultiplyMatrix(ref pResult, ref pToMultiplyBy);
        }

        /// <summary>
        /// Clone the transform.
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new MatrixTransform( this.mInnerMatrix );
        }

        #endregion Methods
    }
}
