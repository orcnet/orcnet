﻿namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Rotation transformation class definition.
    /// </summary>
    public sealed class RotateTransform : ATransform
    {
        #region Fields

        /// <summary>
        /// Stores the rotation angle in degrees.
        /// </summary>
        private double mAngle;

        /// <summary>
        /// Stores the rotation center X component.
        /// </summary>
        private double mCenterX;

        /// <summary>
        /// Stores the rotation center Y component.
        /// </summary>
        private double mCenterY;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the rotation angle in degrees.
        /// </summary>
        public double Angle
        {
            get
            {
                return this.mAngle;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mAngle, value, "Angle" );
            }
        }

        /// <summary>
        /// Gets or sets the rotation center X component.
        /// </summary>
        public double CenterX
        {
            get
            {
                return this.mCenterX;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCenterX, value, "CenterX" );
            }
        }

        /// <summary>
        /// Gets or sets the rotation center Y component.
        /// </summary>
        public double CenterY
        {
            get
            {
                return this.mCenterY;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mCenterY, value, "CenterY" );
            }
        }

        ///<summary>
        /// Return the current transformation value. 
        ///</summary>
        public override Matrix Value
        {
            get
            {
                Matrix lRotation = new Matrix();

                lRotation.RotateAt( this.mAngle, this.mCenterX, this.mCenterY );

                return lRotation;
            }
        }

        ///<summary> 
        /// Returns true if transformation if the transformation is definitely an lIdentity.  There are cases where it will
        /// return false because of computational error or presence of animations (And we're interpolating through a 
        /// transient lIdentity) -- this is intentional.  This property is used internally only.  If you need to check the 
        /// current lMatrix value for lIdentity, use Transform.Value.Identity.
        ///</summary> 
        internal override bool IsIdentity
        {
            get
            {
                return this.mAngle == 0;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RotateTransform"/> class.
        /// </summary>
        public RotateTransform() :
        this( 0, 0, 0 )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RotateTransform"/> class.
        /// </summary>
        /// <param name="pAngle">The rotation angle in degrees.</param>
        public RotateTransform(double pAngle) :
        this( pAngle, 0, 0 )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RotateTransform"/> class.
        /// </summary>
        /// <param name="pAngle">The rotation angle in degrees.</param>
        /// <param name="pCenterX">The rotation center X component.</param>
        /// <param name="pCenterY">The rotation center Y component.</param>
        public RotateTransform(double pAngle, double pCenterX, double pCenterY)
        {
            this.mAngle   = pAngle;
            this.mCenterX = pCenterX;
            this.mCenterY = pCenterY;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Clone the transform.
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new RotateTransform( this.mAngle, this.mCenterX, this.mCenterY );
        }

        #endregion Methods
    }
}
