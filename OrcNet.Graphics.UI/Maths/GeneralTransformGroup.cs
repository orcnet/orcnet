﻿using System.Collections.Generic;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Set of General transformations inducing a hierarchy.
    /// </summary>
    public sealed partial class GeneralTransformGroup : AGeneralTransform
    {
        #region Fields

        /// <summary>
        /// Stores the set of children transforms.
        /// </summary>
        private List<AGeneralTransform> mChildren;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the set of children transforms.
        /// </summary>
        public List<AGeneralTransform> Children
        {
            get
            {
                return this.mChildren;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mChildren, value, "Children" );
            }
        }

        /// <summary>
        /// Returns a best effort affine transform
        /// </summary>        
        internal override ATransform AffineTransform
        {
            get
            {
                if 
                    ( this.mChildren == null || 
                      this.mChildren.Count == 0 )
                {
                    return null;
                }

                Matrix lMatrix = Matrix.Identity;
                foreach 
                    ( AGeneralTransform lTransform in this.mChildren )
                {
                    ATransform lAffine = lTransform.AffineTransform;
                    if 
                        ( lAffine != null )
                    {
                        lMatrix *= lAffine.Value;
                    }
                }

                return new MatrixTransform( lMatrix );
            }
        }

        /// <summary>
        /// Returns the inverse transform if it has an inverse, null otherwise
        /// </summary>        
        public override AGeneralTransform Inverse
        {
            get
            {
                if 
                    ( this.mChildren == null || 
                      this.mChildren.Count == 0 )
                {
                    return null;
                }

                GeneralTransformGroup lGroup = new GeneralTransformGroup();
                for 
                    ( int lCurr = this.mChildren.Count - 1; lCurr >= 0; lCurr-- )
                {
                    AGeneralTransform lTransform = this.mChildren[ lCurr ].Inverse;

                    // if any of the transforms does not have an inverse,
                    // then the entire group does not have one
                    if 
                        ( lTransform == null )
                    {
                        return null;
                    }
                    
                    lGroup.Children.Add( lTransform );
                }

                return lGroup;
            }
        }

        #endregion Properties

        #region Constructor

        ///<summary>
        /// Initializes a new instance of the <see cref="GeneralTransformGroup"/> class.
        ///</summary>
        public GeneralTransformGroup()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Transform a point
        /// </summary>
        /// <param name="pPoint">input point</param>
        /// <param name="pResult">output point</param>
        /// <returns>True if the point is transformed successfully</returns>
        public override bool TryTransform(Point pPoint, out Point pResult)
        {
            pResult = pPoint;
            if 
                ( this.mChildren == null || 
                  this.mChildren.Count == 0 )
            {
                return false;
            }

            Point lPoint = pPoint;
            bool lIsPointTransformed = true;
            // transform the point through each of the transforms
            for 
                ( int lCurr = 0; lCurr < this.mChildren.Count; lCurr++ )
            {
                if 
                    ( this.mChildren[ lCurr ].TryTransform( pPoint, out pResult ) == false )
                {
                    lIsPointTransformed = false;
                }

                pPoint = pResult;
            }

            return lIsPointTransformed;
        }

        /// <summary>
        /// Transforms the bounding box to the smallest axis aligned bounding box
        /// that contains all the points in the original bounding box
        /// </summary>
        /// <param name="pRectangle">Input bounding rect</param>
        /// <returns>Transformed bounding rect</returns>
        public override Rect TransformBounds(Rect pRectangle)
        {
            if 
                ( this.mChildren == null || 
                  this.mChildren.Count == 0 )
            {
                return pRectangle;
            }

            Rect lResult = pRectangle;
            for 
                ( int lCurr = 0; lCurr < this.mChildren.Count; lCurr++ )
            {
                lResult = this.mChildren[ lCurr ].TransformBounds( lResult );
            }

            return lResult;
        }

        #endregion Methods
    }
}
