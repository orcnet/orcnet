﻿namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// UI matrix transformation state
    /// </summary>
    internal enum MatrixState : int
    {
        /// <summary>
        /// Identity matrix.
        /// </summary>
        TRANSFORM_IS_IDENTITY = 0,

        /// <summary>
        /// Translation matrix.
        /// </summary>
        TRANSFORM_IS_TRANSLATION = 1,

        /// <summary>
        /// Scaling matrix.
        /// </summary>
        TRANSFORM_IS_SCALING = 2,
        
        /// <summary>
        /// Unknown transform.
        /// </summary>
        TRANSFORM_IS_UNKNOWN = 4,
    }
}
