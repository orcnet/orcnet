﻿using OpenTK;
using OrcNet.Graphics.UI.Core;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Point utility class definition.
    /// </summary>
    internal static class PointUtil
    {
        #region Methods

        /// <summary>
        /// Convert a pPoint from "client" coordinate space of a window into
        /// the coordinate space of the root element of the same window.
        /// </summary>
        /// <param name="pPoint">The point to transform.</param>
        /// <param name="pPresentationSource">The presentation source needed to transform the point.</param>
        /// <returns></returns>
        public static Point ClientToRoot(Point pPoint, PresentationSource pPresentationSource)
        {
            bool lSuccess = true;
            return TryClientToRoot( pPoint, pPresentationSource, true, out lSuccess );
        }

        /// <summary>
        /// Try to convert a point from client to root space.
        /// </summary>
        /// <param name="pPoint">The point to transform.</param>
        /// <param name="pPresentationSource">The presentation source needed to transform the point.</param>
        /// <param name="pThrowOnError">The flag indicating whether or not to throw on errors.</param>
        /// <param name="pSuccess">The flag indicating whether the transformation is successful or not.</param>
        /// <returns>The point in root space.</returns>
        public static Point TryClientToRoot(Point pPoint, PresentationSource pPresentationSource, bool pThrowOnError, out bool pSuccess)
        {
            // Only do if we allow throwing on error or have a valid PresentationSource and CompositionTarget.
            if 
                ( pThrowOnError || 
                  (pPresentationSource != null && pPresentationSource.Source != null) )
            {
                // Convert from pixels into measure units.
                //System.Drawing.Point lClientPoint = pPresentationSource.Source.PointToClient( new System.Drawing.Point( (int)pPoint.X, (int)pPoint.Y ) );
                
                pPoint = TryApplyVisualTransform( pPoint,//new Point( lClientPoint.X, lClientPoint.Y ),
                                                  pPresentationSource.RootVisual, 
                                                  true, 
                                                  pThrowOnError, 
                                                  out pSuccess );
            }
            else
            {
                pSuccess = false;
                return new Point( 0, 0 );
            }

            return pPoint;
        }

        /// <summary>
        /// Convert a pPoint from the coordinate space of a root element of
        /// a window into the "client" coordinate space of the same window.
        /// </summary>
        /// <param name="pPoint">The point to transform.</param>
        /// <param name="pPresentationSource">The presentation source needed to transform the point.</param>
        /// <returns>The point in client space.</returns>
        public static Point RootToClient(Point pPoint, PresentationSource pPresentationSource)
        {
            pPoint = ApplyVisualTransform( pPoint, 
                                           pPresentationSource.RootVisual, 
                                           false );

            //System.Drawing.Point lScreenP = pPresentationSource.CompositionTarget.TransformToDevice.Transform(pPoint);

            return pPoint;
        }

        /// <summary>
        /// Convert a pPoint from "above" the coordinate space of a
        /// visual into the the coordinate space "below" the visual.
        /// </summary>
        public static Point ApplyVisualTransform(Point pPoint, AVisual pVisual, bool pInverse)
        {
            bool lSuccess = true;
            return TryApplyVisualTransform( pPoint, pVisual, pInverse, true, out lSuccess );
        }

        /// <summary>
        /// Convert a pPoint from "above" the coordinate space of a
        /// visual into the the coordinate space "below" the visual.
        /// </summary>
        /// <param name="pPoint">The point to transform.</param>
        /// <param name="pVisual">The visual element owning the transformation.</param>
        /// <param name="pInverse">The flag indicating whether the inverse of the transform must be applied or not.</param>
        /// <param name="pThrowOnError">The flag indicating whether or not to throw on errors.</param>
        /// <param name="pSuccess">The flag indicating whether the transformation is successful or not.</param>
        /// <returns>The transformed point.</returns>
        public static Point TryApplyVisualTransform(Point pPoint, AVisual pVisual, bool pInverse, bool pThrowOnError, out bool pSuccess)
        {
            pSuccess = true;

            // Notes:
            // 1) First of all the MIL should provide a way of transforming
            //    a pPoint from the window to the root element.
            // 2) A visual can currently have two properties that affect
            //    its coordinate space:
            //    A) Transform - any matrix
            //    B) Offset - a simpification for just a 2D lOffset.
            // 3) In the future a AVisual may have other properties that
            //    affect its coordinate space, which is why the MIL should
            //    provide this API in the first place.
            //
            // The following code was copied from the MIL's TransformToAncestor
            // method on 12/16/2005.
            //
            if
                ( pVisual != null )
            {
                Matrix lMatrix = GetVisualTransform( pVisual );
                if 
                    ( pInverse )
                {
                    if 
                        ( pThrowOnError || 
                          lMatrix.HasInverse )
                    {
                        lMatrix.Invert();
                    }
                    else
                    {
                        pSuccess = false;
                        return new Point( 0, 0 );
                    }
                }

                pPoint = lMatrix.Transform( pPoint );
            }

            return pPoint;
        }

        /// <summary>
        /// Gets the matrix that will convert a pPoint
        /// from "above" the coordinate space of a visual
        /// into the the coordinate space "below" the visual.
        /// </summary>
        /// <param name="pVisual">The visual element the transform is needed.</param>
        internal static Matrix GetVisualTransform(AVisual pVisual)
        {
            if 
                ( pVisual != null )
            {
                Matrix lMatrix = Matrix.Identity;
                ATransform lTransform = pVisual.VisualTransform;
                if 
                    ( lTransform != null )
                {
                    Matrix cm = lTransform.Value;
                    lMatrix = Matrix.Multiply( lMatrix, cm );
                }

                Vector2d lOffset = pVisual.VisualOffset;
                lMatrix.Translate( lOffset.X, lOffset.Y );

                return lMatrix;
            }

            return Matrix.Identity;
        }

        /// <summary>
        /// Convert a pPoint from "client" coordinate space of a window into
        /// the coordinate space of the screen.
        /// </summary>
        /// <param name="pPointToClient">The point to transform.</param>
        /// <param name="pPresentationSource">The presentation source.</param>
        /// <returns>The point in screen space.</returns>
        public static Point ClientToScreen(Point pPointToClient, PresentationSource pPresentationSource)
        {
            if 
                ( pPresentationSource == null )
            {
                return pPointToClient;
            }

            System.Drawing.Point lScreenpoint = pPresentationSource.Source.PointToScreen( new System.Drawing.Point( (int)pPointToClient.X, (int)pPointToClient.Y ) );
            
            return new Point( lScreenpoint.X, lScreenpoint.Y );
        }

        /// <summary>
        /// Convert a pPoint from the coordinate space of the screen into
        /// the "client" coordinate space of a window.
        /// </summary>
        /// <param name="pScreenPoint">The point to transform.</param>
        /// <param name="pPresentationSource">The presentation source.</param>
        /// <returns>The point in client space.</returns>
        internal static Point ScreenToClient(Point pScreenPoint, PresentationSource pPresentationSource)
        {
            if 
                ( pPresentationSource == null ||
                  pPresentationSource.Source == null )
            {
                return pScreenPoint;
            }

            System.Drawing.Point lClientSpace = pPresentationSource.Source.PointToClient( new System.Drawing.Point() );

            return new Point( lClientSpace.X, lClientSpace.Y );
        }

        /// <summary>
        /// Converts a rectangle from element co-ordinate space to that of the root visual
        /// </summary>
        /// <param name="pRectElement">The rectangle to be converted</param>
        /// <param name="pElement">The element whose co-ordinate space you wish to convert from</param>
        /// <param name="pPresentationSource">The PresentationSource which hosts the specified AVisual. This is passed in for performance reasons.</param>
        /// <returns>The rectangle in the co-ordinate space of the root visual</returns>
        internal static Rect ElementToRoot(Rect pRectElement, AVisual pElement, PresentationSource pPresentationSource)
        {
            AGeneralTransform lTransformElementToRoot = pElement.TransformToAncestor( pPresentationSource.RootVisual );
            Rect lRectRoot = lTransformElementToRoot.TransformBounds( pRectElement );
            return lRectRoot;
        }

        /// <summary>
        /// Converts a rectangle from root visual co-ordinate space to client
        /// </summary>
        /// <param name="pRectRoot">The rectangle to be converted</param>
        /// <param name="pPresentationSource">The PresentationSource which hosts the root visual. This is passed in for performance reasons.</param>
        /// <returns>The rectangle in client co-ordinate space.</returns>
        internal static Rect RootToClient(Rect pRectRoot, PresentationSource pPresentationSource)
        {
            Matrix lMatrixRootTransform = PointUtil.GetVisualTransform( pPresentationSource.RootVisual );
            Rect lRectClient = Rect.Transform( pRectRoot, lMatrixRootTransform );
            return lRectClient;
        }

        /// <summary>
        /// Converts a rectangle from client co-ordinate space to Win32 screen
        /// </summary>
        /// <param name="pRectClient">The rectangle to be converted</param>
        /// <param name="pPresentationSource">The PresentationSource corresponding to the window containing the rectangle</param>
        /// <returns>The rectangle in screen co-ordinate space.</returns>
        internal static Rect ClientToScreen(Rect pRectClient, PresentationSource pPresentationSource)
        {
            Point lCorner1 = ClientToScreen( pRectClient.TopLeft, pPresentationSource );
            Point lCorner2 = ClientToScreen( pRectClient.BottomRight, pPresentationSource );
            return new Rect( lCorner1, lCorner2 );
        }

        #endregion Methods
    }
}
