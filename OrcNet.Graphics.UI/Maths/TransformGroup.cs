﻿using OrcNet.Graphics.UI.DataStructures;
using System.Linq;

namespace OrcNet.Graphics.UI.Maths
{
    /// <summary>
    /// Group of transformation class definition.
    /// </summary>
    public sealed class TransformGroup : ATransform
    {
        #region Fields
        
        /// <summary>
        /// Stores the group transformations.
        /// </summary>
        private TransformCollection mTransformations;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the set of transformations of the group.
        /// </summary>
        public TransformCollection Transformations
        {
            get
            {
                return this.mTransformations;
            }
            set
            {
                this.NotifyPropertyChanged( ref this.mTransformations, value, "Transformations" );
            }
        }

        ///<summary>
        /// Return the current transformation value. 
        ///</summary>
        public override Matrix Value
        {
            get
            {
                TransformCollection lTransformations = this.Transformations;
                if 
                    ( lTransformations == null || 
                      lTransformations.Count == 0 )
                {
                    return new Matrix();
                }

                Matrix lFirstTransform = lTransformations.First().Value;
                for 
                    ( int lCurr = 1; lCurr < lTransformations.Count; lCurr++ )
                {
                    lFirstTransform *= lTransformations[ lCurr ].Value;
                }

                return lFirstTransform;
            }
        }

        ///<summary> 
        /// Returns true if transformation if the transformation is definitely an lIdentity.  There are cases where it will
        /// return false because of computational error or presence of animations (And we're interpolating through a 
        /// transient lIdentity) -- this is intentional.  This property is used internally only.  If you need to check the 
        /// current lMatrix value for lIdentity, use Transform.Value.Identity.
        ///</summary> 
        internal override bool IsIdentity
        {
            get
            {
                TransformCollection lTransformations = this.Transformations;
                if 
                    ( lTransformations == null || 
                      lTransformations.Count == 0 )
                {
                    return true;
                }

                for 
                    ( int lCurr = 0; lCurr < lTransformations.Count; lCurr++ )
                {
                    if 
                        ( lTransformations[ lCurr ].IsIdentity == false )
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TransformGroup"/> class.
        /// </summary>
        public TransformGroup()
        {

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Gets the flag indicating whether the pTransform can be serialized to a string or not.
        /// </summary>
        /// <returns>True if it can, false otherwise.</returns>
        internal override bool CanSerializeToString()
        {
            return false;
        }

        /// <summary>
        /// Clone the transform.
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            TransformGroup lClone = new TransformGroup();
            foreach
                ( ATransform lTransform in this.mTransformations )
            {
                lClone.mTransformations.Add( lTransform.Clone() as ATransform );
            }
            return lClone;
        }

        #endregion Methods
    }
}
