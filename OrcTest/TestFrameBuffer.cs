﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Core.Render;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms;
using OrcTest.Helpers;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace OrcTest
{
    /// <summary>
    /// Frame buffer based test class definition.
    /// </summary>
    [TestClass]
    public class TestFrameBuffer
    {
        #region Fields

        /// <summary>
        /// Stores the constant layer instancing shader code.
        /// </summary>
        private static readonly string cLAYER_INSTANCING;

        /// <summary>
        /// Stores the constant fragment shader code using integers.
        /// </summary>
        private static readonly string cFRAGMENT_SHADER;

        /// <summary>
        /// Stores the constant fragment shader code using floats.
        /// </summary>
        private static readonly string cFRAGMENT_SHADER_FLOAT;

        /// <summary>
        /// Stores the constant draw instancing shader code.
        /// </summary>
        private static readonly string cDRAW_INSTANCING;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes the static member(s) of the <see cref="TestFrameBuffer"/> class.
        /// </summary>
        static TestFrameBuffer()
        {
            StringBuilder lLayerInstancingCode = new StringBuilder();
            lLayerInstancingCode.AppendLine( "#ifdef _VERTEX_" );
            lLayerInstancingCode.AppendLine( "layout(location=0) in vec4 pos;" );
            lLayerInstancingCode.AppendLine( "void main() { gl_Position = pos; }" );
            lLayerInstancingCode.AppendLine( "#endif" );
            lLayerInstancingCode.AppendLine( "#ifdef _GEOMETRY_" );
            lLayerInstancingCode.AppendLine( "layout(triangles) in;" );
            lLayerInstancingCode.AppendLine( "layout(triangle_strip, max_vertices = 24) out;" );
            lLayerInstancingCode.AppendLine( "in vec4 pos[];" );
            lLayerInstancingCode.AppendLine( "void main() {" );
            lLayerInstancingCode.AppendLine( "    for (int i = 0; i < 8; ++i) {" );
            lLayerInstancingCode.AppendLine( "        gl_Layer = i;" );
            lLayerInstancingCode.AppendLine( "        gl_Position = gl_in[0].gl_Position;" );
            lLayerInstancingCode.AppendLine( "        EmitVertex();" );
            lLayerInstancingCode.AppendLine( "        gl_Position = gl_in[1].gl_Position;" );
            lLayerInstancingCode.AppendLine( "        EmitVertex();" );
            lLayerInstancingCode.AppendLine( "        gl_Position = gl_in[2].gl_Position;" );
            lLayerInstancingCode.AppendLine( "        EmitVertex();" );
            lLayerInstancingCode.AppendLine( "        EndPrimitive();" );
            lLayerInstancingCode.AppendLine( "    }" );
            lLayerInstancingCode.AppendLine( "}" );
            lLayerInstancingCode.AppendLine( "#endif" );
            cLAYER_INSTANCING = lLayerInstancingCode.ToString();

            StringBuilder lFragmentShaderIntCode = new StringBuilder();
            lFragmentShaderIntCode.AppendLine( "#ifdef _FRAGMENT_" );
            lFragmentShaderIntCode.AppendLine( "layout(location=0) out ivec4 color;" );
            lFragmentShaderIntCode.AppendLine( "void main() { color = ivec4(1, 2, 3, 4); }" );
            lFragmentShaderIntCode.AppendLine( "#endif" );
            cFRAGMENT_SHADER = lFragmentShaderIntCode.ToString();

            StringBuilder lFragmentShaderFloatCode = new StringBuilder();
            lFragmentShaderFloatCode.AppendLine( "#ifdef _FRAGMENT_" );
            lFragmentShaderFloatCode.AppendLine( "layout(location=0) out vec4 color;" );
            lFragmentShaderFloatCode.AppendLine( "void main() { color = vec4(1.0, 2.0, 3.0, 4.0); }" );
            lFragmentShaderFloatCode.AppendLine( "#endif" );
            cFRAGMENT_SHADER_FLOAT = lFragmentShaderFloatCode.ToString();

            StringBuilder lDrawInstancingCode = new StringBuilder();
            lDrawInstancingCode.AppendLine( "#ifdef _VERTEX_" );
            lDrawInstancingCode.AppendLine( "layout(location=0) in vec4 pos;" );
            lDrawInstancingCode.AppendLine( "flat out int instance;" );
            lDrawInstancingCode.AppendLine( "void main() { gl_Position = pos; instance = gl_InstanceID; }" );
            lDrawInstancingCode.AppendLine( "#endif" );
            lDrawInstancingCode.AppendLine( "#ifdef _GEOMETRY_" );
            lDrawInstancingCode.AppendLine( "layout(triangles) in;" );
            lDrawInstancingCode.AppendLine( "layout(triangle_strip, max_vertices = 3) out;" );
            lDrawInstancingCode.AppendLine( "in vec4 pos[];" );
            lDrawInstancingCode.AppendLine( "flat in int instance[];" );
            lDrawInstancingCode.AppendLine( "void main() {" );
            lDrawInstancingCode.AppendLine( "    gl_Layer = instance[0];" );
            lDrawInstancingCode.AppendLine( "    gl_Position = gl_in[0].gl_Position;" );
            lDrawInstancingCode.AppendLine( "    EmitVertex();" );
            lDrawInstancingCode.AppendLine( "    gl_Position = gl_in[1].gl_Position;" );
            lDrawInstancingCode.AppendLine( "    EmitVertex();" );
            lDrawInstancingCode.AppendLine( "    gl_Position = gl_in[2].gl_Position;" );
            lDrawInstancingCode.AppendLine( "    EmitVertex();" );
            lDrawInstancingCode.AppendLine( "    EndPrimitive();" );
            lDrawInstancingCode.AppendLine( "}" );
            lDrawInstancingCode.AppendLine( "#endif" );
            lDrawInstancingCode.AppendLine( "#ifdef _FRAGMENT_" );
            lDrawInstancingCode.AppendLine( "layout(location=0) out ivec4 color;" );
            lDrawInstancingCode.AppendLine( "void main() { color = ivec4(1, 2, 3, 4); }" );
            lDrawInstancingCode.AppendLine( "#endif" );
            cDRAW_INSTANCING = lDrawInstancingCode.ToString();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestFrameBuffer"/> class.
        /// </summary>
        public TestFrameBuffer()
        {
            TestSuite.Instance.AddTest( "TestTexture1DAttachment", this.TestTexture1DAttachment );
            TestSuite.Instance.AddTest( "TestTexture1DArrayAttachmentOneLayer", this.TestTexture1DArrayAttachmentOneLayer );
            TestSuite.Instance.AddTest( "TestTexture1DArrayAttachmentAllLayers", this.TestTexture1DArrayAttachmentAllLayers );
            TestSuite.Instance.AddTest( "TestTexture2DAttachment", this.TestTexture2DAttachment );
            TestSuite.Instance.AddTest( "TestTexture2DArrayAttachmentOneLayer", this.TestTexture2DArrayAttachmentOneLayer );
            TestSuite.Instance.AddTest( "TestTexture2DArrayAttachmentAllLayers", this.TestTexture2DArrayAttachmentAllLayers );
            TestSuite.Instance.AddTest( "TestTextureRectangleAttachment", this.TestTextureRectangleAttachment );
            TestSuite.Instance.AddTest( "TestTexture2DMultisampleAttachment", this.TestTexture2DMultisampleAttachment );
            TestSuite.Instance.AddTest( "TestTexture2DMultisampleArrayAttachmentOneLayer", this.TestTexture2DMultisampleArrayAttachmentOneLayer );
            TestSuite.Instance.AddTest( "TestTexture2DMultisampleArrayAttachmentAllLayers", this.TestTexture2DMultisampleArrayAttachmentAllLayers );
            TestSuite.Instance.AddTest( "TestTexture3DAttachmentOneLayer", this.TestTexture3DAttachmentOneLayer );
            TestSuite.Instance.AddTest( "TestTexture3DAttachmentAllLayers", this.TestTexture3DAttachmentAllLayers );
            TestSuite.Instance.AddTest( "TestDraw", this.TestDraw );
            TestSuite.Instance.AddTest( "TestDrawInstancing", this.TestDrawInstancing );
            TestSuite.Instance.AddTest( "TestDrawPartDirect", this.TestDrawPartDirect );
            TestSuite.Instance.AddTest( "TestDrawPartInstancingDirect", this.TestDrawPartInstancingDirect );
            TestSuite.Instance.AddTest( "TestDrawPartIndices", this.TestDrawPartIndices );
            TestSuite.Instance.AddTest( "TestDrawPartInstancingIndices", this.TestDrawPartInstancingIndices );
            TestSuite.Instance.AddTest( "TestDrawPartIndicesWithBase", this.TestDrawPartIndicesWithBase );
            TestSuite.Instance.AddTest( "TestDrawPartInstancingIndicesWithBase", this.TestDrawPartInstancingIndicesWithBase );
            TestSuite.Instance.AddTest( "TestMultiDrawInstancingDirect", this.TestMultiDrawInstancingDirect );
            TestSuite.Instance.AddTest( "TestMultiDrawInstancingIndices", this.TestMultiDrawInstancingIndices );
            TestSuite.Instance.AddTest( "TestMultiDrawInstancingIndicesWithBase", this.TestMultiDrawInstancingIndicesWithBase );
            TestSuite.Instance.AddTest( "TestDrawIndirectInstancingDirect", this.TestDrawIndirectInstancingDirect, 4 );
            TestSuite.Instance.AddTest( "TestDrawIndirectInstancingIndices", this.TestDrawIndirectInstancingIndices, 4 );
            TestSuite.Instance.AddTest( "TestDrawIndirectInstancingIndicesWithBase", this.TestDrawIndirectInstancingIndicesWithBase, 4 );
            TestSuite.Instance.AddTest( "TestPrimitiveRestart", this.TestPrimitiveRestart );
            TestSuite.Instance.AddTest( "TestCPUMeshModificationDirect", this.TestCPUMeshModificationDirect );
            TestSuite.Instance.AddTest( "TestCPUMeshModificationIndices", this.TestCPUMeshModificationIndices );
            TestSuite.Instance.AddTest( "TestGPUMeshModificationDirect", this.TestGPUMeshModificationDirect );
            TestSuite.Instance.AddTest( "TestGPUMeshModificationIndices", this.TestGPUMeshModificationIndices );
        }

        #endregion Constructor

        #region Methods Tests

        /// <summary>
        /// Tests a texture1D attachment.
        /// </summary>
        [TestMethod]
        public void TestTexture1DAttachment()
        {
            LogManager.Instance.Log( "Running TestTexture1DAttachment...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture1D lTextureBuffer = new Texture1D( 32, 
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 32, 1 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 32 ];
            int[] lFrameData   = new int[ 4 * 32 ];

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture1D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     32, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ 0 ] == 1 && 
                           lTextureData[ 1 ] == 2 &&
                           lTextureData[ 2 ] == 3 &&
                           lTextureData[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture1D array attachment on one layer.
        /// </summary>
        [TestMethod]
        public void TestTexture1DArrayAttachmentOneLayer()
        {
            LogManager.Instance.Log( "Running TestTexture1DArrayAttachmentOneLayer...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture1DArray lTextureBuffer = new Texture1DArray( 32,
                                                                8,
                                                                PixelInternalFormat.Rgba8i, 
                                                                PixelFormat.RgbaInteger, 
                                                                PixelType.Int, 
                                                                lTextureParameters, 
                                                                new BufferLayoutParameters(), 
                                                                new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, 3 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 32, 1 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 32 * 8 ];
            int[] lFrameData   = new int[ 4 * 32 ];
            int lLayer = 4 * 32 * 3;

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture1DArray>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                     PixelFormat.RgbaInteger, 
                                                                                     PixelType.Int, 
                                                                                     lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     32, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ]     == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture1D array attachment on all layers.
        /// </summary>
        [TestMethod]
        public void TestTexture1DArrayAttachmentAllLayers()
        {
            LogManager.Instance.Log( "Running TestTexture1DArrayAttachmentAllLayers...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture1DArray lTextureBuffer = new Texture1DArray( 32,
                                                                8,
                                                                PixelInternalFormat.Rgba8i, 
                                                                PixelFormat.RgbaInteger, 
                                                                PixelType.Int, 
                                                                lTextureParameters, 
                                                                new BufferLayoutParameters(), 
                                                                new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 ); // -1 means all layers.
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 32, 1 ) );

            StringBuilder lSourceCode = new StringBuilder();
            lSourceCode.AppendLine( cLAYER_INSTANCING );
            lSourceCode.AppendLine( cFRAGMENT_SHADER );
            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, lSourceCode.ToString() ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 32 * 8 ];
            int[] lFrameData   = new int[ 4 * 32 ];
            int lLayer = 4 * 32 * 3;

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture1DArray>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                     PixelFormat.RgbaInteger, 
                                                                                     PixelType.Int, 
                                                                                     lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     32, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ]     == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture2D attachment.
        /// </summary>
        [TestMethod]
        public void TestTexture2DAttachment()
        {
            LogManager.Instance.Log( "Running TestTexture2DAttachment...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 32, 
                                                      32,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 32, 32 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 32 * 32 ];
            int[] lFrameData   = new int[ 4 * 32 * 32 ];

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     32, 32, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ 0 ] == 1 && 
                           lTextureData[ 1 ] == 2 &&
                           lTextureData[ 2 ] == 3 &&
                           lTextureData[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture2D array attachment on one layer.
        /// </summary>
        [TestMethod]
        public void TestTexture2DArrayAttachmentOneLayer()
        {
            LogManager.Instance.Log( "Running TestTexture2DArrayAttachmentOneLayer...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2DArray lTextureBuffer = new Texture2DArray( 8,
                                                                8,
                                                                8,
                                                                PixelInternalFormat.Rgba8i, 
                                                                PixelFormat.RgbaInteger, 
                                                                PixelType.Int, 
                                                                lTextureParameters, 
                                                                new BufferLayoutParameters(), 
                                                                new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, 3 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2DArray>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                     PixelFormat.RgbaInteger, 
                                                                                     PixelType.Int, 
                                                                                     lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ]     == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture2D array attachment on all layers.
        /// </summary>
        [TestMethod]
        public void TestTexture2DArrayAttachmentAllLayers()
        {
            LogManager.Instance.Log( "Running TestTexture2DArrayAttachmentAllLayers...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2DArray lTextureBuffer = new Texture2DArray( 8,
                                                                8,
                                                                8,
                                                                PixelInternalFormat.Rgba8i, 
                                                                PixelFormat.RgbaInteger, 
                                                                PixelType.Int, 
                                                                lTextureParameters, 
                                                                new BufferLayoutParameters(), 
                                                                new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 ); // -1 means all layers.
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            StringBuilder lSourceCode = new StringBuilder();
            lSourceCode.AppendLine( cLAYER_INSTANCING );
            lSourceCode.AppendLine( cFRAGMENT_SHADER );
            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, lSourceCode.ToString() ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2DArray>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                     PixelFormat.RgbaInteger, 
                                                                                     PixelType.Int, 
                                                                                     lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ]     == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a textureRectangle attachment.
        /// </summary>
        [TestMethod]
        public void TestTextureRectangleAttachment()
        {
            LogManager.Instance.Log( "Running TestTextureRectangleAttachment...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            TextureRectangle lTextureBuffer = new TextureRectangle( 32, 
                                                                    32,
                                                                    PixelInternalFormat.Rgba8i, 
                                                                    PixelFormat.RgbaInteger, 
                                                                    PixelType.Int, 
                                                                    lTextureParameters, 
                                                                    new BufferLayoutParameters(), 
                                                                    new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 32, 32 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 32 * 32 ];
            int[] lFrameData   = new int[ 4 * 32 * 32 ];

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<TextureRectangle>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                       PixelFormat.RgbaInteger, 
                                                                                       PixelType.Int, 
                                                                                       lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     32, 32, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ 0 ] == 1 && 
                           lTextureData[ 1 ] == 2 &&
                           lTextureData[ 2 ] == 3 &&
                           lTextureData[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture2DMultisample attachment.
        /// </summary>
        [TestMethod]
        public void TestTexture2DMultisampleAttachment()
        {
            LogManager.Instance.Log( "Running TestTexture2DMultisampleAttachment...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            Texture2DMultisample lTextureBuffer = new Texture2DMultisample( 32, 
                                                                            32,
                                                                            4,
                                                                            PixelInternalFormat.Rgba32f,
                                                                            true );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 32, 32 ) );
            lFrameBuffer.Parameters.SamplingDescription.UseMultiSampling = true;

            PipelinePass lPass = new PipelinePass(
                                 new PipelineDescription( 330, cFRAGMENT_SHADER_FLOAT ) );

            lFrameBuffer.DrawQuad( lPass );

            lFrameBuffer.Parameters.SamplingDescription.UseMultiSampling = false;

            StringBuilder lSourceCode = new StringBuilder();
            lSourceCode.AppendLine( "uniform sampler2DMS sampler;" );
            lSourceCode.AppendLine( "layout(location=0) out vec4 color;" );
            lSourceCode.AppendLine( "void main() { color = texelFetch(sampler, ivec2(floor(gl_FragCoord.xy)), 0); }" );
            lPass = new PipelinePass( 
                    new PipelineDescription( 
                    new PipelineStage( 330, lSourceCode.ToString(), StageType.FRAGMENT ) ) );

            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "sampler" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lFrameBuffer.GetAttachment<Texture2DMultisample>( BufferId.COLOR0 );

            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lNewTextureBuffer = new Texture2D( 32, 
                                                         32,
                                                         PixelInternalFormat.Rgba32f, 
                                                         PixelFormat.Rgba, 
                                                         PixelType.Float, 
                                                         lTextureParameters, 
                                                         new BufferLayoutParameters(), 
                                                         new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lNewTextureBuffer, 0 );

            lFrameBuffer.DrawQuad( lPass );

            float[] lTextureData = new float[ 4 * 32 * 32 ];
            float[] lFrameData   = new float[ 4 * 32 * 32 ];

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.Rgba, 
                                                                                PixelType.Float, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     32, 32, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1.0f && 
                           lFrameData[ 1 ] == 2.0f &&
                           lFrameData[ 2 ] == 3.0f &&
                           lFrameData[ 3 ] == 4.0f &&
                           lTextureData[ 0 ] == 1.0f && 
                           lTextureData[ 1 ] == 2.0f &&
                           lTextureData[ 2 ] == 3.0f &&
                           lTextureData[ 3 ] == 4.0f );
        }
        
        /// <summary>
        /// Tests a texture2DMultisample array attachment on one layer.
        /// </summary>
        [TestMethod]
        public void TestTexture2DMultisampleArrayAttachmentOneLayer()
        {
            LogManager.Instance.Log( "Running TestTexture2DMultisampleArrayAttachmentOneLayer...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            Texture2DMultisampleArray lTextureBuffer = new Texture2DMultisampleArray( 8,
                                                                                      8,
                                                                                      8,
                                                                                      4,
                                                                                      PixelInternalFormat.Rgba32f,
                                                                                      true );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, 3 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );
            lFrameBuffer.Parameters.SamplingDescription.UseMultiSampling = true;

            PipelinePass lPass = new PipelinePass(
                                 new PipelineDescription( 330, cFRAGMENT_SHADER_FLOAT ) );

            lFrameBuffer.DrawQuad( lPass );

            lFrameBuffer.Parameters.SamplingDescription.UseMultiSampling = false;

            StringBuilder lSourceCode = new StringBuilder();
            lSourceCode.AppendLine( "uniform sampler2DMSArray sampler;" );
            lSourceCode.AppendLine( "layout(location=0) out vec4 color;" );
            lSourceCode.AppendLine( "void main() { color = texelFetch(sampler, ivec3(floor(gl_FragCoord.xy), 3), 0); }" );
            lPass = new PipelinePass( 
                    new PipelineDescription( 
                    new PipelineStage( 330, lSourceCode.ToString(), StageType.FRAGMENT ) ) );

            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "sampler" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lFrameBuffer.GetAttachment<Texture2DMultisampleArray>( BufferId.COLOR0 );

            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lNewTextureBuffer = new Texture2D( 8, 
                                                         8,
                                                         PixelInternalFormat.Rgba32f, 
                                                         PixelFormat.Rgba, 
                                                         PixelType.Float, 
                                                         lTextureParameters, 
                                                         new BufferLayoutParameters(), 
                                                         new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lNewTextureBuffer, 0 );

            lFrameBuffer.DrawQuad( lPass );

            float[] lTextureData = new float[ 4 * 8 * 8 ];
            float[] lFrameData   = new float[ 4 * 8 * 8 ];

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.Rgba, 
                                                                                PixelType.Float, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1.0f && 
                           lFrameData[ 1 ] == 2.0f &&
                           lFrameData[ 2 ] == 3.0f &&
                           lFrameData[ 3 ] == 4.0f &&
                           lTextureData[ 0 ] == 1.0f && 
                           lTextureData[ 1 ] == 2.0f &&
                           lTextureData[ 2 ] == 3.0f &&
                           lTextureData[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Tests a texture2DMultisample array attachment on all layers.
        /// </summary>
        [TestMethod]
        public void TestTexture2DMultisampleArrayAttachmentAllLayers()
        {
            LogManager.Instance.Log( "Running TestTexture2DMultisampleArrayAttachmentAllLayers...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            Texture2DMultisampleArray lTextureBuffer = new Texture2DMultisampleArray( 8,
                                                                                      8,
                                                                                      8,
                                                                                      4,
                                                                                      PixelInternalFormat.Rgba32f,
                                                                                      true );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );
            lFrameBuffer.Parameters.SamplingDescription.UseMultiSampling = true;

            StringBuilder lSourceCode = new StringBuilder();
            lSourceCode.AppendLine( cLAYER_INSTANCING );
            lSourceCode.AppendLine( cFRAGMENT_SHADER_FLOAT );
            PipelinePass lPass = new PipelinePass(
                                 new PipelineDescription( 330, lSourceCode.ToString() ) );

            lFrameBuffer.DrawQuad( lPass );

            lFrameBuffer.Parameters.SamplingDescription.UseMultiSampling = false;

            lSourceCode = new StringBuilder();
            lSourceCode.AppendLine( "uniform sampler2DMSArray sampler;" );
            lSourceCode.AppendLine( "layout(location=0) out vec4 color;" );
            lSourceCode.AppendLine( "void main() { color = texelFetch(sampler, ivec3(floor(gl_FragCoord.xy), 3), 0); }" );
            lPass = new PipelinePass( 
                    new PipelineDescription( 
                    new PipelineStage( 330, lSourceCode.ToString(), StageType.FRAGMENT ) ) );

            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "sampler" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lFrameBuffer.GetAttachment<Texture2DMultisampleArray>( BufferId.COLOR0 );

            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lNewTextureBuffer = new Texture2D( 8, 
                                                         8,
                                                         PixelInternalFormat.Rgba32f, 
                                                         PixelFormat.Rgba, 
                                                         PixelType.Float, 
                                                         lTextureParameters, 
                                                         new BufferLayoutParameters(), 
                                                         new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lNewTextureBuffer, 0 );

            lFrameBuffer.DrawQuad( lPass );

            float[] lTextureData = new float[ 4 * 8 * 8 ];
            float[] lFrameData   = new float[ 4 * 8 * 8 ];

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.Rgba, 
                                                                                PixelType.Float, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1.0f && 
                           lFrameData[ 1 ] == 2.0f &&
                           lFrameData[ 2 ] == 3.0f &&
                           lFrameData[ 3 ] == 4.0f &&
                           lTextureData[ 0 ] == 1.0f && 
                           lTextureData[ 1 ] == 2.0f &&
                           lTextureData[ 2 ] == 3.0f &&
                           lTextureData[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Tests a texture3D attachment on one layer.
        /// </summary>
        [TestMethod]
        public void TestTexture3DAttachmentOneLayer()
        {
            LogManager.Instance.Log( "Running TestTexture3DAttachmentOneLayer...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8,
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, 3 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ]     == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture3D attachment on all layers.
        /// </summary>
        [TestMethod]
        public void TestTexture3DAttachmentAllLayers()
        {
            LogManager.Instance.Log( "Running TestTexture3DAttachmentAllLayers...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8,
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            StringBuilder lSourceCode = new StringBuilder();
            lSourceCode.AppendLine( cLAYER_INSTANCING );
            lSourceCode.AppendLine( cFRAGMENT_SHADER );
            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, lSourceCode.ToString() ) );

            lFrameBuffer.DrawQuad( lPass );

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ]     == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a draw call.
        /// </summary>
        [TestMethod]
        public void TestDraw()
        {
            LogManager.Instance.Log( "Running TestDraw...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.TriangleStrip, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );

            int[] lTextureData = new int[ 4 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ 0 ] == 1 && 
                           lTextureData[ 1 ] == 2 &&
                           lTextureData[ 2 ] == 3 &&
                           lTextureData[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a draw instancing call.
        /// </summary>
        [TestMethod]
        public void TestDrawInstancing()
        {
            LogManager.Instance.Log( "Running TestDrawInstancing...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8, 
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cDRAW_INSTANCING ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.TriangleStrip, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad, 8 );

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ] == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a draw part direct call.
        /// </summary>
        [TestMethod]
        public void TestDrawPartDirect()
        {
            LogManager.Instance.Log( "Running TestDrawPartDirect...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lMeshBuffers, PrimitiveType.Triangles, 0, 3 );

            int[] lTextureData = new int[ 4 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ 0 ] == 1 && 
                           lTextureData[ 1 ] == 2 &&
                           lTextureData[ 2 ] == 3 &&
                           lTextureData[ 3 ] == 4 &&
                           lFrameData[ lOffset ]     == 0 && 
                           lFrameData[ lOffset + 1 ] == 0 &&
                           lFrameData[ lOffset + 2 ] == 0 &&
                           lFrameData[ lOffset + 3 ] == 0 &&
                           lTextureData[ lOffset ]     == 0 && 
                           lTextureData[ lOffset + 1 ] == 0 &&
                           lTextureData[ lOffset + 2 ] == 0 &&
                           lTextureData[ lOffset + 3 ] == 0 );
        }

        /// <summary>
        /// Tests a draw part instancing direct call.
        /// </summary>
        [TestMethod]
        public void TestDrawPartInstancingDirect()
        {
            LogManager.Instance.Log( "Running TestDrawPartInstancingDirect...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8, 
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cDRAW_INSTANCING ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lMeshBuffers, PrimitiveType.Triangles, 0, 3, 8 );

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ]     == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 &&
                           lFrameData[ lOffset ]     == 0 && 
                           lFrameData[ lOffset + 1 ] == 0 &&
                           lFrameData[ lOffset + 2 ] == 0 &&
                           lFrameData[ lOffset + 3 ] == 0 &&
                           lTextureData[ lLayer + lOffset ]     == 0 && 
                           lTextureData[ lLayer + lOffset + 1 ] == 0 &&
                           lTextureData[ lLayer + lOffset + 2 ] == 0 &&
                           lTextureData[ lLayer + lOffset + 3 ] == 0 );
        }

        /// <summary>
        /// Tests a draw part indices call.
        /// </summary>
        [TestMethod]
        public void TestDrawPartIndices()
        {
            LogManager.Instance.Log( "Running TestDrawPartIndices...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lMeshBuffers, PrimitiveType.Triangles, 3, 3 );

            int[] lTextureData = new int[ 4 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 0 && 
                           lFrameData[ 1 ] == 0 &&
                           lFrameData[ 2 ] == 0 &&
                           lFrameData[ 3 ] == 0 &&
                           lTextureData[ 0 ] == 0 && 
                           lTextureData[ 1 ] == 0 &&
                           lTextureData[ 2 ] == 0 &&
                           lTextureData[ 3 ] == 0 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lTextureData[ lOffset ]     == 1 && 
                           lTextureData[ lOffset + 1 ] == 2 &&
                           lTextureData[ lOffset + 2 ] == 3 &&
                           lTextureData[ lOffset + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a draw part instancing indices call.
        /// </summary>
        [TestMethod]
        public void TestDrawPartInstancingIndices()
        {
            LogManager.Instance.Log( "Running TestDrawPartInstancingIndices...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8, 
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cDRAW_INSTANCING ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lMeshBuffers, PrimitiveType.Triangles, 3, 3, 8 );

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 0 && 
                           lFrameData[ 1 ] == 0 &&
                           lFrameData[ 2 ] == 0 &&
                           lFrameData[ 3 ] == 0 &&
                           lTextureData[ lLayer ]     == 0 && 
                           lTextureData[ lLayer + 1 ] == 0 &&
                           lTextureData[ lLayer + 2 ] == 0 &&
                           lTextureData[ lLayer + 3 ] == 0 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lTextureData[ lLayer + lOffset ]     == 1 && 
                           lTextureData[ lLayer + lOffset + 1 ] == 2 &&
                           lTextureData[ lLayer + lOffset + 2 ] == 3 &&
                           lTextureData[ lLayer + lOffset + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a draw part indices with a base call.
        /// </summary>
        [TestMethod]
        public void TestDrawPartIndicesWithBase()
        {
            LogManager.Instance.Log( "Running TestDrawPartIndicesWithBase...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cDRAW_INSTANCING ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lMeshBuffers, PrimitiveType.Triangles, 0, 3, 1, 1 );

            int[] lTextureData = new int[ 4 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 0 && 
                           lFrameData[ 1 ] == 0 &&
                           lFrameData[ 2 ] == 0 &&
                           lFrameData[ 3 ] == 0 &&
                           lTextureData[ 0 ] == 0 && 
                           lTextureData[ 1 ] == 0 &&
                           lTextureData[ 2 ] == 0 &&
                           lTextureData[ 3 ] == 0 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lTextureData[ lOffset ]     == 1 && 
                           lTextureData[ lOffset + 1 ] == 2 &&
                           lTextureData[ lOffset + 2 ] == 3 &&
                           lTextureData[ lOffset + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a draw part instancing indices with a base call.
        /// </summary>
        [TestMethod]
        public void TestDrawPartInstancingIndicesWithBase()
        {
            LogManager.Instance.Log( "Running TestDrawPartInstancingIndicesWithBase...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8, 
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cDRAW_INSTANCING ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lMeshBuffers, PrimitiveType.Triangles, 0, 3, 8, 1 );

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 0 && 
                           lFrameData[ 1 ] == 0 &&
                           lFrameData[ 2 ] == 0 &&
                           lFrameData[ 3 ] == 0 &&
                           lTextureData[ lLayer ]     == 0 && 
                           lTextureData[ lLayer + 1 ] == 0 &&
                           lTextureData[ lLayer + 2 ] == 0 &&
                           lTextureData[ lLayer + 3 ] == 0 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lTextureData[ lLayer + lOffset ]     == 1 && 
                           lTextureData[ lLayer + lOffset + 1 ] == 2 &&
                           lTextureData[ lLayer + lOffset + 2 ] == 3 &&
                           lTextureData[ lLayer + lOffset + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a multidraw instancing direct call.
        /// </summary>
        [TestMethod]
        public void TestMultiDrawInstancingDirect()
        {
            LogManager.Instance.Log( "Running TestMultiDrawInstancingDirect...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );

            int[] lFirsts =
            {
                0,
                18
            };
            int[] lCounts =
            {
                6,
                6
            };

            lFrameBuffer.MultiDraw( lPass, lMeshBuffers, PrimitiveType.Triangles, lFirsts, lCounts, 2 );

            int[] lTextureData = new int[ 4 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lOffset = 4 * (8 * 8 - 1);
            int lA = 4 * (8 - 1);
            int lB = 4 * (7 * 8);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ 0 ] == 1 && 
                           lTextureData[ 1 ] == 2 &&
                           lTextureData[ 2 ] == 3 &&
                           lTextureData[ 3 ] == 4 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lTextureData[ lOffset ]     == 1 && 
                           lTextureData[ lOffset + 1 ] == 2 &&
                           lTextureData[ lOffset + 2 ] == 3 &&
                           lTextureData[ lOffset + 3 ] == 4 &&
                           lFrameData[ lA ]     == 0 && 
                           lFrameData[ lA + 1 ] == 0 &&
                           lFrameData[ lA + 2 ] == 0 &&
                           lFrameData[ lA + 3 ] == 0 &&
                           lTextureData[ lA ]     == 0 && 
                           lTextureData[ lA + 1 ] == 0 &&
                           lTextureData[ lA + 2 ] == 0 &&
                           lTextureData[ lA + 3 ] == 0 &&
                           lFrameData[ lB ]     == 0 && 
                           lFrameData[ lB + 1 ] == 0 &&
                           lFrameData[ lB + 2 ] == 0 &&
                           lFrameData[ lB + 3 ] == 0 &&
                           lTextureData[ lB ]     == 0 && 
                           lTextureData[ lB + 1 ] == 0 &&
                           lTextureData[ lB + 2 ] == 0 &&
                           lTextureData[ lB + 3 ] == 0 );
        }

        /// <summary>
        /// Tests a multidraw instancing indices call.
        /// </summary>
        [TestMethod]
        public void TestMultiDrawInstancingIndices()
        {
            LogManager.Instance.Log( "Running TestMultiDrawInstancingIndices...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ));
            lQuad.AddVertex( new Vector4(  0, -1, 0, 1 ));
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ));
            lQuad.AddVertex( new Vector4( -1,  0, 0, 1 ));
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ));
            lQuad.AddVertex( new Vector4(  1,  0, 0, 1 ));
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ));
            lQuad.AddVertex( new Vector4(  0,  1, 0, 1 ));
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ));
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );
            lQuad.AddIndex( 3 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 5 );
            lQuad.AddIndex( 3 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 6 );
            lQuad.AddIndex( 6 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 7 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 5 );
            lQuad.AddIndex( 7 );
            lQuad.AddIndex( 7 );
            lQuad.AddIndex( 5 );
            lQuad.AddIndex( 8 );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );

            int[] lFirsts =
            {
                0,
                18
            };
            int[] lCounts =
            {
                6,
                6
            };

            lFrameBuffer.MultiDraw( lPass, lMeshBuffers, PrimitiveType.Triangles, lFirsts, lCounts, 2 );

            int[] lTextureData = new int[ 4 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lOffset = 4 * (8 * 8 - 1);
            int lA = 4 * (8 - 1);
            int lB = 4 * (7 * 8);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ 0 ] == 1 && 
                           lTextureData[ 1 ] == 2 &&
                           lTextureData[ 2 ] == 3 &&
                           lTextureData[ 3 ] == 4 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lTextureData[ lOffset ]     == 1 && 
                           lTextureData[ lOffset + 1 ] == 2 &&
                           lTextureData[ lOffset + 2 ] == 3 &&
                           lTextureData[ lOffset + 3 ] == 4 &&
                           lFrameData[ lA ]     == 0 && 
                           lFrameData[ lA + 1 ] == 0 &&
                           lFrameData[ lA + 2 ] == 0 &&
                           lFrameData[ lA + 3 ] == 0 &&
                           lTextureData[ lA ]     == 0 && 
                           lTextureData[ lA + 1 ] == 0 &&
                           lTextureData[ lA + 2 ] == 0 &&
                           lTextureData[ lA + 3 ] == 0 &&
                           lFrameData[ lB ]     == 0 && 
                           lFrameData[ lB + 1 ] == 0 &&
                           lFrameData[ lB + 2 ] == 0 &&
                           lFrameData[ lB + 3 ] == 0 &&
                           lTextureData[ lB ]     == 0 && 
                           lTextureData[ lB + 1 ] == 0 &&
                           lTextureData[ lB + 2 ] == 0 &&
                           lTextureData[ lB + 3 ] == 0 );
        }

        /// <summary>
        /// Tests a multidraw instancing indices with a base call.
        /// </summary>
        [TestMethod]
        public void TestMultiDrawInstancingIndicesWithBase()
        {
            LogManager.Instance.Log( "Running TestMultiDrawInstancingIndicesWithBase...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ));
            lQuad.AddVertex( new Vector4(  0, -1, 0, 1 ));
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ));
            lQuad.AddVertex( new Vector4( -1,  0, 0, 1 ));
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ));
            lQuad.AddVertex( new Vector4(  1,  0, 0, 1 ));
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ));
            lQuad.AddVertex( new Vector4(  0,  1, 0, 1 ));
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ));
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );
            lQuad.AddIndex( 3 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 5 );
            lQuad.AddIndex( 3 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 6 );
            lQuad.AddIndex( 6 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 7 );
            lQuad.AddIndex( 4 );
            lQuad.AddIndex( 5 );
            lQuad.AddIndex( 7 );
            lQuad.AddIndex( 7 );
            lQuad.AddIndex( 5 );
            lQuad.AddIndex( 8 );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );

            int[] lFirsts =
            {
                0,
                18
            };
            int[] lCounts =
            {
                6,
                6
            };
            int[] lBases =
            {
                1,
                -1
            };

            lFrameBuffer.MultiDraw( lPass, lMeshBuffers, PrimitiveType.Triangles, lFirsts, lCounts, 2, lBases );

            int[] lTextureData = new int[ 4 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lOffset = 4 * (8 * 8 - 1);
            int lA = 4 * (8 - 1);
            int lB = 4 * (7 * 8);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture2D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 0 && 
                           lFrameData[ 1 ] == 0 &&
                           lFrameData[ 2 ] == 0 &&
                           lFrameData[ 3 ] == 0 &&
                           lTextureData[ 0 ] == 0 && 
                           lTextureData[ 1 ] == 0 &&
                           lTextureData[ 2 ] == 0 &&
                           lTextureData[ 3 ] == 0 &&
                           lFrameData[ lOffset ]     == 0 && 
                           lFrameData[ lOffset + 1 ] == 0 &&
                           lFrameData[ lOffset + 2 ] == 0 &&
                           lFrameData[ lOffset + 3 ] == 0 &&
                           lTextureData[ lOffset ]     == 0 && 
                           lTextureData[ lOffset + 1 ] == 0 &&
                           lTextureData[ lOffset + 2 ] == 0 &&
                           lTextureData[ lOffset + 3 ] == 0 &&
                           lFrameData[ lA ]     == 1 && 
                           lFrameData[ lA + 1 ] == 2 &&
                           lFrameData[ lA + 2 ] == 3 &&
                           lFrameData[ lA + 3 ] == 4 &&
                           lTextureData[ lA ]     == 1 && 
                           lTextureData[ lA + 1 ] == 2 &&
                           lTextureData[ lA + 2 ] == 3 &&
                           lTextureData[ lA + 3 ] == 4 &&
                           lFrameData[ lB ]     == 1 && 
                           lFrameData[ lB + 1 ] == 2 &&
                           lFrameData[ lB + 2 ] == 3 &&
                           lFrameData[ lB + 3 ] == 4 &&
                           lTextureData[ lB ]     == 1 && 
                           lTextureData[ lB + 1 ] == 2 &&
                           lTextureData[ lB + 2 ] == 3 &&
                           lTextureData[ lB + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a draw indirect instancing direct call.
        /// </summary>
        [TestMethod]
        public void TestDrawIndirectInstancingDirect()
        {
            LogManager.Instance.Log( "Running TestDrawIndirectInstancingDirect...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8, 
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cDRAW_INSTANCING ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );

            int[] lData =
            {
                3,
                8,
                0,
                0
            };

            GCHandle lDataPtr = GCHandle.Alloc( lData, GCHandleType.Pinned );

            lFrameBuffer.DrawIndirect( lPass, lMeshBuffers, PrimitiveType.Triangles, new CPUBuffer( lDataPtr.AddrOfPinnedObject() ) );

            lDataPtr.Free();

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lTextureData[ lLayer ]     == 1 && 
                           lTextureData[ lLayer + 1 ] == 2 &&
                           lTextureData[ lLayer + 2 ] == 3 &&
                           lTextureData[ lLayer + 3 ] == 4 &&
                           lFrameData[ lOffset ]     == 0 && 
                           lFrameData[ lOffset + 1 ] == 0 &&
                           lFrameData[ lOffset + 2 ] == 0 &&
                           lFrameData[ lOffset + 3 ] == 0 &&
                           lTextureData[ lLayer + lOffset ]     == 0 && 
                           lTextureData[ lLayer + lOffset + 1 ] == 0 &&
                           lTextureData[ lLayer + lOffset + 2 ] == 0 &&
                           lTextureData[ lLayer + lOffset + 3 ] == 0 );
        }

        /// <summary>
        /// Tests a draw indirect instancing indices call.
        /// </summary>
        [TestMethod]
        public void TestDrawIndirectInstancingIndices()
        {
            LogManager.Instance.Log( "Running TestDrawIndirectInstancingIndices...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8, 
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cDRAW_INSTANCING ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );

            int[] lData =
            {
                3,
                8,
                3,
                0,
                0
            };

            GCHandle lDataPtr = GCHandle.Alloc( lData, GCHandleType.Pinned );

            lFrameBuffer.DrawIndirect( lPass, lMeshBuffers, PrimitiveType.Triangles, new CPUBuffer( lDataPtr.AddrOfPinnedObject() ) );

            lDataPtr.Free();

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 0 && 
                           lFrameData[ 1 ] == 0 &&
                           lFrameData[ 2 ] == 0 &&
                           lFrameData[ 3 ] == 0 &&
                           lTextureData[ lLayer ]     == 0 && 
                           lTextureData[ lLayer + 1 ] == 0 &&
                           lTextureData[ lLayer + 2 ] == 0 &&
                           lTextureData[ lLayer + 3 ] == 0 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lTextureData[ lLayer + lOffset ]     == 1 && 
                           lTextureData[ lLayer + lOffset + 1 ] == 2 &&
                           lTextureData[ lLayer + lOffset + 2 ] == 3 &&
                           lTextureData[ lLayer + lOffset + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a draw indirect instancing indices with a base call.
        /// </summary>
        [TestMethod]
        public void TestDrawIndirectInstancingIndicesWithBase()
        {
            LogManager.Instance.Log( "Running TestDrawIndirectInstancingIndicesWithBase...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture3D lTextureBuffer = new Texture3D( 8, 
                                                      8,
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0, -1 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cDRAW_INSTANCING ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.Triangles, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 2 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );

            MeshBuffers lMeshBuffers = lQuad.Buffers;

            lFrameBuffer.Clear( true, true, true );

            int[] lData =
            {
                3,
                8,
                0,
                1,
                0
            };

            GCHandle lDataPtr = GCHandle.Alloc( lData, GCHandleType.Pinned );

            lFrameBuffer.DrawIndirect( lPass, lMeshBuffers, PrimitiveType.Triangles, new CPUBuffer( lDataPtr.AddrOfPinnedObject() ) );

            lDataPtr.Free();

            int[] lTextureData = new int[ 4 * 8 * 8 * 8 ];
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * 8 * 8 * 3;
            int lOffset = 4 * (8 * 8 - 1);

            GCHandle lTextureDataPtr = GCHandle.Alloc( lTextureData, GCHandleType.Pinned );

            lFrameBuffer.GetAttachment<Texture3D>( BufferId.COLOR0 ).GetMipmap( 0, 
                                                                                PixelFormat.RgbaInteger, 
                                                                                PixelType.Int, 
                                                                                lTextureDataPtr.AddrOfPinnedObject() );

            lTextureDataPtr.Free();

            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 0 && 
                           lFrameData[ 1 ] == 0 &&
                           lFrameData[ 2 ] == 0 &&
                           lFrameData[ 3 ] == 0 &&
                           lTextureData[ lLayer ]     == 0 && 
                           lTextureData[ lLayer + 1 ] == 0 &&
                           lTextureData[ lLayer + 2 ] == 0 &&
                           lTextureData[ lLayer + 3 ] == 0 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lTextureData[ lLayer + lOffset ]     == 1 && 
                           lTextureData[ lLayer + lOffset + 1 ] == 2 &&
                           lTextureData[ lLayer + lOffset + 2 ] == 3 &&
                           lTextureData[ lLayer + lOffset + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a primitive restart call.
        /// </summary>
        [TestMethod]
        public void TestPrimitiveRestart()
        {
            LogManager.Instance.Log( "Running TestPrimitiveRestart...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.TriangleStrip, MeshUsage.GPU_STATIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  0, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  0,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 3 );
            lQuad.AddIndex( 255 );
            lQuad.AddIndex( 7 );
            lQuad.AddIndex( 5 );
            lQuad.AddIndex( 8 );

            lQuad.PrimitiveRestart = 255;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );
            
            int[] lFrameData   = new int[ 4 * 8 * 8 ];
            int lOffset = 4 * (8 * 8 - 1);
            int lC = 4 * (8 * 3 + 3);
            
            GCHandle lFrameDataPtr = GCHandle.Alloc( lFrameData, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lFrameDataPtr.AddrOfPinnedObject() ) );

            lFrameDataPtr.Free();

            Assert.IsTrue( lFrameData[ 0 ] == 1 && 
                           lFrameData[ 1 ] == 2 &&
                           lFrameData[ 2 ] == 3 &&
                           lFrameData[ 3 ] == 4 &&
                           lFrameData[ lOffset ]     == 1 && 
                           lFrameData[ lOffset + 1 ] == 2 &&
                           lFrameData[ lOffset + 2 ] == 3 &&
                           lFrameData[ lOffset + 3 ] == 4 &&
                           lFrameData[ lC ]     == 0 && 
                           lFrameData[ lC + 1 ] == 0 &&
                           lFrameData[ lC + 2 ] == 0 &&
                           lFrameData[ lC + 3 ] == 0 );
        }

        /// <summary>
        /// Tests a CPU based mesh modification.
        /// </summary>
        [TestMethod]
        public void TestCPUMeshModificationDirect()
        {
            LogManager.Instance.Log( "Running TestCPUMeshModificationDirect...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.TriangleStrip, MeshUsage.CPU );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );
            
            int[] lOutput1   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * (8 * 8 - 1);
            
            GCHandle lOutput1Ptr = GCHandle.Alloc( lOutput1, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutput1Ptr.AddrOfPinnedObject() ) );

            lOutput1Ptr.Free();

            // Modify the mesh.
            lQuad[ new VertexIndex( 0 ) ] = new Vector4( -1,  1, 0, 1 );
            lQuad[ new VertexIndex( 1 ) ] = new Vector4(  1, -1, 0, 1 );
            lQuad[ new VertexIndex( 2 ) ] = new Vector4(  1,  1, 0, 1 );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );

            int[] lOutput2   = new int[ 4 * 8 * 8 ];

            GCHandle lOutput2Ptr = GCHandle.Alloc( lOutput2, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutput2Ptr.AddrOfPinnedObject() ) );

            lOutput2Ptr.Free();

            Assert.IsTrue( lOutput1[ 0 ] == 1 && 
                           lOutput1[ 1 ] == 2 &&
                           lOutput1[ 2 ] == 3 &&
                           lOutput1[ 3 ] == 4 &&
                           lOutput1[ lLayer ]     == 0 && 
                           lOutput1[ lLayer + 1 ] == 0 &&
                           lOutput1[ lLayer + 2 ] == 0 &&
                           lOutput1[ lLayer + 3 ] == 0 &&
                           lOutput2[ 0 ] == 0 && 
                           lOutput2[ 1 ] == 0 &&
                           lOutput2[ 2 ] == 0 &&
                           lOutput2[ 3 ] == 0 &&
                           lOutput2[ lLayer ]     == 1 && 
                           lOutput2[ lLayer + 1 ] == 2 &&
                           lOutput2[ lLayer + 2 ] == 3 &&
                           lOutput2[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a CPU based mesh indices modification.
        /// </summary>
        [TestMethod]
        public void TestCPUMeshModificationIndices()
        {
            LogManager.Instance.Log( "Running TestCPUMeshModificationIndices...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.TriangleStrip, MeshUsage.CPU );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );
            
            int[] lOutput1   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * (8 * 8 - 1);
            
            GCHandle lOutput1Ptr = GCHandle.Alloc( lOutput1, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutput1Ptr.AddrOfPinnedObject() ) );

            lOutput1Ptr.Free();

            // Modify the mesh.
            lQuad[ new IndexIndex( 0 ) ] = 2;
            lQuad[ new IndexIndex( 1 ) ] = 1;
            lQuad[ new IndexIndex( 2 ) ] = 3;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );

            int[] lOutput2   = new int[ 4 * 8 * 8 ];

            GCHandle lOutput2Ptr = GCHandle.Alloc( lOutput2, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutput2Ptr.AddrOfPinnedObject() ) );

            lOutput2Ptr.Free();

            Assert.IsTrue( lOutput1[ 0 ] == 1 && 
                           lOutput1[ 1 ] == 2 &&
                           lOutput1[ 2 ] == 3 &&
                           lOutput1[ 3 ] == 4 &&
                           lOutput1[ lLayer ]     == 0 && 
                           lOutput1[ lLayer + 1 ] == 0 &&
                           lOutput1[ lLayer + 2 ] == 0 &&
                           lOutput1[ lLayer + 3 ] == 0 &&
                           lOutput2[ 0 ] == 0 && 
                           lOutput2[ 1 ] == 0 &&
                           lOutput2[ 2 ] == 0 &&
                           lOutput2[ 3 ] == 0 &&
                           lOutput2[ lLayer ]     == 1 && 
                           lOutput2[ lLayer + 1 ] == 2 &&
                           lOutput2[ lLayer + 2 ] == 3 &&
                           lOutput2[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a GPU based mesh modification.
        /// </summary>
        [TestMethod]
        public void TestGPUMeshModificationDirect()
        {
            LogManager.Instance.Log( "Running TestGPUMeshModificationDirect...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.TriangleStrip, MeshUsage.GPU_DYNAMIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );
            
            int[] lOutput1   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * (8 * 8 - 1);
            
            GCHandle lOutput1Ptr = GCHandle.Alloc( lOutput1, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutput1Ptr.AddrOfPinnedObject() ) );

            lOutput1Ptr.Free();

            // Modify the mesh.
            lQuad[ new VertexIndex( 0 ) ] = new Vector4( -1,  1, 0, 1 );
            lQuad[ new VertexIndex( 1 ) ] = new Vector4(  1, -1, 0, 1 );
            lQuad[ new VertexIndex( 2 ) ] = new Vector4(  1,  1, 0, 1 );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );

            int[] lOutput2   = new int[ 4 * 8 * 8 ];

            GCHandle lOutput2Ptr = GCHandle.Alloc( lOutput2, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutput2Ptr.AddrOfPinnedObject() ) );

            lOutput2Ptr.Free();

            Assert.IsTrue( lOutput1[ 0 ] == 1 && 
                           lOutput1[ 1 ] == 2 &&
                           lOutput1[ 2 ] == 3 &&
                           lOutput1[ 3 ] == 4 &&
                           lOutput1[ lLayer ]     == 0 && 
                           lOutput1[ lLayer + 1 ] == 0 &&
                           lOutput1[ lLayer + 2 ] == 0 &&
                           lOutput1[ lLayer + 3 ] == 0 &&
                           lOutput2[ 0 ] == 0 && 
                           lOutput2[ 1 ] == 0 &&
                           lOutput2[ 2 ] == 0 &&
                           lOutput2[ 3 ] == 0 &&
                           lOutput2[ lLayer ]     == 1 && 
                           lOutput2[ lLayer + 1 ] == 2 &&
                           lOutput2[ lLayer + 2 ] == 3 &&
                           lOutput2[ lLayer + 3 ] == 4 );
        }

        /// <summary>
        /// Tests a GPU based mesh indices modification.
        /// </summary>
        [TestMethod]
        public void TestGPUMeshModificationIndices()
        {
            LogManager.Instance.Log( "Running TestGPUMeshModificationIndices...", LogType.INFO );

            FrameBuffer lFrameBuffer = new FrameBuffer();
            TextureParameters lTextureParameters = new TextureParameters();
            lTextureParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTextureBuffer = new Texture2D( 8, 
                                                      8,
                                                      PixelInternalFormat.Rgba8i, 
                                                      PixelFormat.RgbaInteger, 
                                                      PixelType.Int, 
                                                      lTextureParameters, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( IntPtr.Zero ) );
            lFrameBuffer.Attach( BufferId.COLOR0, lTextureBuffer, 0 );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, 8, 8 ) );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 330, cFRAGMENT_SHADER ) );

            Mesh<Vector4, uint> lQuad = new Mesh<Vector4, uint>( PrimitiveType.TriangleStrip, MeshUsage.GPU_DYNAMIC );
            lQuad.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lQuad.AddVertex( new Vector4( -1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1, -1, 0, 1 ) );
            lQuad.AddVertex( new Vector4( -1,  1, 0, 1 ) );
            lQuad.AddVertex( new Vector4(  1,  1, 0, 1 ) );
            lQuad.AddIndex( 0 );
            lQuad.AddIndex( 1 );
            lQuad.AddIndex( 2 );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );
            
            int[] lOutput1   = new int[ 4 * 8 * 8 ];
            int lLayer = 4 * (8 * 8 - 1);
            
            GCHandle lOutput1Ptr = GCHandle.Alloc( lOutput1, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutput1Ptr.AddrOfPinnedObject() ) );

            lOutput1Ptr.Free();

            // Modify the mesh.
            lQuad[ new IndexIndex( 0 ) ] = 2;
            lQuad[ new IndexIndex( 1 ) ] = 1;
            lQuad[ new IndexIndex( 2 ) ] = 3;

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.Draw( lPass, lQuad );

            int[] lOutput2   = new int[ 4 * 8 * 8 ];

            GCHandle lOutput2Ptr = GCHandle.Alloc( lOutput2, GCHandleType.Pinned );

            lFrameBuffer.ReadPixels( 0, 0, 
                                     8, 8, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutput2Ptr.AddrOfPinnedObject() ) );

            lOutput2Ptr.Free();

            Assert.IsTrue( lOutput1[ 0 ] == 1 && 
                           lOutput1[ 1 ] == 2 &&
                           lOutput1[ 2 ] == 3 &&
                           lOutput1[ 3 ] == 4 &&
                           lOutput1[ lLayer ]     == 0 && 
                           lOutput1[ lLayer + 1 ] == 0 &&
                           lOutput1[ lLayer + 2 ] == 0 &&
                           lOutput1[ lLayer + 3 ] == 0 &&
                           lOutput2[ 0 ] == 0 && 
                           lOutput2[ 1 ] == 0 &&
                           lOutput2[ 2 ] == 0 &&
                           lOutput2[ 3 ] == 0 &&
                           lOutput2[ lLayer ]     == 1 && 
                           lOutput2[ lLayer + 1 ] == 2 &&
                           lOutput2[ lLayer + 2 ] == 3 &&
                           lOutput2[ lLayer + 3 ] == 4 );
        }

        #endregion Methods Tests
    }
}
