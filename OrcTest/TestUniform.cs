﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcTest.Helpers;
using System.Runtime.InteropServices;
using System.Text;

namespace OrcTest
{
    /// <summary>
    /// Uniform based test class definition.
    /// </summary>
    [TestClass]
    public class TestUniform
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TestUniform"/> class.
        /// </summary>
        public TestUniform()
        {
            TestSuite.Instance.AddTest( "TestUniform1f", this.TestUniform1f );
            TestSuite.Instance.AddTest( "TestUniform2f", this.TestUniform2f );
            TestSuite.Instance.AddTest( "TestUniform3f", this.TestUniform3f );
            TestSuite.Instance.AddTest( "TestUniform4f", this.TestUniform4f );
            TestSuite.Instance.AddTest( "TestUniform1d", this.TestUniform1d, 4 );
            TestSuite.Instance.AddTest( "TestUniform2d", this.TestUniform2d, 4 );
            TestSuite.Instance.AddTest( "TestUniform3d", this.TestUniform3d, 4 );
            TestSuite.Instance.AddTest( "TestUniform4d", this.TestUniform4d, 4 );
            TestSuite.Instance.AddTest( "TestUniform1i", this.TestUniform1i );
            TestSuite.Instance.AddTest( "TestUniform2i", this.TestUniform2i );
            TestSuite.Instance.AddTest( "TestUniform3i", this.TestUniform3i );
            TestSuite.Instance.AddTest( "TestUniform4i", this.TestUniform4i );
            TestSuite.Instance.AddTest( "TestUniform1b", this.TestUniform1b );
            TestSuite.Instance.AddTest( "TestUniform2b", this.TestUniform2b );
            TestSuite.Instance.AddTest( "TestUniform3b", this.TestUniform3b );
            TestSuite.Instance.AddTest( "TestUniform4b", this.TestUniform4b );
            TestSuite.Instance.AddTest( "TestUniformMatrix3f", this.TestUniformMatrix3f );
            TestSuite.Instance.AddTest( "TestUniformMatrix4f", this.TestUniformMatrix4f );
            TestSuite.Instance.AddTest( "TestStructure", this.TestStructure );
            TestSuite.Instance.AddTest( "TestBooleanArray", this.TestBooleanArray );
            TestSuite.Instance.AddTest( "TestFloatArray", this.TestFloatArray );
            TestSuite.Instance.AddTest( "TestIntArray", this.TestIntArray );
            TestSuite.Instance.AddTest( "TestStructureArray", this.TestStructureArray );
            TestSuite.Instance.AddTest( "TestSubroutine", this.TestSubroutine, 4 );
            TestSuite.Instance.AddTest( "TestSubroutineArray", this.TestSubroutineArray, 4 );
        }

        #endregion Constructor

        #region Methods Tests

        /// <summary>
        /// Checks a uniform1 float based.
        /// </summary>
        [TestMethod]
        public void TestUniform1f()
        {
            LogManager.Instance.Log( "Running TestUniform1f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform float u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            FloatUniform lUniform = lPass.GetUniform<FloatUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = 1.0f;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f );
        }

        /// <summary>
        /// Checks a uniform2 float based.
        /// </summary>
        [TestMethod]
        public void TestUniform2f()
        {
            LogManager.Instance.Log( "Running TestUniform2f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform vec2 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector2FUniform lUniform = lPass.GetUniform<Vector2FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector2F( 1.0f, 2.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f );
        }

        /// <summary>
        /// Checks a uniform3 float based.
        /// </summary>
        [TestMethod]
        public void TestUniform3f()
        {
            LogManager.Instance.Log( "Running TestUniform3f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform vec3 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector3FUniform lUniform = lPass.GetUniform<Vector3FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector3F( 1.0f, 2.0f, 3.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f );
        }

        /// <summary>
        /// Checks a uniform4 float based.
        /// </summary>
        [TestMethod]
        public void TestUniform4f()
        {
            LogManager.Instance.Log( "Running TestUniform4f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform vec4 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = u; }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector4FUniform lUniform = lPass.GetUniform<Vector4FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector4F( 1.0f, 2.0f, 3.0f, 4.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Checks a uniform1 double based.
        /// </summary>
        [TestMethod]
        public void TestUniform1d()
        {
            LogManager.Instance.Log( "Running TestUniform1d...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform double u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u), 0.0, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            DoubleUniform lUniform = lPass.GetUniform<DoubleUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = 1.0;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f );
        }

        /// <summary>
        /// Checks a uniform2 double based.
        /// </summary>
        [TestMethod]
        public void TestUniform2d()
        {
            LogManager.Instance.Log( "Running TestUniform2d...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform dvec2 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector2DUniform lUniform = lPass.GetUniform<Vector2DUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector2D( 1.0, 2.0 );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f );
        }

        /// <summary>
        /// Checks a uniform3 double based.
        /// </summary>
        [TestMethod]
        public void TestUniform3d()
        {
            LogManager.Instance.Log( "Running TestUniform3d...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform dvec3 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector3DUniform lUniform = lPass.GetUniform<Vector3DUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector3D( 1.0, 2.0, 3.0 );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f );
        }

        /// <summary>
        /// Checks a uniform4 double based.
        /// </summary>
        [TestMethod]
        public void TestUniform4d()
        {
            LogManager.Instance.Log( "Running TestUniform4d...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform dvec4 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector4DUniform lUniform = lPass.GetUniform<Vector4DUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector4D( 1.0, 2.0, 3.0, 4.0 );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Checks a uniform1 int based.
        /// </summary>
        [TestMethod]
        public void TestUniform1i()
        {
            LogManager.Instance.Log( "Running TestUniform1i...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform int u;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = ivec4(u, 0, 0, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            IntUniform lUniform = lPass.GetUniform<IntUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = 1;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 );
        }

        /// <summary>
        /// Checks a uniform2 int based.
        /// </summary>
        [TestMethod]
        public void TestUniform2i()
        {
            LogManager.Instance.Log( "Running TestUniform2i...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform ivec2 u;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = ivec4(u, 0, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector2IUniform lUniform = lPass.GetUniform<Vector2IUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector2I( 1, 2 );

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32i, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 );
        }

        /// <summary>
        /// Checks a uniform3 int based.
        /// </summary>
        [TestMethod]
        public void TestUniform3i()
        {
            LogManager.Instance.Log( "Running TestUniform3i...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform ivec3 u;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = ivec4(u, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector3IUniform lUniform = lPass.GetUniform<Vector3IUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector3I( 1, 2, 3 );

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32i, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 );
        }

        /// <summary>
        /// Checks a uniform4 int based.
        /// </summary>
        [TestMethod]
        public void TestUniform4i()
        {
            LogManager.Instance.Log( "Running TestUniform4i...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform ivec4 u;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = u; }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector4IUniform lUniform = lPass.GetUniform<Vector4IUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector4I( 1, 2, 3, 4 );

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32i, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 &&
                           lOutput[ 3 ] == 4 );
        }

        /// <summary>
        /// Checks a uniform1 boolean based.
        /// </summary>
        [TestMethod]
        public void TestUniform1b()
        {
            LogManager.Instance.Log( "Running TestUniform1b...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform bool u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u), 0, 0, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            BoolUniform lUniform = lPass.GetUniform<BoolUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = true;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f );
        }

        /// <summary>
        /// Checks a uniform2 boolean based.
        /// </summary>
        [TestMethod]
        public void TestUniform2b()
        {
            LogManager.Instance.Log( "Running TestUniform2b...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform bvec2 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector2BUniform lUniform = lPass.GetUniform<Vector2BUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new System.Tuple<bool, bool>( false, true ); // False, True.

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 0.0f &&
                           lOutput[ 1 ] == 1.0f );
        }

        /// <summary>
        /// Checks a uniform3 boolean based.
        /// </summary>
        [TestMethod]
        public void TestUniform3b()
        {
            LogManager.Instance.Log( "Running TestUniform3b...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform bvec3 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector3BUniform lUniform = lPass.GetUniform<Vector3BUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new System.Tuple<bool, bool, bool>( false, true, false );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 0.0f &&
                           lOutput[ 1 ] == 1.0f &&
                           lOutput[ 2 ] == 0.0f );
        }

        /// <summary>
        /// Checks a uniform4 boolean based.
        /// </summary>
        [TestMethod]
        public void TestUniform4b()
        {
            LogManager.Instance.Log( "Running TestUniform4b...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform bvec4 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector4BUniform lUniform = lPass.GetUniform<Vector4BUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new System.Tuple<bool, bool, bool, bool>( true, false, true, true );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 0.0f &&
                           lOutput[ 2 ] == 1.0f &&
                           lOutput[ 3 ] == 1.0f );
        }

        /// <summary>
        /// Checks a uniformMatrix3x3 float based.
        /// </summary>
        [TestMethod]
        public void TestUniformMatrix3f()
        {
            LogManager.Instance.Log( "Running TestUniformMatrix3f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform mat3 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color.rgb = u * vec3(1.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Matrix3FUniform lUniform = lPass.GetUniform<Matrix3FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Matrix = new Matrix3F( 1.0f, 2.0f, 3.0f, 
                                            0.0f, 4.0f, 5.0f, 
                                            0.0f, 0.0f, 6.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 6.0f &&
                           lOutput[ 1 ] == 9.0f &&
                           lOutput[ 2 ] == 6.0f );
        }

        /// <summary>
        /// Checks a uniformMatrix4x4 float based.
        /// </summary>
        [TestMethod]
        public void TestUniformMatrix4f()
        {
            LogManager.Instance.Log( "Running TestUniformMatrix4f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform mat4 u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = u * vec4(1.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Matrix4FUniform lUniform = lPass.GetUniform<Matrix4FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Matrix = new Matrix4F( 1.0f, 2.0f, 3.0f, 4.0f, 
                                            0.0f, 5.0f, 6.0f, 7.0f, 
                                            0.0f, 0.0f, 8.0f, 9.0f, 
                                            0.0f, 0.0f, 0.0f, 10.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 10.0f &&
                           lOutput[ 1 ] == 18.0f &&
                           lOutput[ 2 ] == 17.0f &&
                           lOutput[ 3 ] == 10.0f );
        }

        /// <summary>
        /// Checks a GPU structure.
        /// </summary>
        [TestMethod]
        public void TestStructure()
        {
            LogManager.Instance.Log( "Running TestStructure...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform struct { bool a; float b; int i; mat2 m; } u;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u.a), u.b, float(u.i), dot(u.m * vec2(1.0), vec2(1.0))); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            BoolUniform lBoolUniform = lPass.GetUniform<BoolUniform>( new UniformName( "u.a" ) );

            Assert.IsNotNull( lBoolUniform );

            lBoolUniform.Value = true;

            FloatUniform lFloatUniform = lPass.GetUniform<FloatUniform>( new UniformName( "u.b" ) );

            Assert.IsNotNull( lFloatUniform );

            lFloatUniform.Value = 2.0f;

            IntUniform lIntUniform = lPass.GetUniform<IntUniform>( new UniformName( "u.i" ) );

            Assert.IsNotNull( lIntUniform );

            lIntUniform.Value = 3;

            Matrix2FUniform lMat2x2Uniform = lPass.GetUniform<Matrix2FUniform>( new UniformName( "u.m" ) );

            Assert.IsNotNull( lMat2x2Uniform );

            lMat2x2Uniform.Matrix = new Matrix2F( 1.0f, 2.0f, 
                                                  0.0f, 3.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 6.0f );
        }

        /// <summary>
        /// Checks a GPU array of boolean.
        /// </summary>
        [TestMethod]
        public void TestBooleanArray()
        {
            LogManager.Instance.Log( "Running TestBooleanArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform bool u[4];" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u[0]), float(u[1]), float(u[2]), float(u[3])); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            BoolUniform lBool1Uniform = lPass.GetUniform<BoolUniform>( new UniformName( "u[0]" ) );

            Assert.IsNotNull( lBool1Uniform );

            lBool1Uniform.Value = true;

            BoolUniform lBool2Uniform = lPass.GetUniform<BoolUniform>( new UniformName( "u[1]" ) );

            Assert.IsNotNull( lBool2Uniform );

            lBool2Uniform.Value = false;

            BoolUniform lBool3Uniform = lPass.GetUniform<BoolUniform>( new UniformName( "u[2]" ) );

            Assert.IsNotNull( lBool3Uniform );

            lBool3Uniform.Value = true;

            BoolUniform lBool4Uniform = lPass.GetUniform<BoolUniform>( new UniformName( "u[3]" ) );

            Assert.IsNotNull( lBool4Uniform );

            lBool4Uniform.Value = true;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 0.0f &&
                           lOutput[ 2 ] == 1.0f &&
                           lOutput[ 3 ] == 1.0f );
        }

        /// <summary>
        /// Checks a GPU array of float.
        /// </summary>
        [TestMethod]
        public void TestFloatArray()
        {
            LogManager.Instance.Log( "Running TestFloatArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform float u[4];" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u[0], u[1], u[2], u[3]); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            FloatUniform lFloat1Uniform = lPass.GetUniform<FloatUniform>( new UniformName( "u[0]" ) );

            Assert.IsNotNull( lFloat1Uniform );

            lFloat1Uniform.Value = 1.0f;

            FloatUniform lFloat2Uniform = lPass.GetUniform<FloatUniform>( new UniformName( "u[1]" ) );

            Assert.IsNotNull( lFloat2Uniform );

            lFloat2Uniform.Value = 2.0f;

            FloatUniform lFloat3Uniform = lPass.GetUniform<FloatUniform>( new UniformName( "u[2]" ) );

            Assert.IsNotNull( lFloat3Uniform );

            lFloat3Uniform.Value = 3.0f;

            FloatUniform lFloat4Uniform = lPass.GetUniform<FloatUniform>( new UniformName( "u[3]" ) );

            Assert.IsNotNull( lFloat4Uniform );

            lFloat4Uniform.Value = 4.0f;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Checks a GPU array of int.
        /// </summary>
        [TestMethod]
        public void TestIntArray()
        {
            LogManager.Instance.Log( "Running TestIntArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform int u[4];" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u[0], u[1], u[2], u[3]); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            IntUniform lInt1Uniform = lPass.GetUniform<IntUniform>( new UniformName( "u[0]" ) );

            Assert.IsNotNull( lInt1Uniform );

            lInt1Uniform.Value = 1;

            IntUniform lInt2Uniform = lPass.GetUniform<IntUniform>( new UniformName( "u[1]" ) );

            Assert.IsNotNull( lInt2Uniform );

            lInt2Uniform.Value = 2;

            IntUniform lInt3Uniform = lPass.GetUniform<IntUniform>( new UniformName( "u[2]" ) );

            Assert.IsNotNull( lInt3Uniform );

            lInt3Uniform.Value = 3;

            IntUniform lInt4Uniform = lPass.GetUniform<IntUniform>( new UniformName( "u[3]" ) );

            Assert.IsNotNull( lInt4Uniform );

            lInt4Uniform.Value = 4;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Checks a GPU structure array.
        /// </summary>
        [TestMethod]
        public void TestStructureArray()
        {
            LogManager.Instance.Log( "Running TestStructureArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "struct s { bool a; float b; int i; mat2 m; };" );
            lBuilder.AppendLine( "uniform s u[4];" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u[0].a), u[1].b, float(u[2].i), dot(u[3].m * vec2(1.0), vec2(1.0))); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            BoolUniform lBoolUniform = lPass.GetUniform<BoolUniform>( new UniformName( "u[0].a" ) );

            Assert.IsNotNull( lBoolUniform );

            lBoolUniform.Value = true;

            FloatUniform lFloatUniform = lPass.GetUniform<FloatUniform>( new UniformName( "u[1].b" ) );

            Assert.IsNotNull( lFloatUniform );

            lFloatUniform.Value = 2.0f;

            IntUniform lIntUniform = lPass.GetUniform<IntUniform>( new UniformName( "u[2].i" ) );

            Assert.IsNotNull( lIntUniform );

            lIntUniform.Value = 3;

            Matrix2FUniform lMat2x2Uniform = lPass.GetUniform<Matrix2FUniform>( new UniformName( "u[3].m" ) );

            Assert.IsNotNull( lMat2x2Uniform );

            lMat2x2Uniform.Matrix = new Matrix2F( 1.0f, 2.0f, 
                                                  0.0f, 3.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 6.0f );
        }

        /// <summary>
        /// Checks a Subroutine.
        /// </summary>
        [TestMethod]
        public void TestSubroutine()
        {
            LogManager.Instance.Log( "Running TestSubroutine...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "subroutine float sr(float x);" );
            lBuilder.AppendLine( "subroutine (sr) float sr1(float x) { return x; }" );
            lBuilder.AppendLine( "subroutine (sr) float sr2(float x) { return x + 1.0f; }" );
            lBuilder.AppendLine( "subroutine uniform sr f;" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(f(1.0), 0.0, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            SubroutineUniform lUniform = lPass.GetSubroutineUniform( StageType.FRAGMENT, new UniformName( "f" ) );

            Assert.IsNotNull( lUniform );

            lUniform.SubroutineName = "sr1";

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32f, 1, 1 );
            
            float[] lPixel1 =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lPixel1Ptr = GCHandle.Alloc( lPixel1, GCHandleType.Pinned );
            
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lPixel1Ptr.AddrOfPinnedObject() ) );

            lPixel1Ptr.Free();

            lUniform.SubroutineName = "sr2";

            float[] lPixel2 =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lPixel2Ptr = GCHandle.Alloc( lPixel2, GCHandleType.Pinned );
            
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lPixel2Ptr.AddrOfPinnedObject() ) );

            lPixel2Ptr.Free();

            Assert.IsTrue( lPixel1[ 0 ] == 1.0f &&
                           lPixel2[ 0 ] == 2.0f );
        }

        /// <summary>
        /// Checks a Subroutine array.
        /// </summary>
        [TestMethod]
        public void TestSubroutineArray()
        {
            LogManager.Instance.Log( "Running TestSubroutineArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "subroutine float sr(float x);" );
            lBuilder.AppendLine( "subroutine (sr) float sr1(float x) { return x; }" );
            lBuilder.AppendLine( "subroutine (sr) float sr2(float x) { return x + 1.0f; }" );
            lBuilder.AppendLine( "subroutine uniform sr f[2];" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(f[0](1.0), f[1](1.0), 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            SubroutineUniform lRoutine1Uniform = lPass.GetSubroutineUniform( StageType.FRAGMENT, new UniformName( "f[0]" ) );

            Assert.IsNotNull( lRoutine1Uniform );

            lRoutine1Uniform.SubroutineName = "sr1";

            SubroutineUniform lRoutine2Uniform = lPass.GetSubroutineUniform( StageType.FRAGMENT, new UniformName( "f[1]" ) );

            Assert.IsNotNull( lRoutine2Uniform );

            lRoutine2Uniform.SubroutineName = "sr2";

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f );
        }

        #endregion Methods Tests
    }
}
