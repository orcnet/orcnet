﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms;
using OrcTest.Helpers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace OrcTest
{
    /// <summary>
    /// Texture based test class definition.
    /// </summary>
    [TestClass]
    public class TestTexture
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TestTexture"/> class.
        /// </summary>
        public TestTexture()
        {
            TestSuite.Instance.AddTest( "TestTextureBuffer", this.TestTextureBuffer );
            TestSuite.Instance.AddTest( "TestTexture1D", this.TestTexture1D );
            TestSuite.Instance.AddTest( "TestTextureArray1D", this.TestTextureArray1D );
            TestSuite.Instance.AddTest( "TestTexture2D", this.TestTexture2D );
            TestSuite.Instance.AddTest( "TestTextureRectangle", this.TestTextureRectangle );
            TestSuite.Instance.AddTest( "TestTextureArray2D", this.TestTextureArray2D );
            TestSuite.Instance.AddTest( "TestTexture3D", this.TestTexture3D );
            TestSuite.Instance.AddTest( "TestTextureCube", this.TestTextureCube );
            TestSuite.Instance.AddTest( "TestTextureArrayCube", this.TestTextureArrayCube );
            TestSuite.Instance.AddTest( "TestAutoTextureBinding", this.TestAutoTextureBinding );
            TestSuite.Instance.AddTest( "TestAutoSamplerBinding", this.TestAutoSamplerBinding );
        }

        #endregion Constructor

        #region Methods Tests

        /// <summary>
        /// Tests a texture buffer.
        /// </summary>
        [TestMethod]
        public void TestTextureBuffer()
        {
            LogManager.Instance.Log( "Running TestTextureBuffer...", LogType.INFO );

            byte[] lInput =
            {
                1,
                2,
                3,
                4
            };

            GPUBuffer lBuffer = new GPUBuffer();
            lBuffer.SetData( lInput, BufferUsageHint.StaticDraw );

            // Create the texture.
            ITexture lTexture = new TextureBuffer( PixelInternalFormat.R8i, lBuffer );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform isamplerBuffer tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = texelFetch(tex, int(floor(gl_FragCoord.x))); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 4, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 4, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 &&
                           lOutput[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture 1D.
        /// </summary>
        [TestMethod]
        public void TestTexture1D()
        {
            LogManager.Instance.Log( "Running TestTexture1D...", LogType.INFO );

            int[] lInput =
            {
                1,
                2,
                3,
                4
            };

            GCHandle lInputPtr = GCHandle.Alloc( lInput, GCHandleType.Pinned );

            // Create the texture.
            TextureParameters lParams = new TextureParameters();
            lParams.MagFilter = TextureMagFilter.Nearest;
            ITexture lTexture = new Texture1D( 4, 
                                               PixelInternalFormat.R8i, 
                                               PixelFormat.RedInteger, 
                                               PixelType.Int, lParams, 
                                               new BufferLayoutParameters(), 
                                               new CPUBuffer( lInputPtr.AddrOfPinnedObject() ) );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform isampler1D tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = texture(tex, gl_FragCoord.x / 4.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 4, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     4, 1, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();
            lInputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 &&
                           lOutput[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture array 1D.
        /// </summary>
        [TestMethod]
        public void TestTextureArray1D()
        {
            LogManager.Instance.Log( "Running TestTextureArray1D...", LogType.INFO );

            int[] lInput =
            {
                1,
                2,
                3,
                4
            };

            GCHandle lInputPtr = GCHandle.Alloc( lInput, GCHandleType.Pinned );

            // Create the texture.
            TextureParameters lParams = new TextureParameters();
            lParams.MagFilter = TextureMagFilter.Nearest;
            ITexture lTexture = new Texture1DArray( 2,
                                                    2,
                                                    PixelInternalFormat.R8i, 
                                                    PixelFormat.RedInteger, 
                                                    PixelType.Int, 
                                                    lParams, 
                                                    new BufferLayoutParameters(), 
                                                    new CPUBuffer( lInputPtr.AddrOfPinnedObject() ) );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform isampler1DArray tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { ivec2 uv = ivec2(floor(gl_FragCoord.xy)); color = texture(tex, uv); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 2, 2 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     2, 2, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();
            lInputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 &&
                           lOutput[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture 2D.
        /// </summary>
        [TestMethod]
        public void TestTexture2D()
        {
            LogManager.Instance.Log( "Running TestTexture2D...", LogType.INFO );

            int[] lInput =
            {
                1,
                2,
                3,
                4
            };

            GCHandle lInputPtr = GCHandle.Alloc( lInput, GCHandleType.Pinned );

            // Create the texture.
            TextureParameters lParams = new TextureParameters();
            lParams.MagFilter = TextureMagFilter.Nearest;
            ITexture lTexture = new Texture2D( 2,
                                               2,
                                               PixelInternalFormat.R8i, 
                                               PixelFormat.RedInteger, 
                                               PixelType.Int, 
                                               lParams, 
                                               new BufferLayoutParameters(), 
                                               new CPUBuffer( lInputPtr.AddrOfPinnedObject() ) );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform isampler2D tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { ivec2 uv = ivec2(floor(gl_FragCoord.xy)); color = texture(tex, uv); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 2, 2 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     2, 2, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();
            lInputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 &&
                           lOutput[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture Rectangle.
        /// </summary>
        [TestMethod]
        public void TestTextureRectangle()
        {
            LogManager.Instance.Log( "Running TestTextureRectangle...", LogType.INFO );

            int[] lInput =
            {
                1,
                2,
                3,
                4
            };

            GCHandle lInputPtr = GCHandle.Alloc( lInput, GCHandleType.Pinned );

            // Create the texture.
            TextureParameters lParams = new TextureParameters();
            lParams.MagFilter = TextureMagFilter.Nearest;
            ITexture lTexture = new TextureRectangle( 2,
                                                      2,
                                                      PixelInternalFormat.R8i, 
                                                      PixelFormat.RedInteger, 
                                                      PixelType.Int, 
                                                      lParams, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( lInputPtr.AddrOfPinnedObject() ) );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform isampler2DRect tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { ivec2 uv = ivec2(floor(gl_FragCoord.xy)); color = texture(tex, uv); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 2, 2 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     2, 2, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();
            lInputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 &&
                           lOutput[ 3 ] == 4 );
        }

        /// <summary>
        /// Tests a texture array 2D.
        /// </summary>
        [TestMethod]
        public void TestTextureArray2D()
        {
            LogManager.Instance.Log( "Running TestTextureArray2D...", LogType.INFO );

            int[] lInput =
            {
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8
            };

            GCHandle lInputPtr = GCHandle.Alloc( lInput, GCHandleType.Pinned );

            // Create the texture.
            TextureParameters lParams = new TextureParameters();
            lParams.MagFilter = TextureMagFilter.Nearest;
            ITexture lTexture = new Texture2DArray( 2,
                                                    2,
                                                    2,
                                                    PixelInternalFormat.R8i, 
                                                    PixelFormat.RedInteger, 
                                                    PixelType.Int, 
                                                    lParams, 
                                                    new BufferLayoutParameters(), 
                                                    new CPUBuffer( lInputPtr.AddrOfPinnedObject() ) );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform isampler2DArray tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { ivec2 uv = ivec2(floor(gl_FragCoord.xy)); color = texture(tex, vec3(uv, 1.0)); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 2, 2 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     2, 2, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();
            lInputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 5 &&
                           lOutput[ 1 ] == 6 &&
                           lOutput[ 2 ] == 7 &&
                           lOutput[ 3 ] == 8 );
        }

        /// <summary>
        /// Tests a texture 3D.
        /// </summary>
        [TestMethod]
        public void TestTexture3D()
        {
            LogManager.Instance.Log( "Running TestTexture3D...", LogType.INFO );

            int[] lInput =
            {
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8
            };

            GCHandle lInputPtr = GCHandle.Alloc( lInput, GCHandleType.Pinned );

            // Create the texture.
            TextureParameters lParams = new TextureParameters();
            lParams.MagFilter = TextureMagFilter.Nearest;
            ITexture lTexture = new Texture3D( 2,
                                               2,
                                               2,
                                               PixelInternalFormat.R8i, 
                                               PixelFormat.RedInteger, 
                                               PixelType.Int, 
                                               lParams, 
                                               new BufferLayoutParameters(), 
                                               new CPUBuffer( lInputPtr.AddrOfPinnedObject() ) );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform isampler3D tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { ivec2 uv = ivec2(floor(gl_FragCoord.xy)); color = texture(tex, vec3(uv, 0.75)); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 2, 2 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     2, 2, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();
            lInputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 5 &&
                           lOutput[ 1 ] == 6 &&
                           lOutput[ 2 ] == 7 &&
                           lOutput[ 3 ] == 8 );
        }

        /// <summary>
        /// Tests a texture Cube.
        /// </summary>
        [TestMethod]
        public void TestTextureCube()
        {
            LogManager.Instance.Log( "Running TestTextureCube...", LogType.INFO );

            int[] lInput =
            {
                1,
                2,
                3,
                4,
                5,
                6
            };

            BufferLayoutParameters[] lParams =
            {
                new BufferLayoutParameters(),
                new BufferLayoutParameters(),
                new BufferLayoutParameters(),
                new BufferLayoutParameters(),
                new BufferLayoutParameters(),
                new BufferLayoutParameters()
            };

            GCHandle lInputHandle = GCHandle.Alloc( lInput, GCHandleType.Pinned );
            IntPtr lInputPtr = lInputHandle.AddrOfPinnedObject();
            int lElementSize = sizeof(int);

            IBuffer[] lBuffers =
            {
                new CPUBuffer( lInputPtr ),
                new CPUBuffer( lInputPtr + ( 1 * lElementSize ) ),
                new CPUBuffer( lInputPtr + ( 2 * lElementSize ) ),
                new CPUBuffer( lInputPtr + ( 3 * lElementSize ) ),
                new CPUBuffer( lInputPtr + ( 4 * lElementSize ) ),
                new CPUBuffer( lInputPtr + ( 5 * lElementSize ) )
            };

            // Create the texture.
            TextureParameters lTexParams = new TextureParameters();
            lTexParams.MagFilter = TextureMagFilter.Nearest;
            ITexture lTexture = new TextureCube( 1,
                                                 1,
                                                 PixelInternalFormat.R8i, 
                                                 PixelFormat.RedInteger, 
                                                 PixelType.Int, 
                                                 lTexParams, 
                                                 lParams, 
                                                 lBuffers );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform isamplerCube tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "const vec3 dir[6] = vec3[](vec3(1.0, 0.0, 0.0), vec3(-1.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, -1.0, 0.0), vec3(0.0, 0.0, 1.0), vec3(0.0, 0.0, -1.0));" );
            lBuilder.AppendLine( "void main() { color = texture(tex, dir[int(floor(gl_FragCoord.x))]); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 6, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     6, 1, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();
            lInputHandle.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 &&
                           lOutput[ 3 ] == 4 &&
                           lOutput[ 4 ] == 5 &&
                           lOutput[ 5 ] == 6 );
        }

        /// <summary>
        /// Tests a texture array Cube.
        /// </summary>
        [TestMethod]
        public void TestTextureArrayCube()
        {
            LogManager.Instance.Log( "Running TestTextureArrayCube...", LogType.INFO );

            int[] lInput =
            {
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12
            };

            GCHandle lInputPtr = GCHandle.Alloc( lInput, GCHandleType.Pinned );

            // Create the texture.
            TextureParameters lParams = new TextureParameters();
            lParams.MagFilter = TextureMagFilter.Nearest;
            ITexture lTexture = new TextureCubeArray( 1,
                                                      1,
                                                      2,
                                                      PixelInternalFormat.R8i, 
                                                      PixelFormat.RedInteger, 
                                                      PixelType.Int, 
                                                      lParams, 
                                                      new BufferLayoutParameters(), 
                                                      new CPUBuffer( lInputPtr.AddrOfPinnedObject() ) );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "#extension GL_ARB_texture_cube_map_array : enable" );
            lBuilder.AppendLine( "uniform isamplerCubeArray tex;" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "const vec3 dir[6] = vec3[](vec3(1.0, 0.0, 0.0), vec3(-1.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, -1.0, 0.0), vec3(0.0, 0.0, 1.0), vec3(0.0, 0.0, -1.0));" );
            lBuilder.AppendLine( "void main() { color = texture(tex, vec4(dir[int(floor(gl_FragCoord.x))], 1.0)); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            TextureUniform lUniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = lTexture;

            int[] lOutput =
            {
                0,
                0,
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 6, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     6, 1, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();
            lInputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 7  &&
                           lOutput[ 1 ] == 8  &&
                           lOutput[ 2 ] == 9  &&
                           lOutput[ 3 ] == 10 &&
                           lOutput[ 4 ] == 11 &&
                           lOutput[ 5 ] == 12 );
        }

        /// <summary>
        /// Tests the automatic texture binding.
        /// </summary>
        [TestMethod]
        public void TestAutoTextureBinding()
        {
            LogManager.Instance.Log( "Running TestAutoTextureBinding...", LogType.INFO );

            List<GCHandle> lHandles = new List<GCHandle>();
            List<ITexture> lTextures = new List<ITexture>();
            for
                ( int lCurrTexture = 0; lCurrTexture < 128; lCurrTexture++ )
            {
                int[] lInput =
                {
                    lCurrTexture
                };
                GCHandle lInputPtr = GCHandle.Alloc( lInput, GCHandleType.Pinned );
                TextureParameters lTexParameters = new TextureParameters();
                lTexParameters.MagFilter = TextureMagFilter.Nearest;
                lTextures.Add( new Texture2D( 1, 
                                              1, 
                                              PixelInternalFormat.R32i, 
                                              PixelFormat.RedInteger, 
                                              PixelType.Int, 
                                              lTexParameters, 
                                              new BufferLayoutParameters(), 
                                              new CPUBuffer( lInputPtr.AddrOfPinnedObject() ) ) );
                lHandles.Add( lInputPtr );
            }

            List<PipelinePass> lPasses = new List<PipelinePass>();
            for
                ( int lCurrPass = 0; lCurrPass < 48; lCurrPass++ )
            {
                StringBuilder lSource = new StringBuilder();
                lSource.AppendLine( "uniform isampler2D tex1;" );
                lSource.AppendLine( "uniform isampler2D tex2;" );
                lSource.AppendLine( "uniform isampler2D tex3;" );
                lSource.AppendLine( "uniform isampler2D tex4;" );
                lSource.AppendLine( "layout(location=0) out ivec4 color;" );
                lSource.AppendLine( "void main() { color = texture(tex1, vec2(0.0)) + texture(tex2, vec2(0.0)) + texture(tex3, vec2(0.0)) + texture(tex4, vec2(0.0)); }" );

                PipelinePass lPass = new PipelinePass( 
                                     new PipelineDescription( 
                                     new PipelineStage( 330,
                                                        lSource.ToString(),
                                                        StageType.FRAGMENT ) ) );

                TextureUniform lTex1Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex1" ) );
                lTex1Uniform.Value = lTextures[ ( 3 * lCurrPass ) % 128 ];
                TextureUniform lTex2Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex2" ) );
                lTex2Uniform.Value = lTextures[ ( 3 * lCurrPass + 1 ) % 128 ];
                TextureUniform lTex3Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex3" ) );
                lTex3Uniform.Value = lTextures[ ( 3 * lCurrPass + 2 ) % 128 ];
                TextureUniform lTex4Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex4" ) );
                lTex4Uniform.Value = lTextures[ ( 3 * lCurrPass + 3 ) % 128 ];
                lPasses.Add( lPass );
            }

            bool lResult = true;
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );
            for
                ( int lCurrDraw = 0; lCurrDraw < 96; lCurrDraw++ )
            {
                int[] lOutput =
                {
                    0
                };
                GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
                int lPassId  = lCurrDraw % 48;
                int lToMatch = (3 * lPassId) % 128 + (3 * lPassId + 1) % 128 + (3 * lPassId + 2) % 128 + (3 * lPassId + 3) % 128;

                lFrameBuffer.Clear( true, true, true );
                lFrameBuffer.DrawQuad( lPasses[ lPassId ] );
                lFrameBuffer.ReadPixels( 0, 0, 
                                         1, 1, 
                                         PixelFormat.RedInteger, 
                                         PixelType.Int, 
                                         new BufferLayoutParameters(), 
                                         new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );
                lOutputPtr.Free();
                lResult = lResult && (lOutput[ 0 ] == lToMatch);
            }

            Assert.IsTrue( lResult );

            lHandles.ForEach( pElt => pElt.Free() );
            lTextures.ForEach( pElt => pElt.Dispose() );
            lPasses.ForEach( pElt => pElt.Dispose() );
            lFrameBuffer.Dispose();
        }

        /// <summary>
        /// Tests the automatic sampler binding.
        /// </summary>
        [TestMethod]
        public void TestAutoSamplerBinding2()
        {
            LogManager.Instance.Log( "Running TestAutoSamplerBinding...", LogType.INFO );

            TextureParameters lTexParameters = new TextureParameters();
            lTexParameters.MagFilter = TextureMagFilter.Nearest;
            Texture2D lTexture = new Texture2D( 1, 
                                                1, 
                                                PixelInternalFormat.R32i, 
                                                PixelFormat.RedInteger, 
                                                PixelType.Int, 
                                                lTexParameters, 
                                                new BufferLayoutParameters(), 
                                                new CPUBuffer( IntPtr.Zero ) );

            SamplerParameters lSampParameters = new SamplerParameters();
            lSampParameters.WrapS = TextureWrapMode.ClampToBorder;
            lSampParameters.WrapT = TextureWrapMode.ClampToBorder;
            lSampParameters.BorderColor = System.Drawing.Color.FromArgb( 0, 2, 0, 0 );
            Sampler lSampler = new Sampler( lSampParameters );

            StringBuilder lSource = new StringBuilder();
            lSource.AppendLine( "uniform isampler2D tex1;" );
            //lSource.AppendLine( "uniform isampler2D tex2;" );
            //lSource.AppendLine( "uniform isampler2D tex3;" );
            //lSource.AppendLine( "uniform isampler2D tex4;" );
            lSource.AppendLine( "layout(location=0) out ivec4 color;" );
            lSource.AppendLine( "void main() { color = texture(tex1, vec2(-1.0)); }" );

            PipelinePass lPass = new PipelinePass( 
                                 new PipelineDescription( 
                                 new PipelineStage( 330,
                                                    lSource.ToString(),
                                                    StageType.FRAGMENT ) ) );

            TextureUniform lTex1Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex1" ) );
            lTex1Uniform.Value   = lTexture;
            lTex1Uniform.Sampler = lSampler;

            //TextureUniform lTex2Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex2" ) );
            //lTex2Uniform.Value   = lTexture;
            //lTex2Uniform.Sampler = lSampler;

            //TextureUniform lTex3Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex3" ) );
            //lTex3Uniform.Value   = lTexture;
            //lTex3Uniform.Sampler = lSampler;

            //TextureUniform lTex4Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex4" ) );
            //lTex4Uniform.Value   = lTexture;
            //lTex4Uniform.Sampler = lSampler;

            int lMagFilter;
            GL.GetTexParameter( lTexture.TextureType, GetTextureParameter.TextureMagFilter, out lMagFilter );

            int lSamplerWrapS;
            int lSamplerWrapT;
            GL.GetSamplerParameter( lSampler.SamplerId, SamplerParameterName.TextureWrapS, out lSamplerWrapS );
            GL.GetSamplerParameter( lSampler.SamplerId, SamplerParameterName.TextureWrapT, out lSamplerWrapT );

            //int[] lInColor = { 2, 0, 0, 0 };
            //GL.SamplerParameterI( lSampler.SamplerId, SamplerParameterName.TextureBorderColor, lInColor );

            int[] lOutColor = new int[ 4 ];
            GL.GetSamplerParameterI( lSampler.SamplerId, All.TextureBorderColor, lOutColor );

            // INFO:
            // And now the magic is that no border color is used after the pass what means my Hardware or GL drivers sucks!!!!
            // As in the case of empty bound texture with a sampler whose WrapModes are ClampToBorder and the border color is set, the returned color must be the sampler border color itself.

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );

            int[] lOutput =
            {
                0
            };
            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RedInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );
            lOutputPtr.Free();
            
            Assert.IsTrue( lOutput[ 0 ] == 2 );
            
            lTexture.Dispose();
            lSampler.Dispose();
            lPass.Dispose();
            lFrameBuffer.Dispose();
        }

        /// <summary>
        /// Tests the automatic sampler binding.
        /// </summary>
        [TestMethod]
        public void TestAutoSamplerBinding()
        {
            LogManager.Instance.Log( "Running TestAutoSamplerBinding...", LogType.INFO );

            List<ITexture> lTextures = new List<ITexture>();
            for
                ( int lCurrTexture = 0; lCurrTexture < 8; lCurrTexture++ )
            {
                TextureParameters lTexParameters = new TextureParameters();
                lTexParameters.MagFilter = TextureMagFilter.Nearest;
                lTextures.Add( new Texture2D( 1, 
                                              1, 
                                              PixelInternalFormat.R32i, 
                                              PixelFormat.RedInteger, 
                                              PixelType.Int, 
                                              lTexParameters, 
                                              new BufferLayoutParameters(), 
                                              new CPUBuffer( IntPtr.Zero ) ) );
            }

            List<ISampler> lSamplers = new List<ISampler>();
            for
                ( int lCurrSampler = 0; lCurrSampler < 128; lCurrSampler++ )
            {
                SamplerParameters lSampParameters = new SamplerParameters();
                lSampParameters.WrapS = TextureWrapMode.ClampToBorder;
                lSampParameters.WrapT = TextureWrapMode.ClampToBorder;
                lSampParameters.BorderColor = System.Drawing.Color.FromArgb( 0, lCurrSampler, 0, 0 );
                lSamplers.Add( new Sampler( lSampParameters ) );
            }

            List<PipelinePass> lPasses = new List<PipelinePass>();
            for
                ( int lCurrPass = 0; lCurrPass < 48; lCurrPass++ )
            {
                StringBuilder lSource = new StringBuilder();
                lSource.AppendLine( "uniform isampler2D tex1;" );
                lSource.AppendLine( "uniform isampler2D tex2;" );
                lSource.AppendLine( "uniform isampler2D tex3;" );
                lSource.AppendLine( "uniform isampler2D tex4;" );
                lSource.AppendLine( "layout(location=0) out ivec4 color;" );
                lSource.AppendLine( "void main() { color = texture(tex1, vec2(-1.0)) + texture(tex2, vec2(-1.0)) + texture(tex3, vec2(-1.0)) + texture(tex4, vec2(-1.0)); }" );

                PipelinePass lPass = new PipelinePass( 
                                     new PipelineDescription( 
                                     new PipelineStage( 330,
                                                        lSource.ToString(),
                                                        StageType.FRAGMENT ) ) );

                TextureUniform lTex1Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex1" ) );
                lTex1Uniform.Value = lTextures[ lCurrPass % 8 ];
                lTex1Uniform.Sampler = lSamplers[ ( 3 * lCurrPass ) % 128 ];

                TextureUniform lTex2Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex2" ) );
                lTex2Uniform.Value = lTextures[ ( lCurrPass + 1 ) % 8 ];
                lTex2Uniform.Sampler = lSamplers[ ( 3 * lCurrPass + 1 ) % 128 ];

                TextureUniform lTex3Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex3" ) );
                lTex3Uniform.Value = lTextures[ ( lCurrPass + 2 ) % 8 ];
                lTex3Uniform.Sampler = lSamplers[ ( 3 * lCurrPass + 2 ) % 128 ];

                TextureUniform lTex4Uniform = lPass.GetUniform<TextureUniform>( new UniformName( "tex4" ) );
                lTex4Uniform.Value = lTextures[ ( lCurrPass + 3 ) % 8 ];
                lTex4Uniform.Sampler = lSamplers[ ( 3 * lCurrPass + 3 ) % 128 ];

                lPasses.Add( lPass );
            }

            bool lResult = true;
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );
            for
                ( int lCurrDraw = 0; lCurrDraw < 96; lCurrDraw++ )
            {
                int[] lOutput =
                {
                    0
                };
                GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
                int lPassId  = lCurrDraw % 48;
                int lToMatch = (3 * lPassId) % 128 + (3 * lPassId + 1) % 128 + (3 * lPassId + 2) % 128 + (3 * lPassId + 3) % 128;

                lFrameBuffer.Clear( true, true, true );
                lFrameBuffer.DrawQuad( lPasses[ lPassId ] );
                lFrameBuffer.ReadPixels( 0, 0, 
                                         1, 1, 
                                         PixelFormat.RedInteger, 
                                         PixelType.Int, 
                                         new BufferLayoutParameters(), 
                                         new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );
                lOutputPtr.Free();
                lResult = lResult && (lOutput[ 0 ] == lToMatch);
            }

            Assert.IsTrue( lResult );
            
            lTextures.ForEach( pElt => pElt.Dispose() );
            lSamplers.ForEach( pElt => pElt.Dispose() );
            lPasses.ForEach( pElt => pElt.Dispose() );
            lFrameBuffer.Dispose();
        }

        #endregion Methods Tests
    }
}
