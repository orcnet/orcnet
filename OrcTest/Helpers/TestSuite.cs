﻿using OrcNet.Core;
using OrcNet.Core.Logger;
using System.Collections.Generic;

namespace OrcTest.Helpers
{
    /// <summary>
    /// Test suite manager class definition.
    /// </summary>
    public sealed class TestSuite
    {
        #region Fields

        /// <summary>
        /// Stores the test manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile TestSuite sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the set of tests to process.
        /// </summary>
        private Dictionary<string, Test> mTests;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the test manager handle.
        /// </summary>
        public static TestSuite Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if ( sInstance == null )
                {
                    // Lock on
                    lock ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if ( sInstance == null )
                        {
                            sInstance = new TestSuite();
                        }
                    }
                }

                return sInstance;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TestSuite"/> class.
        /// </summary>
        private TestSuite()
        {
            // Initialize the logger for tests.
            Application.Instance.Initialize( LoggerType.TXT );

            TestHelpers.InitializeOpenTK();

            this.mTests = new Dictionary<string, Test>();
        }

        /// <summary>
        /// Destroys the tests.
        /// </summary>
        ~TestSuite()
        {
            LogManager.Instance.Dispose(); // Clean files.
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new test.
        /// </summary>
        /// <param name="pName">The test name.</param>
        /// <param name="pJob">The test job.</param>
        /// <param name="pVersion">The test major version.</param>
        public void AddTest(string pName, Test.TestFunctionDelegate pJob, int pVersion = 3)
        {
            this.AddTest( new Test( pName, pJob, pVersion ) );
        }

        /// <summary>
        /// Adds a new test.
        /// </summary>
        /// <param name="pTest">The new test to do.</param>
        public void AddTest(Test pTest)
        {
            this.mTests[ pTest.Name ] = pTest;
        }

        #endregion Methods
    }
}
