﻿namespace OrcTest.Helpers
{
    /// <summary>
    /// Test class definition owning the test name,
    /// method to run and the major version linked to that test 
    /// for filtering purpose.
    /// </summary>
    public class Test
    {
        #region Delegates

        /// <summary>
        /// Test function prototype definition.
        /// </summary>
        public delegate void TestFunctionDelegate();

        #endregion Delegates

        #region Fields

        /// <summary>
        /// Stores the test name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the job to run for the test.
        /// </summary>
        private TestFunctionDelegate mJob;

        /// <summary>
        /// Stores the test major version.
        /// </summary>
        private int mVersion;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the test name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
        }

        /// <summary>
        /// Gets the job to run for the test.
        /// </summary>
        public TestFunctionDelegate Job
        {
            get
            {
                return this.mJob;
            }
        }

        /// <summary>
        /// Gets the test major version.
        /// </summary>
        public int Version
        {
            get
            {
                return this.mVersion;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Test"/> class.
        /// </summary>
        /// <param name="pName">The test name.</param>
        /// <param name="pJob">The test job.</param>
        /// <param name="pVersion">The test major version.</param>
        public Test(string pName, TestFunctionDelegate pJob, int pVersion = 3)
        {
            this.mName    = pName;
            this.mJob     = pJob;
            this.mVersion = pVersion;
        }

        #endregion Constructor
    }
}
