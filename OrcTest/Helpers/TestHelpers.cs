﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OrcNet.Graphics.Render;
using System.IO;
using System.Text;

namespace OrcTest.Helpers
{
    /// <summary>
    /// Test helper methods class definition.
    /// </summary>
    public static class TestHelpers
    {
        #region Methods
        
        /// <summary>
        /// Initializes the OpenTK context and callbacks.
        /// </summary>
        public static void InitializeOpenTK()
        {
            NativeWindow lFakeWindow = new NativeWindow();
            IGraphicsContext lContext = new GraphicsContext( GraphicsMode.Default, lFakeWindow.WindowInfo );
            lContext.MakeCurrent( lFakeWindow.WindowInfo );
            lContext.LoadAll();
        }

        /// <summary>
        /// Gets a new frame buffer for test purpose.
        /// </summary>
        /// <param name="pStorage">The buffer storage type.</param>
        /// <param name="pWidth"></param>
        /// <param name="pHeight"></param>
        /// <returns></returns>
        public static FrameBuffer GetFrameBuffer(OpenTK.Graphics.OpenGL.RenderbufferStorage pStorage, int pWidth, int pHeight)
        {
            RenderBuffer lRenderBuffer = new RenderBuffer( pStorage, pWidth, pHeight );
            FrameBuffer lFrameBuffer   = new FrameBuffer();
            lFrameBuffer.Attach( BufferId.COLOR0, lRenderBuffer );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0, 0, pWidth, pHeight ) );
            return lFrameBuffer;
        }

        /// <summary>
        /// Creates a new file to set it the given content.
        /// </summary>
        /// <param name="pFileName">The file name.</param>
        /// <param name="pContent">The file content.</param>
        public static void CreateFile(string pFileName, string pContent)
        {
            byte[] lContent = Encoding.UTF8.GetBytes( pContent );
            using 
                ( FileStream lFile = File.Create( pFileName, lContent.Length ) )
            {
                lFile.Write( lContent, 0, lContent.Length );
                lFile.Close();
            }
        }

        /// <summary>
        /// Creates a new file of a given size to set it the given content.
        /// </summary>
        /// <param name="pFileName">The file name.</param>
        /// <param name="pSize">The buffer size to force.</param>
        /// <param name="pContent">The file content.</param>
        public static void CreateFile(string pFileName, int pSize, byte[] pContent)
        {
            using 
                ( FileStream lFile = File.Create( pFileName, pSize ) )
            {
                lFile.Write( pContent, 0, pContent.Length );
                lFile.Close();
            }
        }

        /// <summary>
        /// Deletes a file from memory.
        /// </summary>
        /// <param name="pFileName">The file to delete.</param>
        public static void DeleteFile(string pFileName)
        {
            File.Delete( pFileName );
        }

        #endregion Methods
    }
}
