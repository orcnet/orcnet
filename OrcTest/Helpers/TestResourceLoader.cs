﻿using OrcNet.Core.Resource.Loaders;
using OrcNet.Core.Resource;

namespace OrcTest.Helpers
{
    /// <summary>
    /// Resource loader especially done for test purpose.
    /// </summary>
    public class TestResourceLoader : XMLResourceLoader
    {
        #region Methods

        /// <summary>
        /// Loads again a resource description.
        /// </summary>
        /// <param name="pName">The resource name</param>
        /// <param name="pCurrentDescription">The current resource description.</param>
        /// <returns>The resource description, Null otherwise.</returns>
        public override AResourceDescriptor ReloadResource(string pName, AResourceDescriptor pCurrentDescription)
        {
            return this.LoadResource( pName );
        }

        #endregion Methods
    }
}
