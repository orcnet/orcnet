﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Core.Resource;
using OrcNet.Core.Resource.Loaders;
using OrcNet.Core.Service;
using OrcNet.Graphics.Mesh;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Textures;
using OrcNet.Graphics.Render.Uniforms;
using OrcNet.Graphics.Services;
using OrcTest.Helpers;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace OrcTest
{
    /// <summary>
    /// Resource based test class definition.
    /// </summary>
    [TestClass]
    public class TestResource
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TestResource"/> class.
        /// </summary>
        public TestResource()
        {
            TestSuite.Instance.AddTest( "TestPipelineDescriptionResource", this.TestPipelineDescriptionResource );
            TestSuite.Instance.AddTest( "TestTextureResourceUpdate", this.TestTextureResourceUpdate );
            TestSuite.Instance.AddTest( "TestPipelineDescriptionResourceUpdate", this.TestPipelineDescriptionResourceUpdate );
            TestSuite.Instance.AddTest( "TestDescriptionResourceUpdateWithTextureUniforms", this.TestDescriptionResourceUpdateWithTextureUniforms );
            TestSuite.Instance.AddTest( "TestDescriptionResourceUpdateWithSubroutineUniforms", this.TestDescriptionResourceUpdateWithSubroutineUniforms, 4 );
            TestSuite.Instance.AddTest( "TestDescriptionResourceUpdateRemovedUniform", this.TestDescriptionResourceUpdateRemovedUniform );
            TestSuite.Instance.AddTest( "TestDescriptionResourceUpdateRemovedSubroutine", this.TestDescriptionResourceUpdateRemovedSubroutine, 4 );
            TestSuite.Instance.AddTest( "TestDescriptionResourceUpdateWithUniformBlocks", this.TestDescriptionResourceUpdateWithUniformBlocks );
        }

        #endregion Constructor

        #region Methods Tests

        /// <summary>
        /// Checks the creation and load of a pipeline description based resource.
        /// </summary>
        [TestMethod]
        public void TestPipelineDescriptionResource()
        {
            LogManager.Instance.Log( "Running TestPipelineDescriptionResource...", LogType.INFO );

            TestHelpers.CreateFile( "TestPipelineDescriptionResource.glsl", "#ifdef _VERTEX_\nlayout(location=0) in vec4 p; out vec4 q; void main() { q = p; }\n#endif\n" );
            TestHelpers.CreateFile( "TestPipelineDescriptionResource.xml", "<?xml version=\"1.0\" ?>\n<module name=\"TestPipelineDescriptionResource\" version=\"330\" source=\"TestPipelineDescriptionResource.glsl\" feedback=\"interleaved\" varyings=\"q\"/>\n");

            XMLResourceLoader lLoader = new TestResourceLoader();
            lLoader.AddPath( "." );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            PipelinePass lPass = null;
            IResource lPassResource = lResourceService.LoadResource( "TestPipelineDescriptionResource;" );
            if
                ( lPassResource != null )
            {
                lPass = lPassResource.OwnedObject as PipelinePass;
            }

            Assert.AreNotEqual( null, lPass );

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            Mesh<Vector4, uint> lPoint = new Mesh<Vector4, uint>( PrimitiveType.Points, MeshUsage.GPU_STATIC );
            lPoint.AddAttributeType( 0, 4, AttributeType.FLOAT_BASED, false );
            lPoint.AddVertex( new Vector4( 1.0f, 2.0f, 3.0f, 4.0f ) );

            IRenderService lRenderService = ServiceManager.Instance.GetService<IRenderService>();
            TransformFeedback lTransformFeedback = lRenderService.DefaultTransformFeedBack;

            GPUQuery lQuery = new GPUQuery( QueryTarget.PrimitivesGenerated );
            GPUBuffer lBuffer = new GPUBuffer();
            lBuffer.SetData( 128, IntPtr.Zero, BufferUsageHint.StreamCopy );
            lTransformFeedback.RecordVertexVarying( 0, lBuffer );

            lQuery.Begin();
            lRenderService.StartTransformFeedback( lFrameBuffer, lPass, PrimitiveType.Points, lTransformFeedback, false );
            lRenderService.Transform( lPoint.Buffers, 0, 1 );
            lRenderService.EndTransformFeedback();
            lQuery.End();

            int lResult = (int)lQuery.Result;

            float[] lData = new float[ 4 ];
            lBuffer.GetSubData( 0, 16, lData );
            
            Assert.IsTrue( lData[ 0 ] == 1.0f && 
                           lData[ 1 ] == 2.0f &&
                           lData[ 2 ] == 3.0f &&
                           lData[ 3 ] == 4.0f &&
                           lResult == 1 );

            TestHelpers.DeleteFile( "TestPipelineDescriptionResource.glsl" );
            TestHelpers.DeleteFile( "TestPipelineDescriptionResource.xml" );
        }

        /// <summary>
        /// Checks the creation and update of a texture based resource.
        /// </summary>
        [TestMethod]
        public void TestTextureResourceUpdate()
        {
            LogManager.Instance.Log( "Running TestTextureResourceUpdate...", LogType.INFO );

            TestHelpers.CreateFile( "TestTextureResourceUpdate.xml", "<?xml version=\"1.0\" ?>\n<texture2D name=\"TestTextureResourceUpdate\" source=\"TestTextureResourceUpdate.tga\" internalformat=\"RGB8UI\" format=\"RGB_INTEGER\" min=\"NEAREST\" mag=\"NEAREST\"/>\n");

            byte[] lImage1 = 
            {
                0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 24, 0, 2, 1, 0
            };
            TestHelpers.CreateFile( "TestTextureResourceUpdate.tga", 25, lImage1 );

            XMLResourceLoader lLoader = new TestResourceLoader();
            lLoader.AddPath( "." );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            Texture2D lTexture = null;
            IResource lTextureResource = lResourceService.LoadResource( "TestTextureResourceUpdate" );
            if
                ( lTextureResource != null )
            {
                lTexture = lTextureResource.OwnedObject as Texture2D;
            }

            Assert.AreNotEqual( null, lTexture );

            StringBuilder lStageSource = new StringBuilder();
            lStageSource.AppendLine("uniform isampler2D u;");
            lStageSource.AppendLine("layout(location = 0) out ivec4 color;");
            lStageSource.AppendLine("void main() { color = texture(u, vec2(0.0)); }");
            lStageSource.AppendLine();
            PipelinePass lPass = new PipelinePass( new PipelineDescription( new PipelineStage( 330, lStageSource.ToString(), StageType.FRAGMENT ) ) );
            TextureUniform lTextureUniform = lPass.GetUniform<TextureUniform>( new UniformName( "u" ) );
            lTextureUniform.Value = lTexture;

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb8ui, 1, 1 );
            int[] lPixel1 =
            {
                0,
                0,
                0
            };

            GCHandle lPixel1Ptr = GCHandle.Alloc( lPixel1, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RgbInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel1Ptr.AddrOfPinnedObject() ) );

            lPixel1Ptr.Free();

            byte[] lImage2 = 
            {
                0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 24, 0, 5, 4, 3
            };
            TestHelpers.CreateFile( "TestTextureResourceUpdate.tga", 25, lImage2 );

            lResourceService.UpdateResources();

            int[] lPixel2 =
            {
                0,
                0,
                0
            };

            GCHandle lPixel2Ptr = GCHandle.Alloc( lPixel2, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RgbInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel2Ptr.AddrOfPinnedObject() ) );

            lPixel2Ptr.Free();

            Assert.IsTrue( lPixel1[ 0 ] == 0 && lPixel1[ 1 ] == 1 && lPixel1[ 2 ] == 2 &&
                           lPixel2[ 0 ] == 3 && lPixel2[ 1 ] == 4 && lPixel2[ 2 ] == 5 );

            TestHelpers.DeleteFile( "TestTextureResourceUpdate.xml" );
            TestHelpers.DeleteFile( "TestTextureResourceUpdate.tga" );
        }

        /// <summary>
        /// Checks the creation and update of a pipeline description based resource.
        /// </summary>
        [TestMethod]
        public void TestPipelineDescriptionResourceUpdate()
        {
            LogManager.Instance.Log( "Running TestPipelineDescriptionResourceUpdate...", LogType.INFO );

            TestHelpers.CreateFile( "TestPipelineDescriptionResourceUpdate.xml", "<?xml version=\"1.0\" ?>\n<module name=\"TestPipelineDescriptionResourceUpdate\" version=\"330\" source=\"TestPipelineDescriptionResourceUpdate.glsl\">\n<uniform1i name=\"u\" x=\"1\"/>\n</module>\n");
            TestHelpers.CreateFile( "TestPipelineDescriptionResourceUpdate.glsl", "#ifdef _FRAGMENT_\nlayout(location=0) out ivec4 color;\nuniform int u;\nvoid main() { color = ivec4(u); }\n#endif\n" );

            XMLResourceLoader lLoader = new TestResourceLoader();
            lLoader.AddPath( "." );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            PipelinePass lPass = null;
            IResource lPassResource = lResourceService.LoadResource( "TestPipelineDescriptionResourceUpdate;" );
            if
                ( lPassResource != null )
            {
                lPass = lPassResource.OwnedObject as PipelinePass;
            }

            Assert.AreNotEqual( null, lPass );

            IntUniform lUniform = lPass.GetUniform<IntUniform>( new UniformName( "u" ) );

            Assert.AreNotEqual( null, lUniform );

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );
            int[] lPixel1 = { 0 };
            int[] lPixel2 = { 0 };
            int[] lPixel3 = { 0 };
            int[] lPixel4 = { 0 };

            GCHandle lPixel1Ptr = GCHandle.Alloc( lPixel1, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel1Ptr.AddrOfPinnedObject() ) );

            lPixel1Ptr.Free();

            lUniform.Value = 2;

            GCHandle lPixel2Ptr = GCHandle.Alloc( lPixel2, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel2Ptr.AddrOfPinnedObject() ) );

            lPixel2Ptr.Free();

            TestHelpers.CreateFile( "TestPipelineDescriptionResourceUpdate.glsl", "#ifdef _FRAGMENT_\nlayout(location=0) out ivec4 color;\nuniform int u;\nvoid main() { color = ivec4(u + 1); }\n#endif\n" );
            lResourceService.UpdateResources();

            GCHandle lPixel3Ptr = GCHandle.Alloc( lPixel3, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel3Ptr.AddrOfPinnedObject() ) );

            lPixel3Ptr.Free();

            lUniform.Value = 3;

            GCHandle lPixel4Ptr = GCHandle.Alloc( lPixel4, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel4Ptr.AddrOfPinnedObject() ) );

            lPixel4Ptr.Free();

            Assert.IsTrue( lPixel1[ 0 ] == 1 && 
                           lPixel2[ 0 ] == 2 && 
                           lPixel3[ 0 ] == 3 && 
                           lPixel4[ 0 ] == 4 );

            TestHelpers.DeleteFile( "TestPipelineDescriptionResourceUpdate.xml" );
            TestHelpers.DeleteFile( "TestPipelineDescriptionResourceUpdate.glsl" );
        }

        /// <summary>
        /// Checks the creation and update of a 
        /// pipeline description based resource with texture uniforms inside.
        /// </summary>
        [TestMethod]
        public void TestDescriptionResourceUpdateWithTextureUniforms()
        {
            LogManager.Instance.Log( "Running TestDescriptionResourceUpdateWithTextureUniforms...", LogType.INFO );
            
            byte[] lImage1 = { 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 24, 0, 2, 1, 0 };
            byte[] lImage2 = { 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 24, 0, 5, 4, 3 };
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateTex1.xml", "<?xml version=\"1.0\" ?>\n<texture2D name=\"TestDescriptionResourceUpdateTex1\" source=\"TestDescriptionResourceUpdateTex1.tga\" internalformat=\"RGB8UI\" format=\"RGB_INTEGER\" min=\"NEAREST\" mag=\"NEAREST\"/>\n" );
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateTex1.tga", 25, lImage1 );

            TestHelpers.CreateFile( "TestDescriptionResourceUpdateTex2.xml", "<?xml version=\"1.0\" ?>\n<texture2D name=\"TestDescriptionResourceUpdateTex2\" source=\"TestDescriptionResourceUpdateTex2.tga\" internalformat=\"RGB8UI\" format=\"RGB_INTEGER\" min=\"NEAREST\" mag=\"NEAREST\"/>\n" );
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateTex2.tga", 25, lImage2 );

            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithTextureUniforms.xml", "<?xml version=\"1.0\" ?>\n<module name=\"TestDescriptionResourceUpdateWithTextureUniforms\" version=\"330\" fragment=\"TestDescriptionResourceUpdateWithTextureUniforms.glsl\">\n<uniformSampler name=\"u1\" texture=\"TestDescriptionResourceUpdateTex1\"/>\n<uniformSampler name=\"u2\" texture=\"TestDescriptionResourceUpdateTex2\"/>\n</module>\n" );
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithTextureUniforms.glsl", "layout(location=0) out ivec4 color;\nuniform isampler2D u1;\nuniform isampler2D u2;\nvoid main() { color = texture(u1, vec2(0.5)) + texture(u2, vec2(0.5)); }\n" );

            XMLResourceLoader lLoader = new TestResourceLoader();
            lLoader.AddPath( "." );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            PipelinePass lPass = null;
            IResource lPassResource = lResourceService.LoadResource( "TestDescriptionResourceUpdateWithTextureUniforms;" );
            if
                ( lPassResource != null )
            {
                lPass = lPassResource.OwnedObject as PipelinePass;
            }

            Assert.AreNotEqual( null, lPass );

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb8ui, 1, 1 );
            int[] lPixel1 =
            {
                0,
                0,
                0
            };

            GCHandle lPixel1Ptr = GCHandle.Alloc( lPixel1, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RgbInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel1Ptr.AddrOfPinnedObject() ) );

            lPixel1Ptr.Free();

            // Update the resource.
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithTextureUniforms.glsl", "layout(location=0) out ivec4 color;\nuniform isampler2D u1;\nuniform isampler2D u2;\nvoid main() { color = texture(u1, vec2(0.5)) + texture(u2, vec2(0.5)) + ivec4(1); }\n" );
            lResourceService.UpdateResources();

            int[] lPixel2 =
            {
                0,
                0,
                0
            };

            GCHandle lPixel2Ptr = GCHandle.Alloc( lPixel2, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RgbInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel2Ptr.AddrOfPinnedObject() ) );

            lPixel2Ptr.Free();

            Assert.IsTrue( lPixel1[ 0 ] == 3 && lPixel1[ 1 ] == 5 && lPixel1[ 2 ] == 7 &&
                           lPixel2[ 0 ] == 4 && lPixel2[ 1 ] == 6 && lPixel2[ 2 ] == 8 );
            
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateTex1.xml" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateTex1.tga" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateTex2.xml" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateTex2.tga" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateWithTextureUniforms.xml" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateWithTextureUniforms.glsl" );
        }

        /// <summary>
        /// Checks the creation and update of a 
        /// pipeline description based resource with subroutine uniforms inside.
        /// </summary>
        [TestMethod]
        public void TestDescriptionResourceUpdateWithSubroutineUniforms()
        {
            LogManager.Instance.Log( "Running TestDescriptionResourceUpdateWithSubroutineUniforms...", LogType.INFO );

            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithSubroutineUniforms.xml", "<?xml version=\"1.0\" ?>\n<module name=\"TestDescriptionResourceUpdateWithSubroutineUniforms\" version=\"400\" fragment=\"TestDescriptionResourceUpdateWithSubroutineUniforms.glsl\">\n<uniformSubroutine stage=\"FRAGMENT\" name=\"u\" subroutine=\"sr1\"/>\n</module>\n" );
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithSubroutineUniforms.glsl", "layout(location=0) out ivec4 color;\nsubroutine int sr(int x);\nsubroutine (sr) int sr1(int x) { return x; }\nsubroutine (sr) int sr2(int x) { return x + 1; }\nsubroutine uniform sr u;\nvoid main() { color = ivec4(u(0)); }\n" );

            XMLResourceLoader lLoader = new TestResourceLoader();
            lLoader.AddPath( "." );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            PipelinePass lPass = null;
            IResource lPassResource = lResourceService.LoadResource( "TestDescriptionResourceUpdateWithSubroutineUniforms;" );
            if
                ( lPassResource != null )
            {
                lPass = lPassResource.OwnedObject as PipelinePass;
            }

            Assert.AreNotEqual( null, lPass );

            SubroutineUniform lUniform = lPass.GetSubroutineUniform( StageType.FRAGMENT, new UniformName( "u" ) );

            Assert.AreNotEqual( null, lUniform );

            lUniform.SubroutineName = "sr2";

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb8ui, 1, 1 );
            int[] lPixel1 =
            {
                0,
                0,
                0
            };

            GCHandle lPixel1Ptr = GCHandle.Alloc( lPixel1, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RgbInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel1Ptr.AddrOfPinnedObject() ) );

            lPixel1Ptr.Free();

            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithSubroutineUniforms.glsl", "layout(location=0) out ivec4 color;\nsubroutine int sr(int x);\nsubroutine (sr) int sr1(int x) { return x + 2; }\nsubroutine (sr) int sr2(int x) { return x + 3; }\nsubroutine uniform sr u;\nvoid main() { color = ivec4(u(0)); }\n" );
            lResourceService.UpdateResources();

            int[] lPixel2 =
            {
                0,
                0,
                0
            };

            GCHandle lPixel2Ptr = GCHandle.Alloc( lPixel2, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RgbInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel2Ptr.AddrOfPinnedObject() ) );

            lPixel2Ptr.Free();

            Assert.IsTrue( lPixel1[ 0 ] == 1 &&
                           lPixel2[ 0 ] == 3 );

            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateWithSubroutineUniforms.xml" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateWithSubroutineUniforms.glsl" );
        }

        /// <summary>
        /// Checks the creation and update of a 
        /// pipeline description based resource after an uniform removal.
        /// </summary>
        [TestMethod]
        public void TestDescriptionResourceUpdateRemovedUniform()
        {
            LogManager.Instance.Log( "Running TestDescriptionResourceUpdateRemovedUniform...", LogType.INFO );

            TestHelpers.CreateFile( "TestDescriptionResourceUpdateRemovedUniform.xml", "<?xml version=\"1.0\" ?>\n<module name=\"TestDescriptionResourceUpdateRemovedUniform\" version=\"330\" fragment=\"TestDescriptionResourceUpdateRemovedUniform.glsl\">\n<uniform1i name=\"u\" x=\"3\"/>\n</module>\n" );
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateRemovedUniform.glsl", "layout(location=0) out ivec4 color;\nuniform int u;\nvoid main() { color = ivec4(u); }\n" );

            XMLResourceLoader lLoader = new TestResourceLoader();
            lLoader.AddPath( "." );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            PipelinePass lPass = null;
            IResource lPassResource = lResourceService.LoadResource( "TestDescriptionResourceUpdateRemovedUniform;" );
            if
                ( lPassResource != null )
            {
                lPass = lPassResource.OwnedObject as PipelinePass;
            }

            Assert.AreNotEqual( null, lPass );

            IntUniform lUniform = lPass.GetUniform<IntUniform>( new UniformName( "u" ) );

            Assert.AreNotEqual( null, lUniform );

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );
            int[] lPixel1 = { 0 };
            int[] lPixel2 = { 0 };
            int[] lPixel3 = { 0 };
            int[] lPixel4 = { 0 };

            lUniform.Value = 1;

            GCHandle lPixel1Ptr = GCHandle.Alloc( lPixel1, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel1Ptr.AddrOfPinnedObject() ) );

            lPixel1Ptr.Free();

            // Remove the uniform by setting a new shader without it.
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateRemovedUniform.glsl", "layout(location=0) out ivec4 color;\n\nvoid main() { color = ivec4(2); }\n" );
            lResourceService.UpdateResources();

            GCHandle lPixel2Ptr = GCHandle.Alloc( lPixel2, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel2Ptr.AddrOfPinnedObject() ) );

            lPixel2Ptr.Free();

            // Add it back.
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateRemovedUniform.glsl", "layout(location=0) out ivec4 color;\nuniform int u;\nvoid main() { color = ivec4(u + 2); }\n" );
            lResourceService.UpdateResources();

            GCHandle lPixel3Ptr = GCHandle.Alloc( lPixel3, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel3Ptr.AddrOfPinnedObject() ) );

            lPixel3Ptr.Free();

            lUniform.Value = 2;

            GCHandle lPixel4Ptr = GCHandle.Alloc( lPixel4, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel4Ptr.AddrOfPinnedObject() ) );

            lPixel4Ptr.Free();

            Assert.IsTrue( lPixel1[ 0 ] == 1 && 
                           lPixel2[ 0 ] == 2 && 
                           lPixel3[ 0 ] == 3 && 
                           lPixel4[ 0 ] == 4 );

            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateRemovedUniform.xml" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateRemovedUniform.glsl" );
        }

        /// <summary>
        /// Checks the creation and update of a 
        /// pipeline description based resource after a subroutine removal.
        /// </summary>
        [TestMethod]
        public void TestDescriptionResourceUpdateRemovedSubroutine()
        {
            LogManager.Instance.Log( "Running TestDescriptionResourceUpdateRemovedSubroutine...", LogType.INFO );

            TestHelpers.CreateFile( "TestDescriptionResourceUpdateRemovedSubroutine.xml", "<?xml version=\"1.0\" ?>\n<module name=\"TestDescriptionResourceUpdateRemovedSubroutine\" version=\"400\" fragment=\"TestDescriptionResourceUpdateRemovedSubroutine.glsl\">\n<uniformSubroutine stage=\"FRAGMENT\" name=\"u\" subroutine=\"sr1\"/>\n</module>\n" );
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateRemovedSubroutine.glsl", "layout(location=0) out ivec4 color;\nsubroutine int sr(int x);\nsubroutine (sr) int sr1(int x) { return x; }\nsubroutine (sr) int sr2(int x) { return x + 1; }\nsubroutine uniform sr u;\nvoid main() { color = ivec4(u(0)); }\n" );

            XMLResourceLoader lLoader = new TestResourceLoader();
            lLoader.AddPath( "." );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            PipelinePass lPass = null;
            IResource lPassResource = lResourceService.LoadResource( "TestDescriptionResourceUpdateRemovedSubroutine;" );
            if
                ( lPassResource != null )
            {
                lPass = lPassResource.OwnedObject as PipelinePass;
            }

            Assert.AreNotEqual( null, lPass );

            SubroutineUniform lUniform = lPass.GetSubroutineUniform( StageType.FRAGMENT, new UniformName( "u" ) );

            Assert.AreNotEqual( null, lUniform );

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );
            int[] lPixel1 = { 0 };
            int[] lPixel2 = { 0 };
            int[] lPixel3 = { 0 };
            int[] lPixel4 = { 0 };

            lUniform.SubroutineName = "sr1";

            GCHandle lPixel1Ptr = GCHandle.Alloc( lPixel1, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel1Ptr.AddrOfPinnedObject() ) );

            lPixel1Ptr.Free();

            // Remove the subroutine by setting a new shader without it.
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateRemovedSubroutine.glsl", "layout(location=0) out ivec4 color;\nvoid main() { color = ivec4(1); }\n" );
            lResourceService.UpdateResources();

            GCHandle lPixel2Ptr = GCHandle.Alloc( lPixel2, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel2Ptr.AddrOfPinnedObject() ) );

            lPixel2Ptr.Free();

            // Add it back.
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateRemovedSubroutine.glsl", "layout(location=0) out ivec4 color;\nsubroutine int sr(int x);\nsubroutine (sr) int sr1(int x) { return x; }\nsubroutine (sr) int sr2(int x) { return x + 1; }\nsubroutine uniform sr u;\nvoid main() { color = ivec4(u(0)); }\n" );
            lResourceService.UpdateResources();

            GCHandle lPixel3Ptr = GCHandle.Alloc( lPixel3, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel3Ptr.AddrOfPinnedObject() ) );

            lPixel3Ptr.Free();

            lUniform.SubroutineName = "sr2";

            GCHandle lPixel4Ptr = GCHandle.Alloc( lPixel4, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel4Ptr.AddrOfPinnedObject() ) );

            lPixel4Ptr.Free();

            Assert.IsTrue( lPixel1[ 0 ] == 0 && 
                           lPixel2[ 0 ] == 1 && 
                           lPixel3[ 0 ] == 0 && 
                           lPixel4[ 0 ] == 1 );

            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateRemovedSubroutine.xml" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateRemovedSubroutine.glsl" );
        }

        /// <summary>
        /// Checks the creation and update of a 
        /// pipeline description based resource with uniform blocks inside.
        /// </summary>
        [TestMethod]
        public void TestDescriptionResourceUpdateWithUniformBlocks()
        {
            LogManager.Instance.Log( "Running TestDescriptionResourceUpdateWithUniformBlocks...", LogType.INFO );

            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithUniformBlocks.xml", "<?xml version=\"1.0\" ?>\n<module name=\"TestDescriptionResourceUpdateWithUniformBlocks\" version=\"330\" fragment=\"TestDescriptionResourceUpdateWithUniformBlocks.glsl\">\n<uniform1i name=\"u\" x=\"1\"/>\n</module>\n" );
            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithUniformBlocks.glsl", "layout(location=0) out ivec4 color;\nuniform b { int u; };\nvoid main() { color = ivec4(u); }\n" );

            XMLResourceLoader lLoader = new TestResourceLoader();
            lLoader.AddPath( "." );
            IResourceService lResourceService = ServiceManager.Instance.GetService<IResourceService>();
            lResourceService.ModifyLoader( lLoader );

            PipelinePass lPass = null;
            IResource lPassResource = lResourceService.LoadResource( "TestDescriptionResourceUpdateWithUniformBlocks;" );
            if
                ( lPassResource != null )
            {
                lPass = lPassResource.OwnedObject as PipelinePass;
            }

            Assert.AreNotEqual( null, lPass );

            IntUniform lUniform = lPass.GetUniform<IntUniform>( new UniformName( "u" ) );

            Assert.AreNotEqual( null, lUniform );

            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );
            int[] lPixel1 = { 0 };
            int[] lPixel2 = { 0 };
            int[] lPixel3 = { 0 };
            int[] lPixel4 = { 0 };

            GCHandle lPixel1Ptr = GCHandle.Alloc( lPixel1, GCHandleType.Pinned );
            
            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel1Ptr.AddrOfPinnedObject() ) );

            lPixel1Ptr.Free();

            lUniform.Value = 2;

            GCHandle lPixel2Ptr = GCHandle.Alloc( lPixel2, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel2Ptr.AddrOfPinnedObject() ) );

            lPixel2Ptr.Free();

            TestHelpers.CreateFile( "TestDescriptionResourceUpdateWithUniformBlocks.glsl", "layout(location=0) out ivec4 color;\nuniform b { int u; };\nvoid main() { color = ivec4(u + 1); }\n" );
            lResourceService.UpdateResources();

            GCHandle lPixel3Ptr = GCHandle.Alloc( lPixel3, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel3Ptr.AddrOfPinnedObject() ) );

            lPixel3Ptr.Free();

            lUniform.Value = 3;

            GCHandle lPixel4Ptr = GCHandle.Alloc( lPixel4, GCHandleType.Pinned );

            lFrameBuffer.Clear( true, true, true );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 1, 1, PixelFormat.RedInteger, PixelType.Int, new BufferLayoutParameters(), new CPUBuffer( lPixel4Ptr.AddrOfPinnedObject() ) );

            lPixel4Ptr.Free();

            Assert.IsTrue( lPixel1[ 0 ] == 1 && 
                           lPixel2[ 0 ] == 2 && 
                           lPixel3[ 0 ] == 3 && 
                           lPixel4[ 0 ] == 4 );

            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateWithUniformBlocks.xml" );
            TestHelpers.DeleteFile( "TestDescriptionResourceUpdateWithUniformBlocks.glsl" );
        }

        #endregion Methods Tests
    }
}
