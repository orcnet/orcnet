﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK.Graphics.OpenGL;
using OrcNet.Core.Logger;
using OrcNet.Core.Math;
using OrcNet.Graphics.Render;
using OrcNet.Graphics.Render.Uniforms;
using OrcTest.Helpers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace OrcTest
{
    /// <summary>
    /// Uniform block based test class definition.
    /// </summary>
    [TestClass]
    public class TestUniformBlock
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TestUniformBlock"/> class.
        /// </summary>
        public TestUniformBlock()
        {
            TestSuite.Instance.AddTest( "TestUniformBlock1f", this.TestUniformBlock1f );
            TestSuite.Instance.AddTest( "TestUniformBlock2f", this.TestUniformBlock2f );
            TestSuite.Instance.AddTest( "TestUniformBlock3f", this.TestUniformBlock3f );
            TestSuite.Instance.AddTest( "TestUniformBlock4f", this.TestUniformBlock4f );
            TestSuite.Instance.AddTest( "TestUniformBlock1d", this.TestUniformBlock1d, 4 );
            TestSuite.Instance.AddTest( "TestUniformBlock2d", this.TestUniformBlock2d, 4 );
            TestSuite.Instance.AddTest( "TestUniformBlock3d", this.TestUniformBlock3d, 4 );
            TestSuite.Instance.AddTest( "TestUniformBlock4d", this.TestUniformBlock4d, 4 );
            TestSuite.Instance.AddTest( "TestUniformBlock1i", this.TestUniformBlock1i );
            TestSuite.Instance.AddTest( "TestUniformBlock2i", this.TestUniformBlock2i );
            TestSuite.Instance.AddTest( "TestUniformBlock3i", this.TestUniformBlock3i );
            TestSuite.Instance.AddTest( "TestUniformBlock4i", this.TestUniformBlock4i );
            TestSuite.Instance.AddTest( "TestUniformBlock1b", this.TestUniformBlock1b );
            TestSuite.Instance.AddTest( "TestUniformBlock2b", this.TestUniformBlock2b );
            TestSuite.Instance.AddTest( "TestUniformBlock3b", this.TestUniformBlock3b );
            TestSuite.Instance.AddTest( "TestUniformBlock4b", this.TestUniformBlock4b );
            TestSuite.Instance.AddTest( "TestUniformBlockMatrix3f", this.TestUniformBlockMatrix3f );
            TestSuite.Instance.AddTest( "TestUniformBlockMatrix3f", this.TestUniformBlockMatrix4f );
            TestSuite.Instance.AddTest( "TestUniformBlockStructure", this.TestUniformBlockStructure );
            TestSuite.Instance.AddTest( "TestUniformBlockBooleanArray", this.TestUniformBlockBooleanArray );
            TestSuite.Instance.AddTest( "TestUniformBlockFloatArray", this.TestUniformBlockFloatArray );
            TestSuite.Instance.AddTest( "TestUniformBlockIntArray", this.TestUniformBlockIntArray );
            TestSuite.Instance.AddTest( "TestUniformBlockStructureArray", this.TestUniformBlockStructureArray );
            TestSuite.Instance.AddTest( "TestAutoUniformBlockBufferBinding", this.TestAutoUniformBlockBufferBinding );
        }

        #endregion Constructor

        #region Methods Tests

        /// <summary>
        /// Checks an uniformBlock of 1 float.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock1f()
        {
            LogManager.Instance.Log( "Running TestUniformBlock1f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { float u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            FloatUniform lUniform = lPass.GetUniform<FloatUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = 1.0f;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 2 floats.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock2f()
        {
            LogManager.Instance.Log( "Running TestUniformBlock2f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { vec2 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector2FUniform lUniform = lPass.GetUniform<Vector2FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector2F( 1.0f, 2.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 3 floats.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock3f()
        {
            LogManager.Instance.Log( "Running TestUniformBlock3f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { vec3 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector3FUniform lUniform = lPass.GetUniform<Vector3FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector3F( 1.0f, 2.0f, 3.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 4 floats.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock4f()
        {
            LogManager.Instance.Log( "Running TestUniformBlock4f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { vec4 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = u; }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector4FUniform lUniform = lPass.GetUniform<Vector4FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector4F( 1.0f, 2.0f, 3.0f, 4.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 1 double.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock1d()
        {
            LogManager.Instance.Log( "Running TestUniformBlock1d...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { double u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            DoubleUniform lUniform = lPass.GetUniform<DoubleUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = 1.0;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0 );
        }

        /// <summary>
        /// Checks an uniformBlock of 2 doubles.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock2d()
        {
            LogManager.Instance.Log( "Running TestUniformBlock2d...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { dvec2 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector2DUniform lUniform = lPass.GetUniform<Vector2DUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector2D( 1.0, 2.0 );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 3 doubles.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock3d()
        {
            LogManager.Instance.Log( "Running TestUniformBlock3d...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { dvec3 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector3DUniform lUniform = lPass.GetUniform<Vector3DUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector3D( 1.0, 2.0, 3.0 );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 4 doubles.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock4d()
        {
            LogManager.Instance.Log( "Running TestUniformBlock4d...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { dvec4 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 400, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector4DUniform lUniform = lPass.GetUniform<Vector4DUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector4D( 1.0, 2.0, 3.0, 4.0 );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 1 integer.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock1i()
        {
            LogManager.Instance.Log( "Running TestUniformBlock1i...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { int u; };" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = ivec4(u, 0, 0, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            IntUniform lUniform = lPass.GetUniform<IntUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = 1;

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32i, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 );
        }

        /// <summary>
        /// Checks an uniformBlock of 2 integers.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock2i()
        {
            LogManager.Instance.Log( "Running TestUniformBlock2i...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { ivec2 u; };" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = ivec4(u, 0, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector2IUniform lUniform = lPass.GetUniform<Vector2IUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector2I( 1, 2 );

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32i, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 );
        }

        /// <summary>
        /// Checks an uniformBlock of 3 integers.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock3i()
        {
            LogManager.Instance.Log( "Running TestUniformBlock3i...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { ivec3 u; };" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = ivec4(u, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector3IUniform lUniform = lPass.GetUniform<Vector3IUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector3I( 1, 2, 3 );

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32i, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 );
        }

        /// <summary>
        /// Checks an uniformBlock of 4 integers.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock4i()
        {
            LogManager.Instance.Log( "Running TestUniformBlock4i...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { ivec4 u; };" );
            lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
            lBuilder.AppendLine( "void main() { color = u; }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector4IUniform lUniform = lPass.GetUniform<Vector4IUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Vector4I( 1, 2, 3, 4 );

            int[] lOutput =
            {
                0,
                0,
                0,
                0
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32i, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.RgbaInteger, 
                                     PixelType.Int, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1 &&
                           lOutput[ 1 ] == 2 &&
                           lOutput[ 2 ] == 3 &&
                           lOutput[ 3 ] == 4 );
        }

        /// <summary>
        /// Checks an uniformBlock of 1 boolean.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock1b()
        {
            LogManager.Instance.Log( "Running TestUniformBlock1b...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { bool u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u), 0, 0, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            BoolUniform lUniform = lPass.GetUniform<BoolUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = true;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.R32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 2 booleans.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock2b()
        {
            LogManager.Instance.Log( "Running TestUniformBlock2b...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { bvec2 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector2BUniform lUniform = lPass.GetUniform<Vector2BUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Tuple<bool, bool>( false, true );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rg32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 0.0f &&
                           lOutput[ 1 ] == 1.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 3 booleans.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock3b()
        {
            LogManager.Instance.Log( "Running TestUniformBlock3b...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { bvec3 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u, 0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector3BUniform lUniform = lPass.GetUniform<Vector3BUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Tuple<bool, bool, bool>( false, true, false );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 0.0f &&
                           lOutput[ 1 ] == 1.0f &&
                           lOutput[ 2 ] == 0.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of 4 booleans.
        /// </summary>
        [TestMethod]
        public void TestUniformBlock4b()
        {
            LogManager.Instance.Log( "Running TestUniformBlock4b...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { bvec4 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Vector4BUniform lUniform = lPass.GetUniform<Vector4BUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Value = new Tuple<bool, bool, bool, bool>( true, false, true, true );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 0.0f &&
                           lOutput[ 2 ] == 1.0f &&
                           lOutput[ 3 ] == 1.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of a Matrix3x3.
        /// </summary>
        [TestMethod]
        public void TestUniformBlockMatrix3f()
        {
            LogManager.Instance.Log( "Running TestUniformBlockMatrix3f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { mat3 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color.rgb = u * vec3(1.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Matrix3FUniform lUniform = lPass.GetUniform<Matrix3FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Matrix = new Matrix3F( 1.0f, 2.0f, 3.0f, 
                                            0.0f, 4.0f, 5.0f, 
                                            0.0f, 0.0f, 6.0f );
            
            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgb32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 6.0f &&
                           lOutput[ 1 ] == 9.0f &&
                           lOutput[ 2 ] == 6.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of a Matrix4x4.
        /// </summary>
        [TestMethod]
        public void TestUniformBlockMatrix4f()
        {
            LogManager.Instance.Log( "Running TestUniformBlockMatrix4f...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { mat4 u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = u * vec4(1.0); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniform.
            Matrix4FUniform lUniform = lPass.GetUniform<Matrix4FUniform>( new UniformName( "u" ) );

            Assert.IsNotNull( lUniform );

            lUniform.Matrix = new Matrix4F( 1.0f, 2.0f, 3.0f, 4.0f, 
                                            0.0f, 5.0f, 6.0f, 7.0f, 
                                            0.0f, 0.0f, 8.0f, 9.0f, 
                                            0.0f, 0.0f, 0.0f, 10.0f );
            
            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 10.0f &&
                           lOutput[ 1 ] == 18.0f &&
                           lOutput[ 2 ] == 17.0f &&
                           lOutput[ 3 ] == 10.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of a structure.
        /// </summary>
        [TestMethod]
        public void TestUniformBlockStructure()
        {
            LogManager.Instance.Log( "Running TestUniformBlockStructure...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "struct s { bool a; float b; int i; mat2 m; };" );
            lBuilder.AppendLine( "uniform b { s u; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u.a), u.b, float(u.i), dot(u.m * vec2(1.0), vec2(1.0))); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniforms.
            BoolUniform lBoolUniform = lPass.GetUniform<BoolUniform>( new UniformName( "u.a" ) );

            Assert.IsNotNull( lBoolUniform );

            lBoolUniform.Value = true;

            FloatUniform lFloatUniform = lPass.GetUniform<FloatUniform>( new UniformName( "u.b" ) );

            Assert.IsNotNull( lFloatUniform );

            lFloatUniform.Value = 2.0f;

            UniformBlock lBlock = lPass[ new UniformBlockName( "b" ) ];

            Assert.IsNotNull( lBlock );

            IntUniform lIntUniform = lBlock.GetUniform<IntUniform>( "u.i" );

            Assert.IsNotNull( lIntUniform );

            lIntUniform.Value = 3;

            Matrix2FUniform lMat2x2Uniform = lBlock.GetUniform<Matrix2FUniform>( "u.m" );

            Assert.IsNotNull( lMat2x2Uniform );

            lMat2x2Uniform.Matrix = new Matrix2F( 1.0f, 2.0f, 
                                                  0.0f, 3.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 6.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of an array of booleans.
        /// </summary>
        [TestMethod]
        public void TestUniformBlockBooleanArray()
        {
            LogManager.Instance.Log( "Running TestUniformBlockBooleanArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { bool u[4]; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u[0]), float(u[1]), float(u[2]), float(u[3])); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniforms.
            BoolUniform lBool1Uniform = lPass.GetUniform<BoolUniform>( new UniformName( "u[0]" ) );

            Assert.IsNotNull( lBool1Uniform );

            lBool1Uniform.Value = true;

            BoolUniform lBool2Uniform = lPass.GetUniform<BoolUniform>( new UniformName( "u[1]" ) );

            Assert.IsNotNull( lBool2Uniform );

            lBool2Uniform.Value = false;

            UniformBlock lBlock = lPass[ new UniformBlockName( "b" ) ];

            Assert.IsNotNull( lBlock );

            BoolUniform lBool3Uniform = lBlock.GetUniform<BoolUniform>( "u[2]" );

            Assert.IsNotNull( lBool3Uniform );

            lBool3Uniform.Value = true;

            BoolUniform lBool4Uniform = lBlock.GetUniform<BoolUniform>( "u[3]" );

            Assert.IsNotNull( lBool4Uniform );

            lBool4Uniform.Value = true;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 0.0f &&
                           lOutput[ 2 ] == 1.0f &&
                           lOutput[ 3 ] == 1.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of an array of floats.
        /// </summary>
        [TestMethod]
        public void TestUniformBlockFloatArray()
        {
            LogManager.Instance.Log( "Running TestUniformBlockFloatArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { float u[4]; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u[0], u[1], u[2], u[3]); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniforms.
            FloatUniform lFloat1Uniform = lPass.GetUniform<FloatUniform>( new UniformName( "u[0]" ) );

            Assert.IsNotNull( lFloat1Uniform );

            lFloat1Uniform.Value = 1.0f;

            FloatUniform lFloat2Uniform = lPass.GetUniform<FloatUniform>( new UniformName( "u[1]" ) );

            Assert.IsNotNull( lFloat2Uniform );

            lFloat2Uniform.Value = 2.0f;

            UniformBlock lBlock = lPass[ new UniformBlockName( "b" ) ];

            Assert.IsNotNull( lBlock );

            FloatUniform lFloat3Uniform = lBlock.GetUniform<FloatUniform>( "u[2]" );

            Assert.IsNotNull( lFloat3Uniform );

            lFloat3Uniform.Value = 3.0f;

            FloatUniform lFloat4Uniform = lBlock.GetUniform<FloatUniform>( "u[3]" );

            Assert.IsNotNull( lFloat4Uniform );

            lFloat4Uniform.Value = 4.0f;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of an array of integers.
        /// </summary>
        [TestMethod]
        public void TestUniformBlockIntArray()
        {
            LogManager.Instance.Log( "Running TestUniformBlockIntArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "uniform b { int u[4]; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(u[0], u[1], u[2], u[3]); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniforms.
            IntUniform lInt1Uniform = lPass.GetUniform<IntUniform>( new UniformName( "u[0]" ) );

            Assert.IsNotNull( lInt1Uniform );

            lInt1Uniform.Value = 1;

            IntUniform lInt2Uniform = lPass.GetUniform<IntUniform>( new UniformName( "u[1]" ) );

            Assert.IsNotNull( lInt2Uniform );

            lInt2Uniform.Value = 2;

            UniformBlock lBlock = lPass[ new UniformBlockName( "b" ) ];

            Assert.IsNotNull( lBlock );

            IntUniform lInt3Uniform = lBlock.GetUniform<IntUniform>( "u[2]" );

            Assert.IsNotNull( lInt3Uniform );

            lInt3Uniform.Value = 3;

            IntUniform lInt4Uniform = lBlock.GetUniform<IntUniform>( "u[3]" );

            Assert.IsNotNull( lInt4Uniform );

            lInt4Uniform.Value = 4;

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 4.0f );
        }

        /// <summary>
        /// Checks an uniformBlock of an array of structures.
        /// </summary>
        [TestMethod]
        public void TestUniformBlockStructureArray()
        {
            LogManager.Instance.Log( "Running TestUniformBlockStructureArray...", LogType.INFO );

            // Create the pipeline pass.
            StringBuilder lBuilder = new StringBuilder();
            lBuilder.AppendLine( "struct s { bool a; float b; int i; mat2 m; };" );
            lBuilder.AppendLine( "uniform b { s u[4]; };" );
            lBuilder.AppendLine( "layout(location=0) out vec4 color;" );
            lBuilder.AppendLine( "void main() { color = vec4(float(u[0].a), u[1].b, float(u[2].i), dot(u[3].m * vec2(1.0), vec2(1.0))); }" );
            PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                   new PipelineStage( 330, 
                                                                      lBuilder.ToString(), 
                                                                      StageType.FRAGMENT ) ) );

            // Get the corresponding uniforms.
            BoolUniform lBoolUniform = lPass.GetUniform<BoolUniform>( new UniformName( "u[0].a" ) );

            Assert.IsNotNull( lBoolUniform );

            lBoolUniform.Value = true;

            FloatUniform lFloatUniform = lPass.GetUniform<FloatUniform>( new UniformName( "u[1].b" ) );

            Assert.IsNotNull( lFloatUniform );

            lFloatUniform.Value = 2.0f;

            UniformBlock lBlock = lPass[ new UniformBlockName( "b" ) ];

            Assert.IsNotNull( lBlock );

            IntUniform lIntUniform = lBlock.GetUniform<IntUniform>( "u[2].i" );

            Assert.IsNotNull( lIntUniform );

            lIntUniform.Value = 3;

            Matrix2FUniform lMat2x2Uniform = lBlock.GetUniform<Matrix2FUniform>( "u[3].m" );

            Assert.IsNotNull( lMat2x2Uniform );

            lMat2x2Uniform.Matrix = new Matrix2F( 1.0f, 2.0f, 
                                                  0.0f, 3.0f );

            float[] lOutput =
            {
                0.0f,
                0.0f,
                0.0f,
                0.0f
            };

            GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
            FrameBuffer lFrameBuffer = TestHelpers.GetFrameBuffer( RenderbufferStorage.Rgba32f, 1, 1 );
            lFrameBuffer.DrawQuad( lPass );
            lFrameBuffer.ReadPixels( 0, 0, 
                                     1, 1, 
                                     PixelFormat.Rgba, 
                                     PixelType.Float, 
                                     new BufferLayoutParameters(), 
                                     new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );

            lOutputPtr.Free();

            Assert.IsTrue( lOutput[ 0 ] == 1.0f &&
                           lOutput[ 1 ] == 2.0f &&
                           lOutput[ 2 ] == 3.0f &&
                           lOutput[ 3 ] == 6.0f );
        }
        
        /// <summary>
        /// Checks the automatic binding of an uniformBlock buffer.
        /// </summary>
        [TestMethod]
        public void TestAutoUniformBlockBufferBinding()
        {
            List<GPUBuffer> lBuffers = new List<GPUBuffer>();
            for
                ( int lCurr = 0; lCurr < 128; lCurr++ )
            {
                GPUBuffer lBuffer = new GPUBuffer();
                lBuffer.SetData( 16, IntPtr.Zero, BufferUsageHint.DynamicDraw );
                lBuffers.Add( lBuffer );
            }

            List<PipelinePass> lPasses = new List<PipelinePass>();
            for
                ( int lCurr = 0; lCurr < 48; lCurr++ )
            {
                StringBuilder lBuilder = new StringBuilder();
                lBuilder.AppendLine( "uniform b1 { int i1; };" );
                lBuilder.AppendLine( "uniform b2 { int i2; };" );
                lBuilder.AppendLine( "uniform b3 { int i3; };" );
                lBuilder.AppendLine( "uniform b4 { int i4; };" );
                lBuilder.AppendLine( "layout(location=0) out ivec4 color;" );
                lBuilder.AppendLine( "void main() { color = ivec4(i1 + i2 + i3 + i4); }" );
                PipelinePass lPass = new PipelinePass( new PipelineDescription( 
                                                       new PipelineStage( 330, 
                                                                          lBuilder.ToString(), 
                                                                          StageType.FRAGMENT ) ) );

                UniformBlock lBlock1 = lPass[ new UniformBlockName( "b1" ) ];
                lBlock1.Buffer = lBuffers[ (3 * lCurr) % 128 ];
                UniformBlock lBlock2 = lPass[ new UniformBlockName( "b2" ) ];
                lBlock2.Buffer = lBuffers[ (3 * lCurr + 1) % 128 ];
                UniformBlock lBlock3 = lPass[ new UniformBlockName( "b3" ) ];
                lBlock3.Buffer = lBuffers[ (3 * lCurr + 2) % 128 ];
                UniformBlock lBlock4 = lPass[ new UniformBlockName( "b4" ) ];
                lBlock4.Buffer = lBuffers[ (3 * lCurr + 3) % 128 ];

                lPass.GetUniform<IntUniform>( new UniformName( "i1" ) ).Value = (3 * lCurr) % 128;
                lPass.GetUniform<IntUniform>( new UniformName( "i2" ) ).Value = (3 * lCurr + 1) % 128;
                lBlock3.GetUniform<IntUniform>( "i3" ).Value = (3 * lCurr + 2) % 128;
                lBlock4.GetUniform<IntUniform>( "i4" ).Value = (3 * lCurr + 3) % 128;
                lPasses.Add( lPass );
            }

            FrameBuffer lFrameBuffer = new FrameBuffer();
            lFrameBuffer.Attach( BufferId.COLOR0, new RenderBuffer( RenderbufferStorage.R32i, 1, 1 ) );
            lFrameBuffer.Parameters.SingleViewport( new Viewport( 0.0f, 0.0f, 1.0f, 1.0f ) );

            bool lResult = true;
            for 
                ( int lDraw = 0; lDraw < 96; lDraw++ )
            {
                int lPassId  = lDraw % 48;
                int lToMatch = (3 * lPassId) % 128 + (3 * lPassId + 1) % 128 + (3 * lPassId + 2) % 128 + (3 * lPassId + 3) % 128;

                int[] lOutput =
                {
                    0
                };
                GCHandle lOutputPtr = GCHandle.Alloc( lOutput, GCHandleType.Pinned );
            
                lFrameBuffer.Clear( true, true, true );
                lFrameBuffer.DrawQuad( lPasses[ lPassId ] );
                lFrameBuffer.ReadPixels( 0, 0, 
                                         1, 1, 
                                         PixelFormat.RedInteger, 
                                         PixelType.Int, 
                                         new BufferLayoutParameters(), 
                                         new CPUBuffer( lOutputPtr.AddrOfPinnedObject() ) );
                lOutputPtr.Free();

                lResult = lResult && (lOutput[ 0 ] == lToMatch);
            }

            Assert.IsTrue( lResult );
        }

        #endregion Methods Tests
    }
}
